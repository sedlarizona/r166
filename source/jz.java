import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jz")
public class jz extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static Cache q = new Cache(64);

    @ObfuscatedName("ks")
    static int ks;

    @ObfuscatedName("i")
    public boolean i = false;

    @ObfuscatedName("q")
    public void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 2) {
            this.i = true;
        }

    }

    @ObfuscatedName("u")
    public static void u() {
        NpcDefinition.i.a();
        NpcDefinition.a.a();
    }
}
