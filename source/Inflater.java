import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gs")
public class Inflater {

    @ObfuscatedName("t")
    java.util.zip.Inflater inflater;

    Inflater(int var1, int var2, int var3) {
    }

    public Inflater() {
        this(-1, 1000000, 1000000);
    }

    @ObfuscatedName("t")
    public void t(ByteBuffer var1, byte[] var2) {
        if (var1.buffer[var1.index] == 31 && var1.buffer[var1.index + 1] == -117) {
            if (this.inflater == null) {
                this.inflater = new java.util.zip.Inflater(true);
            }

            try {
                this.inflater.setInput(var1.buffer, var1.index + 10, var1.buffer.length - (var1.index + 8 + 10));
                this.inflater.inflate(var2);
            } catch (Exception var4) {
                this.inflater.reset();
                throw new RuntimeException("");
            }

            this.inflater.reset();
        } else {
            throw new RuntimeException("");
        }
    }

    @ObfuscatedName("t")
    public static RTComponent t(int var0) {
        int var1 = var0 >> 16;
        int var2 = var0 & '\uffff';
        if (RTComponent.b[var1] == null || RTComponent.b[var1][var2] == null) {
            boolean var3 = RuneScript.i(var1);
            if (!var3) {
                return null;
            }
        }

        return RTComponent.b[var1][var2];
    }

    @ObfuscatedName("a")
    public static boolean a(iw var0) {
        return iw.t == var0 || iw.q == var0 || iw.i == var0 || iw.a == var0 || iw.l == var0 || iw.b == var0
                || iw.e == var0 || iw.x == var0;
    }

    @ObfuscatedName("l")
    static int l(int var0, RuneScript var1, boolean var2) {
        int var4 = -1;
        RTComponent var3;
        if (var0 >= 2000) {
            var0 -= 1000;
            var4 = cs.e[--b.menuX];
            var3 = t(var4);
        } else {
            var3 = var2 ? ho.v : cs.c;
        }

        if (var0 == 1100) {
            b.menuX -= 2;
            var3.horizontalScrollbarPosition = cs.e[b.menuX];
            if (var3.horizontalScrollbarPosition > var3.al - var3.width) {
                var3.horizontalScrollbarPosition = var3.al - var3.width;
            }

            if (var3.horizontalScrollbarPosition < 0) {
                var3.horizontalScrollbarPosition = 0;
            }

            var3.verticalScrollbarPosition = cs.e[b.menuX + 1];
            if (var3.verticalScrollbarPosition > var3.at - var3.height) {
                var3.verticalScrollbarPosition = var3.at - var3.height;
            }

            if (var3.verticalScrollbarPosition < 0) {
                var3.verticalScrollbarPosition = 0;
            }

            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1101) {
            var3.color = cs.e[--b.menuX];
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1102) {
            var3.aa = cs.e[--b.menuX] == 1;
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1103) {
            var3.alpha = cs.e[--b.menuX];
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1104) {
            var3.ac = cs.e[--b.menuX];
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1105) {
            var3.spriteId = cs.e[--b.menuX];
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1106) {
            var3.spriteRotation = cs.e[--b.menuX];
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1107) {
            var3.bk = cs.e[--b.menuX] == 1;
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1108) {
            var3.entityType = 1;
            var3.entityId = cs.e[--b.menuX];
            GameEngine.jk(var3);
            return 1;
        } else if (var0 == 1109) {
            b.menuX -= 6;
            var3.bq = cs.e[b.menuX];
            var3.bz = cs.e[b.menuX + 1];
            var3.bx = cs.e[b.menuX + 2];
            var3.bf = cs.e[b.menuX + 3];
            var3.bo = cs.e[b.menuX + 4];
            var3.bv = cs.e[b.menuX + 5];
            GameEngine.jk(var3);
            return 1;
        } else {
            int var9;
            if (var0 == 1110) {
                var9 = cs.e[--b.menuX];
                if (var9 != var3.animationId) {
                    var3.animationId = var9;
                    var3.et = 0;
                    var3.ev = 0;
                    GameEngine.jk(var3);
                }

                return 1;
            } else if (var0 == 1111) {
                var3.bl = cs.e[--b.menuX] == 1;
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1112) {
                String var8 = cs.p[--ly.g];
                if (!var8.equals(var3.text)) {
                    var3.text = var8;
                    GameEngine.jk(var3);
                }

                return 1;
            } else if (var0 == 1113) {
                var3.fontId = cs.e[--b.menuX];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1114) {
                b.menuX -= 3;
                var3.cu = cs.e[b.menuX];
                var3.cs = cs.e[b.menuX + 1];
                var3.cm = cs.e[b.menuX + 2];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1115) {
                var3.ct = cs.e[--b.menuX] == 1;
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1116) {
                var3.borderThickness = cs.e[--b.menuX];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1117) {
                var3.shadowColor = cs.e[--b.menuX];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1118) {
                var3.bm = cs.e[--b.menuX] == 1;
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1119) {
                var3.bh = cs.e[--b.menuX] == 1;
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1120) {
                b.menuX -= 2;
                var3.al = cs.e[b.menuX];
                var3.at = cs.e[b.menuX + 1];
                GameEngine.jk(var3);
                if (var4 != -1 && var3.type == 0) {
                    GameEngine.ig(RTComponent.b[var4 >> 16], var3, false);
                }

                return 1;
            } else if (var0 == 1121) {
                jq.hd(var3.id, var3.w);
                Client.ls = var3;
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1122) {
                var3.br = cs.e[--b.menuX];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1123) {
                var3.as = cs.e[--b.menuX];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1124) {
                var3.ab = cs.e[--b.menuX];
                GameEngine.jk(var3);
                return 1;
            } else if (var0 == 1125) {
                var9 = cs.e[--b.menuX];
                ld[] var6 = new ld[] { ld.q, ld.i, ld.l, ld.t, ld.a };
                ld var7 = (ld) aj.t(var6, var9);
                if (var7 != null) {
                    var3.af = var7;
                    GameEngine.jk(var3);
                }

                return 1;
            } else if (var0 == 1126) {
                boolean var5 = cs.e[--b.menuX] == 1;
                var3.ad = var5;
                return 1;
            } else {
                return 2;
            }
        }
    }

    @ObfuscatedName("fv")
    static void fv(Player var0, int var1, int var2) {
        if (var0.animationId == var1 && var1 != -1) {
            int var3 = ff.t(var1).w;
            if (var3 == 1) {
                var0.animationFrameIndex = 0;
                var0.bb = 0;
                var0.bq = var2;
                var0.bz = 0;
            }

            if (var3 == 2) {
                var0.bz = 0;
            }
        } else if (var1 == -1 || var0.animationId == -1 || ff.t(var1).c >= ff.t(var0.animationId).c) {
            var0.animationId = var1;
            var0.animationFrameIndex = 0;
            var0.bb = 0;
            var0.bq = var2;
            var0.bz = 0;
            var0.ci = var0.speed;
        }

    }

    @ObfuscatedName("hq")
    static final void hq(bl var0) {
        int var1 = 0;
        int var2 = -1;
        int var3 = 0;
        int var4 = 0;
        if (var0.q == 0) {
            var1 = an.loadedRegion.ap(var0.t, var0.i, var0.a);
        }

        if (var0.q == 1) {
            var1 = an.loadedRegion.ah(var0.t, var0.i, var0.a);
        }

        if (var0.q == 2) {
            var1 = an.loadedRegion.au(var0.t, var0.i, var0.a);
        }

        if (var0.q == 3) {
            var1 = an.loadedRegion.ax(var0.t, var0.i, var0.a);
        }

        if (var1 != 0) {
            int var5 = an.loadedRegion.ar(var0.t, var0.i, var0.a, var1);
            var2 = var1 >> 14 & 32767;
            var3 = var5 & 31;
            var4 = var5 >> 6 & 3;
        }

        var0.l = var2;
        var0.e = var3;
        var0.b = var4;
    }

    @ObfuscatedName("hr")
    static fv hr(int var0, int var1) {
        Client.rn.t = var0;
        Client.rn.q = var1;
        Client.rn.i = 1;
        Client.rn.a = 1;
        return Client.rn;
    }
}
