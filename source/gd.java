import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gd")
public class gd extends Node {

    @ObfuscatedName("rb")
    static fn rb;

    @ObfuscatedName("l")
    public static gd[] l = new gd[300];

    @ObfuscatedName("b")
    public static int b = 0;

    @ObfuscatedName("eq")
    static int eq;

    @ObfuscatedName("t")
    public fo t;

    @ObfuscatedName("q")
    public int q;

    @ObfuscatedName("i")
    public Packet i;

    @ObfuscatedName("a")
    public int a;

    @ObfuscatedName("i")
    public void i() {
        if (b < l.length) {
            l[++b - 1] = this;
        }
    }

    @ObfuscatedName("i")
    static int i(int var0, RuneScript var1, boolean var2) {
        int var3;
        int var4;
        if (var0 == 100) {
            b.menuX -= 3;
            var3 = cs.e[b.menuX];
            var4 = cs.e[b.menuX + 1];
            int var5 = cs.e[b.menuX + 2];
            if (var4 == 0) {
                throw new RuntimeException();
            } else {
                RTComponent var6 = Inflater.t(var3);
                if (var6.cs2components == null) {
                    var6.cs2components = new RTComponent[var5 + 1];
                }

                if (var6.cs2components.length <= var5) {
                    RTComponent[] var7 = new RTComponent[var5 + 1];

                    for (int var8 = 0; var8 < var6.cs2components.length; ++var8) {
                        var7[var8] = var6.cs2components[var8];
                    }

                    var6.cs2components = var7;
                }

                if (var5 > 0 && var6.cs2components[var5 - 1] == null) {
                    throw new RuntimeException("" + (var5 - 1));
                } else {
                    RTComponent var12 = new RTComponent();
                    var12.type = var4;
                    var12.parentId = var12.id = var6.id;
                    var12.w = var5;
                    var12.modern = true;
                    var6.cs2components[var5] = var12;
                    if (var2) {
                        ho.v = var12;
                    } else {
                        cs.c = var12;
                    }

                    GameEngine.jk(var6);
                    return 1;
                }
            }
        } else {
            RTComponent var9;
            if (var0 == 101) {
                var9 = var2 ? ho.v : cs.c;
                RTComponent var10 = Inflater.t(var9.id);
                var10.cs2components[var9.w] = null;
                GameEngine.jk(var10);
                return 1;
            } else if (var0 == 102) {
                var9 = Inflater.t(cs.e[--b.menuX]);
                var9.cs2components = null;
                GameEngine.jk(var9);
                return 1;
            } else if (var0 != 200) {
                if (var0 == 201) {
                    var9 = Inflater.t(cs.e[--b.menuX]);
                    if (var9 != null) {
                        cs.e[++b.menuX - 1] = 1;
                        if (var2) {
                            ho.v = var9;
                        } else {
                            cs.c = var9;
                        }
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                } else {
                    return 2;
                }
            } else {
                b.menuX -= 2;
                var3 = cs.e[b.menuX];
                var4 = cs.e[b.menuX + 1];
                RTComponent var11 = CollisionMap.q(var3, var4);
                if (var11 != null && var4 != -1) {
                    cs.e[++b.menuX - 1] = 1;
                    if (var2) {
                        ho.v = var11;
                    } else {
                        cs.c = var11;
                    }
                } else {
                    cs.e[++b.menuX - 1] = 0;
                }

                return 1;
            }
        }
    }
}
