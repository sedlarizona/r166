import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jk")
public class jk extends hh {

    @ObfuscatedName("db")
    static ju db;

    @ObfuscatedName("kz")
    static RTComponent kz;

    @ObfuscatedName("t")
    public ju t;

    @ObfuscatedName("q")
    public int q;

    @ObfuscatedName("i")
    public byte i;

    @ObfuscatedName("iq")
   static final void iq(RTComponent[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      li.ck(var2, var3, var4, var5);
      eu.t();

      for(int var9 = 0; var9 < var0.length; ++var9) {
         RTComponent var10 = var0[var9];
         if (var10 != null && (var10.parentId == var1 || var1 == -1412584499 && var10 == Client.ln)) {
            int var11;
            if (var8 == -1) {
               Client.interfaceXPositions[Client.nm] = var10.relativeX + var6;
               Client.interfaceYPositions[Client.nm] = var7 + var10.relativeY;
               Client.interfaceWidths[Client.nm] = var10.width;
               Client.interfaceHeights[Client.nm] = var10.height;
               var11 = ++Client.nm - 1;
            } else {
               var11 = var8;
            }

            var10.arrayIndex = var11;
            var10.cycle = Client.bz;
            if (var10.modern) {
               boolean var12 = var10.hidden;
               if (var12) {
                  continue;
               }
            }

            int var39;
            if (var10.contentType > 0) {
               var39 = var10.contentType;
               if (var39 == 324) {
                  if (Client.qn == -1) {
                     Client.qn = var10.spriteId;
                     Client.qx = var10.br;
                  }

                  if (Client.qv.female) {
                     var10.spriteId = Client.qn;
                  } else {
                     var10.spriteId = Client.qx;
                  }
               } else if (var39 == 325) {
                  if (Client.qn == -1) {
                     Client.qn = var10.spriteId;
                     Client.qx = var10.br;
                  }

                  if (Client.qv.female) {
                     var10.spriteId = Client.qx;
                  } else {
                     var10.spriteId = Client.qn;
                  }
               } else if (var39 == 327) {
                  var10.bx = 150;
                  var10.bf = (int)(Math.sin((double)Client.bz / 40.0D) * 256.0D) & 2047;
                  var10.entityType = 5;
                  var10.entityId = 0;
               } else if (var39 == 328) {
                  var10.bx = 150;
                  var10.bf = (int)(Math.sin((double)Client.bz / 40.0D) * 256.0D) & 2047;
                  var10.entityType = 5;
                  var10.entityId = 1;
               }
            }

            var39 = var10.relativeX + var6;
            int var13 = var7 + var10.relativeY;
            int var14 = var10.alpha;
            int var15;
            int var16;
            if (var10 == Client.ln) {
               if (var1 != -1412584499 && !var10.cz) {
                  i.mq = var0;
                  ItemStorage.mr = var6;
                  c.mx = var7;
                  continue;
               }

               if (Client.lr && Client.lk) {
                  var15 = bs.n;
                  var16 = bs.x;
                  var15 -= Client.li;
                  var16 -= Client.lf;
                  if (var15 < Client.la) {
                     var15 = Client.la;
                  }

                  if (var15 + var10.width > Client.la + Client.lm.width) {
                     var15 = Client.la + Client.lm.width - var10.width;
                  }

                  if (var16 < Client.le) {
                     var16 = Client.le;
                  }

                  if (var16 + var10.height > Client.le + Client.lm.height) {
                     var16 = Client.le + Client.lm.height - var10.height;
                  }

                  var39 = var15;
                  var13 = var16;
               }

               if (!var10.cz) {
                  var14 = 128;
               }
            }

            int var17;
            int var18;
            int var20;
            int var21;
            int var22;
            int var30;
            if (var10.type == 2) {
               var15 = var2;
               var16 = var3;
               var17 = var4;
               var18 = var5;
            } else if (var10.type == 9) {
               var30 = var39;
               var20 = var13;
               var21 = var39 + var10.width;
               var22 = var13 + var10.height;
               if (var21 < var39) {
                  var30 = var21;
                  var21 = var39;
               }

               if (var22 < var13) {
                  var20 = var22;
                  var22 = var13;
               }

               ++var21;
               ++var22;
               var15 = var30 > var2 ? var30 : var2;
               var16 = var20 > var3 ? var20 : var3;
               var17 = var21 < var4 ? var21 : var4;
               var18 = var22 < var5 ? var22 : var5;
            } else {
               var30 = var39 + var10.width;
               var20 = var13 + var10.height;
               var15 = var39 > var2 ? var39 : var2;
               var16 = var13 > var3 ? var13 : var3;
               var17 = var30 < var4 ? var30 : var4;
               var18 = var20 < var5 ? var20 : var5;
            }

            if (!var10.modern || var15 < var17 && var16 < var18) {
               int var23;
               int var24;
               int var25;
               int var26;
               int var27;
               if (var10.contentType != 0) {
                  if (var10.contentType == 1336) {
                     if (Client.bl) {
                        var13 += 15;
                        fv.et.f("Fps:" + GameEngine.n, var39 + var10.width, var13, 16776960, -1);
                        var13 += 15;
                        Runtime var43 = Runtime.getRuntime();
                        var20 = (int)((var43.totalMemory() - var43.freeMemory()) / 1024L);
                        var21 = 16776960;
                        if (var20 > 327680 && !Client.bh) {
                           var21 = 16711680;
                        }

                        fv.et.f("Mem:" + var20 + "k", var39 + var10.width, var13, var21, -1);
                        var13 += 15;
                     }
                     continue;
                  }

                  if (var10.contentType == 1337) {
                     Client.kj = var39;
                     Client.ku = var13;
                     e.gd(var39, var13, var10.width, var10.height);
                     Client.outdatedInterfaces[var10.arrayIndex] = true;
                     li.ck(var2, var3, var4, var5);
                     continue;
                  }

                  iq var42;
                  if (var10.contentType == 1338) {
                     al.fb();
                     var42 = var10.k(false);
                     if (var42 != null) {
                        li.ck(var39, var13, var39 + var42.t, var13 + var42.q);
                        if (Client.oo != 2 && Client.oo != 5) {
                           var20 = Client.hintPlane & 2047;
                           var21 = az.il.regionX / 32 + 48;
                           var22 = 464 - az.il.regionY / 32;
                           GrandExchangeOffer.on.au(var39, var13, var42.t, var42.q, var21, var22, var20, 256, var42.a, var42.i);

                           for(var23 = 0; var23 < Client.oc; ++var23) {
                              var24 = Client.ov[var23] * 4 + 2 - az.il.regionX / 32;
                              var25 = Client.od[var23] * 4 + 2 - az.il.regionY / 32;
                              p.jj(var39, var13, var24, var25, Client.ou[var23], var42);
                           }

                           var23 = 0;

                           while(true) {
                              if (var23 >= 104) {
                                 for(var23 = 0; var23 < Client.dz; ++var23) {
                                    Npc var53 = Client.loadedNpcs[Client.npcIndices[var23]];
                                    if (var53 != null && var53.k()) {
                                       NpcDefinition var47 = var53.composite;
                                       if (var47 != null && var47.ae != null) {
                                          var47 = var47.x();
                                       }

                                       if (var47 != null && var47.f && var47.ap) {
                                          var26 = var53.regionX / 32 - az.il.regionX / 32;
                                          var27 = var53.regionY / 32 - az.il.regionY / 32;
                                          p.jj(var39, var13, var26, var27, Tile.fc[1], var42);
                                       }
                                    }
                                 }

                                 var23 = cx.b;
                                 int[] var54 = cx.e;

                                 for(var25 = 0; var25 < var23; ++var25) {
                                    Player var37 = Client.loadedPlayers[var54[var25]];
                                    if (var37 != null && var37.k() && !var37.f && var37 != az.il) {
                                       var27 = var37.regionX / 32 - az.il.regionX / 32;
                                       int var38 = var37.regionY / 32 - az.il.regionY / 32;
                                       boolean var48 = false;
                                       if (az.il.team != 0 && var37.team != 0 && var37.team == az.il.team) {
                                          var48 = true;
                                       }

                                       if (var37.q()) {
                                          p.jj(var39, var13, var27, var38, Tile.fc[3], var42);
                                       } else if (var48) {
                                          p.jj(var39, var13, var27, var38, Tile.fc[4], var42);
                                       } else if (var37.l()) {
                                          p.jj(var39, var13, var27, var38, Tile.fc[5], var42);
                                       } else {
                                          p.jj(var39, var13, var27, var38, Tile.fc[2], var42);
                                       }
                                    }
                                 }

                                 if (Client.hintArrowType != 0 && Client.bz % 20 < 10) {
                                    if (Client.hintArrowType == 1 && Client.hintNpcIndex >= 0 && Client.hintNpcIndex < Client.loadedNpcs.length) {
                                       Npc var49 = Client.loadedNpcs[Client.hintNpcIndex];
                                       if (var49 != null) {
                                          var26 = var49.regionX / 32 - az.il.regionX / 32;
                                          var27 = var49.regionY / 32 - az.il.regionY / 32;
                                          a.jn(var39, var13, var26, var27, bt.fj[1], var42);
                                       }
                                    }

                                    if (Client.hintArrowType == 2) {
                                       var25 = Client.hintX * 4 - an.regionBaseX * 4 + 2 - az.il.regionX / 32;
                                       var26 = Client.hintY * 4 - PlayerComposite.ep * 4 + 2 - az.il.regionY / 32;
                                       a.jn(var39, var13, var25, var26, bt.fj[1], var42);
                                    }

                                    if (Client.hintArrowType == 10 && Client.hintPlayerIndex >= 0 && Client.hintPlayerIndex < Client.loadedPlayers.length) {
                                       Player var50 = Client.loadedPlayers[Client.hintPlayerIndex];
                                       if (var50 != null) {
                                          var26 = var50.regionX / 32 - az.il.regionX / 32;
                                          var27 = var50.regionY / 32 - az.il.regionY / 32;
                                          a.jn(var39, var13, var26, var27, bt.fj[1], var42);
                                       }
                                    }
                                 }

                                 if (Client.destinationX != 0) {
                                    var25 = Client.destinationX * 4 + 2 - az.il.regionX / 32;
                                    var26 = Client.destinationY * 4 + 2 - az.il.regionY / 32;
                                    p.jj(var39, var13, var25, var26, bt.fj[0], var42);
                                 }

                                 if (!az.il.f) {
                                    li.dl(var42.t / 2 + var39 - 1, var42.q / 2 + var13 - 1, 3, 3, 16777215);
                                 }
                                 break;
                              }

                              for(var24 = 0; var24 < 104; ++var24) {
                                 Deque var31 = Client.loadedGroundItems[kt.ii][var23][var24];
                                 if (var31 != null) {
                                    var26 = var23 * 4 + 2 - az.il.regionX / 32;
                                    var27 = var24 * 4 + 2 - az.il.regionY / 32;
                                    p.jj(var39, var13, var26, var27, Tile.fc[0], var42);
                                 }
                              }

                              ++var23;
                           }
                        } else {
                           li.dv(var39, var13, 0, var42.a, var42.i);
                        }

                        Client.nt[var11] = true;
                     }

                     li.ck(var2, var3, var4, var5);
                     continue;
                  }

                  if (var10.contentType == 1339) {
                     var42 = var10.k(false);
                     if (var42 != null) {
                        if (Client.oo < 3) {
                           y.fy.au(var39, var13, var42.t, var42.q, 25, 25, Client.hintPlane, 256, var42.a, var42.i);
                        } else {
                           li.dv(var39, var13, 0, var42.a, var42.i);
                        }
                     }

                     li.ck(var2, var3, var4, var5);
                     continue;
                  }

                  if (var10.contentType == 1400) {
                     ip.rz.h(var39, var13, var10.width, var10.height, Client.bz);
                  }

                  if (var10.contentType == 1401) {
                     ip.rz.ae(var39, var13, var10.width, var10.height);
                  }
               }

               boolean var46;
               if (var10.type == 0) {
                  if (!var10.modern) {
                     var46 = var10.hidden;
                     if (var46 && var10 != av.kp) {
                        continue;
                     }
                  }

                  if (!var10.modern) {
                     if (var10.verticalScrollbarPosition > var10.at - var10.height) {
                        var10.verticalScrollbarPosition = var10.at - var10.height;
                     }

                     if (var10.verticalScrollbarPosition < 0) {
                        var10.verticalScrollbarPosition = 0;
                     }
                  }

                  iq(var0, var10.id, var15, var16, var17, var18, var39 - var10.horizontalScrollbarPosition, var13 - var10.verticalScrollbarPosition, var11);
                  if (var10.cs2components != null) {
                     iq(var10.cs2components, var10.id, var15, var16, var17, var18, var39 - var10.horizontalScrollbarPosition, var13 - var10.verticalScrollbarPosition, var11);
                  }

                  SubWindow var19 = (SubWindow)Client.subWindowTable.t((long)var10.id);
                  if (var19 != null) {
                     kq.ix(var19.targetWindowId, var15, var16, var17, var18, var39, var13, var11);
                  }

                  li.ck(var2, var3, var4, var5);
                  eu.t();
               }

               if (Client.ny || Client.nj[var11] || Client.nu > 1) {
                  if (var10.type == 0 && !var10.modern && var10.at > var10.height) {
                     l.ih(var39 + var10.width, var13, var10.verticalScrollbarPosition, var10.height, var10.at);
                  }

                  if (var10.type != 1) {
                     if (var10.type == 2) {
                        var30 = 0;

                        for(var20 = 0; var20 < var10.aj; ++var20) {
                           for(var21 = 0; var21 < var10.av; ++var21) {
                              var22 = var39 + var21 * (var10.cw + 32);
                              var23 = var13 + var20 * (var10.ch + 32);
                              if (var30 < 20) {
                                 var22 += var10.cr[var30];
                                 var23 += var10.co[var30];
                              }

                              if (var10.itemIds[var30] <= 0) {
                                 if (var10.cv != null && var30 < 20) {
                                    Sprite var52 = var10.c(var30);
                                    if (var52 != null) {
                                       var52.u(var22, var23);
                                    } else if (RTComponent.j) {
                                       GameEngine.jk(var10);
                                    }
                                 }
                              } else {
                                 boolean var44 = false;
                                 boolean var45 = false;
                                 var26 = var10.itemIds[var30] - 1;
                                 if (var22 + 32 > var2 && var22 < var4 && var23 + 32 > var3 && var23 < var5 || var10 == ak.ik && var30 == Client.is) {
                                    Sprite var36;
                                    if (Client.inventoryItemSelectionState == 1 && var30 == jz.ks && var10.id == bg.ix) {
                                       var36 = m.createSprite(var26, var10.itemStackSizes[var30], 2, 0, 2, false);
                                    } else {
                                       var36 = m.createSprite(var26, var10.itemStackSizes[var30], 1, 3153952, 2, false);
                                    }

                                    if (var36 != null) {
                                       if (var10 == ak.ik && var30 == Client.is) {
                                          var24 = bs.n - Client.io;
                                          var25 = bs.x - Client.ig;
                                          if (var24 < 5 && var24 > -5) {
                                             var24 = 0;
                                          }

                                          if (var25 < 5 && var25 > -5) {
                                             var25 = 0;
                                          }

                                          if (Client.ih < 5) {
                                             var24 = 0;
                                             var25 = 0;
                                          }

                                          var36.f(var24 + var22, var23 + var25, 128);
                                          if (var1 != -1) {
                                             RTComponent var28 = var0[var1 & '\uffff'];
                                             int var29;
                                             if (var23 + var25 < li.ae && var28.verticalScrollbarPosition > 0) {
                                                var29 = (li.ae - var23 - var25) * Client.fs / 3;
                                                if (var29 > Client.fs * 10) {
                                                   var29 = Client.fs * 10;
                                                }

                                                if (var29 > var28.verticalScrollbarPosition) {
                                                   var29 = var28.verticalScrollbarPosition;
                                                }

                                                var28.verticalScrollbarPosition -= var29;
                                                Client.ig += var29;
                                                GameEngine.jk(var28);
                                             }

                                             if (var23 + var25 + 32 > li.am && var28.verticalScrollbarPosition < var28.at - var28.height) {
                                                var29 = (var23 + var25 + 32 - li.am) * Client.fs / 3;
                                                if (var29 > Client.fs * 10) {
                                                   var29 = Client.fs * 10;
                                                }

                                                if (var29 > var28.at - var28.height - var28.verticalScrollbarPosition) {
                                                   var29 = var28.at - var28.height - var28.verticalScrollbarPosition;
                                                }

                                                var28.verticalScrollbarPosition += var29;
                                                Client.ig -= var29;
                                                GameEngine.jk(var28);
                                             }
                                          }
                                       } else if (var10 == MouseTracker.ie && var30 == Client.iv) {
                                          var36.f(var22, var23, 128);
                                       } else {
                                          var36.u(var22, var23);
                                       }
                                    } else {
                                       GameEngine.jk(var10);
                                    }
                                 }
                              }

                              ++var30;
                           }
                        }
                     } else if (var10.type == 3) {
                        if (bc.id(var10)) {
                           var30 = var10.as;
                           if (var10 == av.kp && var10.aq != 0) {
                              var30 = var10.aq;
                           }
                        } else {
                           var30 = var10.color;
                           if (var10 == av.kp && var10.aw != 0) {
                              var30 = var10.aw;
                           }
                        }

                        if (var10.aa) {
                           switch(var10.af.b) {
                           case 1:
                              li.df(var39, var13, var10.width, var10.height, var10.color, var10.as, 256 - (var10.alpha & 255), 256 - (var10.ab & 255));
                              break;
                           case 2:
                              li.dq(var39, var13, var10.width, var10.height, var10.color, var10.as, 256 - (var10.alpha & 255), 256 - (var10.ab & 255));
                              break;
                           case 3:
                              li.dt(var39, var13, var10.width, var10.height, var10.color, var10.as, 256 - (var10.alpha & 255), 256 - (var10.ab & 255));
                              break;
                           case 4:
                              li.dy(var39, var13, var10.width, var10.height, var10.color, var10.as, 256 - (var10.alpha & 255), 256 - (var10.ab & 255));
                              break;
                           default:
                              if (var14 == 0) {
                                 li.dl(var39, var13, var10.width, var10.height, var30);
                              } else {
                                 li.dc(var39, var13, var10.width, var10.height, var30, 256 - (var14 & 255));
                              }
                           }
                        } else if (var14 == 0) {
                           li.dw(var39, var13, var10.width, var10.height, var30);
                        } else {
                           li.dd(var39, var13, var10.width, var10.height, var30, 256 - (var14 & 255));
                        }
                     } else {
                        km var40;
                        if (var10.type == 4) {
                           var40 = var10.o();
                           if (var40 == null) {
                              if (RTComponent.j) {
                                 GameEngine.jk(var10);
                              }
                           } else {
                              String var56 = var10.text;
                              if (bc.id(var10)) {
                                 var20 = var10.as;
                                 if (var10 == av.kp && var10.aq != 0) {
                                    var20 = var10.aq;
                                 }

                                 if (var10.cb.length() > 0) {
                                    var56 = var10.cb;
                                 }
                              } else {
                                 var20 = var10.color;
                                 if (var10 == av.kp && var10.aw != 0) {
                                    var20 = var10.aw;
                                 }
                              }

                              if (var10.modern && var10.itemId != -1) {
                                 ItemDefinition var57 = cs.loadItemDefinition(var10.itemId);
                                 var56 = var57.name;
                                 if (var56 == null) {
                                    var56 = "null";
                                 }

                                 if ((var57.ay == 1 || var10.itemQuantity != 1) && var10.itemQuantity != -1) {
                                    var56 = ar.q(16748608) + var56 + "</col>" + " " + 'x' + ax.if(var10.itemQuantity);
                                 }
                              }

                              if (var10 == Client.ls) {
                                 var56 = "Please wait...";
                                 var20 = var10.color;
                              }

                              if (!var10.modern) {
                                 var56 = Server.ik(var56, var10);
                              }

                              var40.y(var56, var39, var13, var10.width, var10.height, var20, var10.ct ? 0 : -1, var10.cu, var10.cs, var10.cm);
                           }
                        } else if (var10.type == 5) {
                           Sprite var41;
                           if (!var10.modern) {
                              var41 = var10.p(bc.id(var10));
                              if (var41 != null) {
                                 var41.u(var39, var13);
                              } else if (RTComponent.j) {
                                 GameEngine.jk(var10);
                              }
                           } else {
                              if (var10.itemId != -1) {
                                 var41 = m.createSprite(var10.itemId, var10.itemQuantity, var10.borderThickness, var10.shadowColor, var10.bw, false);
                              } else {
                                 var41 = var10.p(false);
                              }

                              if (var41 == null) {
                                 if (RTComponent.j) {
                                    GameEngine.jk(var10);
                                 }
                              } else {
                                 var20 = var41.b;
                                 var21 = var41.e;
                                 if (!var10.bk) {
                                    var22 = var10.width * 4096 / var20;
                                    if (var10.spriteRotation != 0) {
                                       var41.ar(var10.width / 2 + var39, var10.height / 2 + var13, var10.spriteRotation, var22);
                                    } else if (var14 != 0) {
                                       var41.y(var39, var13, var10.width, var10.height, 256 - (var14 & 255));
                                    } else if (var20 == var10.width && var21 == var10.height) {
                                       var41.u(var39, var13);
                                    } else {
                                       var41.z(var39, var13, var10.width, var10.height);
                                    }
                                 } else {
                                    li.db(var39, var13, var39 + var10.width, var13 + var10.height);
                                    var22 = (var20 - 1 + var10.width) / var20;
                                    var23 = (var21 - 1 + var10.height) / var21;

                                    for(var24 = 0; var24 < var22; ++var24) {
                                       for(var25 = 0; var25 < var23; ++var25) {
                                          if (var10.spriteRotation != 0) {
                                             var41.ar(var20 / 2 + var39 + var24 * var20, var21 / 2 + var13 + var25 * var21, var10.spriteRotation, 4096);
                                          } else if (var14 != 0) {
                                             var41.f(var39 + var24 * var20, var13 + var25 * var21, 256 - (var14 & 255));
                                          } else {
                                             var41.u(var39 + var20 * var24, var13 + var25 * var21);
                                          }
                                       }
                                    }

                                    li.ck(var2, var3, var4, var5);
                                 }
                              }
                           }
                        } else {
                           ItemDefinition var35;
                           if (var10.type == 6) {
                              var46 = bc.id(var10);
                              if (var46) {
                                 var20 = var10.bb;
                              } else {
                                 var20 = var10.animationId;
                              }

                              Model var51 = null;
                              var22 = 0;
                              if (var10.itemId != -1) {
                                 var35 = cs.loadItemDefinition(var10.itemId);
                                 if (var35 != null) {
                                    var35 = var35.o(var10.itemQuantity);
                                    var51 = var35.p(1);
                                    if (var51 != null) {
                                       var51.b();
                                       var22 = var51.modelHeight / 2;
                                    } else {
                                       GameEngine.jk(var10);
                                    }
                                 }
                              } else if (var10.entityType == 5) {
                                 if (var10.entityId == 0) {
                                    var51 = Client.qv.e((kf)null, -1, (kf)null, -1);
                                 } else {
                                    var51 = az.il.p();
                                 }
                              } else if (var20 == -1) {
                                 var51 = var10.u((kf)null, -1, var46, az.il.composite);
                                 if (var51 == null && RTComponent.j) {
                                    GameEngine.jk(var10);
                                 }
                              } else {
                                 kf var58 = ff.t(var20);
                                 var51 = var10.u(var58, var10.et, var46, az.il.composite);
                                 if (var51 == null && RTComponent.j) {
                                    GameEngine.jk(var10);
                                 }
                              }

                              eu.a(var10.width / 2 + var39, var10.height / 2 + var13);
                              var23 = eu.m[var10.bx] * var10.bv >> 16;
                              var24 = eu.ay[var10.bx] * var10.bv >> 16;
                              if (var51 != null) {
                                 if (!var10.modern) {
                                    var51.y(0, var10.bf, 0, var10.bx, 0, var23, var24);
                                 } else {
                                    var51.b();
                                    if (var10.bl) {
                                       var51.h(0, var10.bf, var10.bo, var10.bx, var10.bq, var22 + var23 + var10.bz, var24 + var10.bz, var10.bv);
                                    } else {
                                       var51.y(0, var10.bf, var10.bo, var10.bx, var10.bq, var22 + var23 + var10.bz, var24 + var10.bz);
                                    }
                                 }
                              }

                              eu.i();
                           } else {
                              if (var10.type == 7) {
                                 var40 = var10.o();
                                 if (var40 == null) {
                                    if (RTComponent.j) {
                                       GameEngine.jk(var10);
                                    }
                                    continue;
                                 }

                                 var20 = 0;

                                 for(var21 = 0; var21 < var10.aj; ++var21) {
                                    for(var22 = 0; var22 < var10.av; ++var22) {
                                       if (var10.itemIds[var20] > 0) {
                                          var35 = cs.loadItemDefinition(var10.itemIds[var20] - 1);
                                          String var32;
                                          if (var35.ay != 1 && var10.itemStackSizes[var20] == 1) {
                                             var32 = ar.q(16748608) + var35.name + "</col>";
                                          } else {
                                             var32 = ar.q(16748608) + var35.name + "</col>" + " " + 'x' + ax.if(var10.itemStackSizes[var20]);
                                          }

                                          var25 = var22 * (var10.cw + 115) + var39;
                                          var26 = (var10.ch + 12) * var21 + var13;
                                          if (var10.cu == 0) {
                                             var40.s(var32, var25, var26, var10.color, var10.ct ? 0 : -1);
                                          } else if (var10.cu == 1) {
                                             var40.r(var32, var10.width / 2 + var25, var26, var10.color, var10.ct ? 0 : -1);
                                          } else {
                                             var40.f(var32, var25 + var10.width - 1, var26, var10.color, var10.ct ? 0 : -1);
                                          }
                                       }

                                       ++var20;
                                    }
                                 }
                              }

                              if (var10.type == 8 && var10 == kz && Client.kx == Client.ke) {
                                 var30 = 0;
                                 var20 = 0;
                                 km var33 = fv.et;
                                 String var34 = var10.text;

                                 String var55;
                                 for(var34 = Server.ik(var34, var10); var34.length() > 0; var20 = var20 + var33.e + 1) {
                                    var24 = var34.indexOf("<br>");
                                    if (var24 != -1) {
                                       var55 = var34.substring(0, var24);
                                       var34 = var34.substring(var24 + 4);
                                    } else {
                                       var55 = var34;
                                       var34 = "";
                                    }

                                    var25 = var33.c(var55);
                                    if (var25 > var30) {
                                       var30 = var25;
                                    }
                                 }

                                 var30 += 6;
                                 var20 += 7;
                                 var24 = var39 + var10.width - 5 - var30;
                                 var25 = var13 + var10.height + 5;
                                 if (var24 < var39 + 5) {
                                    var24 = var39 + 5;
                                 }

                                 if (var30 + var24 > var4) {
                                    var24 = var4 - var30;
                                 }

                                 if (var20 + var25 > var5) {
                                    var25 = var5 - var20;
                                 }

                                 li.dl(var24, var25, var30, var20, 16777120);
                                 li.dw(var24, var25, var30, var20, 0);
                                 var34 = var10.text;
                                 var26 = var25 + var33.e + 2;

                                 for(var34 = Server.ik(var34, var10); var34.length() > 0; var26 = var26 + var33.e + 1) {
                                    var27 = var34.indexOf("<br>");
                                    if (var27 != -1) {
                                       var55 = var34.substring(0, var27);
                                       var34 = var34.substring(var27 + 4);
                                    } else {
                                       var55 = var34;
                                       var34 = "";
                                    }

                                    var33.s(var55, var24 + 3, var26, 0, -1);
                                 }
                              }

                              if (var10.type == 9) {
                                 if (var10.ad) {
                                    var30 = var39;
                                    var20 = var13 + var10.height;
                                    var21 = var39 + var10.width;
                                    var22 = var13;
                                 } else {
                                    var30 = var39;
                                    var20 = var13;
                                    var21 = var39 + var10.width;
                                    var22 = var13 + var10.height;
                                 }

                                 if (var10.ac == 1) {
                                    li.dx(var30, var20, var21, var22, var10.color);
                                 } else {
                                    FileSystem.iv(var30, var20, var21, var22, var10.color, var10.ac);
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   }
}
