import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fv")
public abstract class fv {

    @ObfuscatedName("o")
    static ix o;

    @ObfuscatedName("ez")
    static km ez;

    @ObfuscatedName("et")
    static km et;

    @ObfuscatedName("t")
    public int t;

    @ObfuscatedName("q")
    public int q;

    @ObfuscatedName("i")
    public int i;

    @ObfuscatedName("a")
    public int a;

    @ObfuscatedName("t")
    protected abstract boolean t(int var1, int var2, int var3, CollisionMap var4);

    @ObfuscatedName("t")
    static int t(byte[] var0, int var1, int var2) {
        int var3 = -1;

        for (int var4 = var1; var4 < var2; ++var4) {
            var3 = var3 >>> 8 ^ ByteBuffer.i[(var3 ^ var0[var4]) & 255];
        }

        var3 = ~var3;
        return var3;
    }
}
