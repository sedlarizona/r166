import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ee")
public class ee {

    @ObfuscatedName("cn")
    static ju cn;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    ee() {
    }

    ee(ee var1) {
        this.t = var1.t;
        this.q = var1.q;
        this.i = var1.i;
        this.a = var1.a;
    }

    @ObfuscatedName("i")
    static ap i(int var0, int var1) {
        he var2 = au.x;
        long var3 = (long) (0 | var0 << 8 | var1);
        return (ap) var2.t(var3);
    }

    @ObfuscatedName("k")
    static final int k(int var0, int var1, int var2) {
        if (var2 > 179) {
            var1 /= 2;
        }

        if (var2 > 192) {
            var1 /= 2;
        }

        if (var2 > 217) {
            var1 /= 2;
        }

        if (var2 > 243) {
            var1 /= 2;
        }

        int var3 = (var1 / 32 << 7) + (var0 / 4 << 10) + var2 / 2;
        return var3;
    }
}
