import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jf")
public class jf extends hh {

    @ObfuscatedName("q")
    public static FileSystem q;

    @ObfuscatedName("i")
    public static int i;

    @ObfuscatedName("a")
    static Cache a = new Cache(64);

    @ObfuscatedName("l")
    public int l = -1;

    @ObfuscatedName("b")
    int[] b;

    @ObfuscatedName("e")
    short[] e;

    @ObfuscatedName("x")
    short[] x;

    @ObfuscatedName("p")
    short[] p;

    @ObfuscatedName("g")
    short[] g;

    @ObfuscatedName("n")
    int[] n = new int[] { -1, -1, -1, -1, -1 };

    @ObfuscatedName("o")
    public boolean o = false;

    @ObfuscatedName("q")
    void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.l = var1.av();
        } else {
            int var3;
            int var4;
            if (var2 == 2) {
                var3 = var1.av();
                this.b = new int[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.b[var4] = var1.ae();
                }
            } else if (var2 == 3) {
                this.o = true;
            } else if (var2 == 40) {
                var3 = var1.av();
                this.e = new short[var3];
                this.x = new short[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.e[var4] = (short) var1.ae();
                    this.x[var4] = (short) var1.ae();
                }
            } else if (var2 == 41) {
                var3 = var1.av();
                this.p = new short[var3];
                this.g = new short[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.p[var4] = (short) var1.ae();
                    this.g[var4] = (short) var1.ae();
                }
            } else if (var2 >= 60 && var2 < 70) {
                this.n[var2 - 60] = var1.ae();
            }
        }

    }

    @ObfuscatedName("a")
    public boolean a() {
        if (this.b == null) {
            return true;
        } else {
            boolean var1 = true;

            for (int var2 = 0; var2 < this.b.length; ++var2) {
                if (!q.l(this.b[var2], 0)) {
                    var1 = false;
                }
            }

            return var1;
        }
    }

    @ObfuscatedName("l")
    public AlternativeModel l() {
        if (this.b == null) {
            return null;
        } else {
            AlternativeModel[] var1 = new AlternativeModel[this.b.length];

            for (int var2 = 0; var2 < this.b.length; ++var2) {
                var1[var2] = AlternativeModel.t(q, this.b[var2], 0);
            }

            AlternativeModel var4;
            if (var1.length == 1) {
                var4 = var1[0];
            } else {
                var4 = new AlternativeModel(var1, var1.length);
            }

            int var3;
            if (this.e != null) {
                for (var3 = 0; var3 < this.e.length; ++var3) {
                    var4.z(this.e[var3], this.x[var3]);
                }
            }

            if (this.p != null) {
                for (var3 = 0; var3 < this.p.length; ++var3) {
                    var4.w(this.p[var3], this.g[var3]);
                }
            }

            return var4;
        }
    }

    @ObfuscatedName("b")
    public boolean b() {
        boolean var1 = true;

        for (int var2 = 0; var2 < 5; ++var2) {
            if (this.n[var2] != -1 && !q.l(this.n[var2], 0)) {
                var1 = false;
            }
        }

        return var1;
    }

    @ObfuscatedName("e")
    public AlternativeModel e() {
        AlternativeModel[] var1 = new AlternativeModel[5];
        int var2 = 0;

        for (int var3 = 0; var3 < 5; ++var3) {
            if (this.n[var3] != -1) {
                var1[var2++] = AlternativeModel.t(q, this.n[var3], 0);
            }
        }

        AlternativeModel var5 = new AlternativeModel(var1, var2);
        int var4;
        if (this.e != null) {
            for (var4 = 0; var4 < this.e.length; ++var4) {
                var5.z(this.e[var4], this.x[var4]);
            }
        }

        if (this.p != null) {
            for (var4 = 0; var4 < this.p.length; ++var4) {
                var5.w(this.p[var4], this.g[var4]);
            }
        }

        return var5;
    }
}
