import java.util.zip.CRC32;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jg")
public class jg {

    @ObfuscatedName("q")
    public static int q = 0;

    @ObfuscatedName("a")
    public static HashTable a = new HashTable(4096);

    @ObfuscatedName("l")
    public static int l = 0;

    @ObfuscatedName("b")
    public static HashTable b = new HashTable(32);

    @ObfuscatedName("e")
    public static int e = 0;

    @ObfuscatedName("x")
    public static Queue x = new Queue();

    @ObfuscatedName("p")
    static HashTable p = new HashTable(4096);

    @ObfuscatedName("g")
    public static int g = 0;

    @ObfuscatedName("n")
    public static HashTable n = new HashTable(4096);

    @ObfuscatedName("o")
    public static int o = 0;

    @ObfuscatedName("c")
    public static boolean c;

    @ObfuscatedName("u")
    public static ByteBuffer u = new ByteBuffer(8);

    @ObfuscatedName("k")
    public static int k = 0;

    @ObfuscatedName("w")
    public static CRC32 w = new CRC32();

    @ObfuscatedName("d")
    public static ju[] d = new ju[256];

    @ObfuscatedName("f")
    public static byte f = 0;

    @ObfuscatedName("r")
    public static int r = 0;

    @ObfuscatedName("y")
    public static int y = 0;

    @ObfuscatedName("bj")
    static Sprite[] bj;
}
