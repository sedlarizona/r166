import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("db")
public class db extends Node {

    @ObfuscatedName("e")
    static byte[] e;

    @ObfuscatedName("x")
    static int x;

    @ObfuscatedName("p")
    static int p;

    @ObfuscatedName("g")
    static int g;

    @ObfuscatedName("n")
    static int n;

    @ObfuscatedName("o")
    static cp[] o;

    @ObfuscatedName("c")
    static da[] c;

    @ObfuscatedName("v")
    static dq[] v;

    @ObfuscatedName("u")
    static dw[] u;

    @ObfuscatedName("j")
    static boolean[] j;

    @ObfuscatedName("k")
    static int[] k;

    @ObfuscatedName("z")
    static boolean z = false;

    @ObfuscatedName("r")
    static float[] r;

    @ObfuscatedName("y")
    static float[] y;

    @ObfuscatedName("h")
    static float[] h;

    @ObfuscatedName("m")
    static float[] m;

    @ObfuscatedName("ay")
    static float[] ay;

    @ObfuscatedName("ao")
    static float[] ao;

    @ObfuscatedName("av")
    static float[] av;

    @ObfuscatedName("aj")
    static int[] aj;

    @ObfuscatedName("ae")
    static int[] ae;

    @ObfuscatedName("t")
    byte[][] t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    boolean b;

    @ObfuscatedName("w")
    float[] w;

    @ObfuscatedName("s")
    int s;

    @ObfuscatedName("d")
    int d;

    @ObfuscatedName("f")
    boolean f;

    @ObfuscatedName("am")
    byte[] am;

    @ObfuscatedName("az")
    int az;

    @ObfuscatedName("ap")
    int ap;

    db(byte[] var1) {
        this.l(var1);
    }

    @ObfuscatedName("l")
    void l(byte[] var1) {
        ByteBuffer var2 = new ByteBuffer(var1);
        this.q = var2.ap();
        this.i = var2.ap();
        this.a = var2.ap();
        this.l = var2.ap();
        if (this.l < 0) {
            this.l = ~this.l;
            this.b = true;
        }

        int var3 = var2.ap();
        this.t = new byte[var3][];

        for (int var4 = 0; var4 < var3; ++var4) {
            int var5 = 0;

            int var6;
            do {
                var6 = var2.av();
                var5 += var6;
            } while (var6 >= 255);

            byte[] var7 = new byte[var5];
            var2.al(var7, 0, var5);
            this.t[var4] = var7;
        }

    }

    @ObfuscatedName("e")
    float[] e(int var1) {
        q(this.t[var1], 0);
        i();
        int var2 = a(id.a(k.length - 1));
        boolean var3 = j[var2];
        int var4 = var3 ? n : g;
        boolean var5 = false;
        boolean var6 = false;
        if (var3) {
            var5 = i() != 0;
            var6 = i() != 0;
        }

        int var7 = var4 >> 1;
        int var8;
        int var9;
        int var10;
        if (var3 && !var5) {
            var8 = (var4 >> 2) - (g >> 2);
            var9 = (g >> 2) + (var4 >> 2);
            var10 = g >> 1;
        } else {
            var8 = 0;
            var9 = var7;
            var10 = var4 >> 1;
        }

        int var11;
        int var12;
        int var13;
        if (var3 && !var6) {
            var11 = var4 - (var4 >> 2) - (g >> 2);
            var12 = (g >> 2) + (var4 - (var4 >> 2));
            var13 = g >> 1;
        } else {
            var11 = var7;
            var12 = var4;
            var13 = var4 >> 1;
        }

        dw var14 = u[k[var2]];
        int var16 = var14.q;
        int var17 = var14.i[var16];
        boolean var15 = !c[var17].b();
        boolean var45 = var15;

        for (var17 = 0; var17 < var14.t; ++var17) {
            dq var18 = v[var14.a[var17]];
            float[] var19 = r;
            var18.t(var19, var4 >> 1, var45);
        }

        int var40;
        if (!var15) {
            var17 = var14.q;
            var40 = var14.i[var17];
            c[var40].e(r, var4 >> 1);
        }

        int var42;
        if (var15) {
            for (var17 = var4 >> 1; var17 < var4; ++var17) {
                r[var17] = 0.0F;
            }
        } else {
            var17 = var4 >> 1;
            var40 = var4 >> 2;
            var42 = var4 >> 3;
            float[] var43 = r;

            int var21;
            for (var21 = 0; var21 < var17; ++var21) {
                var43[var21] *= 0.5F;
            }

            for (var21 = var17; var21 < var4; ++var21) {
                var43[var21] = -var43[var4 - var21 - 1];
            }

            float[] var44 = var3 ? ay : y;
            float[] var22 = var3 ? ao : h;
            float[] var23 = var3 ? av : m;
            int[] var24 = var3 ? ae : aj;

            int var25;
            float var26;
            float var27;
            float var28;
            float var29;
            for (var25 = 0; var25 < var40; ++var25) {
                var26 = var43[var25 * 4] - var43[var4 - var25 * 4 - 1];
                var27 = var43[var25 * 4 + 2] - var43[var4 - var25 * 4 - 3];
                var28 = var44[var25 * 2];
                var29 = var44[var25 * 2 + 1];
                var43[var4 - var25 * 4 - 1] = var26 * var28 - var27 * var29;
                var43[var4 - var25 * 4 - 3] = var26 * var29 + var27 * var28;
            }

            float var30;
            float var31;
            for (var25 = 0; var25 < var42; ++var25) {
                var26 = var43[var17 + var25 * 4 + 3];
                var27 = var43[var17 + var25 * 4 + 1];
                var28 = var43[var25 * 4 + 3];
                var29 = var43[var25 * 4 + 1];
                var43[var17 + var25 * 4 + 3] = var26 + var28;
                var43[var17 + var25 * 4 + 1] = var27 + var29;
                var30 = var44[var17 - 4 - var25 * 4];
                var31 = var44[var17 - 3 - var25 * 4];
                var43[var25 * 4 + 3] = (var26 - var28) * var30 - (var27 - var29) * var31;
                var43[var25 * 4 + 1] = (var27 - var29) * var30 + (var26 - var28) * var31;
            }

            var25 = id.a(var4 - 1);

            int var47;
            int var48;
            int var49;
            int var50;
            for (var47 = 0; var47 < var25 - 3; ++var47) {
                var48 = var4 >> var47 + 2;
                var49 = 8 << var47;

                for (var50 = 0; var50 < 2 << var47; ++var50) {
                    int var51 = var4 - var48 * var50 * 2;
                    int var52 = var4 - var48 * (var50 * 2 + 1);

                    for (int var32 = 0; var32 < var4 >> var47 + 4; ++var32) {
                        int var33 = var32 * 4;
                        float var34 = var43[var51 - 1 - var33];
                        float var35 = var43[var51 - 3 - var33];
                        float var36 = var43[var52 - 1 - var33];
                        float var37 = var43[var52 - 3 - var33];
                        var43[var51 - 1 - var33] = var34 + var36;
                        var43[var51 - 3 - var33] = var35 + var37;
                        float var38 = var44[var32 * var49];
                        float var39 = var44[var32 * var49 + 1];
                        var43[var52 - 1 - var33] = (var34 - var36) * var38 - (var35 - var37) * var39;
                        var43[var52 - 3 - var33] = (var35 - var37) * var38 + (var34 - var36) * var39;
                    }
                }
            }

            for (var47 = 1; var47 < var42 - 1; ++var47) {
                var48 = var24[var47];
                if (var47 < var48) {
                    var49 = var47 * 8;
                    var50 = var48 * 8;
                    var30 = var43[var49 + 1];
                    var43[var49 + 1] = var43[var50 + 1];
                    var43[var50 + 1] = var30;
                    var30 = var43[var49 + 3];
                    var43[var49 + 3] = var43[var50 + 3];
                    var43[var50 + 3] = var30;
                    var30 = var43[var49 + 5];
                    var43[var49 + 5] = var43[var50 + 5];
                    var43[var50 + 5] = var30;
                    var30 = var43[var49 + 7];
                    var43[var49 + 7] = var43[var50 + 7];
                    var43[var50 + 7] = var30;
                }
            }

            for (var47 = 0; var47 < var17; ++var47) {
                var43[var47] = var43[var47 * 2 + 1];
            }

            for (var47 = 0; var47 < var42; ++var47) {
                var43[var4 - 1 - var47 * 2] = var43[var47 * 4];
                var43[var4 - 2 - var47 * 2] = var43[var47 * 4 + 1];
                var43[var4 - var40 - 1 - var47 * 2] = var43[var47 * 4 + 2];
                var43[var4 - var40 - 2 - var47 * 2] = var43[var47 * 4 + 3];
            }

            for (var47 = 0; var47 < var42; ++var47) {
                var27 = var23[var47 * 2];
                var28 = var23[var47 * 2 + 1];
                var29 = var43[var17 + var47 * 2];
                var30 = var43[var17 + var47 * 2 + 1];
                var31 = var43[var4 - 2 - var47 * 2];
                float var53 = var43[var4 - 1 - var47 * 2];
                float var54 = var28 * (var29 - var31) + var27 * (var30 + var53);
                var43[var17 + var47 * 2] = (var29 + var31 + var54) * 0.5F;
                var43[var4 - 2 - var47 * 2] = (var29 + var31 - var54) * 0.5F;
                var54 = var28 * (var30 + var53) - var27 * (var29 - var31);
                var43[var17 + var47 * 2 + 1] = (var30 - var53 + var54) * 0.5F;
                var43[var4 - 1 - var47 * 2] = (-var30 + var53 + var54) * 0.5F;
            }

            for (var47 = 0; var47 < var40; ++var47) {
                var43[var47] = var43[var17 + var47 * 2] * var22[var47 * 2] + var43[var17 + var47 * 2 + 1]
                        * var22[var47 * 2 + 1];
                var43[var17 - 1 - var47] = var43[var17 + var47 * 2] * var22[var47 * 2 + 1]
                        - var43[var17 + var47 * 2 + 1] * var22[var47 * 2];
            }

            for (var47 = 0; var47 < var40; ++var47) {
                var43[var47 + (var4 - var40)] = -var43[var47];
            }

            for (var47 = 0; var47 < var40; ++var47) {
                var43[var47] = var43[var40 + var47];
            }

            for (var47 = 0; var47 < var40; ++var47) {
                var43[var40 + var47] = -var43[var40 - var47 - 1];
            }

            for (var47 = 0; var47 < var40; ++var47) {
                var43[var17 + var47] = var43[var4 - var47 - 1];
            }

            for (var47 = var8; var47 < var9; ++var47) {
                var27 = (float) Math.sin(((double) (var47 - var8) + 0.5D) / (double) var10 * 0.5D * 3.141592653589793D);
                r[var47] *= (float) Math.sin(1.5707963267948966D * (double) var27 * (double) var27);
            }

            for (var47 = var11; var47 < var12; ++var47) {
                var27 = (float) Math.sin(((double) (var47 - var11) + 0.5D) / (double) var13 * 0.5D * 3.141592653589793D
                        + 1.5707963267948966D);
                r[var47] *= (float) Math.sin(1.5707963267948966D * (double) var27 * (double) var27);
            }
        }

        float[] var41 = null;
        if (this.s > 0) {
            var40 = var4 + this.s >> 2;
            var41 = new float[var40];
            int var20;
            if (!this.f) {
                for (var42 = 0; var42 < this.d; ++var42) {
                    var20 = var42 + (this.s >> 1);
                    var41[var42] += this.w[var20];
                }
            }

            if (!var15) {
                for (var42 = var8; var42 < var4 >> 1; ++var42) {
                    var20 = var41.length - (var4 >> 1) + var42;
                    var41[var20] += r[var42];
                }
            }
        }

        float[] var46 = this.w;
        this.w = r;
        r = var46;
        this.s = var4;
        this.d = var12 - (var4 >> 1);
        this.f = var15;
        return var41;
    }

    @ObfuscatedName("o")
    DataRequestNode o(int[] var1) {
        if (var1 != null && var1[0] <= 0) {
            return null;
        } else {
            if (this.am == null) {
                this.s = 0;
                this.w = new float[n];
                this.am = new byte[this.i];
                this.az = 0;
                this.ap = 0;
            }

            for (; this.ap < this.t.length; ++this.ap) {
                if (var1 != null && var1[0] <= 0) {
                    return null;
                }

                float[] var2 = this.e(this.ap);
                if (var2 != null) {
                    int var3 = this.az;
                    int var4 = var2.length;
                    if (var4 > this.i - var3) {
                        var4 = this.i - var3;
                    }

                    for (int var5 = 0; var5 < var4; ++var5) {
                        int var6 = (int) (128.0F + var2[var5] * 128.0F);
                        if ((var6 & -256) != 0) {
                            var6 = ~var6 >> 31;
                        }

                        this.am[var3++] = (byte) (var6 - 128);
                    }

                    if (var1 != null) {
                        var1[0] -= var3 - this.az;
                    }

                    this.az = var3;
                }
            }

            this.w = null;
            byte[] var7 = this.am;
            this.am = null;
            return new DataRequestNode(this.q, var7, this.a, this.l, this.b);
        }
    }

    @ObfuscatedName("t")
    static float t(int var0) {
        int var1 = var0 & 2097151;
        int var2 = var0 & Integer.MIN_VALUE;
        int var3 = (var0 & 2145386496) >> 21;
        if (var2 != 0) {
            var1 = -var1;
        }

        return (float) ((double) var1 * Math.pow(2.0D, (double) (var3 - 788)));
    }

    @ObfuscatedName("q")
    static void q(byte[] var0, int var1) {
        e = var0;
        x = var1;
        p = 0;
    }

    @ObfuscatedName("i")
    static int i() {
        int var0 = e[x] >> p & 1;
        ++p;
        x += p >> 3;
        p &= 7;
        return var0;
    }

    @ObfuscatedName("a")
    static int a(int var0) {
        int var1 = 0;

        int var2;
        int var3;
        for (var2 = 0; var0 >= 8 - p; var0 -= var3) {
            var3 = 8 - p;
            int var4 = (1 << var3) - 1;
            var1 += (e[x] >> p & var4) << var2;
            p = 0;
            ++x;
            var2 += var3;
        }

        if (var0 > 0) {
            var3 = (1 << var0) - 1;
            var1 += (e[x] >> p & var3) << var2;
            p += var0;
        }

        return var1;
    }

    @ObfuscatedName("b")
    static void b(byte[] var0) {
        q(var0, 0);
        g = 1 << a(4);
        n = 1 << a(4);
        r = new float[n];

        int var1;
        int var2;
        int var3;
        int var4;
        int var5;
        for (var1 = 0; var1 < 2; ++var1) {
            var2 = var1 != 0 ? n : g;
            var3 = var2 >> 1;
            var4 = var2 >> 2;
            var5 = var2 >> 3;
            float[] var6 = new float[var3];

            for (int var7 = 0; var7 < var4; ++var7) {
                var6[var7 * 2] = (float) Math.cos((double) (var7 * 4) * 3.141592653589793D / (double) var2);
                var6[var7 * 2 + 1] = -((float) Math.sin((double) (var7 * 4) * 3.141592653589793D / (double) var2));
            }

            float[] var18 = new float[var3];

            for (int var8 = 0; var8 < var4; ++var8) {
                var18[var8 * 2] = (float) Math.cos((double) (var8 * 2 + 1) * 3.141592653589793D / (double) (var2 * 2));
                var18[var8 * 2 + 1] = (float) Math.sin((double) (var8 * 2 + 1) * 3.141592653589793D
                        / (double) (var2 * 2));
            }

            float[] var19 = new float[var4];

            for (int var9 = 0; var9 < var5; ++var9) {
                var19[var9 * 2] = (float) Math.cos((double) (var9 * 4 + 2) * 3.141592653589793D / (double) var2);
                var19[var9 * 2 + 1] = -((float) Math.sin((double) (var9 * 4 + 2) * 3.141592653589793D / (double) var2));
            }

            int[] var20 = new int[var5];
            int var10 = id.a(var5 - 1);

            for (int var11 = 0; var11 < var5; ++var11) {
                int var15 = var11;
                int var16 = var10;

                int var17;
                for (var17 = 0; var16 > 0; --var16) {
                    var17 = var17 << 1 | var15 & 1;
                    var15 >>>= 1;
                }

                var20[var11] = var17;
            }

            if (var1 != 0) {
                ay = var6;
                ao = var18;
                av = var19;
                ae = var20;
            } else {
                y = var6;
                h = var18;
                m = var19;
                aj = var20;
            }
        }

        var1 = a(8) + 1;
        o = new cp[var1];

        for (var2 = 0; var2 < var1; ++var2) {
            o[var2] = new cp();
        }

        var2 = a(6) + 1;

        for (var3 = 0; var3 < var2; ++var3) {
            a(16);
        }

        var2 = a(6) + 1;
        c = new da[var2];

        for (var3 = 0; var3 < var2; ++var3) {
            c[var3] = new da();
        }

        var3 = a(6) + 1;
        v = new dq[var3];

        for (var4 = 0; var4 < var3; ++var4) {
            v[var4] = new dq();
        }

        var4 = a(6) + 1;
        u = new dw[var4];

        for (var5 = 0; var5 < var4; ++var5) {
            u[var5] = new dw();
        }

        var5 = a(6) + 1;
        j = new boolean[var5];
        k = new int[var5];

        for (int var21 = 0; var21 < var5; ++var21) {
            j[var21] = i() != 0;
            a(16);
            a(16);
            k[var21] = a(8);
        }

    }

    @ObfuscatedName("x")
    static boolean x(FileSystem var0) {
        if (!z) {
            byte[] var1 = var0.i(0, 0);
            if (var1 == null) {
                return false;
            }

            b(var1);
            z = true;
        }

        return true;
    }

    @ObfuscatedName("p")
    static db p(FileSystem var0, int var1, int var2) {
        if (!x(var0)) {
            var0.l(var1, var2);
            return null;
        } else {
            byte[] var3 = var0.i(var1, var2);
            return var3 == null ? null : new db(var3);
        }
    }
}
