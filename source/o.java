import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("o")
public class o {

    @ObfuscatedName("qt")
    static lp qt;

    @ObfuscatedName("pw")
    static int pw;

    @ObfuscatedName("t")
    public static final o t = new o();

    @ObfuscatedName("q")
    public static final o q = new o();

    @ObfuscatedName("ai")
    protected static String ai;

    @ObfuscatedName("t")
    public static final int t(double var0, double var2, double var4) {
        double var6 = var4;
        double var8 = var4;
        double var10 = var4;
        if (0.0D != var2) {
            double var12;
            if (var4 < 0.5D) {
                var12 = (var2 + 1.0D) * var4;
            } else {
                var12 = var4 + var2 - var4 * var2;
            }

            double var14 = 2.0D * var4 - var12;
            double var16 = 0.3333333333333333D + var0;
            if (var16 > 1.0D) {
                --var16;
            }

            double var20 = var0 - 0.3333333333333333D;
            if (var20 < 0.0D) {
                ++var20;
            }

            if (var16 * 6.0D < 1.0D) {
                var6 = var14 + (var12 - var14) * 6.0D * var16;
            } else if (var16 * 2.0D < 1.0D) {
                var6 = var12;
            } else if (var16 * 3.0D < 2.0D) {
                var6 = 6.0D * (0.6666666666666666D - var16) * (var12 - var14) + var14;
            } else {
                var6 = var14;
            }

            if (var0 * 6.0D < 1.0D) {
                var8 = var0 * (var12 - var14) * 6.0D + var14;
            } else if (var0 * 2.0D < 1.0D) {
                var8 = var12;
            } else if (var0 * 3.0D < 2.0D) {
                var8 = var14 + (var12 - var14) * (0.6666666666666666D - var0) * 6.0D;
            } else {
                var8 = var14;
            }

            if (var20 * 6.0D < 1.0D) {
                var10 = var14 + var20 * 6.0D * (var12 - var14);
            } else if (var20 * 2.0D < 1.0D) {
                var10 = var12;
            } else if (3.0D * var20 < 2.0D) {
                var10 = (0.6666666666666666D - var20) * (var12 - var14) * 6.0D + var14;
            } else {
                var10 = var14;
            }
        }

        int var22 = (int) (256.0D * var6);
        int var13 = (int) (256.0D * var8);
        int var23 = (int) (256.0D * var10);
        int var15 = var23 + (var13 << 8) + (var22 << 16);
        return var15;
    }

    @ObfuscatedName("t")
    static long t() {
        try {
            URL var0 = new URL(ki.jv("services", false) + "m=accountappeal/login.ws");
            URLConnection var1 = var0.openConnection();
            var1.setRequestProperty("connection", "close");
            var1.setDoInput(true);
            var1.setDoOutput(true);
            var1.setConnectTimeout(5000);
            OutputStreamWriter var2 = new OutputStreamWriter(var1.getOutputStream());
            var2.write("data1=req");
            var2.flush();
            InputStream var3 = var1.getInputStream();
            ByteBuffer var4 = new ByteBuffer(new byte[1000]);

            do {
                int var5 = var3.read(var4.buffer, var4.index, 1000 - var4.index);
                if (var5 == -1) {
                    var4.index = 0;
                    long var7 = var4.ah();
                    return var7;
                }

                var4.index += var5;
            } while (var4.index < 1000);

            return 0L;
        } catch (Exception var9) {
            return 0L;
        }
    }

    @ObfuscatedName("t")
    static int t(int var0, int var1) {
        ItemStorage var2 = (ItemStorage) ItemStorage.t.t((long) var0);
        if (var2 == null) {
            return -1;
        } else {
            return var1 >= 0 && var1 < var2.ids.length ? var2.ids[var1] : -1;
        }
    }

    @ObfuscatedName("i")
    static int i(int var0, int var1) {
        ItemStorage var2 = (ItemStorage) ItemStorage.t.t((long) var0);
        if (var2 == null) {
            return 0;
        } else if (var1 == -1) {
            return 0;
        } else {
            int var3 = 0;

            for (int var4 = 0; var4 < var2.stackSizes.length; ++var4) {
                if (var2.ids[var4] == var1) {
                    var3 += var2.stackSizes[var4];
                }
            }

            return var3;
        }
    }

    @ObfuscatedName("fa")
    static final void fa(Character var0) {
        int var1 = var0.bd - Client.bz;
        int var2 = var0.bu * 128 + var0.ap * 64;
        int var3 = var0.bw * 128 + var0.ap * 64;
        var0.regionX += (var2 - var0.regionX) / var1;
        var0.regionY += (var3 - var0.regionY) / var1;
        var0.cq = 0;
        var0.ct = var0.cm;
    }

    @ObfuscatedName("kk")
    static void kk() {
        Client.ee.i(ap.t(fo.bg, Client.ee.l));
        Client.gn = 0;
    }
}
