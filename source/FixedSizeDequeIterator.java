import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hg")
public class FixedSizeDequeIterator implements Iterator {

    @ObfuscatedName("t")
    FixedSizeDeque deque;

    @ObfuscatedName("q")
    Node q;

    @ObfuscatedName("i")
    int size;

    @ObfuscatedName("a")
    Node a = null;

    FixedSizeDequeIterator(FixedSizeDeque var1) {
        this.deque = var1;
        this.b();
    }

    @ObfuscatedName("b")
    void b() {
        this.q = this.deque.buckets[0].next;
        this.size = 1;
        this.a = null;
    }

    public Object next() {
        Node var1;
        if (this.deque.buckets[this.size - 1] != this.q) {
            var1 = this.q;
            this.q = var1.next;
            this.a = var1;
            return var1;
        } else {
            do {
                if (this.size >= this.deque.t) {
                    return null;
                }

                var1 = this.deque.buckets[this.size++].next;
            } while (var1 == this.deque.buckets[this.size - 1]);

            this.q = var1.next;
            this.a = var1;
            return var1;
        }
    }

    public boolean hasNext() {
        if (this.deque.buckets[this.size - 1] != this.q) {
            return true;
        } else {
            while (this.size < this.deque.t) {
                if (this.deque.buckets[this.size++].next != this.deque.buckets[this.size - 1]) {
                    this.q = this.deque.buckets[this.size - 1].next;
                    return true;
                }

                this.q = this.deque.buckets[this.size - 1];
            }

            return false;
        }
    }

    public void remove() {
        if (this.a == null) {
            throw new IllegalStateException();
        } else {
            this.a.kc();
            this.a = null;
        }
    }
}
