import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jh")
public class jh extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static FileSystem q;

    @ObfuscatedName("i")
    public static FileSystem i;

    @ObfuscatedName("a")
    static Cache a = new Cache(64);

    @ObfuscatedName("l")
    static Cache l = new Cache(64);

    @ObfuscatedName("b")
    static Cache b = new Cache(20);

    @ObfuscatedName("o")
    int o = -1;

    @ObfuscatedName("c")
    public int c = 16777215;

    @ObfuscatedName("v")
    public int v = 70;

    @ObfuscatedName("u")
    int u = -1;

    @ObfuscatedName("j")
    int j = -1;

    @ObfuscatedName("k")
    int k = -1;

    @ObfuscatedName("z")
    int z = -1;

    @ObfuscatedName("w")
    public int w = 0;

    @ObfuscatedName("s")
    public int s = 0;

    @ObfuscatedName("d")
    public int d = -1;

    @ObfuscatedName("f")
    String f = "";

    @ObfuscatedName("r")
    public int r = -1;

    @ObfuscatedName("y")
    public int y = 0;

    @ObfuscatedName("h")
    public int[] h;

    @ObfuscatedName("m")
    int m = -1;

    @ObfuscatedName("ay")
    int ay = -1;

    @ObfuscatedName("q")
    void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.o = var1.aw();
        } else if (var2 == 2) {
            this.c = var1.az();
        } else if (var2 == 3) {
            this.u = var1.aw();
        } else if (var2 == 4) {
            this.k = var1.aw();
        } else if (var2 == 5) {
            this.j = var1.aw();
        } else if (var2 == 6) {
            this.z = var1.aw();
        } else if (var2 == 7) {
            this.w = var1.am();
        } else if (var2 == 8) {
            this.f = var1.an();
        } else if (var2 == 9) {
            this.v = var1.ae();
        } else if (var2 == 10) {
            this.s = var1.am();
        } else if (var2 == 11) {
            this.d = 0;
        } else if (var2 == 12) {
            this.r = var1.av();
        } else if (var2 == 13) {
            this.y = var1.am();
        } else if (var2 == 14) {
            this.d = var1.ae();
        } else if (var2 == 17 || var2 == 18) {
            this.m = var1.ae();
            if (this.m == 65535) {
                this.m = -1;
            }

            this.ay = var1.ae();
            if (this.ay == 65535) {
                this.ay = -1;
            }

            int var3 = -1;
            if (var2 == 18) {
                var3 = var1.ae();
                if (var3 == 65535) {
                    var3 = -1;
                }
            }

            int var4 = var1.av();
            this.h = new int[var4 + 2];

            for (int var5 = 0; var5 <= var4; ++var5) {
                this.h[var5] = var1.ae();
                if (this.h[var5] == 65535) {
                    this.h[var5] = -1;
                }
            }

            this.h[var4 + 1] = var3;
        }

    }

    @ObfuscatedName("a")
    public final jh a() {
        int var1 = -1;
        if (this.m != -1) {
            var1 = Server.t(this.m);
        } else if (this.ay != -1) {
            var1 = iv.varps[this.ay];
        }

        int var2;
        if (var1 >= 0 && var1 < this.h.length - 1) {
            var2 = this.h[var1];
        } else {
            var2 = this.h[this.h.length - 1];
        }

        return var2 != -1 ? bl.t(var2) : null;
    }

    @ObfuscatedName("l")
    public String l(int var1) {
        String var2 = this.f;

        while (true) {
            int var3 = var2.indexOf("%1");
            if (var3 < 0) {
                return var2;
            }

            var2 = var2.substring(0, var3) + fc.b(var1, false) + var2.substring(var3 + 2);
        }
    }

    @ObfuscatedName("b")
    public Sprite b() {
        if (this.u < 0) {
            return null;
        } else {
            Sprite var1 = (Sprite) l.t((long) this.u);
            if (var1 != null) {
                return var1;
            } else {
                var1 = q.t(q, this.u, 0);
                if (var1 != null) {
                    l.i(var1, (long) this.u);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("e")
    public Sprite e() {
        if (this.j < 0) {
            return null;
        } else {
            Sprite var1 = (Sprite) l.t((long) this.j);
            if (var1 != null) {
                return var1;
            } else {
                var1 = q.t(q, this.j, 0);
                if (var1 != null) {
                    l.i(var1, (long) this.j);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("x")
    public Sprite x() {
        if (this.k < 0) {
            return null;
        } else {
            Sprite var1 = (Sprite) l.t((long) this.k);
            if (var1 != null) {
                return var1;
            } else {
                var1 = q.t(q, this.k, 0);
                if (var1 != null) {
                    l.i(var1, (long) this.k);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("p")
    public Sprite p() {
        if (this.z < 0) {
            return null;
        } else {
            Sprite var1 = (Sprite) l.t((long) this.z);
            if (var1 != null) {
                return var1;
            } else {
                var1 = q.t(q, this.z, 0);
                if (var1 != null) {
                    l.i(var1, (long) this.z);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("o")
    public km o() {
        if (this.o == -1) {
            return null;
        } else {
            km var1 = (km) b.t((long) this.o);
            if (var1 != null) {
                return var1;
            } else {
                var1 = TaskData.q(q, i, this.o, 0);
                if (var1 != null) {
                    b.i(var1, (long) this.o);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("t")
    public static void t(FileSystem var0, FileSystem var1) {
        NpcDefinition.t = var0;
        NpcDefinition.q = var1;
    }
}
