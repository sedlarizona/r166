import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("iu")
public class DevelopmentStage {

    @ObfuscatedName("t")
    public static final DevelopmentStage t = new DevelopmentStage("LIVE", 0);

    @ObfuscatedName("q")
    public static final DevelopmentStage q = new DevelopmentStage("BUILDLIVE", 3);

    @ObfuscatedName("i")
    public static final DevelopmentStage i = new DevelopmentStage("RC", 1);

    @ObfuscatedName("a")
    public static final DevelopmentStage a = new DevelopmentStage("WIP", 2);

    @ObfuscatedName("fe")
    static int[][] fe;

    @ObfuscatedName("l")
    public final String label;

    @ObfuscatedName("b")
    public final int id;

    DevelopmentStage(String var1, int var2) {
        this.label = var1;
        this.id = var2;
    }

    @ObfuscatedName("t")
    public static js t(int var0) {
        js var1 = (js) js.q.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = js.t.i(1, var0);
            var1 = new js();
            if (var2 != null) {
                var1.i(new ByteBuffer(var2), var0);
            }

            var1.q();
            js.q.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("je")
    static void je(byte[] var0, int var1) {
        if (Client.de == null) {
            Client.de = new byte[24];
        }

        gj.t(var0, var1, Client.de, 0, 24);
    }
}
