import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ct")
public final class AnimableObject extends Renderable {

    @ObfuscatedName("ff")
    static int[] ff;

    @ObfuscatedName("t")
    int id;

    @ObfuscatedName("q")
    int startCycle;

    @ObfuscatedName("i")
    int plane;

    @ObfuscatedName("a")
    int regionX;

    @ObfuscatedName("l")
    int regionY;

    @ObfuscatedName("b")
    int height;

    @ObfuscatedName("e")
    kf animationSequence;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    int p = 0;

    @ObfuscatedName("g")
    boolean finished = false;

    AnimableObject(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        this.id = var1;
        this.plane = var2;
        this.regionX = var3;
        this.regionY = var4;
        this.height = var5;
        this.startCycle = var7 + var6;
        int var8 = ah.t(this.id).e;
        if (var8 != -1) {
            this.finished = false;
            this.animationSequence = ff.t(var8);
        } else {
            this.finished = true;
        }

    }

    @ObfuscatedName("t")
    final void t(int var1) {
        if (!this.finished) {
            this.p += var1;

            while (this.p > this.animationSequence.x[this.x]) {
                this.p -= this.animationSequence.x[this.x];
                ++this.x;
                if (this.x >= this.animationSequence.b.length) {
                    this.finished = true;
                    break;
                }
            }

        }
    }

    @ObfuscatedName("p")
    protected final Model p() {
        jw var1 = ah.t(this.id);
        Model var2;
        if (!this.finished) {
            var2 = var1.a(this.x);
        } else {
            var2 = var1.a(-1);
        }

        return var2 == null ? null : var2;
    }
}
