import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bu")
public class bu implements Comparator {

    @ObfuscatedName("t")
    boolean t;

    @ObfuscatedName("t")
    int t(u var1, u var2) {
        if (var2.t == var1.t) {
            return 0;
        } else {
            if (this.t) {
                if (Client.currentWorld == var1.t) {
                    return -1;
                }

                if (var2.t == Client.currentWorld) {
                    return 1;
                }
            }

            return var1.t < var2.t ? -1 : 1;
        }
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }

    public int compare(Object var1, Object var2) {
        return this.t((u) var1, (u) var2);
    }
}
