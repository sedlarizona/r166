import java.math.BigInteger;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cd")
public class cd {

    @ObfuscatedName("t")
    static final BigInteger t = new BigInteger("10001", 16);

    @ObfuscatedName("q")
    static final BigInteger q = new BigInteger(
            "8f4a98129626cc10be0802729ed056aa437f35f8e795f3ca1d97127d702b4c2f4a87ea418d036a6ad4279ca8c503f2f012bffde94b75335e2367510a82d059f1aabd30ab7035a750a0d4397d8bc08e74e209dfd0bf09d39fa05e09ec32503a782d76b04b89efddaa9d0b3baae61aeecf312be318f9124d0b8861e6d39eafb253",
            16);

    @ObfuscatedName("t")
    public static jp t(int var0) {
        jp var1 = (jp) jp.q.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = jp.t.i(11, var0);
            var1 = new jp();
            if (var2 != null) {
                var1.i(new ByteBuffer(var2));
            }

            var1.q();
            jp.q.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("c")
    static int c(int var0, RuneScript var1, boolean var2) {
        RTComponent var3 = Inflater.t(cs.e[--b.menuX]);
        if (var0 == 2500) {
            cs.e[++b.menuX - 1] = var3.relativeX;
            return 1;
        } else if (var0 == 2501) {
            cs.e[++b.menuX - 1] = var3.relativeY;
            return 1;
        } else if (var0 == 2502) {
            cs.e[++b.menuX - 1] = var3.width;
            return 1;
        } else if (var0 == 2503) {
            cs.e[++b.menuX - 1] = var3.height;
            return 1;
        } else if (var0 == 2504) {
            cs.e[++b.menuX - 1] = var3.hidden ? 1 : 0;
            return 1;
        } else if (var0 == 2505) {
            cs.e[++b.menuX - 1] = var3.parentId;
            return 1;
        } else {
            return 2;
        }
    }

    @ObfuscatedName("fg")
   static void fg() {
      int var0;
      if (Client.cd == 0) {
         an.loadedRegion = new Region(4, 104, 104, bt.tileHeights);

         for(var0 = 0; var0 < 4; ++var0) {
            Client.collisionMaps[var0] = new CollisionMap(104, 104);
         }

         GrandExchangeOffer.on = new Sprite(512, 512);
         ci.ax = "Starting game engine...";
         ci.au = 5;
         Client.cd = 20;
      } else {
         int var1;
         int var2;
         int var3;
         int var4;
         if (Client.cd == 20) {
            int[] var35 = new int[9];

            for(var1 = 0; var1 < 9; ++var1) {
               var2 = var1 * 32 + 15 + 128;
               var3 = var2 * 3 + 600;
               var4 = eu.m[var2];
               var35[var1] = var4 * var3 >> 16;
            }

            Region.ag(var35, 500, 800, 512, 334);
            ci.ax = "Prepared visibility map";
            ci.au = 10;
            Client.cd = 30;
         } else if (Client.cd == 30) {
            Server.cy = ah.fp(0, false, true, true);
            MenuItemNode.cg = ah.fp(1, false, true, true);
            Client.cj = ah.fp(2, true, false, true);
            ly.cl = ah.fp(3, false, true, true);
            aj.cz = ah.fp(4, false, true, true);
            ee.cn = ah.fp(5, true, true, true);
            Varpbit.ca = ah.fp(6, true, true, true);
            iq.cf = ah.fp(7, false, true, true);
            au.cp = ah.fp(8, false, true, true);
            bt.ck = ah.fp(9, false, true, true);
            jk.db = ah.fp(10, false, true, true);
            bk.dp = ah.fp(11, false, true, true);
            p.da = ah.fp(12, false, true, true);
            Client.dr = ah.fp(13, true, false, true);
            kq.dj = ah.fp(14, false, true, true);
            bn.dh = ah.fp(15, false, true, true);
            gf.dc = ah.fp(16, true, true, true);
            ci.ax = "Connecting to update server";
            ci.au = 20;
            Client.cd = 40;
         } else if (Client.cd == 40) {
            byte var33 = 0;
            var0 = var33 + Server.cy.dc() * 4 / 100;
            var0 += MenuItemNode.cg.dc() * 4 / 100;
            var0 += Client.cj.dc() * 2 / 100;
            var0 += ly.cl.dc() * 2 / 100;
            var0 += aj.cz.dc() * 6 / 100;
            var0 += ee.cn.dc() * 4 / 100;
            var0 += Varpbit.ca.dc() * 2 / 100;
            var0 += iq.cf.dc() * 58 / 100;
            var0 += au.cp.dc() * 2 / 100;
            var0 += bt.ck.dc() * 2 / 100;
            var0 += jk.db.dc() * 2 / 100;
            var0 += bk.dp.dc() * 2 / 100;
            var0 += p.da.dc() * 2 / 100;
            var0 += Client.dr.dc() * 2 / 100;
            var0 += kq.dj.dc() * 2 / 100;
            var0 += bn.dh.dc() * 2 / 100;
            var0 += gf.dc.dc() * 2 / 100;
            if (var0 != 100) {
               if (var0 != 0) {
                  ci.ax = "Checking for updates - " + var0 + "%";
               }

               ci.au = 30;
            } else {
               ScriptEvent.ex(Server.cy, "Animations");
               ScriptEvent.ex(MenuItemNode.cg, "Skeletons");
               ScriptEvent.ex(aj.cz, "Sound FX");
               ScriptEvent.ex(ee.cn, "Maps");
               ScriptEvent.ex(Varpbit.ca, "Music Tracks");
               ScriptEvent.ex(iq.cf, "Models");
               ScriptEvent.ex(au.cp, "Sprites");
               ScriptEvent.ex(bk.dp, "Music Jingles");
               ScriptEvent.ex(kq.dj, "Music Samples");
               ScriptEvent.ex(bn.dh, "Music Patches");
               ScriptEvent.ex(gf.dc, "World Map");
               ci.ax = "Loaded update list";
               ci.au = 30;
               Client.cd = 45;
            }
         } else if (Client.cd == 45) {
            boolean var32 = !Client.bh;
            TaskData.l = 22050;
            TaskData.b = var32;
            gh.p = 2;
            AudioTask var37 = new AudioTask();
            var37.z(9, 128);
            c.pi = al.am(GameEngine.t, 0, 22050);
            c.pi.az(var37);
            ItemPile.t(bn.dh, kq.dj, aj.cz, var37);
            gc.pk = al.am(GameEngine.t, 1, 2048);
            kq.pd = new ca();
            gc.pk.az(kq.pd);
            AppletParameter.pp = new dt(22050, TaskData.l);
            ci.ax = "Prepared sound engine";
            ci.au = 35;
            Client.cd = 50;
            Client.eg = new kn(au.cp, Client.dr);
         } else if (Client.cd == 50) {
            var0 = kq.t().length;
            Client.es = Client.eg.t(kq.t());
            if (Client.es.size() < var0) {
               ci.ax = "Loading fonts - " + Client.es.size() * 100 / var0 + "%";
               ci.au = 40;
            } else {
               fv.ez = (km)Client.es.get(kq.t);
               fv.et = (km)Client.es.get(kq.q);
               b.ev = (km)Client.es.get(kq.i);
               o.qt = new lp(true);
               ci.ax = "Loaded fonts";
               ci.au = 40;
               Client.cd = 60;
            }
         } else if (Client.cd == 60) {
            var0 = ItemDefinition.t(jk.db, au.cp);
            byte var31 = 11;
            if (var0 < var31) {
               ci.ax = "Loading title screen - " + var0 * 100 / var31 + "%";
               ci.au = 50;
            } else {
               ci.ax = "Loaded title screen";
               ci.au = 50;
               d.ea(5);
               Client.cd = 70;
            }
         } else {
            ju var28;
            if (Client.cd == 70) {
               if (!Client.cj.x()) {
                  ci.ax = "Loading config - " + Client.cj.do() + "%";
                  ci.au = 60;
               } else {
                  ip.t(Client.cj);
                  ju var34 = Client.cj;
                  js.t = var34;
                  ju var36 = Client.cj;
                  var28 = iq.cf;
                  io.t = var36;
                  jf.q = var28;
                  jf.i = io.t.w(3);
                  TaskData.t(Client.cj, iq.cf, Client.bh);
                  jh.t(Client.cj, iq.cf);
                  lw.t(Client.cj);
                  ju var29 = Client.cj;
                  ju var30 = iq.cf;
                  boolean var5 = Client.membersWorld;
                  km var6 = fv.ez;
                  ItemDefinition.b = var29;
                  ItemDefinition.e = var30;
                  z.x = var5;
                  GrandExchangeOffer.p = ItemDefinition.b.w(10);
                  dt.c = var6;
                  ju var7 = Client.cj;
                  ju var8 = Server.cy;
                  ju var9 = MenuItemNode.cg;
                  i.t = var7;
                  kf.q = var8;
                  kf.i = var9;
                  ju var10 = Client.cj;
                  ju var11 = iq.cf;
                  jw.t = var10;
                  jw.q = var11;
                  ju var12 = Client.cj;
                  Varpbit.t = var12;
                  ap.t((FileSystem)Client.cj);
                  ju var13 = ly.cl;
                  ju var14 = iq.cf;
                  ju var15 = au.cp;
                  ju var16 = Client.dr;
                  RTComponent.x = var13;
                  kv.p = var14;
                  RuneScriptVM.g = var15;
                  bg.n = var16;
                  RTComponent.b = new RTComponent[RTComponent.x.s()][];
                  c.loadedInterfaces = new boolean[RTComponent.x.s()];
                  gn.t(Client.cj);
                  bl.t(Client.cj);
                  ju var17 = Client.cj;
                  jq.t = var17;
                  af.t(Client.cj);
                  ju var18 = Client.cj;
                  jp.t = var18;
                  g.vm = new RuneScriptVM();
                  ju var19 = Client.cj;
                  ju var20 = au.cp;
                  ju var21 = Client.dr;
                  jh.t = var19;
                  jh.q = var20;
                  jh.i = var21;
                  iz.t(Client.cj, au.cp);
                  ju var22 = Client.cj;
                  ju var23 = au.cp;
                  jj.t = var23;
                  if (var22.x()) {
                     jj.i = var22.w(35);
                     jj.q = new jj[jj.i];

                     for(int var24 = 0; var24 < jj.i; ++var24) {
                        byte[] var25 = var22.i(35, var24);
                        if (var25 != null) {
                           jj.q[var24] = new jj(var24);
                           jj.q[var24].t(new ByteBuffer(var25));
                           jj.q[var24].i();
                        }
                     }
                  }

                  ci.ax = "Loaded config";
                  ci.au = 60;
                  Client.cd = 80;
               }
            } else if (Client.cd == 80) {
               var0 = 0;
               Sprite var27;
               if (y.fy == null) {
                  var28 = au.cp;
                  var3 = var28.h("compass");
                  var4 = var28.av(var3, "");
                  var27 = q.t(var28, var3, var4);
                  y.fy = var27;
               } else {
                  ++var0;
               }

               if (aq.fw == null) {
                  var28 = au.cp;
                  var3 = var28.h("mapedge");
                  var4 = var28.av(var3, "");
                  var27 = q.t(var28, var3, var4);
                  aq.fw = var27;
               } else {
                  ++var0;
               }

               if (ls.fm == null) {
                  ls.fm = az.i(au.cp, "mapscene", "");
               } else {
                  ++var0;
               }

               if (bn.fr == null) {
                  bn.fr = CombatBarDefinition.l(au.cp, "headicons_pk", "");
               } else {
                  ++var0;
               }

               if (n.fi == null) {
                  n.fi = CombatBarDefinition.l(au.cp, "headicons_prayer", "");
               } else {
                  ++var0;
               }

               if (ie.fn == null) {
                  ie.fn = CombatBarDefinition.l(au.cp, "headicons_hint", "");
               } else {
                  ++var0;
               }

               if (bt.fj == null) {
                  bt.fj = CombatBarDefinition.l(au.cp, "mapmarker", "");
               } else {
                  ++var0;
               }

               if (f.fa == null) {
                  f.fa = CombatBarDefinition.l(au.cp, "cross", "");
               } else {
                  ++var0;
               }

               if (Tile.fc == null) {
                  Tile.fc = CombatBarDefinition.l(au.cp, "mapdots", "");
               } else {
                  ++var0;
               }

               if (an.fv == null) {
                  an.fv = az.i(au.cp, "scrollbar", "");
               } else {
                  ++var0;
               }

               if (ff.fq == null) {
                  ff.fq = az.i(au.cp, "mod_icons", "");
               } else {
                  ++var0;
               }

               if (var0 < 11) {
                  ci.ax = "Loading sprites - " + var0 * 100 / 12 + "%";
                  ci.au = 70;
               } else {
                  RSFont.g = ff.fq;
                  aq.fw.a();
                  var1 = (int)(Math.random() * 21.0D) - 10;
                  var2 = (int)(Math.random() * 21.0D) - 10;
                  var3 = (int)(Math.random() * 21.0D) - 10;
                  var4 = (int)(Math.random() * 41.0D) - 20;
                  ls.fm[0].q(var1 + var4, var2 + var4, var4 + var3);
                  ci.ax = "Loaded sprites";
                  ci.au = 70;
                  Client.cd = 90;
               }
            } else if (Client.cd == 90) {
               if (!bt.ck.x()) {
                  ci.ax = "Loading textures - " + "0%";
                  ci.au = 90;
               } else {
                  h.id = new dg(bt.ck, au.cp, 20, 0.8D, Client.bh ? 64 : 128);
                  eu.l(h.id);
                  eu.b(0.8D);
                  Client.cd = 100;
               }
            } else if (Client.cd == 100) {
               var0 = h.id.t();
               if (var0 < 100) {
                  ci.ax = "Loading textures - " + var0 + "%";
                  ci.au = 90;
               } else {
                  ci.ax = "Loaded textures";
                  ci.au = 90;
                  Client.cd = 110;
               }
            } else if (Client.cd == 110) {
               SubWindow.bf = new MouseTracker();
               GameEngine.t.a(SubWindow.bf, 10);
               ci.ax = "Loaded input handler";
               ci.au = 92;
               Client.cd = 120;
            } else if (Client.cd == 120) {
               if (!jk.db.am("huffman", "")) {
                  ci.ax = "Loading wordpack - " + 0 + "%";
                  ci.au = 94;
               } else {
                  go var26 = new go(jk.db.ae("huffman", ""));
                  lw.t = var26;
                  ci.ax = "Loaded wordpack";
                  ci.au = 94;
                  Client.cd = 130;
               }
            } else if (Client.cd == 130) {
               if (!ly.cl.x()) {
                  ci.ax = "Loading interfaces - " + ly.cl.do() * 4 / 5 + "%";
                  ci.au = 96;
               } else if (!p.da.x()) {
                  ci.ax = "Loading interfaces - " + (80 + p.da.do() / 6) + "%";
                  ci.au = 96;
               } else if (!Client.dr.x()) {
                  ci.ax = "Loading interfaces - " + (96 + Client.dr.do() / 50) + "%";
                  ci.au = 96;
               } else {
                  ci.ax = "Loaded interfaces";
                  ci.au = 98;
                  Client.cd = 140;
               }
            } else if (Client.cd == 140) {
               ci.au = 100;
               if (!gf.dc.az(AppletParameter.t.value)) {
                  ci.ax = "Loading world map - " + gf.dc.ah(AppletParameter.t.value) / 10 + "%";
               } else {
                  if (ip.rz == null) {
                     ip.rz = new lo();
                     ip.rz.t(gf.dc, b.ev, Client.es, ls.fm);
                  }

                  var0 = ip.rz.q();
                  if (var0 < 100) {
                     ci.ax = "Loading world map - " + (var0 * 9 / 10 + 10) + "%";
                  } else {
                     ci.ax = "Loaded world map";
                     Client.cd = 150;
                  }
               }
            } else if (Client.cd == 150) {
               d.ea(10);
            }
         }
      }
   }
}
