import java.io.IOException;
import java.io.OutputStream;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fj")
public class fj implements Runnable {

    @ObfuscatedName("t")
    Thread t;

    @ObfuscatedName("q")
    OutputStream q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    byte[] a;

    @ObfuscatedName("l")
    int l = 0;

    @ObfuscatedName("b")
    int b = 0;

    @ObfuscatedName("e")
    IOException e;

    @ObfuscatedName("x")
    boolean x;

    fj(OutputStream var1, int var2) {
        this.q = var1;
        this.i = var2 + 1;
        this.a = new byte[this.i];
        this.t = new Thread(this);
        this.t.setDaemon(true);
        this.t.start();
    }

    @ObfuscatedName("t")
    boolean t() {
        if (this.x) {
            try {
                this.q.close();
                if (this.e == null) {
                    this.e = new IOException("");
                }
            } catch (IOException var2) {
                if (this.e == null) {
                    this.e = new IOException(var2);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    @ObfuscatedName("q")
    void q(byte[] var1, int var2, int var3)
            throws IOException {
        if (var3 >= 0 && var2 >= 0 && var3 + var2 <= var1.length) {
            synchronized (this) {
                if (this.e != null) {
                    throw new IOException(this.e.toString());
                } else {
                    int var5;
                    if (this.l <= this.b) {
                        var5 = this.i - this.b + this.l - 1;
                    } else {
                        var5 = this.l - this.b - 1;
                    }

                    if (var5 < var3) {
                        throw new IOException("");
                    } else {
                        if (var3 + this.b <= this.i) {
                            System.arraycopy(var1, var2, this.a, this.b, var3);
                        } else {
                            int var6 = this.i - this.b;
                            System.arraycopy(var1, var2, this.a, this.b, var6);
                            System.arraycopy(var1, var6 + var2, this.a, 0, var3 - var6);
                        }

                        this.b = (var3 + this.b) % this.i;
                        this.notifyAll();
                    }
                }
            }
        } else {
            throw new IOException();
        }
    }

    @ObfuscatedName("i")
    void i() {
        synchronized (this) {
            this.x = true;
            this.notifyAll();
        }

        try {
            this.t.join();
        } catch (InterruptedException var3) {
            ;
        }

    }

    public void run() {
        do {
            int var1;
            synchronized (this) {
                while (true) {
                    if (this.e != null) {
                        return;
                    }

                    if (this.l <= this.b) {
                        var1 = this.b - this.l;
                    } else {
                        var1 = this.i - this.l + this.b;
                    }

                    if (var1 > 0) {
                        break;
                    }

                    try {
                        this.q.flush();
                    } catch (IOException var11) {
                        this.e = var11;
                        return;
                    }

                    if (this.t()) {
                        return;
                    }

                    try {
                        this.wait();
                    } catch (InterruptedException var12) {
                        ;
                    }
                }
            }

            try {
                if (var1 + this.l <= this.i) {
                    this.q.write(this.a, this.l, var1);
                } else {
                    int var7 = this.i - this.l;
                    this.q.write(this.a, this.l, var7);
                    this.q.write(this.a, 0, var1 - var7);
                }
            } catch (IOException var10) {
                IOException var2 = var10;
                synchronized (this) {
                    this.e = var2;
                    return;
                }
            }

            synchronized (this) {
                this.l = (var1 + this.l) % this.i;
            }
        } while (!this.t());

    }
}
