import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kb")
public class kb implements Comparable {

    @ObfuscatedName("t")
    String t;

    @ObfuscatedName("q")
    String q;

    public kb(String var1, lu var2) {
        this.t = var1;
        this.q = ap.t((CharSequence) var1, (lu) var2);
    }

    @ObfuscatedName("t")
    public String t() {
        return this.t;
    }

    @ObfuscatedName("q")
    public boolean q() {
        return this.q != null;
    }

    @ObfuscatedName("i")
    public int i(kb var1) {
        if (this.q == null) {
            return var1.q == null ? 0 : 1;
        } else {
            return var1.q == null ? -1 : this.q.compareTo(var1.q);
        }
    }

    public boolean equals(Object var1) {
        if (var1 instanceof kb) {
            kb var2 = (kb) var1;
            if (this.q == null) {
                return var2.q == null;
            } else if (var2.q == null) {
                return false;
            } else {
                return this.hashCode() != var2.hashCode() ? false : this.q.equals(var2.q);
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        return this.q == null ? 0 : this.q.hashCode();
    }

    public int compareTo(Object var1) {
        return this.i((kb) var1);
    }

    public String toString() {
        return this.t();
    }

    @ObfuscatedName("u")
    static String u(char var0, int var1) {
        char[] var2 = new char[var1];

        for (int var3 = 0; var3 < var1; ++var3) {
            var2[var3] = var0;
        }

        return new String(var2);
    }
}
