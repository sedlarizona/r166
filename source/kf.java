import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kf")
public class kf extends hh {

    @ObfuscatedName("q")
    public static FileSystem q;

    @ObfuscatedName("i")
    public static FileSystem i;

    @ObfuscatedName("a")
    static Cache a = new Cache(64);

    @ObfuscatedName("l")
    static Cache l = new Cache(100);

    @ObfuscatedName("b")
    public int[] b;

    @ObfuscatedName("e")
    int[] e;

    @ObfuscatedName("x")
    public int[] x;

    @ObfuscatedName("p")
    public int[] p;

    @ObfuscatedName("g")
    public int g = -1;

    @ObfuscatedName("n")
    int[] n;

    @ObfuscatedName("o")
    public boolean o = false;

    @ObfuscatedName("c")
    public int c = 5;

    @ObfuscatedName("v")
    public int v = -1;

    @ObfuscatedName("u")
    public int u = -1;

    @ObfuscatedName("j")
    public int j = 99;

    @ObfuscatedName("k")
    public int k = -1;

    @ObfuscatedName("z")
    public int z = -1;

    @ObfuscatedName("w")
    public int w = 2;

    @ObfuscatedName("q")
    void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        int var3;
        int var4;
        if (var2 == 1) {
            var3 = var1.ae();
            this.x = new int[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.x[var4] = var1.ae();
            }

            this.b = new int[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.b[var4] = var1.ae();
            }

            for (var4 = 0; var4 < var3; ++var4) {
                this.b[var4] += var1.ae() << 16;
            }
        } else if (var2 == 2) {
            this.g = var1.ae();
        } else if (var2 == 3) {
            var3 = var1.av();
            this.n = new int[var3 + 1];

            for (var4 = 0; var4 < var3; ++var4) {
                this.n[var4] = var1.av();
            }

            this.n[var3] = 9999999;
        } else if (var2 == 4) {
            this.o = true;
        } else if (var2 == 5) {
            this.c = var1.av();
        } else if (var2 == 6) {
            this.v = var1.ae();
        } else if (var2 == 7) {
            this.u = var1.ae();
        } else if (var2 == 8) {
            this.j = var1.av();
        } else if (var2 == 9) {
            this.k = var1.av();
        } else if (var2 == 10) {
            this.z = var1.av();
        } else if (var2 == 11) {
            this.w = var1.av();
        } else if (var2 == 12) {
            var3 = var1.av();
            this.e = new int[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.e[var4] = var1.ae();
            }

            for (var4 = 0; var4 < var3; ++var4) {
                this.e[var4] += var1.ae() << 16;
            }
        } else if (var2 == 13) {
            var3 = var1.av();
            this.p = new int[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.p[var4] = var1.az();
            }
        }

    }

    @ObfuscatedName("a")
    void a() {
        if (this.k == -1) {
            if (this.n != null) {
                this.k = 2;
            } else {
                this.k = 0;
            }
        }

        if (this.z == -1) {
            if (this.n != null) {
                this.z = 2;
            } else {
                this.z = 0;
            }
        }

    }

    @ObfuscatedName("l")
    public Model l(Model var1, int var2) {
        var2 = this.b[var2];
        IdentityKitNode var3 = gv.o(var2 >> 16);
        var2 &= 65535;
        if (var3 == null) {
            return var1.q(true);
        } else {
            Model var4 = var1.q(!var3.q(var2));
            var4.c(var3, var2);
            return var4;
        }
    }

    @ObfuscatedName("b")
    Model b(Model var1, int var2, int var3) {
        var2 = this.b[var2];
        IdentityKitNode var4 = gv.o(var2 >> 16);
        var2 &= 65535;
        if (var4 == null) {
            return var1.q(true);
        } else {
            Model var5 = var1.q(!var4.q(var2));
            var3 &= 3;
            if (var3 == 1) {
                var5.s();
            } else if (var3 == 2) {
                var5.w();
            } else if (var3 == 3) {
                var5.z();
            }

            var5.c(var4, var2);
            if (var3 == 1) {
                var5.z();
            } else if (var3 == 2) {
                var5.w();
            } else if (var3 == 3) {
                var5.s();
            }

            return var5;
        }
    }

    @ObfuscatedName("e")
    Model e(Model var1, int var2) {
        var2 = this.b[var2];
        IdentityKitNode var3 = gv.o(var2 >> 16);
        var2 &= 65535;
        if (var3 == null) {
            return var1.i(true);
        } else {
            Model var4 = var1.i(!var3.q(var2));
            var4.c(var3, var2);
            return var4;
        }
    }

    @ObfuscatedName("x")
    public Model x(Model var1, int var2, kf var3, int var4) {
        var2 = this.b[var2];
        IdentityKitNode var5 = gv.o(var2 >> 16);
        var2 &= 65535;
        if (var5 == null) {
            return var3.l(var1, var4);
        } else {
            var4 = var3.b[var4];
            IdentityKitNode var6 = gv.o(var4 >> 16);
            var4 &= 65535;
            Model var7;
            if (var6 == null) {
                var7 = var1.q(!var5.q(var2));
                var7.c(var5, var2);
                return var7;
            } else {
                var7 = var1.q(!var5.q(var2) & !var6.q(var4));
                var7.u(var5, var2, var6, var4, this.n);
                return var7;
            }
        }
    }

    @ObfuscatedName("p")
    public Model p(Model var1, int var2) {
        int var3 = this.b[var2];
        IdentityKitNode var4 = gv.o(var3 >> 16);
        var3 &= 65535;
        if (var4 == null) {
            return var1.q(true);
        } else {
            IdentityKitNode var5 = null;
            int var6 = 0;
            if (this.e != null && var2 < this.e.length) {
                var6 = this.e[var2];
                var5 = gv.o(var6 >> 16);
                var6 &= 65535;
            }

            Model var7;
            if (var5 != null && var6 != 65535) {
                var7 = var1.q(!var4.q(var3) & !var5.q(var6));
                var7.c(var4, var3);
                var7.c(var5, var6);
                return var7;
            } else {
                var7 = var1.q(!var4.q(var3));
                var7.c(var4, var3);
                return var7;
            }
        }
    }
}
