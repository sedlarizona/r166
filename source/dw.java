import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dw")
public class dw {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int[] i;

    @ObfuscatedName("a")
    int[] a;

    dw() {
        db.a(16);
        this.t = db.i() != 0 ? db.a(4) + 1 : 1;
        if (db.i() != 0) {
            db.a(8);
        }

        db.a(2);
        if (this.t > 1) {
            this.q = db.a(4);
        }

        this.i = new int[this.t];
        this.a = new int[this.t];

        for (int var1 = 0; var1 < this.t; ++var1) {
            db.a(8);
            this.i[var1] = db.a(8);
            this.a[var1] = db.a(8);
        }

    }
}
