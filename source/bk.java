import java.util.Date;
import javax.imageio.ImageIO;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bk")
public class bk {

    @ObfuscatedName("i")
    static int[] i;

    @ObfuscatedName("ae")
    static int[] ae;

    @ObfuscatedName("dp")
    static ju dp;

    static {
        ImageIO.setUseCache(false);
    }

    @ObfuscatedName("q")
    static int q(int var0, RuneScript var1, boolean var2) {
        if (var0 < 1000) {
            return gd.i(var0, var1, var2);
        } else if (var0 < 1100) {
            return SubWindow.a(var0, var1, var2);
        } else if (var0 < 1200) {
            return Inflater.l(var0, var1, var2);
        } else if (var0 < 1300) {
            return TaskData.b(var0, var1, var2);
        } else if (var0 < 1400) {
            return bg.e(var0, var1, var2);
        } else if (var0 < 1500) {
            return EventObject.x(var0, var1, var2);
        } else {
            byte var3;
            RTComponent var33;
            if (var0 < 1600) {
                var33 = var2 ? ho.v : cs.c;
                if (var0 == 1500) {
                    cs.e[++b.menuX - 1] = var33.relativeX;
                    var3 = 1;
                } else if (var0 == 1501) {
                    cs.e[++b.menuX - 1] = var33.relativeY;
                    var3 = 1;
                } else if (var0 == 1502) {
                    cs.e[++b.menuX - 1] = var33.width;
                    var3 = 1;
                } else if (var0 == 1503) {
                    cs.e[++b.menuX - 1] = var33.height;
                    var3 = 1;
                } else if (var0 == 1504) {
                    cs.e[++b.menuX - 1] = var33.hidden ? 1 : 0;
                    var3 = 1;
                } else if (var0 == 1505) {
                    cs.e[++b.menuX - 1] = var33.parentId;
                    var3 = 1;
                } else {
                    var3 = 2;
                }

                return var3;
            } else if (var0 < 1700) {
                RTComponent var50 = var2 ? ho.v : cs.c;
                if (var0 == 1600) {
                    cs.e[++b.menuX - 1] = var50.horizontalScrollbarPosition;
                    var3 = 1;
                } else if (var0 == 1601) {
                    cs.e[++b.menuX - 1] = var50.verticalScrollbarPosition;
                    var3 = 1;
                } else if (var0 == 1602) {
                    cs.p[++ly.g - 1] = var50.text;
                    var3 = 1;
                } else if (var0 == 1603) {
                    cs.e[++b.menuX - 1] = var50.al;
                    var3 = 1;
                } else if (var0 == 1604) {
                    cs.e[++b.menuX - 1] = var50.at;
                    var3 = 1;
                } else if (var0 == 1605) {
                    cs.e[++b.menuX - 1] = var50.bv;
                    var3 = 1;
                } else if (var0 == 1606) {
                    cs.e[++b.menuX - 1] = var50.bx;
                    var3 = 1;
                } else if (var0 == 1607) {
                    cs.e[++b.menuX - 1] = var50.bo;
                    var3 = 1;
                } else if (var0 == 1608) {
                    cs.e[++b.menuX - 1] = var50.bf;
                    var3 = 1;
                } else if (var0 == 1609) {
                    cs.e[++b.menuX - 1] = var50.alpha;
                    var3 = 1;
                } else if (var0 == 1610) {
                    cs.e[++b.menuX - 1] = var50.ab;
                    var3 = 1;
                } else if (var0 == 1611) {
                    cs.e[++b.menuX - 1] = var50.color;
                    var3 = 1;
                } else if (var0 == 1612) {
                    cs.e[++b.menuX - 1] = var50.as;
                    var3 = 1;
                } else if (var0 == 1613) {
                    cs.e[++b.menuX - 1] = var50.af.t();
                    var3 = 1;
                } else {
                    var3 = 2;
                }

                return var3;
            } else if (var0 < 1800) {
                var33 = var2 ? ho.v : cs.c;
                if (var0 == 1700) {
                    cs.e[++b.menuX - 1] = var33.itemId;
                    var3 = 1;
                } else if (var0 == 1701) {
                    if (var33.itemId != -1) {
                        cs.e[++b.menuX - 1] = var33.itemQuantity;
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    var3 = 1;
                } else if (var0 == 1702) {
                    cs.e[++b.menuX - 1] = var33.w;
                    var3 = 1;
                } else {
                    var3 = 2;
                }

                return var3;
            } else if (var0 < 1900) {
                return u.p(var0, var1, var2);
            } else if (var0 < 2000) {
                return gt.o(var0, var1, var2);
            } else if (var0 < 2100) {
                return SubWindow.a(var0, var1, var2);
            } else if (var0 < 2200) {
                return Inflater.l(var0, var1, var2);
            } else if (var0 < 2300) {
                return TaskData.b(var0, var1, var2);
            } else if (var0 < 2400) {
                return bg.e(var0, var1, var2);
            } else if (var0 < 2500) {
                return EventObject.x(var0, var1, var2);
            } else if (var0 < 2600) {
                return cd.c(var0, var1, var2);
            } else if (var0 < 2700) {
                return TaskHandler.u(var0, var1, var2);
            } else if (var0 < 2800) {
                return z.k(var0, var1, var2);
            } else if (var0 < 2900) {
                return au.z(var0, var1, var2);
            } else if (var0 < 3000) {
                return gt.o(var0, var1, var2);
            } else if (var0 < 3200) {
                return bs.w(var0, var1, var2);
            } else if (var0 < 3300) {
                if (var0 == 3200) {
                    b.menuX -= 3;
                    AnimableNode.fh(cs.e[b.menuX], cs.e[b.menuX + 1], cs.e[b.menuX + 2]);
                    var3 = 1;
                } else if (var0 == 3201) {
                    az.fs(cs.e[--b.menuX]);
                    var3 = 1;
                } else if (var0 == 3202) {
                    b.menuX -= 2;
                    ci.fy(cs.e[b.menuX], cs.e[b.menuX + 1]);
                    var3 = 1;
                } else {
                    var3 = 2;
                }

                return var3;
            } else if (var0 < 3400) {
                return RSRandomAccessFile.s(var0, var1, var2);
            } else {
                int var4;
                int var6;
                int var9;
                int var12;
                int var47;
                if (var0 < 3500) {
                    if (var0 == 3400) {
                        b.menuX -= 2;
                        var4 = cs.e[b.menuX];
                        var12 = cs.e[b.menuX + 1];
                        jv var43 = kh.q(var4);
                        if (var43.a != 's') {
                            ;
                        }

                        for (var47 = 0; var47 < var43.e; ++var47) {
                            if (var12 == var43.x[var47]) {
                                cs.p[++ly.g - 1] = var43.g[var47];
                                var43 = null;
                                break;
                            }
                        }

                        if (var43 != null) {
                            cs.p[++ly.g - 1] = var43.l;
                        }

                        var3 = 1;
                    } else if (var0 == 3408) {
                        b.menuX -= 4;
                        var4 = cs.e[b.menuX];
                        var12 = cs.e[b.menuX + 1];
                        var6 = cs.e[b.menuX + 2];
                        var47 = cs.e[b.menuX + 3];
                        jv var34 = kh.q(var6);
                        if (var4 == var34.i && var12 == var34.a) {
                            for (var9 = 0; var9 < var34.e; ++var9) {
                                if (var47 == var34.x[var9]) {
                                    if (var12 == 115) {
                                        cs.p[++ly.g - 1] = var34.g[var9];
                                    } else {
                                        cs.e[++b.menuX - 1] = var34.p[var9];
                                    }

                                    var34 = null;
                                    break;
                                }
                            }

                            if (var34 != null) {
                                if (var12 == 115) {
                                    cs.p[++ly.g - 1] = var34.l;
                                } else {
                                    cs.e[++b.menuX - 1] = var34.b;
                                }
                            }

                            var3 = 1;
                        } else {
                            if (var12 == 115) {
                                cs.p[++ly.g - 1] = "null";
                            } else {
                                cs.e[++b.menuX - 1] = 0;
                            }

                            var3 = 1;
                        }
                    } else if (var0 == 3411) {
                        var4 = cs.e[--b.menuX];
                        jv var37 = kh.q(var4);
                        cs.e[++b.menuX - 1] = var37.l();
                        var3 = 1;
                    } else {
                        var3 = 2;
                    }

                    return var3;
                } else if (var0 < 3700) {
                    return ClientPreferences.d(var0, var1, var2);
                } else if (var0 < 4000) {
                    return gh.f(var0, var1, var2);
                } else {
                    int var15;
                    if (var0 < 4100) {
                        if (var0 == 4000) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var12 + var4;
                            var3 = 1;
                        } else if (var0 == 4001) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 - var12;
                            var3 = 1;
                        } else if (var0 == 4002) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 * var12;
                            var3 = 1;
                        } else if (var0 == 4003) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 / var12;
                            var3 = 1;
                        } else if (var0 == 4004) {
                            var4 = cs.e[--b.menuX];
                            cs.e[++b.menuX - 1] = (int) (Math.random() * (double) var4);
                            var3 = 1;
                        } else if (var0 == 4005) {
                            var4 = cs.e[--b.menuX];
                            cs.e[++b.menuX - 1] = (int) (Math.random() * (double) (var4 + 1));
                            var3 = 1;
                        } else if (var0 == 4006) {
                            b.menuX -= 5;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            var6 = cs.e[b.menuX + 2];
                            var47 = cs.e[b.menuX + 3];
                            var15 = cs.e[b.menuX + 4];
                            cs.e[++b.menuX - 1] = var4 + (var15 - var6) * (var12 - var4) / (var47 - var6);
                            var3 = 1;
                        } else if (var0 == 4007) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 + var12 * var4 / 100;
                            var3 = 1;
                        } else if (var0 == 4008) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 | 1 << var12;
                            var3 = 1;
                        } else if (var0 == 4009) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 & -1 - (1 << var12);
                            var3 = 1;
                        } else if (var0 == 4010) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = (var4 & 1 << var12) != 0 ? 1 : 0;
                            var3 = 1;
                        } else if (var0 == 4011) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 % var12;
                            var3 = 1;
                        } else if (var0 == 4012) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            if (var4 == 0) {
                                cs.e[++b.menuX - 1] = 0;
                            } else {
                                cs.e[++b.menuX - 1] = (int) Math.pow((double) var4, (double) var12);
                            }

                            var3 = 1;
                        } else if (var0 == 4013) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            if (var4 == 0) {
                                cs.e[++b.menuX - 1] = 0;
                                var3 = 1;
                            } else {
                                switch (var12) {
                                case 0:
                                    cs.e[++b.menuX - 1] = Integer.MAX_VALUE;
                                    break;
                                case 1:
                                    cs.e[++b.menuX - 1] = var4;
                                    break;
                                case 2:
                                    cs.e[++b.menuX - 1] = (int) Math.sqrt((double) var4);
                                    break;
                                case 3:
                                    cs.e[++b.menuX - 1] = (int) Math.cbrt((double) var4);
                                    break;
                                case 4:
                                    cs.e[++b.menuX - 1] = (int) Math.sqrt(Math.sqrt((double) var4));
                                    break;
                                default:
                                    cs.e[++b.menuX - 1] = (int) Math.pow((double) var4, 1.0D / (double) var12);
                                }

                                var3 = 1;
                            }
                        } else if (var0 == 4014) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 & var12;
                            var3 = 1;
                        } else if (var0 == 4015) {
                            b.menuX -= 2;
                            var4 = cs.e[b.menuX];
                            var12 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = var4 | var12;
                            var3 = 1;
                        } else if (var0 == 4018) {
                            b.menuX -= 3;
                            long var19 = (long) cs.e[b.menuX];
                            long var21 = (long) cs.e[b.menuX + 1];
                            long var23 = (long) cs.e[b.menuX + 2];
                            cs.e[++b.menuX - 1] = (int) (var23 * var19 / var21);
                            var3 = 1;
                        } else {
                            var3 = 2;
                        }

                        return var3;
                    } else {
                        String var25;
                        String var38;
                        if (var0 < 4200) {
                            if (var0 == 4100) {
                                var38 = cs.p[--ly.g];
                                var12 = cs.e[--b.menuX];
                                cs.p[++ly.g - 1] = var38 + var12;
                                var3 = 1;
                            } else if (var0 == 4101) {
                                ly.g -= 2;
                                var38 = cs.p[ly.g];
                                var25 = cs.p[ly.g + 1];
                                cs.p[++ly.g - 1] = var38 + var25;
                                var3 = 1;
                            } else if (var0 == 4102) {
                                var38 = cs.p[--ly.g];
                                var12 = cs.e[--b.menuX];
                                cs.p[++ly.g - 1] = var38 + fc.b(var12, true);
                                var3 = 1;
                            } else if (var0 == 4103) {
                                var38 = cs.p[--ly.g];
                                cs.p[++ly.g - 1] = var38.toLowerCase();
                                var3 = 1;
                            } else if (var0 == 4104) {
                                var4 = cs.e[--b.menuX];
                                long var17 = ((long) var4 + 11745L) * 86400000L;
                                cs.u.setTime(new Date(var17));
                                var47 = cs.u.get(5);
                                var15 = cs.u.get(2);
                                var9 = cs.u.get(1);
                                cs.p[++ly.g - 1] = var47 + "-" + cs.j[var15] + "-" + var9;
                                var3 = 1;
                            } else if (var0 == 4105) {
                                ly.g -= 2;
                                var38 = cs.p[ly.g];
                                var25 = cs.p[ly.g + 1];
                                if (az.il.composite != null && az.il.composite.female) {
                                    cs.p[++ly.g - 1] = var25;
                                } else {
                                    cs.p[++ly.g - 1] = var38;
                                }

                                var3 = 1;
                            } else if (var0 == 4106) {
                                var4 = cs.e[--b.menuX];
                                cs.p[++ly.g - 1] = Integer.toString(var4);
                                var3 = 1;
                            } else if (var0 == 4107) {
                                ly.g -= 2;
                                int[] var49 = cs.e;
                                var12 = ++b.menuX - 1;
                                var47 = aj.t(cs.p[ly.g], cs.p[ly.g + 1], Client.bs);
                                byte var28;
                                if (var47 > 0) {
                                    var28 = 1;
                                } else if (var47 < 0) {
                                    var28 = -1;
                                } else {
                                    var28 = 0;
                                }

                                var49[var12] = var28;
                                var3 = 1;
                            } else {
                                km var8;
                                byte[] var31;
                                if (var0 == 4108) {
                                    var38 = cs.p[--ly.g];
                                    b.menuX -= 2;
                                    var12 = cs.e[b.menuX];
                                    var6 = cs.e[b.menuX + 1];
                                    var31 = Client.dr.i(var6, 0);
                                    var8 = new km(var31);
                                    cs.e[++b.menuX - 1] = var8.z(var38, var12);
                                    var3 = 1;
                                } else if (var0 == 4109) {
                                    var38 = cs.p[--ly.g];
                                    b.menuX -= 2;
                                    var12 = cs.e[b.menuX];
                                    var6 = cs.e[b.menuX + 1];
                                    var31 = Client.dr.i(var6, 0);
                                    var8 = new km(var31);
                                    cs.e[++b.menuX - 1] = var8.k(var38, var12);
                                    var3 = 1;
                                } else if (var0 == 4110) {
                                    ly.g -= 2;
                                    var38 = cs.p[ly.g];
                                    var25 = cs.p[ly.g + 1];
                                    if (cs.e[--b.menuX] == 1) {
                                        cs.p[++ly.g - 1] = var38;
                                    } else {
                                        cs.p[++ly.g - 1] = var25;
                                    }

                                    var3 = 1;
                                } else if (var0 == 4111) {
                                    var38 = cs.p[--ly.g];
                                    cs.p[++ly.g - 1] = RSFont.w(var38);
                                    var3 = 1;
                                } else if (var0 == 4112) {
                                    var38 = cs.p[--ly.g];
                                    var12 = cs.e[--b.menuX];
                                    cs.p[++ly.g - 1] = var38 + (char) var12;
                                    var3 = 1;
                                } else {
                                    int[] var32;
                                    char var46;
                                    boolean var48;
                                    if (var0 == 4113) {
                                        var4 = cs.e[--b.menuX];
                                        var32 = cs.e;
                                        var6 = ++b.menuX - 1;
                                        var46 = (char) var4;
                                        if (var46 >= ' ' && var46 <= '~') {
                                            var48 = true;
                                        } else if (var46 >= 160 && var46 <= 255) {
                                            var48 = true;
                                        } else if (var46 != 8364 && var46 != 338 && var46 != 8212 && var46 != 339
                                                && var46 != 376) {
                                            var48 = false;
                                        } else {
                                            var48 = true;
                                        }

                                        var32[var6] = var48 ? 1 : 0;
                                        var3 = 1;
                                    } else if (var0 == 4114) {
                                        var4 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = ji.o((char) var4) ? 1 : 0;
                                        var3 = 1;
                                    } else if (var0 == 4115) {
                                        var4 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = s.p((char) var4) ? 1 : 0;
                                        var3 = 1;
                                    } else if (var0 == 4116) {
                                        var4 = cs.e[--b.menuX];
                                        var32 = cs.e;
                                        var6 = ++b.menuX - 1;
                                        var46 = (char) var4;
                                        var48 = var46 >= '0' && var46 <= '9';
                                        var32[var6] = var48 ? 1 : 0;
                                        var3 = 1;
                                    } else if (var0 == 4117) {
                                        var38 = cs.p[--ly.g];
                                        if (var38 != null) {
                                            cs.e[++b.menuX - 1] = var38.length();
                                        } else {
                                            cs.e[++b.menuX - 1] = 0;
                                        }

                                        var3 = 1;
                                    } else if (var0 == 4118) {
                                        var38 = cs.p[--ly.g];
                                        b.menuX -= 2;
                                        var12 = cs.e[b.menuX];
                                        var6 = cs.e[b.menuX + 1];
                                        cs.p[++ly.g - 1] = var38.substring(var12, var6);
                                        var3 = 1;
                                    } else if (var0 == 4119) {
                                        var38 = cs.p[--ly.g];
                                        StringBuilder var35 = new StringBuilder(var38.length());
                                        boolean var36 = false;

                                        for (var47 = 0; var47 < var38.length(); ++var47) {
                                            var46 = var38.charAt(var47);
                                            if (var46 == '<') {
                                                var36 = true;
                                            } else if (var46 == '>') {
                                                var36 = false;
                                            } else if (!var36) {
                                                var35.append(var46);
                                            }
                                        }

                                        cs.p[++ly.g - 1] = var35.toString();
                                        var3 = 1;
                                    } else if (var0 == 4120) {
                                        var38 = cs.p[--ly.g];
                                        var12 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = var38.indexOf(var12);
                                        var3 = 1;
                                    } else if (var0 == 4121) {
                                        ly.g -= 2;
                                        var38 = cs.p[ly.g];
                                        var25 = cs.p[ly.g + 1];
                                        var6 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = var38.indexOf(var25, var6);
                                        var3 = 1;
                                    } else {
                                        var3 = 2;
                                    }
                                }
                            }

                            return var3;
                        } else if (var0 < 4300) {
                            if (var0 == 4200) {
                                var4 = cs.e[--b.menuX];
                                cs.p[++ly.g - 1] = cs.loadItemDefinition(var4).name;
                                var3 = 1;
                            } else {
                                ItemDefinition var42;
                                if (var0 == 4201) {
                                    b.menuX -= 2;
                                    var4 = cs.e[b.menuX];
                                    var12 = cs.e[b.menuX + 1];
                                    var42 = cs.loadItemDefinition(var4);
                                    if (var12 >= 1 && var12 <= 5 && var42.groundActions[var12 - 1] != null) {
                                        cs.p[++ly.g - 1] = var42.groundActions[var12 - 1];
                                    } else {
                                        cs.p[++ly.g - 1] = "";
                                    }

                                    var3 = 1;
                                } else if (var0 == 4202) {
                                    b.menuX -= 2;
                                    var4 = cs.e[b.menuX];
                                    var12 = cs.e[b.menuX + 1];
                                    var42 = cs.loadItemDefinition(var4);
                                    if (var12 >= 1 && var12 <= 5 && var42.inventoryActions[var12 - 1] != null) {
                                        cs.p[++ly.g - 1] = var42.inventoryActions[var12 - 1];
                                    } else {
                                        cs.p[++ly.g - 1] = "";
                                    }

                                    var3 = 1;
                                } else if (var0 == 4203) {
                                    var4 = cs.e[--b.menuX];
                                    cs.e[++b.menuX - 1] = cs.loadItemDefinition(var4).storeValue;
                                    var3 = 1;
                                } else if (var0 == 4204) {
                                    var4 = cs.e[--b.menuX];
                                    cs.e[++b.menuX - 1] = cs.loadItemDefinition(var4).ay == 1 ? 1 : 0;
                                    var3 = 1;
                                } else {
                                    ItemDefinition var27;
                                    if (var0 == 4205) {
                                        var4 = cs.e[--b.menuX];
                                        var27 = cs.loadItemDefinition(var4);
                                        if (var27.ak == -1 && var27.af >= 0) {
                                            cs.e[++b.menuX - 1] = var27.af;
                                        } else {
                                            cs.e[++b.menuX - 1] = var4;
                                        }

                                        var3 = 1;
                                    } else if (var0 == 4206) {
                                        var4 = cs.e[--b.menuX];
                                        var27 = cs.loadItemDefinition(var4);
                                        if (var27.ak >= 0 && var27.af >= 0) {
                                            cs.e[++b.menuX - 1] = var27.af;
                                        } else {
                                            cs.e[++b.menuX - 1] = var4;
                                        }

                                        var3 = 1;
                                    } else if (var0 == 4207) {
                                        var4 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = cs.loadItemDefinition(var4).membersOnly ? 1 : 0;
                                        var3 = 1;
                                    } else if (var0 == 4208) {
                                        var4 = cs.e[--b.menuX];
                                        var27 = cs.loadItemDefinition(var4);
                                        if (var27.bs == -1 && var27.bh >= 0) {
                                            cs.e[++b.menuX - 1] = var27.bh;
                                        } else {
                                            cs.e[++b.menuX - 1] = var4;
                                        }

                                        var3 = 1;
                                    } else if (var0 == 4209) {
                                        var4 = cs.e[--b.menuX];
                                        var27 = cs.loadItemDefinition(var4);
                                        if (var27.bs >= 0 && var27.bh >= 0) {
                                            cs.e[++b.menuX - 1] = var27.bh;
                                        } else {
                                            cs.e[++b.menuX - 1] = var4;
                                        }

                                        var3 = 1;
                                    } else if (var0 == 4210) {
                                        var38 = cs.p[--ly.g];
                                        var12 = cs.e[--b.menuX];
                                        gc.jh(var38, var12 == 1);
                                        cs.e[++b.menuX - 1] = dh.ra;
                                        var3 = 1;
                                    } else if (var0 == 4211) {
                                        if (ab.rj != null && n.rd < dh.ra) {
                                            cs.e[++b.menuX - 1] = ab.rj[++n.rd - 1] & '\uffff';
                                        } else {
                                            cs.e[++b.menuX - 1] = -1;
                                        }

                                        var3 = 1;
                                    } else if (var0 == 4212) {
                                        n.rd = 0;
                                        var3 = 1;
                                    } else {
                                        var3 = 2;
                                    }
                                }
                            }

                            return var3;
                        } else if (var0 >= 5100) {
                            if (var0 < 5400) {
                                return ab.r(var0, var1, var2);
                            } else if (var0 < 5600) {
                                if (var0 == 5504) {
                                    b.menuX -= 2;
                                    var47 = cs.e[b.menuX];
                                    var15 = cs.e[b.menuX + 1];
                                    if (!Client.px) {
                                        Client.gq = var47;
                                        Client.hintPlane = var15;
                                    }

                                    var3 = 1;
                                } else if (var0 == 5505) {
                                    cs.e[++b.menuX - 1] = Client.gq;
                                    var3 = 1;
                                } else if (var0 == 5506) {
                                    cs.e[++b.menuX - 1] = Client.hintPlane;
                                    var3 = 1;
                                } else if (var0 == 5530) {
                                    var47 = cs.e[--b.menuX];
                                    if (var47 < 0) {
                                        var47 = 0;
                                    }

                                    Client.gl = var47;
                                    var3 = 1;
                                } else if (var0 == 5531) {
                                    cs.e[++b.menuX - 1] = Client.gl;
                                    var3 = 1;
                                } else {
                                    var3 = 2;
                                }

                                return var3;
                            } else if (var0 < 5700) {
                                if (var0 == 5630) {
                                    Client.ei = 250;
                                    var3 = 1;
                                } else {
                                    var3 = 2;
                                }

                                return var3;
                            } else if (var0 < 6300) {
                                return CombatBarData.y(var0, var1, var2);
                            } else if (var0 < 6600) {
                                return aj.h(var0, var1, var2);
                            } else {
                                return var0 < 6700 ? Tile.av(var0, var1, var2) : 2;
                            }
                        } else {
                            if (var0 == 5000) {
                                cs.e[++b.menuX - 1] = Client.np;
                                var3 = 1;
                            } else {
                                gd var39;
                                if (var0 == 5001) {
                                    b.menuX -= 3;
                                    Client.np = cs.e[b.menuX];
                                    var12 = cs.e[b.menuX + 1];
                                    ll[] var13 = new ll[] { ll.t, ll.q, ll.i };
                                    ll[] var7 = var13;
                                    var15 = 0;

                                    ll var11;
                                    while (true) {
                                        if (var15 >= var7.length) {
                                            var11 = null;
                                            break;
                                        }

                                        ll var16 = var7[var15];
                                        if (var12 == var16.a) {
                                            var11 = var16;
                                            break;
                                        }

                                        ++var15;
                                    }

                                    gf.nh = var11;
                                    if (gf.nh == null) {
                                        gf.nh = ll.q;
                                    }

                                    Client.nb = cs.e[b.menuX + 2];
                                    var39 = ap.t(fo.ci, Client.ee.l);
                                    var39.i.a(Client.np);
                                    var39.i.a(gf.nh.a);
                                    var39.i.a(Client.nb);
                                    Client.ee.i(var39);
                                    var3 = 1;
                                } else if (var0 == 5002) {
                                    var38 = cs.p[--ly.g];
                                    b.menuX -= 2;
                                    var12 = cs.e[b.menuX];
                                    var6 = cs.e[b.menuX + 1];
                                    gd var29 = ap.t(fo.cc, Client.ee.l);
                                    var29.i.a(z.c(var38) + 2);
                                    var29.i.u(var38);
                                    var29.i.a(var12 - 1);
                                    var29.i.a(var6);
                                    Client.ee.i(var29);
                                    var3 = 1;
                                } else if (var0 == 5003) {
                                    b.menuX -= 2;
                                    var4 = cs.e[b.menuX];
                                    var12 = cs.e[b.menuX + 1];
                                    Chatbox var30 = (Chatbox) cg.chatboxes.get(var4);
                                    ChatboxMessage var40 = var30.q(var12);
                                    if (var40 != null) {
                                        cs.e[++b.menuX - 1] = var40.index;
                                        cs.e[++b.menuX - 1] = var40.cycle;
                                        cs.p[++ly.g - 1] = var40.sender != null ? var40.sender : "";
                                        cs.p[++ly.g - 1] = var40.x != null ? var40.x : "";
                                        cs.p[++ly.g - 1] = var40.message != null ? var40.message : "";
                                        cs.e[++b.menuX - 1] = var40.i() ? 1 : (var40.b() ? 2 : 0);
                                    } else {
                                        cs.e[++b.menuX - 1] = -1;
                                        cs.e[++b.menuX - 1] = 0;
                                        cs.p[++ly.g - 1] = "";
                                        cs.p[++ly.g - 1] = "";
                                        cs.p[++ly.g - 1] = "";
                                        cs.e[++b.menuX - 1] = 0;
                                    }

                                    var3 = 1;
                                } else if (var0 == 5004) {
                                    var4 = cs.e[--b.menuX];
                                    ChatboxMessage var5 = w.i(var4);
                                    if (var5 != null) {
                                        cs.e[++b.menuX - 1] = var5.type;
                                        cs.e[++b.menuX - 1] = var5.cycle;
                                        cs.p[++ly.g - 1] = var5.sender != null ? var5.sender : "";
                                        cs.p[++ly.g - 1] = var5.x != null ? var5.x : "";
                                        cs.p[++ly.g - 1] = var5.message != null ? var5.message : "";
                                        cs.e[++b.menuX - 1] = var5.i() ? 1 : (var5.b() ? 2 : 0);
                                    } else {
                                        cs.e[++b.menuX - 1] = -1;
                                        cs.e[++b.menuX - 1] = 0;
                                        cs.p[++ly.g - 1] = "";
                                        cs.p[++ly.g - 1] = "";
                                        cs.p[++ly.g - 1] = "";
                                        cs.e[++b.menuX - 1] = 0;
                                    }

                                    var3 = 1;
                                } else if (var0 == 5005) {
                                    if (gf.nh == null) {
                                        cs.e[++b.menuX - 1] = -1;
                                    } else {
                                        cs.e[++b.menuX - 1] = gf.nh.a;
                                    }

                                    var3 = 1;
                                } else if (var0 == 5008) {
                                    var38 = cs.p[--ly.g];
                                    var12 = cs.e[--b.menuX];
                                    String var41 = var38.toLowerCase();
                                    byte var14 = 0;
                                    if (var41.startsWith("yellow:")) {
                                        var14 = 0;
                                        var38 = var38.substring("yellow:".length());
                                    } else if (var41.startsWith("red:")) {
                                        var14 = 1;
                                        var38 = var38.substring("red:".length());
                                    } else if (var41.startsWith("green:")) {
                                        var14 = 2;
                                        var38 = var38.substring("green:".length());
                                    } else if (var41.startsWith("cyan:")) {
                                        var14 = 3;
                                        var38 = var38.substring("cyan:".length());
                                    } else if (var41.startsWith("purple:")) {
                                        var14 = 4;
                                        var38 = var38.substring("purple:".length());
                                    } else if (var41.startsWith("white:")) {
                                        var14 = 5;
                                        var38 = var38.substring("white:".length());
                                    } else if (var41.startsWith("flash1:")) {
                                        var14 = 6;
                                        var38 = var38.substring("flash1:".length());
                                    } else if (var41.startsWith("flash2:")) {
                                        var14 = 7;
                                        var38 = var38.substring("flash2:".length());
                                    } else if (var41.startsWith("flash3:")) {
                                        var14 = 8;
                                        var38 = var38.substring("flash3:".length());
                                    } else if (var41.startsWith("glow1:")) {
                                        var14 = 9;
                                        var38 = var38.substring("glow1:".length());
                                    } else if (var41.startsWith("glow2:")) {
                                        var14 = 10;
                                        var38 = var38.substring("glow2:".length());
                                    } else if (var41.startsWith("glow3:")) {
                                        var14 = 11;
                                        var38 = var38.substring("glow3:".length());
                                    } else if (Client.bs != 0) {
                                        if (var41.startsWith("yellow:")) {
                                            var14 = 0;
                                            var38 = var38.substring("yellow:".length());
                                        } else if (var41.startsWith("red:")) {
                                            var14 = 1;
                                            var38 = var38.substring("red:".length());
                                        } else if (var41.startsWith("green:")) {
                                            var14 = 2;
                                            var38 = var38.substring("green:".length());
                                        } else if (var41.startsWith("cyan:")) {
                                            var14 = 3;
                                            var38 = var38.substring("cyan:".length());
                                        } else if (var41.startsWith("purple:")) {
                                            var14 = 4;
                                            var38 = var38.substring("purple:".length());
                                        } else if (var41.startsWith("white:")) {
                                            var14 = 5;
                                            var38 = var38.substring("white:".length());
                                        } else if (var41.startsWith("flash1:")) {
                                            var14 = 6;
                                            var38 = var38.substring("flash1:".length());
                                        } else if (var41.startsWith("flash2:")) {
                                            var14 = 7;
                                            var38 = var38.substring("flash2:".length());
                                        } else if (var41.startsWith("flash3:")) {
                                            var14 = 8;
                                            var38 = var38.substring("flash3:".length());
                                        } else if (var41.startsWith("glow1:")) {
                                            var14 = 9;
                                            var38 = var38.substring("glow1:".length());
                                        } else if (var41.startsWith("glow2:")) {
                                            var14 = 10;
                                            var38 = var38.substring("glow2:".length());
                                        } else if (var41.startsWith("glow3:")) {
                                            var14 = 11;
                                            var38 = var38.substring("glow3:".length());
                                        }
                                    }

                                    var41 = var38.toLowerCase();
                                    byte var44 = 0;
                                    if (var41.startsWith("wave:")) {
                                        var44 = 1;
                                        var38 = var38.substring("wave:".length());
                                    } else if (var41.startsWith("wave2:")) {
                                        var44 = 2;
                                        var38 = var38.substring("wave2:".length());
                                    } else if (var41.startsWith("shake:")) {
                                        var44 = 3;
                                        var38 = var38.substring("shake:".length());
                                    } else if (var41.startsWith("scroll:")) {
                                        var44 = 4;
                                        var38 = var38.substring("scroll:".length());
                                    } else if (var41.startsWith("slide:")) {
                                        var44 = 5;
                                        var38 = var38.substring("slide:".length());
                                    } else if (Client.bs != 0) {
                                        if (var41.startsWith("wave:")) {
                                            var44 = 1;
                                            var38 = var38.substring("wave:".length());
                                        } else if (var41.startsWith("wave2:")) {
                                            var44 = 2;
                                            var38 = var38.substring("wave2:".length());
                                        } else if (var41.startsWith("shake:")) {
                                            var44 = 3;
                                            var38 = var38.substring("shake:".length());
                                        } else if (var41.startsWith("scroll:")) {
                                            var44 = 4;
                                            var38 = var38.substring("scroll:".length());
                                        } else if (var41.startsWith("slide:")) {
                                            var44 = 5;
                                            var38 = var38.substring("slide:".length());
                                        }
                                    }

                                    gd var45 = ap.t(fo.as, Client.ee.l);
                                    var45.i.a(0);
                                    int var10 = var45.i.index;
                                    var45.i.a(var12);
                                    var45.i.a(var14);
                                    var45.i.a(var44);
                                    PlayerComposite.t(var45.i, var38);
                                    var45.i.r(var45.i.index - var10);
                                    Client.ee.i(var45);
                                    var3 = 1;
                                } else if (var0 == 5009) {
                                    ly.g -= 2;
                                    var38 = cs.p[ly.g];
                                    var25 = cs.p[ly.g + 1];
                                    var39 = ap.t(fo.bm, Client.ee.l);
                                    var39.i.l(0);
                                    var47 = var39.i.index;
                                    var39.i.u(var38);
                                    PlayerComposite.t(var39.i, var25);
                                    var39.i.f(var39.i.index - var47);
                                    Client.ee.i(var39);
                                    var3 = 1;
                                } else if (var0 == 5015) {
                                    if (az.il != null && az.il.t != null) {
                                        var38 = az.il.t.t();
                                    } else {
                                        var38 = "";
                                    }

                                    cs.p[++ly.g - 1] = var38;
                                    var3 = 1;
                                } else if (var0 == 5016) {
                                    cs.e[++b.menuX - 1] = Client.nb;
                                    var3 = 1;
                                } else if (var0 == 5017) {
                                    var4 = cs.e[--b.menuX];
                                    cs.e[++b.menuX - 1] = ak.a(var4);
                                    var3 = 1;
                                } else if (var0 == 5018) {
                                    var4 = cs.e[--b.menuX];
                                    cs.e[++b.menuX - 1] = Character.p(var4);
                                    var3 = 1;
                                } else if (var0 == 5019) {
                                    var4 = cs.e[--b.menuX];
                                    cs.e[++b.menuX - 1] = bv.x(var4);
                                    var3 = 1;
                                } else if (var0 == 5020) {
                                    var38 = cs.p[--ly.g];
                                    if (var38.equalsIgnoreCase("toggleroof")) {
                                        al.qp.roofsDisabled = !al.qp.roofsDisabled;
                                        ar.i();
                                        if (al.qp.roofsDisabled) {
                                            j.t(99, "", "Roofs are now all hidden");
                                        } else {
                                            j.t(99, "", "Roofs will only be removed selectively");
                                        }
                                    }

                                    if (var38.equalsIgnoreCase("displayfps")) {
                                        Client.bl = !Client.bl;
                                    }

                                    if (var38.equalsIgnoreCase("renderself")) {
                                        Client.iu = !Client.iu;
                                    }

                                    if (var38.equalsIgnoreCase("mouseovertext")) {
                                        Client.ky = !Client.ky;
                                    }

                                    if (Client.localPlayerRights >= 2) {
                                        if (var38.equalsIgnoreCase("aabb")) {
                                            if (!x.t) {
                                                x.t = true;
                                                x.a = o.q;
                                            } else if (o.q == x.a) {
                                                x.t = true;
                                                x.a = o.t;
                                            } else {
                                                x.t = false;
                                            }
                                        }

                                        if (var38.equalsIgnoreCase("showcoord")) {
                                            ip.rz.bc = !ip.rz.bc;
                                        }

                                        if (var38.equalsIgnoreCase("fpson")) {
                                            Client.bl = true;
                                        }

                                        if (var38.equalsIgnoreCase("fpsoff")) {
                                            Client.bl = false;
                                        }

                                        if (var38.equalsIgnoreCase("gc")) {
                                            System.gc();
                                        }

                                        if (var38.equalsIgnoreCase("clientdrop")) {
                                            av.fz();
                                        }

                                        if (var38.equalsIgnoreCase("cs")) {
                                            j.t(99, "", "" + Client.ew);
                                        }

                                        if (var38.equalsIgnoreCase("errortest") && Client.playerWeight == 2) {
                                            throw new RuntimeException();
                                        }
                                    }

                                    gd var26 = ap.t(fo.cb, Client.ee.l);
                                    var26.i.a(var38.length() + 1);
                                    var26.i.u(var38);
                                    Client.ee.i(var26);
                                    var3 = 1;
                                } else if (var0 == 5021) {
                                    Client.nz = cs.p[--ly.g].toLowerCase().trim();
                                    var3 = 1;
                                } else if (var0 == 5022) {
                                    cs.p[++ly.g - 1] = Client.nz;
                                    var3 = 1;
                                } else {
                                    var3 = 2;
                                }
                            }

                            return var3;
                        }
                    }
                }
            }
        }
    }

    @ObfuscatedName("q")
    public static int q(int var0, int var1, int var2, int var3, int var4, int var5) {
        if ((var5 & 1) == 1) {
            int var6 = var3;
            var3 = var4;
            var4 = var6;
        }

        var2 &= 3;
        if (var2 == 0) {
            return var0;
        } else if (var2 == 1) {
            return var1;
        } else {
            return var2 == 2 ? 7 - var0 - (var3 - 1) : 7 - var1 - (var4 - 1);
        }
    }

    @ObfuscatedName("x")
    public static void x() {
        jf.a.a();
    }

    @ObfuscatedName("ic")
    static void ic(RTComponent[] var0, int var1, int var2, int var3, boolean var4) {
        for (int var5 = 0; var5 < var0.length; ++var5) {
            RTComponent var6 = var0[var5];
            if (var6 != null && var6.parentId == var1) {
                aw.it(var6, var2, var3, var4);
                s.iw(var6, var2, var3);
                if (var6.horizontalScrollbarPosition > var6.al - var6.width) {
                    var6.horizontalScrollbarPosition = var6.al - var6.width;
                }

                if (var6.horizontalScrollbarPosition < 0) {
                    var6.horizontalScrollbarPosition = 0;
                }

                if (var6.verticalScrollbarPosition > var6.at - var6.height) {
                    var6.verticalScrollbarPosition = var6.at - var6.height;
                }

                if (var6.verticalScrollbarPosition < 0) {
                    var6.verticalScrollbarPosition = 0;
                }

                if (var6.type == 0) {
                    GameEngine.ig(var0, var6, var4);
                }
            }
        }

    }
}
