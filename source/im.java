import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("im")
public enum im implements gl {
    @ObfuscatedName("t")
    t(7, 0), @ObfuscatedName("q")
    q(5, 1), @ObfuscatedName("i")
    i(0, 2), @ObfuscatedName("a")
    a(1, 3), @ObfuscatedName("l")
    l(3, 4), @ObfuscatedName("b")
    b(2, 5), @ObfuscatedName("e")
    e(4, 6), @ObfuscatedName("x")
    x(6, 7);

    @ObfuscatedName("pv")
    static int pv;

    @ObfuscatedName("p")
    public final int p;

    @ObfuscatedName("g")
    final int g;

    im(int var3, int var4) {
        this.p = var3;
        this.g = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.g;
    }
}
