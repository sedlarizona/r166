import java.lang.ref.SoftReference;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hw")
public class IterableDoublyNode extends hs {

    @ObfuscatedName("t")
    SoftReference node;

    IterableDoublyNode(Object var1, int var2) {
        super(var2);
        this.node = new SoftReference(var1);
    }

    @ObfuscatedName("t")
    Object t() {
        return this.node.get();
    }

    @ObfuscatedName("q")
    boolean q() {
        return true;
    }
}
