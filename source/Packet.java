import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gz")
public final class Packet extends ByteBuffer {

    @ObfuscatedName("g")
    static final int[] g = new int[] { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767,
            65535, 131071, 262143, 524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431, 67108863, 134217727,
            268435455, 536870911, 1073741823, Integer.MAX_VALUE, -1 };

    @ObfuscatedName("p")
    gv cipher;

    @ObfuscatedName("n")
    int position;

    public Packet(int var1) {
        super(var1);
    }

    @ObfuscatedName("iz")
    public void iz(int[] var1) {
        this.cipher = new gv(var1);
    }

    @ObfuscatedName("il")
    public void il(gv var1) {
        this.cipher = var1;
    }

    @ObfuscatedName("iu")
    public void iu(int var1) {
        super.buffer[++super.index - 1] = (byte) (var1 + this.cipher.t());
    }

    @ObfuscatedName("in")
    public int in() {
        return super.buffer[++super.index - 1] - this.cipher.t() & 255;
    }

    @ObfuscatedName("ia")
    public boolean ia() {
        int var1 = super.buffer[super.index] - this.cipher.q() & 255;
        return var1 >= 128;
    }

    @ObfuscatedName("jk")
    public int jk() {
        int var1 = super.buffer[++super.index - 1] - this.cipher.t() & 255;
        return var1 < 128 ? var1 : (var1 - 128 << 8) + (super.buffer[++super.index - 1] - this.cipher.t() & 255);
    }

    @ObfuscatedName("jc")
    public void jc() {
        this.position = super.index * 8;
    }

    @ObfuscatedName("jt")
    public int jt(int var1) {
        int var2 = this.position >> 3;
        int var3 = 8 - (this.position & 7);
        int var4 = 0;

        for (this.position += var1; var1 > var3; var3 = 8) {
            var4 += (super.buffer[var2++] & g[var3]) << var1 - var3;
            var1 -= var3;
        }

        if (var3 == var1) {
            var4 += super.buffer[var2] & g[var3];
        } else {
            var4 += super.buffer[var2] >> var3 - var1 & g[var1];
        }

        return var4;
    }

    @ObfuscatedName("ju")
    public void ju() {
        super.index = (this.position + 7) / 8;
    }

    @ObfuscatedName("jg")
    public int jg(int var1) {
        return var1 * 8 - this.position;
    }
}
