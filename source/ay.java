import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ay")
public class ay implements as {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    int v;

    @ObfuscatedName("t")
    public void t(az var1) {
        if (var1.e > this.l) {
            var1.e = this.l;
        }

        if (var1.x < this.l) {
            var1.x = this.l;
        }

        if (var1.p > this.b) {
            var1.p = this.b;
        }

        if (var1.g < this.b) {
            var1.g = this.b;
        }

    }

    @ObfuscatedName("q")
    public boolean q(int var1, int var2, int var3) {
        if (var1 >= this.t && var1 < this.q + this.t) {
            return var2 >= (this.i << 6) + (this.e << 3) && var2 <= (this.i << 6) + (this.p << 3) + 7
                    && var3 >= (this.a << 6) + (this.x << 3) && var3 <= (this.a << 6) + (this.g << 3) + 7;
        } else {
            return false;
        }
    }

    @ObfuscatedName("i")
    public boolean i(int var1, int var2) {
        return var1 >= (this.l << 6) + (this.n << 3) && var1 <= (this.l << 6) + (this.c << 3) + 7
                && var2 >= (this.b << 6) + (this.o << 3) && var2 <= (this.b << 6) + (this.v << 3) + 7;
    }

    @ObfuscatedName("a")
    public int[] a(int var1, int var2, int var3) {
        if (!this.q(var1, var2, var3)) {
            return null;
        } else {
            int[] var4 = new int[] { this.l * 64 - this.i * 64 + var2 + (this.n * 8 - this.e * 8),
                    var3 + (this.b * 64 - this.a * 64) + (this.o * 8 - this.x * 8) };
            return var4;
        }
    }

    @ObfuscatedName("l")
    public ik l(int var1, int var2) {
        if (!this.i(var1, var2)) {
            return null;
        } else {
            int var3 = this.i * 64 - this.l * 64 + (this.e * 8 - this.n * 8) + var1;
            int var4 = this.a * 64 - this.b * 64 + var2 + (this.x * 8 - this.o * 8);
            return new ik(this.t, var3, var4);
        }
    }

    @ObfuscatedName("b")
    public void b(ByteBuffer var1) {
        this.t = var1.av();
        this.q = var1.av();
        this.i = var1.ae();
        this.e = var1.av();
        this.p = var1.av();
        this.a = var1.ae();
        this.x = var1.av();
        this.g = var1.av();
        this.l = var1.ae();
        this.n = var1.av();
        this.c = var1.av();
        this.b = var1.ae();
        this.o = var1.av();
        this.v = var1.av();
        this.e();
    }

    @ObfuscatedName("e")
    void e() {
    }

    @ObfuscatedName("b")
    static void b() {
        Iterator var0 = cg.q.iterator();

        while (var0.hasNext()) {
            ChatboxMessage var1 = (ChatboxMessage) var0.next();
            var1.q();
        }

    }
}
