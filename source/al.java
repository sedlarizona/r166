import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("al")
public class al {

    @ObfuscatedName("qp")
    static ClientPreferences qp;

    @ObfuscatedName("t")
    public final int t;

    @ObfuscatedName("q")
    public final ik q;

    @ObfuscatedName("i")
    public final ik i;

    @ObfuscatedName("a")
    final int a;

    @ObfuscatedName("l")
    final int l;

    @ObfuscatedName("b")
    final ah b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    al(int var1, ik var2, ik var3, ah var4) {
        this.t = var1;
        this.i = var2;
        this.q = var3;
        this.b = var4;
        jj var5 = jj.q[this.t];
        Sprite var6 = var5.a(false);
        if (var6 != null) {
            this.a = var6.width;
            this.l = var6.height;
        } else {
            this.a = 0;
            this.l = 0;
        }

    }

    @ObfuscatedName("t")
    boolean t(int var1, int var2) {
        if (this.q(var1, var2)) {
            return true;
        } else {
            return this.i(var1, var2);
        }
    }

    @ObfuscatedName("q")
    boolean q(int var1, int var2) {
        jj var3 = jj.q[this.t];
        switch (var3.w.a) {
        case 0:
            if (var1 < this.e - this.a / 2 || var1 > this.a / 2 + this.e) {
                return false;
            }
            break;
        case 1:
            if (var1 < this.e || var1 >= this.e + this.a) {
                return false;
            }
            break;
        case 2:
            if (var1 <= this.e - this.a || var1 > this.e) {
                return false;
            }
        }

        switch (var3.s.a) {
        case 0:
            if (var2 >= this.x - this.l / 2 && var2 <= this.l / 2 + this.x) {
                break;
            }

            return false;
        case 1:
            if (var2 > this.x - this.l && var2 <= this.x) {
                break;
            }

            return false;
        case 2:
            if (var2 < this.x || var2 >= this.l + this.x) {
                return false;
            }
        }

        return true;
    }

    @ObfuscatedName("i")
    boolean i(int var1, int var2) {
        if (this.b == null) {
            return false;
        } else if (var1 >= this.e - this.b.q / 2 && var1 <= this.b.q / 2 + this.e) {
            return var2 >= this.x && var2 <= this.b.i + this.x;
        } else {
            return false;
        }
    }

    @ObfuscatedName("am")
    public static final TaskData am(TaskHandler var0, int var1, int var2) {
        if (TaskData.l == 0) {
            throw new IllegalStateException();
        } else if (var1 >= 0 && var1 < 2) {
            if (var2 < 256) {
                var2 = 256;
            }

            try {
                TaskData var3 = TaskData.c.t();
                var3.v = new int[256 * (TaskData.b ? 2 : 1)];
                var3.w = var2;
                var3.t();
                var3.z = (var2 & -1024) + 1024;
                if (var3.z > 16384) {
                    var3.z = 16384;
                }

                var3.q(var3.z);
                if (gh.p > 0 && TaskData.g == null) {
                    TaskData.g = new dh();
                    CombatBarData.x = Executors.newScheduledThreadPool(1);
                    CombatBarData.x.scheduleAtFixedRate(TaskData.g, 0L, 10L, TimeUnit.MILLISECONDS);
                }

                if (TaskData.g != null) {
                    if (TaskData.g.t[var1] != null) {
                        throw new IllegalArgumentException();
                    }

                    TaskData.g.t[var1] = var3;
                }

                return var3;
            } catch (Throwable var4) {
                return new TaskData();
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @ObfuscatedName("fb")
    static final void fb() {
        if (gc.pk != null) {
            gc.pk.ap();
        }

        if (c.pi != null) {
            c.pi.ap();
        }

    }

    @ObfuscatedName("fx")
    static void fx(int var0) {
        Client.ng = 0L;
        if (var0 >= 2) {
            Client.ny = true;
        } else {
            Client.ny = false;
        }

        if (bg.fq() == 1) {
            ir.ac.b(765, 503);
        } else {
            ir.ac.b(7680, 2160);
        }

        if (Client.packet >= 25) {
            gd var1 = ap.t(fo.af, Client.ee.l);
            var1.i.a(bg.fq());
            var1.i.l(BoundaryObject.r);
            var1.i.l(GameEngine.h);
            Client.ee.i(var1);
        }

    }

    @ObfuscatedName("gk")
    static final void gk(boolean var0) {
        for (int var1 = 0; var1 < Client.dz; ++var1) {
            Npc var2 = Client.loadedNpcs[Client.npcIndices[var1]];
            int var3 = (Client.npcIndices[var1] << 14) + 536870912;
            if (var2 != null && var2.k() && var2.composite.m == var0 && var2.composite.p()) {
                int var4 = var2.regionX >> 7;
                int var5 = var2.regionY >> 7;
                if (var4 >= 0 && var4 < 104 && var5 >= 0 && var5 < 104) {
                    if (var2.ap == 1 && (var2.regionX & 127) == 64 && (var2.regionY & 127) == 64) {
                        if (Client.hl[var4][var5] == Client.hu) {
                            continue;
                        }

                        Client.hl[var4][var5] = Client.hu;
                    }

                    if (!var2.composite.ap) {
                        var3 -= Integer.MIN_VALUE;
                    }

                    var2.ah = Client.bz;
                    an.loadedRegion.u(kt.ii, var2.regionX, var2.regionY,
                            ef.gn(var2.ap * 64 - 64 + var2.regionX, var2.ap * 64 - 64 + var2.regionY, kt.ii),
                            var2.ap * 64 - 64 + 60, var2, var2.orientation, var3, var2.animatingHeldItems);
                }
            }
        }

    }
}
