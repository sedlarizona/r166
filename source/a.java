import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("a")
final class a implements t {

    @ObfuscatedName("u")
    static int u;

    @ObfuscatedName("q")
    public static String q(long var0) {
        if (var0 > 0L && var0 < 6582952005840035281L) {
            if (var0 % 37L == 0L) {
                return null;
            } else {
                int var2 = 0;

                for (long var3 = var0; 0L != var3; var3 /= 37L) {
                    ++var2;
                }

                StringBuilder var5 = new StringBuilder(var2);

                while (0L != var0) {
                    long var6 = var0;
                    var0 /= 37L;
                    var5.append(ly.t[(int) (var6 - var0 * 37L)]);
                }

                return var5.reverse().toString();
            }
        } else {
            return null;
        }
    }

    @ObfuscatedName("hu")
    static final void hu(int var0, int var1, int var2, int var3, String var4, String var5, int var6, int var7) {
        if (var2 >= 2000) {
            var2 -= 2000;
        }

        gd var8;
        if (var2 == 1) {
            Client.lastSelectedInterfaceUID = var6;
            Client.lastSelectedComponentId = var7;
            Client.mouseCrosshairState = 2;
            Client.hb = 0;
            Client.destinationX = var0;
            Client.destinationY = var1;
            var8 = ap.t(fo.cu, Client.ee.l);
            var8.i.by(var0 + an.regionBaseX);
            var8.i.bt(var3 >> 14 & 32767);
            var8.i.bu(bg.ix);
            var8.i.l(l.iz);
            var8.i.ba(ad.pressedKeys[82] ? 1 : 0);
            var8.i.bn(jz.ks);
            var8.i.bn(PlayerComposite.ep + var1);
            Client.ee.i(var8);
        } else if (var2 == 2) {
            Client.lastSelectedInterfaceUID = var6;
            Client.lastSelectedComponentId = var7;
            Client.mouseCrosshairState = 2;
            Client.hb = 0;
            Client.destinationX = var0;
            Client.destinationY = var1;
            var8 = ap.t(fo.cy, Client.ee.l);
            var8.i.bt(var3 >> 14 & 32767);
            var8.i.bt(var0 + an.regionBaseX);
            var8.i.bk(ad.pressedKeys[82] ? 1 : 0);
            var8.i.l(Client.kq);
            var8.i.bu(dt.kr);
            var8.i.bn(PlayerComposite.ep + var1);
            Client.ee.i(var8);
        } else if (var2 == 3) {
            Client.lastSelectedInterfaceUID = var6;
            Client.lastSelectedComponentId = var7;
            Client.mouseCrosshairState = 2;
            Client.hb = 0;
            Client.destinationX = var0;
            Client.destinationY = var1;
            var8 = ap.t(fo.y, Client.ee.l);
            var8.i.bt(PlayerComposite.ep + var1);
            var8.i.by(var0 + an.regionBaseX);
            var8.i.by(var3 >> 14 & 32767);
            var8.i.br(ad.pressedKeys[82] ? 1 : 0);
            Client.ee.i(var8);
        } else if (var2 == 4) {
            Client.lastSelectedInterfaceUID = var6;
            Client.lastSelectedComponentId = var7;
            Client.mouseCrosshairState = 2;
            Client.hb = 0;
            Client.destinationX = var0;
            Client.destinationY = var1;
            var8 = ap.t(fo.bj, Client.ee.l);
            var8.i.l(var0 + an.regionBaseX);
            var8.i.by(var3 >> 14 & 32767);
            var8.i.br(ad.pressedKeys[82] ? 1 : 0);
            var8.i.by(PlayerComposite.ep + var1);
            Client.ee.i(var8);
        } else if (var2 == 5) {
            Client.lastSelectedInterfaceUID = var6;
            Client.lastSelectedComponentId = var7;
            Client.mouseCrosshairState = 2;
            Client.hb = 0;
            Client.destinationX = var0;
            Client.destinationY = var1;
            var8 = ap.t(fo.f, Client.ee.l);
            var8.i.bt(var0 + an.regionBaseX);
            var8.i.a(ad.pressedKeys[82] ? 1 : 0);
            var8.i.bn(PlayerComposite.ep + var1);
            var8.i.bn(var3 >> 14 & 32767);
            Client.ee.i(var8);
        } else if (var2 == 6) {
            Client.lastSelectedInterfaceUID = var6;
            Client.lastSelectedComponentId = var7;
            Client.mouseCrosshairState = 2;
            Client.hb = 0;
            Client.destinationX = var0;
            Client.destinationY = var1;
            var8 = ap.t(fo.bl, Client.ee.l);
            var8.i.l(PlayerComposite.ep + var1);
            var8.i.l(var3 >> 14 & 32767);
            var8.i.bk(ad.pressedKeys[82] ? 1 : 0);
            var8.i.bn(var0 + an.regionBaseX);
            Client.ee.i(var8);
        } else {
            gd var9;
            Npc var19;
            if (var2 == 7) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.av, Client.ee.l);
                    var9.i.bu(bg.ix);
                    var9.i.bn(var3);
                    var9.i.bt(l.iz);
                    var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                    var9.i.by(jz.ks);
                    Client.ee.i(var9);
                }
            } else if (var2 == 8) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.ch, Client.ee.l);
                    var9.i.bn(var3);
                    var9.i.by(Client.kq);
                    var9.i.bk(ad.pressedKeys[82] ? 1 : 0);
                    var9.i.bu(dt.kr);
                    Client.ee.i(var9);
                }
            } else if (var2 == 9) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.bn, Client.ee.l);
                    var9.i.bt(var3);
                    var9.i.ba(ad.pressedKeys[82] ? 1 : 0);
                    Client.ee.i(var9);
                }
            } else if (var2 == 10) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.u, Client.ee.l);
                    var9.i.l(var3);
                    var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                    Client.ee.i(var9);
                }
            } else if (var2 == 11) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.cq, Client.ee.l);
                    var9.i.bn(var3);
                    var9.i.ba(ad.pressedKeys[82] ? 1 : 0);
                    Client.ee.i(var9);
                }
            } else if (var2 == 12) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.am, Client.ee.l);
                    var9.i.ba(ad.pressedKeys[82] ? 1 : 0);
                    var9.i.by(var3);
                    Client.ee.i(var9);
                }
            } else if (var2 == 13) {
                var19 = Client.loadedNpcs[var3];
                if (var19 != null) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var9 = ap.t(fo.cx, Client.ee.l);
                    var9.i.bn(var3);
                    var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                    Client.ee.i(var9);
                }
            } else {
                Player var21;
                if (var2 == 14) {
                    var21 = Client.loadedPlayers[var3];
                    if (var21 != null) {
                        Client.lastSelectedInterfaceUID = var6;
                        Client.lastSelectedComponentId = var7;
                        Client.mouseCrosshairState = 2;
                        Client.hb = 0;
                        Client.destinationX = var0;
                        Client.destinationY = var1;
                        var9 = ap.t(fo.aa, Client.ee.l);
                        var9.i.bk(ad.pressedKeys[82] ? 1 : 0);
                        var9.i.l(jz.ks);
                        var9.i.bt(l.iz);
                        var9.i.bw(bg.ix);
                        var9.i.bt(var3);
                        Client.ee.i(var9);
                    }
                } else if (var2 == 15) {
                    var21 = Client.loadedPlayers[var3];
                    if (var21 != null) {
                        Client.lastSelectedInterfaceUID = var6;
                        Client.lastSelectedComponentId = var7;
                        Client.mouseCrosshairState = 2;
                        Client.hb = 0;
                        Client.destinationX = var0;
                        Client.destinationY = var1;
                        var9 = ap.t(fo.c, Client.ee.l);
                        var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                        var9.i.e(dt.kr);
                        var9.i.bn(Client.kq);
                        var9.i.bn(var3);
                        Client.ee.i(var9);
                    }
                } else if (var2 == 16) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.ae, Client.ee.l);
                    var8.i.bn(jz.ks);
                    var8.i.by(PlayerComposite.ep + var1);
                    var8.i.e(bg.ix);
                    var8.i.bk(ad.pressedKeys[82] ? 1 : 0);
                    var8.i.by(var0 + an.regionBaseX);
                    var8.i.bt(l.iz);
                    var8.i.l(var3);
                    Client.ee.i(var8);
                } else if (var2 == 17) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.bq, Client.ee.l);
                    var8.i.bt(PlayerComposite.ep + var1);
                    var8.i.bn(var0 + an.regionBaseX);
                    var8.i.e(dt.kr);
                    var8.i.bt(Client.kq);
                    var8.i.bn(var3);
                    var8.i.ba(ad.pressedKeys[82] ? 1 : 0);
                    Client.ee.i(var8);
                } else if (var2 == 18) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.ag, Client.ee.l);
                    var8.i.br(ad.pressedKeys[82] ? 1 : 0);
                    var8.i.l(PlayerComposite.ep + var1);
                    var8.i.bn(var0 + an.regionBaseX);
                    var8.i.l(var3);
                    Client.ee.i(var8);
                } else if (var2 == 19) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.cv, Client.ee.l);
                    var8.i.bn(PlayerComposite.ep + var1);
                    var8.i.by(var3);
                    var8.i.a(ad.pressedKeys[82] ? 1 : 0);
                    var8.i.bt(var0 + an.regionBaseX);
                    Client.ee.i(var8);
                } else if (var2 == 20) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.x, Client.ee.l);
                    var8.i.br(ad.pressedKeys[82] ? 1 : 0);
                    var8.i.bt(var3);
                    var8.i.bt(PlayerComposite.ep + var1);
                    var8.i.bn(var0 + an.regionBaseX);
                    Client.ee.i(var8);
                } else if (var2 == 21) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.ao, Client.ee.l);
                    var8.i.bk(ad.pressedKeys[82] ? 1 : 0);
                    var8.i.l(PlayerComposite.ep + var1);
                    var8.i.l(var0 + an.regionBaseX);
                    var8.i.bn(var3);
                    Client.ee.i(var8);
                } else if (var2 == 22) {
                    Client.lastSelectedInterfaceUID = var6;
                    Client.lastSelectedComponentId = var7;
                    Client.mouseCrosshairState = 2;
                    Client.hb = 0;
                    Client.destinationX = var0;
                    Client.destinationY = var1;
                    var8 = ap.t(fo.cm, Client.ee.l);
                    var8.i.br(ad.pressedKeys[82] ? 1 : 0);
                    var8.i.by(var3);
                    var8.i.bt(PlayerComposite.ep + var1);
                    var8.i.bt(var0 + an.regionBaseX);
                    Client.ee.i(var8);
                } else if (var2 == 23) {
                    if (Client.menuOpen) {
                        an.loadedRegion.aq();
                    } else {
                        an.loadedRegion.aw(kt.ii, var0, var1, true);
                    }
                } else {
                    gd var16;
                    RTComponent var23;
                    if (var2 == 24) {
                        var23 = Inflater.t(var1);
                        boolean var15 = true;
                        if (var23.contentType > 0) {
                            var15 = ag.ja(var23);
                        }

                        if (var15) {
                            var16 = ap.t(fo.bz, Client.ee.l);
                            var16.i.e(var1);
                            Client.ee.i(var16);
                        }
                    } else {
                        int var10;
                        int var24;
                        if (var2 == 25) {
                            var23 = CollisionMap.q(var1, var0);
                            if (var23 != null) {
                                kn.hv();
                                var10 = u.jr(var23);
                                var24 = var10 >> 11 & 63;
                                int var12 = var23.itemId;
                                RTComponent var13 = CollisionMap.q(var1, var0);
                                if (var13 != null && var13.dl != null) {
                                    ScriptEvent var14 = new ScriptEvent();
                                    var14.i = var13;
                                    var14.args = var13.dl;
                                    m.t(var14, 500000);
                                }

                                Client.kn = var12;
                                Client.interfaceSelected = true;
                                dt.kr = var1;
                                Client.kq = var0;
                                eq.km = var24;
                                GameEngine.jk(var13);
                                Client.inventoryItemSelectionState = 0;
                                Client.lj = f.jl(var23);
                                if (Client.lj == null) {
                                    Client.lj = "Null";
                                }

                                if (var23.modern) {
                                    Client.selectedSpellName = var23.name + ar.q(16777215);
                                } else {
                                    Client.selectedSpellName = ar.q(65280) + var23.ed + ar.q(16777215);
                                }
                            }

                            return;
                        }

                        if (var2 == 26) {
                            var8 = ap.t(fo.ax, Client.ee.l);
                            Client.ee.i(var8);

                            for (SubWindow var20 = (SubWindow) Client.subWindowTable.a(); var20 != null; var20 = (SubWindow) Client.subWindowTable
                                    .l()) {
                                if (var20.type == 0 || var20.type == 3) {
                                    cx.ji(var20, true);
                                }
                            }

                            if (Client.ls != null) {
                                GameEngine.jk(Client.ls);
                                Client.ls = null;
                            }
                        } else {
                            RTComponent var22;
                            if (var2 == 28) {
                                var8 = ap.t(fo.bz, Client.ee.l);
                                var8.i.e(var1);
                                Client.ee.i(var8);
                                var22 = Inflater.t(var1);
                                if (var22.ej != null && var22.ej[0][0] == 5) {
                                    var10 = var22.ej[0][1];
                                    iv.varps[var10] = 1 - iv.varps[var10];
                                    b.jg(var10);
                                }
                            } else if (var2 == 29) {
                                var8 = ap.t(fo.bz, Client.ee.l);
                                var8.i.e(var1);
                                Client.ee.i(var8);
                                var22 = Inflater.t(var1);
                                if (var22.ej != null && var22.ej[0][0] == 5) {
                                    var10 = var22.ej[0][1];
                                    if (iv.varps[var10] != var22.eu[0]) {
                                        iv.varps[var10] = var22.eu[0];
                                        b.jg(var10);
                                    }
                                }
                            } else if (var2 == 30) {
                                if (Client.ls == null) {
                                    jq.hd(var1, var0);
                                    Client.ls = CollisionMap.q(var1, var0);
                                    GameEngine.jk(Client.ls);
                                }
                            } else if (var2 == 31) {
                                var8 = ap.t(fo.bs, Client.ee.l);
                                var8.i.bw(var1);
                                var8.i.l(l.iz);
                                var8.i.l(jz.ks);
                                var8.i.bl(bg.ix);
                                var8.i.l(var0);
                                var8.i.l(var3);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else if (var2 == 32) {
                                var8 = ap.t(fo.au, Client.ee.l);
                                var8.i.e(var1);
                                var8.i.by(var3);
                                var8.i.bt(var0);
                                var8.i.e(dt.kr);
                                var8.i.by(Client.kq);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else if (var2 == 33) {
                                var8 = ap.t(fo.ak, Client.ee.l);
                                var8.i.l(var3);
                                var8.i.bw(var1);
                                var8.i.by(var0);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else if (var2 == 34) {
                                var8 = ap.t(fo.bf, Client.ee.l);
                                var8.i.bn(var3);
                                var8.i.bt(var0);
                                var8.i.bu(var1);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else if (var2 == 35) {
                                var8 = ap.t(fo.s, Client.ee.l);
                                var8.i.bu(var1);
                                var8.i.l(var3);
                                var8.i.bn(var0);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else if (var2 == 36) {
                                var8 = ap.t(fo.t, Client.ee.l);
                                var8.i.bt(var3);
                                var8.i.bl(var1);
                                var8.i.bn(var0);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else if (var2 == 37) {
                                var8 = ap.t(fo.z, Client.ee.l);
                                var8.i.bt(var3);
                                var8.i.bl(var1);
                                var8.i.bn(var0);
                                Client.ee.i(var8);
                                Client.iq = 0;
                                MouseTracker.ie = Inflater.t(var1);
                                Client.iv = var0;
                            } else {
                                if (var2 == 38) {
                                    kn.hv();
                                    var23 = Inflater.t(var1);
                                    Client.inventoryItemSelectionState = 1;
                                    jz.ks = var0;
                                    bg.ix = var1;
                                    l.iz = var3;
                                    GameEngine.jk(var23);
                                    Client.lastSelectedInventoryItemName = ar.q(16748608)
                                            + cs.loadItemDefinition(var3).name + ar.q(16777215);
                                    if (Client.lastSelectedInventoryItemName == null) {
                                        Client.lastSelectedInventoryItemName = "null";
                                    }

                                    return;
                                }

                                if (var2 == 39) {
                                    var8 = ap.t(fo.cd, Client.ee.l);
                                    var8.i.l(var0);
                                    var8.i.by(var3);
                                    var8.i.bu(var1);
                                    Client.ee.i(var8);
                                    Client.iq = 0;
                                    MouseTracker.ie = Inflater.t(var1);
                                    Client.iv = var0;
                                } else if (var2 == 40) {
                                    var8 = ap.t(fo.j, Client.ee.l);
                                    var8.i.bn(var3);
                                    var8.i.bl(var1);
                                    var8.i.by(var0);
                                    Client.ee.i(var8);
                                    Client.iq = 0;
                                    MouseTracker.ie = Inflater.t(var1);
                                    Client.iv = var0;
                                } else if (var2 == 41) {
                                    var8 = ap.t(fo.az, Client.ee.l);
                                    var8.i.l(var3);
                                    var8.i.by(var0);
                                    var8.i.e(var1);
                                    Client.ee.i(var8);
                                    Client.iq = 0;
                                    MouseTracker.ie = Inflater.t(var1);
                                    Client.iv = var0;
                                } else if (var2 == 42) {
                                    var8 = ap.t(fo.w, Client.ee.l);
                                    var8.i.by(var3);
                                    var8.i.bu(var1);
                                    var8.i.bt(var0);
                                    Client.ee.i(var8);
                                    Client.iq = 0;
                                    MouseTracker.ie = Inflater.t(var1);
                                    Client.iv = var0;
                                } else if (var2 == 43) {
                                    var8 = ap.t(fo.aj, Client.ee.l);
                                    var8.i.l(var0);
                                    var8.i.bn(var3);
                                    var8.i.bu(var1);
                                    Client.ee.i(var8);
                                    Client.iq = 0;
                                    MouseTracker.ie = Inflater.t(var1);
                                    Client.iv = var0;
                                } else if (var2 == 44) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.bo, Client.ee.l);
                                        var9.i.ba(ad.pressedKeys[82] ? 1 : 0);
                                        var9.i.bn(var3);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 45) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.cs, Client.ee.l);
                                        var9.i.l(var3);
                                        var9.i.br(ad.pressedKeys[82] ? 1 : 0);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 46) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.be, Client.ee.l);
                                        var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                                        var9.i.l(var3);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 47) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.v, Client.ee.l);
                                        var9.i.br(ad.pressedKeys[82] ? 1 : 0);
                                        var9.i.bn(var3);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 48) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.cj, Client.ee.l);
                                        var9.i.by(var3);
                                        var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 49) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.a, Client.ee.l);
                                        var9.i.a(ad.pressedKeys[82] ? 1 : 0);
                                        var9.i.l(var3);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 50) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.co, Client.ee.l);
                                        var9.i.br(ad.pressedKeys[82] ? 1 : 0);
                                        var9.i.by(var3);
                                        Client.ee.i(var9);
                                    }
                                } else if (var2 == 51) {
                                    var21 = Client.loadedPlayers[var3];
                                    if (var21 != null) {
                                        Client.lastSelectedInterfaceUID = var6;
                                        Client.lastSelectedComponentId = var7;
                                        Client.mouseCrosshairState = 2;
                                        Client.hb = 0;
                                        Client.destinationX = var0;
                                        Client.destinationY = var1;
                                        var9 = ap.t(fo.ad, Client.ee.l);
                                        var9.i.bk(ad.pressedKeys[82] ? 1 : 0);
                                        var9.i.bt(var3);
                                        Client.ee.i(var9);
                                    }
                                } else {
                                    label1161: {
                                        if (var2 != 57) {
                                            if (var2 == 58) {
                                                var23 = CollisionMap.q(var1, var0);
                                                if (var23 != null) {
                                                    var9 = ap.t(fo.ah, Client.ee.l);
                                                    var9.i.bn(Client.kq);
                                                    var9.i.by(var23.itemId);
                                                    var9.i.l(Client.kn);
                                                    var9.i.bu(var1);
                                                    var9.i.bu(dt.kr);
                                                    var9.i.bt(var0);
                                                    Client.ee.i(var9);
                                                }
                                                break label1161;
                                            }

                                            if (var2 == 1001) {
                                                Client.lastSelectedInterfaceUID = var6;
                                                Client.lastSelectedComponentId = var7;
                                                Client.mouseCrosshairState = 2;
                                                Client.hb = 0;
                                                Client.destinationX = var0;
                                                Client.destinationY = var1;
                                                var8 = ap.t(fo.g, Client.ee.l);
                                                var8.i.bn(var3 >> 14 & 32767);
                                                var8.i.a(ad.pressedKeys[82] ? 1 : 0);
                                                var8.i.bn(var0 + an.regionBaseX);
                                                var8.i.l(PlayerComposite.ep + var1);
                                                Client.ee.i(var8);
                                                break label1161;
                                            }

                                            if (var2 == 1002) {
                                                Client.lastSelectedInterfaceUID = var6;
                                                Client.lastSelectedComponentId = var7;
                                                Client.mouseCrosshairState = 2;
                                                Client.hb = 0;
                                                var8 = ap.t(fo.k, Client.ee.l);
                                                var8.i.by(var3 >> 14 & 32767);
                                                Client.ee.i(var8);
                                                break label1161;
                                            }

                                            if (var2 == 1003) {
                                                Client.lastSelectedInterfaceUID = var6;
                                                Client.lastSelectedComponentId = var7;
                                                Client.mouseCrosshairState = 2;
                                                Client.hb = 0;
                                                var19 = Client.loadedNpcs[var3];
                                                if (var19 != null) {
                                                    NpcDefinition var26 = var19.composite;
                                                    if (var26.ae != null) {
                                                        var26 = var26.x();
                                                    }

                                                    if (var26 != null) {
                                                        var16 = ap.t(fo.bv, Client.ee.l);
                                                        var16.i.by(var26.id);
                                                        Client.ee.i(var16);
                                                    }
                                                }
                                                break label1161;
                                            }

                                            if (var2 == 1004) {
                                                Client.lastSelectedInterfaceUID = var6;
                                                Client.lastSelectedComponentId = var7;
                                                Client.mouseCrosshairState = 2;
                                                Client.hb = 0;
                                                var8 = ap.t(fo.aq, Client.ee.l);
                                                var8.i.bn(var3);
                                                Client.ee.i(var8);
                                                break label1161;
                                            }

                                            if (var2 == 1005) {
                                                var23 = Inflater.t(var1);
                                                if (var23 != null && var23.itemStackSizes[var0] >= 100000) {
                                                    j.t(27,
                                                            "",
                                                            var23.itemStackSizes[var0] + " x "
                                                                    + cs.loadItemDefinition(var3).name);
                                                } else {
                                                    var9 = ap.t(fo.aq, Client.ee.l);
                                                    var9.i.bn(var3);
                                                    Client.ee.i(var9);
                                                }

                                                Client.iq = 0;
                                                MouseTracker.ie = Inflater.t(var1);
                                                Client.iv = var0;
                                                break label1161;
                                            }

                                            if (var2 != 1007) {
                                                if (var2 == 1008 || var2 == 1009 || var2 == 1011 || var2 == 1010
                                                        || var2 == 1012) {
                                                    ip.rz.bn(var2, var3, new ik(var0), new ik(var1));
                                                }
                                                break label1161;
                                            }
                                        }

                                        var23 = CollisionMap.q(var1, var0);
                                        if (var23 != null) {
                                            var24 = var23.itemId;
                                            RTComponent var25 = CollisionMap.q(var1, var0);
                                            if (var25 != null) {
                                                if (var25.du != null) {
                                                    ScriptEvent var17 = new ScriptEvent();
                                                    var17.i = var25;
                                                    var17.b = var3;
                                                    var17.name = var5;
                                                    var17.args = var25.du;
                                                    m.t(var17, 500000);
                                                }

                                                boolean var11 = true;
                                                if (var25.contentType > 0) {
                                                    var11 = ag.ja(var25);
                                                }

                                                if (var11 && cs.t(u.jr(var25), var3 - 1)) {
                                                    gd var18;
                                                    if (var3 == 1) {
                                                        var18 = ap.t(fo.bc, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 2) {
                                                        var18 = ap.t(fo.bu, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 3) {
                                                        var18 = ap.t(fo.cl, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 4) {
                                                        var18 = ap.t(fo.aw, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 5) {
                                                        var18 = ap.t(fo.bk, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 6) {
                                                        var18 = ap.t(fo.an, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 7) {
                                                        var18 = ap.t(fo.d, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 8) {
                                                        var18 = ap.t(fo.ap, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 9) {
                                                        var18 = ap.t(fo.p, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }

                                                    if (var3 == 10) {
                                                        var18 = ap.t(fo.h, Client.ee.l);
                                                        var18.i.e(var1);
                                                        var18.i.l(var0);
                                                        var18.i.l(var24);
                                                        Client.ee.i(var18);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (Client.inventoryItemSelectionState != 0) {
            Client.inventoryItemSelectionState = 0;
            GameEngine.jk(Inflater.t(bg.ix));
        }

        if (Client.interfaceSelected) {
            kn.hv();
        }

        if (MouseTracker.ie != null && Client.iq == 0) {
            GameEngine.jk(MouseTracker.ie);
        }

    }

    @ObfuscatedName("jn")
    static final void jn(int var0, int var1, int var2, int var3, Sprite var4, iq var5) {
        int var6 = var3 * var3 + var2 * var2;
        if (var6 > 4225 && var6 < 90000) {
            int var7 = Client.hintPlane & 2047;
            int var8 = eu.m[var7];
            int var9 = eu.ay[var7];
            int var10 = var9 * var2 + var3 * var8 >> 16;
            int var11 = var3 * var9 - var8 * var2 >> 16;
            double var12 = Math.atan2((double) var10, (double) var11);
            int var14 = var5.t / 2 - 25;
            int var15 = (int) (Math.sin(var12) * (double) var14);
            int var16 = (int) (Math.cos(var12) * (double) var14);
            byte var17 = 20;
            aq.fw.ax(var15 + (var0 + var5.t / 2 - var17 / 2), var5.q / 2 + var1 - var17 / 2 - var16 - 10, var17, var17,
                    15, 15, var12, 256);
        } else {
            p.jj(var0, var1, var2, var3, var4, var5);
        }

    }
}
