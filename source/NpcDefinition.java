import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jd")
public class NpcDefinition extends hh {

    @ObfuscatedName("t")
    static FileSystem t;

    @ObfuscatedName("q")
    static FileSystem q;

    @ObfuscatedName("i")
    static Cache i = new Cache(64);

    @ObfuscatedName("a")
    static Cache a = new Cache(50);

    @ObfuscatedName("l")
    public int id;

    @ObfuscatedName("b")
    public String name = "null";

    @ObfuscatedName("e")
    public int e = 1;

    @ObfuscatedName("x")
    int[] appearance;

    @ObfuscatedName("p")
    int[] p;

    @ObfuscatedName("g")
    public int g = -1;

    @ObfuscatedName("n")
    public int n = -1;

    @ObfuscatedName("o")
    public int o = -1;

    @ObfuscatedName("c")
    public int c = -1;

    @ObfuscatedName("v")
    public int v = -1;

    @ObfuscatedName("u")
    public int u = -1;

    @ObfuscatedName("j")
    public int j = -1;

    @ObfuscatedName("k")
    short[] baseColors;

    @ObfuscatedName("z")
    short[] texturedColors;

    @ObfuscatedName("w")
    short[] w;

    @ObfuscatedName("s")
    short[] s;

    @ObfuscatedName("d")
    public String[] actions = new String[5];

    @ObfuscatedName("f")
    public boolean f = true;

    @ObfuscatedName("r")
    public int level = -1;

    @ObfuscatedName("y")
    int y = 128;

    @ObfuscatedName("h")
    int h = 128;

    @ObfuscatedName("m")
    public boolean m = false;

    @ObfuscatedName("ay")
    int ay = 0;

    @ObfuscatedName("ao")
    int ao = 0;

    @ObfuscatedName("av")
    public int prayerIconIndex = -1;

    @ObfuscatedName("aj")
    public int aj = 32;

    @ObfuscatedName("ae")
    public int[] ae;

    @ObfuscatedName("am")
    int innerNpcExpressionId = -1;

    @ObfuscatedName("az")
    int innerNpcVarpIndex = -1;

    @ObfuscatedName("ap")
    public boolean ap = true;

    @ObfuscatedName("ah")
    public boolean ah = true;

    @ObfuscatedName("au")
    public boolean au = false;

    @ObfuscatedName("ax")
    FixedSizeDeque ax;

    @ObfuscatedName("i")
    void i() {
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.l(var1, var2);
        }
    }

    @ObfuscatedName("l")
    void l(ByteBuffer var1, int var2) {
        int var3;
        int var4;
        if (var2 == 1) {
            var3 = var1.av();
            this.appearance = new int[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.appearance[var4] = var1.ae();
            }
        } else if (var2 == 2) {
            this.name = var1.ar();
        } else if (var2 == 12) {
            this.e = var1.av();
        } else if (var2 == 13) {
            this.g = var1.ae();
        } else if (var2 == 14) {
            this.c = var1.ae();
        } else if (var2 == 15) {
            this.n = var1.ae();
        } else if (var2 == 16) {
            this.o = var1.ae();
        } else if (var2 == 17) {
            this.c = var1.ae();
            this.v = var1.ae();
            this.u = var1.ae();
            this.j = var1.ae();
        } else if (var2 >= 30 && var2 < 35) {
            this.actions[var2 - 30] = var1.ar();
            if (this.actions[var2 - 30].equalsIgnoreCase("Hidden")) {
                this.actions[var2 - 30] = null;
            }
        } else if (var2 == 40) {
            var3 = var1.av();
            this.baseColors = new short[var3];
            this.texturedColors = new short[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.baseColors[var4] = (short) var1.ae();
                this.texturedColors[var4] = (short) var1.ae();
            }
        } else if (var2 == 41) {
            var3 = var1.av();
            this.w = new short[var3];
            this.s = new short[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.w[var4] = (short) var1.ae();
                this.s[var4] = (short) var1.ae();
            }
        } else if (var2 == 60) {
            var3 = var1.av();
            this.p = new int[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.p[var4] = var1.ae();
            }
        } else if (var2 == 93) {
            this.f = false;
        } else if (var2 == 95) {
            this.level = var1.ae();
        } else if (var2 == 97) {
            this.y = var1.ae();
        } else if (var2 == 98) {
            this.h = var1.ae();
        } else if (var2 == 99) {
            this.m = true;
        } else if (var2 == 100) {
            this.ay = var1.aj();
        } else if (var2 == 101) {
            this.ao = var1.aj() * 5;
        } else if (var2 == 102) {
            this.prayerIconIndex = var1.ae();
        } else if (var2 == 103) {
            this.aj = var1.ae();
        } else if (var2 != 106 && var2 != 118) {
            if (var2 == 107) {
                this.ap = false;
            } else if (var2 == 109) {
                this.ah = false;
            } else if (var2 == 111) {
                this.au = true;
            } else if (var2 == 249) {
                this.ax = i.t(var1, this.ax);
            }
        } else {
            this.innerNpcExpressionId = var1.ae();
            if (this.innerNpcExpressionId == 65535) {
                this.innerNpcExpressionId = -1;
            }

            this.innerNpcVarpIndex = var1.ae();
            if (this.innerNpcVarpIndex == 65535) {
                this.innerNpcVarpIndex = -1;
            }

            var3 = -1;
            if (var2 == 118) {
                var3 = var1.ae();
                if (var3 == 65535) {
                    var3 = -1;
                }
            }

            var4 = var1.av();
            this.ae = new int[var4 + 2];

            for (int var5 = 0; var5 <= var4; ++var5) {
                this.ae[var5] = var1.ae();
                if (this.ae[var5] == 65535) {
                    this.ae[var5] = -1;
                }
            }

            this.ae[var4 + 1] = var3;
        }

    }

    @ObfuscatedName("b")
    public final Model b(kf var1, int var2, kf var3, int var4) {
        if (this.ae != null) {
            NpcDefinition var12 = this.x();
            return var12 == null ? null : var12.b(var1, var2, var3, var4);
        } else {
            Model var5 = (Model) a.t((long) this.id);
            if (var5 == null) {
                boolean var6 = false;

                for (int var7 = 0; var7 < this.appearance.length; ++var7) {
                    if (!q.l(this.appearance[var7], 0)) {
                        var6 = true;
                    }
                }

                if (var6) {
                    return null;
                }

                AlternativeModel[] var8 = new AlternativeModel[this.appearance.length];

                int var9;
                for (var9 = 0; var9 < this.appearance.length; ++var9) {
                    var8[var9] = AlternativeModel.t(q, this.appearance[var9], 0);
                }

                AlternativeModel var11;
                if (var8.length == 1) {
                    var11 = var8[0];
                } else {
                    var11 = new AlternativeModel(var8, var8.length);
                }

                if (this.baseColors != null) {
                    for (var9 = 0; var9 < this.baseColors.length; ++var9) {
                        var11.z(this.baseColors[var9], this.texturedColors[var9]);
                    }
                }

                if (this.w != null) {
                    for (var9 = 0; var9 < this.w.length; ++var9) {
                        var11.w(this.w[var9], this.s[var9]);
                    }
                }

                var5 = var11.av(this.ay + 64, this.ao + 850, -30, -50, -30);
                a.i(var5, (long) this.id);
            }

            Model var10;
            if (var1 != null && var3 != null) {
                var10 = var1.x(var5, var2, var3, var4);
            } else if (var1 != null) {
                var10 = var1.l(var5, var2);
            } else if (var3 != null) {
                var10 = var3.l(var5, var4);
            } else {
                var10 = var5.q(true);
            }

            if (this.y != 128 || this.h != 128) {
                var10.r(this.y, this.h, this.y);
            }

            return var10;
        }
    }

    @ObfuscatedName("e")
    public final AlternativeModel e() {
        if (this.ae != null) {
            NpcDefinition var1 = this.x();
            return var1 == null ? null : var1.e();
        } else if (this.p == null) {
            return null;
        } else {
            boolean var5 = false;

            for (int var2 = 0; var2 < this.p.length; ++var2) {
                if (!q.l(this.p[var2], 0)) {
                    var5 = true;
                }
            }

            if (var5) {
                return null;
            } else {
                AlternativeModel[] var6 = new AlternativeModel[this.p.length];

                for (int var3 = 0; var3 < this.p.length; ++var3) {
                    var6[var3] = AlternativeModel.t(q, this.p[var3], 0);
                }

                AlternativeModel var7;
                if (var6.length == 1) {
                    var7 = var6[0];
                } else {
                    var7 = new AlternativeModel(var6, var6.length);
                }

                int var4;
                if (this.baseColors != null) {
                    for (var4 = 0; var4 < this.baseColors.length; ++var4) {
                        var7.z(this.baseColors[var4], this.texturedColors[var4]);
                    }
                }

                if (this.w != null) {
                    for (var4 = 0; var4 < this.w.length; ++var4) {
                        var7.w(this.w[var4], this.s[var4]);
                    }
                }

                return var7;
            }
        }
    }

    @ObfuscatedName("x")
    public final NpcDefinition x() {
        int var1 = -1;
        if (this.innerNpcExpressionId != -1) {
            var1 = Server.t(this.innerNpcExpressionId);
        } else if (this.innerNpcVarpIndex != -1) {
            var1 = iv.varps[this.innerNpcVarpIndex];
        }

        int var2;
        if (var1 >= 0 && var1 < this.ae.length - 1) {
            var2 = this.ae[var1];
        } else {
            var2 = this.ae[this.ae.length - 1];
        }

        return var2 != -1 ? ho.q(var2) : null;
    }

    @ObfuscatedName("p")
    public boolean p() {
        if (this.ae == null) {
            return true;
        } else {
            int var1 = -1;
            if (this.innerNpcExpressionId != -1) {
                var1 = Server.t(this.innerNpcExpressionId);
            } else if (this.innerNpcVarpIndex != -1) {
                var1 = iv.varps[this.innerNpcVarpIndex];
            }

            if (var1 >= 0 && var1 < this.ae.length) {
                return this.ae[var1] != -1;
            } else {
                return this.ae[this.ae.length - 1] != -1;
            }
        }
    }

    @ObfuscatedName("o")
    public int o(int var1, int var2) {
        return ky.q(this.ax, var1, var2);
    }

    @ObfuscatedName("c")
    public String c(int var1, String var2) {
        FixedSizeDeque var4 = this.ax;
        String var3;
        if (var4 == null) {
            var3 = var2;
        } else {
            gy var5 = (gy) var4.t((long) var1);
            if (var5 == null) {
                var3 = var2;
            } else {
                var3 = (String) var5.t;
            }
        }

        return var3;
    }
}
