import java.awt.Font;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("r")
public class r extends av {

    @ObfuscatedName("ps")
    static int ps;

    @ObfuscatedName("o")
    static int[] o;

    @ObfuscatedName("aj")
    static Font aj;

    @ObfuscatedName("t")
    void t(ByteBuffer var1, ByteBuffer var2) {
        int var3 = var2.av();
        if (var3 != ar.t.i) {
            throw new IllegalStateException("");
        } else {
            super.l = var2.av();
            super.b = var2.av();
            super.t = var2.ae();
            super.q = var2.ae();
            super.i = var2.ae();
            super.a = var2.ae();
            super.b = Math.min(super.b, 4);
            super.e = new short[1][64][64];
            super.x = new short[super.b][64][64];
            super.p = new byte[super.b][64][64];
            super.g = new byte[super.b][64][64];
            super.n = new am[super.b][64][64][];
            var3 = var1.av();
            if (var3 != ax.t.i) {
                throw new IllegalStateException("");
            } else {
                int var4 = var1.av();
                int var5 = var1.av();
                if (var4 == super.i && var5 == super.a) {
                    for (int var6 = 0; var6 < 64; ++var6) {
                        for (int var7 = 0; var7 < 64; ++var7) {
                            this.b(var6, var7, var1);
                        }
                    }

                } else {
                    throw new IllegalStateException("");
                }
            }
        }
    }

    public int hashCode() {
        return super.i | super.a << 8;
    }

    public boolean equals(Object var1) {
        if (!(var1 instanceof r)) {
            return false;
        } else {
            r var2 = (r) var1;
            return super.i == var2.i && var2.a == super.a;
        }
    }

    @ObfuscatedName("ju")
    static final void ju(RTComponent[] var0, int var1) {
        for (int var2 = 0; var2 < var0.length; ++var2) {
            RTComponent var3 = var0[var2];
            if (var3 != null && var3.parentId == var1) {
                boolean var4;
                if (var3.modern) {
                    var4 = var3.hidden;
                    if (var4) {
                        continue;
                    }
                }

                int var5;
                if (var3.type == 0) {
                    if (!var3.modern) {
                        var4 = var3.hidden;
                        if (var4 && var3 != av.kp) {
                            continue;
                        }
                    }

                    ju(var0, var3.id);
                    if (var3.cs2components != null) {
                        ju(var3.cs2components, var3.id);
                    }

                    SubWindow var7 = (SubWindow) Client.subWindowTable.t((long) var3.id);
                    if (var7 != null) {
                        var5 = var7.targetWindowId;
                        if (RuneScript.i(var5)) {
                            ju(RTComponent.b[var5], -1);
                        }
                    }
                }

                if (var3.type == 6) {
                    if (var3.animationId != -1 || var3.bb != -1) {
                        var4 = bc.id(var3);
                        if (var4) {
                            var5 = var3.bb;
                        } else {
                            var5 = var3.animationId;
                        }

                        if (var5 != -1) {
                            kf var6 = ff.t(var5);

                            for (var3.ev += Client.fs; var3.ev > var6.x[var3.et]; GameEngine.jk(var3)) {
                                var3.ev -= var6.x[var3.et];
                                ++var3.et;
                                if (var3.et >= var6.b.length) {
                                    var3.et -= var6.g;
                                    if (var3.et < 0 || var3.et >= var6.b.length) {
                                        var3.et = 0;
                                    }
                                }
                            }
                        }
                    }

                    if (var3.bu != 0 && !var3.modern) {
                        int var8 = var3.bu >> 16;
                        var5 = var3.bu << 16 >> 16;
                        var8 *= Client.fs;
                        var5 *= Client.fs;
                        var3.bx = var8 + var3.bx & 2047;
                        var3.bf = var5 + var3.bf & 2047;
                        GameEngine.jk(var3);
                    }
                }
            }
        }

    }
}
