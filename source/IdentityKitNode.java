import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("et")
public class IdentityKitNode extends hh {

    @ObfuscatedName("t")
    IdentityKit[] kits;

    IdentityKitNode(FileSystem var1, FileSystem var2, int var3, boolean var4) {
        Deque var5 = new Deque();
        int var6 = var1.w(var3);
        this.kits = new IdentityKit[var6];
        int[] var7 = var1.z(var3);

        for (int var8 = 0; var8 < var7.length; ++var8) {
            byte[] var9 = var1.i(var3, var7[var8]);
            Skins var10 = null;
            int var11 = (var9[0] & 255) << 8 | var9[1] & 255;

            for (Skins var12 = (Skins) var5.e(); var12 != null; var12 = (Skins) var5.p()) {
                if (var11 == var12.t) {
                    var10 = var12;
                    break;
                }
            }

            if (var10 == null) {
                byte[] var13;
                if (var4) {
                    var13 = var2.c(0, var11);
                } else {
                    var13 = var2.c(var11, 0);
                }

                var10 = new Skins(var11, var13);
                var5.q(var10);
            }

            this.kits[var7[var8]] = new IdentityKit(var9, var10);
        }

    }

    @ObfuscatedName("q")
    public boolean q(int var1) {
        return this.kits[var1].female;
    }

    @ObfuscatedName("a")
    static void a(int var0, int var1) {
        long var2 = (long) ((var0 << 16) + var1);
        jk var4 = (jk) jg.p.t(var2);
        if (var4 != null) {
            jg.x.q(var4);
        }
    }
}
