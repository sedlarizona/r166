import java.io.File;
import java.math.BigInteger;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cr")
public class cr {

    @ObfuscatedName("l")
    static final BigInteger l = new BigInteger(
            "80782894952180643741752986186714059433953886149239752893425047584684715842049");

    @ObfuscatedName("b")
    static final BigInteger b = new BigInteger(
            "7237300117305667488707183861728052766358166655052137727439795191253340127955075499635575104901523446809299097934591732635674173519120047404024393881551683");

    @ObfuscatedName("n")
    public static boolean n;

    @ObfuscatedName("cd")
    public static int cd;

    @ObfuscatedName("du")
    static int du;

    @ObfuscatedName("t")
    public static void t(File var0) {
        fw.q = var0;
        if (!fw.q.exists()) {
            throw new RuntimeException("");
        } else {
            fw.t = true;
        }
    }
}
