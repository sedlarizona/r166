import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ao")
public enum ao implements gl {
    @ObfuscatedName("t")
    t(1, (byte) 0), @ObfuscatedName("q")
    q(0, (byte) 1), @ObfuscatedName("i")
    i(3, (byte) 2), @ObfuscatedName("a")
    a(2, (byte) 3);

    @ObfuscatedName("pr")
    static int pr;

    @ObfuscatedName("ao")
    static int[] ao;

    @ObfuscatedName("l")
    final int l;

    @ObfuscatedName("b")
    final byte b;

    ao(int var3, byte var4) {
        this.l = var3;
        this.b = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.b;
    }

    @ObfuscatedName("t")
    static void t() {
        bt.a = null;
        bt.l = null;
        bt.b = null;
        Skins.e = null;
        bt.u = null;
        bt.x = null;
        bt.p = null;
        g.g = null;
        ax.n = null;
        r.o = null;
        gn.c = null;
        ax.v = null;
    }

    @ObfuscatedName("jx")
    static void jx(int var0) {
        for (hc var1 = (hc) Client.mf.a(); var1 != null; var1 = (hc) Client.mf.l()) {
            if ((long) var0 == (var1.uid >> 48 & 65535L)) {
                var1.kc();
            }
        }

    }
}
