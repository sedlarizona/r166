import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ax")
public class ax {
   @ObfuscatedName("t")
   static final ax t = new ax(0);
   @ObfuscatedName("q")
   static final ax q = new ax(1);
   @ObfuscatedName("n")
   static int[] n;
   @ObfuscatedName("v")
   static int[] v;
   @ObfuscatedName("i")
   final int i;

   ax(int var1) {
      this.i = var1;
   }

   @ObfuscatedName("if")
   static final String if(int var0) {
      String var1 = Integer.toString(var0);

      for(int var2 = var1.length() - 3; var2 > 0; var2 -= 3) {
         var1 = var1.substring(0, var2) + "," + var1.substring(var2);
      }

      if (var1.length() > 9) {
         return " " + ar.q(65408) + var1.substring(0, var1.length() - 8) + "M" + " " + " (" + var1 + ")" + "</col>";
      } else {
         return var1.length() > 6 ? " " + ar.q(16777215) + var1.substring(0, var1.length() - 4) + "K" + " " + " (" + var1 + ")" + "</col>" : " " + ar.q(16776960) + var1 + "</col>";
      }
   }
}
