import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cf")
public interface cf {

    @ObfuscatedName("t")
    TaskData t();
}
