import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("f")
public class f {

    @ObfuscatedName("pu")
    static int pu;

    @ObfuscatedName("i")
    static FileSystem i;

    @ObfuscatedName("fa")
    static Sprite[] fa;

    @ObfuscatedName("q")
    static int q(int var0, int var1, int var2) {
        if (var2 > 179) {
            var1 /= 2;
        }

        if (var2 > 192) {
            var1 /= 2;
        }

        if (var2 > 217) {
            var1 /= 2;
        }

        if (var2 > 243) {
            var1 /= 2;
        }

        int var3 = (var1 / 32 << 7) + (var0 / 4 << 10) + var2 / 2;
        return var3;
    }

    @ObfuscatedName("a")
    static void setContainer(int var0, int var1, int var2, int var3) {
        ItemStorage var4 = (ItemStorage) ItemStorage.t.t((long) var0);
        if (var4 == null) {
            var4 = new ItemStorage();
            ItemStorage.t.q(var4, (long) var0);
        }

        if (var4.ids.length <= var1) {
            int[] var5 = new int[var1 + 1];
            int[] var6 = new int[var1 + 1];

            int var7;
            for (var7 = 0; var7 < var4.ids.length; ++var7) {
                var5[var7] = var4.ids[var7];
                var6[var7] = var4.stackSizes[var7];
            }

            for (var7 = var4.ids.length; var7 < var1; ++var7) {
                var5[var7] = -1;
                var6[var7] = 0;
            }

            var4.ids = var5;
            var4.stackSizes = var6;
        }

        var4.ids[var1] = var2;
        var4.stackSizes[var1] = var3;
    }

    @ObfuscatedName("fe")
    static void fe() {
        Client.ee.t();
        Client.ee.b.index = 0;
        Client.ee.e = null;
        Client.ee.o = null;
        Client.ee.c = null;
        Client.ee.v = null;
        Client.ee.x = 0;
        Client.ee.g = 0;
        Client.mouseTrackerIdleTime = 0;
        z.hx();
        Client.oo = 0;
        Client.destinationX = 0;

        int var0;
        for (var0 = 0; var0 < 2048; ++var0) {
            Client.loadedPlayers[var0] = null;
        }

        az.il = null;

        for (var0 = 0; var0 < Client.loadedNpcs.length; ++var0) {
            Npc var1 = Client.loadedNpcs[var0];
            if (var1 != null) {
                var1.interactingEntityIndex = -1;
                var1.bm = false;
            }
        }

        ec.e();
        d.ea(30);

        for (var0 = 0; var0 < 100; ++var0) {
            Client.outdatedInterfaces[var0] = true;
        }

        gd var2 = ap.t(fo.af, Client.ee.l);
        var2.i.a(bg.fq());
        var2.i.l(BoundaryObject.r);
        var2.i.l(GameEngine.h);
        Client.ee.i(var2);
    }

    @ObfuscatedName("ga")
    static final void ga(String var0, boolean var1) {
        if (Client.ir) {
            byte var2 = 4;
            int var3 = var2 + 6;
            int var4 = var2 + 6;
            int var5 = fv.et.k(var0, 250);
            int var6 = fv.et.z(var0, 250) * 13;
            li.dl(var3 - var2, var4 - var2, var5 + var2 + var2, var2 + var2 + var6, 0);
            li.dw(var3 - var2, var4 - var2, var2 + var2 + var5, var2 + var6 + var2, 16777215);
            fv.et.y(var0, var3, var4, var5, var6, 16777215, -1, 1, 1, 0);
            Canvas.hw(var3 - var2, var4 - var2, var2 + var2 + var5, var2 + var2 + var6);
            if (var1) {
                ad.interfaceProducer.q(0, 0);
            } else {
                ItemStorage.hn(var3, var4, var5, var6);
            }

        }
    }

    @ObfuscatedName("jl")
    static String jl(RTComponent var0) {
        int var2 = u.jr(var0);
        int var1 = var2 >> 11 & 63;
        if (var1 == 0) {
            return null;
        } else {
            return var0.cn != null && var0.cn.trim().length() != 0 ? var0.cn : null;
        }
    }
}
