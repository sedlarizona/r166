import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kj")
public class kj extends ku {

    @ObfuscatedName("i")
    final lu i;

    public kj(lu var1) {
        super(400);
        this.i = var1;
    }

    @ObfuscatedName("t")
    kv t() {
        return new kt();
    }

    @ObfuscatedName("q")
    kv[] q(int var1) {
        return new kt[var1];
    }

    @ObfuscatedName("i")
    public void i(ByteBuffer var1, int var2) {
        while (true) {
            if (var1.index < var2) {
                int var3 = var1.av();
                boolean var4 = (var3 & 1) == 1;
                kb var5 = new kb(var1.ar(), this.i);
                kb var6 = new kb(var1.ar(), this.i);
                var1.ar();
                if (var5 != null && var5.q()) {
                    kt var7 = (kt) this.s(var4 ? var6 : var5);
                    if (var7 != null) {
                        this.ae(var7, var5, var6);
                        continue;
                    }

                    if (this.u() < 400) {
                        int var8 = this.u();
                        var7 = (kt) this.h(var5, var6);
                        var7.t = var8;
                    }
                    continue;
                }

                throw new IllegalStateException();
            }

            return;
        }
    }

    @ObfuscatedName("t")
    static gu[] t() {
        return new gu[] { gu.t, gu.i, gu.q, gu.a };
    }
}
