import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bz")
public class SubWindow extends Node {

    @ObfuscatedName("bf")
    static MouseTracker bf;

    @ObfuscatedName("t")
    int targetWindowId;

    @ObfuscatedName("q")
    int type;

    @ObfuscatedName("i")
    boolean root = false;

    @ObfuscatedName("a")
    static int a(int var0, RuneScript var1, boolean var2) {
        int var3 = -1;
        RTComponent var4;
        if (var0 >= 2000) {
            var0 -= 1000;
            var3 = cs.e[--b.menuX];
            var4 = Inflater.t(var3);
        } else {
            var4 = var2 ? ho.v : cs.c;
        }

        if (var0 == 1000) {
            b.menuX -= 4;
            var4.ay = cs.e[b.menuX];
            var4.ao = cs.e[b.menuX + 1];
            var4.r = cs.e[b.menuX + 2];
            var4.y = cs.e[b.menuX + 3];
            GameEngine.jk(var4);
            ir.ac.io(var4);
            if (var3 != -1 && var4.type == 0) {
                GameEngine.ig(RTComponent.b[var3 >> 16], var4, false);
            }

            return 1;
        } else if (var0 == 1001) {
            b.menuX -= 4;
            var4.av = cs.e[b.menuX];
            var4.aj = cs.e[b.menuX + 1];
            var4.h = cs.e[b.menuX + 2];
            var4.m = cs.e[b.menuX + 3];
            GameEngine.jk(var4);
            ir.ac.io(var4);
            if (var3 != -1 && var4.type == 0) {
                GameEngine.ig(RTComponent.b[var3 >> 16], var4, false);
            }

            return 1;
        } else if (var0 == 1003) {
            boolean var5 = cs.e[--b.menuX] == 1;
            if (var5 != var4.hidden) {
                var4.hidden = var5;
                GameEngine.jk(var4);
            }

            return 1;
        } else if (var0 == 1005) {
            var4.er = cs.e[--b.menuX] == 1;
            return 1;
        } else if (var0 == 1006) {
            var4.eh = cs.e[--b.menuX] == 1;
            return 1;
        } else {
            return 2;
        }
    }
}
