import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cl")
public class AnimableNode extends Renderable {

    @ObfuscatedName("t")
    int id;

    @ObfuscatedName("q")
    int type;

    @ObfuscatedName("i")
    int orientation;

    @ObfuscatedName("a")
    int plane;

    @ObfuscatedName("l")
    int regionX;

    @ObfuscatedName("b")
    int regionY;

    @ObfuscatedName("e")
    kf animationSequence;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    AnimableNode(int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8, Renderable var9) {
        this.id = var1;
        this.type = var2;
        this.orientation = var3;
        this.plane = var4;
        this.regionX = var5;
        this.regionY = var6;
        if (var7 != -1) {
            this.animationSequence = ff.t(var7);
            this.x = 0;
            this.p = Client.bz - 1;
            if (this.animationSequence.w == 0 && var9 != null && var9 instanceof AnimableNode) {
                AnimableNode var10 = (AnimableNode) var9;
                if (this.animationSequence == var10.animationSequence) {
                    this.x = var10.x;
                    this.p = var10.p;
                    return;
                }
            }

            if (var8 && this.animationSequence.g != -1) {
                this.x = (int) (Math.random() * (double) this.animationSequence.b.length);
                this.p -= (int) (Math.random() * (double) this.animationSequence.x[this.x]);
            }
        }

    }

    @ObfuscatedName("p")
    protected final Model p() {
        if (this.animationSequence != null) {
            int var1 = Client.bz - this.p;
            if (var1 > 100 && this.animationSequence.g > 0) {
                var1 = 100;
            }

            label56: {
                do {
                    do {
                        if (var1 <= this.animationSequence.x[this.x]) {
                            break label56;
                        }

                        var1 -= this.animationSequence.x[this.x];
                        ++this.x;
                    } while (this.x < this.animationSequence.b.length);

                    this.x -= this.animationSequence.g;
                } while (this.x >= 0 && this.x < this.animationSequence.b.length);

                this.animationSequence = null;
            }

            this.p = Client.bz - var1;
        }

        ObjectDefinition var12 = jw.q(this.id);
        if (var12.innerObjectIds != null) {
            var12 = var12.u();
        }

        if (var12 == null) {
            return null;
        } else {
            int var2;
            int var3;
            if (this.orientation != 1 && this.orientation != 3) {
                var2 = var12.width;
                var3 = var12.height;
            } else {
                var2 = var12.height;
                var3 = var12.width;
            }

            int var4 = (var2 >> 1) + this.regionX;
            int var5 = (var2 + 1 >> 1) + this.regionX;
            int var6 = (var3 >> 1) + this.regionY;
            int var7 = (var3 + 1 >> 1) + this.regionY;
            int[][] var8 = bt.tileHeights[this.plane];
            int var9 = var8[var5][var7] + var8[var5][var6] + var8[var4][var6] + var8[var4][var7] >> 2;
            int var10 = (this.regionX << 7) + (var2 << 6);
            int var11 = (this.regionY << 7) + (var3 << 6);
            return var12.o(this.type, this.orientation, var8, var10, var9, var11, this.animationSequence, this.x);
        }
    }

    @ObfuscatedName("fh")
    static void fh(int var0, int var1, int var2) {
        if (Client.ow != 0 && var1 != 0 && Client.audioEffectCount < 50) {
            Client.po[Client.audioEffectCount] = var0;
            Client.pe[Client.audioEffectCount] = var1;
            Client.pt[Client.audioEffectCount] = var2;
            Client.audioTracks[Client.audioEffectCount] = null;
            Client.pl[Client.audioEffectCount] = 0;
            ++Client.audioEffectCount;
        }

    }
}
