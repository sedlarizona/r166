import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cb")
public class ClientPreferences {

    @ObfuscatedName("t")
    static int t = 6;

    @ObfuscatedName("i")
    boolean roofsDisabled;

    @ObfuscatedName("a")
    boolean loadingAudioDisabled;

    @ObfuscatedName("l")
    int l = 1;

    @ObfuscatedName("b")
    String b = null;

    @ObfuscatedName("e")
    boolean e = false;

    @ObfuscatedName("x")
    LinkedHashMap map = new LinkedHashMap();

    ClientPreferences() {
        this.t(true);
    }

    ClientPreferences(ByteBuffer var1) {
        if (var1 != null && var1.buffer != null) {
            int var2 = var1.av();
            if (var2 >= 0 && var2 <= t) {
                if (var1.av() == 1) {
                    this.roofsDisabled = true;
                }

                if (var2 > 1) {
                    this.loadingAudioDisabled = var1.av() == 1;
                }

                if (var2 > 3) {
                    this.l = var1.av();
                }

                if (var2 > 2) {
                    int var3 = var1.av();

                    for (int var4 = 0; var4 < var3; ++var4) {
                        int var5 = var1.ap();
                        int var6 = var1.ap();
                        this.map.put(var5, var6);
                    }
                }

                if (var2 > 4) {
                    this.b = var1.ax();
                }

                if (var2 > 5) {
                    this.e = var1.au();
                }
            } else {
                this.t(true);
            }
        } else {
            this.t(true);
        }

    }

    @ObfuscatedName("t")
    void t(boolean var1) {
    }

    @ObfuscatedName("q")
    ByteBuffer q() {
        ByteBuffer var1 = new ByteBuffer(100);
        var1.a(t);
        var1.a(this.roofsDisabled ? 1 : 0);
        var1.a(this.loadingAudioDisabled ? 1 : 0);
        var1.a(this.l);
        var1.a(this.map.size());
        Iterator var2 = this.map.entrySet().iterator();

        while (var2.hasNext()) {
            Entry var3 = (Entry) var2.next();
            var1.e((Integer) var3.getKey());
            var1.e((Integer) var3.getValue());
        }

        var1.u(this.b != null ? this.b : "");
        var1.o(this.e);
        return var1;
    }

    @ObfuscatedName("t")
    public static final void t(int var0, int var1) {
        ej.q = var0;
        ej.i = var1;
        ej.t = true;
        ej.v = 0;
        ej.a = false;
    }

    @ObfuscatedName("p")
    public static byte[] p(Object var0, boolean var1) {
        if (var0 == null) {
            return null;
        } else if (var0 instanceof byte[]) {
            byte[] var6 = (byte[]) ((byte[]) var0);
            if (var1) {
                int var4 = var6.length;
                byte[] var5 = new byte[var4];
                System.arraycopy(var6, 0, var5, 0, var4);
                return var5;
            } else {
                return var6;
            }
        } else if (var0 instanceof gr) {
            gr var2 = (gr) var0;
            return var2.t();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @ObfuscatedName("d")
    static int d(int var0, RuneScript var1, boolean var2) {
        if (var0 == 3600) {
            if (BoundaryObject.qi.e == 0) {
                cs.e[++b.menuX - 1] = -2;
            } else if (BoundaryObject.qi.e == 1) {
                cs.e[++b.menuX - 1] = -1;
            } else {
                cs.e[++b.menuX - 1] = BoundaryObject.qi.l.u();
            }

            return 1;
        } else {
            int var3;
            if (var0 == 3601) {
                var3 = cs.e[--b.menuX];
                if (BoundaryObject.qi.t() && var3 >= 0 && var3 < BoundaryObject.qi.l.u()) {
                    ks var10 = (ks) BoundaryObject.qi.l.av(var3);
                    cs.p[++ly.g - 1] = var10.y();
                    cs.p[++ly.g - 1] = var10.h();
                } else {
                    cs.p[++ly.g - 1] = "";
                    cs.p[++ly.g - 1] = "";
                }

                return 1;
            } else if (var0 == 3602) {
                var3 = cs.e[--b.menuX];
                if (BoundaryObject.qi.t() && var3 >= 0 && var3 < BoundaryObject.qi.l.u()) {
                    cs.e[++b.menuX - 1] = ((kp) BoundaryObject.qi.l.av(var3)).l;
                } else {
                    cs.e[++b.menuX - 1] = 0;
                }

                return 1;
            } else if (var0 == 3603) {
                var3 = cs.e[--b.menuX];
                if (BoundaryObject.qi.t() && var3 >= 0 && var3 < BoundaryObject.qi.l.u()) {
                    cs.e[++b.menuX - 1] = ((kp) BoundaryObject.qi.l.av(var3)).e;
                } else {
                    cs.e[++b.menuX - 1] = 0;
                }

                return 1;
            } else {
                String var6;
                if (var0 == 3604) {
                    var6 = cs.p[--ly.g];
                    int var7 = cs.e[--b.menuX];
                    gd var5 = ap.t(fo.br, Client.ee.l);
                    var5.i.a(z.c(var6) + 1);
                    var5.i.u(var6);
                    var5.i.bk(var7);
                    Client.ee.i(var5);
                    return 1;
                } else if (var0 == 3605) {
                    var6 = cs.p[--ly.g];
                    BoundaryObject.qi.x(var6);
                    return 1;
                } else if (var0 == 3606) {
                    var6 = cs.p[--ly.g];
                    BoundaryObject.qi.r(var6);
                    return 1;
                } else if (var0 == 3607) {
                    var6 = cs.p[--ly.g];
                    BoundaryObject.qi.z(var6);
                    return 1;
                } else if (var0 == 3608) {
                    var6 = cs.p[--ly.g];
                    BoundaryObject.qi.y(var6);
                    return 1;
                } else if (var0 == 3609) {
                    var6 = cs.p[--ly.g];
                    var6 = ScriptEvent.ka(var6);
                    cs.e[++b.menuX - 1] = BoundaryObject.qi.b(new kb(var6, ad.bc), false) ? 1 : 0;
                    return 1;
                } else if (var0 == 3611) {
                    if (ad.ot != null) {
                        cs.p[++ly.g - 1] = ad.ot.g;
                    } else {
                        cs.p[++ly.g - 1] = "";
                    }

                    return 1;
                } else if (var0 == 3612) {
                    if (ad.ot != null) {
                        cs.e[++b.menuX - 1] = ad.ot.u();
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                } else if (var0 == 3613) {
                    var3 = cs.e[--b.menuX];
                    if (ad.ot != null && var3 < ad.ot.u()) {
                        cs.p[++ly.g - 1] = ad.ot.av(var3).r().t();
                    } else {
                        cs.p[++ly.g - 1] = "";
                    }

                    return 1;
                } else if (var0 == 3614) {
                    var3 = cs.e[--b.menuX];
                    if (ad.ot != null && var3 < ad.ot.u()) {
                        cs.e[++b.menuX - 1] = ((kp) ad.ot.av(var3)).bg();
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                } else if (var0 == 3615) {
                    var3 = cs.e[--b.menuX];
                    if (ad.ot != null && var3 < ad.ot.u()) {
                        cs.e[++b.menuX - 1] = ((kp) ad.ot.av(var3)).e;
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                } else if (var0 == 3616) {
                    cs.e[++b.menuX - 1] = ad.ot != null ? ad.ot.o : 0;
                    return 1;
                } else if (var0 == 3617) {
                    var6 = cs.p[--ly.g];
                    bq.jf(var6);
                    return 1;
                } else if (var0 == 3618) {
                    cs.e[++b.menuX - 1] = ad.ot != null ? ad.ot.c : 0;
                    return 1;
                } else if (var0 == 3619) {
                    var6 = cs.p[--ly.g];
                    if (!var6.equals("")) {
                        gd var9 = ap.t(fo.by, Client.ee.l);
                        var9.i.a(z.c(var6));
                        var9.i.u(var6);
                        Client.ee.i(var9);
                    }

                    return 1;
                } else if (var0 == 3620) {
                    RuneScriptVM.jb();
                    return 1;
                } else if (var0 == 3621) {
                    if (!BoundaryObject.qi.t()) {
                        cs.e[++b.menuX - 1] = -1;
                    } else {
                        cs.e[++b.menuX - 1] = BoundaryObject.qi.b.u();
                    }

                    return 1;
                } else if (var0 == 3622) {
                    var3 = cs.e[--b.menuX];
                    if (BoundaryObject.qi.t() && var3 >= 0 && var3 < BoundaryObject.qi.b.u()) {
                        kt var4 = (kt) BoundaryObject.qi.b.av(var3);
                        cs.p[++ly.g - 1] = var4.y();
                        cs.p[++ly.g - 1] = var4.h();
                    } else {
                        cs.p[++ly.g - 1] = "";
                        cs.p[++ly.g - 1] = "";
                    }

                    return 1;
                } else if (var0 == 3623) {
                    var6 = cs.p[--ly.g];
                    var6 = ScriptEvent.ka(var6);
                    cs.e[++b.menuX - 1] = BoundaryObject.qi.e(new kb(var6, ad.bc)) ? 1 : 0;
                    return 1;
                } else if (var0 == 3624) {
                    var3 = cs.e[--b.menuX];
                    if (ad.ot != null && var3 < ad.ot.u() && ad.ot.av(var3).r().equals(az.il.t)) {
                        cs.e[++b.menuX - 1] = 1;
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                } else if (var0 == 3625) {
                    if (ad.ot != null && ad.ot.n != null) {
                        cs.p[++ly.g - 1] = ad.ot.n;
                    } else {
                        cs.p[++ly.g - 1] = "";
                    }

                    return 1;
                } else if (var0 == 3626) {
                    var3 = cs.e[--b.menuX];
                    if (ad.ot != null && var3 < ad.ot.u() && ((ki) ad.ot.av(var3)).q()) {
                        cs.e[++b.menuX - 1] = 1;
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                } else if (var0 != 3627) {
                    if (var0 == 3628) {
                        BoundaryObject.qi.l.ax();
                        return 1;
                    } else {
                        boolean var8;
                        if (var0 == 3629) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new lh(var8));
                            return 1;
                        } else if (var0 == 3630) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new lc(var8));
                            return 1;
                        } else if (var0 == 3631) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new ex(var8));
                            return 1;
                        } else if (var0 == 3632) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new ef(var8));
                            return 1;
                        } else if (var0 == 3633) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new eh(var8));
                            return 1;
                        } else if (var0 == 3634) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new fp(var8));
                            return 1;
                        } else if (var0 == 3635) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new er(var8));
                            return 1;
                        } else if (var0 == 3636) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new ea(var8));
                            return 1;
                        } else if (var0 == 3637) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new eq(var8));
                            return 1;
                        } else if (var0 == 3638) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new em(var8));
                            return 1;
                        } else if (var0 == 3639) {
                            BoundaryObject.qi.l.aj();
                            return 1;
                        } else if (var0 == 3640) {
                            BoundaryObject.qi.b.ax();
                            return 1;
                        } else if (var0 == 3641) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.b.ar(new lh(var8));
                            return 1;
                        } else if (var0 == 3642) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.b.ar(new lc(var8));
                            return 1;
                        } else if (var0 == 3643) {
                            BoundaryObject.qi.b.aj();
                            return 1;
                        } else if (var0 == 3644) {
                            if (ad.ot != null) {
                                ad.ot.ax();
                            }

                            return 1;
                        } else if (var0 == 3645) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new lh(var8));
                            }

                            return 1;
                        } else if (var0 == 3646) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new lc(var8));
                            }

                            return 1;
                        } else if (var0 == 3647) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new ex(var8));
                            }

                            return 1;
                        } else if (var0 == 3648) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new ef(var8));
                            }

                            return 1;
                        } else if (var0 == 3649) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new eh(var8));
                            }

                            return 1;
                        } else if (var0 == 3650) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new fp(var8));
                            }

                            return 1;
                        } else if (var0 == 3651) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new er(var8));
                            }

                            return 1;
                        } else if (var0 == 3652) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new ea(var8));
                            }

                            return 1;
                        } else if (var0 == 3653) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new eq(var8));
                            }

                            return 1;
                        } else if (var0 == 3654) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new em(var8));
                            }

                            return 1;
                        } else if (var0 == 3655) {
                            if (ad.ot != null) {
                                ad.ot.aj();
                            }

                            return 1;
                        } else if (var0 == 3656) {
                            var8 = cs.e[--b.menuX] == 1;
                            BoundaryObject.qi.l.ar(new fg(var8));
                            return 1;
                        } else if (var0 == 3657) {
                            var8 = cs.e[--b.menuX] == 1;
                            if (ad.ot != null) {
                                ad.ot.ar(new fg(var8));
                            }

                            return 1;
                        } else {
                            return 2;
                        }
                    }
                } else {
                    var3 = cs.e[--b.menuX];
                    if (ad.ot != null && var3 < ad.ot.u() && ((ki) ad.ot.av(var3)).l()) {
                        cs.e[++b.menuX - 1] = 1;
                    } else {
                        cs.e[++b.menuX - 1] = 0;
                    }

                    return 1;
                }
            }
        }
    }
}
