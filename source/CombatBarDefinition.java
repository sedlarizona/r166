import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jb")
public class CombatBarDefinition extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("i")
    public static Cache i = new Cache(64);

    @ObfuscatedName("a")
    public static Cache a = new Cache(64);

    @ObfuscatedName("l")
    public int l;

    @ObfuscatedName("x")
    public int x = 255;

    @ObfuscatedName("p")
    public int p = 255;

    @ObfuscatedName("g")
    public int g = -1;

    @ObfuscatedName("n")
    public int n = 1;

    @ObfuscatedName("o")
    public int o = 70;

    @ObfuscatedName("c")
    int c = -1;

    @ObfuscatedName("v")
    int v = -1;

    @ObfuscatedName("u")
    public int maxWidth = 30;

    @ObfuscatedName("j")
    public int j = 0;

    @ObfuscatedName("q")
    public void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            var1.ae();
        } else if (var2 == 2) {
            this.x = var1.av();
        } else if (var2 == 3) {
            this.p = var1.av();
        } else if (var2 == 4) {
            this.g = 0;
        } else if (var2 == 5) {
            this.o = var1.ae();
        } else if (var2 == 6) {
            var1.av();
        } else if (var2 == 7) {
            this.c = var1.aw();
        } else if (var2 == 8) {
            this.v = var1.aw();
        } else if (var2 == 11) {
            this.g = var1.ae();
        } else if (var2 == 14) {
            this.maxWidth = var1.av();
        } else if (var2 == 15) {
            this.j = var1.av();
        }

    }

    @ObfuscatedName("a")
    public Sprite a() {
        if (this.c < 0) {
            return null;
        } else {
            Sprite var1 = (Sprite) a.t((long) this.c);
            if (var1 != null) {
                return var1;
            } else {
                var1 = q.t(hi.q, this.c, 0);
                if (var1 != null) {
                    a.i(var1, (long) this.c);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("l")
    public Sprite l() {
        if (this.v < 0) {
            return null;
        } else {
            Sprite var1 = (Sprite) a.t((long) this.v);
            if (var1 != null) {
                return var1;
            } else {
                var1 = q.t(hi.q, this.v, 0);
                if (var1 != null) {
                    a.i(var1, (long) this.v);
                }

                return var1;
            }
        }
    }

    @ObfuscatedName("l")
    public static Sprite[] l(FileSystem var0, String var1, String var2) {
        int var3 = var0.h(var1);
        int var4 = var0.av(var3, var2);
        byte[] var7 = var0.i(var3, var4);
        boolean var6;
        if (var7 == null) {
            var6 = false;
        } else {
            RTComponent.k(var7);
            var6 = true;
        }

        Sprite[] var5;
        if (!var6) {
            var5 = null;
        } else {
            var5 = kq.o();
        }

        return var5;
    }
}
