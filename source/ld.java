import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ld")
public enum ld implements gl {
    @ObfuscatedName("t")
    t(0, 0), @ObfuscatedName("q")
    q(1, 1), @ObfuscatedName("i")
    i(2, 2), @ObfuscatedName("a")
    a(3, 3), @ObfuscatedName("l")
    l(4, 4);

    @ObfuscatedName("b")
    public final int b;

    @ObfuscatedName("e")
    final int e;

    ld(int var3, int var4) {
        this.b = var3;
        this.e = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.e;
    }
}
