import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gm")
public final class Queue {

    @ObfuscatedName("t")
    hh node = new hh();

    public Queue() {
        this.node.cj = this.node;
        this.node.cl = this.node;
    }

    @ObfuscatedName("t")
    public void t(hh var1) {
        if (var1.cl != null) {
            var1.ce();
        }

        var1.cl = this.node.cl;
        var1.cj = this.node;
        var1.cl.cj = var1;
        var1.cj.cl = var1;
    }

    @ObfuscatedName("q")
    public void q(hh var1) {
        if (var1.cl != null) {
            var1.ce();
        }

        var1.cl = this.node;
        var1.cj = this.node.cj;
        var1.cl.cj = var1;
        var1.cj.cl = var1;
    }

    @ObfuscatedName("a")
    hh a() {
        hh var1 = this.node.cj;
        if (var1 == this.node) {
            return null;
        } else {
            var1.ce();
            return var1;
        }
    }

    @ObfuscatedName("l")
    public hh l() {
        hh var1 = this.node.cj;
        return var1 == this.node ? null : var1;
    }

    @ObfuscatedName("b")
    void b() {
        while (true) {
            hh var1 = this.node.cj;
            if (var1 == this.node) {
                return;
            }

            var1.ce();
        }
    }

    @ObfuscatedName("i")
    static void i(hh var0, hh var1) {
        if (var0.cl != null) {
            var0.ce();
        }

        var0.cl = var1;
        var0.cj = var1.cj;
        var0.cl.cj = var0;
        var0.cj.cl = var0;
    }
}
