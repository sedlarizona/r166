import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("j")
final class j implements Comparator {

    @ObfuscatedName("b")
    static int b;

    @ObfuscatedName("t")
    int t(u var1, u var2) {
        return var1.i.totalOfferQuantity < var2.i.totalOfferQuantity ? -1
                : (var2.i.totalOfferQuantity == var1.i.totalOfferQuantity ? 0 : 1);
    }

    public int compare(Object var1, Object var2) {
        return this.t((u) var1, (u) var2);
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }

    @ObfuscatedName("t")
    static void t(int var0, String var1, String var2) {
        bn.addMessage(var0, var1, var2, (String) null);
    }
}
