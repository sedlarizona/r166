import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hl")
public class ByteArrayNode extends Node {

    @ObfuscatedName("t")
    public byte[] bytes;

    public ByteArrayNode(byte[] var1) {
        this.bytes = var1;
    }
}
