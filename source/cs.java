import java.util.Calendar;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cs")
public class cs {

    @ObfuscatedName("l")
    static int[] l = new int[5];

    @ObfuscatedName("b")
    static int[][] b = new int[5][5000];

    @ObfuscatedName("e")
    static int[] e = new int[1000];

    @ObfuscatedName("p")
    static String[] p = new String[1000];

    @ObfuscatedName("n")
    static int n = 0;

    @ObfuscatedName("o")
    static RuneScriptStackItem[] o = new RuneScriptStackItem[50];

    @ObfuscatedName("c")
    static RTComponent c;

    @ObfuscatedName("u")
    static Calendar u = Calendar.getInstance();

    @ObfuscatedName("j")
    static final String[] j = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
            "Nov", "Dec" };

    @ObfuscatedName("z")
    static int z = 0;

    @ObfuscatedName("fu")
    static byte[][] locationFileBytes;

    @ObfuscatedName("t")
    public static ItemDefinition loadItemDefinition(int var0) {
        ItemDefinition var1 = (ItemDefinition) ItemDefinition.g.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = ItemDefinition.b.i(10, var0);
            var1 = new ItemDefinition();
            var1.id = var0;
            if (var2 != null) {
                var1.i(new ByteBuffer(var2));
            }

            var1.q();
            if (var1.ak != -1) {
                var1.l(loadItemDefinition(var1.ak), loadItemDefinition(var1.af));
            }

            if (var1.bm != -1) {
                var1.b(loadItemDefinition(var1.bm), loadItemDefinition(var1.bc));
            }

            if (var1.bs != -1) {
                var1.e(loadItemDefinition(var1.bs), loadItemDefinition(var1.bh));
            }

            if (!z.x && var1.membersOnly) {
                var1.name = "Members object";
                var1.be = false;
                var1.groundActions = null;
                var1.inventoryActions = null;
                var1.az = -1;
                var1.ba = 0;
                if (var1.bk != null) {
                    boolean var3 = false;

                    for (Node var4 = var1.bk.a(); var4 != null; var4 = var1.bk.l()) {
                        jp var5 = cd.t((int) var4.uid);
                        if (var5.b) {
                            var4.kc();
                        } else {
                            var3 = true;
                        }
                    }

                    if (!var3) {
                        var1.bk = null;
                    }
                }
            }

            ItemDefinition.g.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("t")
    public static boolean t(int var0, int var1) {
        return (var0 >> var1 + 1 & 1) != 0;
    }
}
