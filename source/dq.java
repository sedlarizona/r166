import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dq")
public class dq {

    @ObfuscatedName("t")
    int t = db.a(16);

    @ObfuscatedName("q")
    int q = db.a(24);

    @ObfuscatedName("i")
    int i = db.a(24);

    @ObfuscatedName("a")
    int a = db.a(24) + 1;

    @ObfuscatedName("l")
    int l = db.a(6) + 1;

    @ObfuscatedName("b")
    int b = db.a(8);

    @ObfuscatedName("e")
    int[] e;

    dq() {
        int[] var1 = new int[this.l];

        int var2;
        for (var2 = 0; var2 < this.l; ++var2) {
            int var3 = 0;
            int var4 = db.a(3);
            boolean var5 = db.i() != 0;
            if (var5) {
                var3 = db.a(5);
            }

            var1[var2] = var3 << 3 | var4;
        }

        this.e = new int[this.l * 8];

        for (var2 = 0; var2 < this.l * 8; ++var2) {
            this.e[var2] = (var1[var2 >> 3] & 1 << (var2 & 7)) != 0 ? db.a(8) : -1;
        }

    }

    @ObfuscatedName("t")
    void t(float[] var1, int var2, boolean var3) {
        int var4;
        for (var4 = 0; var4 < var2; ++var4) {
            var1[var4] = 0.0F;
        }

        if (!var3) {
            var4 = db.o[this.b].t;
            int var5 = this.i - this.q;
            int var6 = var5 / this.a;
            int[] var7 = new int[var6];

            for (int var8 = 0; var8 < 8; ++var8) {
                int var9 = 0;

                while (var9 < var6) {
                    int var10;
                    int var11;
                    if (var8 == 0) {
                        var10 = db.o[this.b].i();

                        for (var11 = var4 - 1; var11 >= 0; --var11) {
                            if (var9 + var11 < var6) {
                                var7[var9 + var11] = var10 % this.l;
                            }

                            var10 /= this.l;
                        }
                    }

                    for (var10 = 0; var10 < var4; ++var10) {
                        var11 = var7[var9];
                        int var12 = this.e[var8 + var11 * 8];
                        if (var12 >= 0) {
                            int var13 = var9 * this.a + this.q;
                            cp var14 = db.o[var12];
                            int var15;
                            if (this.t == 0) {
                                var15 = this.a / var14.t;

                                for (int var16 = 0; var16 < var15; ++var16) {
                                    float[] var17 = var14.a();

                                    for (int var18 = 0; var18 < var14.t; ++var18) {
                                        var1[var13 + var16 + var18 * var15] += var17[var18];
                                    }
                                }
                            } else {
                                var15 = 0;

                                while (var15 < this.a) {
                                    float[] var19 = var14.a();

                                    for (int var20 = 0; var20 < var14.t; ++var20) {
                                        var1[var13 + var15] += var19[var20];
                                        ++var15;
                                    }
                                }
                            }
                        }

                        ++var9;
                        if (var9 >= var6) {
                            break;
                        }
                    }
                }
            }

        }
    }
}
