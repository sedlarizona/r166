import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fz")
public class fz extends fr {

    @ObfuscatedName("h")
    static int[] h;

    @ObfuscatedName("t")
    long[] t = new long[10];

    @ObfuscatedName("q")
    int q = 256;

    @ObfuscatedName("i")
    int i = 1;

    @ObfuscatedName("a")
    long a = au.t();

    @ObfuscatedName("l")
    int l = 0;

    @ObfuscatedName("b")
    int b;

    fz() {
        for (int var1 = 0; var1 < 10; ++var1) {
            this.t[var1] = this.a;
        }

    }

    @ObfuscatedName("t")
    public void t() {
        for (int var1 = 0; var1 < 10; ++var1) {
            this.t[var1] = 0L;
        }

    }

    @ObfuscatedName("q")
    public int q(int var1, int var2) {
        int var3 = this.q;
        int var4 = this.i;
        this.q = 300;
        this.i = 1;
        this.a = au.t();
        if (this.t[this.b] == 0L) {
            this.q = var3;
            this.i = var4;
        } else if (this.a > this.t[this.b]) {
            this.q = (int) ((long) (var1 * 2560) / (this.a - this.t[this.b]));
        }

        if (this.q < 25) {
            this.q = 25;
        }

        if (this.q > 256) {
            this.q = 256;
            this.i = (int) ((long) var1 - (this.a - this.t[this.b]) / 10L);
        }

        if (this.i > var1) {
            this.i = var1;
        }

        this.t[this.b] = this.a;
        this.b = (this.b + 1) % 10;
        int var5;
        if (this.i > 1) {
            for (var5 = 0; var5 < 10; ++var5) {
                if (this.t[var5] != 0L) {
                    this.t[var5] += (long) this.i;
                }
            }
        }

        if (this.i < var2) {
            this.i = var2;
        }

        cx.t((long) this.i);

        for (var5 = 0; this.l < 256; this.l += this.q) {
            ++var5;
        }

        this.l &= 255;
        return var5;
    }

    @ObfuscatedName("q")
    static char q(char var0, int var1) {
        if (var0 >= 192 && var0 <= 255) {
            if (var0 >= 192 && var0 <= 198) {
                return 'A';
            }

            if (var0 == 199) {
                return 'C';
            }

            if (var0 >= 200 && var0 <= 203) {
                return 'E';
            }

            if (var0 >= 204 && var0 <= 207) {
                return 'I';
            }

            if (var0 >= 210 && var0 <= 214) {
                return 'O';
            }

            if (var0 >= 217 && var0 <= 220) {
                return 'U';
            }

            if (var0 == 221) {
                return 'Y';
            }

            if (var0 == 223) {
                return 's';
            }

            if (var0 >= 224 && var0 <= 230) {
                return 'a';
            }

            if (var0 == 231) {
                return 'c';
            }

            if (var0 >= 232 && var0 <= 235) {
                return 'e';
            }

            if (var0 >= 236 && var0 <= 239) {
                return 'i';
            }

            if (var0 >= 242 && var0 <= 246) {
                return 'o';
            }

            if (var0 >= 249 && var0 <= 252) {
                return 'u';
            }

            if (var0 == 253 || var0 == 255) {
                return 'y';
            }
        }

        if (var0 == 338) {
            return 'O';
        } else if (var0 == 339) {
            return 'o';
        } else {
            return var0 == 376 ? 'Y' : var0;
        }
    }

    @ObfuscatedName("b")
    public static km b(FileSystem var0, FileSystem var1, String var2, String var3) {
        int var4 = var0.h(var2);
        int var5 = var0.av(var4, var3);
        return TaskData.q(var0, var1, var4, var5);
    }
}
