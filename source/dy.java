import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dy")
public class dy extends TaskDataNode {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    boolean n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    int v;

    @ObfuscatedName("u")
    int u;

    dy(DataRequestNode var1, int var2, int var3, int var4) {
        super.integerNode = var1;
        this.p = var1.i;
        this.g = var1.a;
        this.n = var1.incomplete;
        this.q = var2;
        this.i = var3;
        this.a = var4;
        this.t = 0;
        this.l();
    }

    dy(DataRequestNode var1, int var2, int var3) {
        super.integerNode = var1;
        this.p = var1.i;
        this.g = var1.a;
        this.n = var1.incomplete;
        this.q = var2;
        this.i = var3;
        this.a = 8192;
        this.t = 0;
        this.l();
    }

    @ObfuscatedName("l")
    void l() {
        this.l = this.i;
        this.b = t(this.i, this.a);
        this.e = q(this.i, this.a);
    }

    @ObfuscatedName("b")
    protected TaskDataNode b() {
        return null;
    }

    @ObfuscatedName("e")
    protected TaskDataNode e() {
        return null;
    }

    @ObfuscatedName("x")
    protected int x() {
        return this.i == 0 && this.o == 0 ? 0 : 1;
    }

    @ObfuscatedName("p")
    public synchronized void p(int[] var1, int var2, int var3) {
        if (this.i == 0 && this.o == 0) {
            this.c(var3);
        } else {
            DataRequestNode var4 = (DataRequestNode) super.integerNode;
            int var5 = this.p << 8;
            int var6 = this.g << 8;
            int var7 = var4.buffer.length << 8;
            int var8 = var6 - var5;
            if (var8 <= 0) {
                this.x = 0;
            }

            int var9 = var2;
            var3 += var2;
            if (this.t < 0) {
                if (this.q <= 0) {
                    this.r();
                    this.kc();
                    return;
                }

                this.t = 0;
            }

            if (this.t >= var7) {
                if (this.q >= 0) {
                    this.r();
                    this.kc();
                    return;
                }

                this.t = var7 - 1;
            }

            if (this.x < 0) {
                if (this.n) {
                    if (this.q < 0) {
                        var9 = this.aa(var1, var2, var5, var3, var4.buffer[this.p]);
                        if (this.t >= var5) {
                            return;
                        }

                        this.t = var5 + var5 - 1 - this.t;
                        this.q = -this.q;
                    }

                    while (true) {
                        var9 = this.aq(var1, var9, var6, var3, var4.buffer[this.g - 1]);
                        if (this.t < var6) {
                            return;
                        }

                        this.t = var6 + var6 - 1 - this.t;
                        this.q = -this.q;
                        var9 = this.aa(var1, var9, var5, var3, var4.buffer[this.p]);
                        if (this.t >= var5) {
                            return;
                        }

                        this.t = var5 + var5 - 1 - this.t;
                        this.q = -this.q;
                    }
                } else if (this.q < 0) {
                    while (true) {
                        var9 = this.aa(var1, var9, var5, var3, var4.buffer[this.g - 1]);
                        if (this.t >= var5) {
                            return;
                        }

                        this.t = var6 - 1 - (var6 - 1 - this.t) % var8;
                    }
                } else {
                    while (true) {
                        var9 = this.aq(var1, var9, var6, var3, var4.buffer[this.p]);
                        if (this.t < var6) {
                            return;
                        }

                        this.t = var5 + (this.t - var5) % var8;
                    }
                }
            } else {
                if (this.x > 0) {
                    if (this.n) {
                        label120: {
                            if (this.q < 0) {
                                var9 = this.aa(var1, var2, var5, var3, var4.buffer[this.p]);
                                if (this.t >= var5) {
                                    return;
                                }

                                this.t = var5 + var5 - 1 - this.t;
                                this.q = -this.q;
                                if (--this.x == 0) {
                                    break label120;
                                }
                            }

                            do {
                                var9 = this.aq(var1, var9, var6, var3, var4.buffer[this.g - 1]);
                                if (this.t < var6) {
                                    return;
                                }

                                this.t = var6 + var6 - 1 - this.t;
                                this.q = -this.q;
                                if (--this.x == 0) {
                                    break;
                                }

                                var9 = this.aa(var1, var9, var5, var3, var4.buffer[this.p]);
                                if (this.t >= var5) {
                                    return;
                                }

                                this.t = var5 + var5 - 1 - this.t;
                                this.q = -this.q;
                            } while (--this.x != 0);
                        }
                    } else {
                        int var10;
                        if (this.q < 0) {
                            while (true) {
                                var9 = this.aa(var1, var9, var5, var3, var4.buffer[this.g - 1]);
                                if (this.t >= var5) {
                                    return;
                                }

                                var10 = (var6 - 1 - this.t) / var8;
                                if (var10 >= this.x) {
                                    this.t += var8 * this.x;
                                    this.x = 0;
                                    break;
                                }

                                this.t += var8 * var10;
                                this.x -= var10;
                            }
                        } else {
                            while (true) {
                                var9 = this.aq(var1, var9, var6, var3, var4.buffer[this.p]);
                                if (this.t < var6) {
                                    return;
                                }

                                var10 = (this.t - var5) / var8;
                                if (var10 >= this.x) {
                                    this.t -= var8 * this.x;
                                    this.x = 0;
                                    break;
                                }

                                this.t -= var8 * var10;
                                this.x -= var10;
                            }
                        }
                    }
                }

                if (this.q < 0) {
                    this.aa(var1, var9, 0, var3, 0);
                    if (this.t < 0) {
                        this.t = -1;
                        this.r();
                        this.kc();
                    }
                } else {
                    this.aq(var1, var9, var7, var3, 0);
                    if (this.t >= var7) {
                        this.t = var7;
                        this.r();
                        this.kc();
                    }
                }

            }
        }
    }

    @ObfuscatedName("o")
    public synchronized void o(int var1) {
        this.x = var1;
    }

    @ObfuscatedName("c")
    public synchronized void c(int var1) {
        if (this.o > 0) {
            if (var1 >= this.o) {
                if (this.i == Integer.MIN_VALUE) {
                    this.i = 0;
                    this.e = 0;
                    this.b = 0;
                    this.l = 0;
                    this.kc();
                    var1 = this.o;
                }

                this.o = 0;
                this.l();
            } else {
                this.l += this.c * var1;
                this.b += this.v * var1;
                this.e += this.u * var1;
                this.o -= var1;
            }
        }

        DataRequestNode var2 = (DataRequestNode) super.integerNode;
        int var3 = this.p << 8;
        int var4 = this.g << 8;
        int var5 = var2.buffer.length << 8;
        int var6 = var4 - var3;
        if (var6 <= 0) {
            this.x = 0;
        }

        if (this.t < 0) {
            if (this.q <= 0) {
                this.r();
                this.kc();
                return;
            }

            this.t = 0;
        }

        if (this.t >= var5) {
            if (this.q >= 0) {
                this.r();
                this.kc();
                return;
            }

            this.t = var5 - 1;
        }

        this.t += this.q * var1;
        if (this.x < 0) {
            if (!this.n) {
                if (this.q < 0) {
                    if (this.t >= var3) {
                        return;
                    }

                    this.t = var4 - 1 - (var4 - 1 - this.t) % var6;
                } else {
                    if (this.t < var4) {
                        return;
                    }

                    this.t = var3 + (this.t - var3) % var6;
                }

            } else {
                if (this.q < 0) {
                    if (this.t >= var3) {
                        return;
                    }

                    this.t = var3 + var3 - 1 - this.t;
                    this.q = -this.q;
                }

                while (this.t >= var4) {
                    this.t = var4 + var4 - 1 - this.t;
                    this.q = -this.q;
                    if (this.t >= var3) {
                        return;
                    }

                    this.t = var3 + var3 - 1 - this.t;
                    this.q = -this.q;
                }

            }
        } else {
            if (this.x > 0) {
                if (this.n) {
                    label126: {
                        if (this.q < 0) {
                            if (this.t >= var3) {
                                return;
                            }

                            this.t = var3 + var3 - 1 - this.t;
                            this.q = -this.q;
                            if (--this.x == 0) {
                                break label126;
                            }
                        }

                        do {
                            if (this.t < var4) {
                                return;
                            }

                            this.t = var4 + var4 - 1 - this.t;
                            this.q = -this.q;
                            if (--this.x == 0) {
                                break;
                            }

                            if (this.t >= var3) {
                                return;
                            }

                            this.t = var3 + var3 - 1 - this.t;
                            this.q = -this.q;
                        } while (--this.x != 0);
                    }
                } else {
                    label158: {
                        int var7;
                        if (this.q < 0) {
                            if (this.t >= var3) {
                                return;
                            }

                            var7 = (var4 - 1 - this.t) / var6;
                            if (var7 >= this.x) {
                                this.t += var6 * this.x;
                                this.x = 0;
                                break label158;
                            }

                            this.t += var6 * var7;
                            this.x -= var7;
                        } else {
                            if (this.t < var4) {
                                return;
                            }

                            var7 = (this.t - var3) / var6;
                            if (var7 >= this.x) {
                                this.t -= var6 * this.x;
                                this.x = 0;
                                break label158;
                            }

                            this.t -= var6 * var7;
                            this.x -= var7;
                        }

                        return;
                    }
                }
            }

            if (this.q < 0) {
                if (this.t < 0) {
                    this.t = -1;
                    this.r();
                    this.kc();
                }
            } else if (this.t >= var5) {
                this.t = var5;
                this.r();
                this.kc();
            }

        }
    }

    @ObfuscatedName("u")
    public synchronized void u(int var1) {
        this.z(var1 << 6, this.s());
    }

    @ObfuscatedName("k")
    synchronized void k(int var1) {
        this.z(var1, this.s());
    }

    @ObfuscatedName("z")
    synchronized void z(int var1, int var2) {
        this.i = var1;
        this.a = var2;
        this.o = 0;
        this.l();
    }

    @ObfuscatedName("w")
    public synchronized int w() {
        return this.i == Integer.MIN_VALUE ? 0 : this.i;
    }

    @ObfuscatedName("s")
    public synchronized int s() {
        return this.a < 0 ? -1 : this.a;
    }

    @ObfuscatedName("d")
    public synchronized void d(int var1) {
        int var2 = ((DataRequestNode) super.integerNode).buffer.length << 8;
        if (var1 < -1) {
            var1 = -1;
        }

        if (var1 > var2) {
            var1 = var2;
        }

        this.t = var1;
    }

    @ObfuscatedName("f")
    public synchronized void f() {
        this.q = (this.q ^ this.q >> 31) + (this.q >>> 31);
        this.q = -this.q;
    }

    @ObfuscatedName("r")
    void r() {
        if (this.o != 0) {
            if (this.i == Integer.MIN_VALUE) {
                this.i = 0;
            }

            this.o = 0;
            this.l();
        }

    }

    @ObfuscatedName("y")
    public synchronized void y(int var1, int var2) {
        this.h(var1, var2, this.s());
    }

    @ObfuscatedName("h")
    public synchronized void h(int var1, int var2, int var3) {
        if (var1 == 0) {
            this.z(var2, var3);
        } else {
            int var4 = t(var2, var3);
            int var5 = q(var2, var3);
            if (var4 == this.b && var5 == this.e) {
                this.o = 0;
            } else {
                int var6 = var2 - this.l;
                if (this.l - var2 > var6) {
                    var6 = this.l - var2;
                }

                if (var4 - this.b > var6) {
                    var6 = var4 - this.b;
                }

                if (this.b - var4 > var6) {
                    var6 = this.b - var4;
                }

                if (var5 - this.e > var6) {
                    var6 = var5 - this.e;
                }

                if (this.e - var5 > var6) {
                    var6 = this.e - var5;
                }

                if (var1 > var6) {
                    var1 = var6;
                }

                this.o = var1;
                this.i = var2;
                this.a = var3;
                this.c = (var2 - this.l) / var1;
                this.v = (var4 - this.b) / var1;
                this.u = (var5 - this.e) / var1;
            }
        }
    }

    @ObfuscatedName("av")
    public synchronized void av(int var1) {
        if (var1 == 0) {
            this.k(0);
            this.kc();
        } else if (this.b == 0 && this.e == 0) {
            this.o = 0;
            this.i = 0;
            this.l = 0;
            this.kc();
        } else {
            int var2 = -this.l;
            if (this.l > var2) {
                var2 = this.l;
            }

            if (-this.b > var2) {
                var2 = -this.b;
            }

            if (this.b > var2) {
                var2 = this.b;
            }

            if (-this.e > var2) {
                var2 = -this.e;
            }

            if (this.e > var2) {
                var2 = this.e;
            }

            if (var1 > var2) {
                var1 = var2;
            }

            this.o = var1;
            this.i = Integer.MIN_VALUE;
            this.c = -this.l / var1;
            this.v = -this.b / var1;
            this.u = -this.e / var1;
        }
    }

    @ObfuscatedName("an")
    public synchronized void an(int var1) {
        if (this.q < 0) {
            this.q = -var1;
        } else {
            this.q = var1;
        }

    }

    @ObfuscatedName("ai")
    public synchronized int ai() {
        return this.q < 0 ? -this.q : this.q;
    }

    @ObfuscatedName("ag")
    public boolean ag() {
        return this.t < 0 || this.t >= ((DataRequestNode) super.integerNode).buffer.length << 8;
    }

    @ObfuscatedName("as")
    public boolean as() {
        return this.o != 0;
    }

    @ObfuscatedName("aw")
    int aw() {
        int var1 = this.l * 3 >> 6;
        var1 = (var1 ^ var1 >> 31) + (var1 >>> 31);
        if (this.x == 0) {
            var1 -= var1 * this.t / (((DataRequestNode) super.integerNode).buffer.length << 8);
        } else if (this.x >= 0) {
            var1 -= var1 * this.p / ((DataRequestNode) super.integerNode).buffer.length;
        }

        return var1 > 255 ? 255 : var1;
    }

    @ObfuscatedName("aq")
    int aq(int[] var1, int var2, int var3, int var4, int var5) {
        while (true) {
            if (this.o > 0) {
                int var6 = var2 + this.o;
                if (var6 > var4) {
                    var6 = var4;
                }

                this.o += var2;
                if (this.q == 256 && (this.t & 255) == 0) {
                    if (TaskData.b) {
                        var2 = bc(0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e,
                                this.v, this.u, 0, var6, var3, this);
                    } else {
                        var2 = be(((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, this.c, 0,
                                var6, var3, this);
                    }
                } else if (TaskData.b) {
                    var2 = bj(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e,
                            this.v, this.u, 0, var6, var3, this, this.q, var5);
                } else {
                    var2 = bs(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, this.c,
                            0, var6, var3, this, this.q, var5);
                }

                this.o -= var2;
                if (this.o != 0) {
                    return var2;
                }

                if (!this.af()) {
                    continue;
                }

                return var4;
            }

            if (this.q == 256 && (this.t & 255) == 0) {
                if (TaskData.b) {
                    return ab(0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e, 0,
                            var4, var3, this);
                }

                return ak(((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, 0, var4, var3, this);
            }

            if (TaskData.b) {
                return br(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e, 0,
                        var4, var3, this, this.q, var5);
            }

            return bg(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, 0, var4, var3,
                    this, this.q, var5);
        }
    }

    @ObfuscatedName("aa")
    int aa(int[] var1, int var2, int var3, int var4, int var5) {
        while (true) {
            if (this.o > 0) {
                int var6 = var2 + this.o;
                if (var6 > var4) {
                    var6 = var4;
                }

                this.o += var2;
                if (this.q == -256 && (this.t & 255) == 0) {
                    if (TaskData.b) {
                        var2 = bh(0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e,
                                this.v, this.u, 0, var6, var3, this);
                    } else {
                        var2 = bm(((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, this.c, 0,
                                var6, var3, this);
                    }
                } else if (TaskData.b) {
                    var2 = by(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e,
                            this.v, this.u, 0, var6, var3, this, this.q, var5);
                } else {
                    var2 = bt(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, this.c,
                            0, var6, var3, this, this.q, var5);
                }

                this.o -= var2;
                if (this.o != 0) {
                    return var2;
                }

                if (!this.af()) {
                    continue;
                }

                return var4;
            }

            if (this.q == -256 && (this.t & 255) == 0) {
                if (TaskData.b) {
                    return ad(0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e, 0,
                            var4, var3, this);
                }

                return ac(((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, 0, var4, var3, this);
            }

            if (TaskData.b) {
                return bk(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.b, this.e, 0,
                        var4, var3, this, this.q, var5);
            }

            return ba(0, 0, ((DataRequestNode) super.integerNode).buffer, var1, this.t, var2, this.l, 0, var4, var3,
                    this, this.q, var5);
        }
    }

    @ObfuscatedName("af")
    boolean af() {
        int var1 = this.i;
        int var2;
        int var3;
        if (var1 == Integer.MIN_VALUE) {
            var3 = 0;
            var2 = 0;
            var1 = 0;
        } else {
            var2 = t(var1, this.a);
            var3 = q(var1, this.a);
        }

        if (var1 == this.l && var2 == this.b && var3 == this.e) {
            if (this.i == Integer.MIN_VALUE) {
                this.i = 0;
                this.e = 0;
                this.b = 0;
                this.l = 0;
                this.kc();
                return true;
            } else {
                this.l();
                return false;
            }
        } else {
            if (this.l < var1) {
                this.c = 1;
                this.o = var1 - this.l;
            } else if (this.l > var1) {
                this.c = -1;
                this.o = this.l - var1;
            } else {
                this.c = 0;
            }

            if (this.b < var2) {
                this.v = 1;
                if (this.o == 0 || this.o > var2 - this.b) {
                    this.o = var2 - this.b;
                }
            } else if (this.b > var2) {
                this.v = -1;
                if (this.o == 0 || this.o > this.b - var2) {
                    this.o = this.b - var2;
                }
            } else {
                this.v = 0;
            }

            if (this.e < var3) {
                this.u = 1;
                if (this.o == 0 || this.o > var3 - this.e) {
                    this.o = var3 - this.e;
                }
            } else if (this.e > var3) {
                this.u = -1;
                if (this.o == 0 || this.o > this.e - var3) {
                    this.o = this.e - var3;
                }
            } else {
                this.u = 0;
            }

            return false;
        }
    }

    @ObfuscatedName("t")
    static int t(int var0, int var1) {
        return var1 < 0 ? var0 : (int) ((double) var0 * Math.sqrt((double) (16384 - var1) * 1.220703125E-4D) + 0.5D);
    }

    @ObfuscatedName("q")
    static int q(int var0, int var1) {
        return var1 < 0 ? -var0 : (int) ((double) var0 * Math.sqrt((double) var1 * 1.220703125E-4D) + 0.5D);
    }

    @ObfuscatedName("i")
    public static dy i(DataRequestNode var0, int var1, int var2) {
        return var0.buffer != null && var0.buffer.length != 0 ? new dy(var0,
                (int) ((long) var0.t * 256L * (long) var1 / (long) (TaskData.l * 100)), var2 << 6) : null;
    }

    @ObfuscatedName("a")
    public static dy a(DataRequestNode var0, int var1, int var2, int var3) {
        return var0.buffer != null && var0.buffer.length != 0 ? new dy(var0, var1, var2, var3) : null;
    }

    @ObfuscatedName("ak")
    static int ak(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, dy var8) {
        var2 >>= 8;
        var7 >>= 8;
        var4 <<= 2;
        if ((var5 = var3 + var7 - var2) > var6) {
            var5 = var6;
        }

        int var10001;
        for (var5 -= 3; var3 < var5; var1[var10001] += var0[var2++] * var4) {
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var10001 = var3++;
        }

        for (var5 += 3; var3 < var5; var1[var10001] += var0[var2++] * var4) {
            var10001 = var3++;
        }

        var8.t = var2 << 8;
        return var3;
    }

    @ObfuscatedName("ab")
    static int ab(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8,
            int var9, dy var10) {
        var3 >>= 8;
        var9 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        if ((var7 = var4 + var9 - var3) > var8) {
            var7 = var8;
        }

        var4 <<= 1;
        var7 <<= 1;

        int var10001;
        byte var11;
        for (var7 -= 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        for (var7 += 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        var10.t = var3 << 8;
        return var4 >> 1;
    }

    @ObfuscatedName("ac")
    static int ac(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, dy var8) {
        var2 >>= 8;
        var7 >>= 8;
        var4 <<= 2;
        if ((var5 = var3 + var2 - (var7 - 1)) > var6) {
            var5 = var6;
        }

        int var10001;
        for (var5 -= 3; var3 < var5; var1[var10001] += var0[var2--] * var4) {
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var10001 = var3++;
        }

        for (var5 += 3; var3 < var5; var1[var10001] += var0[var2--] * var4) {
            var10001 = var3++;
        }

        var8.t = var2 << 8;
        return var3;
    }

    @ObfuscatedName("ad")
    static int ad(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8,
            int var9, dy var10) {
        var3 >>= 8;
        var9 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        if ((var7 = var3 + var4 - (var9 - 1)) > var8) {
            var7 = var8;
        }

        var4 <<= 1;
        var7 <<= 1;

        int var10001;
        byte var11;
        for (var7 -= 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        for (var7 += 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        var10.t = var3 << 8;
        return var4 >> 1;
    }

    @ObfuscatedName("bg")
    static int bg(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, dy var10, int var11, int var12) {
        if (var11 == 0 || (var7 = var5 + (var11 + (var9 - var4) - 257) / var11) > var8) {
            var7 = var8;
        }

        byte var13;
        int var10001;
        while (var5 < var7) {
            var1 = var4 >> 8;
            var13 = var2[var1];
            var10001 = var5++;
            var3[var10001] += ((var13 << 8) + (var2[var1 + 1] - var13) * (var4 & 255)) * var6 >> 6;
            var4 += var11;
        }

        if (var11 == 0 || (var7 = var5 + (var11 + (var9 - var4) - 1) / var11) > var8) {
            var7 = var8;
        }

        for (var1 = var12; var5 < var7; var4 += var11) {
            var13 = var2[var4 >> 8];
            var10001 = var5++;
            var3[var10001] += ((var13 << 8) + (var1 - var13) * (var4 & 255)) * var6 >> 6;
        }

        var10.t = var4;
        return var5;
    }

    @ObfuscatedName("br")
    static int br(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, dy var11, int var12, int var13) {
        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 257) / var12) > var9) {
            var8 = var9;
        }

        var5 <<= 1;

        byte var14;
        int var10001;
        for (var8 <<= 1; var5 < var8; var4 += var12) {
            var1 = var4 >> 8;
            var14 = var2[var1];
            var0 = (var14 << 8) + (var4 & 255) * (var2[var1 + 1] - var14);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        if (var12 == 0 || (var8 = (var5 >> 1) + (var10 - var4 + var12 - 1) / var12) > var9) {
            var8 = var9;
        }

        var8 <<= 1;

        for (var1 = var13; var5 < var8; var4 += var12) {
            var14 = var2[var4 >> 8];
            var0 = (var14 << 8) + (var1 - var14) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        var11.t = var4;
        return var5 >> 1;
    }

    @ObfuscatedName("ba")
    static int ba(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, dy var10, int var11, int var12) {
        if (var11 == 0 || (var7 = var5 + (var11 + (var9 + 256 - var4)) / var11) > var8) {
            var7 = var8;
        }

        int var10001;
        while (var5 < var7) {
            var1 = var4 >> 8;
            byte var13 = var2[var1 - 1];
            var10001 = var5++;
            var3[var10001] += ((var13 << 8) + (var2[var1] - var13) * (var4 & 255)) * var6 >> 6;
            var4 += var11;
        }

        if (var11 == 0 || (var7 = var5 + (var11 + (var9 - var4)) / var11) > var8) {
            var7 = var8;
        }

        var0 = var12;

        for (var1 = var11; var5 < var7; var4 += var1) {
            var10001 = var5++;
            var3[var10001] += ((var0 << 8) + (var2[var4 >> 8] - var0) * (var4 & 255)) * var6 >> 6;
        }

        var10.t = var4;
        return var5;
    }

    @ObfuscatedName("bk")
    static int bk(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, dy var11, int var12, int var13) {
        if (var12 == 0 || (var8 = var5 + (var10 + 256 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        var5 <<= 1;

        int var10001;
        for (var8 <<= 1; var5 < var8; var4 += var12) {
            var1 = var4 >> 8;
            byte var14 = var2[var1 - 1];
            var0 = (var2[var1] - var14) * (var4 & 255) + (var14 << 8);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        if (var12 == 0 || (var8 = (var5 >> 1) + (var10 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        var8 <<= 1;

        for (var1 = var13; var5 < var8; var4 += var12) {
            var0 = (var1 << 8) + (var4 & 255) * (var2[var4 >> 8] - var1);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        var11.t = var4;
        return var5 >> 1;
    }

    @ObfuscatedName("be")
    static int be(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, dy var9) {
        var2 >>= 8;
        var8 >>= 8;
        var4 <<= 2;
        var5 <<= 2;
        if ((var6 = var3 + var8 - var2) > var7) {
            var6 = var7;
        }

        var9.b += var9.v * (var6 - var3);
        var9.e += var9.u * (var6 - var3);

        int var10001;
        for (var6 -= 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
        }

        for (var6 += 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
        }

        var9.l = var4 >> 2;
        var9.t = var2 << 8;
        return var3;
    }

    @ObfuscatedName("bc")
    static int bc(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, int var11, dy var12) {
        var3 >>= 8;
        var11 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        var7 <<= 2;
        var8 <<= 2;
        if ((var9 = var11 + var4 - var3) > var10) {
            var9 = var10;
        }

        var12.l += var12.c * (var9 - var4);
        var4 <<= 1;
        var9 <<= 1;

        byte var13;
        int var10001;
        for (var9 -= 6; var4 < var9; var6 += var8) {
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        for (var9 += 6; var4 < var9; var6 += var8) {
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        var12.b = var5 >> 2;
        var12.e = var6 >> 2;
        var12.t = var3 << 8;
        return var4 >> 1;
    }

    @ObfuscatedName("bm")
    static int bm(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, dy var9) {
        var2 >>= 8;
        var8 >>= 8;
        var4 <<= 2;
        var5 <<= 2;
        if ((var6 = var3 + var2 - (var8 - 1)) > var7) {
            var6 = var7;
        }

        var9.b += var9.v * (var6 - var3);
        var9.e += var9.u * (var6 - var3);

        int var10001;
        for (var6 -= 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
        }

        for (var6 += 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
        }

        var9.l = var4 >> 2;
        var9.t = var2 << 8;
        return var3;
    }

    @ObfuscatedName("bh")
    static int bh(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, int var11, dy var12) {
        var3 >>= 8;
        var11 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        var7 <<= 2;
        var8 <<= 2;
        if ((var9 = var3 + var4 - (var11 - 1)) > var10) {
            var9 = var10;
        }

        var12.l += var12.c * (var9 - var4);
        var4 <<= 1;
        var9 <<= 1;

        byte var13;
        int var10001;
        for (var9 -= 6; var4 < var9; var6 += var8) {
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        for (var9 += 6; var4 < var9; var6 += var8) {
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        var12.b = var5 >> 2;
        var12.e = var6 >> 2;
        var12.t = var3 << 8;
        return var4 >> 1;
    }

    @ObfuscatedName("bs")
    static int bs(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, dy var11, int var12, int var13) {
        var11.b -= var11.v * var5;
        var11.e -= var11.u * var5;
        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 257) / var12) > var9) {
            var8 = var9;
        }

        byte var14;
        int var10001;
        while (var5 < var8) {
            var1 = var4 >> 8;
            var14 = var2[var1];
            var10001 = var5++;
            var3[var10001] += ((var14 << 8) + (var2[var1 + 1] - var14) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
            var4 += var12;
        }

        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 1) / var12) > var9) {
            var8 = var9;
        }

        for (var1 = var13; var5 < var8; var4 += var12) {
            var14 = var2[var4 >> 8];
            var10001 = var5++;
            var3[var10001] += ((var14 << 8) + (var1 - var14) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
        }

        var11.b += var11.v * var5;
        var11.e += var11.u * var5;
        var11.l = var6;
        var11.t = var4;
        return var5;
    }

    @ObfuscatedName("bj")
    static int bj(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, int var11, int var12, dy var13, int var14, int var15) {
        var13.l -= var5 * var13.c;
        if (var14 == 0 || (var10 = var5 + (var12 - var4 + var14 - 257) / var14) > var11) {
            var10 = var11;
        }

        var5 <<= 1;

        byte var16;
        int var10001;
        for (var10 <<= 1; var5 < var10; var4 += var14) {
            var1 = var4 >> 8;
            var16 = var2[var1];
            var0 = (var16 << 8) + (var4 & 255) * (var2[var1 + 1] - var16);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        if (var14 == 0 || (var10 = (var5 >> 1) + (var12 - var4 + var14 - 1) / var14) > var11) {
            var10 = var11;
        }

        var10 <<= 1;

        for (var1 = var15; var5 < var10; var4 += var14) {
            var16 = var2[var4 >> 8];
            var0 = (var16 << 8) + (var1 - var16) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        var5 >>= 1;
        var13.l += var13.c * var5;
        var13.b = var6;
        var13.e = var7;
        var13.t = var4;
        return var5;
    }

    @ObfuscatedName("bt")
    static int bt(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, dy var11, int var12, int var13) {
        var11.b -= var11.v * var5;
        var11.e -= var11.u * var5;
        if (var12 == 0 || (var8 = var5 + (var10 + 256 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        int var10001;
        while (var5 < var8) {
            var1 = var4 >> 8;
            byte var14 = var2[var1 - 1];
            var10001 = var5++;
            var3[var10001] += ((var14 << 8) + (var2[var1] - var14) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
            var4 += var12;
        }

        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        var0 = var13;

        for (var1 = var12; var5 < var8; var4 += var1) {
            var10001 = var5++;
            var3[var10001] += ((var0 << 8) + (var2[var4 >> 8] - var0) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
        }

        var11.b += var11.v * var5;
        var11.e += var11.u * var5;
        var11.l = var6;
        var11.t = var4;
        return var5;
    }

    @ObfuscatedName("by")
    static int by(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, int var11, int var12, dy var13, int var14, int var15) {
        var13.l -= var5 * var13.c;
        if (var14 == 0 || (var10 = var5 + (var12 + 256 - var4 + var14) / var14) > var11) {
            var10 = var11;
        }

        var5 <<= 1;

        int var10001;
        for (var10 <<= 1; var5 < var10; var4 += var14) {
            var1 = var4 >> 8;
            byte var16 = var2[var1 - 1];
            var0 = (var2[var1] - var16) * (var4 & 255) + (var16 << 8);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        if (var14 == 0 || (var10 = (var5 >> 1) + (var12 - var4 + var14) / var14) > var11) {
            var10 = var11;
        }

        var10 <<= 1;

        for (var1 = var15; var5 < var10; var4 += var14) {
            var0 = (var1 << 8) + (var4 & 255) * (var2[var4 >> 8] - var1);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        var5 >>= 1;
        var13.l += var13.c * var5;
        var13.b = var6;
        var13.e = var7;
        var13.t = var4;
        return var5;
    }
}
