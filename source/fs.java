import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fs")
public interface fs {

    @ObfuscatedName("i")
    int i();
}
