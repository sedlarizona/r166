import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dx")
public final class ItemPile {

    @ObfuscatedName("t")
    int groundElevation;

    @ObfuscatedName("q")
    int regionX;

    @ObfuscatedName("i")
    int regionY;

    @ObfuscatedName("a")
    Renderable bottom;

    @ObfuscatedName("l")
    Renderable middle;

    @ObfuscatedName("b")
    Renderable top;

    @ObfuscatedName("e")
    int hash;

    @ObfuscatedName("x")
    int objectElevation;

    @ObfuscatedName("t")
    public static int t(int var0, iw var1) {
        return (var0 + '鱀' << 8) + var1.n;
    }

    @ObfuscatedName("t")
    public static boolean t(FileSystem var0, FileSystem var1, FileSystem var2, AudioTask var3) {
        c.t = var0;
        ic.q = var1;
        hi.i = var2;
        hi.audioTask = var3;
        return true;
    }

    @ObfuscatedName("i")
    static void i(Server[] var0, int var1, int var2, int[] var3, int[] var4) {
        if (var1 < var2) {
            int var5 = var1 - 1;
            int var6 = var2 + 1;
            int var7 = (var2 + var1) / 2;
            Server var8 = var0[var7];
            var0[var7] = var0[var1];
            var0[var1] = var8;

            while (var5 < var6) {
                boolean var9 = true;

                int var10;
                int var11;
                int var12;
                do {
                    --var6;

                    for (var10 = 0; var10 < 4; ++var10) {
                        if (var3[var10] == 2) {
                            var11 = var0[var6].index;
                            var12 = var8.index;
                        } else if (var3[var10] == 1) {
                            var11 = var0[var6].population;
                            var12 = var8.population;
                            if (var11 == -1 && var4[var10] == 1) {
                                var11 = 2001;
                            }

                            if (var12 == -1 && var4[var10] == 1) {
                                var12 = 2001;
                            }
                        } else if (var3[var10] == 3) {
                            var11 = var0[var6].x() ? 1 : 0;
                            var12 = var8.x() ? 1 : 0;
                        } else {
                            var11 = var0[var6].number;
                            var12 = var8.number;
                        }

                        if (var12 != var11) {
                            if ((var4[var10] != 1 || var11 <= var12) && (var4[var10] != 0 || var11 >= var12)) {
                                var9 = false;
                            }
                            break;
                        }

                        if (var10 == 3) {
                            var9 = false;
                        }
                    }
                } while (var9);

                var9 = true;

                do {
                    ++var5;

                    for (var10 = 0; var10 < 4; ++var10) {
                        if (var3[var10] == 2) {
                            var11 = var0[var5].index;
                            var12 = var8.index;
                        } else if (var3[var10] == 1) {
                            var11 = var0[var5].population;
                            var12 = var8.population;
                            if (var11 == -1 && var4[var10] == 1) {
                                var11 = 2001;
                            }

                            if (var12 == -1 && var4[var10] == 1) {
                                var12 = 2001;
                            }
                        } else if (var3[var10] == 3) {
                            var11 = var0[var5].x() ? 1 : 0;
                            var12 = var8.x() ? 1 : 0;
                        } else {
                            var11 = var0[var5].number;
                            var12 = var8.number;
                        }

                        if (var12 != var11) {
                            if ((var4[var10] != 1 || var11 >= var12) && (var4[var10] != 0 || var11 <= var12)) {
                                var9 = false;
                            }
                            break;
                        }

                        if (var10 == 3) {
                            var9 = false;
                        }
                    }
                } while (var9);

                if (var5 < var6) {
                    Server var13 = var0[var5];
                    var0[var5] = var0[var6];
                    var0[var6] = var13;
                }
            }

            i(var0, var1, var6, var3, var4);
            i(var0, var6 + 1, var2, var3, var4);
        }

    }
}
