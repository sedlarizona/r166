import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jr")
public class Varpbit extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    static Cache q = new Cache(64);

    @ObfuscatedName("ca")
    static ju ca;

    @ObfuscatedName("i")
    public int varp;

    @ObfuscatedName("a")
    public int low;

    @ObfuscatedName("l")
    public int high;

    @ObfuscatedName("q")
    void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.varp = var1.ae();
            this.low = var1.av();
            this.high = var1.av();
        }

    }
}
