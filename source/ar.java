import java.io.File;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ar")
public class ar {

    @ObfuscatedName("t")
    static final ar t = new ar(0);

    @ObfuscatedName("q")
    static final ar q = new ar(1);

    @ObfuscatedName("a")
    static File a;

    @ObfuscatedName("i")
    final int i;

    ar(int var1) {
        this.i = var1;
    }

    @ObfuscatedName("q")
    public static void q() {
        Object var0 = jt.l;
        synchronized (jt.l) {
            if (jt.a != 0) {
                jt.a = 1;

                try {
                    jt.l.wait();
                } catch (InterruptedException var3) {
                    ;
                }
            }

        }
    }

    @ObfuscatedName("q")
    static String q(int var0) {
        return "<col=" + Integer.toHexString(var0) + ">";
    }

    @ObfuscatedName("i")
    static void i() {
        RSRandomAccessFile var0 = null;

        try {
            var0 = ky.a("", Client.be.e, true);
            ByteBuffer var1 = al.qp.q();
            var0.q(var1.buffer, 0, var1.index);
        } catch (Exception var3) {
            ;
        }

        try {
            if (var0 != null) {
                var0.a(true);
            }
        } catch (Exception var2) {
            ;
        }

    }

    @ObfuscatedName("ht")
    static final void ht(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        bl var9 = null;

        for (bl var10 = (bl) Client.jw.e(); var10 != null; var10 = (bl) Client.jw.p()) {
            if (var0 == var10.t && var10.i == var1 && var2 == var10.a && var3 == var10.q) {
                var9 = var10;
                break;
            }
        }

        if (var9 == null) {
            var9 = new bl();
            var9.t = var0;
            var9.q = var3;
            var9.i = var1;
            var9.a = var2;
            Inflater.hq(var9);
            Client.jw.q(var9);
        }

        var9.x = var4;
        var9.g = var5;
        var9.p = var6;
        var9.n = var7;
        var9.o = var8;
    }
}
