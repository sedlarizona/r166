import java.applet.Applet;
import net.runelite.mapping.ObfuscatedName;
import netscape.javascript.JSObject;

@ObfuscatedName("br")
public class br {

    @ObfuscatedName("t")
    public static Object t(Applet var0, String var1)
            throws Throwable {
        return JSObject.getWindow(var0).call(var1, (Object[]) null);
    }

    @ObfuscatedName("q")
    public static Object q(Applet var0, String var1, Object[] var2)
            throws Throwable {
        return JSObject.getWindow(var0).call(var1, var2);
    }
}
