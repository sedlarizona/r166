import java.io.EOFException;
import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fn")
public final class fn {

    @ObfuscatedName("t")
    static byte[] t = new byte[520];

    @ObfuscatedName("q")
    RSRandomAccessFileChannel q = null;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    RSRandomAccessFileChannel a = null;

    @ObfuscatedName("l")
    int l = 65000;

    public fn(int var1, RSRandomAccessFileChannel var2, RSRandomAccessFileChannel var3, int var4) {
        this.i = var1;
        this.q = var2;
        this.a = var3;
        this.l = var4;
    }

    @ObfuscatedName("t")
    public byte[] t(int var1) {
        RSRandomAccessFileChannel var2 = this.q;
        synchronized (this.q) {
            try {
                Object var10000;
                if (this.a.i() < (long) (var1 * 6 + 6)) {
                    var10000 = null;
                    return (byte[]) var10000;
                } else {
                    this.a.q((long) (var1 * 6));
                    this.a.l(t, 0, 6);
                    int var3 = ((t[0] & 255) << 16) + (t[2] & 255) + ((t[1] & 255) << 8);
                    int var4 = (t[5] & 255) + ((t[3] & 255) << 16) + ((t[4] & 255) << 8);
                    if (var3 < 0 || var3 > this.l) {
                        var10000 = null;
                        return (byte[]) var10000;
                    } else if (var4 <= 0 || (long) var4 > this.q.i() / 520L) {
                        var10000 = null;
                        return (byte[]) var10000;
                    } else {
                        byte[] var5 = new byte[var3];
                        int var6 = 0;
                        int var7 = 0;

                        while (var6 < var3) {
                            if (var4 == 0) {
                                var10000 = null;
                                return (byte[]) var10000;
                            }

                            this.q.q((long) (var4 * 520));
                            int var8 = var3 - var6;
                            if (var8 > 512) {
                                var8 = 512;
                            }

                            this.q.l(t, 0, var8 + 8);
                            int var9 = (t[1] & 255) + ((t[0] & 255) << 8);
                            int var10 = (t[3] & 255) + ((t[2] & 255) << 8);
                            int var11 = ((t[5] & 255) << 8) + ((t[4] & 255) << 16) + (t[6] & 255);
                            int var12 = t[7] & 255;
                            if (var9 == var1 && var10 == var7 && var12 == this.i) {
                                if (var11 >= 0 && (long) var11 <= this.q.i() / 520L) {
                                    for (int var13 = 0; var13 < var8; ++var13) {
                                        var5[var6++] = t[var13 + 8];
                                    }

                                    var4 = var11;
                                    ++var7;
                                    continue;
                                }

                                var10000 = null;
                                return (byte[]) var10000;
                            }

                            var10000 = null;
                            return (byte[]) var10000;
                        }

                        byte[] var18 = var5;
                        return var18;
                    }
                }
            } catch (IOException var16) {
                return null;
            }
        }
    }

    @ObfuscatedName("q")
    public boolean q(int var1, byte[] var2, int var3) {
        RSRandomAccessFileChannel var4 = this.q;
        synchronized (this.q) {
            if (var3 >= 0 && var3 <= this.l) {
                boolean var5 = this.i(var1, var2, var3, true);
                if (!var5) {
                    var5 = this.i(var1, var2, var3, false);
                }

                return var5;
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    @ObfuscatedName("i")
    boolean i(int var1, byte[] var2, int var3, boolean var4) {
        RSRandomAccessFileChannel var5 = this.q;
        synchronized (this.q) {
            try {
                int var6;
                boolean var10000;
                if (var4) {
                    if (this.a.i() < (long) (var1 * 6 + 6)) {
                        var10000 = false;
                        return var10000;
                    }

                    this.a.q((long) (var1 * 6));
                    this.a.l(t, 0, 6);
                    var6 = (t[5] & 255) + ((t[3] & 255) << 16) + ((t[4] & 255) << 8);
                    if (var6 <= 0 || (long) var6 > this.q.i() / 520L) {
                        var10000 = false;
                        return var10000;
                    }
                } else {
                    var6 = (int) ((this.q.i() + 519L) / 520L);
                    if (var6 == 0) {
                        var6 = 1;
                    }
                }

                t[0] = (byte) (var3 >> 16);
                t[1] = (byte) (var3 >> 8);
                t[2] = (byte) var3;
                t[3] = (byte) (var6 >> 16);
                t[4] = (byte) (var6 >> 8);
                t[5] = (byte) var6;
                this.a.q((long) (var1 * 6));
                this.a.e(t, 0, 6);
                int var7 = 0;
                int var8 = 0;

                while (true) {
                    if (var7 < var3) {
                        label145: {
                            int var9 = 0;
                            int var14;
                            if (var4) {
                                this.q.q((long) (var6 * 520));

                                try {
                                    this.q.l(t, 0, 8);
                                } catch (EOFException var16) {
                                    break label145;
                                }

                                var14 = (t[1] & 255) + ((t[0] & 255) << 8);
                                int var11 = (t[3] & 255) + ((t[2] & 255) << 8);
                                var9 = ((t[5] & 255) << 8) + ((t[4] & 255) << 16) + (t[6] & 255);
                                int var12 = t[7] & 255;
                                if (var14 != var1 || var11 != var8 || var12 != this.i) {
                                    var10000 = false;
                                    return var10000;
                                }

                                if (var9 < 0 || (long) var9 > this.q.i() / 520L) {
                                    var10000 = false;
                                    return var10000;
                                }
                            }

                            if (var9 == 0) {
                                var4 = false;
                                var9 = (int) ((this.q.i() + 519L) / 520L);
                                if (var9 == 0) {
                                    ++var9;
                                }

                                if (var6 == var9) {
                                    ++var9;
                                }
                            }

                            if (var3 - var7 <= 512) {
                                var9 = 0;
                            }

                            t[0] = (byte) (var1 >> 8);
                            t[1] = (byte) var1;
                            t[2] = (byte) (var8 >> 8);
                            t[3] = (byte) var8;
                            t[4] = (byte) (var9 >> 16);
                            t[5] = (byte) (var9 >> 8);
                            t[6] = (byte) var9;
                            t[7] = (byte) this.i;
                            this.q.q((long) (var6 * 520));
                            this.q.e(t, 0, 8);
                            var14 = var3 - var7;
                            if (var14 > 512) {
                                var14 = 512;
                            }

                            this.q.e(var2, var7, var14);
                            var7 += var14;
                            var6 = var9;
                            ++var8;
                            continue;
                        }
                    }

                    var10000 = true;
                    return var10000;
                }
            } catch (IOException var17) {
                return false;
            }
        }
    }
}
