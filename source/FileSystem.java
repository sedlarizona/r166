import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jc")
public abstract class FileSystem {

    @ObfuscatedName("v")
    static Inflater v = new Inflater();

    @ObfuscatedName("z")
    static int z = 0;

    @ObfuscatedName("fg")
    static int[] fg;

    @ObfuscatedName("q")
    int entryIndexCount;

    @ObfuscatedName("i")
    int[] entryIndices;

    @ObfuscatedName("a")
    int[] entryIdentifiers;

    @ObfuscatedName("l")
    IdentityTable entryIdentityTable;

    @ObfuscatedName("b")
    int[] entryCrcs;

    @ObfuscatedName("e")
    int[] childIndexCounts;

    @ObfuscatedName("x")
    int[] entryChildCounts;

    @ObfuscatedName("p")
    int[][] childIndices;

    @ObfuscatedName("g")
    int[][] childIdentifiers;

    @ObfuscatedName("n")
    IdentityTable[] childIdentityTables;

    @ObfuscatedName("o")
    Object[] entryBuffers;

    @ObfuscatedName("c")
    Object[][] childBuffers;

    @ObfuscatedName("u")
    public int discardUnpacked;

    @ObfuscatedName("j")
    boolean j;

    @ObfuscatedName("k")
    boolean k;

    FileSystem(boolean var1, boolean var2) {
        this.j = var1;
        this.k = var2;
    }

    @ObfuscatedName("t")
    void t(byte[] var1) {
        this.discardUnpacked = am.q(var1, var1.length);
        ByteBuffer var2 = new ByteBuffer(gq.au(var1));
        int var3 = var2.av();
        if (var3 >= 5 && var3 <= 7) {
            if (var3 >= 6) {
                var2.ap();
            }

            int var4 = var2.av();
            if (var3 >= 7) {
                this.entryIndexCount = var2.as();
            } else {
                this.entryIndexCount = var2.ae();
            }

            int var5 = 0;
            int var6 = -1;
            this.entryIndices = new int[this.entryIndexCount];
            int var7;
            if (var3 >= 7) {
                for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                    this.entryIndices[var7] = var5 += var2.as();
                    if (this.entryIndices[var7] > var6) {
                        var6 = this.entryIndices[var7];
                    }
                }
            } else {
                for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                    this.entryIndices[var7] = var5 += var2.ae();
                    if (this.entryIndices[var7] > var6) {
                        var6 = this.entryIndices[var7];
                    }
                }
            }

            this.entryCrcs = new int[var6 + 1];
            this.childIndexCounts = new int[var6 + 1];
            this.entryChildCounts = new int[var6 + 1];
            this.childIndices = new int[var6 + 1][];
            this.entryBuffers = new Object[var6 + 1];
            this.childBuffers = new Object[var6 + 1][];
            if (var4 != 0) {
                this.entryIdentifiers = new int[var6 + 1];

                for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                    this.entryIdentifiers[this.entryIndices[var7]] = var2.ap();
                }

                this.entryIdentityTable = new IdentityTable(this.entryIdentifiers);
            }

            for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                this.entryCrcs[this.entryIndices[var7]] = var2.ap();
            }

            for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                this.childIndexCounts[this.entryIndices[var7]] = var2.ap();
            }

            for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                this.entryChildCounts[this.entryIndices[var7]] = var2.ae();
            }

            int var8;
            int var9;
            int var10;
            int var11;
            int var12;
            if (var3 >= 7) {
                for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                    var8 = this.entryIndices[var7];
                    var9 = this.entryChildCounts[var8];
                    var5 = 0;
                    var10 = -1;
                    this.childIndices[var8] = new int[var9];

                    for (var11 = 0; var11 < var9; ++var11) {
                        var12 = this.childIndices[var8][var11] = var5 += var2.as();
                        if (var12 > var10) {
                            var10 = var12;
                        }
                    }

                    this.childBuffers[var8] = new Object[var10 + 1];
                }
            } else {
                for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                    var8 = this.entryIndices[var7];
                    var9 = this.entryChildCounts[var8];
                    var5 = 0;
                    var10 = -1;
                    this.childIndices[var8] = new int[var9];

                    for (var11 = 0; var11 < var9; ++var11) {
                        var12 = this.childIndices[var8][var11] = var5 += var2.ae();
                        if (var12 > var10) {
                            var10 = var12;
                        }
                    }

                    this.childBuffers[var8] = new Object[var10 + 1];
                }
            }

            if (var4 != 0) {
                this.childIdentifiers = new int[var6 + 1][];
                this.childIdentityTables = new IdentityTable[var6 + 1];

                for (var7 = 0; var7 < this.entryIndexCount; ++var7) {
                    var8 = this.entryIndices[var7];
                    var9 = this.entryChildCounts[var8];
                    this.childIdentifiers[var8] = new int[this.childBuffers[var8].length];

                    for (var10 = 0; var10 < var9; ++var10) {
                        this.childIdentifiers[var8][this.childIndices[var8][var10]] = var2.ap();
                    }

                    this.childIdentityTables[var8] = new IdentityTable(this.childIdentifiers[var8]);
                }
            }

        } else {
            throw new RuntimeException("");
        }
    }

    @ObfuscatedName("q")
    void q(int var1) {
    }

    @ObfuscatedName("i")
    public byte[] i(int var1, int var2) {
        return this.a(var1, var2, (int[]) null);
    }

    @ObfuscatedName("a")
    public byte[] a(int var1, int var2, int[] var3) {
        if (var1 >= 0 && var1 < this.childBuffers.length && this.childBuffers[var1] != null && var2 >= 0
                && var2 < this.childBuffers[var1].length) {
            if (this.childBuffers[var1][var2] == null) {
                boolean var4 = this.y(var1, var3);
                if (!var4) {
                    this.k(var1);
                    var4 = this.y(var1, var3);
                    if (!var4) {
                        return null;
                    }
                }
            }

            byte[] var5 = ClientPreferences.p(this.childBuffers[var1][var2], false);
            if (this.k) {
                this.childBuffers[var1][var2] = null;
            }

            return var5;
        } else {
            return null;
        }
    }

    @ObfuscatedName("l")
    public boolean l(int var1, int var2) {
        if (var1 >= 0 && var1 < this.childBuffers.length && this.childBuffers[var1] != null && var2 >= 0
                && var2 < this.childBuffers[var1].length) {
            if (this.childBuffers[var1][var2] != null) {
                return true;
            } else if (this.entryBuffers[var1] != null) {
                return true;
            } else {
                this.k(var1);
                return this.entryBuffers[var1] != null;
            }
        } else {
            return false;
        }
    }

    @ObfuscatedName("b")
    public boolean b(int var1) {
        if (this.childBuffers.length == 1) {
            return this.l(0, var1);
        } else if (this.childBuffers[var1].length == 1) {
            return this.l(var1, 0);
        } else {
            throw new RuntimeException();
        }
    }

    @ObfuscatedName("e")
    public boolean e(int var1) {
        if (this.entryBuffers[var1] != null) {
            return true;
        } else {
            this.k(var1);
            return this.entryBuffers[var1] != null;
        }
    }

    @ObfuscatedName("x")
    public boolean x() {
        boolean var1 = true;

        for (int var2 = 0; var2 < this.entryIndices.length; ++var2) {
            int var3 = this.entryIndices[var2];
            if (this.entryBuffers[var3] == null) {
                this.k(var3);
                if (this.entryBuffers[var3] == null) {
                    var1 = false;
                }
            }
        }

        return var1;
    }

    @ObfuscatedName("p")
    int p(int var1) {
        return this.entryBuffers[var1] != null ? 100 : 0;
    }

    @ObfuscatedName("o")
    public byte[] o(int var1) {
        if (this.childBuffers.length == 1) {
            return this.i(0, var1);
        } else if (this.childBuffers[var1].length == 1) {
            return this.i(var1, 0);
        } else {
            throw new RuntimeException();
        }
    }

    @ObfuscatedName("c")
    public byte[] c(int var1, int var2) {
        if (var1 >= 0 && var1 < this.childBuffers.length && this.childBuffers[var1] != null && var2 >= 0
                && var2 < this.childBuffers[var1].length) {
            if (this.childBuffers[var1][var2] == null) {
                boolean var3 = this.y(var1, (int[]) null);
                if (!var3) {
                    this.k(var1);
                    var3 = this.y(var1, (int[]) null);
                    if (!var3) {
                        return null;
                    }
                }
            }

            byte[] var4 = ClientPreferences.p(this.childBuffers[var1][var2], false);
            return var4;
        } else {
            return null;
        }
    }

    @ObfuscatedName("u")
    public byte[] u(int var1) {
        if (this.childBuffers.length == 1) {
            return this.c(0, var1);
        } else if (this.childBuffers[var1].length == 1) {
            return this.c(var1, 0);
        } else {
            throw new RuntimeException();
        }
    }

    @ObfuscatedName("k")
    void k(int var1) {
    }

    @ObfuscatedName("z")
    public int[] z(int var1) {
        return this.childIndices[var1];
    }

    @ObfuscatedName("w")
    public int w(int var1) {
        return this.childBuffers[var1].length;
    }

    @ObfuscatedName("s")
    public int s() {
        return this.childBuffers.length;
    }

    @ObfuscatedName("d")
    public void d() {
        for (int var1 = 0; var1 < this.entryBuffers.length; ++var1) {
            this.entryBuffers[var1] = null;
        }

    }

    @ObfuscatedName("f")
    public void f(int var1) {
        for (int var2 = 0; var2 < this.childBuffers[var1].length; ++var2) {
            this.childBuffers[var1][var2] = null;
        }

    }

    @ObfuscatedName("r")
    public void r() {
        for (int var1 = 0; var1 < this.childBuffers.length; ++var1) {
            if (this.childBuffers[var1] != null) {
                for (int var2 = 0; var2 < this.childBuffers[var1].length; ++var2) {
                    this.childBuffers[var1][var2] = null;
                }
            }
        }

    }

    @ObfuscatedName("y")
    boolean y(int var1, int[] var2) {
        if (this.entryBuffers[var1] == null) {
            return false;
        } else {
            int var3 = this.entryChildCounts[var1];
            int[] var4 = this.childIndices[var1];
            Object[] var5 = this.childBuffers[var1];
            boolean var6 = true;

            for (int var7 = 0; var7 < var3; ++var7) {
                if (var5[var4[var7]] == null) {
                    var6 = false;
                    break;
                }
            }

            if (var6) {
                return true;
            } else {
                byte[] var18;
                if (var2 != null && (var2[0] != 0 || var2[1] != 0 || var2[2] != 0 || var2[3] != 0)) {
                    var18 = ClientPreferences.p(this.entryBuffers[var1], true);
                    ByteBuffer var8 = new ByteBuffer(var18);
                    var8.ab(var2, 5, var8.buffer.length);
                } else {
                    var18 = ClientPreferences.p(this.entryBuffers[var1], false);
                }

                byte[] var20 = gq.au(var18);
                if (this.j) {
                    this.entryBuffers[var1] = null;
                }

                if (var3 > 1) {
                    int var9 = var20.length;
                    --var9;
                    int var10 = var20[var9] & 255;
                    var9 -= var10 * var3 * 4;
                    ByteBuffer var11 = new ByteBuffer(var20);
                    int[] var12 = new int[var3];
                    var11.index = var9;

                    int var14;
                    int var15;
                    for (int var13 = 0; var13 < var10; ++var13) {
                        var14 = 0;

                        for (var15 = 0; var15 < var3; ++var15) {
                            var14 += var11.ap();
                            var12[var15] += var14;
                        }
                    }

                    byte[][] var19 = new byte[var3][];

                    for (var14 = 0; var14 < var3; ++var14) {
                        var19[var14] = new byte[var12[var14]];
                        var12[var14] = 0;
                    }

                    var11.index = var9;
                    var14 = 0;

                    for (var15 = 0; var15 < var10; ++var15) {
                        int var16 = 0;

                        for (int var17 = 0; var17 < var3; ++var17) {
                            var16 += var11.ap();
                            System.arraycopy(var20, var14, var19[var17], var12[var17], var16);
                            var12[var17] += var16;
                            var14 += var16;
                        }
                    }

                    for (var15 = 0; var15 < var3; ++var15) {
                        if (!this.k) {
                            var5[var4[var15]] = hi.x(var19[var15], false);
                        } else {
                            var5[var4[var15]] = var19[var15];
                        }
                    }
                } else if (!this.k) {
                    var5[var4[0]] = hi.x(var20, false);
                } else {
                    var5[var4[0]] = var20;
                }

                return true;
            }
        }
    }

    @ObfuscatedName("h")
    public int h(String var1) {
        var1 = var1.toLowerCase();
        return this.entryIdentityTable.t(ey.e(var1));
    }

    @ObfuscatedName("av")
    public int av(int var1, String var2) {
        var2 = var2.toLowerCase();
        return this.childIdentityTables[var1].t(ey.e(var2));
    }

    @ObfuscatedName("aj")
    public boolean aj(String var1, String var2) {
        var1 = var1.toLowerCase();
        var2 = var2.toLowerCase();
        int var3 = this.entryIdentityTable.t(ey.e(var1));
        if (var3 < 0) {
            return false;
        } else {
            int var4 = this.childIdentityTables[var3].t(ey.e(var2));
            return var4 >= 0;
        }
    }

    @ObfuscatedName("ae")
    public byte[] ae(String var1, String var2) {
        var1 = var1.toLowerCase();
        var2 = var2.toLowerCase();
        int var3 = this.entryIdentityTable.t(ey.e(var1));
        int var4 = this.childIdentityTables[var3].t(ey.e(var2));
        return this.i(var3, var4);
    }

    @ObfuscatedName("am")
    public boolean am(String var1, String var2) {
        var1 = var1.toLowerCase();
        var2 = var2.toLowerCase();
        int var3 = this.entryIdentityTable.t(ey.e(var1));
        int var4 = this.childIdentityTables[var3].t(ey.e(var2));
        return this.l(var3, var4);
    }

    @ObfuscatedName("az")
    public boolean az(String var1) {
        var1 = var1.toLowerCase();
        int var2 = this.entryIdentityTable.t(ey.e(var1));
        return this.e(var2);
    }

    @ObfuscatedName("ap")
    public void ap(String var1) {
        var1 = var1.toLowerCase();
        int var2 = this.entryIdentityTable.t(ey.e(var1));
        if (var2 >= 0) {
            this.q(var2);
        }
    }

    @ObfuscatedName("ah")
    public int ah(String var1) {
        var1 = var1.toLowerCase();
        int var2 = this.entryIdentityTable.t(ey.e(var1));
        return this.p(var2);
    }

    @ObfuscatedName("iv")
    static final void iv(int var0, int var1, int var2, int var3, int var4, int var5) {
        int var6 = var2 - var0;
        int var7 = var3 - var1;
        int var8 = var6 >= 0 ? var6 : -var6;
        int var9 = var7 >= 0 ? var7 : -var7;
        int var10 = var8;
        if (var8 < var9) {
            var10 = var9;
        }

        if (var10 != 0) {
            int var11 = (var6 << 16) / var10;
            int var12 = (var7 << 16) / var10;
            if (var12 <= var11) {
                var11 = -var11;
            } else {
                var12 = -var12;
            }

            int var13 = var5 * var12 >> 17;
            int var14 = var5 * var12 + 1 >> 17;
            int var15 = var5 * var11 >> 17;
            int var16 = var5 * var11 + 1 >> 17;
            var0 -= li.az;
            var1 -= li.ae;
            int var17 = var0 + var13;
            int var18 = var0 - var14;
            int var19 = var0 + var6 - var14;
            int var20 = var0 + var13 + var6;
            int var21 = var15 + var1;
            int var22 = var1 - var16;
            int var23 = var7 + var1 - var16;
            int var24 = var15 + var7 + var1;
            eu.p(var17, var18, var19);
            eu.u(var21, var22, var23, var17, var18, var19, var4);
            eu.p(var17, var19, var20);
            eu.u(var21, var23, var24, var17, var19, var20, var4);
        }
    }
}
