import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ly")
public final class ly {

    @ObfuscatedName("t")
    static final char[] t = new char[] { '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9' };

    @ObfuscatedName("g")
    static int g;

    @ObfuscatedName("cl")
    static ju cl;

    @ObfuscatedName("z")
    static void z() {
        ci.a = null;
        er.l = null;
        le.b = null;
        GrandExchangeOffer.e = null;
        le.x = null;
        ClassStructureNode.p = null;
    }
}
