import java.applet.Applet;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ft")
public class RSException extends RuntimeException {

    @ObfuscatedName("q")
    public static Applet q;

    @ObfuscatedName("l")
    String text;

    @ObfuscatedName("b")
    Throwable throwable;

    @ObfuscatedName("q")
    static synchronized byte[] q(int var0) {
        return gc.t(var0, false);
    }

    @ObfuscatedName("jp")
    static RTComponent jp(RTComponent var0) {
        int var1 = GrandExchangeOffer.q(u.jr(var0));
        if (var1 == 0) {
            return null;
        } else {
            for (int var2 = 0; var2 < var1; ++var2) {
                var0 = Inflater.t(var0.parentId);
                if (var0 == null) {
                    return null;
                }
            }

            return var0;
        }
    }
}
