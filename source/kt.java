import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kt")
public class kt extends kv {

    @ObfuscatedName("ii")
    static int ii;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("t")
    int t(kt var1) {
        return this.t - var1.t;
    }

    @ObfuscatedName("aj")
    public int aj(kv var1) {
        return this.t((kt) var1);
    }

    public int compareTo(Object var1) {
        return this.t((kt) var1);
    }

    @ObfuscatedName("p")
    static lk p() {
        lk var0 = new lk();
        var0.e = le.q;
        var0.x = le.i;
        var0.l = ci.a[0];
        var0.b = er.l[0];
        var0.i = le.b[0];
        var0.a = GrandExchangeOffer.e[0];
        var0.q = le.x;
        var0.t = ClassStructureNode.p[0];
        ly.z();
        return var0;
    }
}
