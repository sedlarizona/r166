import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fr")
public abstract class fr {

    @ObfuscatedName("bt")
    static lk[] bt;

    @ObfuscatedName("t")
    public abstract void t();

    @ObfuscatedName("q")
    public abstract int q(int var1, int var2);

    @ObfuscatedName("t")
    public static kg[] t() {
        return new kg[] { kg.u, kg.a, kg.g, kg.i, kg.e, kg.c, kg.v, kg.j, kg.p, kg.t, kg.q, kg.b, kg.x, kg.l, kg.o,
                kg.n };
    }

    @ObfuscatedName("q")
    public static void q(int var0, int var1) {
        Varpbit var2 = ji.t(var0);
        int var3 = var2.varp;
        int var4 = var2.low;
        int var5 = var2.high;
        int var6 = iv.t[var5 - var4];
        if (var1 < 0 || var1 > var6) {
            var1 = 0;
        }

        var6 <<= var4;
        iv.varps[var3] = iv.varps[var3] & ~var6 | var1 << var4 & var6;
    }

    @ObfuscatedName("c")
    public static void c() {
        kf.a.a();
        kf.l.a();
    }
}
