import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ev")
public interface ev {

    @ObfuscatedName("i")
    int[] i(int var1);

    @ObfuscatedName("a")
    int a(int var1);

    @ObfuscatedName("l")
    boolean l(int var1);

    @ObfuscatedName("b")
    boolean b(int var1);
}
