import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("w")
final class w implements Comparator {

    @ObfuscatedName("t")
    int t(u var1, u var2) {
        return var1.t().compareTo(var2.t());
    }

    public int compare(Object var1, Object var2) {
        return this.t((u) var1, (u) var2);
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }

    @ObfuscatedName("i")
    static ChatboxMessage i(int var0) {
        return (ChatboxMessage) cg.q.t((long) var0);
    }
}
