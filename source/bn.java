import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bn")
public class bn {

    @ObfuscatedName("bn")
    static int bn;

    @ObfuscatedName("dh")
    static ju dh;

    @ObfuscatedName("fd")
    static int fd;

    @ObfuscatedName("fr")
    static Sprite[] fr;

    @ObfuscatedName("t")
    final ju t;

    @ObfuscatedName("q")
    final int q;

    @ObfuscatedName("i")
    int i = 0;

    bn(ju var1, String var2) {
        this.t = var1;
        this.q = var1.s();
    }

    @ObfuscatedName("t")
    boolean t() {
        this.i = 0;

        for (int var1 = 0; var1 < this.q; ++var1) {
            if (!this.t.dn(var1) || this.t.dy(var1)) {
                ++this.i;
            }
        }

        return this.i >= this.q;
    }

    @ObfuscatedName("q")
    static void addMessage(int var0, String var1, String var2, String var3) {
        Chatbox var4 = (Chatbox) cg.chatboxes.get(var0);
        if (var4 == null) {
            var4 = new Chatbox();
            cg.chatboxes.put(var0, var4);
        }

        ChatboxMessage var5 = var4.t(var0, var1, var2, var3);
        cg.q.q(var5, (long) var5.index);
        cg.i.q(var5);
        Client.mc = Client.mn;
    }

    @ObfuscatedName("b")
    public static void b(int var0, FileSystem var1, String var2, String var3, int var4, boolean var5) {
        int var6 = var1.h(var2);
        int var7 = var1.av(var6, var3);
        Npc.e(var0, var1, var6, var7, var4, var5);
    }

    @ObfuscatedName("gz")
    static final void gz(int var0, int var1, boolean var2) {
        if (!var2 || var0 != ab.ef || gd.eq != var1) {
            ab.ef = var0;
            gd.eq = var1;
            d.ea(25);
            f.ga("Loading - please wait.", true);
            int var3 = an.regionBaseX;
            int var4 = PlayerComposite.ep;
            an.regionBaseX = (var0 - 6) * 8;
            PlayerComposite.ep = (var1 - 6) * 8;
            int var5 = an.regionBaseX - var3;
            int var6 = PlayerComposite.ep - var4;
            var3 = an.regionBaseX;
            var4 = PlayerComposite.ep;

            int var7;
            int var9;
            for (var7 = 0; var7 < 32768; ++var7) {
                Npc var8 = Client.loadedNpcs[var7];
                if (var8 != null) {
                    for (var9 = 0; var9 < 10; ++var9) {
                        var8.co[var9] -= var5;
                        var8.cv[var9] -= var6;
                    }

                    var8.regionX -= var5 * 128;
                    var8.regionY -= var6 * 128;
                }
            }

            for (var7 = 0; var7 < 2048; ++var7) {
                Player var21 = Client.loadedPlayers[var7];
                if (var21 != null) {
                    for (var9 = 0; var9 < 10; ++var9) {
                        var21.co[var9] -= var5;
                        var21.cv[var9] -= var6;
                    }

                    var21.regionX -= var5 * 128;
                    var21.regionY -= var6 * 128;
                }
            }

            byte var20 = 0;
            byte var18 = 104;
            byte var22 = 1;
            if (var5 < 0) {
                var20 = 103;
                var18 = -1;
                var22 = -1;
            }

            byte var10 = 0;
            byte var11 = 104;
            byte var12 = 1;
            if (var6 < 0) {
                var10 = 103;
                var11 = -1;
                var12 = -1;
            }

            int var14;
            for (int var13 = var20; var18 != var13; var13 += var22) {
                for (var14 = var10; var14 != var11; var14 += var12) {
                    int var15 = var13 + var5;
                    int var16 = var14 + var6;

                    for (int var17 = 0; var17 < 4; ++var17) {
                        if (var15 >= 0 && var16 >= 0 && var15 < 104 && var16 < 104) {
                            Client.loadedGroundItems[var17][var13][var14] = Client.loadedGroundItems[var17][var15][var16];
                        } else {
                            Client.loadedGroundItems[var17][var13][var14] = null;
                        }
                    }
                }
            }

            for (bl var19 = (bl) Client.jw.e(); var19 != null; var19 = (bl) Client.jw.p()) {
                var19.i -= var5;
                var19.a -= var6;
                if (var19.i < 0 || var19.a < 0 || var19.i >= 104 || var19.a >= 104) {
                    var19.kc();
                }
            }

            if (Client.destinationX != 0) {
                Client.destinationX -= var5;
                Client.destinationY -= var6;
            }

            Client.audioEffectCount = 0;
            Client.px = false;
            RuneScriptVM.go -= var5 << 7;
            n.gp -= var6 << 7;
            am.gz -= var5 << 7;
            gf.gw -= var6 << 7;
            Client.oj = -1;
            Client.jf.t();
            Client.loadedProjectiles.t();

            for (var14 = 0; var14 < 4; ++var14) {
                Client.collisionMaps[var14].t();
            }

        }
    }
}
