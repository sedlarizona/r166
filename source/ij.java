import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ij")
public enum ij implements gl {
    @ObfuscatedName("t")
    t(0, 0), @ObfuscatedName("q")
    q(1, 0), @ObfuscatedName("i")
    i(2, 0), @ObfuscatedName("a")
    a(3, 0), @ObfuscatedName("l")
    l(9, 2), @ObfuscatedName("b")
    b(4, 1), @ObfuscatedName("e")
    e(5, 1), @ObfuscatedName("x")
    x(6, 1), @ObfuscatedName("p")
    p(7, 1), @ObfuscatedName("g")
    g(8, 1), @ObfuscatedName("n")
    n(12, 2), @ObfuscatedName("o")
    o(13, 2), @ObfuscatedName("c")
    c(14, 2), @ObfuscatedName("v")
    v(15, 2), @ObfuscatedName("u")
    u(16, 2), @ObfuscatedName("j")
    j(17, 2), @ObfuscatedName("k")
    k(18, 2), @ObfuscatedName("z")
    z(19, 2), @ObfuscatedName("w")
    w(20, 2), @ObfuscatedName("s")
    s(21, 2), @ObfuscatedName("d")
    d(10, 2), @ObfuscatedName("f")
    f(11, 2), @ObfuscatedName("r")
    r(22, 3);

    @ObfuscatedName("y")
    public final int y;

    ij(int var3, int var4) {
        this.y = var3;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.y;
    }
}
