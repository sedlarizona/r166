import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hq")
public final class FixedSizeDeque implements Iterable {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    Node[] buckets;

    @ObfuscatedName("i")
    Node i;

    @ObfuscatedName("a")
    Node current;

    @ObfuscatedName("l")
    int size = 0;

    public FixedSizeDeque(int var1) {
        this.t = var1;
        this.buckets = new Node[var1];

        for (int var2 = 0; var2 < var1; ++var2) {
            Node var3 = this.buckets[var2] = new Node();
            var3.next = var3;
            var3.previous = var3;
        }

    }

    @ObfuscatedName("t")
    public Node t(long var1) {
        Node var3 = this.buckets[(int) (var1 & (long) (this.t - 1))];

        for (this.i = var3.next; var3 != this.i; this.i = this.i.next) {
            if (this.i.uid == var1) {
                Node var4 = this.i;
                this.i = this.i.next;
                return var4;
            }
        }

        this.i = null;
        return null;
    }

    @ObfuscatedName("q")
    public void q(Node var1, long var2) {
        if (var1.previous != null) {
            var1.kc();
        }

        Node var4 = this.buckets[(int) (var2 & (long) (this.t - 1))];
        var1.previous = var4.previous;
        var1.next = var4;
        var1.previous.next = var1;
        var1.next.previous = var1;
        var1.uid = var2;
    }

    @ObfuscatedName("i")
    public void i() {
        for (int var1 = 0; var1 < this.t; ++var1) {
            Node var2 = this.buckets[var1];

            while (true) {
                Node var3 = var2.next;
                if (var3 == var2) {
                    break;
                }

                var3.kc();
            }
        }

        this.i = null;
        this.current = null;
    }

    @ObfuscatedName("a")
    public Node a() {
        this.size = 0;
        return this.l();
    }

    @ObfuscatedName("l")
    public Node l() {
        Node var1;
        if (this.size > 0 && this.buckets[this.size - 1] != this.current) {
            var1 = this.current;
            this.current = var1.next;
            return var1;
        } else {
            do {
                if (this.size >= this.t) {
                    return null;
                }

                var1 = this.buckets[this.size++].next;
            } while (var1 == this.buckets[this.size - 1]);

            this.current = var1.next;
            return var1;
        }
    }

    public Iterator iterator() {
        return new FixedSizeDequeIterator(this);
    }
}
