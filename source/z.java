import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("z")
final class z implements Comparator {

    @ObfuscatedName("l")
    static int l;

    @ObfuscatedName("x")
    public static boolean x;

    @ObfuscatedName("t")
    int t(u var1, u var2) {
        return var1.i.itemPrice < var2.i.itemPrice ? -1 : (var2.i.itemPrice == var1.i.itemPrice ? 0 : 1);
    }

    public int compare(Object var1, Object var2) {
        return this.t((u) var1, (u) var2);
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }

    @ObfuscatedName("p")
    static final int p(int var0, int var1) {
        int var2 = MouseTracker.o(var0 - 1, var1 - 1) + MouseTracker.o(var0 + 1, var1 - 1)
                + MouseTracker.o(var0 - 1, 1 + var1) + MouseTracker.o(1 + var0, 1 + var1);
        int var3 = MouseTracker.o(var0 - 1, var1) + MouseTracker.o(var0 + 1, var1) + MouseTracker.o(var0, var1 - 1)
                + MouseTracker.o(var0, 1 + var1);
        int var4 = MouseTracker.o(var0, var1);
        return var2 / 16 + var3 / 8 + var4 / 4;
    }

    @ObfuscatedName("c")
    public static int c(String var0) {
        return var0.length() + 1;
    }

    @ObfuscatedName("k")
    static int k(int var0, RuneScript var1, boolean var2) {
        RTComponent var3;
        if (var0 == 2700) {
            var3 = Inflater.t(cs.e[--b.menuX]);
            cs.e[++b.menuX - 1] = var3.itemId;
            return 1;
        } else if (var0 == 2701) {
            var3 = Inflater.t(cs.e[--b.menuX]);
            if (var3.itemId != -1) {
                cs.e[++b.menuX - 1] = var3.itemQuantity;
            } else {
                cs.e[++b.menuX - 1] = 0;
            }

            return 1;
        } else if (var0 == 2702) {
            int var5 = cs.e[--b.menuX];
            SubWindow var4 = (SubWindow) Client.subWindowTable.t((long) var5);
            if (var4 != null) {
                cs.e[++b.menuX - 1] = 1;
            } else {
                cs.e[++b.menuX - 1] = 0;
            }

            return 1;
        } else if (var0 == 2706) {
            cs.e[++b.menuX - 1] = Client.hudIndex;
            return 1;
        } else {
            return 2;
        }
    }

    @ObfuscatedName("hx")
    static void hx() {
        Client.menuSize = 0;
        Client.menuOpen = false;
    }
}
