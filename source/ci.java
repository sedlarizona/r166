import java.text.DecimalFormat;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ci")
public class ci {

    @ObfuscatedName("t")
    static boolean t;

    @ObfuscatedName("q")
    static int worldCount = 0;

    @ObfuscatedName("i")
    static lk i;

    @ObfuscatedName("a")
    static int[] a;

    @ObfuscatedName("p")
    static lk[] p;

    @ObfuscatedName("o")
    static lk o;

    @ObfuscatedName("c")
    static lk c;

    @ObfuscatedName("z")
    static int z;

    @ObfuscatedName("d")
    static int[] d;

    @ObfuscatedName("m")
    static int m;

    @ObfuscatedName("ay")
    static int ay;

    @ObfuscatedName("am")
    static int am;

    @ObfuscatedName("az")
    static int az;

    @ObfuscatedName("ap")
    static int ap;

    @ObfuscatedName("ah")
    static int ah;

    @ObfuscatedName("au")
    static int au;

    @ObfuscatedName("ax")
    static String ax;

    @ObfuscatedName("ar")
    static int loginState;

    @ObfuscatedName("an")
    static String an;

    @ObfuscatedName("ai")
    static String loginLine1;

    @ObfuscatedName("al")
    static String loginLine2;

    @ObfuscatedName("at")
    static String loginLine3;

    @ObfuscatedName("ag")
    static String username;

    @ObfuscatedName("as")
    static String password;

    @ObfuscatedName("aw")
    static boolean aw;

    @ObfuscatedName("ad")
    static boolean ad;

    @ObfuscatedName("bg")
    static boolean bg;

    @ObfuscatedName("bk")
    static boolean bk;

    @ObfuscatedName("be")
    static int be;

    @ObfuscatedName("bs")
    static boolean isWorldSelectorOpen;

    @ObfuscatedName("bq")
    static int bq;

    @ObfuscatedName("bz")
    static long bz;

    @ObfuscatedName("bx")
    static long bx;

    @ObfuscatedName("jh")
    static int menuY;

    static {
        z = worldCount + 202;
        d = new int[256];
        m = 0;
        ay = 0;
        am = 0;
        az = 0;
        ap = 0;
        ah = 0;
        au = 10;
        ax = "";
        loginState = 0;
        an = "";
        loginLine1 = "";
        loginLine2 = "";
        loginLine3 = "";
        username = "";
        password = "";
        aw = false;
        ad = false;
        bg = false;
        bk = true;
        be = 0;
        isWorldSelectorOpen = false;
        bq = -1;
        new DecimalFormat("##0.00");
        new fk();
        bz = -1L;
        bx = -1L;
    }

    @ObfuscatedName("i")
    static final void i() {
        if (!ej.a) {
            int var0 = Region.ay;
            int var1 = Region.ao;
            int var2 = Region.av;
            int var3 = Region.aj;
            byte var4 = 50;
            short var5 = 3500;
            int var6 = (ej.q - eu.c) * var4 / eu.o;
            int var7 = (ej.i - eu.v) * var4 / eu.o;
            int var8 = (ej.q - eu.c) * var5 / eu.o;
            int var9 = (ej.i - eu.v) * var5 / eu.o;
            int var10 = eu.aj(var7, var4, var1, var0);
            int var11 = eu.ae(var7, var4, var1, var0);
            var7 = var10;
            var10 = eu.aj(var9, var5, var1, var0);
            int var12 = eu.ae(var9, var5, var1, var0);
            var9 = var10;
            var10 = eu.h(var6, var11, var3, var2);
            var11 = eu.av(var6, var11, var3, var2);
            var6 = var10;
            var10 = eu.h(var8, var12, var3, var2);
            var12 = eu.av(var8, var12, var3, var2);
            b.l = (var10 + var6) / 2;
            ej.b = (var9 + var7) / 2;
            bj.e = (var12 + var11) / 2;
            Npc.x = (var10 - var6) / 2;
            VarInfo.p = (var9 - var7) / 2;
            hd.g = (var12 - var11) / 2;
            lm.n = Math.abs(Npc.x);
            ej.o = Math.abs(VarInfo.p);
            ej.c = Math.abs(hd.g);
        }
    }

    @ObfuscatedName("fy")
    static void fy(int var0, int var1) {
        if (Client.os != 0 && var0 != -1) {
            CombatBarData.i(bk.dp, var0, 0, Client.os, false);
            Client.oe = true;
        }

    }
}
