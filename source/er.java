import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("er")
public class er extends ky {

    @ObfuscatedName("l")
    static int[] l;

    @ObfuscatedName("ch")
    static boolean ch;

    @ObfuscatedName("t")
    final boolean t;

    public er(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (var1.l != 0 && var2.l != 0) {
            return this.t ? var1.b - var2.b : var2.b - var1.b;
        } else {
            return this.p(var1, var2);
        }
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("i")
    public static boolean i(int var0) {
        return (var0 >> 28 & 1) != 0;
    }

    @ObfuscatedName("b")
    public static int b() {
        return ad.cn;
    }

    @ObfuscatedName("x")
    public static void x() {
        try {
            if (hi.audioTrackId == 1) {
                int var0 = hi.audioTask.q();
                if (var0 > 0 && hi.audioTask.k()) {
                    var0 -= i.g;
                    if (var0 < 0) {
                        var0 = 0;
                    }

                    hi.audioTask.t(var0);
                    return;
                }

                hi.audioTask.u();
                hi.audioTask.l();
                if (hi.b != null) {
                    hi.audioTrackId = 2;
                } else {
                    hi.audioTrackId = 0;
                }

                fv.o = null;
                jq.c = null;
            }
        } catch (Exception var2) {
            var2.printStackTrace();
            hi.audioTask.u();
            hi.audioTrackId = 0;
            fv.o = null;
            jq.c = null;
            hi.b = null;
        }

    }

    @ObfuscatedName("hs")
    static final boolean hs(int var0) {
        if (var0 < 0) {
            return false;
        } else {
            int var1 = Client.menuOpcodes[var0];
            if (var1 >= 2000) {
                var1 -= 2000;
            }

            return var1 == 1007;
        }
    }

    @ObfuscatedName("ia")
    static void ia(int var0, int var1) {
        bq.hl(am.kt, var0, var1);
        am.kt = null;
    }
}
