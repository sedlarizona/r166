import java.util.Random;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lj")
public abstract class RSFont extends li {

    @ObfuscatedName("g")
    public static lk[] g;

    @ObfuscatedName("o")
    static int o = -1;

    @ObfuscatedName("c")
    static int c = -1;

    @ObfuscatedName("v")
    static int v = -1;

    @ObfuscatedName("u")
    static int u = -1;

    @ObfuscatedName("j")
    static int j = 0;

    @ObfuscatedName("k")
    static int k = 0;

    @ObfuscatedName("z")
    static int z = 256;

    @ObfuscatedName("w")
    static int w = 0;

    @ObfuscatedName("s")
    static int s = 0;

    @ObfuscatedName("d")
    static Random d = new Random();

    @ObfuscatedName("f")
    static String[] f = new String[100];

    @ObfuscatedName("t")
    byte[][] characterPixels = new byte[256][];

    @ObfuscatedName("q")
    int[] q;

    @ObfuscatedName("i")
    int[] i;

    @ObfuscatedName("a")
    int[] a;

    @ObfuscatedName("l")
    int[] l;

    @ObfuscatedName("b")
    int[] b;

    @ObfuscatedName("e")
    public int e = 0;

    @ObfuscatedName("x")
    public int x;

    @ObfuscatedName("p")
    public int p;

    @ObfuscatedName("n")
    byte[] n;

    RSFont(byte[] var1, int[] var2, int[] var3, int[] var4, int[] var5, int[] var6, byte[][] var7) {
        this.l = var2;
        this.b = var3;
        this.i = var4;
        this.a = var5;
        this.x(var1);
        this.characterPixels = var7;
        int var8 = Integer.MAX_VALUE;
        int var9 = Integer.MIN_VALUE;

        for (int var10 = 0; var10 < 256; ++var10) {
            if (this.b[var10] < var8 && this.a[var10] != 0) {
                var8 = this.b[var10];
            }

            if (this.b[var10] + this.a[var10] > var9) {
                var9 = this.b[var10] + this.a[var10];
            }
        }

        this.x = this.e - var8;
        this.p = var9 - this.e;
    }

    RSFont(byte[] var1) {
        this.x(var1);
    }

    @ObfuscatedName("t")
    abstract void t(byte[] var1, int var2, int var3, int var4, int var5, int var6);

    @ObfuscatedName("q")
    abstract void q(byte[] var1, int var2, int var3, int var4, int var5, int var6, int var7);

    @ObfuscatedName("x")
    void x(byte[] var1) {
        this.q = new int[256];
        int var2;
        if (var1.length == 257) {
            for (var2 = 0; var2 < this.q.length; ++var2) {
                this.q[var2] = var1[var2] & 255;
            }

            this.e = var1[256] & 255;
        } else {
            var2 = 0;

            for (int var3 = 0; var3 < 256; ++var3) {
                this.q[var3] = var1[var2++] & 255;
            }

            int[] var10 = new int[256];
            int[] var4 = new int[256];

            int var5;
            for (var5 = 0; var5 < 256; ++var5) {
                var10[var5] = var1[var2++] & 255;
            }

            for (var5 = 0; var5 < 256; ++var5) {
                var4[var5] = var1[var2++] & 255;
            }

            byte[][] var11 = new byte[256][];

            int var8;
            for (int var6 = 0; var6 < 256; ++var6) {
                var11[var6] = new byte[var10[var6]];
                byte var7 = 0;

                for (var8 = 0; var8 < var11[var6].length; ++var8) {
                    var7 += var1[var2++];
                    var11[var6][var8] = var7;
                }
            }

            byte[][] var12 = new byte[256][];

            int var13;
            for (var13 = 0; var13 < 256; ++var13) {
                var12[var13] = new byte[var10[var13]];
                byte var14 = 0;

                for (int var9 = 0; var9 < var12[var13].length; ++var9) {
                    var14 += var1[var2++];
                    var12[var13][var9] = var14;
                }
            }

            this.n = new byte[65536];

            for (var13 = 0; var13 < 256; ++var13) {
                if (var13 != 32 && var13 != 160) {
                    for (var8 = 0; var8 < 256; ++var8) {
                        if (var8 != 32 && var8 != 160) {
                            this.n[var8 + (var13 << 8)] = (byte) p(var11, var12, var4, this.q, var10, var13, var8);
                        }
                    }
                }
            }

            this.e = var4[32] + var10[32];
        }

    }

    @ObfuscatedName("o")
    int o(char var1) {
        if (var1 == 160) {
            var1 = ' ';
        }

        return this.q[ko.t(var1) & 255];
    }

    @ObfuscatedName("c")
    public int c(String var1) {
        if (var1 == null) {
            return 0;
        } else {
            int var2 = -1;
            int var3 = -1;
            int var4 = 0;

            for (int var5 = 0; var5 < var1.length(); ++var5) {
                char var6 = var1.charAt(var5);
                if (var6 == '<') {
                    var2 = var5;
                } else {
                    if (var6 == '>' && var2 != -1) {
                        String var7 = var1.substring(var2 + 1, var5);
                        var2 = -1;
                        if (var7.equals("lt")) {
                            var6 = '<';
                        } else {
                            if (!var7.equals("gt")) {
                                if (var7.startsWith("img=")) {
                                    try {
                                        int var8 = ff.i(var7.substring(4));
                                        var4 += g[var8].e;
                                        var3 = -1;
                                    } catch (Exception var10) {
                                        ;
                                    }
                                }
                                continue;
                            }

                            var6 = '>';
                        }
                    }

                    if (var6 == 160) {
                        var6 = ' ';
                    }

                    if (var2 == -1) {
                        var4 += this.q[(char) (ko.t(var6) & 255)];
                        if (this.n != null && var3 != -1) {
                            var4 += this.n[var6 + (var3 << 8)];
                        }

                        var3 = var6;
                    }
                }
            }

            return var4;
        }
    }

    @ObfuscatedName("u")
    public int u(String var1, int[] var2, String[] var3) {
        if (var1 == null) {
            return 0;
        } else {
            int var4 = 0;
            int var5 = 0;
            StringBuilder var6 = new StringBuilder(100);
            int var7 = -1;
            int var8 = 0;
            byte var9 = 0;
            int var10 = -1;
            char var11 = 0;
            int var12 = 0;
            int var13 = var1.length();

            for (int var14 = 0; var14 < var13; ++var14) {
                char var15 = var1.charAt(var14);
                if (var15 == '<') {
                    var10 = var14;
                } else {
                    if (var15 == '>' && var10 != -1) {
                        String var16 = var1.substring(var10 + 1, var14);
                        var10 = -1;
                        var6.append('<');
                        var6.append(var16);
                        var6.append('>');
                        if (var16.equals("br")) {
                            var3[var12] = var6.toString().substring(var5, var6.length());
                            ++var12;
                            var5 = var6.length();
                            var4 = 0;
                            var7 = -1;
                            var11 = 0;
                        } else if (var16.equals("lt")) {
                            var4 += this.o('<');
                            if (this.n != null && var11 != -1) {
                                var4 += this.n[(var11 << 8) + 60];
                            }

                            var11 = '<';
                        } else if (var16.equals("gt")) {
                            var4 += this.o('>');
                            if (this.n != null && var11 != -1) {
                                var4 += this.n[(var11 << 8) + 62];
                            }

                            var11 = '>';
                        } else if (var16.startsWith("img=")) {
                            try {
                                int var17 = ff.i(var16.substring(4));
                                var4 += g[var17].e;
                                var11 = 0;
                            } catch (Exception var20) {
                                ;
                            }
                        }

                        var15 = 0;
                    }

                    if (var10 == -1) {
                        if (var15 != 0) {
                            var6.append(var15);
                            var4 += this.o(var15);
                            if (this.n != null && var11 != -1) {
                                var4 += this.n[var15 + (var11 << 8)];
                            }

                            var11 = var15;
                        }

                        if (var15 == ' ') {
                            var7 = var6.length();
                            var8 = var4;
                            var9 = 1;
                        }

                        if (var2 != null && var4 > var2[var12 < var2.length ? var12 : var2.length - 1] && var7 >= 0) {
                            var3[var12] = var6.toString().substring(var5, var7 - var9);
                            ++var12;
                            var5 = var7;
                            var7 = -1;
                            var4 -= var8;
                            var11 = 0;
                        }

                        if (var15 == '-') {
                            var7 = var6.length();
                            var8 = var4;
                            var9 = 0;
                        }
                    }
                }
            }

            String var19 = var6.toString();
            if (var19.length() > var5) {
                var3[var12++] = var19.substring(var5, var19.length());
            }

            return var12;
        }
    }

    @ObfuscatedName("k")
    public int k(String var1, int var2) {
        int var3 = this.u(var1, new int[] { var2 }, f);
        int var4 = 0;

        for (int var5 = 0; var5 < var3; ++var5) {
            int var6 = this.c(f[var5]);
            if (var6 > var4) {
                var4 = var6;
            }
        }

        return var4;
    }

    @ObfuscatedName("z")
    public int z(String var1, int var2) {
        return this.u(var1, new int[] { var2 }, f);
    }

    @ObfuscatedName("s")
    public void s(String var1, int var2, int var3, int var4, int var5) {
        if (var1 != null) {
            this.am(var4, var5);
            this.ah(var1, var2, var3);
        }
    }

    @ObfuscatedName("d")
    public void d(String var1, int var2, int var3, int var4, int var5, int var6) {
        if (var1 != null) {
            this.am(var4, var5);
            z = var6;
            this.ah(var1, var2, var3);
        }
    }

    @ObfuscatedName("f")
    public void f(String var1, int var2, int var3, int var4, int var5) {
        if (var1 != null) {
            this.am(var4, var5);
            this.ah(var1, var2 - this.c(var1), var3);
        }
    }

    @ObfuscatedName("r")
    public void r(String var1, int var2, int var3, int var4, int var5) {
        if (var1 != null) {
            this.am(var4, var5);
            this.ah(var1, var2 - this.c(var1) / 2, var3);
        }
    }

    @ObfuscatedName("y")
    public int y(String var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
        if (var1 == null) {
            return 0;
        } else {
            this.am(var6, var7);
            if (var10 == 0) {
                var10 = this.e;
            }

            int[] var11 = new int[] { var4 };
            if (var5 < var10 + this.x + this.p && var5 < var10 + var10) {
                var11 = null;
            }

            int var12 = this.u(var1, var11, f);
            if (var9 == 3 && var12 == 1) {
                var9 = 1;
            }

            int var13;
            int var14;
            if (var9 == 0) {
                var13 = var3 + this.x;
            } else if (var9 == 1) {
                var13 = var3 + (var5 - this.x - this.p - var10 * (var12 - 1)) / 2 + this.x;
            } else if (var9 == 2) {
                var13 = var3 + var5 - this.p - var10 * (var12 - 1);
            } else {
                var14 = (var5 - this.x - this.p - var10 * (var12 - 1)) / (var12 + 1);
                if (var14 < 0) {
                    var14 = 0;
                }

                var13 = var3 + var14 + this.x;
                var10 += var14;
            }

            for (var14 = 0; var14 < var12; ++var14) {
                if (var8 == 0) {
                    this.ah(f[var14], var2, var13);
                } else if (var8 == 1) {
                    this.ah(f[var14], var2 + (var4 - this.c(f[var14])) / 2, var13);
                } else if (var8 == 2) {
                    this.ah(f[var14], var2 + var4 - this.c(f[var14]), var13);
                } else if (var14 == var12 - 1) {
                    this.ah(f[var14], var2, var13);
                } else {
                    this.ap(f[var14], var4);
                    this.ah(f[var14], var2, var13);
                    w = 0;
                }

                var13 += var10;
            }

            return var12;
        }
    }

    @ObfuscatedName("h")
    public void h(String var1, int var2, int var3, int var4, int var5, int var6) {
        if (var1 != null) {
            this.am(var4, var5);
            int[] var7 = new int[var1.length()];

            for (int var8 = 0; var8 < var1.length(); ++var8) {
                var7[var8] = (int) (Math.sin((double) var8 / 2.0D + (double) var6 / 5.0D) * 5.0D);
            }

            this.au(var1, var2 - this.c(var1) / 2, var3, (int[]) null, var7);
        }
    }

    @ObfuscatedName("av")
    public void av(String var1, int var2, int var3, int var4, int var5, int var6) {
        if (var1 != null) {
            this.am(var4, var5);
            int[] var7 = new int[var1.length()];
            int[] var8 = new int[var1.length()];

            for (int var9 = 0; var9 < var1.length(); ++var9) {
                var7[var9] = (int) (Math.sin((double) var9 / 5.0D + (double) var6 / 5.0D) * 5.0D);
                var8[var9] = (int) (Math.sin((double) var9 / 3.0D + (double) var6 / 5.0D) * 5.0D);
            }

            this.au(var1, var2 - this.c(var1) / 2, var3, var7, var8);
        }
    }

    @ObfuscatedName("aj")
    public void aj(String var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        if (var1 != null) {
            this.am(var4, var5);
            double var8 = 7.0D - (double) var7 / 8.0D;
            if (var8 < 0.0D) {
                var8 = 0.0D;
            }

            int[] var10 = new int[var1.length()];

            for (int var11 = 0; var11 < var1.length(); ++var11) {
                var10[var11] = (int) (Math.sin((double) var11 / 1.5D + (double) var6 / 1.0D) * var8);
            }

            this.au(var1, var2 - this.c(var1) / 2, var3, (int[]) null, var10);
        }
    }

    @ObfuscatedName("ae")
    public void ae(String var1, int var2, int var3, int var4, int var5, int var6) {
        if (var1 != null) {
            this.am(var4, var5);
            d.setSeed((long) var6);
            z = 192 + (d.nextInt() & 31);
            int[] var7 = new int[var1.length()];
            int var8 = 0;

            for (int var9 = 0; var9 < var1.length(); ++var9) {
                var7[var9] = var8;
                if ((d.nextInt() & 3) == 0) {
                    ++var8;
                }
            }

            this.au(var1, var2, var3, var7, (int[]) null);
        }
    }

    @ObfuscatedName("am")
    void am(int var1, int var2) {
        o = -1;
        c = -1;
        v = var2;
        u = var2;
        j = var1;
        k = var1;
        z = 256;
        w = 0;
        s = 0;
    }

    @ObfuscatedName("az")
    void az(String var1) {
        try {
            if (var1.startsWith("col=")) {
                k = ku.a(var1.substring(4), 16);
            } else if (var1.equals("/col")) {
                k = j;
            } else if (var1.startsWith("str=")) {
                o = ku.a(var1.substring(4), 16);
            } else if (var1.equals("str")) {
                o = 8388608;
            } else if (var1.equals("/str")) {
                o = -1;
            } else if (var1.startsWith("u=")) {
                c = ku.a(var1.substring(2), 16);
            } else if (var1.equals("u")) {
                c = 0;
            } else if (var1.equals("/u")) {
                c = -1;
            } else if (var1.startsWith("shad=")) {
                u = ku.a(var1.substring(5), 16);
            } else if (var1.equals("shad")) {
                u = 0;
            } else if (var1.equals("/shad")) {
                u = v;
            } else if (var1.equals("br")) {
                this.am(j, v);
            }
        } catch (Exception var3) {
            ;
        }

    }

    @ObfuscatedName("ap")
    void ap(String var1, int var2) {
        int var3 = 0;
        boolean var4 = false;

        for (int var5 = 0; var5 < var1.length(); ++var5) {
            char var6 = var1.charAt(var5);
            if (var6 == '<') {
                var4 = true;
            } else if (var6 == '>') {
                var4 = false;
            } else if (!var4 && var6 == ' ') {
                ++var3;
            }
        }

        if (var3 > 0) {
            w = (var2 - this.c(var1) << 8) / var3;
        }

    }

    @ObfuscatedName("ah")
    void ah(String var1, int var2, int var3) {
        var3 -= this.e;
        int var4 = -1;
        int var5 = -1;

        for (int var6 = 0; var6 < var1.length(); ++var6) {
            if (var1.charAt(var6) != 0) {
                char var7 = (char) (ko.t(var1.charAt(var6)) & 255);
                if (var7 == '<') {
                    var4 = var6;
                } else {
                    int var9;
                    if (var7 == '>' && var4 != -1) {
                        String var8 = var1.substring(var4 + 1, var6);
                        var4 = -1;
                        if (var8.equals("lt")) {
                            var7 = '<';
                        } else {
                            if (!var8.equals("gt")) {
                                if (var8.startsWith("img=")) {
                                    try {
                                        var9 = ff.i(var8.substring(4));
                                        lk var10 = g[var9];
                                        var10.i(var2, var3 + this.e - var10.x);
                                        var2 += var10.e;
                                        var5 = -1;
                                    } catch (Exception var14) {
                                        ;
                                    }
                                } else {
                                    this.az(var8);
                                }
                                continue;
                            }

                            var7 = '>';
                        }
                    }

                    if (var7 == 160) {
                        var7 = ' ';
                    }

                    if (var4 == -1) {
                        if (this.n != null && var5 != -1) {
                            var2 += this.n[var7 + (var5 << 8)];
                        }

                        int var12 = this.i[var7];
                        var9 = this.a[var7];
                        if (var7 != ' ') {
                            if (z == 256) {
                                if (u != -1) {
                                    ax(this.characterPixels[var7], var2 + this.l[var7] + 1, var3 + this.b[var7] + 1,
                                            var12, var9, u);
                                }

                                this.t(this.characterPixels[var7], var2 + this.l[var7], var3 + this.b[var7], var12,
                                        var9, k);
                            } else {
                                if (u != -1) {
                                    an(this.characterPixels[var7], var2 + this.l[var7] + 1, var3 + this.b[var7] + 1,
                                            var12, var9, u, z);
                                }

                                this.q(this.characterPixels[var7], var2 + this.l[var7], var3 + this.b[var7], var12,
                                        var9, k, z);
                            }
                        } else if (w > 0) {
                            s += w;
                            var2 += s >> 8;
                            s &= 255;
                        }

                        int var13 = this.q[var7];
                        if (o != -1) {
                            li.du(var2, var3 + (int) ((double) this.e * 0.7D), var13, o);
                        }

                        if (c != -1) {
                            li.du(var2, var3 + this.e + 1, var13, c);
                        }

                        var2 += var13;
                        var5 = var7;
                    }
                }
            }
        }

    }

    @ObfuscatedName("au")
    void au(String var1, int var2, int var3, int[] var4, int[] var5) {
        var3 -= this.e;
        int var6 = -1;
        int var7 = -1;
        int var8 = 0;

        for (int var9 = 0; var9 < var1.length(); ++var9) {
            if (var1.charAt(var9) != 0) {
                char var10 = (char) (ko.t(var1.charAt(var9)) & 255);
                if (var10 == '<') {
                    var6 = var9;
                } else {
                    int var12;
                    int var13;
                    int var14;
                    if (var10 == '>' && var6 != -1) {
                        String var11 = var1.substring(var6 + 1, var9);
                        var6 = -1;
                        if (var11.equals("lt")) {
                            var10 = '<';
                        } else {
                            if (!var11.equals("gt")) {
                                if (var11.startsWith("img=")) {
                                    try {
                                        if (var4 != null) {
                                            var12 = var4[var8];
                                        } else {
                                            var12 = 0;
                                        }

                                        if (var5 != null) {
                                            var13 = var5[var8];
                                        } else {
                                            var13 = 0;
                                        }

                                        ++var8;
                                        var14 = ff.i(var11.substring(4));
                                        lk var15 = g[var14];
                                        var15.i(var12 + var2, var13 + (var3 + this.e - var15.x));
                                        var2 += var15.e;
                                        var7 = -1;
                                    } catch (Exception var19) {
                                        ;
                                    }
                                } else {
                                    this.az(var11);
                                }
                                continue;
                            }

                            var10 = '>';
                        }
                    }

                    if (var10 == 160) {
                        var10 = ' ';
                    }

                    if (var6 == -1) {
                        if (this.n != null && var7 != -1) {
                            var2 += this.n[var10 + (var7 << 8)];
                        }

                        int var17 = this.i[var10];
                        var12 = this.a[var10];
                        if (var4 != null) {
                            var13 = var4[var8];
                        } else {
                            var13 = 0;
                        }

                        if (var5 != null) {
                            var14 = var5[var8];
                        } else {
                            var14 = 0;
                        }

                        ++var8;
                        if (var10 != ' ') {
                            if (z == 256) {
                                if (u != -1) {
                                    ax(this.characterPixels[var10], var13 + var2 + this.l[var10] + 1, var3 + var14
                                            + this.b[var10] + 1, var17, var12, u);
                                }

                                this.t(this.characterPixels[var10], var13 + var2 + this.l[var10], var3 + var14
                                        + this.b[var10], var17, var12, k);
                            } else {
                                if (u != -1) {
                                    an(this.characterPixels[var10], var13 + var2 + this.l[var10] + 1, var3 + var14
                                            + this.b[var10] + 1, var17, var12, u, z);
                                }

                                this.q(this.characterPixels[var10], var13 + var2 + this.l[var10], var3 + var14
                                        + this.b[var10], var17, var12, k, z);
                            }
                        } else if (w > 0) {
                            s += w;
                            var2 += s >> 8;
                            s &= 255;
                        }

                        int var18 = this.q[var10];
                        if (o != -1) {
                            li.du(var2, var3 + (int) ((double) this.e * 0.7D), var18, o);
                        }

                        if (c != -1) {
                            li.du(var2, var3 + this.e, var18, c);
                        }

                        var2 += var18;
                        var7 = var10;
                    }
                }
            }
        }

    }

    @ObfuscatedName("p")
    static int p(byte[][] var0, byte[][] var1, int[] var2, int[] var3, int[] var4, int var5, int var6) {
        int var7 = var2[var5];
        int var8 = var7 + var4[var5];
        int var9 = var2[var6];
        int var10 = var9 + var4[var6];
        int var11 = var7;
        if (var9 > var7) {
            var11 = var9;
        }

        int var12 = var8;
        if (var10 < var8) {
            var12 = var10;
        }

        int var13 = var3[var5];
        if (var3[var6] < var13) {
            var13 = var3[var6];
        }

        byte[] var14 = var1[var5];
        byte[] var15 = var0[var6];
        int var16 = var11 - var7;
        int var17 = var11 - var9;

        for (int var18 = var11; var18 < var12; ++var18) {
            int var19 = var14[var16++] + var15[var17++];
            if (var19 < var13) {
                var13 = var19;
            }
        }

        return -var13;
    }

    @ObfuscatedName("w")
    public static String w(String var0) {
        int var1 = var0.length();
        int var2 = 0;

        for (int var3 = 0; var3 < var1; ++var3) {
            char var4 = var0.charAt(var3);
            if (var4 == '<' || var4 == '>') {
                var2 += 3;
            }
        }

        StringBuilder var6 = new StringBuilder(var1 + var2);

        for (int var7 = 0; var7 < var1; ++var7) {
            char var5 = var0.charAt(var7);
            if (var5 == '<') {
                var6.append("<lt>");
            } else if (var5 == '>') {
                var6.append("<gt>");
            } else {
                var6.append(var5);
            }
        }

        return var6.toString();
    }

    @ObfuscatedName("ax")
    static void ax(byte[] var0, int var1, int var2, int var3, int var4, int var5) {
        int var6 = var1 + var2 * li.av;
        int var7 = li.av - var3;
        int var8 = 0;
        int var9 = 0;
        int var10;
        if (var2 < li.ae) {
            var10 = li.ae - var2;
            var4 -= var10;
            var2 = li.ae;
            var9 += var3 * var10;
            var6 += var10 * li.av;
        }

        if (var2 + var4 > li.am) {
            var4 -= var2 + var4 - li.am;
        }

        if (var1 < li.az) {
            var10 = li.az - var1;
            var3 -= var10;
            var1 = li.az;
            var9 += var10;
            var6 += var10;
            var8 += var10;
            var7 += var10;
        }

        if (var3 + var1 > li.ap) {
            var10 = var3 + var1 - li.ap;
            var3 -= var10;
            var8 += var10;
            var7 += var10;
        }

        if (var3 > 0 && var4 > 0) {
            ar(li.ao, var0, var5, var9, var6, var3, var4, var7, var8);
        }
    }

    @ObfuscatedName("ar")
    static void ar(int[] var0, byte[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        int var9 = -(var5 >> 2);
        var5 = -(var5 & 3);

        for (int var10 = -var6; var10 < 0; ++var10) {
            int var11;
            for (var11 = var9; var11 < 0; ++var11) {
                if (var1[var3++] != 0) {
                    var0[var4++] = var2;
                } else {
                    ++var4;
                }

                if (var1[var3++] != 0) {
                    var0[var4++] = var2;
                } else {
                    ++var4;
                }

                if (var1[var3++] != 0) {
                    var0[var4++] = var2;
                } else {
                    ++var4;
                }

                if (var1[var3++] != 0) {
                    var0[var4++] = var2;
                } else {
                    ++var4;
                }
            }

            for (var11 = var5; var11 < 0; ++var11) {
                if (var1[var3++] != 0) {
                    var0[var4++] = var2;
                } else {
                    ++var4;
                }
            }

            var4 += var7;
            var3 += var8;
        }

    }

    @ObfuscatedName("an")
    static void an(byte[] var0, int var1, int var2, int var3, int var4, int var5, int var6) {
        int var7 = var1 + var2 * li.av;
        int var8 = li.av - var3;
        int var9 = 0;
        int var10 = 0;
        int var11;
        if (var2 < li.ae) {
            var11 = li.ae - var2;
            var4 -= var11;
            var2 = li.ae;
            var10 += var3 * var11;
            var7 += var11 * li.av;
        }

        if (var2 + var4 > li.am) {
            var4 -= var2 + var4 - li.am;
        }

        if (var1 < li.az) {
            var11 = li.az - var1;
            var3 -= var11;
            var1 = li.az;
            var10 += var11;
            var7 += var11;
            var9 += var11;
            var8 += var11;
        }

        if (var3 + var1 > li.ap) {
            var11 = var3 + var1 - li.ap;
            var3 -= var11;
            var9 += var11;
            var8 += var11;
        }

        if (var3 > 0 && var4 > 0) {
            ai(li.ao, var0, var5, var10, var7, var3, var4, var8, var9, var6);
        }
    }

    @ObfuscatedName("ai")
    static void ai(int[] var0, byte[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8,
            int var9) {
        var2 = ((var2 & '\uff00') * var9 & 16711680) + (var9 * (var2 & 16711935) & -16711936) >> 8;
        var9 = 256 - var9;

        for (int var10 = -var6; var10 < 0; ++var10) {
            for (int var11 = -var5; var11 < 0; ++var11) {
                if (var1[var3++] != 0) {
                    int var12 = var0[var4];
                    var0[var4++] = (((var12 & '\uff00') * var9 & 16711680) + ((var12 & 16711935) * var9 & -16711936) >> 8)
                            + var2;
                } else {
                    ++var4;
                }
            }

            var4 += var7;
            var3 += var8;
        }

    }
}
