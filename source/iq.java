import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("iq")
public class iq extends hh {

    @ObfuscatedName("cf")
    static ju cf;

    @ObfuscatedName("t")
    public final int t;

    @ObfuscatedName("q")
    public final int q;

    @ObfuscatedName("i")
    public final int[] i;

    @ObfuscatedName("a")
    public final int[] a;

    iq(int var1, int var2, int[] var3, int[] var4, int var5) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
        this.a = var4;
    }

    @ObfuscatedName("t")
    public boolean t(int var1, int var2) {
        if (var2 >= 0 && var2 < this.a.length) {
            int var3 = this.a[var2];
            if (var1 >= var3 && var1 <= var3 + this.i[var2]) {
                return true;
            }
        }

        return false;
    }

    @ObfuscatedName("a")
    static jm[] a() {
        return new jm[] { jm.q, jm.i, jm.t };
    }
}
