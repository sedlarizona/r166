import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ht")
public class ht extends hs {

    @ObfuscatedName("t")
    Object t;

    ht(Object var1, int var2) {
        super(var2);
        this.t = var1;
    }

    @ObfuscatedName("t")
    Object t() {
        return this.t;
    }

    @ObfuscatedName("q")
    boolean q() {
        return false;
    }
}
