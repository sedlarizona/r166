import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jt")
public class jt implements Runnable {

    @ObfuscatedName("q")
    public static Deque q = new Deque();

    @ObfuscatedName("i")
    public static Deque i = new Deque();

    @ObfuscatedName("a")
    static int a = 0;

    @ObfuscatedName("l")
    static Object l = new Object();

    @ObfuscatedName("b")
    static Thread b;

    public void run() {
        try {
            while (true) {
                Deque var2 = q;
                ia var1;
                synchronized (q) {
                    var1 = (ia) q.e();
                }

                Object var14;
                if (var1 != null) {
                    if (var1.t == 0) {
                        var1.i.q((int) var1.uid, var1.q, var1.q.length);
                        var2 = q;
                        synchronized (q) {
                            var1.kc();
                        }
                    } else if (var1.t == 1) {
                        var1.q = var1.i.t((int) var1.uid);
                        var2 = q;
                        synchronized (q) {
                            i.q(var1);
                        }
                    }

                    var14 = l;
                    synchronized (l) {
                        if (a <= 1) {
                            a = 0;
                            l.notifyAll();
                            return;
                        }

                        a = 600;
                    }
                } else {
                    cx.t(100L);
                    var14 = l;
                    synchronized (l) {
                        if (a <= 1) {
                            a = 0;
                            l.notifyAll();
                            return;
                        }

                        --a;
                    }
                }
            }
        } catch (Exception var13) {
            FloorObject.t((String) null, var13);
        }
    }

    @ObfuscatedName("ge")
    static final void ge(int var0, int var1) {
        if (Client.hintArrowType == 2) {
            m.gr((Client.hintX - an.regionBaseX << 7) + Client.ct,
                    (Client.hintY - PlayerComposite.ep << 7) + Client.cw, Client.cs * 2);
            if (Client.hv > -1 && Client.bz % 20 < 10) {
                ie.fn[0].u(var0 + Client.hv - 12, Client.hd + var1 - 28);
            }

        }
    }
}
