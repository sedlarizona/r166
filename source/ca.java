import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ca")
public class ca extends TaskDataNode {

    @ObfuscatedName("t")
    Deque t = new Deque();

    @ObfuscatedName("q")
    Deque q = new Deque();

    @ObfuscatedName("i")
    int i = 0;

    @ObfuscatedName("a")
    int a = -1;

    @ObfuscatedName("t")
    public final synchronized void t(TaskDataNode var1) {
        this.t.i(var1);
    }

    @ObfuscatedName("q")
    public final synchronized void q(TaskDataNode var1) {
        var1.kc();
    }

    @ObfuscatedName("i")
    void i() {
        if (this.i > 0) {
            for (df var1 = (df) this.q.e(); var1 != null; var1 = (df) this.q.p()) {
                var1.t -= this.i;
            }

            this.a -= this.i;
            this.i = 0;
        }

    }

    @ObfuscatedName("a")
    void a(Node var1, df var2) {
        while (this.q.sentinel != var1 && ((df) var1).t <= var2.t) {
            var1 = var1.next;
        }

        Deque.a(var2, var1);
        this.a = ((df) this.q.sentinel.next).t;
    }

    @ObfuscatedName("l")
    void l(df var1) {
        var1.kc();
        var1.t();
        Node var2 = this.q.sentinel.next;
        if (var2 == this.q.sentinel) {
            this.a = -1;
        } else {
            this.a = ((df) var2).t;
        }

    }

    @ObfuscatedName("b")
    protected TaskDataNode b() {
        return (TaskDataNode) this.t.e();
    }

    @ObfuscatedName("e")
    protected TaskDataNode e() {
        return (TaskDataNode) this.t.p();
    }

    @ObfuscatedName("x")
    protected int x() {
        return 0;
    }

    @ObfuscatedName("p")
    public final synchronized void p(int[] var1, int var2, int var3) {
        do {
            if (this.a < 0) {
                this.o(var1, var2, var3);
                return;
            }

            if (var3 + this.i < this.a) {
                this.i += var3;
                this.o(var1, var2, var3);
                return;
            }

            int var4 = this.a - this.i;
            this.o(var1, var2, var4);
            var2 += var4;
            var3 -= var4;
            this.i += var4;
            this.i();
            df var5 = (df) this.q.e();
            synchronized (var5) {
                int var7 = var5.q();
                if (var7 < 0) {
                    var5.t = 0;
                    this.l(var5);
                } else {
                    var5.t = var7;
                    this.a(var5.next, var5);
                }
            }
        } while (var3 != 0);

    }

    @ObfuscatedName("o")
    void o(int[] var1, int var2, int var3) {
        for (TaskDataNode var4 = (TaskDataNode) this.t.e(); var4 != null; var4 = (TaskDataNode) this.t.p()) {
            var4.er(var1, var2, var3);
        }

    }

    @ObfuscatedName("c")
    public final synchronized void c(int var1) {
        do {
            if (this.a < 0) {
                this.u(var1);
                return;
            }

            if (this.i + var1 < this.a) {
                this.i += var1;
                this.u(var1);
                return;
            }

            int var2 = this.a - this.i;
            this.u(var2);
            var1 -= var2;
            this.i += var2;
            this.i();
            df var3 = (df) this.q.e();
            synchronized (var3) {
                int var5 = var3.q();
                if (var5 < 0) {
                    var3.t = 0;
                    this.l(var3);
                } else {
                    var3.t = var5;
                    this.a(var3.next, var3);
                }
            }
        } while (var1 != 0);

    }

    @ObfuscatedName("u")
    void u(int var1) {
        for (TaskDataNode var2 = (TaskDataNode) this.t.e(); var2 != null; var2 = (TaskDataNode) this.t.p()) {
            var2.c(var1);
        }

    }
}
