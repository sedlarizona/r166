import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dp")
public class DataRequestNode extends IntegerNode {

    @ObfuscatedName("t")
    public int t;

    @ObfuscatedName("q")
    public byte[] buffer;

    @ObfuscatedName("i")
    public int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    public boolean incomplete;

    DataRequestNode(int var1, byte[] var2, int var3, int var4) {
        this.t = var1;
        this.buffer = var2;
        this.i = var3;
        this.a = var4;
    }

    DataRequestNode(int var1, byte[] var2, int var3, int var4, boolean var5) {
        this.t = var1;
        this.buffer = var2;
        this.i = var3;
        this.a = var4;
        this.incomplete = var5;
    }

    @ObfuscatedName("t")
    public DataRequestNode t(dt var1) {
        this.buffer = var1.t(this.buffer);
        this.t = var1.q(this.t);
        if (this.i == this.a) {
            this.i = this.a = var1.i(this.i);
        } else {
            this.i = var1.i(this.i);
            this.a = var1.i(this.a);
            if (this.i == this.a) {
                --this.i;
            }
        }

        return this;
    }
}
