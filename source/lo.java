import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lo")
public class lo {

    @ObfuscatedName("x")
    static final kq x;

    @ObfuscatedName("p")
    static final kq p;

    @ObfuscatedName("g")
    static final kq g;

    @ObfuscatedName("e")
    FileSystem e;

    @ObfuscatedName("n")
    km n;

    @ObfuscatedName("o")
    HashMap o;

    @ObfuscatedName("c")
    lk[] c;

    @ObfuscatedName("v")
    HashMap v;

    @ObfuscatedName("u")
    az u;

    @ObfuscatedName("j")
    az j;

    @ObfuscatedName("k")
    az k;

    @ObfuscatedName("z")
    ag z;

    @ObfuscatedName("w")
    lg w;

    @ObfuscatedName("s")
    int s;

    @ObfuscatedName("d")
    int d;

    @ObfuscatedName("f")
    int f = -1;

    @ObfuscatedName("r")
    int r = -1;

    @ObfuscatedName("y")
    float y;

    @ObfuscatedName("h")
    float h;

    @ObfuscatedName("m")
    int m = -1;

    @ObfuscatedName("ay")
    int ay = -1;

    @ObfuscatedName("ao")
    int ao = -1;

    @ObfuscatedName("av")
    int av = -1;

    @ObfuscatedName("aj")
    int aj = 3;

    @ObfuscatedName("ae")
    int ae = 50;

    @ObfuscatedName("am")
    boolean am = false;

    @ObfuscatedName("az")
    HashSet az = null;

    @ObfuscatedName("ap")
    int ap = -1;

    @ObfuscatedName("ah")
    int ah = -1;

    @ObfuscatedName("au")
    int au = -1;

    @ObfuscatedName("ax")
    int ax = -1;

    @ObfuscatedName("ar")
    int ar = -1;

    @ObfuscatedName("an")
    int an = -1;

    @ObfuscatedName("ai")
    long ai;

    @ObfuscatedName("al")
    int al;

    @ObfuscatedName("at")
    int at;

    @ObfuscatedName("ag")
    boolean ag = true;

    @ObfuscatedName("aq")
    HashSet aq = new HashSet();

    @ObfuscatedName("aa")
    HashSet aa = new HashSet();

    @ObfuscatedName("af")
    HashSet af = new HashSet();

    @ObfuscatedName("ak")
    HashSet ak = new HashSet();

    @ObfuscatedName("ab")
    boolean ab = false;

    @ObfuscatedName("ac")
    int ac = 0;

    @ObfuscatedName("bg")
    final int[] bg = new int[] { 1008, 1009, 1010, 1011, 1012 };

    @ObfuscatedName("br")
    List br;

    @ObfuscatedName("ba")
    Iterator ba;

    @ObfuscatedName("bk")
    HashSet bk = new HashSet();

    @ObfuscatedName("be")
    ik be = null;

    @ObfuscatedName("bc")
    public boolean bc = false;

    @ObfuscatedName("bm")
    Sprite bm;

    @ObfuscatedName("bh")
    int bh;

    @ObfuscatedName("bs")
    int bs = -1;

    @ObfuscatedName("bj")
    int bj = -1;

    @ObfuscatedName("bt")
    int bt = -1;

    static {
        x = kq.a;
        p = kq.l;
        g = kq.b;
    }

    @ObfuscatedName("t")
    public void t(FileSystem var1, km var2, HashMap var3, lk[] var4) {
        this.c = var4;
        this.e = var1;
        this.n = var2;
        this.o = new HashMap();
        this.o.put(h.t, var3.get(x));
        this.o.put(h.q, var3.get(p));
        this.o.put(h.i, var3.get(g));
        this.w = new lg(var1);
        int var5 = this.e.h(AppletParameter.t.value);
        int[] var6 = this.e.z(var5);
        this.v = new HashMap(var6.length);

        for (int var7 = 0; var7 < var6.length; ++var7) {
            ByteBuffer var8 = new ByteBuffer(this.e.i(var5, var6[var7]));
            az var9 = new az();
            var9.t(var8, var6[var7]);
            this.v.put(var9.o(), var9);
            if (var9.p()) {
                this.u = var9;
            }
        }

        this.d(this.u);
        this.k = null;
    }

    @ObfuscatedName("q")
    public int q() {
        return this.e.am(this.u.o(), AppletParameter.a.value) ? 100 : this.e.ah(this.u.o());
    }

    @ObfuscatedName("i")
    public void i() {
        ea.a();
    }

    @ObfuscatedName("a")
    public void a(int var1, int var2, boolean var3, int var4, int var5, int var6, int var7) {
        if (this.w.i()) {
            this.e();
            this.x();
            if (var3) {
                int var8 = (int) Math.ceil((double) ((float) var6 / this.y));
                int var9 = (int) Math.ceil((double) ((float) var7 / this.y));
                List var10 = this.z.e(this.s - var8 / 2 - 1, this.d - var9 / 2 - 1, var8 / 2 + this.s + 1, var9 / 2
                        + this.d + 1, var4, var5, var6, var7, var1, var2);
                HashSet var11 = new HashSet();

                Iterator var12;
                al var13;
                ScriptEvent var14;
                ak var15;
                for (var12 = var10.iterator(); var12.hasNext(); m.t(var14, 500000)) {
                    var13 = (al) var12.next();
                    var11.add(var13);
                    var14 = new ScriptEvent();
                    var15 = new ak(var13.t, var13.i, var13.q);
                    var14.t(new Object[] { var15, var1, var2 });
                    if (this.bk.contains(var13)) {
                        var14.q(iw.x);
                    } else {
                        var14.q(iw.b);
                    }
                }

                var12 = this.bk.iterator();

                while (var12.hasNext()) {
                    var13 = (al) var12.next();
                    if (!var11.contains(var13)) {
                        var14 = new ScriptEvent();
                        var15 = new ak(var13.t, var13.i, var13.q);
                        var14.t(new Object[] { var15, var1, var2 });
                        var14.q(iw.e);
                        m.t(var14, 500000);
                    }
                }

                this.bk = var11;
            }
        }
    }

    @ObfuscatedName("l")
    public void l(int var1, int var2, boolean var3, boolean var4) {
        long var5 = au.t();
        this.b(var1, var2, var4, var5);
        if (!this.c() && (var4 || var3)) {
            if (var4) {
                this.ar = var1;
                this.an = var2;
                this.au = this.s;
                this.ax = this.d;
            }

            if (this.au != -1) {
                int var7 = var1 - this.ar;
                int var8 = var2 - this.an;
                this.p(this.au - (int) ((float) var7 / this.h), (int) ((float) var8 / this.h) + this.ax, false);
            }
        } else {
            this.o();
        }

        if (var4) {
            this.ai = var5;
            this.al = var1;
            this.at = var2;
        }

    }

    @ObfuscatedName("b")
    void b(int var1, int var2, boolean var3, long var4) {
        if (this.j != null) {
            int var6 = (int) ((float) this.s + ((float) (var1 - this.ao) - (float) this.aq() * this.y / 2.0F) / this.y);
            int var7 = (int) ((float) this.d - ((float) (var2 - this.av) - (float) this.aa() * this.y / 2.0F) / this.y);
            this.be = this.j.b(var6 + this.j.z() * 64, var7 + this.j.s() * 64);
            if (this.be != null && var3) {
                boolean var8 = Client.localPlayerRights >= 2;
                if (var8 && ad.pressedKeys[82] && ad.pressedKeys[81]) {
                    RSRandomAccessFile.kw(this.be.q, this.be.i, this.be.t, false);
                } else {
                    boolean var9 = true;
                    if (this.ag) {
                        int var10 = var1 - this.al;
                        int var11 = var2 - this.at;
                        if (var4 - this.ai > 500L || var10 < -25 || var10 > 25 || var11 < -25 || var11 > 25) {
                            var9 = false;
                        }
                    }

                    if (var9) {
                        gd var12 = ap.t(fo.e, Client.ee.l);
                        var12.i.bl(this.be.q());
                        Client.ee.i(var12);
                        this.ai = 0L;
                    }
                }
            }
        } else {
            this.be = null;
        }

    }

    @ObfuscatedName("e")
    void e() {
        if (cn.ry != null) {
            this.y = this.h;
        } else {
            if (this.y < this.h) {
                this.y = Math.min(this.h, this.y / 30.0F + this.y);
            }

            if (this.y > this.h) {
                this.y = Math.max(this.h, this.y - this.y / 30.0F);
            }

        }
    }

    @ObfuscatedName("x")
    void x() {
        if (this.c()) {
            int var1 = this.f - this.s;
            int var2 = this.r - this.d;
            if (var1 != 0) {
                var1 /= Math.min(8, Math.abs(var1));
            }

            if (var2 != 0) {
                var2 /= Math.min(8, Math.abs(var2));
            }

            this.p(var1 + this.s, var2 + this.d, true);
            if (this.s == this.f && this.d == this.r) {
                this.f = -1;
                this.r = -1;
            }

        }
    }

    @ObfuscatedName("p")
    final void p(int var1, int var2, boolean var3) {
        this.s = var1;
        this.d = var2;
        au.t();
        if (var3) {
            this.o();
        }

    }

    @ObfuscatedName("o")
    final void o() {
        this.an = -1;
        this.ar = -1;
        this.ax = -1;
        this.au = -1;
    }

    @ObfuscatedName("c")
    boolean c() {
        return this.f != -1 && this.r != -1;
    }

    @ObfuscatedName("u")
    public az u(int var1, int var2, int var3) {
        Iterator var4 = this.v.values().iterator();

        az var5;
        do {
            if (!var4.hasNext()) {
                return null;
            }

            var5 = (az) var4.next();
        } while (!var5.i(var1, var2, var3));

        return var5;
    }

    @ObfuscatedName("k")
    public void k(int var1, int var2, int var3, boolean var4) {
        az var5 = this.u(var1, var2, var3);
        if (var5 == null) {
            if (!var4) {
                return;
            }

            var5 = this.u;
        }

        boolean var6 = false;
        if (var5 != this.k || var4) {
            this.k = var5;
            this.d(var5);
            var6 = true;
        }

        if (var6 || var4) {
            this.y(var1, var2, var3);
        }

    }

    @ObfuscatedName("z")
    public void z(int var1) {
        az var2 = this.ar(var1);
        if (var2 != null) {
            this.d(var2);
        }

    }

    @ObfuscatedName("w")
    public int w() {
        return this.j == null ? -1 : this.j.x();
    }

    @ObfuscatedName("s")
    public az s() {
        return this.j;
    }

    @ObfuscatedName("d")
    void d(az var1) {
        if (this.j == null || var1 != this.j) {
            this.f(var1);
            this.y(-1, -1, -1);
        }
    }

    @ObfuscatedName("f")
    void f(az var1) {
        this.j = var1;
        this.z = new ag(this.c, this.o);
        this.w.t(this.j.o());
    }

    @ObfuscatedName("r")
    public void r(az var1, ik var2, ik var3, boolean var4) {
        if (var1 != null) {
            if (this.j == null || var1 != this.j) {
                this.f(var1);
            }

            if (!var4 && this.j.i(var2.t, var2.q, var2.i)) {
                this.y(var2.t, var2.q, var2.i);
            } else {
                this.y(var3.t, var3.q, var3.i);
            }

        }
    }

    @ObfuscatedName("y")
    void y(int var1, int var2, int var3) {
        if (this.j != null) {
            int[] var4 = this.j.l(var1, var2, var3);
            if (var4 == null) {
                var4 = this.j.l(this.j.r(), this.j.f(), this.j.y());
            }

            this.p(var4[0] - this.j.z() * 64, var4[1] - this.j.s() * 64, true);
            this.f = -1;
            this.r = -1;
            this.y = this.ap(this.j.k());
            this.h = this.y;
            this.br = null;
            this.ba = null;
            this.z.q();
        }
    }

    @ObfuscatedName("h")
    public void h(int var1, int var2, int var3, int var4, int var5) {
        int[] var6 = new int[4];
        li.dp(var6);
        li.ck(var1, var2, var3 + var1, var2 + var4);
        li.dl(var1, var2, var3, var4, -16777216);
        int var7 = this.w.a();
        if (var7 < 100) {
            this.az(var1, var2, var3, var4, var7);
        } else {
            if (!this.z.p()) {
                this.z.t(this.e, this.j.o(), Client.membersWorld);
                if (!this.z.p()) {
                    return;
                }
            }

            if (this.az != null) {
                ++this.ah;
                if (this.ah % this.ae == 0) {
                    this.ah = 0;
                    ++this.ap;
                }

                if (this.ap >= this.aj && !this.am) {
                    this.az = null;
                }
            }

            int var8 = (int) Math.ceil((double) ((float) var3 / this.y));
            int var9 = (int) Math.ceil((double) ((float) var4 / this.y));
            this.z.i(this.s - var8 / 2, this.d - var9 / 2, var8 / 2 + this.s, var9 / 2 + this.d, var1, var2, var3
                    + var1, var2 + var4);
            boolean var10;
            if (!this.ab) {
                var10 = false;
                if (var5 - this.ac > 100) {
                    this.ac = var5;
                    var10 = true;
                }

                this.z.a(this.s - var8 / 2, this.d - var9 / 2, var8 / 2 + this.s, var9 / 2 + this.d, var1, var2, var3
                        + var1, var2 + var4, this.ak, this.az, this.ah, this.ae, var10);
            }

            this.aj(var1, var2, var3, var4, var8, var9);
            var10 = Client.localPlayerRights >= 2;
            if (var10 && this.bc && this.be != null) {
                this.n.s("Coord: " + this.be, li.az + 10, li.ae + 20, 16776960, -1);
            }

            this.m = var8;
            this.ay = var9;
            this.ao = var1;
            this.av = var2;
            li.da(var6);
        }
    }

    @ObfuscatedName("av")
    boolean av(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (this.bm == null) {
            return true;
        } else if (this.bm.width == var1 && this.bm.height == var2) {
            if (this.z.v != this.bh) {
                return true;
            } else if (this.bt != Client.rh) {
                return true;
            } else if (var3 <= 0 && var4 <= 0) {
                return var3 + var1 < var5 || var2 + var4 < var6;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @ObfuscatedName("aj")
    void aj(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (cn.ry != null) {
            int var7 = 512 / (this.z.v * 2);
            int var8 = var3 + 512;
            int var9 = var4 + 512;
            float var10 = 1.0F;
            var8 = (int) ((float) var8 / var10);
            var9 = (int) ((float) var9 / var10);
            int var11 = this.ag() - var5 / 2 - var7;
            int var12 = this.as() - var6 / 2 - var7;
            int var13 = var1 - (var11 + var7 - this.bs) * this.z.v;
            int var14 = var2 - this.z.v * (var7 - (var12 - this.bj));
            if (this.av(var8, var9, var13, var14, var3, var4)) {
                if (this.bm != null && this.bm.width == var8 && this.bm.height == var9) {
                    Arrays.fill(this.bm.pixels, 0);
                } else {
                    this.bm = new Sprite(var8, var9);
                }

                this.bs = this.ag() - var5 / 2 - var7;
                this.bj = this.as() - var6 / 2 - var7;
                this.bh = this.z.v;
                cn.ry.l(this.bs, this.bj, this.bm, (float) this.bh / var10);
                this.bt = Client.rh;
                var13 = var1 - (var11 + var7 - this.bs) * this.z.v;
                var14 = var2 - this.z.v * (var7 - (var12 - this.bj));
            }

            li.dc(var1, var2, var3, var4, 0, 128);
            if (var10 == 1.0F) {
                this.bm.av(var13, var14, 192);
            } else {
                this.bm.am(var13, var14, (int) ((float) var8 * var10), (int) ((float) var9 * var10), 192);
            }
        }

    }

    @ObfuscatedName("ae")
    public void ae(int var1, int var2, int var3, int var4) {
        if (this.w.i()) {
            if (!this.z.p()) {
                this.z.t(this.e, this.j.o(), Client.membersWorld);
                if (!this.z.p()) {
                    return;
                }
            }

            this.z.b(var1, var2, var3, var4, this.az, this.ah, this.ae);
        }
    }

    @ObfuscatedName("am")
    public void am(int var1) {
        this.h = this.ap(var1);
    }

    @ObfuscatedName("az")
    void az(int var1, int var2, int var3, int var4, int var5) {
        byte var6 = 20;
        int var7 = var3 / 2 + var1;
        int var8 = var4 / 2 + var2 - 18 - var6;
        li.dl(var1, var2, var3, var4, -16777216);
        li.dw(var7 - 152, var8, 304, 34, -65536);
        li.dl(var7 - 150, var8 + 2, var5 * 3, 30, -65536);
        this.n.r("Loading...", var7, var8 + var6, -1, -1);
    }

    @ObfuscatedName("ap")
    float ap(int var1) {
        if (var1 == 25) {
            return 1.0F;
        } else if (var1 == 37) {
            return 1.5F;
        } else if (var1 == 50) {
            return 2.0F;
        } else if (var1 == 75) {
            return 3.0F;
        } else {
            return var1 == 100 ? 4.0F : 8.0F;
        }
    }

    @ObfuscatedName("ah")
    public int ah() {
        if (1.0D == (double) this.h) {
            return 25;
        } else if ((double) this.h == 1.5D) {
            return 37;
        } else if ((double) this.h == 2.0D) {
            return 50;
        } else if ((double) this.h == 3.0D) {
            return 75;
        } else {
            return 4.0D == (double) this.h ? 100 : 200;
        }
    }

    @ObfuscatedName("au")
    public void au() {
        this.w.q();
    }

    @ObfuscatedName("ax")
    public boolean ax() {
        return this.w.i();
    }

    @ObfuscatedName("ar")
    public az ar(int var1) {
        Iterator var2 = this.v.values().iterator();

        az var3;
        do {
            if (!var2.hasNext()) {
                return null;
            }

            var3 = (az) var2.next();
        } while (var3.x() != var1);

        return var3;
    }

    @ObfuscatedName("an")
    public void an(int var1, int var2) {
        if (this.j != null && this.j.a(var1, var2)) {
            this.f = var1 - this.j.z() * 64;
            this.r = var2 - this.j.s() * 64;
        }
    }

    @ObfuscatedName("ai")
    public void ai(int var1, int var2) {
        if (this.j != null) {
            this.p(var1 - this.j.z() * 64, var2 - this.j.s() * 64, true);
            this.f = -1;
            this.r = -1;
        }
    }

    @ObfuscatedName("al")
    public void al(int var1, int var2, int var3) {
        if (this.j != null) {
            int[] var4 = this.j.l(var1, var2, var3);
            if (var4 != null) {
                this.an(var4[0], var4[1]);
            }

        }
    }

    @ObfuscatedName("at")
    public void at(int var1, int var2, int var3) {
        if (this.j != null) {
            int[] var4 = this.j.l(var1, var2, var3);
            if (var4 != null) {
                this.ai(var4[0], var4[1]);
            }

        }
    }

    @ObfuscatedName("ag")
    public int ag() {
        return this.j == null ? -1 : this.s + this.j.z() * 64;
    }

    @ObfuscatedName("as")
    public int as() {
        return this.j == null ? -1 : this.d + this.j.s() * 64;
    }

    @ObfuscatedName("aw")
    public ik aw() {
        return this.j == null ? null : this.j.b(this.ag(), this.as());
    }

    @ObfuscatedName("aq")
    public int aq() {
        return this.m;
    }

    @ObfuscatedName("aa")
    public int aa() {
        return this.ay;
    }

    @ObfuscatedName("af")
    public void af(int var1) {
        if (var1 >= 1) {
            this.aj = var1;
        }

    }

    @ObfuscatedName("ak")
    public void ak() {
        this.aj = 3;
    }

    @ObfuscatedName("ab")
    public void ab(int var1) {
        if (var1 >= 1) {
            this.ae = var1;
        }

    }

    @ObfuscatedName("ac")
    public void ac() {
        this.ae = 50;
    }

    @ObfuscatedName("ad")
    public void ad(boolean var1) {
        this.am = var1;
    }

    @ObfuscatedName("bg")
    public void bg(int var1) {
        this.az = new HashSet();
        this.az.add(var1);
        this.ap = 0;
        this.ah = 0;
    }

    @ObfuscatedName("br")
    public void br(int var1) {
        this.az = new HashSet();
        this.ap = 0;
        this.ah = 0;

        for (int var2 = 0; var2 < jj.q.length; ++var2) {
            if (jj.q[var2] != null && jj.q[var2].r == var1) {
                this.az.add(jj.q[var2].l);
            }
        }

    }

    @ObfuscatedName("ba")
    public void ba() {
        this.az = null;
    }

    @ObfuscatedName("bk")
    public void bk(boolean var1) {
        this.ab = !var1;
    }

    @ObfuscatedName("be")
    public void be(int var1, boolean var2) {
        if (!var2) {
            this.aq.add(var1);
        } else {
            this.aq.remove(var1);
        }

        this.bj();
    }

    @ObfuscatedName("bc")
    public void bc(int var1, boolean var2) {
        if (!var2) {
            this.aa.add(var1);
        } else {
            this.aa.remove(var1);
        }

        for (int var3 = 0; var3 < jj.q.length; ++var3) {
            if (jj.q[var3] != null && jj.q[var3].r == var1) {
                int var4 = jj.q[var3].l;
                if (!var2) {
                    this.af.add(var4);
                } else {
                    this.af.remove(var4);
                }
            }
        }

        this.bj();
    }

    @ObfuscatedName("bm")
    public boolean bm() {
        return !this.ab;
    }

    @ObfuscatedName("bh")
    public boolean bh(int var1) {
        return !this.aq.contains(var1);
    }

    @ObfuscatedName("bs")
    public boolean bs(int var1) {
        return !this.aa.contains(var1);
    }

    @ObfuscatedName("bj")
    void bj() {
        this.ak.clear();
        this.ak.addAll(this.aq);
        this.ak.addAll(this.af);
    }

    @ObfuscatedName("bt")
    public void bt(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (this.w.i()) {
            int var7 = (int) Math.ceil((double) ((float) var3 / this.y));
            int var8 = (int) Math.ceil((double) ((float) var4 / this.y));
            List var9 = this.z.e(this.s - var7 / 2 - 1, this.d - var8 / 2 - 1, var7 / 2 + this.s + 1, var8 / 2 + this.d
                    + 1, var1, var2, var3, var4, var5, var6);
            if (!var9.isEmpty()) {
                Iterator var10 = var9.iterator();

                boolean var13;
                do {
                    if (!var10.hasNext()) {
                        return;
                    }

                    al var11 = (al) var10.next();
                    jj var12 = jj.q[var11.t];
                    var13 = false;

                    for (int var14 = this.bg.length - 1; var14 >= 0; --var14) {
                        if (var12.o[var14] != null) {
                            fy.hi(var12.o[var14], var12.c, this.bg[var14], var11.t, var11.i.q(), var11.q.q());
                            var13 = true;
                        }
                    }
                } while (!var13);

            }
        }
    }

    @ObfuscatedName("by")
    public ik by(int var1, ik var2) {
        if (!this.w.i()) {
            return null;
        } else if (!this.z.p()) {
            return null;
        } else if (!this.j.a(var2.q, var2.i)) {
            return null;
        } else {
            HashMap var3 = this.z.o();
            List var4 = (List) var3.get(var1);
            if (var4 != null && !var4.isEmpty()) {
                al var5 = null;
                int var6 = -1;
                Iterator var7 = var4.iterator();

                while (true) {
                    al var8;
                    int var11;
                    do {
                        if (!var7.hasNext()) {
                            return var5.q;
                        }

                        var8 = (al) var7.next();
                        int var9 = var8.q.q - var2.q;
                        int var10 = var8.q.i - var2.i;
                        var11 = var10 * var10 + var9 * var9;
                        if (var11 == 0) {
                            return var8.q;
                        }
                    } while (var11 >= var6 && var5 != null);

                    var5 = var8;
                    var6 = var11;
                }
            } else {
                return null;
            }
        }
    }

    @ObfuscatedName("bn")
    public void bn(int var1, int var2, ik var3, ik var4) {
        ScriptEvent var5 = new ScriptEvent();
        ak var6 = new ak(var2, var3, var4);
        var5.t(new Object[] { var6 });
        switch (var1) {
        case 1008:
            var5.q(iw.t);
            break;
        case 1009:
            var5.q(iw.q);
            break;
        case 1010:
            var5.q(iw.i);
            break;
        case 1011:
            var5.q(iw.a);
            break;
        case 1012:
            var5.q(iw.l);
        }

        m.t(var5, 500000);
    }

    @ObfuscatedName("bb")
    public al bb() {
        if (!this.w.i()) {
            return null;
        } else if (!this.z.p()) {
            return null;
        } else {
            HashMap var1 = this.z.o();
            this.br = new LinkedList();
            Iterator var2 = var1.values().iterator();

            while (var2.hasNext()) {
                List var3 = (List) var2.next();
                this.br.addAll(var3);
            }

            this.ba = this.br.iterator();
            return this.bq();
        }
    }

    @ObfuscatedName("bq")
    public al bq() {
        if (this.ba == null) {
            return null;
        } else {
            return !this.ba.hasNext() ? null : (al) this.ba.next();
        }
    }
}
