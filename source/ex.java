import java.net.URL;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ex")
public class ex extends ky {

    @ObfuscatedName("t")
    final boolean t;

    public ex(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (var2.l != var1.l) {
            return this.t ? var1.l - var2.l : var2.l - var1.l;
        } else {
            return this.p(var1, var2);
        }
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("t")
    static boolean t() {
        try {
            if (Server.g == null) {
                Server.g = ak.en.t(new URL(GrandExchangeOffer.bj));
            } else if (Server.g.t()) {
                byte[] var0 = Server.g.q();
                ByteBuffer var1 = new ByteBuffer(var0);
                var1.ap();
                Server.b = var1.ae();
                Server.l = new Server[Server.b];

                Server var3;
                for (int var2 = 0; var2 < Server.b; var3.index = var2++) {
                    var3 = Server.l[var2] = new Server();
                    var3.number = var1.ae();
                    var3.type = var1.ap();
                    var3.domain = var1.ar();
                    var3.activity = var1.ar();
                    var3.location = var1.av();
                    var3.population = var1.am();
                }

                ItemPile.i(Server.l, 0, Server.l.length - 1, Server.p, Server.x);
                Server.g = null;
                return true;
            }
        } catch (Exception var4) {
            var4.printStackTrace();
            Server.g = null;
        }

        return false;
    }

    @ObfuscatedName("ep")
    static lo ep() {
        return ip.rz;
    }
}
