import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ch")
public final class Npc extends Character {

    @ObfuscatedName("x")
    static int x;

    @ObfuscatedName("t")
    NpcDefinition composite;

    @ObfuscatedName("t")
    final void t(int var1, byte var2) {
        int var3 = super.co[0];
        int var4 = super.cv[0];
        if (var1 == 0) {
            --var3;
            ++var4;
        }

        if (var1 == 1) {
            ++var4;
        }

        if (var1 == 2) {
            ++var3;
            ++var4;
        }

        if (var1 == 3) {
            --var3;
        }

        if (var1 == 4) {
            ++var3;
        }

        if (var1 == 5) {
            --var3;
            --var4;
        }

        if (var1 == 6) {
            --var4;
        }

        if (var1 == 7) {
            ++var3;
            --var4;
        }

        if (super.animationId != -1 && ff.t(super.animationId).z == 1) {
            super.animationId = -1;
        }

        if (super.speed < 9) {
            ++super.speed;
        }

        for (int var5 = super.speed; var5 > 0; --var5) {
            super.co[var5] = super.co[var5 - 1];
            super.cv[var5] = super.cv[var5 - 1];
            super.cd[var5] = super.cd[var5 - 1];
        }

        super.co[0] = var3;
        super.cv[0] = var4;
        super.cd[0] = var2;
    }

    @ObfuscatedName("q")
    final void q(int var1, int var2, boolean var3) {
        if (super.animationId != -1 && ff.t(super.animationId).z == 1) {
            super.animationId = -1;
        }

        if (!var3) {
            int var4 = var1 - super.co[0];
            int var5 = var2 - super.cv[0];
            if (var4 >= -8 && var4 <= 8 && var5 >= -8 && var5 <= 8) {
                if (super.speed < 9) {
                    ++super.speed;
                }

                for (int var6 = super.speed; var6 > 0; --var6) {
                    super.co[var6] = super.co[var6 - 1];
                    super.cv[var6] = super.cv[var6 - 1];
                    super.cd[var6] = super.cd[var6 - 1];
                }

                super.co[0] = var1;
                super.cv[0] = var2;
                super.cd[0] = 1;
                return;
            }
        }

        super.speed = 0;
        super.ci = 0;
        super.cq = 0;
        super.co[0] = var1;
        super.cv[0] = var2;
        super.regionX = super.ap * 64 + super.co[0] * 128;
        super.regionY = super.ap * 64 + super.cv[0] * 128;
    }

    @ObfuscatedName("p")
    protected final Model p() {
        if (this.composite == null) {
            return null;
        } else {
            kf var1 = super.animationId != -1 && super.bq == 0 ? ff.t(super.animationId) : null;
            kf var2 = super.bs == -1 || super.bs == super.au && var1 != null ? null : ff.t(super.bs);
            Model var3 = this.composite.b(var1, super.animationFrameIndex, var2, super.stanceFrameIndex);
            if (var3 == null) {
                return null;
            } else {
                var3.b();
                super.cs = var3.modelHeight;
                if (super.bx != -1 && super.graphicId != -1) {
                    Model var4 = ah.t(super.bx).a(super.graphicId);
                    if (var4 != null) {
                        var4.f(0, -super.bi, 0);
                        Model[] var5 = new Model[] { var3, var4 };
                        var3 = new Model(var5, 2);
                    }
                }

                if (this.composite.e == 1) {
                    var3.ay = true;
                }

                return var3;
            }
        }
    }

    @ObfuscatedName("k")
    final boolean k() {
        return this.composite != null;
    }

    @ObfuscatedName("t")
    public static void t(String[] var0, short[] var1, int var2, int var3) {
        if (var2 < var3) {
            int var4 = (var3 + var2) / 2;
            int var5 = var2;
            String var6 = var0[var4];
            var0[var4] = var0[var3];
            var0[var3] = var6;
            short var7 = var1[var4];
            var1[var4] = var1[var3];
            var1[var3] = var7;

            for (int var8 = var2; var8 < var3; ++var8) {
                if (var6 == null || var0[var8] != null && var0[var8].compareTo(var6) < (var8 & 1)) {
                    String var9 = var0[var8];
                    var0[var8] = var0[var5];
                    var0[var5] = var9;
                    short var10 = var1[var8];
                    var1[var8] = var1[var5];
                    var1[var5++] = var10;
                }
            }

            var0[var3] = var0[var5];
            var0[var5] = var6;
            var1[var3] = var1[var5];
            var1[var5] = var7;
            t(var0, var1, var2, var5 - 1);
            t(var0, var1, var5 + 1, var3);
        }

    }

    @ObfuscatedName("l")
    public static void l() {
        try {
            File var0 = new File(fh.r, "random.dat");
            int var2;
            if (var0.exists()) {
                fh.o = new RSRandomAccessFileChannel(new RSRandomAccessFile(var0, "rw", 25L), 24, 0);
            } else {
                label38: for (int var1 = 0; var1 < fy.z.length; ++var1) {
                    for (var2 = 0; var2 < s.k.length; ++var2) {
                        File var3 = new File(s.k[var2] + fy.z[var1] + File.separatorChar + "random.dat");
                        if (var3.exists()) {
                            fh.o = new RSRandomAccessFileChannel(new RSRandomAccessFile(var3, "rw", 25L), 24, 0);
                            break label38;
                        }
                    }
                }
            }

            if (fh.o == null) {
                RandomAccessFile var4 = new RandomAccessFile(var0, "rw");
                var2 = var4.read();
                var4.seek(0L);
                var4.write(var2);
                var4.seek(0L);
                var4.close();
                fh.o = new RSRandomAccessFileChannel(new RSRandomAccessFile(var0, "rw", 25L), 24, 0);
            }
        } catch (IOException var5) {
            ;
        }

    }

    @ObfuscatedName("e")
    public static void e(int var0, FileSystem var1, int var2, int var3, int var4, boolean var5) {
        hi.audioTrackId = 1;
        hi.b = var1;
        fm.e = var2;
        hi.x = var3;
        hi.p = var4;
        cr.n = var5;
        i.g = var0;
    }
}
