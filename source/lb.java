import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lb")
public class lb {

    @ObfuscatedName("t")
    public int t;

    @ObfuscatedName("q")
    public int q;

    @ObfuscatedName("i")
    public int i;

    @ObfuscatedName("a")
    public int a;

    public lb(int var1, int var2, int var3, int var4) {
        this.t(var1, var2);
        this.q(var3, var4);
    }

    public lb(int var1, int var2) {
        this(0, 0, var1, var2);
    }

    @ObfuscatedName("t")
    public void t(int var1, int var2) {
        this.t = var1;
        this.q = var2;
    }

    @ObfuscatedName("q")
    public void q(int var1, int var2) {
        this.i = var1;
        this.a = var2;
    }

    @ObfuscatedName("i")
    public void i(lb var1, lb var2) {
        this.a(var1, var2);
        this.l(var1, var2);
    }

    @ObfuscatedName("a")
    void a(lb var1, lb var2) {
        var2.t = this.t;
        var2.i = this.i;
        if (this.t < var1.t) {
            var2.i -= var1.t - this.t;
            var2.t = var1.t;
        }

        if (var2.b() > var1.b()) {
            var2.i -= var2.b() - var1.b();
        }

        if (var2.i < 0) {
            var2.i = 0;
        }

    }

    @ObfuscatedName("l")
    void l(lb var1, lb var2) {
        var2.q = this.q;
        var2.a = this.a;
        if (this.q < var1.q) {
            var2.a -= var1.q - this.q;
            var2.q = var1.q;
        }

        if (var2.e() > var1.e()) {
            var2.a -= var2.e() - var1.e();
        }

        if (var2.a < 0) {
            var2.a = 0;
        }

    }

    @ObfuscatedName("b")
    int b() {
        return this.t + this.i;
    }

    @ObfuscatedName("e")
    int e() {
        return this.q + this.a;
    }

    public String toString() {
        return null;
    }
}
