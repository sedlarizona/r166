import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hx")
public class Node {

    @ObfuscatedName("ce")
    public long uid;

    @ObfuscatedName("cx")
    public Node next;

    @ObfuscatedName("cy")
    Node previous;

    @ObfuscatedName("kc")
    public void kc() {
        if (this.previous != null) {
            this.previous.next = this.next;
            this.next.previous = this.previous;
            this.next = null;
            this.previous = null;
        }
    }

    @ObfuscatedName("kg")
    public boolean kg() {
        return this.previous != null;
    }
}
