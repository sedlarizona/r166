import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fg")
public class fg extends ky {

    @ObfuscatedName("bb")
    static lk bb;

    @ObfuscatedName("t")
    final boolean t;

    public fg(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (var2.e != var1.e) {
            return this.t ? var1.e - var2.e : var2.e - var1.e;
        } else {
            return this.p(var1, var2);
        }
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }
}
