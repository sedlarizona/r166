import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hy")
public class hy {

    @ObfuscatedName("g")
    static final byte[] g = new byte[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    @ObfuscatedName("t")
    ByteBuffer t = new ByteBuffer((byte[]) null);

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int[] i;

    @ObfuscatedName("a")
    int[] a;

    @ObfuscatedName("l")
    int[] l;

    @ObfuscatedName("b")
    int[] b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("p")
    long p;

    hy(byte[] var1) {
        this.t(var1);
    }

    hy() {
    }

    @ObfuscatedName("t")
    void t(byte[] var1) {
        this.t.buffer = var1;
        this.t.index = 10;
        int var2 = this.t.ae();
        this.q = this.t.ae();
        this.e = 500000;
        this.i = new int[var2];

        int var3;
        int var5;
        for (var3 = 0; var3 < var2; this.t.index += var5) {
            int var4 = this.t.ap();
            var5 = this.t.ap();
            if (var4 == 1297379947) {
                this.i[var3] = this.t.index;
                ++var3;
            }
        }

        this.p = 0L;
        this.a = new int[var2];

        for (var3 = 0; var3 < var2; ++var3) {
            this.a[var3] = this.i[var3];
        }

        this.l = new int[var2];
        this.b = new int[var2];
    }

    @ObfuscatedName("q")
    void q() {
        this.t.buffer = null;
        this.i = null;
        this.a = null;
        this.l = null;
        this.b = null;
    }

    @ObfuscatedName("i")
    boolean i() {
        return this.t.buffer != null;
    }

    @ObfuscatedName("a")
    int a() {
        return this.a.length;
    }

    @ObfuscatedName("l")
    void l(int var1) {
        this.t.index = this.a[var1];
    }

    @ObfuscatedName("b")
    void b(int var1) {
        this.a[var1] = this.t.index;
    }

    @ObfuscatedName("e")
    void e() {
        this.t.index = -1;
    }

    @ObfuscatedName("x")
    void x(int var1) {
        int var2 = this.t.aq();
        this.l[var1] += var2;
    }

    @ObfuscatedName("p")
    int p(int var1) {
        int var2 = this.o(var1);
        return var2;
    }

    @ObfuscatedName("o")
    int o(int var1) {
        byte var2 = this.t.buffer[this.t.index];
        int var5;
        if (var2 < 0) {
            var5 = var2 & 255;
            this.b[var1] = var5;
            ++this.t.index;
        } else {
            var5 = this.b[var1];
        }

        if (var5 != 240 && var5 != 247) {
            return this.c(var1, var5);
        } else {
            int var3 = this.t.aq();
            if (var5 == 247 && var3 > 0) {
                int var4 = this.t.buffer[this.t.index] & 255;
                if (var4 >= 241 && var4 <= 243 || var4 == 246 || var4 == 248 || var4 >= 250 && var4 <= 252
                        || var4 == 254) {
                    ++this.t.index;
                    this.b[var1] = var4;
                    return this.c(var1, var4);
                }
            }

            this.t.index += var3;
            return 0;
        }
    }

    @ObfuscatedName("c")
    int c(int var1, int var2) {
        int var4;
        if (var2 == 255) {
            int var7 = this.t.av();
            var4 = this.t.aq();
            if (var7 == 47) {
                this.t.index += var4;
                return 1;
            } else if (var7 == 81) {
                int var5 = this.t.az();
                var4 -= 3;
                int var6 = this.l[var1];
                this.p += (long) var6 * (long) (this.e - var5);
                this.e = var5;
                this.t.index += var4;
                return 2;
            } else {
                this.t.index += var4;
                return 3;
            }
        } else {
            byte var3 = g[var2 - 128];
            var4 = var2;
            if (var3 >= 1) {
                var4 = var2 | this.t.av() << 8;
            }

            if (var3 >= 2) {
                var4 |= this.t.av() << 16;
            }

            return var4;
        }
    }

    @ObfuscatedName("u")
    long u(int var1) {
        return this.p + (long) var1 * (long) this.e;
    }

    @ObfuscatedName("k")
    int k() {
        int var1 = this.a.length;
        int var2 = -1;
        int var3 = Integer.MAX_VALUE;

        for (int var4 = 0; var4 < var1; ++var4) {
            if (this.a[var4] >= 0 && this.l[var4] < var3) {
                var2 = var4;
                var3 = this.l[var4];
            }
        }

        return var2;
    }

    @ObfuscatedName("z")
    boolean z() {
        int var1 = this.a.length;

        for (int var2 = 0; var2 < var1; ++var2) {
            if (this.a[var2] >= 0) {
                return false;
            }
        }

        return true;
    }

    @ObfuscatedName("w")
    void w(long var1) {
        this.p = var1;
        int var3 = this.a.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            this.l[var4] = 0;
            this.b[var4] = 0;
            this.t.index = this.i[var4];
            this.x(var4);
            this.a[var4] = this.t.index;
        }

    }
}
