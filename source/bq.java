import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bq")
public class bq {

    @ObfuscatedName("fp")
    static int[] fp;

    @ObfuscatedName("hl")
    static final void hl(MenuItemNode var0, int var1, int var2) {
        a.hu(var0.q, var0.i, var0.a, var0.l, var0.text, var0.text, var1, var2);
    }

    @ObfuscatedName("jf")
    static final void jf(String var0) {
        if (ad.ot != null) {
            gd var1 = ap.t(fo.bd, Client.ee.l);
            var1.i.a(z.c(var0));
            var1.i.u(var0);
            Client.ee.i(var1);
        }
    }
}
