import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lu")
public class lu {

    @ObfuscatedName("t")
    public static final lu t = new lu(2, 0, "", "");

    @ObfuscatedName("q")
    static final lu q = new lu(4, 1, "", "");

    @ObfuscatedName("i")
    static final lu i = new lu(0, 2, "", "");

    @ObfuscatedName("a")
    static final lu a = new lu(8, 3, "", "");

    @ObfuscatedName("l")
    static final lu l = new lu(3, 4, "", "");

    @ObfuscatedName("b")
    static final lu b = new lu(5, 5, "", "");

    @ObfuscatedName("e")
    static final lu e = new lu(1, 6, "", "");

    @ObfuscatedName("x")
    static final lu x = new lu(7, 7, "", "");

    @ObfuscatedName("p")
    public static final lu p;

    @ObfuscatedName("g")
    public final int g;

    @ObfuscatedName("n")
    final String n;

    static {
        p = new lu(6, -1, "", "", true, new lu[] { t, q, i, l, a });
    }

    lu(int var1, int var2, String var3, String var4) {
        this.g = var1;
        this.n = var4;
    }

    lu(int var1, int var2, String var3, String var4, boolean var5, lu[] var6) {
        this.g = var1;
        this.n = var4;
    }

    public String toString() {
        return this.n;
    }
}
