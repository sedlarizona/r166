import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ha")
public class Link {

    @ObfuscatedName("t")
    Link previous;

    @ObfuscatedName("q")
    Link next;

    @ObfuscatedName("t")
    public void t() {
        if (this.next != null) {
            this.next.previous = this.previous;
            this.previous.next = this.next;
            this.previous = null;
            this.next = null;
        }
    }
}
