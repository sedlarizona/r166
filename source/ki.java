import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ki")
public class ki extends kp {

    @ObfuscatedName("t")
    kx t;

    @ObfuscatedName("q")
    kx q;

    ki() {
        this.t = kx.t;
        this.q = kx.t;
    }

    @ObfuscatedName("t")
    void t() {
        this.t = kx.t;
    }

    @ObfuscatedName("q")
    public final boolean q() {
        if (this.t == kx.t) {
            this.i();
        }

        return this.t == kx.q;
    }

    @ObfuscatedName("i")
    void i() {
        this.t = BoundaryObject.qi.l.z(super.i) ? kx.q : kx.i;
    }

    @ObfuscatedName("a")
    void a() {
        this.q = kx.t;
    }

    @ObfuscatedName("l")
    public final boolean l() {
        if (this.q == kx.t) {
            this.b();
        }

        return this.q == kx.q;
    }

    @ObfuscatedName("b")
    void b() {
        this.q = BoundaryObject.qi.b.z(super.i) ? kx.q : kx.i;
    }

    @ObfuscatedName("jv")
    static String jv(String var0, boolean var1) {
        String var2 = var1 ? "https://" : "http://";
        if (Client.playerWeight == 1) {
            var0 = var0 + "-wtrc";
        } else if (Client.playerWeight == 2) {
            var0 = var0 + "-wtqa";
        } else if (Client.playerWeight == 3) {
            var0 = var0 + "-wtwip";
        } else if (Client.playerWeight == 5) {
            var0 = var0 + "-wti";
        } else if (Client.playerWeight == 4) {
            var0 = "local";
        }

        String var3 = "";
        if (Projectile.bt != null) {
            var3 = "/p=" + Projectile.bt;
        }

        String var4 = "runescape.com";
        return var2 + var0 + "." + var4 + "/l=" + Client.bs + "/a=" + jv.by + var3 + "/";
    }
}
