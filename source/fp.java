import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fp")
public class fp extends ky {

    @ObfuscatedName("t")
    final boolean t;

    public fp(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (var1.l != 0 && var2.l != 0) {
            return this.t ? var1.r().i(var2.r()) : var2.r().i(var1.r());
        } else {
            return this.p(var1, var2);
        }
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("t")
    static String t(int var0) {
        return "<img=" + var0 + ">";
    }
}
