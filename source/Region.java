import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ew")
public class Region {

    @ObfuscatedName("t")
    public static boolean t = true;

    @ObfuscatedName("u")
    static int u = 0;

    @ObfuscatedName("j")
    static int j = 0;

    @ObfuscatedName("k")
    static int k;

    @ObfuscatedName("z")
    static int z;

    @ObfuscatedName("w")
    static int w;

    @ObfuscatedName("s")
    static int s;

    @ObfuscatedName("d")
    static int d;

    @ObfuscatedName("f")
    static int f;

    @ObfuscatedName("r")
    static int r;

    @ObfuscatedName("y")
    static int y;

    @ObfuscatedName("h")
    static int h;

    @ObfuscatedName("m")
    static int m;

    @ObfuscatedName("ay")
    static int ay;

    @ObfuscatedName("ao")
    static int ao;

    @ObfuscatedName("av")
    static int av;

    @ObfuscatedName("aj")
    static int aj;

    @ObfuscatedName("ae")
    static EventObject[] ae = new EventObject[100];

    @ObfuscatedName("am")
    static boolean am = false;

    @ObfuscatedName("az")
    static int az = 0;

    @ObfuscatedName("ap")
    static int ap = 0;

    @ObfuscatedName("ah")
    static int ah = 0;

    @ObfuscatedName("au")
    public static int au = -1;

    @ObfuscatedName("ax")
    public static int ax = -1;

    @ObfuscatedName("ar")
    static boolean ar = false;

    @ObfuscatedName("al")
    static int al = 4;

    @ObfuscatedName("at")
    static int[] at;

    @ObfuscatedName("ag")
    static es[][] ag;

    @ObfuscatedName("as")
    static int as;

    @ObfuscatedName("aw")
    static es[] aw;

    @ObfuscatedName("aq")
    static Deque aq;

    @ObfuscatedName("aa")
    static final int[] aa;

    @ObfuscatedName("af")
    static final int[] af;

    @ObfuscatedName("ak")
    static final int[] ak;

    @ObfuscatedName("ab")
    static final int[] ab;

    @ObfuscatedName("ac")
    static final int[] ac;

    @ObfuscatedName("ad")
    static final int[] ad;

    @ObfuscatedName("bg")
    static final int[] bg;

    @ObfuscatedName("bk")
    static boolean[][][][] bk;

    @ObfuscatedName("be")
    static boolean[][] be;

    @ObfuscatedName("bc")
    static int bc;

    @ObfuscatedName("bm")
    static int bm;

    @ObfuscatedName("bh")
    static int bh;

    @ObfuscatedName("bs")
    static int bs;

    @ObfuscatedName("bj")
    static int bj;

    @ObfuscatedName("bt")
    static int bt;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int[][][] l;

    @ObfuscatedName("b")
    Tile[][][] tiles;

    @ObfuscatedName("e")
    int e = 0;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    EventObject[] eventObjects = new EventObject[5000];

    @ObfuscatedName("g")
    int[][][] g;

    @ObfuscatedName("br")
    int[][] br = new int[][] { { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1 },
            { 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 }, { 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1 },
            { 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0 },
            { 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1 }, { 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 },
            { 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1 } };

    @ObfuscatedName("ba")
    int[][] ba = new int[][] { { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 },
            { 12, 8, 4, 0, 13, 9, 5, 1, 14, 10, 6, 2, 15, 11, 7, 3 },
            { 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 },
            { 3, 7, 11, 15, 2, 6, 10, 14, 1, 5, 9, 13, 0, 4, 8, 12 } };

    static {
        at = new int[al];
        ag = new es[al][500];
        as = 0;
        aw = new es[500];
        aq = new Deque();
        aa = new int[] { 19, 55, 38, 155, 255, 110, 137, 205, 76 };
        af = new int[] { 160, 192, 80, 96, 0, 144, 80, 48, 160 };
        ak = new int[] { 76, 8, 137, 4, 0, 1, 38, 2, 19 };
        ab = new int[] { 0, 0, 2, 0, 0, 2, 1, 1, 0 };
        ac = new int[] { 2, 0, 0, 2, 0, 0, 0, 4, 4 };
        ad = new int[] { 0, 4, 4, 8, 0, 0, 8, 0, 0 };
        bg = new int[] { 1, 1, 0, 0, 0, 8, 0, 0, 8 };
        bk = new boolean[8][32][51][51];
    }

    public Region(int var1, int var2, int var3, int[][][] var4) {
        this.q = var1;
        this.i = var2;
        this.a = var3;
        this.tiles = new Tile[var1][var2][var3];
        this.g = new int[var1][var2 + 1][var3 + 1];
        this.l = var4;
        this.t();
    }

    @ObfuscatedName("t")
    public void t() {
        int var1;
        int var2;
        for (var1 = 0; var1 < this.q; ++var1) {
            for (var2 = 0; var2 < this.i; ++var2) {
                for (int var3 = 0; var3 < this.a; ++var3) {
                    this.tiles[var1][var2][var3] = null;
                }
            }
        }

        for (var1 = 0; var1 < al; ++var1) {
            for (var2 = 0; var2 < at[var1]; ++var2) {
                ag[var1][var2] = null;
            }

            at[var1] = 0;
        }

        for (var1 = 0; var1 < this.x; ++var1) {
            this.eventObjects[var1] = null;
        }

        this.x = 0;

        for (var1 = 0; var1 < ae.length; ++var1) {
            ae[var1] = null;
        }

    }

    @ObfuscatedName("q")
    public void q(int var1) {
        this.e = var1;

        for (int var2 = 0; var2 < this.i; ++var2) {
            for (int var3 = 0; var3 < this.a; ++var3) {
                if (this.tiles[var1][var2][var3] == null) {
                    this.tiles[var1][var2][var3] = new Tile(var1, var2, var3);
                }
            }
        }

    }

    @ObfuscatedName("i")
    public void i(int var1, int var2) {
        Tile var3 = this.tiles[0][var1][var2];

        for (int var4 = 0; var4 < 3; ++var4) {
            Tile var5 = this.tiles[var4][var1][var2] = this.tiles[var4 + 1][var1][var2];
            if (var5 != null) {
                --var5.plane;

                for (int var6 = 0; var6 < var5.n; ++var6) {
                    EventObject var7 = var5.eventObjects[var6];
                    if ((var7.hash >> 29 & 3) == 2 && var7.x == var1 && var2 == var7.y) {
                        --var7.plane;
                    }
                }
            }
        }

        if (this.tiles[0][var1][var2] == null) {
            this.tiles[0][var1][var2] = new Tile(0, var1, var2);
        }

        this.tiles[0][var1][var2].baseTile = var3;
        this.tiles[3][var1][var2] = null;
    }

    @ObfuscatedName("l")
    public void l(int var1, int var2, int var3, int var4) {
        Tile var5 = this.tiles[var1][var2][var3];
        if (var5 != null) {
            this.tiles[var1][var2][var3].u = var4;
        }
    }

    @ObfuscatedName("b")
    public void b(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10,
            int var11, int var12, int var13, int var14, int var15, int var16, int var17, int var18, int var19, int var20) {
        TileModel var21;
        int var22;
        if (var4 == 0) {
            var21 = new TileModel(var11, var12, var13, var14, -1, var19, false);

            for (var22 = var1; var22 >= 0; --var22) {
                if (this.tiles[var22][var2][var3] == null) {
                    this.tiles[var22][var2][var3] = new Tile(var22, var2, var3);
                }
            }

            this.tiles[var1][var2][var3].tileModel = var21;
        } else if (var4 != 1) {
            TilePaint var23 = new TilePaint(var4, var5, var6, var2, var3, var7, var8, var9, var10, var11, var12, var13,
                    var14, var15, var16, var17, var18, var19, var20);

            for (var22 = var1; var22 >= 0; --var22) {
                if (this.tiles[var22][var2][var3] == null) {
                    this.tiles[var22][var2][var3] = new Tile(var22, var2, var3);
                }
            }

            this.tiles[var1][var2][var3].tilePaint = var23;
        } else {
            var21 = new TileModel(var15, var16, var17, var18, var6, var20, var8 == var7 && var7 == var9
                    && var10 == var7);

            for (var22 = var1; var22 >= 0; --var22) {
                if (this.tiles[var22][var2][var3] == null) {
                    this.tiles[var22][var2][var3] = new Tile(var22, var2, var3);
                }
            }

            this.tiles[var1][var2][var3].tileModel = var21;
        }
    }

    @ObfuscatedName("e")
    public void e(int var1, int var2, int var3, int var4, Renderable var5, int var6, int var7) {
        if (var5 != null) {
            FloorObject var8 = new FloorObject();
            var8.model = var5;
            var8.regionX = var2 * 128 + 64;
            var8.regionY = var3 * 128 + 64;
            var8.height = var4;
            var8.hash = var6;
            var8.placement = var7;
            if (this.tiles[var1][var2][var3] == null) {
                this.tiles[var1][var2][var3] = new Tile(var1, var2, var3);
            }

            this.tiles[var1][var2][var3].floorObject = var8;
        }
    }

    @ObfuscatedName("x")
    public void x(int var1, int var2, int var3, int var4, Renderable var5, int var6, Renderable var7, Renderable var8) {
        ItemPile var9 = new ItemPile();
        var9.bottom = var5;
        var9.regionX = var2 * 128 + 64;
        var9.regionY = var3 * 128 + 64;
        var9.groundElevation = var4;
        var9.hash = var6;
        var9.middle = var7;
        var9.top = var8;
        int var10 = 0;
        Tile var11 = this.tiles[var1][var2][var3];
        if (var11 != null) {
            for (int var12 = 0; var12 < var11.n; ++var12) {
                if ((var11.eventObjects[var12].placement & 256) == 256
                        && var11.eventObjects[var12].model instanceof Model) {
                    Model var13 = (Model) var11.eventObjects[var12].model;
                    var13.b();
                    if (var13.modelHeight > var10) {
                        var10 = var13.modelHeight;
                    }
                }
            }
        }

        var9.objectElevation = var10;
        if (this.tiles[var1][var2][var3] == null) {
            this.tiles[var1][var2][var3] = new Tile(var1, var2, var3);
        }

        this.tiles[var1][var2][var3].itemPile = var9;
    }

    @ObfuscatedName("p")
    public void p(int var1, int var2, int var3, int var4, Renderable var5, Renderable var6, int var7, int var8,
            int var9, int var10) {
        if (var5 != null || var6 != null) {
            BoundaryObject var11 = new BoundaryObject();
            var11.hash = var9;
            var11.placement = var10;
            var11.regionX = var2 * 128 + 64;
            var11.regionY = var3 * 128 + 64;
            var11.plane = var4;
            var11.modelA = var5;
            var11.modelB = var6;
            var11.orientationA = var7;
            var11.orientationB = var8;

            for (int var12 = var1; var12 >= 0; --var12) {
                if (this.tiles[var12][var2][var3] == null) {
                    this.tiles[var12][var2][var3] = new Tile(var12, var2, var3);
                }
            }

            this.tiles[var1][var2][var3].boundaryObject = var11;
        }
    }

    @ObfuscatedName("o")
    public void o(int var1, int var2, int var3, int var4, Renderable var5, Renderable var6, int var7, int var8,
            int var9, int var10, int var11, int var12) {
        if (var5 != null) {
            WallObject var13 = new WallObject();
            var13.hash = var11;
            var13.placement = var12;
            var13.regionX = var2 * 128 + 64;
            var13.regionY = var3 * 128 + 64;
            var13.height = var4;
            var13.modelA = var5;
            var13.modelB = var6;
            var13.orientationA = var7;
            var13.orientationB = var8;
            var13.insetX = var9;
            var13.insetY = var10;

            for (int var14 = var1; var14 >= 0; --var14) {
                if (this.tiles[var14][var2][var3] == null) {
                    this.tiles[var14][var2][var3] = new Tile(var14, var2, var3);
                }
            }

            this.tiles[var1][var2][var3].wallObject = var13;
        }
    }

    @ObfuscatedName("c")
    public boolean c(int var1, int var2, int var3, int var4, int var5, int var6, Renderable var7, int var8, int var9,
            int var10) {
        if (var7 == null) {
            return true;
        } else {
            int var11 = var5 * 64 + var2 * 128;
            int var12 = var6 * 64 + var3 * 128;
            return this.z(var1, var2, var3, var5, var6, var11, var12, var4, var7, var8, false, var9, var10);
        }
    }

    @ObfuscatedName("u")
    public boolean u(int var1, int var2, int var3, int var4, int var5, Renderable var6, int var7, int var8, boolean var9) {
        if (var6 == null) {
            return true;
        } else {
            int var10 = var2 - var5;
            int var11 = var3 - var5;
            int var12 = var5 + var2;
            int var13 = var3 + var5;
            if (var9) {
                if (var7 > 640 && var7 < 1408) {
                    var13 += 128;
                }

                if (var7 > 1152 && var7 < 1920) {
                    var12 += 128;
                }

                if (var7 > 1664 || var7 < 384) {
                    var11 -= 128;
                }

                if (var7 > 128 && var7 < 896) {
                    var10 -= 128;
                }
            }

            var10 /= 128;
            var11 /= 128;
            var12 /= 128;
            var13 /= 128;
            return this.z(var1, var10, var11, var12 - var10 + 1, var13 - var11 + 1, var2, var3, var4, var6, var7, true,
                    var8, 0);
        }
    }

    @ObfuscatedName("k")
    public boolean k(int var1, int var2, int var3, int var4, int var5, Renderable var6, int var7, int var8, int var9,
            int var10, int var11, int var12) {
        return var6 == null ? true : this.z(var1, var9, var10, var11 - var9 + 1, var12 - var10 + 1, var2, var3, var4,
                var6, var7, true, var8, 0);
    }

    @ObfuscatedName("z")
    boolean z(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, Renderable var9,
            int var10, boolean var11, int var12, int var13) {
        int var15;
        for (int var14 = var2; var14 < var2 + var4; ++var14) {
            for (var15 = var3; var15 < var3 + var5; ++var15) {
                if (var14 < 0 || var15 < 0 || var14 >= this.i || var15 >= this.a) {
                    return false;
                }

                Tile var22 = this.tiles[var1][var14][var15];
                if (var22 != null && var22.n >= 5) {
                    return false;
                }
            }
        }

        EventObject var20 = new EventObject();
        var20.hash = var12;
        var20.placement = var13;
        var20.plane = var1;
        var20.regionX = var6;
        var20.regionY = var7;
        var20.height = var8;
        var20.model = var9;
        var20.orientation = var10;
        var20.x = var2;
        var20.y = var3;
        var20.sizeX = var2 + var4 - 1;
        var20.sizeY = var3 + var5 - 1;

        for (var15 = var2; var15 < var2 + var4; ++var15) {
            for (int var16 = var3; var16 < var3 + var5; ++var16) {
                int var17 = 0;
                if (var15 > var2) {
                    ++var17;
                }

                if (var15 < var2 + var4 - 1) {
                    var17 += 4;
                }

                if (var16 > var3) {
                    var17 += 8;
                }

                if (var16 < var3 + var5 - 1) {
                    var17 += 2;
                }

                for (int var18 = var1; var18 >= 0; --var18) {
                    if (this.tiles[var18][var15][var16] == null) {
                        this.tiles[var18][var15][var16] = new Tile(var18, var15, var16);
                    }
                }

                Tile var21 = this.tiles[var1][var15][var16];
                var21.eventObjects[var21.n] = var20;
                var21.boundaryRenderBits[var21.n] = var17;
                var21.v |= var17;
                ++var21.n;
            }
        }

        if (var11) {
            this.eventObjects[this.x++] = var20;
        }

        return true;
    }

    @ObfuscatedName("w")
    public void w() {
        for (int var1 = 0; var1 < this.x; ++var1) {
            EventObject var2 = this.eventObjects[var1];
            this.s(var2);
            this.eventObjects[var1] = null;
        }

        this.x = 0;
    }

    @ObfuscatedName("s")
    void s(EventObject var1) {
        for (int var2 = var1.x; var2 <= var1.sizeX; ++var2) {
            for (int var3 = var1.y; var3 <= var1.sizeY; ++var3) {
                Tile var4 = this.tiles[var1.plane][var2][var3];
                if (var4 != null) {
                    int var5;
                    for (var5 = 0; var5 < var4.n; ++var5) {
                        if (var4.eventObjects[var5] == var1) {
                            --var4.n;

                            for (int var6 = var5; var6 < var4.n; ++var6) {
                                var4.eventObjects[var6] = var4.eventObjects[var6 + 1];
                                var4.boundaryRenderBits[var6] = var4.boundaryRenderBits[var6 + 1];
                            }

                            var4.eventObjects[var4.n] = null;
                            break;
                        }
                    }

                    var4.v = 0;

                    for (var5 = 0; var5 < var4.n; ++var5) {
                        var4.v |= var4.boundaryRenderBits[var5];
                    }
                }
            }
        }

    }

    @ObfuscatedName("d")
    public void d(int var1, int var2, int var3, int var4) {
        Tile var5 = this.tiles[var1][var2][var3];
        if (var5 != null) {
            WallObject var6 = var5.wallObject;
            if (var6 != null) {
                var6.insetX = var4 * var6.insetX / 16;
                var6.insetY = var4 * var6.insetY / 16;
            }
        }
    }

    @ObfuscatedName("f")
    public void f(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 != null) {
            var4.boundaryObject = null;
        }
    }

    @ObfuscatedName("r")
    public void r(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 != null) {
            var4.wallObject = null;
        }
    }

    @ObfuscatedName("y")
    public void y(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 != null) {
            for (int var5 = 0; var5 < var4.n; ++var5) {
                EventObject var6 = var4.eventObjects[var5];
                if ((var6.hash >> 29 & 3) == 2 && var2 == var6.x && var3 == var6.y) {
                    this.s(var6);
                    return;
                }
            }

        }
    }

    @ObfuscatedName("h")
    public void h(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 != null) {
            var4.floorObject = null;
        }
    }

    @ObfuscatedName("av")
    public void av(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 != null) {
            var4.itemPile = null;
        }
    }

    @ObfuscatedName("aj")
    public BoundaryObject aj(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        return var4 == null ? null : var4.boundaryObject;
    }

    @ObfuscatedName("ae")
    public WallObject ae(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        return var4 == null ? null : var4.wallObject;
    }

    @ObfuscatedName("am")
    public EventObject am(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 == null) {
            return null;
        } else {
            for (int var5 = 0; var5 < var4.n; ++var5) {
                EventObject var6 = var4.eventObjects[var5];
                if ((var6.hash >> 29 & 3) == 2 && var2 == var6.x && var3 == var6.y) {
                    return var6;
                }
            }

            return null;
        }
    }

    @ObfuscatedName("az")
    public FloorObject az(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        return var4 != null && var4.floorObject != null ? var4.floorObject : null;
    }

    @ObfuscatedName("ap")
    public int ap(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        return var4 != null && var4.boundaryObject != null ? var4.boundaryObject.hash : 0;
    }

    @ObfuscatedName("ah")
    public int ah(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        return var4 != null && var4.wallObject != null ? var4.wallObject.hash : 0;
    }

    @ObfuscatedName("au")
    public int au(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        if (var4 == null) {
            return 0;
        } else {
            for (int var5 = 0; var5 < var4.n; ++var5) {
                EventObject var6 = var4.eventObjects[var5];
                if ((var6.hash >> 29 & 3) == 2 && var2 == var6.x && var3 == var6.y) {
                    return var6.hash;
                }
            }

            return 0;
        }
    }

    @ObfuscatedName("ax")
    public int ax(int var1, int var2, int var3) {
        Tile var4 = this.tiles[var1][var2][var3];
        return var4 != null && var4.floorObject != null ? var4.floorObject.hash : 0;
    }

    @ObfuscatedName("ar")
    public int ar(int var1, int var2, int var3, int var4) {
        Tile var5 = this.tiles[var1][var2][var3];
        if (var5 == null) {
            return -1;
        } else if (var5.boundaryObject != null && var5.boundaryObject.hash == var4) {
            return var5.boundaryObject.placement & 255;
        } else if (var5.wallObject != null && var5.wallObject.hash == var4) {
            return var5.wallObject.placement & 255;
        } else if (var5.floorObject != null && var5.floorObject.hash == var4) {
            return var5.floorObject.placement & 255;
        } else {
            for (int var6 = 0; var6 < var5.n; ++var6) {
                if (var4 == var5.eventObjects[var6].hash) {
                    return var5.eventObjects[var6].placement & 255;
                }
            }

            return -1;
        }
    }

    @ObfuscatedName("an")
    public void an(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < this.q; ++var4) {
            for (int var5 = 0; var5 < this.i; ++var5) {
                for (int var6 = 0; var6 < this.a; ++var6) {
                    Tile var7 = this.tiles[var4][var5][var6];
                    if (var7 != null) {
                        BoundaryObject var8 = var7.boundaryObject;
                        AlternativeModel var10;
                        if (var8 != null && var8.modelA instanceof AlternativeModel) {
                            AlternativeModel var9 = (AlternativeModel) var8.modelA;
                            this.al(var9, var4, var5, var6, 1, 1);
                            if (var8.modelB instanceof AlternativeModel) {
                                var10 = (AlternativeModel) var8.modelB;
                                this.al(var10, var4, var5, var6, 1, 1);
                                AlternativeModel.h(var9, var10, 0, 0, 0, false);
                                var8.modelB = var10.av(var10.ah, var10.au, var1, var2, var3);
                            }

                            var8.modelA = var9.av(var9.ah, var9.au, var1, var2, var3);
                        }

                        for (int var12 = 0; var12 < var7.n; ++var12) {
                            EventObject var14 = var7.eventObjects[var12];
                            if (var14 != null && var14.model instanceof AlternativeModel) {
                                AlternativeModel var11 = (AlternativeModel) var14.model;
                                this.al(var11, var4, var5, var6, var14.sizeX - var14.x + 1, var14.sizeY - var14.y + 1);
                                var14.model = var11.av(var11.ah, var11.au, var1, var2, var3);
                            }
                        }

                        FloorObject var13 = var7.floorObject;
                        if (var13 != null && var13.model instanceof AlternativeModel) {
                            var10 = (AlternativeModel) var13.model;
                            this.ai(var10, var4, var5, var6);
                            var13.model = var10.av(var10.ah, var10.au, var1, var2, var3);
                        }
                    }
                }
            }
        }

    }

    @ObfuscatedName("ai")
    void ai(AlternativeModel var1, int var2, int var3, int var4) {
        Tile var5;
        AlternativeModel var6;
        if (var3 < this.i) {
            var5 = this.tiles[var2][var3 + 1][var4];
            if (var5 != null && var5.floorObject != null && var5.floorObject.model instanceof AlternativeModel) {
                var6 = (AlternativeModel) var5.floorObject.model;
                AlternativeModel.h(var1, var6, 128, 0, 0, true);
            }
        }

        if (var4 < this.i) {
            var5 = this.tiles[var2][var3][var4 + 1];
            if (var5 != null && var5.floorObject != null && var5.floorObject.model instanceof AlternativeModel) {
                var6 = (AlternativeModel) var5.floorObject.model;
                AlternativeModel.h(var1, var6, 0, 0, 128, true);
            }
        }

        if (var3 < this.i && var4 < this.a) {
            var5 = this.tiles[var2][var3 + 1][var4 + 1];
            if (var5 != null && var5.floorObject != null && var5.floorObject.model instanceof AlternativeModel) {
                var6 = (AlternativeModel) var5.floorObject.model;
                AlternativeModel.h(var1, var6, 128, 0, 128, true);
            }
        }

        if (var3 < this.i && var4 > 0) {
            var5 = this.tiles[var2][var3 + 1][var4 - 1];
            if (var5 != null && var5.floorObject != null && var5.floorObject.model instanceof AlternativeModel) {
                var6 = (AlternativeModel) var5.floorObject.model;
                AlternativeModel.h(var1, var6, 128, 0, -128, true);
            }
        }

    }

    @ObfuscatedName("al")
    void al(AlternativeModel var1, int var2, int var3, int var4, int var5, int var6) {
        boolean var7 = true;
        int var8 = var3;
        int var9 = var3 + var5;
        int var10 = var4 - 1;
        int var11 = var4 + var6;

        for (int var12 = var2; var12 <= var2 + 1; ++var12) {
            if (var12 != this.q) {
                for (int var13 = var8; var13 <= var9; ++var13) {
                    if (var13 >= 0 && var13 < this.i) {
                        for (int var14 = var10; var14 <= var11; ++var14) {
                            if (var14 >= 0 && var14 < this.a
                                    && (!var7 || var13 >= var9 || var14 >= var11 || var14 < var4 && var3 != var13)) {
                                Tile var15 = this.tiles[var12][var13][var14];
                                if (var15 != null) {
                                    int var16 = (this.l[var12][var13 + 1][var14] + this.l[var12][var13 + 1][var14 + 1]
                                            + this.l[var12][var13][var14] + this.l[var12][var13][var14 + 1])
                                            / 4
                                            - (this.l[var2][var3 + 1][var4] + this.l[var2][var3][var4]
                                                    + this.l[var2][var3 + 1][var4 + 1] + this.l[var2][var3][var4 + 1])
                                            / 4;
                                    BoundaryObject var17 = var15.boundaryObject;
                                    if (var17 != null) {
                                        AlternativeModel var18;
                                        if (var17.modelA instanceof AlternativeModel) {
                                            var18 = (AlternativeModel) var17.modelA;
                                            AlternativeModel.h(var1, var18, (1 - var5) * 64 + (var13 - var3) * 128,
                                                    var16, (var14 - var4) * 128 + (1 - var6) * 64, var7);
                                        }

                                        if (var17.modelB instanceof AlternativeModel) {
                                            var18 = (AlternativeModel) var17.modelB;
                                            AlternativeModel.h(var1, var18, (1 - var5) * 64 + (var13 - var3) * 128,
                                                    var16, (var14 - var4) * 128 + (1 - var6) * 64, var7);
                                        }
                                    }

                                    for (int var23 = 0; var23 < var15.n; ++var23) {
                                        EventObject var19 = var15.eventObjects[var23];
                                        if (var19 != null && var19.model instanceof AlternativeModel) {
                                            AlternativeModel var20 = (AlternativeModel) var19.model;
                                            int var21 = var19.sizeX - var19.x + 1;
                                            int var22 = var19.sizeY - var19.y + 1;
                                            AlternativeModel.h(var1, var20, (var21 - var5) * 64 + (var19.x - var3)
                                                    * 128, var16, (var19.y - var4) * 128 + (var22 - var6) * 64, var7);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                --var8;
                var7 = false;
            }
        }

    }

    @ObfuscatedName("at")
    public void at(int[] var1, int var2, int var3, int var4, int var5, int var6) {
        Tile var7 = this.tiles[var4][var5][var6];
        if (var7 != null) {
            TileModel var8 = var7.tileModel;
            int var10;
            if (var8 != null) {
                int var9 = var8.color;
                if (var9 != 0) {
                    for (var10 = 0; var10 < 4; ++var10) {
                        var1[var2] = var9;
                        var1[var2 + 1] = var9;
                        var1[var2 + 2] = var9;
                        var1[var2 + 3] = var9;
                        var2 += var3;
                    }

                }
            } else {
                TilePaint var18 = var7.tilePaint;
                if (var18 != null) {
                    var10 = var18.shape;
                    int var11 = var18.rotation;
                    int var12 = var18.rgb;
                    int var13 = var18.rgba;
                    int[] var14 = this.br[var10];
                    int[] var15 = this.ba[var11];
                    int var16 = 0;
                    int var17;
                    if (var12 != 0) {
                        for (var17 = 0; var17 < 4; ++var17) {
                            var1[var2] = var14[var15[var16++]] == 0 ? var12 : var13;
                            var1[var2 + 1] = var14[var15[var16++]] == 0 ? var12 : var13;
                            var1[var2 + 2] = var14[var15[var16++]] == 0 ? var12 : var13;
                            var1[var2 + 3] = var14[var15[var16++]] == 0 ? var12 : var13;
                            var2 += var3;
                        }
                    } else {
                        for (var17 = 0; var17 < 4; ++var17) {
                            if (var14[var15[var16++]] != 0) {
                                var1[var2] = var13;
                            }

                            if (var14[var15[var16++]] != 0) {
                                var1[var2 + 1] = var13;
                            }

                            if (var14[var15[var16++]] != 0) {
                                var1[var2 + 2] = var13;
                            }

                            if (var14[var15[var16++]] != 0) {
                                var1[var2 + 3] = var13;
                            }

                            var2 += var3;
                        }
                    }

                }
            }
        }
    }

    @ObfuscatedName("aw")
    public void aw(int var1, int var2, int var3, boolean var4) {
        if (!aa() || var4) {
            am = true;
            ar = var4;
            az = var1;
            ap = var2;
            ah = var3;
            au = -1;
            ax = -1;
        }
    }

    @ObfuscatedName("aq")
    public void aq() {
        ar = true;
    }

    @ObfuscatedName("ak")
    public void ak(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (var1 < 0) {
            var1 = 0;
        } else if (var1 >= this.i * 128) {
            var1 = this.i * 128 - 1;
        }

        if (var3 < 0) {
            var3 = 0;
        } else if (var3 >= this.a * 128) {
            var3 = this.a * 128 - 1;
        }

        if (var4 < 128) {
            var4 = 128;
        } else if (var4 > 383) {
            var4 = 383;
        }

        ++k;
        ay = eu.m[var4];
        ao = eu.ay[var4];
        av = eu.m[var5];
        aj = eu.ay[var5];
        be = bk[(var4 - 128) / 32][var5 / 64];
        y = var1;
        h = var2;
        m = var3;
        f = var1 / 128;
        r = var3 / 128;
        j = var6;
        z = f - 25;
        if (z < 0) {
            z = 0;
        }

        s = r - 25;
        if (s < 0) {
            s = 0;
        }

        w = f + 25;
        if (w > this.i) {
            w = this.i;
        }

        d = r + 25;
        if (d > this.a) {
            d = this.a;
        }

        this.ba();
        u = 0;

        int var7;
        Tile[][] var8;
        int var9;
        int var10;
        for (var7 = this.e; var7 < this.q; ++var7) {
            var8 = this.tiles[var7];

            for (var9 = z; var9 < w; ++var9) {
                for (var10 = s; var10 < d; ++var10) {
                    Tile var11 = var8[var9][var10];
                    if (var11 != null) {
                        if (var11.u <= var6
                                && (be[var9 - f + 25][var10 - r + 25] || this.l[var7][var9][var10] - var2 >= 2000)) {
                            var11.j = true;
                            var11.k = true;
                            if (var11.n > 0) {
                                var11.z = true;
                            } else {
                                var11.z = false;
                            }

                            ++u;
                        } else {
                            var11.j = false;
                            var11.k = false;
                            var11.w = 0;
                        }
                    }
                }
            }
        }

        int var12;
        int var13;
        int var14;
        Tile var15;
        int var16;
        for (var7 = this.e; var7 < this.q; ++var7) {
            var8 = this.tiles[var7];

            for (var9 = -25; var9 <= 0; ++var9) {
                var10 = var9 + f;
                var16 = f - var9;
                if (var10 >= z || var16 < w) {
                    for (var12 = -25; var12 <= 0; ++var12) {
                        var13 = var12 + r;
                        var14 = r - var12;
                        if (var10 >= z) {
                            if (var13 >= s) {
                                var15 = var8[var10][var13];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, true);
                                }
                            }

                            if (var14 < d) {
                                var15 = var8[var10][var14];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, true);
                                }
                            }
                        }

                        if (var16 < w) {
                            if (var13 >= s) {
                                var15 = var8[var16][var13];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, true);
                                }
                            }

                            if (var14 < d) {
                                var15 = var8[var16][var14];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, true);
                                }
                            }
                        }

                        if (u == 0) {
                            am = false;
                            return;
                        }
                    }
                }
            }
        }

        for (var7 = this.e; var7 < this.q; ++var7) {
            var8 = this.tiles[var7];

            for (var9 = -25; var9 <= 0; ++var9) {
                var10 = var9 + f;
                var16 = f - var9;
                if (var10 >= z || var16 < w) {
                    for (var12 = -25; var12 <= 0; ++var12) {
                        var13 = var12 + r;
                        var14 = r - var12;
                        if (var10 >= z) {
                            if (var13 >= s) {
                                var15 = var8[var10][var13];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, false);
                                }
                            }

                            if (var14 < d) {
                                var15 = var8[var10][var14];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, false);
                                }
                            }
                        }

                        if (var16 < w) {
                            if (var13 >= s) {
                                var15 = var8[var16][var13];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, false);
                                }
                            }

                            if (var14 < d) {
                                var15 = var8[var16][var14];
                                if (var15 != null && var15.j) {
                                    this.ab(var15, false);
                                }
                            }
                        }

                        if (u == 0) {
                            am = false;
                            return;
                        }
                    }
                }
            }
        }

        am = false;
    }

    @ObfuscatedName("ab")
    void ab(Tile var1, boolean var2) {
        aq.q(var1);

        while (true) {
            Tile var3;
            int var4;
            int var5;
            int var6;
            int var7;
            Tile[][] var8;
            Tile var9;
            int var11;
            int var14;
            int var15;
            int var16;
            int var24;
            int var25;
            do {
                do {
                    do {
                        do {
                            do {
                                do {
                                    while (true) {
                                        BoundaryObject var10;
                                        EventObject var12;
                                        int var17;
                                        int var18;
                                        boolean var20;
                                        int var21;
                                        Tile var36;
                                        while (true) {
                                            do {
                                                var3 = (Tile) aq.l();
                                                if (var3 == null) {
                                                    return;
                                                }
                                            } while (!var3.k);

                                            var4 = var3.regionX;
                                            var5 = var3.regionY;
                                            var6 = var3.plane;
                                            var7 = var3.a;
                                            var8 = this.tiles[var6];
                                            if (!var3.j) {
                                                break;
                                            }

                                            if (var2) {
                                                if (var6 > 0) {
                                                    var9 = this.tiles[var6 - 1][var4][var5];
                                                    if (var9 != null && var9.k) {
                                                        continue;
                                                    }
                                                }

                                                if (var4 <= f && var4 > z) {
                                                    var9 = var8[var4 - 1][var5];
                                                    if (var9 != null && var9.k && (var9.j || (var3.v & 1) == 0)) {
                                                        continue;
                                                    }
                                                }

                                                if (var4 >= f && var4 < w - 1) {
                                                    var9 = var8[var4 + 1][var5];
                                                    if (var9 != null && var9.k && (var9.j || (var3.v & 4) == 0)) {
                                                        continue;
                                                    }
                                                }

                                                if (var5 <= r && var5 > s) {
                                                    var9 = var8[var4][var5 - 1];
                                                    if (var9 != null && var9.k && (var9.j || (var3.v & 8) == 0)) {
                                                        continue;
                                                    }
                                                }

                                                if (var5 >= r && var5 < d - 1) {
                                                    var9 = var8[var4][var5 + 1];
                                                    if (var9 != null && var9.k && (var9.j || (var3.v & 2) == 0)) {
                                                        continue;
                                                    }
                                                }
                                            } else {
                                                var2 = true;
                                            }

                                            var3.j = false;
                                            if (var3.baseTile != null) {
                                                var9 = var3.baseTile;
                                                if (var9.tileModel != null) {
                                                    if (!this.bk(0, var4, var5)) {
                                                        this.ac(var9.tileModel, 0, ay, ao, av, aj, var4, var5);
                                                    }
                                                } else if (var9.tilePaint != null && !this.bk(0, var4, var5)) {
                                                    this.ad(var9.tilePaint, ay, ao, av, aj, var4, var5);
                                                }

                                                var10 = var9.boundaryObject;
                                                if (var10 != null) {
                                                    var10.modelA.cd(0, ay, ao, av, aj, var10.regionX - y, var10.plane
                                                            - h, var10.regionY - m, var10.hash);
                                                }

                                                for (var11 = 0; var11 < var9.n; ++var11) {
                                                    var12 = var9.eventObjects[var11];
                                                    if (var12 != null) {
                                                        var12.model.cd(var12.orientation, ay, ao, av, aj, var12.regionX
                                                                - y, var12.height - h, var12.regionY - m, var12.hash);
                                                    }
                                                }
                                            }

                                            var20 = false;
                                            if (var3.tileModel != null) {
                                                if (!this.bk(var7, var4, var5)) {
                                                    var20 = true;
                                                    if (var3.tileModel.i != 12345678 || am && var6 <= az) {
                                                        this.ac(var3.tileModel, var7, ay, ao, av, aj, var4, var5);
                                                    }
                                                }
                                            } else if (var3.tilePaint != null && !this.bk(var7, var4, var5)) {
                                                var20 = true;
                                                this.ad(var3.tilePaint, ay, ao, av, aj, var4, var5);
                                            }

                                            var21 = 0;
                                            var11 = 0;
                                            BoundaryObject var31 = var3.boundaryObject;
                                            WallObject var13 = var3.wallObject;
                                            if (var31 != null || var13 != null) {
                                                if (var4 == f) {
                                                    ++var21;
                                                } else if (f < var4) {
                                                    var21 += 2;
                                                }

                                                if (var5 == r) {
                                                    var21 += 3;
                                                } else if (r > var5) {
                                                    var21 += 6;
                                                }

                                                var11 = aa[var21];
                                                var3.f = ak[var21];
                                            }

                                            if (var31 != null) {
                                                if ((var31.orientationA & af[var21]) != 0) {
                                                    if (var31.orientationA == 16) {
                                                        var3.w = 3;
                                                        var3.s = ab[var21];
                                                        var3.d = 3 - var3.s;
                                                    } else if (var31.orientationA == 32) {
                                                        var3.w = 6;
                                                        var3.s = ac[var21];
                                                        var3.d = 6 - var3.s;
                                                    } else if (var31.orientationA == 64) {
                                                        var3.w = 12;
                                                        var3.s = ad[var21];
                                                        var3.d = 12 - var3.s;
                                                    } else {
                                                        var3.w = 9;
                                                        var3.s = bg[var21];
                                                        var3.d = 9 - var3.s;
                                                    }
                                                } else {
                                                    var3.w = 0;
                                                }

                                                if ((var31.orientationA & var11) != 0
                                                        && !this.be(var7, var4, var5, var31.orientationA)) {
                                                    var31.modelA.cd(0, ay, ao, av, aj, var31.regionX - y, var31.plane
                                                            - h, var31.regionY - m, var31.hash);
                                                }

                                                if ((var31.orientationB & var11) != 0
                                                        && !this.be(var7, var4, var5, var31.orientationB)) {
                                                    var31.modelB.cd(0, ay, ao, av, aj, var31.regionX - y, var31.plane
                                                            - h, var31.regionY - m, var31.hash);
                                                }
                                            }

                                            if (var13 != null && !this.bc(var7, var4, var5, var13.modelA.modelHeight)) {
                                                if ((var13.orientationA & var11) != 0) {
                                                    var13.modelA.cd(0, ay, ao, av, aj,
                                                            var13.regionX - y + var13.insetX, var13.height - h,
                                                            var13.regionY - m + var13.insetY, var13.hash);
                                                } else if (var13.orientationA == 256) {
                                                    var14 = var13.regionX - y;
                                                    var15 = var13.height - h;
                                                    var16 = var13.regionY - m;
                                                    var17 = var13.orientationB;
                                                    if (var17 != 1 && var17 != 2) {
                                                        var18 = var14;
                                                    } else {
                                                        var18 = -var14;
                                                    }

                                                    int var19;
                                                    if (var17 != 2 && var17 != 3) {
                                                        var19 = var16;
                                                    } else {
                                                        var19 = -var16;
                                                    }

                                                    if (var19 < var18) {
                                                        var13.modelA.cd(0, ay, ao, av, aj, var14 + var13.insetX, var15,
                                                                var16 + var13.insetY, var13.hash);
                                                    } else if (var13.modelB != null) {
                                                        var13.modelB.cd(0, ay, ao, av, aj, var14, var15, var16,
                                                                var13.hash);
                                                    }
                                                }
                                            }

                                            if (var20) {
                                                FloorObject var22 = var3.floorObject;
                                                if (var22 != null) {
                                                    var22.model.cd(0, ay, ao, av, aj, var22.regionX - y, var22.height
                                                            - h, var22.regionY - m, var22.hash);
                                                }

                                                ItemPile var23 = var3.itemPile;
                                                if (var23 != null && var23.objectElevation == 0) {
                                                    if (var23.middle != null) {
                                                        var23.middle.cd(0, ay, ao, av, aj, var23.regionX - y,
                                                                var23.groundElevation - h, var23.regionY - m,
                                                                var23.hash);
                                                    }

                                                    if (var23.top != null) {
                                                        var23.top.cd(0, ay, ao, av, aj, var23.regionX - y,
                                                                var23.groundElevation - h, var23.regionY - m,
                                                                var23.hash);
                                                    }

                                                    if (var23.bottom != null) {
                                                        var23.bottom.cd(0, ay, ao, av, aj, var23.regionX - y,
                                                                var23.groundElevation - h, var23.regionY - m,
                                                                var23.hash);
                                                    }
                                                }
                                            }

                                            var14 = var3.v;
                                            if (var14 != 0) {
                                                if (var4 < f && (var14 & 4) != 0) {
                                                    var36 = var8[var4 + 1][var5];
                                                    if (var36 != null && var36.k) {
                                                        aq.q(var36);
                                                    }
                                                }

                                                if (var5 < r && (var14 & 2) != 0) {
                                                    var36 = var8[var4][var5 + 1];
                                                    if (var36 != null && var36.k) {
                                                        aq.q(var36);
                                                    }
                                                }

                                                if (var4 > f && (var14 & 1) != 0) {
                                                    var36 = var8[var4 - 1][var5];
                                                    if (var36 != null && var36.k) {
                                                        aq.q(var36);
                                                    }
                                                }

                                                if (var5 > r && (var14 & 8) != 0) {
                                                    var36 = var8[var4][var5 - 1];
                                                    if (var36 != null && var36.k) {
                                                        aq.q(var36);
                                                    }
                                                }
                                            }
                                            break;
                                        }

                                        if (var3.w != 0) {
                                            var20 = true;

                                            for (var21 = 0; var21 < var3.n; ++var21) {
                                                if (var3.eventObjects[var21].o != k
                                                        && (var3.boundaryRenderBits[var21] & var3.w) == var3.s) {
                                                    var20 = false;
                                                    break;
                                                }
                                            }

                                            if (var20) {
                                                var10 = var3.boundaryObject;
                                                if (!this.be(var7, var4, var5, var10.orientationA)) {
                                                    var10.modelA.cd(0, ay, ao, av, aj, var10.regionX - y, var10.plane
                                                            - h, var10.regionY - m, var10.hash);
                                                }

                                                var3.w = 0;
                                            }
                                        }

                                        if (!var3.z) {
                                            break;
                                        }

                                        try {
                                            int var34 = var3.n;
                                            var3.z = false;
                                            var21 = 0;

                                            label563: for (var11 = 0; var11 < var34; ++var11) {
                                                var12 = var3.eventObjects[var11];
                                                if (var12.o != k) {
                                                    for (var24 = var12.x; var24 <= var12.sizeX; ++var24) {
                                                        for (var14 = var12.y; var14 <= var12.sizeY; ++var14) {
                                                            var36 = var8[var24][var14];
                                                            if (var36.j) {
                                                                var3.z = true;
                                                                continue label563;
                                                            }

                                                            if (var36.w != 0) {
                                                                var16 = 0;
                                                                if (var24 > var12.x) {
                                                                    ++var16;
                                                                }

                                                                if (var24 < var12.sizeX) {
                                                                    var16 += 4;
                                                                }

                                                                if (var14 > var12.y) {
                                                                    var16 += 8;
                                                                }

                                                                if (var14 < var12.sizeY) {
                                                                    var16 += 2;
                                                                }

                                                                if ((var16 & var36.w) == var3.d) {
                                                                    var3.z = true;
                                                                    continue label563;
                                                                }
                                                            }
                                                        }
                                                    }

                                                    ae[var21++] = var12;
                                                    var24 = f - var12.x;
                                                    var14 = var12.sizeX - f;
                                                    if (var14 > var24) {
                                                        var24 = var14;
                                                    }

                                                    var15 = r - var12.y;
                                                    var16 = var12.sizeY - r;
                                                    if (var16 > var15) {
                                                        var12.n = var24 + var16;
                                                    } else {
                                                        var12.n = var24 + var15;
                                                    }
                                                }
                                            }

                                            while (var21 > 0) {
                                                var11 = -50;
                                                var25 = -1;

                                                for (var24 = 0; var24 < var21; ++var24) {
                                                    EventObject var35 = ae[var24];
                                                    if (var35.o != k) {
                                                        if (var35.n > var11) {
                                                            var11 = var35.n;
                                                            var25 = var24;
                                                        } else if (var11 == var35.n) {
                                                            var15 = var35.regionX - y;
                                                            var16 = var35.regionY - m;
                                                            var17 = ae[var25].regionX - y;
                                                            var18 = ae[var25].regionY - m;
                                                            if (var15 * var15 + var16 * var16 > var17 * var17 + var18
                                                                    * var18) {
                                                                var25 = var24;
                                                            }
                                                        }
                                                    }
                                                }

                                                if (var25 == -1) {
                                                    break;
                                                }

                                                EventObject var33 = ae[var25];
                                                var33.o = k;
                                                if (!this.bm(var7, var33.x, var33.sizeX, var33.y, var33.sizeY,
                                                        var33.model.modelHeight)) {
                                                    var33.model.cd(var33.orientation, ay, ao, av, aj,
                                                            var33.regionX - y, var33.height - h, var33.regionY - m,
                                                            var33.hash);
                                                }

                                                for (var14 = var33.x; var14 <= var33.sizeX; ++var14) {
                                                    for (var15 = var33.y; var15 <= var33.sizeY; ++var15) {
                                                        Tile var26 = var8[var14][var15];
                                                        if (var26.w != 0) {
                                                            aq.q(var26);
                                                        } else if ((var14 != var4 || var15 != var5) && var26.k) {
                                                            aq.q(var26);
                                                        }
                                                    }
                                                }
                                            }

                                            if (!var3.z) {
                                                break;
                                            }
                                        } catch (Exception var28) {
                                            var3.z = false;
                                            break;
                                        }
                                    }
                                } while (!var3.k);
                            } while (var3.w != 0);

                            if (var4 > f || var4 <= z) {
                                break;
                            }

                            var9 = var8[var4 - 1][var5];
                        } while (var9 != null && var9.k);

                        if (var4 < f || var4 >= w - 1) {
                            break;
                        }

                        var9 = var8[var4 + 1][var5];
                    } while (var9 != null && var9.k);

                    if (var5 > r || var5 <= s) {
                        break;
                    }

                    var9 = var8[var4][var5 - 1];
                } while (var9 != null && var9.k);

                if (var5 < r || var5 >= d - 1) {
                    break;
                }

                var9 = var8[var4][var5 + 1];
            } while (var9 != null && var9.k);

            var3.k = false;
            --u;
            ItemPile var32 = var3.itemPile;
            if (var32 != null && var32.objectElevation != 0) {
                if (var32.middle != null) {
                    var32.middle.cd(0, ay, ao, av, aj, var32.regionX - y, var32.groundElevation - h
                            - var32.objectElevation, var32.regionY - m, var32.hash);
                }

                if (var32.top != null) {
                    var32.top.cd(0, ay, ao, av, aj, var32.regionX - y, var32.groundElevation - h
                            - var32.objectElevation, var32.regionY - m, var32.hash);
                }

                if (var32.bottom != null) {
                    var32.bottom.cd(0, ay, ao, av, aj, var32.regionX - y, var32.groundElevation - h
                            - var32.objectElevation, var32.regionY - m, var32.hash);
                }
            }

            if (var3.f != 0) {
                WallObject var29 = var3.wallObject;
                if (var29 != null && !this.bc(var7, var4, var5, var29.modelA.modelHeight)) {
                    if ((var29.orientationA & var3.f) != 0) {
                        var29.modelA.cd(0, ay, ao, av, aj, var29.regionX - y + var29.insetX, var29.height - h,
                                var29.regionY - m + var29.insetY, var29.hash);
                    } else if (var29.orientationA == 256) {
                        var11 = var29.regionX - y;
                        var25 = var29.height - h;
                        var24 = var29.regionY - m;
                        var14 = var29.orientationB;
                        if (var14 != 1 && var14 != 2) {
                            var15 = var11;
                        } else {
                            var15 = -var11;
                        }

                        if (var14 != 2 && var14 != 3) {
                            var16 = var24;
                        } else {
                            var16 = -var24;
                        }

                        if (var16 >= var15) {
                            var29.modelA.cd(0, ay, ao, av, aj, var11 + var29.insetX, var25, var24 + var29.insetY,
                                    var29.hash);
                        } else if (var29.modelB != null) {
                            var29.modelB.cd(0, ay, ao, av, aj, var11, var25, var24, var29.hash);
                        }
                    }
                }

                BoundaryObject var27 = var3.boundaryObject;
                if (var27 != null) {
                    if ((var27.orientationB & var3.f) != 0 && !this.be(var7, var4, var5, var27.orientationB)) {
                        var27.modelB.cd(0, ay, ao, av, aj, var27.regionX - y, var27.plane - h, var27.regionY - m,
                                var27.hash);
                    }

                    if ((var27.orientationA & var3.f) != 0 && !this.be(var7, var4, var5, var27.orientationA)) {
                        var27.modelA.cd(0, ay, ao, av, aj, var27.regionX - y, var27.plane - h, var27.regionY - m,
                                var27.hash);
                    }
                }
            }

            Tile var30;
            if (var6 < this.q - 1) {
                var30 = this.tiles[var6 + 1][var4][var5];
                if (var30 != null && var30.k) {
                    aq.q(var30);
                }
            }

            if (var4 < f) {
                var30 = var8[var4 + 1][var5];
                if (var30 != null && var30.k) {
                    aq.q(var30);
                }
            }

            if (var5 < r) {
                var30 = var8[var4][var5 + 1];
                if (var30 != null && var30.k) {
                    aq.q(var30);
                }
            }

            if (var4 > f) {
                var30 = var8[var4 - 1][var5];
                if (var30 != null && var30.k) {
                    aq.q(var30);
                }
            }

            if (var5 > r) {
                var30 = var8[var4][var5 - 1];
                if (var30 != null && var30.k) {
                    aq.q(var30);
                }
            }
        }
    }

    @ObfuscatedName("ac")
    void ac(TileModel var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        int var9;
        int var10 = var9 = (var7 << 7) - y;
        int var11;
        int var12 = var11 = (var8 << 7) - m;
        int var13;
        int var14 = var13 = var10 + 128;
        int var15;
        int var16 = var15 = var12 + 128;
        int var17 = this.l[var2][var7][var8] - h;
        int var18 = this.l[var2][var7 + 1][var8] - h;
        int var19 = this.l[var2][var7 + 1][var8 + 1] - h;
        int var20 = this.l[var2][var7][var8 + 1] - h;
        int var21 = var10 * var6 + var5 * var12 >> 16;
        var12 = var12 * var6 - var5 * var10 >> 16;
        var10 = var21;
        var21 = var17 * var4 - var3 * var12 >> 16;
        var12 = var3 * var17 + var12 * var4 >> 16;
        var17 = var21;
        if (var12 >= 50) {
            var21 = var14 * var6 + var5 * var11 >> 16;
            var11 = var11 * var6 - var5 * var14 >> 16;
            var14 = var21;
            var21 = var18 * var4 - var3 * var11 >> 16;
            var11 = var3 * var18 + var11 * var4 >> 16;
            var18 = var21;
            if (var11 >= 50) {
                var21 = var13 * var6 + var5 * var16 >> 16;
                var16 = var16 * var6 - var5 * var13 >> 16;
                var13 = var21;
                var21 = var19 * var4 - var3 * var16 >> 16;
                var16 = var3 * var19 + var16 * var4 >> 16;
                var19 = var21;
                if (var16 >= 50) {
                    var21 = var9 * var6 + var5 * var15 >> 16;
                    var15 = var15 * var6 - var5 * var9 >> 16;
                    var9 = var21;
                    var21 = var20 * var4 - var3 * var15 >> 16;
                    var15 = var3 * var20 + var15 * var4 >> 16;
                    if (var15 >= 50) {
                        int var22 = var10 * eu.o / var12 + eu.c;
                        int var23 = var17 * eu.o / var12 + eu.v;
                        int var24 = var14 * eu.o / var11 + eu.c;
                        int var25 = var18 * eu.o / var11 + eu.v;
                        int var26 = var13 * eu.o / var16 + eu.c;
                        int var27 = var19 * eu.o / var16 + eu.v;
                        int var28 = var9 * eu.o / var15 + eu.c;
                        int var29 = var21 * eu.o / var15 + eu.v;
                        eu.l = 0;
                        int var30;
                        if ((var26 - var28) * (var25 - var29) - (var27 - var29) * (var24 - var28) > 0) {
                            eu.t = false;
                            if (var26 < 0 || var28 < 0 || var24 < 0 || var26 > eu.u || var28 > eu.u || var24 > eu.u) {
                                eu.t = true;
                            }

                            if (am && br(ap, ah, var27, var29, var25, var26, var28, var24)) {
                                au = var7;
                                ax = var8;
                            }

                            if (var1.texture == -1) {
                                if (var1.i != 12345678) {
                                    eu.o(var27, var29, var25, var26, var28, var24, var1.i, var1.a, var1.q);
                                }
                            } else if (!t) {
                                if (var1.flat) {
                                    eu.s(var27, var29, var25, var26, var28, var24, var1.i, var1.a, var1.q, var10,
                                            var14, var9, var17, var18, var21, var12, var11, var15, var1.texture);
                                } else {
                                    eu.s(var27, var29, var25, var26, var28, var24, var1.i, var1.a, var1.q, var13, var9,
                                            var14, var19, var21, var18, var16, var15, var11, var1.texture);
                                }
                            } else {
                                var30 = eu.r.a(var1.texture);
                                eu.o(var27, var29, var25, var26, var28, var24, bg(var30, var1.i), bg(var30, var1.a),
                                        bg(var30, var1.q));
                            }
                        }

                        if ((var22 - var24) * (var29 - var25) - (var23 - var25) * (var28 - var24) > 0) {
                            eu.t = false;
                            if (var22 < 0 || var24 < 0 || var28 < 0 || var22 > eu.u || var24 > eu.u || var28 > eu.u) {
                                eu.t = true;
                            }

                            if (am && br(ap, ah, var23, var25, var29, var22, var24, var28)) {
                                au = var7;
                                ax = var8;
                            }

                            if (var1.texture == -1) {
                                if (var1.t != 12345678) {
                                    eu.o(var23, var25, var29, var22, var24, var28, var1.t, var1.q, var1.a);
                                }
                            } else if (!t) {
                                eu.s(var23, var25, var29, var22, var24, var28, var1.t, var1.q, var1.a, var10, var14,
                                        var9, var17, var18, var21, var12, var11, var15, var1.texture);
                            } else {
                                var30 = eu.r.a(var1.texture);
                                eu.o(var23, var25, var29, var22, var24, var28, bg(var30, var1.t), bg(var30, var1.q),
                                        bg(var30, var1.a));
                            }
                        }

                    }
                }
            }
        }
    }

    @ObfuscatedName("ad")
    void ad(TilePaint var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int var8 = var1.xVertices.length;

        int var9;
        int var10;
        int var11;
        int var12;
        int var13;
        for (var9 = 0; var9 < var8; ++var9) {
            var10 = var1.xVertices[var9] - y;
            var11 = var1.yVertices[var9] - h;
            var12 = var1.zVertices[var9] - m;
            var13 = var12 * var4 + var5 * var10 >> 16;
            var12 = var5 * var12 - var10 * var4 >> 16;
            var10 = var13;
            var13 = var3 * var11 - var12 * var2 >> 16;
            var12 = var11 * var2 + var3 * var12 >> 16;
            if (var12 < 50) {
                return;
            }

            if (var1.g != null) {
                TilePaint.z[var9] = var10;
                TilePaint.w[var9] = var13;
                TilePaint.s[var9] = var12;
            }

            TilePaint.j[var9] = var10 * eu.o / var12 + eu.c;
            TilePaint.k[var9] = var13 * eu.o / var12 + eu.v;
        }

        eu.l = 0;
        var8 = var1.aIndices.length;

        for (var9 = 0; var9 < var8; ++var9) {
            var10 = var1.aIndices[var9];
            var11 = var1.bIndices[var9];
            var12 = var1.cIndices[var9];
            var13 = TilePaint.j[var10];
            int var14 = TilePaint.j[var11];
            int var15 = TilePaint.j[var12];
            int var16 = TilePaint.k[var10];
            int var17 = TilePaint.k[var11];
            int var18 = TilePaint.k[var12];
            if ((var13 - var14) * (var18 - var17) - (var16 - var17) * (var15 - var14) > 0) {
                eu.t = false;
                if (var13 < 0 || var14 < 0 || var15 < 0 || var13 > eu.u || var14 > eu.u || var15 > eu.u) {
                    eu.t = true;
                }

                if (am && br(ap, ah, var16, var17, var18, var13, var14, var15)) {
                    au = var6;
                    ax = var7;
                }

                if (var1.g != null && var1.g[var9] != -1) {
                    if (!t) {
                        if (var1.n) {
                            eu.s(var16, var17, var18, var13, var14, var15, var1.texturedAIndices[var9],
                                    var1.texturedBIndices[var9], var1.texturedCIndices[var9], TilePaint.z[0],
                                    TilePaint.z[1], TilePaint.z[3], TilePaint.w[0], TilePaint.w[1], TilePaint.w[3],
                                    TilePaint.s[0], TilePaint.s[1], TilePaint.s[3], var1.g[var9]);
                        } else {
                            eu.s(var16, var17, var18, var13, var14, var15, var1.texturedAIndices[var9],
                                    var1.texturedBIndices[var9], var1.texturedCIndices[var9], TilePaint.z[var10],
                                    TilePaint.z[var11], TilePaint.z[var12], TilePaint.w[var10], TilePaint.w[var11],
                                    TilePaint.w[var12], TilePaint.s[var10], TilePaint.s[var11], TilePaint.s[var12],
                                    var1.g[var9]);
                        }
                    } else {
                        int var19 = eu.r.a(var1.g[var9]);
                        eu.o(var16, var17, var18, var13, var14, var15, bg(var19, var1.texturedAIndices[var9]),
                                bg(var19, var1.texturedBIndices[var9]), bg(var19, var1.texturedCIndices[var9]));
                    }
                } else if (var1.texturedAIndices[var9] != 12345678) {
                    eu.o(var16, var17, var18, var13, var14, var15, var1.texturedAIndices[var9],
                            var1.texturedBIndices[var9], var1.texturedCIndices[var9]);
                }
            }
        }

    }

    @ObfuscatedName("ba")
    void ba() {
        int var1 = at[j];
        es[] var2 = ag[j];
        as = 0;

        for (int var3 = 0; var3 < var1; ++var3) {
            es var4 = var2[var3];
            int var5;
            int var6;
            int var7;
            int var9;
            boolean var13;
            if (var4.l == 1) {
                var5 = var4.t - f + 25;
                if (var5 >= 0 && var5 <= 50) {
                    var6 = var4.i - r + 25;
                    if (var6 < 0) {
                        var6 = 0;
                    }

                    var7 = var4.a - r + 25;
                    if (var7 > 50) {
                        var7 = 50;
                    }

                    var13 = false;

                    while (var6 <= var7) {
                        if (be[var5][var6++]) {
                            var13 = true;
                            break;
                        }
                    }

                    if (var13) {
                        var9 = y - var4.b;
                        if (var9 > 32) {
                            var4.o = 1;
                        } else {
                            if (var9 >= -32) {
                                continue;
                            }

                            var4.o = 2;
                            var9 = -var9;
                        }

                        var4.u = (var4.x - m << 8) / var9;
                        var4.j = (var4.p - m << 8) / var9;
                        var4.k = (var4.g - h << 8) / var9;
                        var4.z = (var4.n - h << 8) / var9;
                        aw[as++] = var4;
                    }
                }
            } else if (var4.l == 2) {
                var5 = var4.i - r + 25;
                if (var5 >= 0 && var5 <= 50) {
                    var6 = var4.t - f + 25;
                    if (var6 < 0) {
                        var6 = 0;
                    }

                    var7 = var4.q - f + 25;
                    if (var7 > 50) {
                        var7 = 50;
                    }

                    var13 = false;

                    while (var6 <= var7) {
                        if (be[var6++][var5]) {
                            var13 = true;
                            break;
                        }
                    }

                    if (var13) {
                        var9 = m - var4.x;
                        if (var9 > 32) {
                            var4.o = 3;
                        } else {
                            if (var9 >= -32) {
                                continue;
                            }

                            var4.o = 4;
                            var9 = -var9;
                        }

                        var4.c = (var4.b - y << 8) / var9;
                        var4.v = (var4.e - y << 8) / var9;
                        var4.k = (var4.g - h << 8) / var9;
                        var4.z = (var4.n - h << 8) / var9;
                        aw[as++] = var4;
                    }
                }
            } else if (var4.l == 4) {
                var5 = var4.g - h;
                if (var5 > 128) {
                    var6 = var4.i - r + 25;
                    if (var6 < 0) {
                        var6 = 0;
                    }

                    var7 = var4.a - r + 25;
                    if (var7 > 50) {
                        var7 = 50;
                    }

                    if (var6 <= var7) {
                        int var8 = var4.t - f + 25;
                        if (var8 < 0) {
                            var8 = 0;
                        }

                        var9 = var4.q - f + 25;
                        if (var9 > 50) {
                            var9 = 50;
                        }

                        boolean var10 = false;

                        label144: for (int var11 = var8; var11 <= var9; ++var11) {
                            for (int var12 = var6; var12 <= var7; ++var12) {
                                if (be[var11][var12]) {
                                    var10 = true;
                                    break label144;
                                }
                            }
                        }

                        if (var10) {
                            var4.o = 5;
                            var4.c = (var4.b - y << 8) / var5;
                            var4.v = (var4.e - y << 8) / var5;
                            var4.u = (var4.x - m << 8) / var5;
                            var4.j = (var4.p - m << 8) / var5;
                            aw[as++] = var4;
                        }
                    }
                }
            }
        }

    }

    @ObfuscatedName("bk")
    boolean bk(int var1, int var2, int var3) {
        int var4 = this.g[var1][var2][var3];
        if (var4 == -k) {
            return false;
        } else if (var4 == k) {
            return true;
        } else {
            int var5 = var2 << 7;
            int var6 = var3 << 7;
            if (this.bh(var5 + 1, this.l[var1][var2][var3], var6 + 1)
                    && this.bh(var5 + 128 - 1, this.l[var1][var2 + 1][var3], var6 + 1)
                    && this.bh(var5 + 128 - 1, this.l[var1][var2 + 1][var3 + 1], var6 + 128 - 1)
                    && this.bh(var5 + 1, this.l[var1][var2][var3 + 1], var6 + 128 - 1)) {
                this.g[var1][var2][var3] = k;
                return true;
            } else {
                this.g[var1][var2][var3] = -k;
                return false;
            }
        }
    }

    @ObfuscatedName("be")
    boolean be(int var1, int var2, int var3, int var4) {
        if (!this.bk(var1, var2, var3)) {
            return false;
        } else {
            int var5 = var2 << 7;
            int var6 = var3 << 7;
            int var7 = this.l[var1][var2][var3] - 1;
            int var8 = var7 - 120;
            int var9 = var7 - 230;
            int var10 = var7 - 238;
            if (var4 < 16) {
                if (var4 == 1) {
                    if (var5 > y) {
                        if (!this.bh(var5, var7, var6)) {
                            return false;
                        }

                        if (!this.bh(var5, var7, var6 + 128)) {
                            return false;
                        }
                    }

                    if (var1 > 0) {
                        if (!this.bh(var5, var8, var6)) {
                            return false;
                        }

                        if (!this.bh(var5, var8, var6 + 128)) {
                            return false;
                        }
                    }

                    if (!this.bh(var5, var9, var6)) {
                        return false;
                    }

                    if (!this.bh(var5, var9, var6 + 128)) {
                        return false;
                    }

                    return true;
                }

                if (var4 == 2) {
                    if (var6 < m) {
                        if (!this.bh(var5, var7, var6 + 128)) {
                            return false;
                        }

                        if (!this.bh(var5 + 128, var7, var6 + 128)) {
                            return false;
                        }
                    }

                    if (var1 > 0) {
                        if (!this.bh(var5, var8, var6 + 128)) {
                            return false;
                        }

                        if (!this.bh(var5 + 128, var8, var6 + 128)) {
                            return false;
                        }
                    }

                    if (!this.bh(var5, var9, var6 + 128)) {
                        return false;
                    }

                    if (!this.bh(var5 + 128, var9, var6 + 128)) {
                        return false;
                    }

                    return true;
                }

                if (var4 == 4) {
                    if (var5 < y) {
                        if (!this.bh(var5 + 128, var7, var6)) {
                            return false;
                        }

                        if (!this.bh(var5 + 128, var7, var6 + 128)) {
                            return false;
                        }
                    }

                    if (var1 > 0) {
                        if (!this.bh(var5 + 128, var8, var6)) {
                            return false;
                        }

                        if (!this.bh(var5 + 128, var8, var6 + 128)) {
                            return false;
                        }
                    }

                    if (!this.bh(var5 + 128, var9, var6)) {
                        return false;
                    }

                    if (!this.bh(var5 + 128, var9, var6 + 128)) {
                        return false;
                    }

                    return true;
                }

                if (var4 == 8) {
                    if (var6 > m) {
                        if (!this.bh(var5, var7, var6)) {
                            return false;
                        }

                        if (!this.bh(var5 + 128, var7, var6)) {
                            return false;
                        }
                    }

                    if (var1 > 0) {
                        if (!this.bh(var5, var8, var6)) {
                            return false;
                        }

                        if (!this.bh(var5 + 128, var8, var6)) {
                            return false;
                        }
                    }

                    if (!this.bh(var5, var9, var6)) {
                        return false;
                    }

                    if (!this.bh(var5 + 128, var9, var6)) {
                        return false;
                    }

                    return true;
                }
            }

            if (!this.bh(var5 + 64, var10, var6 + 64)) {
                return false;
            } else if (var4 == 16) {
                return this.bh(var5, var9, var6 + 128);
            } else if (var4 == 32) {
                return this.bh(var5 + 128, var9, var6 + 128);
            } else if (var4 == 64) {
                return this.bh(var5 + 128, var9, var6);
            } else if (var4 == 128) {
                return this.bh(var5, var9, var6);
            } else {
                return true;
            }
        }
    }

    @ObfuscatedName("bc")
    boolean bc(int var1, int var2, int var3, int var4) {
        if (!this.bk(var1, var2, var3)) {
            return false;
        } else {
            int var5 = var2 << 7;
            int var6 = var3 << 7;
            return this.bh(var5 + 1, this.l[var1][var2][var3] - var4, var6 + 1)
                    && this.bh(var5 + 128 - 1, this.l[var1][var2 + 1][var3] - var4, var6 + 1)
                    && this.bh(var5 + 128 - 1, this.l[var1][var2 + 1][var3 + 1] - var4, var6 + 128 - 1)
                    && this.bh(var5 + 1, this.l[var1][var2][var3 + 1] - var4, var6 + 128 - 1);
        }
    }

    @ObfuscatedName("bm")
    boolean bm(int var1, int var2, int var3, int var4, int var5, int var6) {
        int var7;
        int var8;
        if (var3 == var2 && var5 == var4) {
            if (!this.bk(var1, var2, var4)) {
                return false;
            } else {
                var7 = var2 << 7;
                var8 = var4 << 7;
                return this.bh(var7 + 1, this.l[var1][var2][var4] - var6, var8 + 1)
                        && this.bh(var7 + 128 - 1, this.l[var1][var2 + 1][var4] - var6, var8 + 1)
                        && this.bh(var7 + 128 - 1, this.l[var1][var2 + 1][var4 + 1] - var6, var8 + 128 - 1)
                        && this.bh(var7 + 1, this.l[var1][var2][var4 + 1] - var6, var8 + 128 - 1);
            }
        } else {
            for (var7 = var2; var7 <= var3; ++var7) {
                for (var8 = var4; var8 <= var5; ++var8) {
                    if (this.g[var1][var7][var8] == -k) {
                        return false;
                    }
                }
            }

            var7 = (var2 << 7) + 1;
            var8 = (var4 << 7) + 2;
            int var9 = this.l[var1][var2][var4] - var6;
            if (!this.bh(var7, var9, var8)) {
                return false;
            } else {
                int var10 = (var3 << 7) - 1;
                if (!this.bh(var10, var9, var8)) {
                    return false;
                } else {
                    int var11 = (var5 << 7) - 1;
                    if (!this.bh(var7, var9, var11)) {
                        return false;
                    } else if (!this.bh(var10, var9, var11)) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }
    }

    @ObfuscatedName("bh")
    boolean bh(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < as; ++var4) {
            es var5 = aw[var4];
            int var6;
            int var7;
            int var8;
            int var9;
            int var10;
            if (var5.o == 1) {
                var6 = var5.b - var1;
                if (var6 > 0) {
                    var7 = (var6 * var5.u >> 8) + var5.x;
                    var8 = (var6 * var5.j >> 8) + var5.p;
                    var9 = (var6 * var5.k >> 8) + var5.g;
                    var10 = (var6 * var5.z >> 8) + var5.n;
                    if (var3 >= var7 && var3 <= var8 && var2 >= var9 && var2 <= var10) {
                        return true;
                    }
                }
            } else if (var5.o == 2) {
                var6 = var1 - var5.b;
                if (var6 > 0) {
                    var7 = (var6 * var5.u >> 8) + var5.x;
                    var8 = (var6 * var5.j >> 8) + var5.p;
                    var9 = (var6 * var5.k >> 8) + var5.g;
                    var10 = (var6 * var5.z >> 8) + var5.n;
                    if (var3 >= var7 && var3 <= var8 && var2 >= var9 && var2 <= var10) {
                        return true;
                    }
                }
            } else if (var5.o == 3) {
                var6 = var5.x - var3;
                if (var6 > 0) {
                    var7 = (var6 * var5.c >> 8) + var5.b;
                    var8 = (var6 * var5.v >> 8) + var5.e;
                    var9 = (var6 * var5.k >> 8) + var5.g;
                    var10 = (var6 * var5.z >> 8) + var5.n;
                    if (var1 >= var7 && var1 <= var8 && var2 >= var9 && var2 <= var10) {
                        return true;
                    }
                }
            } else if (var5.o == 4) {
                var6 = var3 - var5.x;
                if (var6 > 0) {
                    var7 = (var6 * var5.c >> 8) + var5.b;
                    var8 = (var6 * var5.v >> 8) + var5.e;
                    var9 = (var6 * var5.k >> 8) + var5.g;
                    var10 = (var6 * var5.z >> 8) + var5.n;
                    if (var1 >= var7 && var1 <= var8 && var2 >= var9 && var2 <= var10) {
                        return true;
                    }
                }
            } else if (var5.o == 5) {
                var6 = var2 - var5.g;
                if (var6 > 0) {
                    var7 = (var6 * var5.c >> 8) + var5.b;
                    var8 = (var6 * var5.v >> 8) + var5.e;
                    var9 = (var6 * var5.u >> 8) + var5.x;
                    var10 = (var6 * var5.j >> 8) + var5.p;
                    if (var1 >= var7 && var1 <= var8 && var3 >= var9 && var3 <= var10) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @ObfuscatedName("a")
    public static void a(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        es var8 = new es();
        var8.t = var2 / 128;
        var8.q = var3 / 128;
        var8.i = var4 / 128;
        var8.a = var5 / 128;
        var8.l = var1;
        var8.b = var2;
        var8.e = var3;
        var8.x = var4;
        var8.p = var5;
        var8.g = var6;
        var8.n = var7;
        ag[var0][at[var0]++] = var8;
    }

    @ObfuscatedName("ag")
    public static void ag(int[] var0, int var1, int var2, int var3, int var4) {
        bh = 0;
        bs = 0;
        bj = var3;
        bt = var4;
        bc = var3 / 2;
        bm = var4 / 2;
        boolean[][][][] var5 = new boolean[9][32][53][53];

        int var6;
        int var7;
        int var8;
        int var9;
        int var11;
        int var12;
        for (var6 = 128; var6 <= 384; var6 += 32) {
            for (var7 = 0; var7 < 2048; var7 += 64) {
                ay = eu.m[var6];
                ao = eu.ay[var6];
                av = eu.m[var7];
                aj = eu.ay[var7];
                var8 = (var6 - 128) / 32;
                var9 = var7 / 64;

                for (int var10 = -26; var10 <= 26; ++var10) {
                    for (var11 = -26; var11 <= 26; ++var11) {
                        var12 = var10 * 128;
                        int var13 = var11 * 128;
                        boolean var14 = false;

                        for (int var15 = -var1; var15 <= var2; var15 += 128) {
                            if (as(var12, var0[var8] + var15, var13)) {
                                var14 = true;
                                break;
                            }
                        }

                        var5[var8][var9][var10 + 1 + 25][var11 + 1 + 25] = var14;
                    }
                }
            }
        }

        for (var6 = 0; var6 < 8; ++var6) {
            for (var7 = 0; var7 < 32; ++var7) {
                for (var8 = -25; var8 < 25; ++var8) {
                    for (var9 = -25; var9 < 25; ++var9) {
                        boolean var16 = false;

                        label76: for (var11 = -1; var11 <= 1; ++var11) {
                            for (var12 = -1; var12 <= 1; ++var12) {
                                if (var5[var6][var7][var8 + var11 + 1 + 25][var9 + var12 + 1 + 25]) {
                                    var16 = true;
                                    break label76;
                                }

                                if (var5[var6][(var7 + 1) % 31][var8 + var11 + 1 + 25][var9 + var12 + 1 + 25]) {
                                    var16 = true;
                                    break label76;
                                }

                                if (var5[var6 + 1][var7][var8 + var11 + 1 + 25][var9 + var12 + 1 + 25]) {
                                    var16 = true;
                                    break label76;
                                }

                                if (var5[var6 + 1][(var7 + 1) % 31][var8 + var11 + 1 + 25][var9 + var12 + 1 + 25]) {
                                    var16 = true;
                                    break label76;
                                }
                            }
                        }

                        bk[var6][var7][var8 + 25][var9 + 25] = var16;
                    }
                }
            }
        }

    }

    @ObfuscatedName("as")
    static boolean as(int var0, int var1, int var2) {
        int var3 = var0 * aj + var2 * av >> 16;
        int var4 = var2 * aj - var0 * av >> 16;
        int var5 = var4 * ao + ay * var1 >> 16;
        int var6 = ao * var1 - var4 * ay >> 16;
        if (var5 >= 50 && var5 <= 3500) {
            int var7 = var3 * 390 / var5 + bc;
            int var8 = var6 * 390 / var5 + bm;
            return var7 >= bh && var7 <= bj && var8 >= bs && var8 <= bt;
        } else {
            return false;
        }
    }

    @ObfuscatedName("aa")
    public static boolean aa() {
        return ar && au != -1;
    }

    @ObfuscatedName("af")
    public static void af() {
        au = -1;
        ar = false;
    }

    @ObfuscatedName("bg")
    static final int bg(int var0, int var1) {
        var1 = (var0 & 127) * var1 >> 7;
        if (var1 < 2) {
            var1 = 2;
        } else if (var1 > 126) {
            var1 = 126;
        }

        return (var0 & 'ﾀ') + var1;
    }

    @ObfuscatedName("br")
    static boolean br(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        if (var1 < var2 && var1 < var3 && var1 < var4) {
            return false;
        } else if (var1 > var2 && var1 > var3 && var1 > var4) {
            return false;
        } else if (var0 < var5 && var0 < var6 && var0 < var7) {
            return false;
        } else if (var0 > var5 && var0 > var6 && var0 > var7) {
            return false;
        } else {
            int var8 = (var1 - var2) * (var6 - var5) - (var0 - var5) * (var3 - var2);
            int var9 = (var7 - var6) * (var1 - var3) - (var0 - var6) * (var4 - var3);
            int var10 = (var5 - var7) * (var1 - var4) - (var2 - var4) * (var0 - var7);
            if (var8 == 0) {
                if (var9 != 0) {
                    return var9 < 0 ? var10 <= 0 : var10 >= 0;
                } else {
                    return true;
                }
            } else {
                return var8 < 0 ? var9 <= 0 && var10 <= 0 : var9 >= 0 && var10 >= 0;
            }
        }
    }

    @ObfuscatedName("bs")
    public static final int[] bs(int var0, int var1, int var2) {
        int var3 = var0 * aj + var2 * av >> 16;
        var2 = var2 * aj - var0 * av >> 16;
        var0 = var3;
        var3 = ao * var1 - var2 * ay >> 16;
        var2 = ay * var1 + var2 * ao >> 16;
        var2 |= 1;
        int var4 = var0 * eu.o / var2 + eu.c + li.az;
        int var5 = eu.o * var3 / var2 + eu.v + li.ae;
        return new int[] { var4, var5 };
    }
}
