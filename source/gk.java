import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gk")
public class gk {

    @ObfuscatedName("a")
    static String[] a;

    @ObfuscatedName("t")
    static boolean t(int var0, int var1) {
        return var0 != 4 || var1 < 8;
    }
}
