import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("by")
public class RuneScriptStackItem {

    @ObfuscatedName("oi")
    static boolean oi;

    @ObfuscatedName("t")
    RuneScript script;

    @ObfuscatedName("q")
    int q = -1;

    @ObfuscatedName("i")
    int[] i;

    @ObfuscatedName("a")
    String[] a;

    @ObfuscatedName("i")
    static char i(char var0) {
        if (var0 == 198) {
            return 'E';
        } else if (var0 == 230) {
            return 'e';
        } else if (var0 == 223) {
            return 's';
        } else if (var0 == 338) {
            return 'E';
        } else {
            return (char) (var0 == 339 ? 'e' : '\u0000');
        }
    }

    @ObfuscatedName("l")
    public static void l() {
        hi.audioTask.u();
        hi.audioTrackId = 1;
        hi.b = null;
    }

    @ObfuscatedName("b")
    static final void b(int var0, int var1, int var2, int var3, int var4, int var5, Region var6, CollisionMap var7) {
        if (!Client.bh || (bt.landscapeData[0][var1][var2] & 2) != 0 || (bt.landscapeData[var0][var1][var2] & 16) == 0) {
            if (var0 < bt.i) {
                bt.i = var0;
            }

            ObjectDefinition var8 = jw.q(var3);
            int var9;
            int var10;
            if (var4 != 1 && var4 != 3) {
                var9 = var8.width;
                var10 = var8.height;
            } else {
                var9 = var8.height;
                var10 = var8.width;
            }

            int var11;
            int var12;
            if (var9 + var1 <= 104) {
                var11 = (var9 >> 1) + var1;
                var12 = (var9 + 1 >> 1) + var1;
            } else {
                var11 = var1;
                var12 = var1 + 1;
            }

            int var13;
            int var14;
            if (var10 + var2 <= 104) {
                var13 = (var10 >> 1) + var2;
                var14 = var2 + (var10 + 1 >> 1);
            } else {
                var13 = var2;
                var14 = var2 + 1;
            }

            int[][] var15 = bt.tileHeights[var0];
            int var16 = var15[var12][var14] + var15[var11][var14] + var15[var11][var13] + var15[var12][var13] >> 2;
            int var17 = (var1 << 7) + (var9 << 6);
            int var18 = (var2 << 7) + (var10 << 6);
            int var19 = (var3 << 14) + (var2 << 7) + var1 + 1073741824;
            if (var8.d == 0) {
                var19 -= Integer.MIN_VALUE;
            }

            int var20 = var5 + (var4 << 6);
            if (var8.at == 1) {
                var20 += 256;
            }

            int var22;
            int var23;
            if (var8.w()) {
                AreaSoundEmitter var21 = new AreaSoundEmitter();
                var21.q = var0;
                var21.i = var1 * 128;
                var21.a = var2 * 128;
                var22 = var8.width;
                var23 = var8.height;
                if (var4 == 1 || var4 == 3) {
                    var22 = var8.height;
                    var23 = var8.width;
                }

                var21.l = (var22 + var1) * 128;
                var21.b = (var23 + var2) * 128;
                var21.ambientSoundId = var8.aq;
                var21.e = var8.aa * 128;
                var21.g = var8.af;
                var21.n = var8.ak;
                var21.shufflingSoundIds = var8.ab;
                if (var8.innerObjectIds != null) {
                    var21.emittingObjectsDefinition = var8;
                    var21.t();
                }

                AreaSoundEmitter.t.q(var21);
                if (var21.shufflingSoundIds != null) {
                    var21.c = var21.g + (int) (Math.random() * (double) (var21.n - var21.g));
                }
            }

            Object var30;
            if (var5 == 22) {
                if (!Client.bh || var8.d != 0 || var8.w == 1 || var8.ai) {
                    if (var8.h == -1 && var8.innerObjectIds == null) {
                        var30 = var8.x(22, var4, var15, var17, var16, var18);
                    } else {
                        var30 = new AnimableNode(var3, 22, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                    }

                    var6.e(var0, var1, var2, var16, (Renderable) var30, var19, var20);
                    if (var8.w == 1 && var7 != null) {
                        var7.l(var1, var2);
                    }

                }
            } else if (var5 != 10 && var5 != 11) {
                if (var5 >= 12) {
                    if (var8.h == -1 && var8.innerObjectIds == null) {
                        var30 = var8.x(var5, var4, var15, var17, var16, var18);
                    } else {
                        var30 = new AnimableNode(var3, var5, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                    }

                    var6.c(var0, var1, var2, var16, 1, 1, (Renderable) var30, 0, var19, var20);
                    if (var5 >= 12 && var5 <= 17 && var5 != 13 && var0 > 0) {
                        bt.u[var0][var1][var2] |= 2340;
                    }

                    if (var8.w != 0 && var7 != null) {
                        var7.i(var1, var2, var9, var10, var8.s);
                    }

                } else if (var5 == 0) {
                    if (var8.h == -1 && var8.innerObjectIds == null) {
                        var30 = var8.x(0, var4, var15, var17, var16, var18);
                    } else {
                        var30 = new AnimableNode(var3, 0, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                    }

                    var6.p(var0, var1, var2, var16, (Renderable) var30, (Renderable) null, bt.w[var4], 0, var19, var20);
                    if (var4 == 0) {
                        if (var8.az) {
                            bt.x[var0][var1][var2] = 50;
                            bt.x[var0][var1][var2 + 1] = 50;
                        }

                        if (var8.clipped) {
                            bt.u[var0][var1][var2] |= 585;
                        }
                    } else if (var4 == 1) {
                        if (var8.az) {
                            bt.x[var0][var1][var2 + 1] = 50;
                            bt.x[var0][var1 + 1][var2 + 1] = 50;
                        }

                        if (var8.clipped) {
                            bt.u[var0][var1][1 + var2] |= 1170;
                        }
                    } else if (var4 == 2) {
                        if (var8.az) {
                            bt.x[var0][var1 + 1][var2] = 50;
                            bt.x[var0][var1 + 1][var2 + 1] = 50;
                        }

                        if (var8.clipped) {
                            bt.u[var0][var1 + 1][var2] |= 585;
                        }
                    } else if (var4 == 3) {
                        if (var8.az) {
                            bt.x[var0][var1][var2] = 50;
                            bt.x[var0][var1 + 1][var2] = 50;
                        }

                        if (var8.clipped) {
                            bt.u[var0][var1][var2] |= 1170;
                        }
                    }

                    if (var8.w != 0 && var7 != null) {
                        var7.q(var1, var2, var5, var4, var8.s);
                    }

                    if (var8.m != 16) {
                        var6.d(var0, var1, var2, var8.m);
                    }

                } else if (var5 == 1) {
                    if (var8.h == -1 && var8.innerObjectIds == null) {
                        var30 = var8.x(1, var4, var15, var17, var16, var18);
                    } else {
                        var30 = new AnimableNode(var3, 1, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                    }

                    var6.p(var0, var1, var2, var16, (Renderable) var30, (Renderable) null, bt.s[var4], 0, var19, var20);
                    if (var8.az) {
                        if (var4 == 0) {
                            bt.x[var0][var1][var2 + 1] = 50;
                        } else if (var4 == 1) {
                            bt.x[var0][var1 + 1][var2 + 1] = 50;
                        } else if (var4 == 2) {
                            bt.x[var0][var1 + 1][var2] = 50;
                        } else if (var4 == 3) {
                            bt.x[var0][var1][var2] = 50;
                        }
                    }

                    if (var8.w != 0 && var7 != null) {
                        var7.q(var1, var2, var5, var4, var8.s);
                    }

                } else {
                    int var26;
                    Object var28;
                    if (var5 == 2) {
                        var26 = var4 + 1 & 3;
                        Object var27;
                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var27 = var8.x(2, var4 + 4, var15, var17, var16, var18);
                            var28 = var8.x(2, var26, var15, var17, var16, var18);
                        } else {
                            var27 = new AnimableNode(var3, 2, var4 + 4, var0, var1, var2, var8.h, true,
                                    (Renderable) null);
                            var28 = new AnimableNode(var3, 2, var26, var0, var1, var2, var8.h, true, (Renderable) null);
                        }

                        var6.p(var0, var1, var2, var16, (Renderable) var27, (Renderable) var28, bt.w[var4],
                                bt.w[var26], var19, var20);
                        if (var8.clipped) {
                            if (var4 == 0) {
                                bt.u[var0][var1][var2] |= 585;
                                bt.u[var0][var1][var2 + 1] |= 1170;
                            } else if (var4 == 1) {
                                bt.u[var0][var1][var2 + 1] |= 1170;
                                bt.u[var0][var1 + 1][var2] |= 585;
                            } else if (var4 == 2) {
                                bt.u[var0][var1 + 1][var2] |= 585;
                                bt.u[var0][var1][var2] |= 1170;
                            } else if (var4 == 3) {
                                bt.u[var0][var1][var2] |= 1170;
                                bt.u[var0][var1][var2] |= 585;
                            }
                        }

                        if (var8.w != 0 && var7 != null) {
                            var7.q(var1, var2, var5, var4, var8.s);
                        }

                        if (var8.m != 16) {
                            var6.d(var0, var1, var2, var8.m);
                        }

                    } else if (var5 == 3) {
                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var30 = var8.x(3, var4, var15, var17, var16, var18);
                        } else {
                            var30 = new AnimableNode(var3, 3, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                        }

                        var6.p(var0, var1, var2, var16, (Renderable) var30, (Renderable) null, bt.s[var4], 0, var19,
                                var20);
                        if (var8.az) {
                            if (var4 == 0) {
                                bt.x[var0][var1][var2 + 1] = 50;
                            } else if (var4 == 1) {
                                bt.x[var0][var1 + 1][var2 + 1] = 50;
                            } else if (var4 == 2) {
                                bt.x[var0][var1 + 1][var2] = 50;
                            } else if (var4 == 3) {
                                bt.x[var0][var1][var2] = 50;
                            }
                        }

                        if (var8.w != 0 && var7 != null) {
                            var7.q(var1, var2, var5, var4, var8.s);
                        }

                    } else if (var5 == 9) {
                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var30 = var8.x(var5, var4, var15, var17, var16, var18);
                        } else {
                            var30 = new AnimableNode(var3, var5, var4, var0, var1, var2, var8.h, true,
                                    (Renderable) null);
                        }

                        var6.c(var0, var1, var2, var16, 1, 1, (Renderable) var30, 0, var19, var20);
                        if (var8.w != 0 && var7 != null) {
                            var7.i(var1, var2, var9, var10, var8.s);
                        }

                        if (var8.m != 16) {
                            var6.d(var0, var1, var2, var8.m);
                        }

                    } else if (var5 == 4) {
                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var30 = var8.x(4, var4, var15, var17, var16, var18);
                        } else {
                            var30 = new AnimableNode(var3, 4, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                        }

                        var6.o(var0, var1, var2, var16, (Renderable) var30, (Renderable) null, bt.w[var4], 0, 0, 0,
                                var19, var20);
                    } else if (var5 == 5) {
                        var26 = 16;
                        var22 = var6.ap(var0, var1, var2);
                        if (var22 != 0) {
                            var26 = jw.q(var22 >> 14 & 32767).m;
                        }

                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var28 = var8.x(4, var4, var15, var17, var16, var18);
                        } else {
                            var28 = new AnimableNode(var3, 4, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                        }

                        var6.o(var0, var1, var2, var16, (Renderable) var28, (Renderable) null, bt.w[var4], 0, var26
                                * bt.d[var4], var26 * bt.f[var4], var19, var20);
                    } else if (var5 == 6) {
                        var26 = 8;
                        var22 = var6.ap(var0, var1, var2);
                        if (var22 != 0) {
                            var26 = jw.q(var22 >> 14 & 32767).m / 2;
                        }

                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var28 = var8.x(4, var4 + 4, var15, var17, var16, var18);
                        } else {
                            var28 = new AnimableNode(var3, 4, var4 + 4, var0, var1, var2, var8.h, true,
                                    (Renderable) null);
                        }

                        var6.o(var0, var1, var2, var16, (Renderable) var28, (Renderable) null, 256, var4, var26
                                * bt.r[var4], var26 * bt.y[var4], var19, var20);
                    } else if (var5 == 7) {
                        var22 = var4 + 2 & 3;
                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var30 = var8.x(4, var22 + 4, var15, var17, var16, var18);
                        } else {
                            var30 = new AnimableNode(var3, 4, var22 + 4, var0, var1, var2, var8.h, true,
                                    (Renderable) null);
                        }

                        var6.o(var0, var1, var2, var16, (Renderable) var30, (Renderable) null, 256, var22, 0, 0, var19,
                                var20);
                    } else if (var5 == 8) {
                        var26 = 8;
                        var22 = var6.ap(var0, var1, var2);
                        if (var22 != 0) {
                            var26 = jw.q(var22 >> 14 & 32767).m / 2;
                        }

                        int var25 = var4 + 2 & 3;
                        Object var29;
                        if (var8.h == -1 && var8.innerObjectIds == null) {
                            var28 = var8.x(4, var4 + 4, var15, var17, var16, var18);
                            var29 = var8.x(4, var25 + 4, var15, var17, var16, var18);
                        } else {
                            var28 = new AnimableNode(var3, 4, var4 + 4, var0, var1, var2, var8.h, true,
                                    (Renderable) null);
                            var29 = new AnimableNode(var3, 4, var25 + 4, var0, var1, var2, var8.h, true,
                                    (Renderable) null);
                        }

                        var6.o(var0, var1, var2, var16, (Renderable) var28, (Renderable) var29, 256, var4, var26
                                * bt.r[var4], var26 * bt.y[var4], var19, var20);
                    }
                }
            } else {
                if (var8.h == -1 && var8.innerObjectIds == null) {
                    var30 = var8.x(10, var4, var15, var17, var16, var18);
                } else {
                    var30 = new AnimableNode(var3, 10, var4, var0, var1, var2, var8.h, true, (Renderable) null);
                }

                if (var30 != null
                        && var6.c(var0, var1, var2, var16, var9, var10, (Renderable) var30, var5 == 11 ? 256 : 0,
                                var19, var20) && var8.az) {
                    var22 = 15;
                    if (var30 instanceof Model) {
                        var22 = ((Model) var30).x() / 4;
                        if (var22 > 30) {
                            var22 = 30;
                        }
                    }

                    for (var23 = 0; var23 <= var9; ++var23) {
                        for (int var24 = 0; var24 <= var10; ++var24) {
                            if (var22 > bt.x[var0][var23 + var1][var24 + var2]) {
                                bt.x[var0][var23 + var1][var24 + var2] = (byte) var22;
                            }
                        }
                    }
                }

                if (var8.w != 0 && var7 != null) {
                    var7.i(var1, var2, var9, var10, var8.s);
                }

            }
        }
    }

    @ObfuscatedName("fr")
    static final void fr() {
        int[] var0 = cx.e;

        int var1;
        for (var1 = 0; var1 < cx.b; ++var1) {
            Player var2 = Client.loadedPlayers[var0[var1]];
            if (var2 != null && var2.aa > 0) {
                --var2.aa;
                if (var2.aa == 0) {
                    var2.textSpoken = null;
                }
            }
        }

        for (var1 = 0; var1 < Client.dz; ++var1) {
            int var4 = Client.npcIndices[var1];
            Npc var3 = Client.loadedNpcs[var4];
            if (var3 != null && var3.aa > 0) {
                --var3.aa;
                if (var3.aa == 0) {
                    var3.textSpoken = null;
                }
            }
        }

    }
}
