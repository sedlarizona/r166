import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kl")
public class kl extends ku {

    @ObfuscatedName("q")
    final lu q;

    @ObfuscatedName("i")
    final kz i;

    @ObfuscatedName("g")
    public String g = null;

    @ObfuscatedName("n")
    public String n = null;

    @ObfuscatedName("o")
    public byte o;

    @ObfuscatedName("c")
    public int c;

    @ObfuscatedName("v")
    int v = 1;

    public kl(lu var1, kz var2) {
        super(100);
        this.q = var1;
        this.i = var2;
    }

    @ObfuscatedName("t")
    kv t() {
        return new ki();
    }

    @ObfuscatedName("q")
    kv[] q(int var1) {
        return new ki[var1];
    }

    @ObfuscatedName("i")
    final void i(String var1) {
        String var3 = m.i(u.t(var1));
        if (var3 == null) {
            var3 = "";
        }

        this.g = var3;
    }

    @ObfuscatedName("p")
    final void p(String var1) {
        String var3 = m.i(u.t(var1));
        if (var3 == null) {
            var3 = "";
        }

        this.n = var3;
    }

    @ObfuscatedName("o")
    public final void o(ByteBuffer var1) {
        this.p(var1.ar());
        long var2 = var1.ah();
        this.i(a.q(var2));
        this.o = var1.aj();
        int var4 = var1.av();
        if (var4 != 255) {
            this.c();

            for (int var5 = 0; var5 < var4; ++var5) {
                ki var6 = (ki) this.y(new kb(var1.ar(), this.q));
                int var7 = var1.ae();
                var6.ad(var7, ++this.v - 1);
                var6.e = var1.aj();
                var1.ar();
                this.cg(var6);
            }

        }
    }

    @ObfuscatedName("ce")
    public final void ce(ByteBuffer var1) {
        kb var2 = new kb(var1.ar(), this.q);
        int var3 = var1.ae();
        byte var4 = var1.aj();
        boolean var5 = false;
        if (var4 == -128) {
            var5 = true;
        }

        ki var6;
        if (var5) {
            if (this.u() == 0) {
                return;
            }

            var6 = (ki) this.s(var2);
            if (var6 != null && var6.bg() == var3) {
                this.r(var6);
            }
        } else {
            var1.ar();
            var6 = (ki) this.w(var2);
            if (var6 == null) {
                if (this.u() > super.a) {
                    return;
                }

                var6 = (ki) this.y(var2);
            }

            var6.ad(var3, ++this.v - 1);
            var6.e = var4;
            this.cg(var6);
        }

    }

    @ObfuscatedName("cx")
    public final void cx() {
        for (int var1 = 0; var1 < this.u(); ++var1) {
            ((ki) this.av(var1)).t();
        }

    }

    @ObfuscatedName("cy")
    public final void cy() {
        for (int var1 = 0; var1 < this.u(); ++var1) {
            ((ki) this.av(var1)).a();
        }

    }

    @ObfuscatedName("cg")
    final void cg(ki var1) {
        if (var1.r().equals(this.i.jd())) {
            this.c = var1.e;
        }

    }
}
