import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("h")
public class h {

    @ObfuscatedName("t")
    public static final h t = new h("SMALL", 1, 0, 4);

    @ObfuscatedName("q")
    public static final h q = new h("MEDIUM", 0, 1, 2);

    @ObfuscatedName("i")
    public static final h i = new h("LARGE", 2, 2, 0);

    @ObfuscatedName("cv")
    public static char cv;

    @ObfuscatedName("dk")
    static int dk;

    @ObfuscatedName("id")
    static dg id;

    @ObfuscatedName("a")
    final String a;

    @ObfuscatedName("l")
    final int l;

    @ObfuscatedName("b")
    final int b;

    @ObfuscatedName("e")
    final int e;

    h(String var1, int var2, int var3, int var4) {
        this.a = var1;
        this.l = var2;
        this.b = var3;
        this.e = var4;
    }

    @ObfuscatedName("t")
    boolean t(float var1) {
        return var1 >= (float) this.e;
    }

    @ObfuscatedName("t")
    static Sprite t(int var0, int var1, int var2) {
        he var3 = au.e;
        long var4 = (long) (var2 << 16 | var0 << 8 | var1);
        return (Sprite) var3.t(var4);
    }

    @ObfuscatedName("e")
    static void e() {
        Iterator var0 = cg.q.iterator();

        while (var0.hasNext()) {
            ChatboxMessage var1 = (ChatboxMessage) var0.next();
            var1.l();
        }

    }

    @ObfuscatedName("hk")
    static final void addMenuItem(String var0, String var1, int var2, int var3, int var4, int var5, boolean var6) {
        if (!Client.menuOpen) {
            if (Client.menuSize < 500) {
                Client.menuActions[Client.menuSize] = var0;
                Client.menuTargets[Client.menuSize] = var1;
                Client.menuOpcodes[Client.menuSize] = var2;
                Client.ka[Client.menuSize] = var3;
                Client.menuArg1[Client.menuSize] = var4;
                Client.menuArg2[Client.menuSize] = var5;
                Client.kc[Client.menuSize] = var6;
                ++Client.menuSize;
            }

        }
    }

    @ObfuscatedName("jy")
    static void jy(ByteBuffer var0, int var1) {
        DevelopmentStage.je(var0.buffer, var1);
        if (fh.o != null) {
            try {
                fh.o.q(0L);
                fh.o.e(var0.buffer, var1, 24);
            } catch (Exception var3) {
                ;
            }
        }

    }
}
