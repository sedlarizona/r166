import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bd")
public class Server {

    @ObfuscatedName("l")
    static Server[] l;

    @ObfuscatedName("b")
    static int b = 0;

    @ObfuscatedName("e")
    static int e = 0;

    @ObfuscatedName("x")
    static int[] x = new int[] { 1, 1, 1, 1 };

    @ObfuscatedName("p")
    static int[] p = new int[] { 0, 1, 2, 3 };

    @ObfuscatedName("g")
    static ep g;

    @ObfuscatedName("cy")
    static ju cy;

    @ObfuscatedName("n")
    int number;

    @ObfuscatedName("o")
    int type;

    @ObfuscatedName("c")
    int population;

    @ObfuscatedName("v")
    String domain;

    @ObfuscatedName("u")
    String activity;

    @ObfuscatedName("j")
    int location;

    @ObfuscatedName("k")
    int index;

    @ObfuscatedName("x")
    boolean x() {
        return (1 & this.type) != 0;
    }

    @ObfuscatedName("p")
    boolean p() {
        return (2 & this.type) != 0;
    }

    @ObfuscatedName("o")
    boolean o() {
        return (4 & this.type) != 0;
    }

    @ObfuscatedName("c")
    boolean c() {
        return (8 & this.type) != 0;
    }

    @ObfuscatedName("u")
    boolean u() {
        return (536870912 & this.type) != 0;
    }

    @ObfuscatedName("k")
    boolean k() {
        return (33554432 & this.type) != 0;
    }

    @ObfuscatedName("t")
    public static int t(int var0) {
        Varpbit var1 = ji.t(var0);
        int var2 = var1.varp;
        int var3 = var1.low;
        int var4 = var1.high;
        int var5 = iv.t[var4 - var3];
        return iv.varps[var2] >> var3 & var5;
    }

    @ObfuscatedName("ae")
    public static final void ae(cf var0) {
        TaskData.c = var0;
    }

    @ObfuscatedName("ik")
    static String ik(String var0, RTComponent var1) {
        if (var0.indexOf("%") != -1) {
            for (int var2 = 1; var2 <= 5; ++var2) {
                while (true) {
                    int var3 = var0.indexOf("%" + var2);
                    if (var3 == -1) {
                        break;
                    }

                    var0 = var0.substring(0, var3) + ByteBuffer.ij(dc.ir(var1, var2 - 1)) + var0.substring(var3 + 2);
                }
            }
        }

        return var0;
    }
}
