import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("k")
public class GrandExchangeOffer {

    @ObfuscatedName("on")
    static Sprite on;

    @ObfuscatedName("e")
    static int[] e;

    @ObfuscatedName("p")
    public static int p;

    @ObfuscatedName("bj")
    static String bj;

    @ObfuscatedName("t")
    byte state;

    @ObfuscatedName("q")
    public int itemId;

    @ObfuscatedName("i")
    public int itemPrice;

    @ObfuscatedName("a")
    public int totalOfferQuantity;

    @ObfuscatedName("l")
    public int itemsExchanged;

    @ObfuscatedName("b")
    public int coinsExchanged;

    public GrandExchangeOffer() {
    }

    public GrandExchangeOffer(ByteBuffer var1, boolean var2) {
        this.state = var1.aj();
        this.itemId = var1.ae();
        this.itemPrice = var1.ap();
        this.totalOfferQuantity = var1.ap();
        this.itemsExchanged = var1.ap();
        this.coinsExchanged = var1.ap();
    }

    @ObfuscatedName("i")
    public int i() {
        return this.state & 7;
    }

    @ObfuscatedName("a")
    public int a() {
        return (this.state & 8) == 8 ? 1 : 0;
    }

    @ObfuscatedName("l")
    void l(int var1) {
        this.state &= -8;
        this.state = (byte) (this.state | var1 & 7);
    }

    @ObfuscatedName("b")
    void b(int var1) {
        this.state &= -9;
        if (var1 == 1) {
            this.state = (byte) (this.state | 8);
        }

    }

    @ObfuscatedName("q")
    public static int q(int var0) {
        return var0 >> 17 & 7;
    }

    @ObfuscatedName("a")
    static void a(int var0, boolean var1, int var2, boolean var3) {
        if (Server.l != null) {
            ae.l(0, Server.l.length - 1, var0, var1, var2, var3);
        }

    }

    @ObfuscatedName("u")
    static final int u(int var0, int var1) {
        if (var0 == -2) {
            return 12345678;
        } else if (var0 == -1) {
            if (var1 < 2) {
                var1 = 2;
            } else if (var1 > 126) {
                var1 = 126;
            }

            return var1;
        } else {
            var1 = (var0 & 127) * var1 / 128;
            if (var1 < 2) {
                var1 = 2;
            } else if (var1 > 126) {
                var1 = 126;
            }

            return (var0 & 'ﾀ') + var1;
        }
    }
}
