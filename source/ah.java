import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ah")
public class ah {

    @ObfuscatedName("r")
    static int[] r;

    @ObfuscatedName("eu")
    static fy eu;

    @ObfuscatedName("lp")
    static int lp;

    @ObfuscatedName("t")
    String t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    h a;

    ah(String var1, int var2, int var3, h var4) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
        this.a = var4;
    }

    @ObfuscatedName("t")
    public static jw t(int var0) {
        jw var1 = (jw) jw.i.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = jw.t.i(13, var0);
            var1 = new jw();
            var1.l = var0;
            if (var2 != null) {
                var1.q(new ByteBuffer(var2));
            }

            jw.i.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("t")
    public static IdentityKitNode t(FileSystem var0, FileSystem var1, int var2, boolean var3) {
        boolean var4 = true;
        int[] var5 = var0.z(var2);

        for (int var6 = 0; var6 < var5.length; ++var6) {
            byte[] var7 = var0.c(var2, var5[var6]);
            if (var7 == null) {
                var4 = false;
            } else {
                int var8 = (var7[0] & 255) << 8 | var7[1] & 255;
                byte[] var9;
                if (var3) {
                    var9 = var1.c(0, var8);
                } else {
                    var9 = var1.c(var8, 0);
                }

                if (var9 == null) {
                    var4 = false;
                }
            }
        }

        if (!var4) {
            return null;
        } else {
            try {
                return new IdentityKitNode(var0, var1, var2, var3);
            } catch (Exception var11) {
                return null;
            }
        }
    }

    @ObfuscatedName("eq")
    static final void eq() {
        Region.t = false;
        Client.bh = false;
    }

    @ObfuscatedName("fp")
    static ju fp(int var0, boolean var1, boolean var2, boolean var3) {
        fn var4 = null;
        if (fh.c != null) {
            var4 = new fn(var0, fh.c, ag.u[var0], 1000000);
        }

        return new ju(var4, gd.rb, var0, var1, var2, var3);
    }
}
