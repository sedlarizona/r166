import java.io.File;
import java.io.RandomAccessFile;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jv")
public class jv extends hh {

    @ObfuscatedName("t")
    static FileSystem t;

    @ObfuscatedName("q")
    static Cache q = new Cache(64);

    @ObfuscatedName("k")
    static int k;

    @ObfuscatedName("by")
    static int by;

    @ObfuscatedName("i")
    public char i;

    @ObfuscatedName("a")
    public char a;

    @ObfuscatedName("l")
    public String l = "null";

    @ObfuscatedName("b")
    public int b;

    @ObfuscatedName("e")
    public int e = 0;

    @ObfuscatedName("x")
    public int[] x;

    @ObfuscatedName("p")
    public int[] p;

    @ObfuscatedName("g")
    public String[] g;

    @ObfuscatedName("i")
    void i(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.a(var1, var2);
        }
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.i = (char) var1.av();
        } else if (var2 == 2) {
            this.a = (char) var1.av();
        } else if (var2 == 3) {
            this.l = var1.ar();
        } else if (var2 == 4) {
            this.b = var1.ap();
        } else {
            int var3;
            if (var2 == 5) {
                this.e = var1.ae();
                this.x = new int[this.e];
                this.g = new String[this.e];

                for (var3 = 0; var3 < this.e; ++var3) {
                    this.x[var3] = var1.ap();
                    this.g[var3] = var1.ar();
                }
            } else if (var2 == 6) {
                this.e = var1.ae();
                this.x = new int[this.e];
                this.p = new int[this.e];

                for (var3 = 0; var3 < this.e; ++var3) {
                    this.x[var3] = var1.ap();
                    this.p[var3] = var1.ap();
                }
            }
        }

    }

    @ObfuscatedName("l")
    public int l() {
        return this.e;
    }

    @ObfuscatedName("q")
    public static File q(String var0) {
        if (!fw.t) {
            throw new RuntimeException("");
        } else {
            File var1 = (File) fw.i.get(var0);
            if (var1 != null) {
                return var1;
            } else {
                File var2 = new File(fw.q, var0);
                RandomAccessFile var3 = null;

                try {
                    File var4 = new File(var2.getParent());
                    if (!var4.exists()) {
                        throw new RuntimeException("");
                    } else {
                        var3 = new RandomAccessFile(var2, "rw");
                        int var5 = var3.read();
                        var3.seek(0L);
                        var3.write(var5);
                        var3.seek(0L);
                        var3.close();
                        fw.i.put(var0, var2);
                        return var2;
                    }
                } catch (Exception var8) {
                    try {
                        if (var3 != null) {
                            var3.close();
                            var3 = null;
                        }
                    } catch (Exception var7) {
                        ;
                    }

                    throw new RuntimeException();
                }
            }
        }
    }

    @ObfuscatedName("gi")
    static void gi() {
        int var0 = cx.b;
        int[] var1 = cx.e;

        for (int var2 = 0; var2 < var0; ++var2) {
            if (var1[var2] != Client.jq && var1[var2] != Client.localPlayerIndex) {
                bt.gh(Client.loadedPlayers[var1[var2]], true);
            }
        }

    }
}
