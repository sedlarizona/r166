import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kg")
public class kg {

    @ObfuscatedName("t")
    static final kg t = new kg("7", "7");

    @ObfuscatedName("q")
    static final kg q = new kg("13", "13");

    @ObfuscatedName("i")
    static final kg i = new kg("11", "11");

    @ObfuscatedName("a")
    static final kg a = new kg("3", "3");

    @ObfuscatedName("l")
    static final kg l = new kg("8", "8");

    @ObfuscatedName("b")
    static final kg b = new kg("14", "14");

    @ObfuscatedName("e")
    static final kg e = new kg("12", "12");

    @ObfuscatedName("x")
    static final kg x = new kg("5", "5");

    @ObfuscatedName("p")
    static final kg p = new kg("1", "1");

    @ObfuscatedName("g")
    static final kg g = new kg("2", "2");

    @ObfuscatedName("n")
    static final kg n = new kg("6", "6");

    @ObfuscatedName("o")
    static final kg o = new kg("4", "4");

    @ObfuscatedName("c")
    public static final kg c = new kg("15", "15");

    @ObfuscatedName("v")
    public static final kg v = new kg("10", "10");

    @ObfuscatedName("u")
    static final kg u = new kg("16", "16");

    @ObfuscatedName("j")
    static final kg j = new kg("9", "9");

    @ObfuscatedName("k")
    public final String k;

    kg(String var1, String var2) {
        this.k = var2;
    }
}
