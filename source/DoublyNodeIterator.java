import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hz")
public class DoublyNodeIterator implements Iterator {

    @ObfuscatedName("t")
    SinglyLinkedList iterableNode;

    @ObfuscatedName("q")
    Node next;

    @ObfuscatedName("i")
    Node current = null;

    DoublyNodeIterator(SinglyLinkedList var1) {
        this.iterableNode = var1;
        this.next = this.iterableNode.sentinel.next;
        this.current = null;
    }

    public Object next() {
        Node var1 = this.next;
        if (var1 == this.iterableNode.sentinel) {
            var1 = null;
            this.next = null;
        } else {
            this.next = var1.next;
        }

        this.current = var1;
        return var1;
    }

    public boolean hasNext() {
        return this.iterableNode.sentinel != this.next;
    }

    public void remove() {
        this.current.kc();
        this.current = null;
    }
}
