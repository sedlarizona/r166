import java.applet.Applet;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bc")
public class bc {

    @ObfuscatedName("t")
    static Applet t = null;

    @ObfuscatedName("q")
    static String q = "";

    @ObfuscatedName("hz")
    static final void hz(int var0, int var1, int var2, int var3, int var4, int var5, int var6) {
        if (var2 >= 1 && var3 >= 1 && var2 <= 102 && var3 <= 102) {
            if (Client.bh && var0 != kt.ii) {
                return;
            }

            int var7 = 0;
            boolean var8 = true;
            boolean var9 = false;
            boolean var10 = false;
            if (var1 == 0) {
                var7 = an.loadedRegion.ap(var0, var2, var3);
            }

            if (var1 == 1) {
                var7 = an.loadedRegion.ah(var0, var2, var3);
            }

            if (var1 == 2) {
                var7 = an.loadedRegion.au(var0, var2, var3);
            }

            if (var1 == 3) {
                var7 = an.loadedRegion.ax(var0, var2, var3);
            }

            int var11;
            if (var7 != 0) {
                var11 = an.loadedRegion.ar(var0, var2, var3, var7);
                int var34 = var7 >> 14 & 32767;
                int var35 = var11 & 31;
                int var36 = var11 >> 6 & 3;
                ObjectDefinition var12;
                if (var1 == 0) {
                    an.loadedRegion.f(var0, var2, var3);
                    var12 = jw.q(var34);
                    if (var12.w != 0) {
                        Client.collisionMaps[var0].e(var2, var3, var35, var36, var12.s);
                    }
                }

                if (var1 == 1) {
                    an.loadedRegion.r(var0, var2, var3);
                }

                if (var1 == 2) {
                    an.loadedRegion.y(var0, var2, var3);
                    var12 = jw.q(var34);
                    if (var2 + var12.width > 103 || var3 + var12.width > 103 || var2 + var12.height > 103
                            || var3 + var12.height > 103) {
                        return;
                    }

                    if (var12.w != 0) {
                        Client.collisionMaps[var0].x(var2, var3, var12.width, var12.height, var36, var12.s);
                    }
                }

                if (var1 == 3) {
                    an.loadedRegion.h(var0, var2, var3);
                    var12 = jw.q(var34);
                    if (var12.w == 1) {
                        Client.collisionMaps[var0].o(var2, var3);
                    }
                }
            }

            if (var4 >= 0) {
                var11 = var0;
                if (var0 < 3 && (bt.landscapeData[1][var2][var3] & 2) == 2) {
                    var11 = var0 + 1;
                }

                Region var37 = an.loadedRegion;
                CollisionMap var13 = Client.collisionMaps[var0];
                ObjectDefinition var14 = jw.q(var4);
                int var15;
                int var16;
                if (var5 != 1 && var5 != 3) {
                    var15 = var14.width;
                    var16 = var14.height;
                } else {
                    var15 = var14.height;
                    var16 = var14.width;
                }

                int var17;
                int var18;
                if (var15 + var2 <= 104) {
                    var17 = (var15 >> 1) + var2;
                    var18 = var2 + (var15 + 1 >> 1);
                } else {
                    var17 = var2;
                    var18 = var2 + 1;
                }

                int var19;
                int var20;
                if (var3 + var16 <= 104) {
                    var19 = var3 + (var16 >> 1);
                    var20 = var3 + (var16 + 1 >> 1);
                } else {
                    var19 = var3;
                    var20 = var3 + 1;
                }

                int[][] var21 = bt.tileHeights[var11];
                int var22 = var21[var17][var20] + var21[var17][var19] + var21[var18][var19] + var21[var18][var20] >> 2;
                int var23 = (var2 << 7) + (var15 << 6);
                int var24 = (var3 << 7) + (var16 << 6);
                int var25 = (var3 << 7) + var2 + (var4 << 14) + 1073741824;
                if (var14.d == 0) {
                    var25 -= Integer.MIN_VALUE;
                }

                int var26 = (var5 << 6) + var6;
                if (var14.at == 1) {
                    var26 += 256;
                }

                Object var27;
                if (var6 == 22) {
                    if (var14.h == -1 && var14.innerObjectIds == null) {
                        var27 = var14.p(22, var5, var21, var23, var22, var24);
                    } else {
                        var27 = new AnimableNode(var4, 22, var5, var11, var2, var3, var14.h, true, (Renderable) null);
                    }

                    var37.e(var0, var2, var3, var22, (Renderable) var27, var25, var26);
                    if (var14.w == 1) {
                        var13.l(var2, var3);
                    }
                } else if (var6 != 10 && var6 != 11) {
                    if (var6 >= 12) {
                        if (var14.h == -1 && var14.innerObjectIds == null) {
                            var27 = var14.p(var6, var5, var21, var23, var22, var24);
                        } else {
                            var27 = new AnimableNode(var4, var6, var5, var11, var2, var3, var14.h, true,
                                    (Renderable) null);
                        }

                        var37.c(var0, var2, var3, var22, 1, 1, (Renderable) var27, 0, var25, var26);
                        if (var14.w != 0) {
                            var13.i(var2, var3, var15, var16, var14.s);
                        }
                    } else if (var6 == 0) {
                        if (var14.h == -1 && var14.innerObjectIds == null) {
                            var27 = var14.p(0, var5, var21, var23, var22, var24);
                        } else {
                            var27 = new AnimableNode(var4, 0, var5, var11, var2, var3, var14.h, true, (Renderable) null);
                        }

                        var37.p(var0, var2, var3, var22, (Renderable) var27, (Renderable) null, bt.w[var5], 0, var25,
                                var26);
                        if (var14.w != 0) {
                            var13.q(var2, var3, var6, var5, var14.s);
                        }
                    } else if (var6 == 1) {
                        if (var14.h == -1 && var14.innerObjectIds == null) {
                            var27 = var14.p(1, var5, var21, var23, var22, var24);
                        } else {
                            var27 = new AnimableNode(var4, 1, var5, var11, var2, var3, var14.h, true, (Renderable) null);
                        }

                        var37.p(var0, var2, var3, var22, (Renderable) var27, (Renderable) null, bt.s[var5], 0, var25,
                                var26);
                        if (var14.w != 0) {
                            var13.q(var2, var3, var6, var5, var14.s);
                        }
                    } else {
                        Object var29;
                        int var32;
                        if (var6 == 2) {
                            var32 = var5 + 1 & 3;
                            Object var28;
                            if (var14.h == -1 && var14.innerObjectIds == null) {
                                var28 = var14.p(2, var5 + 4, var21, var23, var22, var24);
                                var29 = var14.p(2, var32, var21, var23, var22, var24);
                            } else {
                                var28 = new AnimableNode(var4, 2, var5 + 4, var11, var2, var3, var14.h, true,
                                        (Renderable) null);
                                var29 = new AnimableNode(var4, 2, var32, var11, var2, var3, var14.h, true,
                                        (Renderable) null);
                            }

                            var37.p(var0, var2, var3, var22, (Renderable) var28, (Renderable) var29, bt.w[var5],
                                    bt.w[var32], var25, var26);
                            if (var14.w != 0) {
                                var13.q(var2, var3, var6, var5, var14.s);
                            }
                        } else if (var6 == 3) {
                            if (var14.h == -1 && var14.innerObjectIds == null) {
                                var27 = var14.p(3, var5, var21, var23, var22, var24);
                            } else {
                                var27 = new AnimableNode(var4, 3, var5, var11, var2, var3, var14.h, true,
                                        (Renderable) null);
                            }

                            var37.p(var0, var2, var3, var22, (Renderable) var27, (Renderable) null, bt.s[var5], 0,
                                    var25, var26);
                            if (var14.w != 0) {
                                var13.q(var2, var3, var6, var5, var14.s);
                            }
                        } else if (var6 == 9) {
                            if (var14.h == -1 && var14.innerObjectIds == null) {
                                var27 = var14.p(var6, var5, var21, var23, var22, var24);
                            } else {
                                var27 = new AnimableNode(var4, var6, var5, var11, var2, var3, var14.h, true,
                                        (Renderable) null);
                            }

                            var37.c(var0, var2, var3, var22, 1, 1, (Renderable) var27, 0, var25, var26);
                            if (var14.w != 0) {
                                var13.i(var2, var3, var15, var16, var14.s);
                            }
                        } else if (var6 == 4) {
                            if (var14.h == -1 && var14.innerObjectIds == null) {
                                var27 = var14.p(4, var5, var21, var23, var22, var24);
                            } else {
                                var27 = new AnimableNode(var4, 4, var5, var11, var2, var3, var14.h, true,
                                        (Renderable) null);
                            }

                            var37.o(var0, var2, var3, var22, (Renderable) var27, (Renderable) null, bt.w[var5], 0, 0,
                                    0, var25, var26);
                        } else {
                            int var33;
                            if (var6 == 5) {
                                var32 = 16;
                                var33 = var37.ap(var0, var2, var3);
                                if (var33 != 0) {
                                    var32 = jw.q(var33 >> 14 & 32767).m;
                                }

                                if (var14.h == -1 && var14.innerObjectIds == null) {
                                    var29 = var14.p(4, var5, var21, var23, var22, var24);
                                } else {
                                    var29 = new AnimableNode(var4, 4, var5, var11, var2, var3, var14.h, true,
                                            (Renderable) null);
                                }

                                var37.o(var0, var2, var3, var22, (Renderable) var29, (Renderable) null, bt.w[var5], 0,
                                        var32 * bt.d[var5], var32 * bt.f[var5], var25, var26);
                            } else if (var6 == 6) {
                                var32 = 8;
                                var33 = var37.ap(var0, var2, var3);
                                if (var33 != 0) {
                                    var32 = jw.q(var33 >> 14 & 32767).m / 2;
                                }

                                if (var14.h == -1 && var14.innerObjectIds == null) {
                                    var29 = var14.p(4, var5 + 4, var21, var23, var22, var24);
                                } else {
                                    var29 = new AnimableNode(var4, 4, var5 + 4, var11, var2, var3, var14.h, true,
                                            (Renderable) null);
                                }

                                var37.o(var0, var2, var3, var22, (Renderable) var29, (Renderable) null, 256, var5,
                                        var32 * bt.r[var5], var32 * bt.y[var5], var25, var26);
                            } else if (var6 == 7) {
                                var33 = var5 + 2 & 3;
                                if (var14.h == -1 && var14.innerObjectIds == null) {
                                    var27 = var14.p(4, var33 + 4, var21, var23, var22, var24);
                                } else {
                                    var27 = new AnimableNode(var4, 4, var33 + 4, var11, var2, var3, var14.h, true,
                                            (Renderable) null);
                                }

                                var37.o(var0, var2, var3, var22, (Renderable) var27, (Renderable) null, 256, var33, 0,
                                        0, var25, var26);
                            } else if (var6 == 8) {
                                var32 = 8;
                                var33 = var37.ap(var0, var2, var3);
                                if (var33 != 0) {
                                    var32 = jw.q(var33 >> 14 & 32767).m / 2;
                                }

                                int var31 = var5 + 2 & 3;
                                Object var30;
                                if (var14.h == -1 && var14.innerObjectIds == null) {
                                    var29 = var14.p(4, var5 + 4, var21, var23, var22, var24);
                                    var30 = var14.p(4, var31 + 4, var21, var23, var22, var24);
                                } else {
                                    var29 = new AnimableNode(var4, 4, var5 + 4, var11, var2, var3, var14.h, true,
                                            (Renderable) null);
                                    var30 = new AnimableNode(var4, 4, var31 + 4, var11, var2, var3, var14.h, true,
                                            (Renderable) null);
                                }

                                var37.o(var0, var2, var3, var22, (Renderable) var29, (Renderable) var30, 256, var5,
                                        var32 * bt.r[var5], var32 * bt.y[var5], var25, var26);
                            }
                        }
                    }
                } else {
                    if (var14.h == -1 && var14.innerObjectIds == null) {
                        var27 = var14.p(10, var5, var21, var23, var22, var24);
                    } else {
                        var27 = new AnimableNode(var4, 10, var5, var11, var2, var3, var14.h, true, (Renderable) null);
                    }

                    if (var27 != null) {
                        var37.c(var0, var2, var3, var22, var15, var16, (Renderable) var27, var6 == 11 ? 256 : 0, var25,
                                var26);
                    }

                    if (var14.w != 0) {
                        var13.i(var2, var3, var15, var16, var14.s);
                    }
                }
            }
        }

    }

    @ObfuscatedName("id")
    static final boolean id(RTComponent var0) {
        if (var0.ee == null) {
            return false;
        } else {
            for (int var1 = 0; var1 < var0.ee.length; ++var1) {
                int var2 = dc.ir(var0, var1);
                int var3 = var0.eu[var1];
                if (var0.ee[var1] == 2) {
                    if (var2 >= var3) {
                        return false;
                    }
                } else if (var0.ee[var1] == 3) {
                    if (var2 <= var3) {
                        return false;
                    }
                } else if (var0.ee[var1] == 4) {
                    if (var3 == var2) {
                        return false;
                    }
                } else if (var3 != var2) {
                    return false;
                }
            }

            return true;
        }
    }
}
