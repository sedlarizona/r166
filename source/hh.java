import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hh")
public class hh extends Node {

    @ObfuscatedName("cg")
    long cg;

    @ObfuscatedName("cj")
    public hh cj;

    @ObfuscatedName("cl")
    public hh cl;

    @ObfuscatedName("ce")
    public void ce() {
        if (this.cl != null) {
            this.cl.cj = this.cj;
            this.cj.cl = this.cl;
            this.cj = null;
            this.cl = null;
        }
    }
}
