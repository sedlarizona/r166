import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ae")
public class ae implements as {

    @ObfuscatedName("pq")
    static int pq;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("t")
    public void t(az var1) {
        if (var1.e > this.e) {
            var1.e = this.e;
        }

        if (var1.x < this.p) {
            var1.x = this.p;
        }

        if (var1.p > this.x) {
            var1.p = this.x;
        }

        if (var1.g < this.g) {
            var1.g = this.g;
        }

    }

    @ObfuscatedName("q")
    public boolean q(int var1, int var2, int var3) {
        if (var1 >= this.t && var1 < this.t + this.q) {
            return var2 >> 6 >= this.i && var2 >> 6 <= this.l && var3 >> 6 >= this.a && var3 >> 6 <= this.b;
        } else {
            return false;
        }
    }

    @ObfuscatedName("i")
    public boolean i(int var1, int var2) {
        return var1 >> 6 >= this.e && var1 >> 6 <= this.p && var2 >> 6 >= this.x && var2 >> 6 <= this.g;
    }

    @ObfuscatedName("a")
    public int[] a(int var1, int var2, int var3) {
        if (!this.q(var1, var2, var3)) {
            return null;
        } else {
            int[] var4 = new int[] { this.e * 64 - this.i * 64 + var2, var3 + (this.x * 64 - this.a * 64) };
            return var4;
        }
    }

    @ObfuscatedName("l")
    public ik l(int var1, int var2) {
        if (!this.i(var1, var2)) {
            return null;
        } else {
            int var3 = this.i * 64 - this.e * 64 + var1;
            int var4 = this.a * 64 - this.x * 64 + var2;
            return new ik(this.t, var3, var4);
        }
    }

    @ObfuscatedName("b")
    public void b(ByteBuffer var1) {
        this.t = var1.av();
        this.q = var1.av();
        this.i = var1.ae();
        this.a = var1.ae();
        this.l = var1.ae();
        this.b = var1.ae();
        this.e = var1.ae();
        this.x = var1.ae();
        this.p = var1.ae();
        this.g = var1.ae();
        this.e();
    }

    @ObfuscatedName("e")
    void e() {
    }

    @ObfuscatedName("q")
    public static String q(ByteBuffer var0) {
        String var1;
        try {
            int var2 = var0.ag();
            if (var2 > 32767) {
                var2 = 32767;
            }

            byte[] var3 = new byte[var2];
            var0.index += lw.t.q(var0.buffer, var0.index, var3, 0, var2);
            String var4 = ChatboxMessage.a(var3, 0, var2);
            var1 = var4;
        } catch (Exception var6) {
            var1 = "Cabbage";
        }

        return var1;
    }

    @ObfuscatedName("i")
    public static void i() {
        if (ad.keyboard != null) {
            ad var0 = ad.keyboard;
            synchronized (ad.keyboard) {
                ad.keyboard = null;
            }
        }

    }

    @ObfuscatedName("a")
    public static void a() {
        ad var0 = ad.keyboard;
        synchronized (ad.keyboard) {
            ++ad.cn;
            ad.cj = ad.cz;
            ad.cg = 0;
            int var1;
            if (ad.cc < 0) {
                for (var1 = 0; var1 < 112; ++var1) {
                    ad.pressedKeys[var1] = false;
                }

                ad.cc = ad.ci;
            } else {
                while (ad.cc != ad.ci) {
                    var1 = ad.cq[ad.ci];
                    ad.ci = ad.ci + 1 & 127;
                    if (var1 < 0) {
                        ad.pressedKeys[~var1] = false;
                    } else {
                        if (!ad.pressedKeys[var1] && ad.cg < ad.cy.length - 1) {
                            ad.cy[++ad.cg - 1] = var1;
                        }

                        ad.pressedKeys[var1] = true;
                    }
                }
            }

            if (ad.cg > 0) {
                ad.cn = 0;
            }

            ad.cz = ad.cl;
        }
    }

    @ObfuscatedName("l")
    static void l(int var0, int var1, int var2, boolean var3, int var4, boolean var5) {
        if (var0 < var1) {
            int var6 = (var0 + var1) / 2;
            int var7 = var0;
            Server var8 = Server.l[var6];
            Server.l[var6] = Server.l[var1];
            Server.l[var1] = var8;

            for (int var9 = var0; var9 < var1; ++var9) {
                Server var11 = Server.l[var9];
                int var12 = g.b(var11, var8, var2, var3);
                int var10;
                if (var12 != 0) {
                    if (var3) {
                        var10 = -var12;
                    } else {
                        var10 = var12;
                    }
                } else if (var4 == -1) {
                    var10 = 0;
                } else {
                    int var13 = g.b(var11, var8, var4, var5);
                    if (var5) {
                        var10 = -var13;
                    } else {
                        var10 = var13;
                    }
                }

                if (var10 <= 0) {
                    Server var14 = Server.l[var9];
                    Server.l[var9] = Server.l[var7];
                    Server.l[var7++] = var14;
                }
            }

            Server.l[var1] = Server.l[var7];
            Server.l[var7] = var8;
            l(var0, var7 - 1, var2, var3, var4, var5);
            l(var7 + 1, var1, var2, var3, var4, var5);
        }

    }
}
