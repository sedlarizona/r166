import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ak")
public class ak {

    @ObfuscatedName("en")
    static ec en;

    @ObfuscatedName("ik")
    static RTComponent ik;

    @ObfuscatedName("jy")
    static int jy;

    @ObfuscatedName("t")
    public int t;

    @ObfuscatedName("q")
    public ik q;

    @ObfuscatedName("i")
    public ik i;

    public ak(int var1, ik var2, ik var3) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
    }

    @ObfuscatedName("t")
    static int t(int var0, int var1) {
        if (var0 == -2) {
            return 12345678;
        } else if (var0 == -1) {
            if (var1 < 0) {
                var1 = 0;
            } else if (var1 > 127) {
                var1 = 127;
            }

            var1 = 127 - var1;
            return var1;
        } else {
            var1 = (var0 & 127) * var1 / 128;
            if (var1 < 2) {
                var1 = 2;
            } else if (var1 > 126) {
                var1 = 126;
            }

            return (var0 & 'ﾀ') + var1;
        }
    }

    @ObfuscatedName("a")
    static int a(int var0) {
        Chatbox var1 = (Chatbox) cg.chatboxes.get(var0);
        return var1 == null ? 0 : var1.i();
    }

    @ObfuscatedName("p")
    static final void p(String var0) {
        j.t(30, "", var0);
    }

    @ObfuscatedName("jw")
    static final void jw() {
        Client.ms = Client.mn;
    }
}
