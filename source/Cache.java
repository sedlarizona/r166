import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hj")
public final class Cache {

    @ObfuscatedName("t")
    hh node = new hh();

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int remaining;

    @ObfuscatedName("a")
    HashTable table;

    @ObfuscatedName("l")
    Queue queue = new Queue();

    public Cache(int var1) {
        this.q = var1;
        this.remaining = var1;

        int var2;
        for (var2 = 1; var2 + var2 < var1; var2 += var2) {
            ;
        }

        this.table = new HashTable(var2);
    }

    @ObfuscatedName("t")
    public hh t(long var1) {
        hh var3 = (hh) this.table.t(var1);
        if (var3 != null) {
            this.queue.t(var3);
        }

        return var3;
    }

    @ObfuscatedName("q")
    public void q(long var1) {
        hh var3 = (hh) this.table.t(var1);
        if (var3 != null) {
            var3.kc();
            var3.ce();
            ++this.remaining;
        }

    }

    @ObfuscatedName("i")
    public void i(hh var1, long var2) {
        if (this.remaining == 0) {
            hh var4 = this.queue.a();
            var4.kc();
            var4.ce();
            if (var4 == this.node) {
                var4 = this.queue.a();
                var4.kc();
                var4.ce();
            }
        } else {
            --this.remaining;
        }

        this.table.q(var1, var2);
        this.queue.t(var1);
    }

    @ObfuscatedName("a")
    public void a() {
        this.queue.b();
        this.table.i();
        this.node = new hh();
        this.remaining = this.q;
    }
}
