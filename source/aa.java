import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("aa")
public class aa extends av {

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    int v;

    @ObfuscatedName("u")
    int u;

    @ObfuscatedName("t")
    void t(ByteBuffer var1, ByteBuffer var2) {
        int var3 = var2.av();
        if (var3 != ar.q.i) {
            throw new IllegalStateException("");
        } else {
            super.l = var2.av();
            super.b = var2.av();
            super.t = var2.ae();
            super.q = var2.ae();
            this.o = var2.av();
            this.c = var2.av();
            super.i = var2.ae();
            super.a = var2.ae();
            this.v = var2.av();
            this.u = var2.av();
            super.b = Math.min(super.b, 4);
            super.e = new short[1][64][64];
            super.x = new short[super.b][64][64];
            super.p = new byte[super.b][64][64];
            super.g = new byte[super.b][64][64];
            super.n = new am[super.b][64][64][];
            var3 = var1.av();
            if (var3 != ax.q.i) {
                throw new IllegalStateException("");
            } else {
                int var4 = var1.av();
                int var5 = var1.av();
                int var6 = var1.av();
                int var7 = var1.av();
                if (var4 == super.i && var5 == super.a && var6 == this.v && var7 == this.u) {
                    for (int var8 = 0; var8 < 8; ++var8) {
                        for (int var9 = 0; var9 < 8; ++var9) {
                            this.b(var8 + this.v * 8, var9 + this.u * 8, var1);
                        }
                    }

                } else {
                    throw new IllegalStateException("");
                }
            }
        }
    }

    @ObfuscatedName("q")
    boolean q(int var1, int var2) {
        if (var1 < this.v * 8) {
            return false;
        } else if (var2 < this.u * 8) {
            return false;
        } else if (var1 >= this.v * 8 + 8) {
            return false;
        } else {
            return var2 < this.u * 8 + 8;
        }
    }

    @ObfuscatedName("i")
    int i() {
        return this.o;
    }

    @ObfuscatedName("a")
    int a() {
        return this.c;
    }

    @ObfuscatedName("l")
    int l() {
        return this.v;
    }

    @ObfuscatedName("am")
    int am() {
        return this.u;
    }

    public boolean equals(Object var1) {
        if (!(var1 instanceof aa)) {
            return false;
        } else {
            aa var2 = (aa) var1;
            if (super.i == var2.i && super.a == var2.a) {
                return var2.v == this.v && var2.u == this.u;
            } else {
                return false;
            }
        }
    }

    public int hashCode() {
        return super.i | super.a << 8 | this.v << 16 | this.u << 24;
    }

    @ObfuscatedName("t")
    public static boolean t() {
        ClassStructureNode var0 = (ClassStructureNode) lm.t.b();
        return var0 != null;
    }

    @ObfuscatedName("e")
    static final int e(int var0, int var1, int var2) {
        int var3 = var0 / var2;
        int var4 = var0 & var2 - 1;
        int var5 = var1 / var2;
        int var6 = var1 & var2 - 1;
        int var7 = z.p(var3, var5);
        int var8 = z.p(var3 + 1, var5);
        int var9 = z.p(var3, var5 + 1);
        int var10 = z.p(var3 + 1, var5 + 1);
        int var11 = s.x(var7, var8, var4, var2);
        int var12 = s.x(var9, var10, var4, var2);
        return s.x(var11, var12, var6, var2);
    }
}
