import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fu")
public class fu extends fr {

    @ObfuscatedName("t")
    long t = System.nanoTime();

    @ObfuscatedName("t")
    public void t() {
        this.t = System.nanoTime();
    }

    @ObfuscatedName("q")
    public int q(int var1, int var2) {
        long var3 = (long) var2 * 1000000L;
        long var5 = this.t - System.nanoTime();
        if (var5 < var3) {
            var5 = var3;
        }

        cx.t(var5 / 1000000L);
        long var7 = System.nanoTime();

        int var9;
        for (var9 = 0; var9 < 10 && (var9 < 1 || this.t < var7); this.t += 1000000L * (long) var1) {
            ++var9;
        }

        if (this.t < var7) {
            this.t = var7;
        }

        return var9;
    }
}
