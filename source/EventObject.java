import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("eo")
public final class EventObject {

    @ObfuscatedName("t")
    int plane;

    @ObfuscatedName("q")
    int height;

    @ObfuscatedName("i")
    int regionX;

    @ObfuscatedName("a")
    int regionY;

    @ObfuscatedName("l")
    public Renderable model;

    @ObfuscatedName("b")
    int orientation;

    @ObfuscatedName("e")
    int x;

    @ObfuscatedName("x")
    int sizeX;

    @ObfuscatedName("p")
    int y;

    @ObfuscatedName("g")
    int sizeY;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    public int hash = 0;

    @ObfuscatedName("v")
    int placement = 0;

    @ObfuscatedName("x")
   static int x(int var0, RuneScript var1, boolean var2) {
      RTComponent var3;
      if (var0 >= 2000) {
         var0 -= 1000;
         var3 = Inflater.t(cs.e[--b.menuX]);
      } else {
         var3 = var2 ? ho.v : cs.c;
      }

      String var4 = cs.p[--ly.g];
      int[] var5 = null;
      if (var4.length() > 0 && var4.charAt(var4.length() - 1) == 'Y') {
         int var6 = cs.e[--b.menuX];
         if (var6 > 0) {
            for(var5 = new int[var6]; var6-- > 0; var5[var6] = cs.e[--b.menuX]) {
               ;
            }
         }

         var4 = var4.substring(0, var4.length() - 1);
      }

      Object[] var8 = new Object[var4.length() + 1];

      int var7;
      for(var7 = var8.length - 1; var7 >= 1; --var7) {
         if (var4.charAt(var7 - 1) == 's') {
            var8[var7] = cs.p[--ly.g];
         } else {
            var8[var7] = new Integer(cs.e[--b.menuX]);
         }
      }

      var7 = cs.e[--b.menuX];
      if (var7 != -1) {
         var8[0] = new Integer(var7);
      } else {
         var8 = null;
      }

      if (var0 == 1400) {
         var3.cp = var8;
      } else if (var0 == 1401) {
         var3.dp = var8;
      } else if (var0 == 1402) {
         var3.db = var8;
      } else if (var0 == 1403) {
         var3.da = var8;
      } else if (var0 == 1404) {
         var3.dj = var8;
      } else if (var0 == 1405) {
         var3.dh = var8;
      } else if (var0 == 1406) {
         var3.df = var8;
      } else if (var0 == 1407) {
         var3.dq = var8;
         var3.dt = var5;
      } else if (var0 == 1408) {
         var3.dd = var8;
      } else if (var0 == 1409) {
         var3.du = var8;
      } else if (var0 == 1410) {
         var3.dc = var8;
      } else if (var0 == 1411) {
         var3.ck = var8;
      } else if (var0 == 1412) {
         var3.dr = var8;
      } else if (var0 == 1414) {
         var3.dy = var8;
         var3.dn = var5;
      } else if (var0 == 1415) {
         var3.do = var8;
         var3.dw = var5;
      } else if (var0 == 1416) {
         var3.dl = var8;
      } else if (var0 == 1417) {
         var3.dk = var8;
      } else if (var0 == 1418) {
         var3.de = var8;
      } else if (var0 == 1419) {
         var3.dg = var8;
      } else if (var0 == 1420) {
         var3.dx = var8;
      } else if (var0 == 1421) {
         var3.di = var8;
      } else if (var0 == 1422) {
         var3.dv = var8;
      } else if (var0 == 1423) {
         var3.dz = var8;
      } else if (var0 == 1424) {
         var3.ds = var8;
      } else if (var0 == 1425) {
         var3.eb = var8;
      } else if (var0 == 1426) {
         var3.ek = var8;
      } else {
         if (var0 != 1427) {
            return 2;
         }

         var3.dm = var8;
      }

      var3.ca = true;
      return 1;
   }
}
