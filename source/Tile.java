import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("eb")
public final class Tile extends Node {

    @ObfuscatedName("fc")
    static Sprite[] fc;

    @ObfuscatedName("t")
    int plane;

    @ObfuscatedName("q")
    int regionX;

    @ObfuscatedName("i")
    int regionY;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    TileModel tileModel;

    @ObfuscatedName("b")
    TilePaint tilePaint;

    @ObfuscatedName("e")
    BoundaryObject boundaryObject;

    @ObfuscatedName("x")
    WallObject wallObject;

    @ObfuscatedName("p")
    FloorObject floorObject;

    @ObfuscatedName("g")
    ItemPile itemPile;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    EventObject[] eventObjects = new EventObject[5];

    @ObfuscatedName("c")
    int[] boundaryRenderBits = new int[5];

    @ObfuscatedName("v")
    int v = 0;

    @ObfuscatedName("u")
    int u;

    @ObfuscatedName("j")
    boolean j;

    @ObfuscatedName("k")
    boolean k;

    @ObfuscatedName("z")
    boolean z;

    @ObfuscatedName("w")
    int w;

    @ObfuscatedName("s")
    int s;

    @ObfuscatedName("d")
    int d;

    @ObfuscatedName("f")
    int f;

    @ObfuscatedName("r")
    Tile baseTile;

    Tile(int var1, int var2, int var3) {
        this.a = this.plane = var1;
        this.regionX = var2;
        this.regionY = var3;
    }

    @ObfuscatedName("a")
    static void a() {
        if (ci.username == null || ci.username.length() <= 0) {
            if (al.qp.b != null) {
                ci.username = al.qp.b;
                ci.aw = true;
            } else {
                ci.aw = false;
            }

        }
    }

    @ObfuscatedName("av")
    static int av(int var0, RuneScript var1, boolean var2) {
        int var3;
        if (var0 == 6600) {
            var3 = kt.ii;
            int var9 = (az.il.regionX >> 7) + an.regionBaseX;
            int var5 = (az.il.regionY >> 7) + PlayerComposite.ep;
            ex.ep().k(var3, var9, var5, true);
            return 1;
        } else {
            az var11;
            if (var0 == 6601) {
                var3 = cs.e[--b.menuX];
                String var16 = "";
                var11 = ex.ep().ar(var3);
                if (var11 != null) {
                    var16 = var11.c();
                }

                cs.p[++ly.g - 1] = var16;
                return 1;
            } else if (var0 == 6602) {
                var3 = cs.e[--b.menuX];
                ex.ep().z(var3);
                return 1;
            } else if (var0 == 6603) {
                cs.e[++b.menuX - 1] = ex.ep().ah();
                return 1;
            } else if (var0 == 6604) {
                var3 = cs.e[--b.menuX];
                ex.ep().am(var3);
                return 1;
            } else if (var0 == 6605) {
                cs.e[++b.menuX - 1] = ex.ep().ax() ? 1 : 0;
                return 1;
            } else {
                ik var15;
                if (var0 == 6606) {
                    var15 = new ik(cs.e[--b.menuX]);
                    ex.ep().an(var15.q, var15.i);
                    return 1;
                } else if (var0 == 6607) {
                    var15 = new ik(cs.e[--b.menuX]);
                    ex.ep().ai(var15.q, var15.i);
                    return 1;
                } else if (var0 == 6608) {
                    var15 = new ik(cs.e[--b.menuX]);
                    ex.ep().al(var15.t, var15.q, var15.i);
                    return 1;
                } else if (var0 == 6609) {
                    var15 = new ik(cs.e[--b.menuX]);
                    ex.ep().at(var15.t, var15.q, var15.i);
                    return 1;
                } else if (var0 == 6610) {
                    cs.e[++b.menuX - 1] = ex.ep().ag();
                    cs.e[++b.menuX - 1] = ex.ep().as();
                    return 1;
                } else {
                    az var13;
                    if (var0 == 6611) {
                        var3 = cs.e[--b.menuX];
                        var13 = ex.ep().ar(var3);
                        if (var13 == null) {
                            cs.e[++b.menuX - 1] = 0;
                        } else {
                            cs.e[++b.menuX - 1] = var13.h().q();
                        }

                        return 1;
                    } else if (var0 == 6612) {
                        var3 = cs.e[--b.menuX];
                        var13 = ex.ep().ar(var3);
                        if (var13 == null) {
                            cs.e[++b.menuX - 1] = 0;
                            cs.e[++b.menuX - 1] = 0;
                        } else {
                            cs.e[++b.menuX - 1] = (var13.w() - var13.z() + 1) * 64;
                            cs.e[++b.menuX - 1] = (var13.d() - var13.s() + 1) * 64;
                        }

                        return 1;
                    } else if (var0 == 6613) {
                        var3 = cs.e[--b.menuX];
                        var13 = ex.ep().ar(var3);
                        if (var13 == null) {
                            cs.e[++b.menuX - 1] = 0;
                            cs.e[++b.menuX - 1] = 0;
                            cs.e[++b.menuX - 1] = 0;
                            cs.e[++b.menuX - 1] = 0;
                        } else {
                            cs.e[++b.menuX - 1] = var13.z() * 64;
                            cs.e[++b.menuX - 1] = var13.s() * 64;
                            cs.e[++b.menuX - 1] = var13.w() * 64 + 64 - 1;
                            cs.e[++b.menuX - 1] = var13.d() * 64 + 64 - 1;
                        }

                        return 1;
                    } else if (var0 == 6614) {
                        var3 = cs.e[--b.menuX];
                        var13 = ex.ep().ar(var3);
                        if (var13 == null) {
                            cs.e[++b.menuX - 1] = -1;
                        } else {
                            cs.e[++b.menuX - 1] = var13.k();
                        }

                        return 1;
                    } else if (var0 == 6615) {
                        var15 = ex.ep().aw();
                        if (var15 == null) {
                            cs.e[++b.menuX - 1] = -1;
                            cs.e[++b.menuX - 1] = -1;
                        } else {
                            cs.e[++b.menuX - 1] = var15.q;
                            cs.e[++b.menuX - 1] = var15.i;
                        }

                        return 1;
                    } else if (var0 == 6616) {
                        cs.e[++b.menuX - 1] = ex.ep().w();
                        return 1;
                    } else if (var0 == 6617) {
                        var15 = new ik(cs.e[--b.menuX]);
                        var13 = ex.ep().s();
                        if (var13 == null) {
                            cs.e[++b.menuX - 1] = -1;
                            cs.e[++b.menuX - 1] = -1;
                            return 1;
                        } else {
                            int[] var14 = var13.l(var15.t, var15.q, var15.i);
                            if (var14 == null) {
                                cs.e[++b.menuX - 1] = -1;
                                cs.e[++b.menuX - 1] = -1;
                            } else {
                                cs.e[++b.menuX - 1] = var14[0];
                                cs.e[++b.menuX - 1] = var14[1];
                            }

                            return 1;
                        }
                    } else {
                        ik var7;
                        if (var0 == 6618) {
                            var15 = new ik(cs.e[--b.menuX]);
                            var13 = ex.ep().s();
                            if (var13 == null) {
                                cs.e[++b.menuX - 1] = -1;
                                cs.e[++b.menuX - 1] = -1;
                                return 1;
                            } else {
                                var7 = var13.b(var15.q, var15.i);
                                if (var7 == null) {
                                    cs.e[++b.menuX - 1] = -1;
                                } else {
                                    cs.e[++b.menuX - 1] = var7.q();
                                }

                                return 1;
                            }
                        } else {
                            ik var12;
                            if (var0 == 6619) {
                                b.menuX -= 2;
                                var3 = cs.e[b.menuX];
                                var12 = new ik(cs.e[b.menuX + 1]);
                                ai.ae(var3, var12, false);
                                return 1;
                            } else if (var0 == 6620) {
                                b.menuX -= 2;
                                var3 = cs.e[b.menuX];
                                var12 = new ik(cs.e[b.menuX + 1]);
                                ai.ae(var3, var12, true);
                                return 1;
                            } else if (var0 == 6621) {
                                b.menuX -= 2;
                                var3 = cs.e[b.menuX];
                                var12 = new ik(cs.e[b.menuX + 1]);
                                var11 = ex.ep().ar(var3);
                                if (var11 == null) {
                                    cs.e[++b.menuX - 1] = 0;
                                    return 1;
                                } else {
                                    cs.e[++b.menuX - 1] = var11.i(var12.t, var12.q, var12.i) ? 1 : 0;
                                    return 1;
                                }
                            } else if (var0 == 6622) {
                                cs.e[++b.menuX - 1] = ex.ep().aq();
                                cs.e[++b.menuX - 1] = ex.ep().aa();
                                return 1;
                            } else if (var0 == 6623) {
                                var15 = new ik(cs.e[--b.menuX]);
                                var13 = ex.ep().u(var15.t, var15.q, var15.i);
                                if (var13 == null) {
                                    cs.e[++b.menuX - 1] = -1;
                                } else {
                                    cs.e[++b.menuX - 1] = var13.x();
                                }

                                return 1;
                            } else if (var0 == 6624) {
                                ex.ep().af(cs.e[--b.menuX]);
                                return 1;
                            } else if (var0 == 6625) {
                                ex.ep().ak();
                                return 1;
                            } else if (var0 == 6626) {
                                ex.ep().ab(cs.e[--b.menuX]);
                                return 1;
                            } else if (var0 == 6627) {
                                ex.ep().ac();
                                return 1;
                            } else {
                                boolean var10;
                                if (var0 == 6628) {
                                    var10 = cs.e[--b.menuX] == 1;
                                    ex.ep().ad(var10);
                                    return 1;
                                } else if (var0 == 6629) {
                                    var3 = cs.e[--b.menuX];
                                    ex.ep().bg(var3);
                                    return 1;
                                } else if (var0 == 6630) {
                                    var3 = cs.e[--b.menuX];
                                    ex.ep().br(var3);
                                    return 1;
                                } else if (var0 == 6631) {
                                    ex.ep().ba();
                                    return 1;
                                } else if (var0 == 6632) {
                                    var10 = cs.e[--b.menuX] == 1;
                                    ex.ep().bk(var10);
                                    return 1;
                                } else {
                                    boolean var4;
                                    if (var0 == 6633) {
                                        b.menuX -= 2;
                                        var3 = cs.e[b.menuX];
                                        var4 = cs.e[b.menuX + 1] == 1;
                                        ex.ep().be(var3, var4);
                                        return 1;
                                    } else if (var0 == 6634) {
                                        b.menuX -= 2;
                                        var3 = cs.e[b.menuX];
                                        var4 = cs.e[b.menuX + 1] == 1;
                                        ex.ep().bc(var3, var4);
                                        return 1;
                                    } else if (var0 == 6635) {
                                        cs.e[++b.menuX - 1] = ex.ep().bm() ? 1 : 0;
                                        return 1;
                                    } else if (var0 == 6636) {
                                        var3 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = ex.ep().bh(var3) ? 1 : 0;
                                        return 1;
                                    } else if (var0 == 6637) {
                                        var3 = cs.e[--b.menuX];
                                        cs.e[++b.menuX - 1] = ex.ep().bs(var3) ? 1 : 0;
                                        return 1;
                                    } else if (var0 == 6638) {
                                        b.menuX -= 2;
                                        var3 = cs.e[b.menuX];
                                        var12 = new ik(cs.e[b.menuX + 1]);
                                        var7 = ex.ep().by(var3, var12);
                                        if (var7 == null) {
                                            cs.e[++b.menuX - 1] = -1;
                                        } else {
                                            cs.e[++b.menuX - 1] = var7.q();
                                        }

                                        return 1;
                                    } else {
                                        al var8;
                                        if (var0 == 6639) {
                                            var8 = ex.ep().bb();
                                            if (var8 == null) {
                                                cs.e[++b.menuX - 1] = -1;
                                                cs.e[++b.menuX - 1] = -1;
                                            } else {
                                                cs.e[++b.menuX - 1] = var8.t;
                                                cs.e[++b.menuX - 1] = var8.q.q();
                                            }

                                            return 1;
                                        } else if (var0 == 6640) {
                                            var8 = ex.ep().bq();
                                            if (var8 == null) {
                                                cs.e[++b.menuX - 1] = -1;
                                                cs.e[++b.menuX - 1] = -1;
                                            } else {
                                                cs.e[++b.menuX - 1] = var8.t;
                                                cs.e[++b.menuX - 1] = var8.q.q();
                                            }

                                            return 1;
                                        } else {
                                            jj var6;
                                            if (var0 == 6693) {
                                                var3 = cs.e[--b.menuX];
                                                var6 = jj.q[var3];
                                                if (var6.x == null) {
                                                    cs.p[++ly.g - 1] = "";
                                                } else {
                                                    cs.p[++ly.g - 1] = var6.x;
                                                }

                                                return 1;
                                            } else if (var0 == 6694) {
                                                var3 = cs.e[--b.menuX];
                                                var6 = jj.q[var3];
                                                cs.e[++b.menuX - 1] = var6.g;
                                                return 1;
                                            } else if (var0 == 6695) {
                                                var3 = cs.e[--b.menuX];
                                                var6 = jj.q[var3];
                                                if (var6 == null) {
                                                    cs.e[++b.menuX - 1] = -1;
                                                } else {
                                                    cs.e[++b.menuX - 1] = var6.r;
                                                }

                                                return 1;
                                            } else if (var0 == 6696) {
                                                var3 = cs.e[--b.menuX];
                                                var6 = jj.q[var3];
                                                if (var6 == null) {
                                                    cs.e[++b.menuX - 1] = -1;
                                                } else {
                                                    cs.e[++b.menuX - 1] = var6.b;
                                                }

                                                return 1;
                                            } else if (var0 == 6697) {
                                                cs.e[++b.menuX - 1] = v.k.t;
                                                return 1;
                                            } else if (var0 == 6698) {
                                                cs.e[++b.menuX - 1] = v.k.q.q();
                                                return 1;
                                            } else if (var0 == 6699) {
                                                cs.e[++b.menuX - 1] = v.k.i.q();
                                                return 1;
                                            } else {
                                                return 2;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
