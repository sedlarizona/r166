import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dj")
public class AudioEnvelope {

    @ObfuscatedName("t")
    int frameCount = 2;

    @ObfuscatedName("q")
    int[] durations = new int[2];

    @ObfuscatedName("i")
    int[] peaks = new int[2];

    @ObfuscatedName("a")
    int start;

    @ObfuscatedName("l")
    int end;

    @ObfuscatedName("b")
    int form;

    @ObfuscatedName("e")
    int critical;

    @ObfuscatedName("p")
    int frameIndex;

    @ObfuscatedName("g")
    int step;

    @ObfuscatedName("n")
    int amplitude;

    @ObfuscatedName("o")
    int ticks;

    AudioEnvelope() {
        this.durations[0] = 0;
        this.durations[1] = 65535;
        this.peaks[0] = 0;
        this.peaks[1] = 65535;
    }

    @ObfuscatedName("t")
    final void t(ByteBuffer var1) {
        this.form = var1.av();
        this.start = var1.ap();
        this.end = var1.ap();
        this.q(var1);
    }

    @ObfuscatedName("q")
    final void q(ByteBuffer var1) {
        this.frameCount = var1.av();
        this.durations = new int[this.frameCount];
        this.peaks = new int[this.frameCount];

        for (int var2 = 0; var2 < this.frameCount; ++var2) {
            this.durations[var2] = var1.ae();
            this.peaks[var2] = var1.ae();
        }

    }

    @ObfuscatedName("i")
    final void i() {
        this.critical = 0;
        this.frameIndex = 0;
        this.step = 0;
        this.amplitude = 0;
        this.ticks = 0;
    }

    @ObfuscatedName("a")
    final int a(int var1) {
        if (this.ticks >= this.critical) {
            this.amplitude = this.peaks[this.frameIndex++] << 15;
            if (this.frameIndex >= this.frameCount) {
                this.frameIndex = this.frameCount - 1;
            }

            this.critical = (int) ((double) this.durations[this.frameIndex] / 65536.0D * (double) var1);
            if (this.critical > this.ticks) {
                this.step = ((this.peaks[this.frameIndex] << 15) - this.amplitude) / (this.critical - this.ticks);
            }
        }

        this.amplitude += this.step;
        ++this.ticks;
        return this.amplitude - this.step >> 15;
    }
}
