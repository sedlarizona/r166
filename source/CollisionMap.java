import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fa")
public class CollisionMap {

    @ObfuscatedName("ap")
    int widthOffset = 0;

    @ObfuscatedName("ah")
    int heightOffset = 0;

    @ObfuscatedName("au")
    int width;

    @ObfuscatedName("ax")
    int height;

    @ObfuscatedName("ar")
    public int[][] flags;

    public CollisionMap(int var1, int var2) {
        this.width = var1;
        this.height = var2;
        this.flags = new int[this.width][this.height];
        this.t();
    }

    @ObfuscatedName("t")
    public void t() {
        for (int var1 = 0; var1 < this.width; ++var1) {
            for (int var2 = 0; var2 < this.height; ++var2) {
                if (var1 != 0 && var2 != 0 && var1 < this.width - 5 && var2 < this.height - 5) {
                    this.flags[var1][var2] = 16777216;
                } else {
                    this.flags[var1][var2] = 16777215;
                }
            }
        }

    }

    @ObfuscatedName("q")
    public void q(int var1, int var2, int var3, int var4, boolean var5) {
        var1 -= this.widthOffset;
        var2 -= this.heightOffset;
        if (var3 == 0) {
            if (var4 == 0) {
                this.b(var1, var2, 128);
                this.b(var1 - 1, var2, 8);
            }

            if (var4 == 1) {
                this.b(var1, var2, 2);
                this.b(var1, var2 + 1, 32);
            }

            if (var4 == 2) {
                this.b(var1, var2, 8);
                this.b(var1 + 1, var2, 128);
            }

            if (var4 == 3) {
                this.b(var1, var2, 32);
                this.b(var1, var2 - 1, 2);
            }
        }

        if (var3 == 1 || var3 == 3) {
            if (var4 == 0) {
                this.b(var1, var2, 1);
                this.b(var1 - 1, var2 + 1, 16);
            }

            if (var4 == 1) {
                this.b(var1, var2, 4);
                this.b(var1 + 1, var2 + 1, 64);
            }

            if (var4 == 2) {
                this.b(var1, var2, 16);
                this.b(var1 + 1, var2 - 1, 1);
            }

            if (var4 == 3) {
                this.b(var1, var2, 64);
                this.b(var1 - 1, var2 - 1, 4);
            }
        }

        if (var3 == 2) {
            if (var4 == 0) {
                this.b(var1, var2, 130);
                this.b(var1 - 1, var2, 8);
                this.b(var1, var2 + 1, 32);
            }

            if (var4 == 1) {
                this.b(var1, var2, 10);
                this.b(var1, var2 + 1, 32);
                this.b(var1 + 1, var2, 128);
            }

            if (var4 == 2) {
                this.b(var1, var2, 40);
                this.b(var1 + 1, var2, 128);
                this.b(var1, var2 - 1, 2);
            }

            if (var4 == 3) {
                this.b(var1, var2, 160);
                this.b(var1, var2 - 1, 2);
                this.b(var1 - 1, var2, 8);
            }
        }

        if (var5) {
            if (var3 == 0) {
                if (var4 == 0) {
                    this.b(var1, var2, 65536);
                    this.b(var1 - 1, var2, 4096);
                }

                if (var4 == 1) {
                    this.b(var1, var2, 1024);
                    this.b(var1, var2 + 1, 16384);
                }

                if (var4 == 2) {
                    this.b(var1, var2, 4096);
                    this.b(var1 + 1, var2, 65536);
                }

                if (var4 == 3) {
                    this.b(var1, var2, 16384);
                    this.b(var1, var2 - 1, 1024);
                }
            }

            if (var3 == 1 || var3 == 3) {
                if (var4 == 0) {
                    this.b(var1, var2, 512);
                    this.b(var1 - 1, var2 + 1, 8192);
                }

                if (var4 == 1) {
                    this.b(var1, var2, 2048);
                    this.b(var1 + 1, var2 + 1, 32768);
                }

                if (var4 == 2) {
                    this.b(var1, var2, 8192);
                    this.b(var1 + 1, var2 - 1, 512);
                }

                if (var4 == 3) {
                    this.b(var1, var2, 32768);
                    this.b(var1 - 1, var2 - 1, 2048);
                }
            }

            if (var3 == 2) {
                if (var4 == 0) {
                    this.b(var1, var2, 66560);
                    this.b(var1 - 1, var2, 4096);
                    this.b(var1, var2 + 1, 16384);
                }

                if (var4 == 1) {
                    this.b(var1, var2, 5120);
                    this.b(var1, var2 + 1, 16384);
                    this.b(var1 + 1, var2, 65536);
                }

                if (var4 == 2) {
                    this.b(var1, var2, 20480);
                    this.b(var1 + 1, var2, 65536);
                    this.b(var1, var2 - 1, 1024);
                }

                if (var4 == 3) {
                    this.b(var1, var2, 81920);
                    this.b(var1, var2 - 1, 1024);
                    this.b(var1 - 1, var2, 4096);
                }
            }
        }

    }

    @ObfuscatedName("i")
    public void i(int var1, int var2, int var3, int var4, boolean var5) {
        int var6 = 256;
        if (var5) {
            var6 += 131072;
        }

        var1 -= this.widthOffset;
        var2 -= this.heightOffset;

        for (int var7 = var1; var7 < var3 + var1; ++var7) {
            if (var7 >= 0 && var7 < this.width) {
                for (int var8 = var2; var8 < var2 + var4; ++var8) {
                    if (var8 >= 0 && var8 < this.height) {
                        this.b(var7, var8, var6);
                    }
                }
            }
        }

    }

    @ObfuscatedName("a")
    public void a(int var1, int var2) {
        var1 -= this.widthOffset;
        var2 -= this.heightOffset;
        this.flags[var1][var2] |= 2097152;
    }

    @ObfuscatedName("l")
    public void l(int var1, int var2) {
        var1 -= this.widthOffset;
        var2 -= this.heightOffset;
        this.flags[var1][var2] |= 262144;
    }

    @ObfuscatedName("b")
    void b(int var1, int var2, int var3) {
        this.flags[var1][var2] |= var3;
    }

    @ObfuscatedName("e")
    public void e(int var1, int var2, int var3, int var4, boolean var5) {
        var1 -= this.widthOffset;
        var2 -= this.heightOffset;
        if (var3 == 0) {
            if (var4 == 0) {
                this.p(var1, var2, 128);
                this.p(var1 - 1, var2, 8);
            }

            if (var4 == 1) {
                this.p(var1, var2, 2);
                this.p(var1, var2 + 1, 32);
            }

            if (var4 == 2) {
                this.p(var1, var2, 8);
                this.p(var1 + 1, var2, 128);
            }

            if (var4 == 3) {
                this.p(var1, var2, 32);
                this.p(var1, var2 - 1, 2);
            }
        }

        if (var3 == 1 || var3 == 3) {
            if (var4 == 0) {
                this.p(var1, var2, 1);
                this.p(var1 - 1, var2 + 1, 16);
            }

            if (var4 == 1) {
                this.p(var1, var2, 4);
                this.p(var1 + 1, var2 + 1, 64);
            }

            if (var4 == 2) {
                this.p(var1, var2, 16);
                this.p(var1 + 1, var2 - 1, 1);
            }

            if (var4 == 3) {
                this.p(var1, var2, 64);
                this.p(var1 - 1, var2 - 1, 4);
            }
        }

        if (var3 == 2) {
            if (var4 == 0) {
                this.p(var1, var2, 130);
                this.p(var1 - 1, var2, 8);
                this.p(var1, var2 + 1, 32);
            }

            if (var4 == 1) {
                this.p(var1, var2, 10);
                this.p(var1, var2 + 1, 32);
                this.p(var1 + 1, var2, 128);
            }

            if (var4 == 2) {
                this.p(var1, var2, 40);
                this.p(var1 + 1, var2, 128);
                this.p(var1, var2 - 1, 2);
            }

            if (var4 == 3) {
                this.p(var1, var2, 160);
                this.p(var1, var2 - 1, 2);
                this.p(var1 - 1, var2, 8);
            }
        }

        if (var5) {
            if (var3 == 0) {
                if (var4 == 0) {
                    this.p(var1, var2, 65536);
                    this.p(var1 - 1, var2, 4096);
                }

                if (var4 == 1) {
                    this.p(var1, var2, 1024);
                    this.p(var1, var2 + 1, 16384);
                }

                if (var4 == 2) {
                    this.p(var1, var2, 4096);
                    this.p(var1 + 1, var2, 65536);
                }

                if (var4 == 3) {
                    this.p(var1, var2, 16384);
                    this.p(var1, var2 - 1, 1024);
                }
            }

            if (var3 == 1 || var3 == 3) {
                if (var4 == 0) {
                    this.p(var1, var2, 512);
                    this.p(var1 - 1, var2 + 1, 8192);
                }

                if (var4 == 1) {
                    this.p(var1, var2, 2048);
                    this.p(var1 + 1, var2 + 1, 32768);
                }

                if (var4 == 2) {
                    this.p(var1, var2, 8192);
                    this.p(var1 + 1, var2 - 1, 512);
                }

                if (var4 == 3) {
                    this.p(var1, var2, 32768);
                    this.p(var1 - 1, var2 - 1, 2048);
                }
            }

            if (var3 == 2) {
                if (var4 == 0) {
                    this.p(var1, var2, 66560);
                    this.p(var1 - 1, var2, 4096);
                    this.p(var1, var2 + 1, 16384);
                }

                if (var4 == 1) {
                    this.p(var1, var2, 5120);
                    this.p(var1, var2 + 1, 16384);
                    this.p(var1 + 1, var2, 65536);
                }

                if (var4 == 2) {
                    this.p(var1, var2, 20480);
                    this.p(var1 + 1, var2, 65536);
                    this.p(var1, var2 - 1, 1024);
                }

                if (var4 == 3) {
                    this.p(var1, var2, 81920);
                    this.p(var1, var2 - 1, 1024);
                    this.p(var1 - 1, var2, 4096);
                }
            }
        }

    }

    @ObfuscatedName("x")
    public void x(int var1, int var2, int var3, int var4, int var5, boolean var6) {
        int var7 = 256;
        if (var6) {
            var7 += 131072;
        }

        var1 -= this.widthOffset;
        var2 -= this.heightOffset;
        int var8;
        if (var5 == 1 || var5 == 3) {
            var8 = var3;
            var3 = var4;
            var4 = var8;
        }

        for (var8 = var1; var8 < var3 + var1; ++var8) {
            if (var8 >= 0 && var8 < this.width) {
                for (int var9 = var2; var9 < var2 + var4; ++var9) {
                    if (var9 >= 0 && var9 < this.height) {
                        this.p(var8, var9, var7);
                    }
                }
            }
        }

    }

    @ObfuscatedName("p")
    void p(int var1, int var2, int var3) {
        this.flags[var1][var2] &= ~var3;
    }

    @ObfuscatedName("o")
    public void o(int var1, int var2) {
        var1 -= this.widthOffset;
        var2 -= this.heightOffset;
        this.flags[var1][var2] &= -262145;
    }

    @ObfuscatedName("q")
    public static RTComponent q(int var0, int var1) {
        RTComponent var2 = Inflater.t(var0);
        if (var1 == -1) {
            return var2;
        } else {
            return var2 != null && var2.cs2components != null && var1 < var2.cs2components.length ? var2.cs2components[var1]
                    : null;
        }
    }
}
