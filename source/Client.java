import java.applet.Applet;
import java.io.File;
import java.io.IOException;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;
import netscape.javascript.JSObject;

@ObfuscatedName("client")
public final class Client extends GameEngine implements kz {

    @ObfuscatedName("nq")
    static int nq;

    @ObfuscatedName("nm")
    static int nm;

    @ObfuscatedName("ng")
    static long ng;

    @ObfuscatedName("nc")
    static boolean[] outdatedInterfaces;

    @ObfuscatedName("nj")
    static boolean[] nj;

    @ObfuscatedName("nt")
    static boolean[] nt;

    @ObfuscatedName("mn")
    static int mn;

    @ObfuscatedName("ny")
    static boolean ny;

    @ObfuscatedName("rh")
    public static int rh;

    @ObfuscatedName("ms")
    static int ms;

    @ObfuscatedName("oe")
    static boolean oe;

    @ObfuscatedName("mf")
    static HashTable mf;

    @ObfuscatedName("nu")
    static int nu;

    @ObfuscatedName("nw")
    static int[] interfaceXPositions;

    @ObfuscatedName("nk")
    static int[] interfaceWidths;

    @ObfuscatedName("mw")
    static int mw;

    @ObfuscatedName("nl")
    static int[] interfaceHeights;

    @ObfuscatedName("ni")
    static int[] interfaceYPositions;

    @ObfuscatedName("rp")
    static int[] rp;

    @ObfuscatedName("re")
    static int[] re;

    @ObfuscatedName("rn")
    static final bv rn;

    @ObfuscatedName("me")
    static Deque me;

    @ObfuscatedName("mv")
    static int mv;

    @ObfuscatedName("rk")
    static ArrayList rk;

    @ObfuscatedName("rm")
    static int rm;

    @ObfuscatedName("pa")
    static int pa;

    @ObfuscatedName("px")
    static boolean px;

    @ObfuscatedName("pf")
    static boolean[] pf;

    @ObfuscatedName("nb")
    static int nb;

    @ObfuscatedName("np")
    static int np;

    @ObfuscatedName("mk")
    static int[] mk;

    @ObfuscatedName("ma")
    static int ma;

    @ObfuscatedName("ox")
    static int ox;

    @ObfuscatedName("qn")
    static int qn;

    @ObfuscatedName("os")
    static int os;

    @ObfuscatedName("oz")
    static long oz;

    @ObfuscatedName("qv")
    static PlayerComposite qv;

    @ObfuscatedName("qx")
    static int qx;

    @ObfuscatedName("oj")
    static int oj;

    @ObfuscatedName("py")
    static int audioEffectCount;

    @ObfuscatedName("po")
    static int[] po;

    @ObfuscatedName("pe")
    static int[] pe;

    @ObfuscatedName("pt")
    static int[] pt;

    @ObfuscatedName("ph")
    static AudioTrack[] audioTracks;

    @ObfuscatedName("ob")
    static int destinationX;

    @ObfuscatedName("pl")
    static int[] pl;

    @ObfuscatedName("oa")
    static int destinationY;

    @ObfuscatedName("mg")
    static int[] mg;

    @ObfuscatedName("oo")
    static int oo;

    @ObfuscatedName("my")
    static int my;

    @ObfuscatedName("ow")
    static int ow;

    @ObfuscatedName("mu")
    static int[] mu;

    @ObfuscatedName("oc")
    static int oc;

    @ObfuscatedName("mz")
    static int mz;

    @ObfuscatedName("nx")
    static long[] nx;

    @ObfuscatedName("ou")
    static Sprite[] ou;

    @ObfuscatedName("nd")
    static int nd;

    @ObfuscatedName("ov")
    static int[] ov;

    @ObfuscatedName("od")
    static int[] od;

    @ObfuscatedName("qk")
    static short qk;

    @ObfuscatedName("qw")
    static short qw;

    @ObfuscatedName("og")
    static int og;

    @ObfuscatedName("qf")
    static short qf;

    @ObfuscatedName("qa")
    static short qa;

    @ObfuscatedName("qd")
    static short qd;

    @ObfuscatedName("qu")
    static short qu;

    @ObfuscatedName("qr")
    static short qr;

    @ObfuscatedName("qm")
    static short qm;

    @ObfuscatedName("qc")
    static int viewportScale;

    @ObfuscatedName("pb")
    static int[] pb;

    @ObfuscatedName("qg")
    static int viewportWidth;

    @ObfuscatedName("qh")
    static int[] qh;

    @ObfuscatedName("ql")
    static int[] currentSkillLevels;

    @ObfuscatedName("qs")
    static int[] qs;

    @ObfuscatedName("mc")
    static int mc;

    @ObfuscatedName("ol")
    static int[] ol;

    @ObfuscatedName("mh")
    static Deque mh;

    @ObfuscatedName("or")
    static int[] or;

    @ObfuscatedName("ml")
    static Deque ml;

    @ObfuscatedName("qb")
    static int viewportHeight;

    @ObfuscatedName("qq")
    static int qq;

    @ObfuscatedName("qo")
    static int screenState;

    @ObfuscatedName("mi")
    static int mi;

    @ObfuscatedName("qj")
    static GrandExchangeOffer[] exchangeOffers;

    @ObfuscatedName("mo")
    static int mo;

    @ObfuscatedName("mj")
    static int mj;

    @ObfuscatedName("nz")
    static String nz;

    @ObfuscatedName("qz")
    static bu qz;

    @ObfuscatedName("ne")
    static int[] ne;

    @ObfuscatedName("w")
    static CollisionMap[] collisionMaps;

    @ObfuscatedName("ad")
    static boolean ad = true;

    @ObfuscatedName("bg")
    public static int currentWorld = 1;

    @ObfuscatedName("br")
    static int br = 0;

    @ObfuscatedName("bk")
    static int playerWeight = 0;

    @ObfuscatedName("be")
    static in be;

    @ObfuscatedName("bm")
    public static boolean membersWorld = false;

    @ObfuscatedName("bh")
    static boolean bh = false;

    @ObfuscatedName("bs")
    static int bs = 0;

    @ObfuscatedName("bb")
    static int packet = 0;

    @ObfuscatedName("bq")
    static boolean bq = true;

    @ObfuscatedName("bz")
    static int bz = 0;

    @ObfuscatedName("bx")
    static long bx = 1L;

    @ObfuscatedName("bo")
    static int bo = -1;

    @ObfuscatedName("bv")
    static int bv = -1;

    @ObfuscatedName("bi")
    static int bi = -1;

    @ObfuscatedName("bu")
    static boolean bu = true;

    @ObfuscatedName("bl")
    static boolean bl = false;

    @ObfuscatedName("bw")
    static int mouseTrackerIdleTime = 0;

    @ObfuscatedName("bp")
    static int hintArrowType = 0;

    @ObfuscatedName("bd")
    static int hintNpcIndex = 0;

    @ObfuscatedName("cb")
    static int hintPlayerIndex = 0;

    @ObfuscatedName("cm")
    static int hintX = 0;

    @ObfuscatedName("cu")
    static int hintY = 0;

    @ObfuscatedName("cs")
    static int cs = 0;

    @ObfuscatedName("ct")
    static int ct = 0;

    @ObfuscatedName("cw")
    static int cw = 0;

    @ObfuscatedName("cr")
    static cq cr;

    @ObfuscatedName("co")
    static cq co;

    @ObfuscatedName("cd")
    static int cd;

    @ObfuscatedName("cq")
    static Task cq;

    @ObfuscatedName("cc")
    static int cc;

    @ObfuscatedName("ce")
    static int ce;

    @ObfuscatedName("cj")
    static ju cj;

    @ObfuscatedName("dr")
    static ju dr;

    @ObfuscatedName("dl")
    static int dl;

    @ObfuscatedName("df")
    static int df;

    @ObfuscatedName("dq")
    static int dq;

    @ObfuscatedName("dt")
    static int dt;

    @ObfuscatedName("dy")
    static int dy;

    @ObfuscatedName("dn")
    static ff dn;

    @ObfuscatedName("de")
    static byte[] de;

    @ObfuscatedName("dv")
    static Npc[] loadedNpcs;

    @ObfuscatedName("dz")
    static int dz;

    @ObfuscatedName("ds")
    static int[] npcIndices;

    @ObfuscatedName("dm")
    static int dm;

    @ObfuscatedName("eb")
    static int[] eb;

    @ObfuscatedName("ee")
    public static final cn ee;

    @ObfuscatedName("ei")
    static int ei;

    @ObfuscatedName("ed")
    static boolean ed;

    @ObfuscatedName("ew")
    static boolean ew;

    @ObfuscatedName("ey")
    static kh ey;

    @ObfuscatedName("eg")
    static kn eg;

    @ObfuscatedName("es")
    static HashMap es;

    @ObfuscatedName("ea")
    static int ea;

    @ObfuscatedName("em")
    static int em;

    @ObfuscatedName("er")
    static int er;

    @ObfuscatedName("eh")
    static int eh;

    @ObfuscatedName("ex")
    static int ex;

    @ObfuscatedName("fl")
    static boolean dynamicRegion;

    @ObfuscatedName("ft")
    static int[][][] dynamicRegionFlags;

    @ObfuscatedName("fb")
    static final int[] fb;

    @ObfuscatedName("fs")
    static int fs;

    @ObfuscatedName("fx")
    static int fx;

    @ObfuscatedName("fo")
    static int fo;

    @ObfuscatedName("gu")
    static int gu;

    @ObfuscatedName("ga")
    static int ga;

    @ObfuscatedName("gd")
    static boolean gd;

    @ObfuscatedName("gg")
    static int gg;

    @ObfuscatedName("gq")
    static int gq;

    @ObfuscatedName("gb")
    static int hintPlane;

    @ObfuscatedName("gc")
    static int gc;

    @ObfuscatedName("gf")
    static int gf;

    @ObfuscatedName("gt")
    static int gt;

    @ObfuscatedName("gr")
    static int gr;

    @ObfuscatedName("gn")
    static int gn;

    @ObfuscatedName("gl")
    static int gl;

    @ObfuscatedName("gx")
    static int gx;

    @ObfuscatedName("gj")
    static int gj;

    @ObfuscatedName("gm")
    static int gm;

    @ObfuscatedName("ht")
    static int ht;

    @ObfuscatedName("hq")
    static int hq;

    @ObfuscatedName("hm")
    static int hm;

    @ObfuscatedName("hz")
    static boolean hz;

    @ObfuscatedName("hj")
    static int hj;

    @ObfuscatedName("he")
    static boolean he;

    @ObfuscatedName("hp")
    static int hp;

    @ObfuscatedName("hr")
    static int hr;

    @ObfuscatedName("hx")
    static int hx;

    @ObfuscatedName("hf")
    static int[] hf;

    @ObfuscatedName("hh")
    static int[] hh;

    @ObfuscatedName("hw")
    static int[] hw;

    @ObfuscatedName("hn")
    static int[] hn;

    @ObfuscatedName("ha")
    static int[] ha;

    @ObfuscatedName("hg")
    static int[] hg;

    @ObfuscatedName("hc")
    static int[] hc;

    @ObfuscatedName("hs")
    static String[] hs;

    @ObfuscatedName("hl")
    static int[][] hl;

    @ObfuscatedName("hu")
    static int hu;

    @ObfuscatedName("hv")
    static int hv;

    @ObfuscatedName("hd")
    static int hd;

    @ObfuscatedName("hi")
    static int lastSelectedInterfaceUID;

    @ObfuscatedName("hk")
    static int lastSelectedComponentId;

    @ObfuscatedName("hb")
    static int hb;

    @ObfuscatedName("hy")
    static int mouseCrosshairState;

    @ObfuscatedName("ho")
    static boolean ho;

    @ObfuscatedName("iq")
    static int iq;

    @ObfuscatedName("iv")
    static int iv;

    @ObfuscatedName("is")
    static int is;

    @ObfuscatedName("io")
    static int io;

    @ObfuscatedName("ig")
    static int ig;

    @ObfuscatedName("ic")
    static int ic;

    @ObfuscatedName("it")
    static String lastSelectedInventoryItemName;

    @ObfuscatedName("iw")
    static boolean iw;

    @ObfuscatedName("ih")
    static int ih;

    @ObfuscatedName("ij")
    static int ij;

    @ObfuscatedName("ir")
    static boolean ir;

    @ObfuscatedName("ip")
    static Player[] loadedPlayers;

    @ObfuscatedName("iy")
    static int localPlayerIndex;

    @ObfuscatedName("im")
    static int im;

    @ObfuscatedName("iu")
    static boolean iu;

    @ObfuscatedName("jt")
    static int jt;

    @ObfuscatedName("ju")
    static int ju;

    @ObfuscatedName("jg")
    static int[] jg;

    @ObfuscatedName("ji")
    static final int[] ji;

    @ObfuscatedName("ja")
    static String[] playerActions;

    @ObfuscatedName("jn")
    static boolean[] jn;

    @ObfuscatedName("jj")
    static int[] jj;

    @ObfuscatedName("jq")
    static int jq;

    @ObfuscatedName("jz")
    static Deque[][][] loadedGroundItems;

    @ObfuscatedName("jw")
    static Deque jw;

    @ObfuscatedName("js")
    static Deque loadedProjectiles;

    @ObfuscatedName("jf")
    static Deque jf;

    @ObfuscatedName("jb")
    static int[] jb;

    @ObfuscatedName("jx")
    static int[] baseSkillLevels;

    @ObfuscatedName("jr")
    static int[] skillExperiences;

    @ObfuscatedName("jp")
    static int jp;

    @ObfuscatedName("jl")
    static boolean menuOpen;

    @ObfuscatedName("jd")
    static int menuSize;

    @ObfuscatedName("kw")
    static int[] menuArg1;

    @ObfuscatedName("kf")
    static int[] menuArg2;

    @ObfuscatedName("kk")
    static int[] menuOpcodes;

    @ObfuscatedName("ka")
    static int[] ka;

    @ObfuscatedName("ko")
    static String[] menuActions;

    @ObfuscatedName("kd")
    static String[] menuTargets;

    @ObfuscatedName("kc")
    static boolean[] kc;

    @ObfuscatedName("kg")
    static boolean kg;

    @ObfuscatedName("ki")
    static boolean ki;

    @ObfuscatedName("kv")
    static boolean kv;

    @ObfuscatedName("ky")
    static boolean ky;

    @ObfuscatedName("kj")
    static int kj;

    @ObfuscatedName("ku")
    static int ku;

    @ObfuscatedName("kx")
    static int kx;

    @ObfuscatedName("ke")
    static int ke;

    @ObfuscatedName("kb")
    static int inventoryItemSelectionState;

    @ObfuscatedName("kl")
    static boolean interfaceSelected;

    @ObfuscatedName("kq")
    static int kq;

    @ObfuscatedName("kn")
    static int kn;

    @ObfuscatedName("lj")
    static String lj;

    @ObfuscatedName("lw")
    static String selectedSpellName;

    @ObfuscatedName("lv")
    static int hudIndex;

    @ObfuscatedName("lt")
    static HashTable subWindowTable;

    @ObfuscatedName("ly")
    static int ly;

    @ObfuscatedName("lq")
    static int lq;

    @ObfuscatedName("ls")
    static RTComponent ls;

    @ObfuscatedName("lx")
    static int lx;

    @ObfuscatedName("ll")
    static int ll;

    @ObfuscatedName("lh")
    public static int localPlayerRights;

    @ObfuscatedName("lc")
    static int lc;

    @ObfuscatedName("lb")
    static boolean lb;

    @ObfuscatedName("lz")
    static RTComponent lz;

    @ObfuscatedName("ln")
    static RTComponent ln;

    @ObfuscatedName("lm")
    static RTComponent lm;

    @ObfuscatedName("li")
    static int li;

    @ObfuscatedName("lf")
    static int lf;

    @ObfuscatedName("ld")
    static RTComponent ld;

    @ObfuscatedName("lk")
    static boolean lk;

    @ObfuscatedName("la")
    static int la;

    @ObfuscatedName("le")
    static int le;

    @ObfuscatedName("lg")
    static boolean lg;

    @ObfuscatedName("lo")
    static int lo;

    @ObfuscatedName("lu")
    static int lu;

    @ObfuscatedName("lr")
    static boolean lr;

    static {
        cr = cq.a;
        co = cq.a;
        cd = 0;
        cc = 0;
        ce = 0;
        dl = 0;
        df = 0;
        dq = 0;
        dt = 0;
        dy = 0;
        dn = ff.i;
        de = null;
        loadedNpcs = new Npc['耀'];
        dz = 0;
        npcIndices = new int['耀'];
        dm = 0;
        eb = new int[250];
        ee = new cn();
        ei = 0;
        ed = false;
        ew = true;
        ey = new kh();
        es = new HashMap();
        ea = 0;
        em = 1;
        er = 0;
        eh = 1;
        ex = 0;
        collisionMaps = new CollisionMap[4];
        dynamicRegion = false;
        dynamicRegionFlags = new int[4][13][13];
        fb = new int[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3 };
        fs = 0;
        fx = 2301979;
        fo = 5063219;
        gu = 3353893;
        ga = 7759444;
        gd = false;
        gg = 0;
        gq = 128;
        hintPlane = 0;
        gc = 0;
        gf = 0;
        gt = 0;
        gr = 0;
        gn = 0;
        gl = 50;
        gx = 0;
        gj = 0;
        gm = 0;
        ht = 12;
        hq = 6;
        hm = 0;
        hz = false;
        hj = 0;
        he = false;
        hp = 0;
        hr = 0;
        hx = 50;
        hf = new int[hx];
        hh = new int[hx];
        hw = new int[hx];
        hn = new int[hx];
        ha = new int[hx];
        hg = new int[hx];
        hc = new int[hx];
        hs = new String[hx];
        hl = new int[104][104];
        hu = 0;
        hv = -1;
        hd = -1;
        lastSelectedInterfaceUID = 0;
        lastSelectedComponentId = 0;
        hb = 0;
        mouseCrosshairState = 0;
        ho = true;
        iq = 0;
        iv = 0;
        is = 0;
        io = 0;
        ig = 0;
        ic = 0;
        iw = false;
        ih = 0;
        ij = 0;
        ir = true;
        loadedPlayers = new Player[2048];
        localPlayerIndex = -1;
        im = 0;
        iu = true;
        jt = 0;
        ju = 0;
        jg = new int[1000];
        ji = new int[] { 44, 45, 46, 47, 48, 49, 50, 51 };
        playerActions = new String[8];
        jn = new boolean[8];
        jj = new int[] { 768, 1024, 1280, 512, 1536, 256, 0, 1792 };
        jq = -1;
        loadedGroundItems = new Deque[4][104][104];
        jw = new Deque();
        loadedProjectiles = new Deque();
        jf = new Deque();
        jb = new int[25];
        baseSkillLevels = new int[25];
        skillExperiences = new int[25];
        jp = 0;
        menuOpen = false;
        menuSize = 0;
        menuArg1 = new int[500];
        menuArg2 = new int[500];
        menuOpcodes = new int[500];
        ka = new int[500];
        menuActions = new String[500];
        menuTargets = new String[500];
        kc = new boolean[500];
        kg = false;
        ki = false;
        kv = false;
        ky = true;
        kj = -1;
        ku = -1;
        kx = 0;
        ke = 50;
        inventoryItemSelectionState = 0;
        lastSelectedInventoryItemName = null;
        interfaceSelected = false;
        kq = -1;
        kn = -1;
        lj = null;
        selectedSpellName = null;
        hudIndex = -1;
        subWindowTable = new HashTable(8);
        ly = 0;
        lq = 0;
        ls = null;
        lx = 0;
        ll = 0;
        localPlayerRights = 0;
        lc = -1;
        lb = false;
        lz = null;
        ln = null;
        lm = null;
        li = 0;
        lf = 0;
        ld = null;
        lk = false;
        la = -1;
        le = -1;
        lg = false;
        lo = -1;
        lu = -1;
        lr = false;
        mn = 1;
        mk = new int[32];
        ma = 0;
        mu = new int[32];
        mz = 0;
        mg = new int[32];
        my = 0;
        mc = 0;
        ms = 0;
        mj = 0;
        mo = 0;
        mi = 0;
        mv = 0;
        mw = 0;
        me = new Deque();
        mh = new Deque();
        ml = new Deque();
        mf = new HashTable(512);
        nm = 0;
        nq = -2;
        outdatedInterfaces = new boolean[100];
        nt = new boolean[100];
        nj = new boolean[100];
        interfaceXPositions = new int[100];
        interfaceYPositions = new int[100];
        interfaceWidths = new int[100];
        interfaceHeights = new int[100];
        nu = 0;
        ng = 0L;
        ny = true;
        ne = new int[] { 16776960, 16711680, 65280, 65535, 16711935, 16777215 };
        np = 0;
        nb = 0;
        nz = "";
        nx = new long[100];
        nd = 0;
        og = 0;
        or = new int[128];
        ol = new int[128];
        oz = -1L;
        oj = -1;
        oc = 0;
        ov = new int[1000];
        od = new int[1000];
        ou = new Sprite[1000];
        destinationX = 0;
        destinationY = 0;
        oo = 0;
        os = 255;
        ox = -1;
        oe = false;
        ow = 127;
        pa = 127;
        audioEffectCount = 0;
        po = new int[50];
        pe = new int[50];
        pt = new int[50];
        pl = new int[50];
        audioTracks = new AudioTrack[50];
        px = false;
        pf = new boolean[5];
        pb = new int[5];
        qh = new int[5];
        currentSkillLevels = new int[5];
        qs = new int[5];
        qw = 256;
        qk = 205;
        qr = 256;
        qu = 320;
        qm = 1;
        qd = 32767;
        qf = 1;
        qa = 32767;
        qq = 0;
        screenState = 0;
        viewportWidth = 0;
        viewportHeight = 0;
        viewportScale = 0;
        qv = new PlayerComposite();
        qn = -1;
        qx = -1;
        exchangeOffers = new GrandExchangeOffer[8];
        qz = new bu();
        rh = -1;
        rk = new ArrayList(10);
        rm = 0;
        rn = new bv();
        rp = new int[50];
        re = new int[50];
    }

    @ObfuscatedName("z")
    protected final void z() {
        ng = au.t() + 500L;
        this.fo();
        if (hudIndex != -1) {
            this.is(true);
        }

    }

    @ObfuscatedName("az")
    protected final void az() {
        int[] var1 = new int[] { 20, 260, 10000 };
        int[] var2 = new int[] { 1000, 100, 500 };
        if (var1 != null && var2 != null) {
            CombatBarData.e = var1;
            gc.x = new int[var1.length];
            gc.p = new byte[var1.length][][];

            for (int var3 = 0; var3 < CombatBarData.e.length; ++var3) {
                gc.p[var3] = new byte[var2[var3]][];
            }
        } else {
            CombatBarData.e = null;
            gc.x = null;
            gc.p = null;
        }

        dc.dw = playerWeight == 0 ? 'ꩊ' : currentWorld + '鱀';
        Player.dd = playerWeight == 0 ? 443 : currentWorld + '썐';
        cr.du = dc.dw;
        PlayerComposite.e = is.t;
        PlayerComposite.x = is.q;
        c.p = is.i;
        PlayerComposite.g = is.a;
        ak.en = new ec();
        this.c();
        this.u();
        aq.mb = this.x();
        gd.rb = new fn(255, fh.c, fh.v, 500000);
        RSRandomAccessFile var4 = null;
        ClientPreferences var5 = new ClientPreferences();

        try {
            var4 = ky.a("", be.e, false);
            byte[] var6 = new byte[(int) var4.l()];

            int var8;
            for (int var7 = 0; var7 < var6.length; var7 += var8) {
                var8 = var4.b(var6, var7, var6.length - var7);
                if (var8 == -1) {
                    throw new IOException();
                }
            }

            var5 = new ClientPreferences(new ByteBuffer(var6));
        } catch (Exception var11) {
            ;
        }

        try {
            if (var4 != null) {
                var4.i();
            }
        } catch (Exception var10) {
            ;
        }

        al.qp = var5;
        this.p();
        eq.t((Applet) this, (String) o.ai);
        if (playerWeight != 0) {
            bl = true;
        }

        al.fx(al.qp.l);
        BoundaryObject.qi = new cm(ad.bc);
    }

    @ObfuscatedName("ap")
    protected final void ap() {
        ++bz;
        this.em();

        while (true) {
            Deque var2 = jt.q;
            ia var1;
            synchronized (jt.q) {
                var1 = (ia) jt.i.l();
            }

            if (var1 == null) {
                er.x();
                al.fb();
                ae.a();
                d.q();
                if (aq.mb != null) {
                    int var4 = aq.mb.i();
                    mw = var4;
                }

                if (packet == 0) {
                    cd.fg();
                    p.am();
                } else if (packet == 5) {
                    gc.l(this);
                    cd.fg();
                    p.am();
                } else if (packet != 10 && packet != 11) {
                    if (packet == 20) {
                        gc.l(this);
                        this.ff();
                    } else if (packet == 25) {
                        Player.gw();
                    }
                } else {
                    gc.l(this);
                }

                if (packet == 30) {
                    this.ft();
                } else if (packet == 40 || packet == 45) {
                    this.ff();
                }

                return;
            }

            var1.a.dq(var1.i, (int) var1.uid, var1.q, false);
        }
    }

    @ObfuscatedName("ah")
    protected final void ah(boolean var1) {
        boolean var2 = kq.p();
        if (var2 && oe && c.pi != null) {
            c.pi.au();
        }

        if ((packet == 10 || packet == 20 || packet == 30) && 0L != ng && au.t() > ng) {
            al.fx(bg.fq());
        }

        int var3;
        if (var1) {
            for (var3 = 0; var3 < 100; ++var3) {
                outdatedInterfaces[var3] = true;
            }
        }

        if (packet == 0) {
            this.ag(ci.au, ci.ax, var1);
        } else if (packet == 5) {
            ab.x(b.ev, fv.ez, fv.et, var1);
        } else if (packet != 10 && packet != 11) {
            if (packet == 20) {
                ab.x(b.ev, fv.ez, fv.et, var1);
            } else if (packet == 25) {
                if (ex == 1) {
                    if (ea > em) {
                        em = ea;
                    }

                    var3 = (em * 50 - ea * 50) / em;
                    f.ga("Loading - please wait." + "<br>" + " (" + var3 + "%" + ")", false);
                } else if (ex == 2) {
                    if (er > eh) {
                        eh = er;
                    }

                    var3 = (eh * 50 - er * 50) / eh + 50;
                    f.ga("Loading - please wait." + "<br>" + " (" + var3 + "%" + ")", false);
                } else {
                    f.ga("Loading - please wait.", false);
                }
            } else if (packet == 30) {
                this.gu();
            } else if (packet == 40) {
                f.ga("Connection lost" + "<br>" + "Please wait - attempting to reestablish", false);
            } else if (packet == 45) {
                f.ga("Please wait...", false);
            }
        } else {
            ab.x(b.ev, fv.ez, fv.et, var1);
        }

        if (packet == 30 && nu == 0 && !var1 && !ny) {
            for (var3 = 0; var3 < nm; ++var3) {
                if (nt[var3]) {
                    ad.interfaceProducer.i(interfaceXPositions[var3], interfaceYPositions[var3], interfaceWidths[var3],
                            interfaceHeights[var3]);
                    nt[var3] = false;
                }
            }
        } else if (packet > 0) {
            ad.interfaceProducer.q(0, 0);

            for (var3 = 0; var3 < nm; ++var3) {
                nt[var3] = false;
            }
        }

    }

    @ObfuscatedName("at")
    protected final void at() {
        if (g.vm.o()) {
            g.vm.e();
        }

        if (SubWindow.bf != null) {
            SubWindow.bf.alive = false;
        }

        SubWindow.bf = null;
        ee.l();
        ae.i();
        if (bs.mouse != null) {
            bs var1 = bs.mouse;
            synchronized (bs.mouse) {
                bs.mouse = null;
            }
        }

        aq.mb = null;
        if (c.pi != null) {
            c.pi.ax();
        }

        if (gc.pk != null) {
            gc.pk.ax();
        }

        if (bg.t != null) {
            bg.t.b();
        }

        ar.q();
        if (ak.en != null) {
            ak.en.q();
            ak.en = null;
        }

        try {
            fh.c.t();

            for (int var3 = 0; var3 < fh.b; ++var3) {
                ag.u[var3].t();
            }

            fh.v.t();
            fh.o.t();
        } catch (Exception var5) {
            ;
        }

    }

    @ObfuscatedName("ak")
    protected final void ak() {
    }

    public final void init() {
      if (this.r()) {
         kg[] var1 = fr.t();

         int var6;
         for(int var2 = 0; var2 < var1.length; ++var2) {
            kg var3 = var1[var2];
            String var4 = this.getParameter(var3.k);
            if (var4 != null) {
               switch(Integer.parseInt(var3.k)) {
               case 1:
                  GrandExchangeOffer.bj = var4;
                  break;
               case 2:
                  br = Integer.parseInt(var4);
                  break;
               case 3:
                  be = (in)aj.t(ih.a(), Integer.parseInt(var4));
                  if (be == in.b) {
                     ad.bc = lu.t;
                  } else {
                     ad.bc = lu.p;
                  }
                  break;
               case 4:
                  jv.by = Integer.parseInt(var4);
                  break;
               case 5:
                  bs = Integer.parseInt(var4);
                  break;
               case 6:
                  Projectile.bt = var4;
                  break;
               case 7:
                  if (var4.equalsIgnoreCase("true")) {
                     ;
                  }
               case 8:
               case 10:
               case 15:
               default:
                  break;
               case 9:
                  ew = Integer.parseInt(var4) != 0;
                  break;
               case 11:
                  var6 = Integer.parseInt(var4);
                  DevelopmentStage[] var7 = new DevelopmentStage[]{DevelopmentStage.q, DevelopmentStage.i, DevelopmentStage.t, DevelopmentStage.a};
                  DevelopmentStage[] var8 = var7;
                  int var9 = 0;

                  DevelopmentStage var5;
                  while(true) {
                     if (var9 >= var8.length) {
                        var5 = null;
                        break;
                     }

                     DevelopmentStage var10 = var8[var9];
                     if (var6 == var10.id) {
                        var5 = var10;
                        break;
                     }

                     ++var9;
                  }

                  q.ba = var5;
                  break;
               case 12:
                  if (var4.equalsIgnoreCase("true")) {
                     membersWorld = true;
                  } else {
                     membersWorld = false;
                  }
                  break;
               case 13:
                  playerWeight = Integer.parseInt(var4);
                  break;
               case 14:
                  currentWorld = Integer.parseInt(var4);
                  break;
               case 16:
                  bn.bn = Integer.parseInt(var4);
               }
            }
         }

         ah.eq();
         u.do = this.getCodeBase().getHost();
         String var16 = q.ba.label;
         byte var17 = 0;

         try {
            fh.b = 17;
            ai.j = var17;

            try {
               aw.f = System.getProperty("os.name");
            } catch (Exception var14) {
               aw.f = "Unknown";
            }

            fh.d = aw.f.toLowerCase();

            try {
               fh.r = System.getProperty("user.home");
               if (fh.r != null) {
                  fh.r = fh.r + "/";
               }
            } catch (Exception var13) {
               ;
            }

            try {
               if (fh.d.startsWith("win")) {
                  if (fh.r == null) {
                     fh.r = System.getenv("USERPROFILE");
                  }
               } else if (fh.r == null) {
                  fh.r = System.getenv("HOME");
               }

               if (fh.r != null) {
                  fh.r = fh.r + "/";
               }
            } catch (Exception var12) {
               ;
            }

            if (fh.r == null) {
               fh.r = "~/";
            }

            s.k = new String[]{"c:/rscache/", "/rscache/", "c:/windows/", "c:/winnt/", "c:/", fh.r, "/tmp/", ""};
            fy.z = new String[]{".jagex_cache_" + ai.j, ".file_store_" + ai.j};

            int var11;
            label125:
            for(var11 = 0; var11 < 4; ++var11) {
               ep.l = Skins.t("oldschool", var16, var11);
               if (!ep.l.exists()) {
                  ep.l.mkdirs();
               }

               File[] var18 = ep.l.listFiles();
               if (var18 == null) {
                  break;
               }

               File[] var19 = var18;
               var6 = 0;

               while(true) {
                  if (var6 >= var19.length) {
                     break label125;
                  }

                  File var20 = var19[var6];
                  if (!ad.i(var20, false)) {
                     break;
                  }

                  ++var6;
               }
            }

            cr.t(ep.l);
            Npc.l();
            fh.c = new RSRandomAccessFileChannel(new RSRandomAccessFile(jv.q("main_file_cache.dat2"), "rw", 1048576000L), 5200, 0);
            fh.v = new RSRandomAccessFileChannel(new RSRandomAccessFile(jv.q("main_file_cache.idx255"), "rw", 1048576L), 6000, 0);
            ag.u = new RSRandomAccessFileChannel[fh.b];

            for(var11 = 0; var11 < fh.b; ++var11) {
               ag.u[var11] = new RSRandomAccessFileChannel(new RSRandomAccessFile(jv.q("main_file_cache.idx" + var11), "rw", 1048576L), 6000, 0);
            }
         } catch (Exception var15) {
            FloorObject.t((String)null, var15);
         }

         ir.ac = this;
         this.d(765, 503, 166);
      }
   }

    @ObfuscatedName("em")
    void em() {
        if (packet != 1000) {
            long var2 = au.t();
            int var4 = (int) (var2 - ea.i);
            ea.i = var2;
            if (var4 > 200) {
                var4 = 200;
            }

            jg.q += var4;
            boolean var1;
            if (jg.o == 0 && jg.e == 0 && jg.g == 0 && jg.l == 0) {
                var1 = true;
            } else if (bg.t == null) {
                var1 = false;
            } else {
                try {
                    label241: {
                        if (jg.q > 30000) {
                            throw new IOException();
                        }

                        jk var5;
                        ByteBuffer var6;
                        while (jg.e < 200 && jg.l > 0) {
                            var5 = (jk) jg.a.a();
                            var6 = new ByteBuffer(4);
                            var6.a(1);
                            var6.b((int) var5.uid);
                            bg.t.l(var6.buffer, 0, 4);
                            jg.b.q(var5, var5.uid);
                            --jg.l;
                            ++jg.e;
                        }

                        while (jg.o < 200 && jg.g > 0) {
                            var5 = (jk) jg.x.l();
                            var6 = new ByteBuffer(4);
                            var6.a(0);
                            var6.b((int) var5.uid);
                            bg.t.l(var6.buffer, 0, 4);
                            var5.ce();
                            jg.n.q(var5, var5.uid);
                            --jg.g;
                            ++jg.o;
                        }

                        for (int var17 = 0; var17 < 100; ++var17) {
                            int var18 = bg.t.q();
                            if (var18 < 0) {
                                throw new IOException();
                            }

                            if (var18 == 0) {
                                break;
                            }

                            jg.q = 0;
                            byte var7 = 0;
                            if (TaskHandler.v == null) {
                                var7 = 8;
                            } else if (jg.k == 0) {
                                var7 = 1;
                            }

                            int var8;
                            int var9;
                            int var10;
                            int var12;
                            if (var7 > 0) {
                                var8 = var7 - jg.u.index;
                                if (var8 > var18) {
                                    var8 = var18;
                                }

                                bg.t.a(jg.u.buffer, jg.u.index, var8);
                                if (jg.f != 0) {
                                    for (var9 = 0; var9 < var8; ++var9) {
                                        jg.u.buffer[jg.u.index + var9] ^= jg.f;
                                    }
                                }

                                jg.u.index += var8;
                                if (jg.u.index < var7) {
                                    break;
                                }

                                if (TaskHandler.v == null) {
                                    jg.u.index = 0;
                                    var9 = jg.u.av();
                                    var10 = jg.u.ae();
                                    int var11 = jg.u.av();
                                    var12 = jg.u.ap();
                                    long var13 = (long) (var10 + (var9 << 16));
                                    jk var15 = (jk) jg.b.t(var13);
                                    jg.c = true;
                                    if (var15 == null) {
                                        var15 = (jk) jg.n.t(var13);
                                        jg.c = false;
                                    }

                                    if (var15 == null) {
                                        throw new IOException();
                                    }

                                    int var16 = var11 == 0 ? 5 : 9;
                                    TaskHandler.v = var15;
                                    RSRandomAccessFileChannel.j = new ByteBuffer(var12 + var16 + TaskHandler.v.i);
                                    RSRandomAccessFileChannel.j.a(var11);
                                    RSRandomAccessFileChannel.j.e(var12);
                                    jg.k = 8;
                                    jg.u.index = 0;
                                } else if (jg.k == 0) {
                                    if (jg.u.buffer[0] == -1) {
                                        jg.k = 1;
                                        jg.u.index = 0;
                                    } else {
                                        TaskHandler.v = null;
                                    }
                                }
                            } else {
                                var8 = RSRandomAccessFileChannel.j.buffer.length - TaskHandler.v.i;
                                var9 = 512 - jg.k;
                                if (var9 > var8 - RSRandomAccessFileChannel.j.index) {
                                    var9 = var8 - RSRandomAccessFileChannel.j.index;
                                }

                                if (var9 > var18) {
                                    var9 = var18;
                                }

                                bg.t.a(RSRandomAccessFileChannel.j.buffer, RSRandomAccessFileChannel.j.index, var9);
                                if (jg.f != 0) {
                                    for (var10 = 0; var10 < var9; ++var10) {
                                        RSRandomAccessFileChannel.j.buffer[RSRandomAccessFileChannel.j.index + var10] ^= jg.f;
                                    }
                                }

                                RSRandomAccessFileChannel.j.index += var9;
                                jg.k += var9;
                                if (var8 == RSRandomAccessFileChannel.j.index) {
                                    if (16711935L == TaskHandler.v.uid) {
                                        em.s = RSRandomAccessFileChannel.j;

                                        for (var10 = 0; var10 < 256; ++var10) {
                                            ju var19 = jg.d[var10];
                                            if (var19 != null) {
                                                em.s.index = var10 * 8 + 5;
                                                var12 = em.s.ap();
                                                int var20 = em.s.ap();
                                                var19.dl(var12, var20);
                                            }
                                        }
                                    } else {
                                        jg.w.reset();
                                        jg.w.update(RSRandomAccessFileChannel.j.buffer, 0, var8);
                                        var10 = (int) jg.w.getValue();
                                        if (var10 != TaskHandler.v.q) {
                                            try {
                                                bg.t.b();
                                            } catch (Exception var23) {
                                                ;
                                            }

                                            ++jg.r;
                                            bg.t = null;
                                            jg.f = (byte) ((int) (Math.random() * 255.0D + 1.0D));
                                            var1 = false;
                                            break label241;
                                        }

                                        jg.r = 0;
                                        jg.y = 0;
                                        TaskHandler.v.t.df((int) (TaskHandler.v.uid & 65535L),
                                                RSRandomAccessFileChannel.j.buffer,
                                                (TaskHandler.v.uid & 16711680L) == 16711680L, jg.c);
                                    }

                                    TaskHandler.v.kc();
                                    if (jg.c) {
                                        --jg.e;
                                    } else {
                                        --jg.o;
                                    }

                                    jg.k = 0;
                                    TaskHandler.v = null;
                                    RSRandomAccessFileChannel.j = null;
                                } else {
                                    if (jg.k != 512) {
                                        break;
                                    }

                                    jg.k = 0;
                                }
                            }
                        }

                        var1 = true;
                    }
                } catch (IOException var24) {
                    try {
                        bg.t.b();
                    } catch (Exception var22) {
                        ;
                    }

                    ++jg.y;
                    bg.t = null;
                    var1 = false;
                }
            }

            if (!var1) {
                this.er();
            }

        }
    }

    @ObfuscatedName("er")
   void er() {
      if (jg.r >= 4) {
         this.aw("js5crc");
         packet = 1000;
      } else {
         if (jg.y >= 4) {
            if (packet <= 5) {
               this.aw("js5io");
               packet = 1000;
               return;
            }

            ce = 3000;
            jg.y = 3;
         }

         if (--ce + 1 <= 0) {
            try {
               if (cc == 0) {
                  cq = GameEngine.t.i(u.do, cr.du);
                  ++cc;
               }

               if (cc == 1) {
                  if (cq.l == 2) {
                     this.eh(-1);
                     return;
                  }

                  if (cq.l == 1) {
                     ++cc;
                  }
               }

               if (cc == 2) {
                  if (ew) {
                     Socket var2 = (Socket)cq.p;
                     fm var1 = new fm(var2, 40000, 5000);
                     fw.ci = var1;
                  } else {
                     fw.ci = new RSSocket((Socket)cq.p, GameEngine.t, 5000);
                  }

                  ByteBuffer var5 = new ByteBuffer(5);
                  var5.a(15);
                  var5.e(166);
                  fw.ci.l(var5.buffer, 0, 5);
                  ++cc;
                  ih.cx = au.t();
               }

               if (cc == 3) {
                  if (fw.ci.q() > 0 || !ew && packet <= 5) {
                     int var3 = fw.ci.i();
                     if (var3 != 0) {
                        this.eh(var3);
                        return;
                     }

                     ++cc;
                  } else if (au.t() - ih.cx > 30000L) {
                     this.eh(-2);
                     return;
                  }
               }

               if (cc == 4) {
                  TaskHandler.q(fw.ci, packet > 20);
                  cq = null;
                  fw.ci = null;
                  cc = 0;
                  dl = 0;
               }
            } catch (IOException var4) {
               this.eh(-3);
            }

         }
      }
   }

    @ObfuscatedName("eh")
    void eh(int var1) {
        cq = null;
        fw.ci = null;
        cc = 0;
        if (cr.du == dc.dw) {
            cr.du = Player.dd;
        } else {
            cr.du = dc.dw;
        }

        ++dl;
        if (dl >= 2 && (var1 == 7 || var1 == 9)) {
            if (packet <= 5) {
                this.aw("js5connect_full");
                packet = 1000;
            } else {
                ce = 3000;
            }
        } else if (dl >= 2 && var1 == 6) {
            this.aw("js5connect_outofdate");
            packet = 1000;
        } else if (dl >= 4) {
            if (packet <= 5) {
                this.aw("js5connect");
                packet = 1000;
            } else {
                ce = 3000;
            }
        }

    }

    @ObfuscatedName("ff")
   final void ff() {
      Object var1 = ee.e();
      Packet var2 = ee.b;

      try {
         if (df == 0) {
            if (var1 != null) {
               ((fy)var1).b();
               var1 = null;
            }

            gv.ej = null;
            ed = false;
            dq = 0;
            df = 1;
         }

         if (df == 1) {
            if (gv.ej == null) {
               gv.ej = GameEngine.t.i(u.do, cr.du);
            }

            if (gv.ej.l == 2) {
               throw new IOException();
            }

            if (gv.ej.l == 1) {
               if (ew) {
                  Socket var4 = (Socket)gv.ej.p;
                  fm var3 = new fm(var4, 40000, 5000);
                  var1 = var3;
               } else {
                  var1 = new RSSocket((Socket)gv.ej.p, GameEngine.t, 5000);
               }

               ee.a((fy)var1);
               gv.ej = null;
               df = 2;
            }
         }

         gd var19;
         if (df == 2) {
            ee.t();
            var19 = ep.q();
            var19.i.a(gu.t.l);
            ee.i(var19);
            ee.q();
            var2.index = 0;
            df = 3;
         }

         boolean var11;
         int var12;
         if (df == 3) {
            if (c.pi != null) {
               c.pi.ah();
            }

            if (gc.pk != null) {
               gc.pk.ah();
            }

            var11 = true;
            if (ew && !((fy)var1).t(1)) {
               var11 = false;
            }

            if (var11) {
               var12 = ((fy)var1).i();
               if (c.pi != null) {
                  c.pi.ah();
               }

               if (gc.pk != null) {
                  gc.pk.ah();
               }

               if (var12 != 0) {
                  e.fk(var12);
                  return;
               }

               var2.index = 0;
               df = 4;
            }
         }

         int var28;
         if (df == 4) {
            if (var2.index < 8) {
               var28 = ((fy)var1).q();
               if (var28 > 8 - var2.index) {
                  var28 = 8 - var2.index;
               }

               if (var28 > 0) {
                  ((fy)var1).a(var2.buffer, var2.index, var28);
                  var2.index += var28;
               }
            }

            if (var2.index == 8) {
               var2.index = 0;
               TileModel.ek = var2.ah();
               df = 5;
            }
         }

         int var8;
         int var13;
         if (df == 5) {
            ee.b.index = 0;
            ee.t();
            Packet var20 = new Packet(500);
            int[] var22 = new int[]{(int)(Math.random() * 9.9999999E7D), (int)(Math.random() * 9.9999999E7D), (int)(TileModel.ek >> 32), (int)(TileModel.ek & -1L)};
            var20.index = 0;
            var20.a(1);
            var20.a(dn.t());
            var20.e(var22[0]);
            var20.e(var22[1]);
            var20.e(var22[2]);
            var20.e(var22[3]);
            switch(dn.l) {
            case 0:
            case 2:
               var20.b(AreaSoundEmitter.br);
               var20.index += 5;
               break;
            case 1:
               var20.e((Integer)al.qp.map.get(kc.x(ci.username)));
               var20.index += 4;
               break;
            case 3:
               var20.index += 8;
            }

            var20.u(ci.password);
            var20.ac(cd.t, cd.q);
            gd var6;
            if (gd.b == 0) {
               var6 = new gd();
            } else {
               var6 = gd.l[--gd.b];
            }

            var6.t = null;
            var6.q = 0;
            var6.i = new Packet(5000);
            var6.i.index = 0;
            if (packet == 40) {
               var6.i.a(gu.a.l);
            } else {
               var6.i.a(gu.i.l);
            }

            var6.i.l(0);
            var13 = var6.i.index;
            var6.i.e(166);
            var6.i.s(var20.buffer, 0, var20.index);
            var8 = var6.i.index;
            var6.i.u(ci.username);
            var6.i.a((ny ? 1 : 0) << 1 | (bh ? 1 : 0));
            var6.i.l(BoundaryObject.r);
            var6.i.l(GameEngine.h);
            e.jm(var6.i);
            var6.i.u(Projectile.bt);
            var6.i.e(jv.by);
            ByteBuffer var9 = new ByteBuffer(o.qt.i());
            o.qt.q(var9);
            var6.i.s(var9.buffer, 0, var9.buffer.length);
            var6.i.a(bn.bn);
            var6.i.e(0);
            var6.i.e(Server.cy.discardUnpacked);
            var6.i.e(MenuItemNode.cg.discardUnpacked);
            var6.i.e(cj.discardUnpacked);
            var6.i.e(ly.cl.discardUnpacked);
            var6.i.e(aj.cz.discardUnpacked);
            var6.i.e(ee.cn.discardUnpacked);
            var6.i.e(Varpbit.ca.discardUnpacked);
            var6.i.e(iq.cf.discardUnpacked);
            var6.i.e(au.cp.discardUnpacked);
            var6.i.e(bt.ck.discardUnpacked);
            var6.i.e(jk.db.discardUnpacked);
            var6.i.e(bk.dp.discardUnpacked);
            var6.i.e(p.da.discardUnpacked);
            var6.i.e(dr.discardUnpacked);
            var6.i.e(kq.dj.discardUnpacked);
            var6.i.e(bn.dh.discardUnpacked);
            var6.i.e(gf.dc.discardUnpacked);
            var6.i.ak(var22, var8, var6.i.index);
            var6.i.f(var6.i.index - var13);
            ee.i(var6);
            ee.q();
            ee.l = new gv(var22);

            for(int var10 = 0; var10 < 4; ++var10) {
               var22[var10] += 50;
            }

            var2.iz(var22);
            df = 6;
         }

         if (df == 6 && ((fy)var1).q() > 0) {
            var28 = ((fy)var1).i();
            if (var28 == 21 && packet == 20) {
               df = 7;
            } else if (var28 == 2) {
               df = 9;
            } else if (var28 == 15 && packet == 40) {
               ee.x = -1;
               df = 13;
            } else if (var28 == 23 && dt < 1) {
               ++dt;
               df = 0;
            } else {
               if (var28 != 29) {
                  e.fk(var28);
                  return;
               }

               df = 11;
            }
         }

         if (df == 7 && ((fy)var1).q() > 0) {
            dy = (((fy)var1).i() + 3) * 60;
            df = 8;
         }

         if (df == 8) {
            dq = 0;
            cx.p("You have only just left another world.", "Your profile will be transferred in:", dy / 60 + " seconds.");
            if (--dy <= 0) {
               df = 0;
            }

         } else {
            boolean var29;
            if (df == 9 && ((fy)var1).q() >= 13) {
               var11 = ((fy)var1).i() == 1;
               ((fy)var1).a(var2.buffer, 0, 4);
               var2.index = 0;
               var29 = false;
               if (var11) {
                  var12 = var2.in() << 24;
                  var12 |= var2.in() << 16;
                  var12 |= var2.in() << 8;
                  var12 |= var2.in();
                  String var27 = ci.username;
                  var13 = var27.length();
                  var8 = 0;
                  int var14 = 0;

                  while(true) {
                     if (var14 >= var13) {
                        if (al.qp.map.size() >= 10 && !al.qp.map.containsKey(var8)) {
                           Iterator var16 = al.qp.map.entrySet().iterator();
                           var16.next();
                           var16.remove();
                        }

                        al.qp.map.put(var8, var12);
                        break;
                     }

                     var8 = (var8 << 5) - var8 + var27.charAt(var14);
                     ++var14;
                  }
               }

               if (ci.aw) {
                  al.qp.b = ci.username;
               } else {
                  al.qp.b = null;
               }

               ar.i();
               localPlayerRights = ((fy)var1).i();
               lb = ((fy)var1).i() == 1;
               localPlayerIndex = ((fy)var1).i();
               localPlayerIndex <<= 8;
               localPlayerIndex += ((fy)var1).i();
               im = ((fy)var1).i();
               ((fy)var1).a(var2.buffer, 0, 1);
               var2.index = 0;
               fx[] var5 = new fx[]{fx.t, fx.q, fx.i, fx.a, fx.l, fx.b, fx.e, fx.x, fx.p, fx.g, fx.n, fx.o, fx.c, fx.v, fx.u, fx.j, fx.k, fx.z, fx.w, fx.s, fx.d, fx.f, fx.r, fx.y, fx.h, fx.m, fx.ay, fx.ao, fx.av, fx.aj, fx.ae, fx.am, fx.az, fx.ap, fx.ah, fx.au, fx.ax, fx.ar, fx.an, fx.ai, fx.al, fx.at, fx.ag, fx.as, fx.aw, fx.aq, fx.aa, fx.af, fx.ak, fx.ab, fx.ac, fx.ad, fx.bg, fx.br, fx.ba, fx.bk, fx.be, fx.bc, fx.bm, fx.bh, fx.bs, fx.bj, fx.bt, fx.by, fx.bn, fx.bb, fx.bq, fx.bz, fx.bx, fx.bf, fx.bo, fx.bv, fx.bi, fx.bu, fx.bl, fx.bw, fx.bp, fx.bd, fx.cb, fx.cm, fx.cu, fx.cs, fx.ct, fx.cw};
               var13 = var2.jk();
               if (var13 < 0 || var13 >= var5.length) {
                  throw new IOException(var13 + " " + var2.index);
               }

               ee.e = var5[var13];
               ee.x = ee.e.cr;
               ((fy)var1).a(var2.buffer, 0, 2);
               var2.index = 0;
               ee.x = var2.ae();

               try {
                  br.t(ir.ac, "zap");
               } catch (Throwable var17) {
                  ;
               }

               df = 10;
            }

            if (df != 10) {
               if (df == 11 && ((fy)var1).q() >= 2) {
                  var2.index = 0;
                  ((fy)var1).a(var2.buffer, 0, 2);
                  var2.index = 0;
                  h.dk = var2.ae();
                  df = 12;
               }

               if (df == 12 && ((fy)var1).q() >= h.dk) {
                  var2.index = 0;
                  ((fy)var1).a(var2.buffer, 0, h.dk);
                  var2.index = 0;
                  String var24 = var2.ar();
                  String var25 = var2.ar();
                  String var26 = var2.ar();
                  cx.p(var24, var25, var26);
                  d.ea(10);
               }

               if (df == 13) {
                  if (ee.x == -1) {
                     if (((fy)var1).q() < 2) {
                        return;
                     }

                     ((fy)var1).a(var2.buffer, 0, 2);
                     var2.index = 0;
                     ee.x = var2.ae();
                  }

                  if (((fy)var1).q() >= ee.x) {
                     ((fy)var1).a(var2.buffer, 0, ee.x);
                     var2.index = 0;
                     var28 = ee.x;
                     ey.b();
                     f.fe();
                     ChatboxMessage.t(var2);
                     if (var28 != var2.index) {
                        throw new RuntimeException();
                     }
                  }
               } else {
                  ++dq;
                  if (dq > 2000) {
                     if (dt < 1) {
                        if (dc.dw == cr.du) {
                           cr.du = Player.dd;
                        } else {
                           cr.du = dc.dw;
                        }

                        ++dt;
                        df = 0;
                     } else {
                        e.fk(-3);
                     }
                  }
               }
            } else {
               if (((fy)var1).q() >= ee.x) {
                  var2.index = 0;
                  ((fy)var1).a(var2.buffer, 0, ee.x);
                  ey.a();
                  bx = 1L;
                  bi = -1;
                  SubWindow.bf.index = 0;
                  ByteBuffer.aq = true;
                  bu = true;
                  oz = -1L;
                  lm.t = new SinglyLinkedList();
                  ee.t();
                  ee.b.index = 0;
                  ee.e = null;
                  ee.o = null;
                  ee.c = null;
                  ee.v = null;
                  ee.x = 0;
                  ee.g = 0;
                  mouseTrackerIdleTime = 0;
                  ei = 0;
                  hintArrowType = 0;
                  z.hx();
                  bs.l = 0;
                  RSSocket.l();
                  inventoryItemSelectionState = 0;
                  interfaceSelected = false;
                  audioEffectCount = 0;
                  hintPlane = 0;
                  gn = 0;
                  cn.ry = null;
                  oo = 0;
                  oj = -1;
                  destinationX = 0;
                  destinationY = 0;
                  cr = cq.a;
                  co = cq.a;
                  dz = 0;
                  hi.l();

                  for(var28 = 0; var28 < 2048; ++var28) {
                     loadedPlayers[var28] = null;
                  }

                  for(var28 = 0; var28 < 32768; ++var28) {
                     loadedNpcs[var28] = null;
                  }

                  jq = -1;
                  loadedProjectiles.t();
                  jf.t();

                  int var15;
                  for(var28 = 0; var28 < 4; ++var28) {
                     for(var12 = 0; var12 < 104; ++var12) {
                        for(var15 = 0; var15 < 104; ++var15) {
                           loadedGroundItems[var28][var12][var15] = null;
                        }
                     }
                  }

                  jw = new Deque();
                  BoundaryObject.qi.l();

                  for(var28 = 0; var28 < VarInfo.q; ++var28) {
                     VarInfo var23 = q(var28);
                     if (var23 != null) {
                        iv.q[var28] = 0;
                        iv.varps[var28] = 0;
                     }
                  }

                  g.vm.l();
                  lc = -1;
                  if (hudIndex != -1) {
                     var28 = hudIndex;
                     if (var28 != -1 && c.loadedInterfaces[var28]) {
                        RTComponent.x.f(var28);
                        if (RTComponent.b[var28] != null) {
                           var29 = true;

                           for(var15 = 0; var15 < RTComponent.b[var28].length; ++var15) {
                              if (RTComponent.b[var28][var15] != null) {
                                 if (RTComponent.b[var28][var15].type != 2) {
                                    RTComponent.b[var28][var15] = null;
                                 } else {
                                    var29 = false;
                                 }
                              }
                           }

                           if (var29) {
                              RTComponent.b[var28] = null;
                           }

                           c.loadedInterfaces[var28] = false;
                        }
                     }
                  }

                  for(SubWindow var21 = (SubWindow)subWindowTable.a(); var21 != null; var21 = (SubWindow)subWindowTable.l()) {
                     cx.ji(var21, true);
                  }

                  hudIndex = -1;
                  subWindowTable = new HashTable(8);
                  ls = null;
                  z.hx();
                  qv.t((int[])null, new int[]{0, 0, 0, 0, 0}, false, -1);

                  for(var28 = 0; var28 < 8; ++var28) {
                     playerActions[var28] = null;
                     jn[var28] = false;
                  }

                  ec.e();
                  bq = true;

                  for(var28 = 0; var28 < 100; ++var28) {
                     outdatedInterfaces[var28] = true;
                  }

                  var19 = ap.t(fo.af, ee.l);
                  var19.i.a(bg.fq());
                  var19.i.l(BoundaryObject.r);
                  var19.i.l(GameEngine.h);
                  ee.i(var19);
                  ad.ot = null;

                  for(var28 = 0; var28 < 8; ++var28) {
                     exchangeOffers[var28] = new GrandExchangeOffer();
                  }

                  es.qe = null;
                  ChatboxMessage.t(var2);
                  ab.ef = -1;
                  y.gl(false, var2);
                  ee.e = null;
               }

            }
         }
      } catch (IOException var18) {
         if (dt < 1) {
            if (dc.dw == cr.du) {
               cr.du = Player.dd;
            } else {
               cr.du = dc.dw;
            }

            ++dt;
            df = 0;
         } else {
            e.fk(-2);
         }
      }
   }

    @ObfuscatedName("ft")
   final void ft() {
      if (mouseTrackerIdleTime > 1) {
         --mouseTrackerIdleTime;
      }

      if (ei > 0) {
         --ei;
      }

      if (ed) {
         ed = false;
         av.fz();
      } else {
         if (!menuOpen) {
            v.hf();
         }

         int var1;
         for(var1 = 0; var1 < 100 && this.gm(ee); ++var1) {
            ;
         }

         if (packet == 30) {
            int var2;
            gd var15;
            while(aa.t()) {
               var15 = ap.t(fo.bh, ee.l);
               var15.i.a(0);
               var2 = var15.i.index;
               ag.q(var15.i);
               var15.i.r(var15.i.index - var2);
               ee.i(var15);
            }

            if (ey.i) {
               var15 = ap.t(fo.o, ee.l);
               var15.i.a(0);
               var2 = var15.i.index;
               ey.e(var15.i);
               var15.i.r(var15.i.index - var2);
               ee.i(var15);
               ey.l();
            }

            Object var36 = SubWindow.bf.lock;
            int var3;
            int var4;
            int var5;
            int var6;
            int var7;
            int var8;
            int var9;
            int var10;
            synchronized(SubWindow.bf.lock) {
               if (!ad) {
                  SubWindow.bf.index = 0;
               } else if (bs.j != 0 || SubWindow.bf.index >= 40) {
                  gd var16 = ap.t(fo.bt, ee.l);
                  var16.i.a(0);
                  var3 = var16.i.index;
                  var4 = 0;

                  for(var5 = 0; var5 < SubWindow.bf.index && var16.i.index - var3 < 240; ++var5) {
                     ++var4;
                     var6 = SubWindow.bf.trackingY[var5];
                     if (var6 < 0) {
                        var6 = 0;
                     } else if (var6 > 502) {
                        var6 = 502;
                     }

                     var7 = SubWindow.bf.trackingX[var5];
                     if (var7 < 0) {
                        var7 = 0;
                     } else if (var7 > 764) {
                        var7 = 764;
                     }

                     var8 = var6 * 765 + var7;
                     if (SubWindow.bf.trackingY[var5] == -1 && SubWindow.bf.trackingX[var5] == -1) {
                        var7 = -1;
                        var6 = -1;
                        var8 = 524287;
                     }

                     if (var7 == bo && var6 == bv) {
                        if (bi < 2047) {
                           ++bi;
                        }
                     } else {
                        var9 = var7 - bo;
                        bo = var7;
                        var10 = var6 - bv;
                        bv = var6;
                        if (bi < 8 && var9 >= -32 && var9 <= 31 && var10 >= -32 && var10 <= 31) {
                           var9 += 32;
                           var10 += 32;
                           var16.i.l(var10 + (bi << 12) + (var9 << 6));
                           bi = 0;
                        } else if (bi < 8) {
                           var16.i.b((bi << 19) + var8 + 8388608);
                           bi = 0;
                        } else {
                           var16.i.e((bi << 19) + var8 + -1073741824);
                           bi = 0;
                        }
                     }
                  }

                  var16.i.r(var16.i.index - var3);
                  ee.i(var16);
                  if (var4 >= SubWindow.bf.index) {
                     SubWindow.bf.index = 0;
                  } else {
                     SubWindow.bf.index -= var4;

                     for(var5 = 0; var5 < SubWindow.bf.index; ++var5) {
                        SubWindow.bf.trackingX[var5] = SubWindow.bf.trackingX[var4 + var5];
                        SubWindow.bf.trackingY[var5] = SubWindow.bf.trackingY[var5 + var4];
                     }
                  }
               }
            }

            gd var19;
            if (bs.j == 1 || !er.ch && bs.j == 4 || bs.j == 2) {
               long var17 = (bs.w - bx * -1L) / 50L;
               if (var17 > 4095L) {
                  var17 = 4095L;
               }

               bx = bs.w * -1L;
               var3 = bs.z;
               if (var3 < 0) {
                  var3 = 0;
               } else if (var3 > GameEngine.h) {
                  var3 = GameEngine.h;
               }

               var4 = bs.cameraZ;
               if (var4 < 0) {
                  var4 = 0;
               } else if (var4 > BoundaryObject.r) {
                  var4 = BoundaryObject.r;
               }

               var5 = (int)var17;
               var19 = ap.t(fo.i, ee.l);
               var19.i.l((var5 << 1) + (bs.j == 2 ? 1 : 0));
               var19.i.l(var4);
               var19.i.l(var3);
               ee.i(var19);
            }

            if (ad.cg > 0) {
               var15 = ap.t(fo.q, ee.l);
               var15.i.l(0);
               var2 = var15.i.index;
               long var20 = au.t();

               for(var5 = 0; var5 < ad.cg; ++var5) {
                  long var22 = var20 - oz;
                  if (var22 > 16777215L) {
                     var22 = 16777215L;
                  }

                  oz = var20;
                  var15.i.bk(ad.cy[var5]);
                  var15.i.bv((int)var22);
               }

               var15.i.f(var15.i.index - var2);
               ee.i(var15);
            }

            if (hj > 0) {
               --hj;
            }

            if (ad.pressedKeys[96] || ad.pressedKeys[97] || ad.pressedKeys[98] || ad.pressedKeys[99]) {
               he = true;
            }

            if (he && hj <= 0) {
               hj = 20;
               he = false;
               var15 = ap.t(fo.ar, ee.l);
               var15.i.bt(gq);
               var15.i.l(hintPlane);
               ee.i(var15);
            }

            if (ByteBuffer.aq && !bu) {
               bu = true;
               var15 = ap.t(fo.b, ee.l);
               var15.i.a(1);
               ee.i(var15);
            }

            if (!ByteBuffer.aq && bu) {
               bu = false;
               var15 = ap.t(fo.b, ee.l);
               var15.i.a(0);
               ee.i(var15);
            }

            if (ip.rz != null) {
               ip.rz.i();
            }

            TaskData.fw();
            if (oj != kt.ii) {
               oj = kt.ii;
               q.gx(kt.ii);
            }

            if (packet == 30) {
               ga.hm();

               for(var1 = 0; var1 < audioEffectCount; ++var1) {
                  --pt[var1];
                  if (pt[var1] >= -10) {
                     AudioTrack var37 = audioTracks[var1];
                     if (var37 == null) {
                        Object var10000 = null;
                        var37 = AudioTrack.t(aj.cz, po[var1], 0);
                        if (var37 == null) {
                           continue;
                        }

                        pt[var1] += var37.i();
                        audioTracks[var1] = var37;
                     }

                     if (pt[var1] < 0) {
                        if (pl[var1] != 0) {
                           var4 = (pl[var1] & 255) * 128;
                           var5 = pl[var1] >> 16 & 255;
                           var6 = var5 * 128 + 64 - az.il.regionX;
                           if (var6 < 0) {
                              var6 = -var6;
                           }

                           var7 = pl[var1] >> 8 & 255;
                           var8 = var7 * 128 + 64 - az.il.regionY;
                           if (var8 < 0) {
                              var8 = -var8;
                           }

                           var9 = var6 + var8 - 128;
                           if (var9 > var4) {
                              pt[var1] = -100;
                              continue;
                           }

                           if (var9 < 0) {
                              var9 = 0;
                           }

                           var3 = (var4 - var9) * pa / var4;
                        } else {
                           var3 = ow;
                        }

                        if (var3 > 0) {
                           DataRequestNode var24 = var37.q().t(AppletParameter.pp);
                           dy var25 = dy.i(var24, 100, var3);
                           var25.o(pe[var1] - 1);
                           kq.pd.t(var25);
                        }

                        pt[var1] = -100;
                     }
                  } else {
                     --audioEffectCount;

                     for(var2 = var1; var2 < audioEffectCount; ++var2) {
                        po[var2] = po[var2 + 1];
                        audioTracks[var2] = audioTracks[var2 + 1];
                        pe[var2] = pe[var2 + 1];
                        pt[var2] = pt[var2 + 1];
                        pl[var2] = pl[var2 + 1];
                     }

                     --var1;
                  }
               }

               if (oe) {
                  boolean var31;
                  if (hi.audioTrackId != 0) {
                     var31 = true;
                  } else {
                     var31 = hi.audioTask.k();
                  }

                  if (!var31) {
                     if (os != 0 && ox != -1) {
                        CombatBarData.i(Varpbit.ca, ox, 0, os, false);
                     }

                     oe = false;
                  }
               }

               ++ee.g;
               if (ee.g > 750) {
                  av.fz();
               } else {
                  hd.fi();
                  eh.fn();
                  RuneScriptStackItem.fr();
                  ++fs;
                  if (mouseCrosshairState != 0) {
                     hb += 20;
                     if (hb >= 400) {
                        mouseCrosshairState = 0;
                     }
                  }

                  if (MouseTracker.ie != null) {
                     ++iq;
                     if (iq >= 15) {
                        GameEngine.jk(MouseTracker.ie);
                        MouseTracker.ie = null;
                     }
                  }

                  RTComponent var38 = av.kp;
                  RTComponent var39 = jk.kz;
                  av.kp = null;
                  jk.kz = null;
                  ld = null;
                  lg = false;
                  lk = false;
                  og = 0;

                  while(ko.l() && og < 128) {
                     if (localPlayerRights >= 2 && ad.pressedKeys[82] && cr.cd == 66) {
                        String var26 = bj.o();
                        ir.ac.o(var26);
                     } else if (gn != 1 || h.cv <= 0) {
                        ol[og] = cr.cd;
                        or[og] = h.cv;
                        ++og;
                     }
                  }

                  boolean var32 = localPlayerRights >= 2;
                  if (var32 && ad.pressedKeys[82] && ad.pressedKeys[81] && mw != 0) {
                     var4 = az.il.r - mw;
                     if (var4 < 0) {
                        var4 = 0;
                     } else if (var4 > 3) {
                        var4 = 3;
                     }

                     if (var4 != az.il.r) {
                        RSRandomAccessFile.kw(az.il.co[0] + an.regionBaseX, az.il.cv[0] + PlayerComposite.ep, var4, false);
                     }

                     mw = 0;
                  }

                  if (hudIndex != -1) {
                     var4 = hudIndex;
                     var5 = BoundaryObject.r;
                     var6 = GameEngine.h;
                     if (RuneScript.i(var4)) {
                        gh.im(RTComponent.b[var4], -1, 0, 0, var5, var6, 0, 0);
                     }
                  }

                  ++mn;

                  while(true) {
                     RTComponent var40;
                     ScriptEvent var41;
                     RTComponent var42;
                     do {
                        var41 = (ScriptEvent)mh.l();
                        if (var41 == null) {
                           while(true) {
                              do {
                                 var41 = (ScriptEvent)ml.l();
                                 if (var41 == null) {
                                    while(true) {
                                       do {
                                          var41 = (ScriptEvent)me.l();
                                          if (var41 == null) {
                                             this.ha();
                                             if (ip.rz != null) {
                                                ip.rz.k(kt.ii, (az.il.regionX >> 7) + an.regionBaseX, (az.il.regionY >> 7) + PlayerComposite.ep, false);
                                                ip.rz.au();
                                             }

                                             if (ln != null) {
                                                this.in();
                                             }

                                             if (ak.ik != null) {
                                                GameEngine.jk(ak.ik);
                                                ++ih;
                                                if (bs.g == 0) {
                                                   if (iw) {
                                                      if (ak.ik == c.if && ic != is) {
                                                         RTComponent var43 = ak.ik;
                                                         byte var33 = 0;
                                                         if (lq == 1 && var43.contentType == 206) {
                                                            var33 = 1;
                                                         }

                                                         if (var43.itemIds[ic] <= 0) {
                                                            var33 = 0;
                                                         }

                                                         if (eh.a(u.jr(var43))) {
                                                            var6 = is;
                                                            var7 = ic;
                                                            var43.itemIds[var7] = var43.itemIds[var6];
                                                            var43.itemStackSizes[var7] = var43.itemStackSizes[var6];
                                                            var43.itemIds[var6] = -1;
                                                            var43.itemStackSizes[var6] = 0;
                                                         } else if (var33 == 1) {
                                                            var6 = is;
                                                            var7 = ic;

                                                            while(var6 != var7) {
                                                               if (var6 > var7) {
                                                                  var43.x(var6 - 1, var6);
                                                                  --var6;
                                                               } else if (var6 < var7) {
                                                                  var43.x(var6 + 1, var6);
                                                                  ++var6;
                                                               }
                                                            }
                                                         } else {
                                                            var43.x(ic, is);
                                                         }

                                                         var19 = ap.t(fo.ct, ee.l);
                                                         var19.i.bw(ak.ik.id);
                                                         var19.i.by(is);
                                                         var19.i.l(ic);
                                                         var19.i.bk(var33);
                                                         ee.i(var19);
                                                      }
                                                   } else if (this.hg()) {
                                                      this.hc(io, ig);
                                                   } else if (menuSize > 0) {
                                                      er.ia(io, ig);
                                                   }

                                                   iq = 10;
                                                   bs.j = 0;
                                                   ak.ik = null;
                                                } else if (ih >= 5 && (bs.n > io + 5 || bs.n < io - 5 || bs.x > ig + 5 || bs.x < ig - 5)) {
                                                   iw = true;
                                                }
                                             }

                                             if (Region.aa()) {
                                                var4 = Region.au;
                                                var5 = Region.ax;
                                                var19 = ap.t(fo.bi, ee.l);
                                                var19.i.a(5);
                                                var19.i.bt(var5 + PlayerComposite.ep);
                                                var19.i.bk(ad.pressedKeys[82] ? (ad.pressedKeys[81] ? 2 : 1) : 0);
                                                var19.i.bn(var4 + an.regionBaseX);
                                                ee.i(var19);
                                                Region.af();
                                                lastSelectedInterfaceUID = bs.cameraZ;
                                                lastSelectedComponentId = bs.z;
                                                mouseCrosshairState = 1;
                                                hb = 0;
                                                destinationX = var4;
                                                destinationY = var5;
                                             }

                                             if (var38 != av.kp) {
                                                if (var38 != null) {
                                                   GameEngine.jk(var38);
                                                }

                                                if (av.kp != null) {
                                                   GameEngine.jk(av.kp);
                                                }
                                             }

                                             if (var39 != jk.kz && ke == kx) {
                                                if (var39 != null) {
                                                   GameEngine.jk(var39);
                                                }

                                                if (jk.kz != null) {
                                                   GameEngine.jk(jk.kz);
                                                }
                                             }

                                             if (jk.kz != null) {
                                                if (kx < ke) {
                                                   ++kx;
                                                   if (kx == ke) {
                                                      GameEngine.jk(jk.kz);
                                                   }
                                                }
                                             } else if (kx > 0) {
                                                --kx;
                                             }

                                             int var12;
                                             int var13;
                                             int var27;
                                             if (gn == 0) {
                                                var4 = az.il.regionX;
                                                var5 = az.il.regionY;
                                                if (am.gz - var4 < -500 || am.gz - var4 > 500 || gf.gw - var5 < -500 || gf.gw - var5 > 500) {
                                                   am.gz = var4;
                                                   gf.gw = var5;
                                                }

                                                if (var4 != am.gz) {
                                                   am.gz += (var4 - am.gz) / 16;
                                                }

                                                if (var5 != gf.gw) {
                                                   gf.gw += (var5 - gf.gw) / 16;
                                                }

                                                var6 = am.gz >> 7;
                                                var7 = gf.gw >> 7;
                                                var8 = ef.gn(am.gz, gf.gw, kt.ii);
                                                var9 = 0;
                                                if (var6 > 3 && var7 > 3 && var6 < 100 && var7 < 100) {
                                                   for(var10 = var6 - 4; var10 <= var6 + 4; ++var10) {
                                                      for(var27 = var7 - 4; var27 <= var7 + 4; ++var27) {
                                                         var12 = kt.ii;
                                                         if (var12 < 3 && (bt.landscapeData[1][var10][var27] & 2) == 2) {
                                                            ++var12;
                                                         }

                                                         var13 = var8 - bt.tileHeights[var12][var10][var27];
                                                         if (var13 > var9) {
                                                            var9 = var13;
                                                         }
                                                      }
                                                   }
                                                }

                                                var10 = var9 * 192;
                                                if (var10 > 98048) {
                                                   var10 = 98048;
                                                }

                                                if (var10 < 32768) {
                                                   var10 = 32768;
                                                }

                                                if (var10 > hp) {
                                                   hp += (var10 - hp) / 24;
                                                } else if (var10 < hp) {
                                                   hp += (var10 - hp) / 80;
                                                }

                                                ez.gv = ef.gn(az.il.regionX, az.il.regionY, kt.ii) - gl;
                                             } else if (gn == 1) {
                                                if (hz && az.il != null) {
                                                   var4 = az.il.co[0];
                                                   var5 = az.il.cv[0];
                                                   if (var4 >= 0 && var5 >= 0 && var4 < 104 && var5 < 104) {
                                                      am.gz = az.il.regionX;
                                                      ez.gv = ef.gn(az.il.regionX, az.il.regionY, kt.ii) - gl;
                                                      gf.gw = az.il.regionY;
                                                      hz = false;
                                                   }
                                                }

                                                short var34 = -1;
                                                if (ad.pressedKeys[33]) {
                                                   var34 = 0;
                                                } else if (ad.pressedKeys[49]) {
                                                   var34 = 1024;
                                                }

                                                if (ad.pressedKeys[48]) {
                                                   if (var34 == 0) {
                                                      var34 = 1792;
                                                   } else if (var34 == 1024) {
                                                      var34 = 1280;
                                                   } else {
                                                      var34 = 1536;
                                                   }
                                                } else if (ad.pressedKeys[50]) {
                                                   if (var34 == 0) {
                                                      var34 = 256;
                                                   } else if (var34 == 1024) {
                                                      var34 = 768;
                                                   } else {
                                                      var34 = 512;
                                                   }
                                                }

                                                byte var35 = 0;
                                                if (ad.pressedKeys[35]) {
                                                   var35 = -1;
                                                } else if (ad.pressedKeys[51]) {
                                                   var35 = 1;
                                                }

                                                var6 = 0;
                                                if (var34 >= 0 || var35 != 0) {
                                                   var6 = ad.pressedKeys[81] ? hq : ht;
                                                   var6 *= 16;
                                                   gj = var34;
                                                   gm = var35;
                                                }

                                                if (gx < var6) {
                                                   gx += var6 / 8;
                                                   if (gx > var6) {
                                                      gx = var6;
                                                   }
                                                } else if (gx > var6) {
                                                   gx = gx * 9 / 10;
                                                }

                                                if (gx > 0) {
                                                   var7 = gx / 16;
                                                   if (gj >= 0) {
                                                      var4 = gj - bt.cameraYaw & 2047;
                                                      var8 = eu.m[var4];
                                                      var9 = eu.ay[var4];
                                                      am.gz += var8 * var7 / 65536;
                                                      gf.gw += var7 * var9 / 65536;
                                                   }

                                                   if (gm != 0) {
                                                      ez.gv += var7 * gm;
                                                      if (ez.gv > 0) {
                                                         ez.gv = 0;
                                                      }
                                                   }
                                                } else {
                                                   gj = -1;
                                                   gm = -1;
                                                }

                                                if (ad.pressedKeys[13]) {
                                                   o.kk();
                                                }
                                             }

                                             if (bs.g == 4 && er.ch) {
                                                var4 = bs.x - gr;
                                                gf = var4 * 2;
                                                gr = var4 != -1 && var4 != 1 ? (bs.x + gr) / 2 : bs.x;
                                                var5 = gt - bs.n;
                                                gc = var5 * 2;
                                                gt = var5 != -1 && var5 != 1 ? (gt + bs.n) / 2 : bs.n;
                                             } else {
                                                if (ad.pressedKeys[96]) {
                                                   gc += (-24 - gc) / 2;
                                                } else if (ad.pressedKeys[97]) {
                                                   gc += (24 - gc) / 2;
                                                } else {
                                                   gc /= 2;
                                                }

                                                if (ad.pressedKeys[98]) {
                                                   gf += (12 - gf) / 2;
                                                } else if (ad.pressedKeys[99]) {
                                                   gf += (-12 - gf) / 2;
                                                } else {
                                                   gf /= 2;
                                                }

                                                gr = bs.x;
                                                gt = bs.n;
                                             }

                                             hintPlane = gc / 2 + hintPlane & 2047;
                                             gq += gf / 2;
                                             if (gq < 128) {
                                                gq = 128;
                                             }

                                             if (gq > 383) {
                                                gq = 383;
                                             }

                                             if (px) {
                                                var4 = ga.pj * 128 + 64;
                                                var5 = r.ps * 128 + 64;
                                                var6 = ef.gn(var4, var5, kt.ii) - ae.pq;
                                                if (RuneScriptVM.go < var4) {
                                                   RuneScriptVM.go = (var4 - RuneScriptVM.go) * bj.pc / 1000 + RuneScriptVM.go + o.pw;
                                                   if (RuneScriptVM.go > var4) {
                                                      RuneScriptVM.go = var4;
                                                   }
                                                }

                                                if (RuneScriptVM.go > var4) {
                                                   RuneScriptVM.go -= bj.pc * (RuneScriptVM.go - var4) / 1000 + o.pw;
                                                   if (RuneScriptVM.go < var4) {
                                                      RuneScriptVM.go = var4;
                                                   }
                                                }

                                                if (fk.gs < var6) {
                                                   fk.gs = (var6 - fk.gs) * bj.pc / 1000 + fk.gs + o.pw;
                                                   if (fk.gs > var6) {
                                                      fk.gs = var6;
                                                   }
                                                }

                                                if (fk.gs > var6) {
                                                   fk.gs -= bj.pc * (fk.gs - var6) / 1000 + o.pw;
                                                   if (fk.gs < var6) {
                                                      fk.gs = var6;
                                                   }
                                                }

                                                if (n.gp < var5) {
                                                   n.gp = (var5 - n.gp) * bj.pc / 1000 + n.gp + o.pw;
                                                   if (n.gp > var5) {
                                                      n.gp = var5;
                                                   }
                                                }

                                                if (n.gp > var5) {
                                                   n.gp -= bj.pc * (n.gp - var5) / 1000 + o.pw;
                                                   if (n.gp < var5) {
                                                      n.gp = var5;
                                                   }
                                                }

                                                var4 = im.pv * 128 + 64;
                                                var5 = ao.pr * 128 + 64;
                                                var6 = ef.gn(var4, var5, kt.ii) - i.pm;
                                                var7 = var4 - RuneScriptVM.go;
                                                var8 = var6 - fk.gs;
                                                var9 = var5 - n.gp;
                                                var10 = (int)Math.sqrt((double)(var7 * var7 + var9 * var9));
                                                var27 = (int)(Math.atan2((double)var8, (double)var10) * 325.949D) & 2047;
                                                var12 = (int)(Math.atan2((double)var7, (double)var9) * -325.949D) & 2047;
                                                if (var27 < 128) {
                                                   var27 = 128;
                                                }

                                                if (var27 > 383) {
                                                   var27 = 383;
                                                }

                                                if (ap.gi < var27) {
                                                   ap.gi = (var27 - ap.gi) * Canvas.pz / 1000 + ap.gi + f.pu;
                                                   if (ap.gi > var27) {
                                                      ap.gi = var27;
                                                   }
                                                }

                                                if (ap.gi > var27) {
                                                   ap.gi -= Canvas.pz * (ap.gi - var27) / 1000 + f.pu;
                                                   if (ap.gi < var27) {
                                                      ap.gi = var27;
                                                   }
                                                }

                                                var13 = var12 - bt.cameraYaw;
                                                if (var13 > 1024) {
                                                   var13 -= 2048;
                                                }

                                                if (var13 < -1024) {
                                                   var13 += 2048;
                                                }

                                                if (var13 > 0) {
                                                   bt.cameraYaw = var13 * Canvas.pz / 1000 + bt.cameraYaw + f.pu;
                                                   bt.cameraYaw &= 2047;
                                                }

                                                if (var13 < 0) {
                                                   bt.cameraYaw -= f.pu + -var13 * Canvas.pz / 1000;
                                                   bt.cameraYaw &= 2047;
                                                }

                                                int var14 = var12 - bt.cameraYaw;
                                                if (var14 > 1024) {
                                                   var14 -= 2048;
                                                }

                                                if (var14 < -1024) {
                                                   var14 += 2048;
                                                }

                                                if (var14 < 0 && var13 > 0 || var14 > 0 && var13 < 0) {
                                                   bt.cameraYaw = var12;
                                                }
                                             }

                                             for(var4 = 0; var4 < 5; ++var4) {
                                                ++qs[var4];
                                             }

                                             g.vm.p();
                                             var4 = ++bs.l - 1;
                                             var6 = er.b();
                                             gd var28;
                                             if (var4 > 15000 && var6 > 15000) {
                                                ei = 250;
                                                ap.i(14500);
                                                var28 = ap.t(fo.cw, ee.l);
                                                ee.i(var28);
                                             }

                                             BoundaryObject.qi.a();
                                             ++ee.n;
                                             if (ee.n > 50) {
                                                var28 = ap.t(fo.m, ee.l);
                                                ee.i(var28);
                                             }

                                             try {
                                                ee.q();
                                             } catch (IOException var29) {
                                                av.fz();
                                             }

                                             return;
                                          }

                                          var42 = var41.i;
                                          if (var42.w < 0) {
                                             break;
                                          }

                                          var40 = Inflater.t(var42.parentId);
                                       } while(var40 == null || var40.cs2components == null || var42.w >= var40.cs2components.length || var42 != var40.cs2components[var42.w]);

                                       m.t(var41, 500000);
                                    }
                                 }

                                 var42 = var41.i;
                                 if (var42.w < 0) {
                                    break;
                                 }

                                 var40 = Inflater.t(var42.parentId);
                              } while(var40 == null || var40.cs2components == null || var42.w >= var40.cs2components.length || var42 != var40.cs2components[var42.w]);

                              m.t(var41, 500000);
                           }
                        }

                        var42 = var41.i;
                        if (var42.w < 0) {
                           break;
                        }

                        var40 = Inflater.t(var42.parentId);
                     } while(var40 == null || var40.cs2components == null || var42.w >= var40.cs2components.length || var42 != var40.cs2components[var42.w]);

                     m.t(var41, 500000);
                  }
               }
            }
         }
      }
   }

    @ObfuscatedName("fo")
    void fo() {
        int var1 = BoundaryObject.r;
        int var2 = GameEngine.h;
        if (super.z < var1) {
            var1 = super.z;
        }

        if (super.s < var2) {
            var2 = super.s;
        }

        if (al.qp != null) {
            try {
                br.q(ir.ac, "resize", new Object[] { bg.fq() });
            } catch (Throwable var4) {
                ;
            }
        }

    }

    @ObfuscatedName("gu")
   final void gu() {
      int var1;
      if (hudIndex != -1) {
         var1 = hudIndex;
         if (RuneScript.i(var1)) {
            r.ju(RTComponent.b[var1], -1);
         }
      }

      for(var1 = 0; var1 < nm; ++var1) {
         if (outdatedInterfaces[var1]) {
            nt[var1] = true;
         }

         nj[var1] = outdatedInterfaces[var1];
         outdatedInterfaces[var1] = false;
      }

      nq = bz;
      kj = -1;
      ku = -1;
      c.if = null;
      if (hudIndex != -1) {
         nm = 0;
         kq.ix(hudIndex, 0, 0, BoundaryObject.r, GameEngine.h, 0, 0, -1);
      }

      li.cp();
      int var2;
      int var3;
      if (!menuOpen) {
         if (kj != -1) {
            var1 = kj;
            var2 = ku;
            if ((menuSize >= 2 || inventoryItemSelectionState != 0 || interfaceSelected) && ky) {
               var3 = menuSize - 1;
               String var5;
               if (inventoryItemSelectionState == 1 && menuSize < 2) {
                  var5 = "Use" + " " + lastSelectedInventoryItemName + " " + "->";
               } else if (interfaceSelected && menuSize < 2) {
                  var5 = lj + " " + selectedSpellName + " " + "->";
               } else {
                  var5 = aw.hb(var3);
               }

               if (menuSize > 2) {
                  var5 = var5 + ar.q(16777215) + " " + '/' + " " + (menuSize - 2) + " more options";
               }

               b.ev.ae(var5, var1 + 4, var2 + 15, 16777215, 0, bz / 1000);
            }
         }
      } else {
         aw.hh();
      }

      if (nu == 3) {
         for(var1 = 0; var1 < nm; ++var1) {
            if (nj[var1]) {
               li.dc(interfaceXPositions[var1], interfaceYPositions[var1], interfaceWidths[var1], interfaceHeights[var1], 16711935, 128);
            } else if (nt[var1]) {
               li.dc(interfaceXPositions[var1], interfaceYPositions[var1], interfaceWidths[var1], interfaceHeights[var1], 16711680, 128);
            }
         }
      }

      var1 = kt.ii;
      var2 = az.il.regionX;
      var3 = az.il.regionY;
      int var4 = fs;

      for(AreaSoundEmitter var13 = (AreaSoundEmitter)AreaSoundEmitter.t.e(); var13 != null; var13 = (AreaSoundEmitter)AreaSoundEmitter.t.p()) {
         if (var13.ambientSoundId != -1 || var13.shufflingSoundIds != null) {
            int var6 = 0;
            if (var2 > var13.l) {
               var6 += var2 - var13.l;
            } else if (var2 < var13.i) {
               var6 += var13.i - var2;
            }

            if (var3 > var13.b) {
               var6 += var3 - var13.b;
            } else if (var3 < var13.a) {
               var6 += var13.a - var3;
            }

            if (var6 - 64 <= var13.e && pa != 0 && var1 == var13.q) {
               var6 -= 64;
               if (var6 < 0) {
                  var6 = 0;
               }

               int var7 = (var13.e - var6) * pa / var13.e;
               Object var10000;
               if (var13.p == null) {
                  if (var13.ambientSoundId >= 0) {
                     var10000 = null;
                     AudioTrack var8 = AudioTrack.t(aj.cz, var13.ambientSoundId, 0);
                     if (var8 != null) {
                        DataRequestNode var9 = var8.q().t(AppletParameter.pp);
                        dy var10 = dy.i(var9, 100, var7);
                        var10.o(-1);
                        kq.pd.t(var10);
                        var13.p = var10;
                     }
                  }
               } else {
                  var13.p.u(var7);
               }

               if (var13.v == null) {
                  if (var13.shufflingSoundIds != null && (var13.c -= var4) <= 0) {
                     int var12 = (int)(Math.random() * (double)var13.shufflingSoundIds.length);
                     var10000 = null;
                     AudioTrack var14 = AudioTrack.t(aj.cz, var13.shufflingSoundIds[var12], 0);
                     if (var14 != null) {
                        DataRequestNode var15 = var14.q().t(AppletParameter.pp);
                        dy var11 = dy.i(var15, 100, var7);
                        var11.o(0);
                        kq.pd.t(var11);
                        var13.v = var11;
                        var13.c = var13.g + (int)(Math.random() * (double)(var13.n - var13.g));
                     }
                  }
               } else {
                  var13.v.u(var7);
                  if (!var13.v.kg()) {
                     var13.v = null;
                  }
               }
            } else {
               if (var13.p != null) {
                  kq.pd.q(var13.p);
                  var13.p = null;
               }

               if (var13.v != null) {
                  kq.pd.q(var13.v);
                  var13.v = null;
               }
            }
         }
      }

      fs = 0;
   }

    @ObfuscatedName("gm")
    final boolean gm(cn var1) {
        fy var2 = var1.e();
        Packet var3 = var1.b;
        if (var2 == null) {
            return false;
        } else {
            String var5;
            int var6;
            try {
                if (var1.e == null) {
                    if (var1.p) {
                        if (!var2.t(1)) {
                            return false;
                        }

                        var2.a(var1.b.buffer, 0, 1);
                        var1.g = 0;
                        var1.p = false;
                    }

                    var3.index = 0;
                    if (var3.ia()) {
                        if (!var2.t(1)) {
                            return false;
                        }

                        var2.a(var1.b.buffer, 1, 1);
                        var1.g = 0;
                    }

                    var1.p = true;
                    fx[] var4 = new fx[] { fx.t, fx.q, fx.i, fx.a, fx.l, fx.b, fx.e, fx.x, fx.p, fx.g, fx.n, fx.o,
                            fx.c, fx.v, fx.u, fx.j, fx.k, fx.z, fx.w, fx.s, fx.d, fx.f, fx.r, fx.y, fx.h, fx.m, fx.ay,
                            fx.ao, fx.av, fx.aj, fx.ae, fx.am, fx.az, fx.ap, fx.ah, fx.au, fx.ax, fx.ar, fx.an, fx.ai,
                            fx.al, fx.at, fx.ag, fx.as, fx.aw, fx.aq, fx.aa, fx.af, fx.ak, fx.ab, fx.ac, fx.ad, fx.bg,
                            fx.br, fx.ba, fx.bk, fx.be, fx.bc, fx.bm, fx.bh, fx.bs, fx.bj, fx.bt, fx.by, fx.bn, fx.bb,
                            fx.bq, fx.bz, fx.bx, fx.bf, fx.bo, fx.bv, fx.bi, fx.bu, fx.bl, fx.bw, fx.bp, fx.bd, fx.cb,
                            fx.cm, fx.cu, fx.cs, fx.ct, fx.cw };
                    var6 = var3.jk();
                    if (var6 < 0 || var6 >= var4.length) {
                        throw new IOException(var6 + " " + var3.index);
                    }

                    var1.e = var4[var6];
                    var1.x = var1.e.cr;
                }

                if (var1.x == -1) {
                    if (!var2.t(1)) {
                        return false;
                    }

                    var1.e().a(var3.buffer, 0, 1);
                    var1.x = var3.buffer[0] & 255;
                }

                if (var1.x == -2) {
                    if (!var2.t(2)) {
                        return false;
                    }

                    var1.e().a(var3.buffer, 0, 2);
                    var3.index = 0;
                    var1.x = var3.ae();
                }

                if (!var2.t(var1.x)) {
                    return false;
                }

                var3.index = 0;
                var2.a(var3.buffer, 0, var1.x);
                var1.g = 0;
                ey.t();
                var1.v = var1.c;
                var1.c = var1.o;
                var1.o = var1.e;
                int var20;
                int var21;
                RTComponent var87;
                if (fx.e == var1.e) {
                    var20 = var3.ae();
                    var21 = var3.bp();
                    var87 = Inflater.t(var21);
                    if (var87.entityType != 2 || var20 != var87.entityId) {
                        var87.entityType = 2;
                        var87.entityId = var20;
                        GameEngine.jk(var87);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.av == var1.e) {
                    for (var20 = 0; var20 < loadedPlayers.length; ++var20) {
                        if (loadedPlayers[var20] != null) {
                            loadedPlayers[var20].animationId = -1;
                        }
                    }

                    for (var20 = 0; var20 < loadedNpcs.length; ++var20) {
                        if (loadedNpcs[var20] != null) {
                            loadedNpcs[var20].animationId = -1;
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ao == var1.e) {
                    y.gl(false, var1.b);
                    var1.e = null;
                    return true;
                }

                if (fx.bi == var1.e) {
                    an.jc();
                    lx = var3.av();
                    mv = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.v == var1.e) {
                    BoundaryObject.qi.b.i(var3, var1.x);
                    h.e();
                    if (ad.ot != null) {
                        ad.ot.cy();
                    }

                    ms = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.x == var1.e) {
                    TileModel.gy(ga.x);
                    var1.e = null;
                    return true;
                }

                if (fx.am == var1.e) {
                    TileModel.gy(ga.e);
                    var1.e = null;
                    return true;
                }

                if (fx.k == var1.e) {
                    for (var20 = 0; var20 < iv.varps.length; ++var20) {
                        if (iv.q[var20] != iv.varps[var20]) {
                            iv.varps[var20] = iv.q[var20];
                            b.jg(var20);
                            mk[++ma - 1 & 31] = var20;
                        }
                    }

                    var1.e = null;
                    return true;
                }

                RTComponent var65;
                if (fx.ag == var1.e) {
                    var20 = var3.bf();
                    var21 = var3.bo();
                    var6 = var3.bp();
                    var65 = Inflater.t(var6);
                    if (var21 != var65.ay || var20 != var65.ao || var65.r != 0 || var65.y != 0) {
                        var65.ay = var21;
                        var65.ao = var20;
                        var65.r = 0;
                        var65.y = 0;
                        GameEngine.jk(var65);
                        this.io(var65);
                        if (var65.type == 0) {
                            GameEngine.ig(RTComponent.b[var6 >> 16], var65, false);
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ae == var1.e) {
                    px = false;

                    for (var20 = 0; var20 < 5; ++var20) {
                        pf[var20] = false;
                    }

                    var1.e = null;
                    return true;
                }

                SubWindow var7;
                RTComponent var68;
                if (fx.bk == var1.e) {
                    var20 = var3.bd();
                    var21 = var3.cb();
                    SubWindow var95 = (SubWindow) subWindowTable.t((long) var21);
                    var7 = (SubWindow) subWindowTable.t((long) var20);
                    if (var7 != null) {
                        cx.ji(var7, var95 == null || var95.targetWindowId != var7.targetWindowId);
                    }

                    if (var95 != null) {
                        var95.kc();
                        subWindowTable.q(var95, (long) var20);
                    }

                    var68 = Inflater.t(var21);
                    if (var68 != null) {
                        GameEngine.jk(var68);
                    }

                    var68 = Inflater.t(var20);
                    if (var68 != null) {
                        GameEngine.jk(var68);
                        GameEngine.ig(RTComponent.b[var68.id >>> 16], var68, true);
                    }

                    if (hudIndex != -1) {
                        g.iz(hudIndex, 1);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.p == var1.e) {
                    bn.fd = var3.av();
                    ez.fh = var3.bc();
                    var1.e = null;
                    return true;
                }

                if (fx.aw == var1.e) {
                    nb = var3.bc();
                    np = var3.bm();
                    var1.e = null;
                    return true;
                }

                if (fx.b == var1.e) {
                    var20 = var3.ae();
                    if (var20 == 65535) {
                        var20 = -1;
                    }

                    az.fs(var20);
                    var1.e = null;
                    return true;
                }

                if (fx.a == var1.e) {
                    var20 = var3.bi();
                    var21 = var3.bb();
                    if (var21 == 65535) {
                        var21 = -1;
                    }

                    ci.fy(var21, var20);
                    var1.e = null;
                    return true;
                }

                RTComponent var60;
                if (fx.l == var1.e) {
                    var20 = var3.bd();
                    var60 = Inflater.t(var20);
                    var60.entityType = 3;
                    var60.entityId = az.il.composite.p();
                    GameEngine.jk(var60);
                    var1.e = null;
                    return true;
                }

                int var9;
                int var11;
                int var12;
                int var14;
                int var16;
                int var17;
                int var23;
                int var24;
                int var27;
                int var81;
                int var93;
                if (fx.bf == var1.e) {
                    var20 = var1.x;
                    var21 = var3.index;
                    cx.c = 0;
                    var6 = 0;
                    var3.jc();

                    for (var23 = 0; var23 < cx.b; ++var23) {
                        var24 = cx.e[var23];
                        if ((cx.i[var24] & 1) == 0) {
                            if (var6 > 0) {
                                --var6;
                                cx.i[var24] = (byte) (cx.i[var24] | 2);
                            } else {
                                var9 = var3.jt(1);
                                if (var9 == 0) {
                                    var6 = ag.q(var3);
                                    cx.i[var24] = (byte) (cx.i[var24] | 2);
                                } else {
                                    PlayerComposite.i(var3, var24);
                                }
                            }
                        }
                    }

                    var3.ju();
                    if (var6 != 0) {
                        throw new RuntimeException();
                    }

                    var3.jc();

                    for (var23 = 0; var23 < cx.b; ++var23) {
                        var24 = cx.e[var23];
                        if ((cx.i[var24] & 1) != 0) {
                            if (var6 > 0) {
                                --var6;
                                cx.i[var24] = (byte) (cx.i[var24] | 2);
                            } else {
                                var9 = var3.jt(1);
                                if (var9 == 0) {
                                    var6 = ag.q(var3);
                                    cx.i[var24] = (byte) (cx.i[var24] | 2);
                                } else {
                                    PlayerComposite.i(var3, var24);
                                }
                            }
                        }
                    }

                    var3.ju();
                    if (var6 != 0) {
                        throw new RuntimeException();
                    }

                    var3.jc();

                    for (var23 = 0; var23 < cx.x; ++var23) {
                        var24 = cx.p[var23];
                        if ((cx.i[var24] & 1) != 0) {
                            if (var6 > 0) {
                                --var6;
                                cx.i[var24] = (byte) (cx.i[var24] | 2);
                            } else {
                                var9 = var3.jt(1);
                                if (var9 == 0) {
                                    var6 = ag.q(var3);
                                    cx.i[var24] = (byte) (cx.i[var24] | 2);
                                } else if (fl.a(var3, var24)) {
                                    cx.i[var24] = (byte) (cx.i[var24] | 2);
                                }
                            }
                        }
                    }

                    var3.ju();
                    if (var6 != 0) {
                        throw new RuntimeException();
                    }

                    var3.jc();

                    for (var23 = 0; var23 < cx.x; ++var23) {
                        var24 = cx.p[var23];
                        if ((cx.i[var24] & 1) == 0) {
                            if (var6 > 0) {
                                --var6;
                                cx.i[var24] = (byte) (cx.i[var24] | 2);
                            } else {
                                var9 = var3.jt(1);
                                if (var9 == 0) {
                                    var6 = ag.q(var3);
                                    cx.i[var24] = (byte) (cx.i[var24] | 2);
                                } else if (fl.a(var3, var24)) {
                                    cx.i[var24] = (byte) (cx.i[var24] | 2);
                                }
                            }
                        }
                    }

                    var3.ju();
                    if (var6 != 0) {
                        throw new RuntimeException();
                    }

                    cx.b = 0;
                    cx.x = 0;

                    Player var76;
                    for (var23 = 1; var23 < 2048; ++var23) {
                        cx.i[var23] = (byte) (cx.i[var23] >> 1);
                        var76 = loadedPlayers[var23];
                        if (var76 != null) {
                            cx.e[++cx.b - 1] = var23;
                        } else {
                            cx.p[++cx.x - 1] = var23;
                        }
                    }

                    for (var6 = 0; var6 < cx.c; ++var6) {
                        var23 = cx.v[var6];
                        var76 = loadedPlayers[var23];
                        var9 = var3.av();
                        if ((var9 & 128) != 0) {
                            var9 += var3.av() << 8;
                        }

                        byte var77 = -1;
                        if ((var9 & 16) != 0) {
                            var11 = var3.bb();
                            if (var11 == 65535) {
                                var11 = -1;
                            }

                            var12 = var3.bm();
                            Inflater.fv(var76, var11, var12);
                        }

                        if ((var9 & 1024) != 0) {
                            var77 = var3.bs();
                        }

                        if ((var9 & 8) != 0) {
                            var76.interactingEntityIndex = var3.bz();
                            if (var76.interactingEntityIndex == 65535) {
                                var76.interactingEntityIndex = -1;
                            }
                        }

                        if ((var9 & 2048) != 0) {
                            var76.bu = var3.aj();
                            var76.bw = var3.bh();
                            var76.bl = var3.aj();
                            var76.bp = var3.bh();
                            var76.bd = var3.bb() + bz;
                            var76.cb = var3.ae() + bz;
                            var76.cm = var3.ae();
                            if (var76.ay) {
                                var76.bu += var76.ao;
                                var76.bw += var76.av;
                                var76.bl += var76.ao;
                                var76.bp += var76.av;
                                var76.speed = 0;
                            } else {
                                var76.bu += var76.co[0];
                                var76.bw += var76.cv[0];
                                var76.bl += var76.co[0];
                                var76.bp += var76.cv[0];
                                var76.speed = 1;
                            }

                            var76.ci = 0;
                        }

                        if ((var9 & 512) != 0) {
                            cx.a[var23] = var3.aj();
                        }

                        if ((var9 & 1) != 0) {
                            var76.bh = var3.ae();
                            if (var76.speed == 0) {
                                var76.ct = var76.bh;
                                var76.bh = -1;
                            }
                        }

                        if ((var9 & 32) != 0) {
                            var11 = var3.ae();
                            il[] var89 = new il[] { il.t, il.b, il.i, il.a, il.q, il.l };
                            il var13 = (il) aj.t(var89, var3.bm());
                            boolean var80 = var3.bc() == 1;
                            var81 = var3.bm();
                            var16 = var3.index;
                            if (var76.t != null && var76.composite != null) {
                                boolean var83 = false;
                                if (var13.g && BoundaryObject.qi.e(var76.t)) {
                                    var83 = true;
                                }

                                if (!var83 && ij == 0 && !var76.f) {
                                    cx.u.index = 0;
                                    var3.cm(cx.u.buffer, 0, var81);
                                    cx.u.index = 0;
                                    String var18 = RSFont.w(kx.c(ae.q(cx.u)));
                                    var76.textSpoken = var18.trim();
                                    var76.af = var11 >> 8;
                                    var76.ak = var11 & 255;
                                    var76.aa = 150;
                                    var76.aw = var80;
                                    var76.aq = var76 != az.il && var13.g && "" != nz
                                            && var18.toLowerCase().indexOf(nz) == -1;
                                    int var19;
                                    if (var13.p) {
                                        var19 = var80 ? 91 : 1;
                                    } else {
                                        var19 = var80 ? 90 : 2;
                                    }

                                    if (var13.x != -1) {
                                        j.t(var19, fp.t(var13.x) + var76.t.t(), var18);
                                    } else {
                                        j.t(var19, var76.t.t(), var18);
                                    }
                                }
                            }

                            var3.index = var16 + var81;
                        }

                        if ((var9 & 256) != 0) {
                            var76.bx = var3.bz();
                            var11 = var3.bd();
                            var76.bi = var11 >> 16;
                            var76.bv = (var11 & '\uffff') + bz;
                            var76.graphicId = 0;
                            var76.bo = 0;
                            if (var76.bv > bz) {
                                var76.graphicId = -1;
                            }

                            if (var76.bx == 65535) {
                                var76.bx = -1;
                            }
                        }

                        if ((var9 & 64) != 0) {
                            var76.textSpoken = var3.ar();
                            if (var76.textSpoken.charAt(0) == '~') {
                                var76.textSpoken = var76.textSpoken.substring(1);
                                j.t(2, var76.t.t(), var76.textSpoken);
                            } else if (var76 == az.il) {
                                j.t(2, var76.t.t(), var76.textSpoken);
                            }

                            var76.aw = false;
                            var76.af = 0;
                            var76.ak = 0;
                            var76.aa = 150;
                        }

                        if ((var9 & 4) != 0) {
                            var11 = var3.av();
                            byte[] var90 = new byte[var11];
                            ByteBuffer var78 = new ByteBuffer(var90);
                            var3.cm(var90, 0, var11);
                            cx.l[var23] = var78;
                            var76.t(var78);
                        }

                        if ((var9 & 2) != 0) {
                            var11 = var3.av();
                            if (var11 > 0) {
                                for (var12 = 0; var12 < var11; ++var12) {
                                    var14 = -1;
                                    var81 = -1;
                                    var16 = -1;
                                    var93 = var3.ag();
                                    if (var93 == 32767) {
                                        var93 = var3.ag();
                                        var81 = var3.ag();
                                        var14 = var3.ag();
                                        var16 = var3.ag();
                                    } else if (var93 != 32766) {
                                        var81 = var3.ag();
                                    } else {
                                        var93 = -1;
                                    }

                                    var17 = var3.ag();
                                    var76.ad(var93, var81, var14, var16, bz, var17);
                                }
                            }

                            var12 = var3.av();
                            if (var12 > 0) {
                                for (var93 = 0; var93 < var12; ++var93) {
                                    var14 = var3.ag();
                                    var81 = var3.ag();
                                    if (var81 != 32767) {
                                        var16 = var3.ag();
                                        var17 = var3.bc();
                                        var27 = var81 > 0 ? var3.be() : var17;
                                        var76.bg(var14, bz, var81, var16, var17, var27);
                                    } else {
                                        var76.br(var14);
                                    }
                                }
                            }
                        }

                        if ((var9 & 4096) != 0) {
                            for (var11 = 0; var11 < 3; ++var11) {
                                var76.b[var11] = var3.ar();
                            }
                        }

                        if (var76.ay) {
                            if (var77 == 127) {
                                var76.c(var76.ao, var76.av);
                            } else {
                                byte var79;
                                if (var77 != -1) {
                                    var79 = var77;
                                } else {
                                    var79 = cx.a[var23];
                                }

                                var76.o(var76.ao, var76.av, var79);
                            }
                        }
                    }

                    if (var20 != var3.index - var21) {
                        throw new RuntimeException(var3.index - var21 + " " + var20);
                    }

                    ab.ko();
                    var1.e = null;
                    return true;
                }

                long var28;
                long var30;
                long var32;
                String var56;
                if (fx.d == var1.e) {
                    var56 = var3.ar();
                    var28 = var3.ah();
                    var30 = (long) var3.ae();
                    var32 = (long) var3.az();
                    il[] var96 = new il[] { il.t, il.b, il.i, il.a, il.q, il.l };
                    il var25 = (il) aj.t(var96, var3.av());
                    long var35 = (var30 << 32) + var32;
                    boolean var82 = false;

                    for (var16 = 0; var16 < 100; ++var16) {
                        if (nx[var16] == var35) {
                            var82 = true;
                            break;
                        }
                    }

                    if (var25.g && BoundaryObject.qi.e(new kb(var56, ad.bc))) {
                        var82 = true;
                    }

                    if (!var82 && ij == 0) {
                        nx[nd] = var35;
                        nd = (nd + 1) % 100;
                        String var37 = RSFont.w(kx.c(ae.q(var3)));
                        if (var25.x != -1) {
                            bn.addMessage(9, fp.t(var25.x) + var56, var37, m.i(var28));
                        } else {
                            bn.addMessage(9, var56, var37, m.i(var28));
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bg == var1.e) {
                    for (var20 = 0; var20 < VarInfo.q; ++var20) {
                        VarInfo var67 = q(var20);
                        if (var67 != null) {
                            iv.q[var20] = 0;
                            iv.varps[var20] = 0;
                        }
                    }

                    an.jc();
                    ma += 32;
                    var1.e = null;
                    return true;
                }

                if (fx.g == var1.e) {
                    TileModel.gy(ga.i);
                    var1.e = null;
                    return true;
                }

                if (fx.q == var1.e) {
                    var21 = var3.av();
                    ll[] var94 = new ll[] { ll.t, ll.q, ll.i };
                    ll[] var74 = var94;
                    var24 = 0;

                    ll var61;
                    while (true) {
                        if (var24 >= var74.length) {
                            var61 = null;
                            break;
                        }

                        ll var99 = var74[var24];
                        if (var21 == var99.a) {
                            var61 = var99;
                            break;
                        }

                        ++var24;
                    }

                    gf.nh = var61;
                    var1.e = null;
                    return true;
                }

                String var8;
                String var22;
                long var39;
                String var98;
                if (fx.t == var1.e) {
                    var56 = var3.ar();
                    Projectile.bt = var56;

                    try {
                        var5 = ir.ac.getParameter(kg.c.k);
                        var22 = ir.ac.getParameter(kg.v.k);
                        String var69 = var5 + "settings=" + var56 + "; version=1; path=/; domain=" + var22;
                        if (var56.length() == 0) {
                            var69 = var69 + "; Expires=Thu, 01-Jan-1970 00:00:00 GMT; Max-Age=0";
                        } else {
                            var8 = var69 + "; Expires=";
                            var39 = au.t() + 94608000000L;
                            gx.i.setTime(new Date(var39));
                            var12 = gx.i.get(7);
                            var93 = gx.i.get(5);
                            var14 = gx.i.get(2);
                            var81 = gx.i.get(1);
                            var16 = gx.i.get(11);
                            var17 = gx.i.get(12);
                            var27 = gx.i.get(13);
                            var98 = gx.q[var12 - 1] + ", " + var93 / 10 + var93 % 10 + "-" + gx.t[0][var14] + "-"
                                    + var81 + " " + var16 / 10 + var16 % 10 + ":" + var17 / 10 + var17 % 10 + ":"
                                    + var27 / 10 + var27 % 10 + " GMT";
                            var69 = var8 + var98 + "; Max-Age=" + 94608000L;
                        }

                        Client var75 = ir.ac;
                        var98 = "document.cookie=\"" + var69 + "\"";
                        JSObject.getWindow(var75).eval(var98);
                    } catch (Throwable var52) {
                        ;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ap == var1.e) {
                    an.jc();
                    var20 = var3.be();
                    var21 = var3.bc();
                    var6 = var3.bp();
                    skillExperiences[var21] = var6;
                    jb[var21] = var20;
                    baseSkillLevels[var21] = 1;

                    for (var23 = 0; var23 < 98; ++var23) {
                        if (var6 >= id.i[var23]) {
                            baseSkillLevels[var21] = var23 + 2;
                        }
                    }

                    mg[++my - 1 & 31] = var21;
                    var1.e = null;
                    return true;
                }

                if (fx.bj == var1.e) {
                    y.gl(true, var1.b);
                    var1.e = null;
                    return true;
                }

                if (fx.s == var1.e) {
                    var20 = var3.cb();
                    var21 = var3.bz();
                    var6 = var3.bz();
                    var65 = Inflater.t(var20);
                    var65.bu = var6 + (var21 << 16);
                    var1.e = null;
                    return true;
                }

                if (fx.bs == var1.e) {
                    var20 = var3.ap();
                    var21 = var3.ae();
                    if (var20 < -70000) {
                        var21 += 32768;
                    }

                    if (var20 >= 0) {
                        var87 = Inflater.t(var20);
                    } else {
                        var87 = null;
                    }

                    for (; var3.index < var1.x; f.setContainer(var21, var23, var24 - 1, var9)) {
                        var23 = var3.ag();
                        var24 = var3.ae();
                        var9 = 0;
                        if (var24 != 0) {
                            var9 = var3.av();
                            if (var9 == 255) {
                                var9 = var3.ap();
                            }
                        }

                        if (var87 != null && var23 >= 0 && var23 < var87.itemIds.length) {
                            var87.itemIds[var23] = var24;
                            var87.itemStackSizes[var23] = var9;
                        }
                    }

                    if (var87 != null) {
                        GameEngine.jk(var87);
                    }

                    an.jc();
                    mu[++mz - 1 & 31] = var21 & 32767;
                    var1.e = null;
                    return true;
                }

                if (fx.cm == var1.e) {
                    px = true;
                    ga.pj = var3.av();
                    r.ps = var3.av();
                    ae.pq = var3.ae();
                    o.pw = var3.av();
                    bj.pc = var3.av();
                    if (bj.pc >= 100) {
                        RuneScriptVM.go = ga.pj * 128 + 64;
                        n.gp = r.ps * 128 + 64;
                        fk.gs = ef.gn(RuneScriptVM.go, n.gp, kt.ii) - ae.pq;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.be == var1.e) {
                    TileModel.gy(ga.l);
                    var1.e = null;
                    return true;
                }

                if (fx.az == var1.e) {
                    var20 = var3.bp();
                    var21 = var3.bf();
                    var87 = Inflater.t(var20);
                    if (var21 != var87.animationId || var21 == -1) {
                        var87.animationId = var21;
                        var87.et = 0;
                        var87.ev = 0;
                        GameEngine.jk(var87);
                    }

                    var1.e = null;
                    return true;
                }

                boolean var91;
                if (fx.cw == var1.e) {
                    var91 = var3.au();
                    if (var91) {
                        if (cn.ry == null) {
                            cn.ry = new jo();
                        }
                    } else {
                        cn.ry = null;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.u == var1.e) {
                    var20 = var3.ap();
                    SubWindow var64 = (SubWindow) subWindowTable.t((long) var20);
                    if (var64 != null) {
                        cx.ji(var64, true);
                    }

                    if (ls != null) {
                        GameEngine.jk(ls);
                        ls = null;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bh == var1.e) {
                    var20 = var3.cb();
                    var21 = var3.bb();
                    var87 = Inflater.t(var20);
                    if (var87 != null && var87.type == 0) {
                        if (var21 > var87.at - var87.height) {
                            var21 = var87.at - var87.height;
                        }

                        if (var21 < 0) {
                            var21 = 0;
                        }

                        if (var21 != var87.verticalScrollbarPosition) {
                            var87.verticalScrollbarPosition = var21;
                            GameEngine.jk(var87);
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ba == var1.e) {
                    TileModel.gy(ga.q);
                    var1.e = null;
                    return true;
                }

                if (fx.al == var1.e) {
                    var20 = var3.bq();
                    hudIndex = var20;
                    this.is(false);
                    if (RuneScript.i(var20)) {
                        RTComponent[] var63 = RTComponent.b[var20];

                        for (var6 = 0; var6 < var63.length; ++var6) {
                            var65 = var63[var6];
                            if (var65 != null) {
                                var65.et = 0;
                                var65.ev = 0;
                            }
                        }
                    }

                    az.aj(hudIndex);

                    for (var21 = 0; var21 < 100; ++var21) {
                        outdatedInterfaces[var21] = true;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bx == var1.e) {
                    var20 = var3.ae();
                    if (var20 == 65535) {
                        var20 = -1;
                    }

                    var21 = var3.bp();
                    var6 = var3.cb();
                    var65 = Inflater.t(var21);
                    ItemDefinition var73;
                    if (!var65.modern) {
                        if (var20 == -1) {
                            var65.entityType = 0;
                            var1.e = null;
                            return true;
                        }

                        var73 = cs.loadItemDefinition(var20);
                        var65.entityType = 4;
                        var65.entityId = var20;
                        var65.bx = var73.f;
                        var65.bf = var73.r;
                        var65.bv = var73.d * 100 / var6;
                        GameEngine.jk(var65);
                    } else {
                        var65.itemId = var20;
                        var65.itemQuantity = var6;
                        var73 = cs.loadItemDefinition(var20);
                        var65.bx = var73.f;
                        var65.bf = var73.r;
                        var65.bo = var73.y;
                        var65.bq = var73.h;
                        var65.bz = var73.m;
                        var65.bv = var73.d;
                        if (var73.ay == 1) {
                            var65.bw = 1;
                        } else {
                            var65.bw = 2;
                        }

                        if (var65.bi > 0) {
                            var65.bv = var65.bv * 32 / var65.bi;
                        } else if (var65.av > 0) {
                            var65.bv = var65.bv * 32 / var65.av;
                        }

                        GameEngine.jk(var65);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.cu == var1.e) {
                    cq.i(var3, var1.x);
                    var1.e = null;
                    return true;
                }

                if (fx.bw == var1.e) {
                    TileModel.gy(ga.a);
                    var1.e = null;
                    return true;
                }

                if (fx.ay == var1.e) {
                    TileModel.gy(ga.t);
                    var1.e = null;
                    return true;
                }

                if (fx.r == var1.e) {
                    if (hudIndex != -1) {
                        g.iz(hudIndex, 0);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.m == var1.e) {
                    destinationX = var3.av();
                    if (destinationX == 255) {
                        destinationX = 0;
                    }

                    destinationY = var3.av();
                    if (destinationY == 255) {
                        destinationY = 0;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.h == var1.e) {
                    fh.he(true, var3);
                    var1.e = null;
                    return true;
                }

                if (fx.bo == var1.e) {
                    ez.fh = var3.bc();
                    bn.fd = var3.bc();

                    for (var20 = bn.fd; var20 < bn.fd + 8; ++var20) {
                        for (var21 = ez.fh; var21 < ez.fh + 8; ++var21) {
                            if (loadedGroundItems[kt.ii][var20][var21] != null) {
                                loadedGroundItems[kt.ii][var20][var21] = null;
                                Canvas.hj(var20, var21);
                            }
                        }
                    }

                    for (bl var59 = (bl) jw.e(); var59 != null; var59 = (bl) jw.p()) {
                        if (var59.i >= bn.fd && var59.i < bn.fd + 8 && var59.a >= ez.fh && var59.a < ez.fh + 8
                                && var59.t == kt.ii) {
                            var59.o = 0;
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bb == var1.e) {
                    var20 = var3.av();
                    var21 = var3.av();
                    var6 = var3.av();
                    var23 = var3.av();
                    pf[var20] = true;
                    pb[var20] = var21;
                    qh[var20] = var6;
                    currentSkillLevels[var20] = var23;
                    qs[var20] = 0;
                    var1.e = null;
                    return true;
                }

                if (fx.ak == var1.e) {
                    var56 = var3.ar();
                    Object[] var62 = new Object[var56.length() + 1];

                    for (var6 = var56.length() - 1; var6 >= 0; --var6) {
                        if (var56.charAt(var6) == 's') {
                            var62[var6 + 1] = var3.ar();
                        } else {
                            var62[var6 + 1] = new Integer(var3.ap());
                        }
                    }

                    var62[0] = new Integer(var3.ap());
                    ScriptEvent var92 = new ScriptEvent();
                    var92.args = var62;
                    m.t(var92, 500000);
                    var1.e = null;
                    return true;
                }

                if (fx.by == var1.e) {
                    var20 = var3.bp();
                    var21 = var3.bz();
                    iv.q[var21] = var20;
                    if (iv.varps[var21] != var20) {
                        iv.varps[var21] = var20;
                    }

                    b.jg(var21);
                    mk[++ma - 1 & 31] = var21;
                    var1.e = null;
                    return true;
                }

                if (fx.aj == var1.e) {
                    var20 = var3.be();
                    var5 = var3.ar();
                    var6 = var3.bc();
                    if (var20 >= 1 && var20 <= 8) {
                        if (var5.equalsIgnoreCase("null")) {
                            var5 = null;
                        }

                        playerActions[var20 - 1] = var5;
                        jn[var20 - 1] = var6 == 0;
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bl == var1.e) {
                    var20 = var3.bq();
                    m.b(var20);
                    mu[++mz - 1 & 31] = var20 & 32767;
                    var1.e = null;
                    return true;
                }

                if (fx.an == var1.e) {
                    var56 = var3.ar();
                    var21 = var3.bp();
                    var87 = Inflater.t(var21);
                    if (!var56.equals(var87.text)) {
                        var87.text = var56;
                        GameEngine.jk(var87);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ab == var1.e) {
                    Server var57 = new Server();
                    var57.domain = var3.ar();
                    var57.number = var3.ae();
                    var21 = var3.ap();
                    var57.type = var21;
                    d.ea(45);
                    var2.b();
                    var2 = null;
                    aq.k(var57);
                    var1.e = null;
                    return false;
                }

                boolean var84;
                if (fx.f == var1.e) {
                    var20 = var3.bp();
                    var84 = var3.be() == 1;
                    var87 = Inflater.t(var20);
                    if (var84 != var87.hidden) {
                        var87.hidden = var84;
                        GameEngine.jk(var87);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bn == var1.e) {
                    oo = var3.av();
                    var1.e = null;
                    return true;
                }

                if (fx.z == var1.e) {
                    var56 = var3.ar();

                    try {
                        var23 = var3.ag();
                        if (var23 > 32767) {
                            var23 = 32767;
                        }

                        byte[] var72 = new byte[var23];
                        var3.index += lw.t.q(var3.buffer, var3.index, var72, 0, var23);
                        var98 = ChatboxMessage.a(var72, 0, var23);
                        var22 = var98;
                    } catch (Exception var51) {
                        var22 = "Cabbage";
                    }

                    var22 = RSFont.w(kx.c(var22));
                    j.t(6, var56, var22);
                    var1.e = null;
                    return true;
                }

                if (fx.o == var1.e) {
                    TileModel.gy(ga.b);
                    var1.e = null;
                    return true;
                }

                if (fx.aq == var1.e) {
                    var20 = var3.bq();
                    var21 = var3.bd();
                    var87 = Inflater.t(var21);
                    if (var87.entityType != 1 || var20 != var87.entityId) {
                        var87.entityType = 1;
                        var87.entityId = var20;
                        GameEngine.jk(var87);
                    }

                    var1.e = null;
                    return true;
                }

                int var10;
                if (fx.w == var1.e) {
                    var20 = var3.ap();
                    var21 = var3.ae();
                    if (var20 < -70000) {
                        var21 += 32768;
                    }

                    if (var20 >= 0) {
                        var87 = Inflater.t(var20);
                    } else {
                        var87 = null;
                    }

                    if (var87 != null) {
                        for (var23 = 0; var23 < var87.itemIds.length; ++var23) {
                            var87.itemIds[var23] = 0;
                            var87.itemStackSizes[var23] = 0;
                        }
                    }

                    fl.l(var21);
                    var23 = var3.ae();

                    for (var24 = 0; var24 < var23; ++var24) {
                        var9 = var3.bb();
                        var10 = var3.bm();
                        if (var10 == 255) {
                            var10 = var3.ap();
                        }

                        if (var87 != null && var24 < var87.itemIds.length) {
                            var87.itemIds[var24] = var9;
                            var87.itemStackSizes[var24] = var10;
                        }

                        f.setContainer(var21, var24, var9 - 1, var10);
                    }

                    if (var87 != null) {
                        GameEngine.jk(var87);
                    }

                    an.jc();
                    mu[++mz - 1 & 31] = var21 & 32767;
                    var1.e = null;
                    return true;
                }

                if (fx.ad == var1.e) {
                    hintArrowType = var3.av();
                    if (hintArrowType == 1) {
                        hintNpcIndex = var3.ae();
                    }

                    if (hintArrowType >= 2 && hintArrowType <= 6) {
                        if (hintArrowType == 2) {
                            ct = 64;
                            cw = 64;
                        }

                        if (hintArrowType == 3) {
                            ct = 0;
                            cw = 64;
                        }

                        if (hintArrowType == 4) {
                            ct = 128;
                            cw = 64;
                        }

                        if (hintArrowType == 5) {
                            ct = 64;
                            cw = 0;
                        }

                        if (hintArrowType == 6) {
                            ct = 64;
                            cw = 128;
                        }

                        hintArrowType = 2;
                        hintX = var3.ae();
                        hintY = var3.ae();
                        cs = var3.av();
                    }

                    if (hintArrowType == 10) {
                        hintPlayerIndex = var3.ae();
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.cs == var1.e) {
                    var91 = var3.av() == 1;
                    if (var91) {
                        gx.qy = au.t() - var3.ah();
                        es.qe = new v(var3, true);
                    } else {
                        es.qe = null;
                    }

                    mi = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.j == var1.e) {
                    var20 = var3.bq();
                    if (var20 == 65535) {
                        var20 = -1;
                    }

                    var21 = var3.bb();
                    if (var21 == 65535) {
                        var21 = -1;
                    }

                    var6 = var3.ap();
                    var23 = var3.bp();

                    for (var24 = var21; var24 <= var20; ++var24) {
                        var32 = ((long) var6 << 32) + (long) var24;
                        Node var34 = mf.t(var32);
                        if (var34 != null) {
                            var34.kc();
                        }

                        mf.q(new hc(var23), var32);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ct == var1.e) {
                    fh.he(false, var3);
                    var1.e = null;
                    return true;
                }

                if (fx.bm == var1.e) {
                    var20 = var3.av();
                    if (var3.av() == 0) {
                        exchangeOffers[var20] = new GrandExchangeOffer();
                        var3.index += 18;
                    } else {
                        --var3.index;
                        exchangeOffers[var20] = new GrandExchangeOffer(var3, false);
                    }

                    mo = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.ax == var1.e) {
                    var20 = var3.av();
                    AppletParameter.kf(var20);
                    var1.e = null;
                    return true;
                }

                if (fx.c == var1.e) {
                    var20 = var3.ae();
                    var21 = var3.av();
                    var6 = var3.ae();
                    AnimableNode.fh(var20, var21, var6);
                    var1.e = null;
                    return true;
                }

                if (fx.bc == var1.e) {
                    TileModel.gy(ga.g);
                    var1.e = null;
                    return true;
                }

                long var43;
                if (fx.at == var1.e) {
                    var20 = var3.ap();
                    var21 = var3.ap();
                    var23 = 0;
                    if (b.aa == null || !b.aa.isValid()) {
                        try {
                            Iterator var70 = ManagementFactory.getGarbageCollectorMXBeans().iterator();

                            while (var70.hasNext()) {
                                GarbageCollectorMXBean var97 = (GarbageCollectorMXBean) var70.next();
                                if (var97.isValid()) {
                                    b.aa = var97;
                                    GameEngine.ak = -1L;
                                    GameEngine.af = -1L;
                                }
                            }
                        } catch (Throwable var53) {
                            ;
                        }
                    }

                    if (b.aa != null) {
                        long var41 = au.t();
                        var39 = b.aa.getCollectionTime();
                        if (GameEngine.af != -1L) {
                            var43 = var39 - GameEngine.af;
                            long var45 = var41 - GameEngine.ak;
                            if (var45 != 0L) {
                                var23 = (int) (var43 * 100L / var45);
                            }
                        }

                        GameEngine.af = var39;
                        GameEngine.ak = var41;
                    }

                    gd var71 = ap.t(fo.n, ee.l);
                    var71.i.br(GameEngine.n);
                    var71.i.br(var23);
                    var71.i.bw(var20);
                    var71.i.bl(var21);
                    ee.i(var71);
                    var1.e = null;
                    return true;
                }

                if (fx.n == var1.e) {
                    BoundaryObject.qi.i(var3, var1.x);
                    ms = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.bv == var1.e) {
                    BoundaryObject.qi.q();
                    ms = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.bu == var1.e) {
                    ej.fu();
                    var1.e = null;
                    return false;
                }

                if (fx.au == var1.e) {
                    byte var85 = var3.bh();
                    var21 = var3.bb();
                    iv.q[var21] = var85;
                    if (iv.varps[var21] != var85) {
                        iv.varps[var21] = var85;
                    }

                    b.jg(var21);
                    mk[++ma - 1 & 31] = var21;
                    var1.e = null;
                    return true;
                }

                if (fx.ar == var1.e) {
                    an.jc();
                    ll = var3.am();
                    mv = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.br == var1.e) {
                    var20 = var3.cb();
                    var60 = Inflater.t(var20);

                    for (var6 = 0; var6 < var60.itemIds.length; ++var6) {
                        var60.itemIds[var6] = -1;
                        var60.itemIds[var6] = 0;
                    }

                    GameEngine.jk(var60);
                    var1.e = null;
                    return true;
                }

                if (fx.i == var1.e) {
                    var56 = var3.ar();
                    var28 = (long) var3.ae();
                    var30 = (long) var3.az();
                    il[] var38 = new il[] { il.t, il.b, il.i, il.a, il.q, il.l };
                    il var101 = (il) aj.t(var38, var3.av());
                    long var48 = (var28 << 32) + var30;
                    boolean var26 = false;

                    for (var14 = 0; var14 < 100; ++var14) {
                        if (nx[var14] == var48) {
                            var26 = true;
                            break;
                        }
                    }

                    if (BoundaryObject.qi.e(new kb(var56, ad.bc))) {
                        var26 = true;
                    }

                    if (!var26 && ij == 0) {
                        nx[nd] = var48;
                        nd = (nd + 1) % 100;
                        String var50 = RSFont.w(kx.c(ae.q(var3)));
                        byte var15;
                        if (var101.p) {
                            var15 = 7;
                        } else {
                            var15 = 3;
                        }

                        if (var101.x != -1) {
                            j.t(var15, fp.t(var101.x) + var56, var50);
                        } else {
                            j.t(var15, var56, var50);
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.as == var1.e) {
                    var20 = var3.ae();
                    var21 = var3.ap();
                    var6 = var3.be();
                    var7 = (SubWindow) subWindowTable.t((long) var21);
                    if (var7 != null) {
                        cx.ji(var7, var20 != var7.targetWindowId);
                    }

                    ej.jo(var21, var20, var6);
                    var1.e = null;
                    return true;
                }

                RTComponent var47;
                if (fx.aa == var1.e) {
                    var20 = var3.bz();
                    var21 = var3.ap();
                    var6 = var20 >> 10 & 31;
                    var23 = var20 >> 5 & 31;
                    var24 = var20 & 31;
                    var9 = (var23 << 11) + (var6 << 19) + (var24 << 3);
                    var47 = Inflater.t(var21);
                    if (var9 != var47.color) {
                        var47.color = var9;
                        GameEngine.jk(var47);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ac == var1.e) {
                    var20 = var3.bz();
                    var21 = var3.bz();
                    var6 = var3.bq();
                    var23 = var3.bp();
                    var68 = Inflater.t(var23);
                    if (var21 != var68.bx || var20 != var68.bf || var6 != var68.bv) {
                        var68.bx = var21;
                        var68.bf = var20;
                        var68.bv = var6;
                        GameEngine.jk(var68);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bz == var1.e) {
                    var20 = var3.index + var1.x;
                    var21 = var3.ae();
                    var6 = var3.ae();
                    if (var21 != hudIndex) {
                        hudIndex = var21;
                        this.is(false);
                        var23 = hudIndex;
                        if (RuneScript.i(var23)) {
                            RTComponent[] var66 = RTComponent.b[var23];

                            for (var9 = 0; var9 < var66.length; ++var9) {
                                var47 = var66[var9];
                                if (var47 != null) {
                                    var47.et = 0;
                                    var47.ev = 0;
                                }
                            }
                        }

                        az.aj(hudIndex);

                        for (var24 = 0; var24 < 100; ++var24) {
                            outdatedInterfaces[var24] = true;
                        }
                    }

                    SubWindow var100;
                    for (; var6-- > 0; var100.root = true) {
                        var23 = var3.ap();
                        var24 = var3.ae();
                        var9 = var3.av();
                        var100 = (SubWindow) subWindowTable.t((long) var23);
                        if (var100 != null && var24 != var100.targetWindowId) {
                            cx.ji(var100, true);
                            var100 = null;
                        }

                        if (var100 == null) {
                            var100 = ej.jo(var23, var24, var9);
                        }
                    }

                    for (var7 = (SubWindow) subWindowTable.a(); var7 != null; var7 = (SubWindow) subWindowTable.l()) {
                        if (var7.root) {
                            var7.root = false;
                        } else {
                            cx.ji(var7, true);
                        }
                    }

                    mf = new HashTable(512);

                    while (var3.index < var20) {
                        var23 = var3.ap();
                        var24 = var3.ae();
                        var9 = var3.ae();
                        var10 = var3.ap();

                        for (var11 = var24; var11 <= var9; ++var11) {
                            var43 = (long) var11 + ((long) var23 << 32);
                            mf.q(new hc(var10), var43);
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bd == var1.e) {
                    var3.index += 28;
                    if (var3.bg()) {
                        h.jy(var3, var3.index - 28);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.bt == var1.e) {
                    var20 = var3.ap();
                    if (var20 != hm) {
                        hm = var20;
                        if (gn == 1) {
                            hz = true;
                        }
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ai == var1.e) {
                    ez.fh = var3.av();
                    bn.fd = var3.bc();

                    while (var3.index < var1.x) {
                        var20 = var3.av();
                        ga[] var58 = new ga[] { ga.t, ga.q, ga.i, ga.a, ga.l, ga.b, ga.e, ga.x, ga.p, ga.g };
                        ga var86 = var58[var20];
                        TileModel.gy(var86);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.ah == var1.e) {
                    TileModel.gy(ga.p);
                    var1.e = null;
                    return true;
                }

                if (fx.y == var1.e) {
                    if (var1.x == 0) {
                        ad.ot = null;
                    } else {
                        if (ad.ot == null) {
                            ad.ot = new kl(ad.bc, ir.ac);
                        }

                        ad.ot.o(var3);
                    }

                    gx.js();
                    var1.e = null;
                    return true;
                }

                if (fx.cb == var1.e) {
                    var20 = var3.ag();
                    var84 = var3.av() == 1;
                    var22 = "";
                    boolean var88 = false;
                    if (var84) {
                        var22 = var3.ar();
                        if (BoundaryObject.qi.e(new kb(var22, ad.bc))) {
                            var88 = true;
                        }
                    }

                    var8 = var3.ar();
                    if (!var88) {
                        j.t(var20, var22, var8);
                    }

                    var1.e = null;
                    return true;
                }

                if (fx.af == var1.e) {
                    if (ad.ot != null) {
                        ad.ot.ce(var3);
                    }

                    gx.js();
                    var1.e = null;
                    return true;
                }

                if (fx.bp == var1.e) {
                    mouseTrackerIdleTime = var3.ae() * 30;
                    mv = mn;
                    var1.e = null;
                    return true;
                }

                if (fx.bq == var1.e) {
                    px = true;
                    im.pv = var3.av();
                    ao.pr = var3.av();
                    i.pm = var3.ae();
                    f.pu = var3.av();
                    Canvas.pz = var3.av();
                    if (Canvas.pz >= 100) {
                        var20 = im.pv * 128 + 64;
                        var21 = ao.pr * 128 + 64;
                        var6 = ef.gn(var20, var21, kt.ii) - i.pm;
                        var23 = var20 - RuneScriptVM.go;
                        var24 = var6 - fk.gs;
                        var9 = var21 - n.gp;
                        var10 = (int) Math.sqrt((double) (var9 * var9 + var23 * var23));
                        ap.gi = (int) (Math.atan2((double) var24, (double) var10) * 325.949D) & 2047;
                        bt.cameraYaw = (int) (Math.atan2((double) var23, (double) var9) * -325.949D) & 2047;
                        if (ap.gi < 128) {
                            ap.gi = 128;
                        }

                        if (ap.gi > 383) {
                            ap.gi = 383;
                        }
                    }

                    var1.e = null;
                    return true;
                }

                FloorObject.t("" + (var1.e != null ? var1.e.ch : -1) + "," + (var1.c != null ? var1.c.ch : -1) + ","
                        + (var1.v != null ? var1.v.ch : -1) + "," + var1.x, (Throwable) null);
                ej.fu();
            } catch (IOException var54) {
                av.fz();
            } catch (Exception var55) {
                var5 = "" + (var1.e != null ? var1.e.ch : -1) + "," + (var1.c != null ? var1.c.ch : -1) + ","
                        + (var1.v != null ? var1.v.ch : -1) + "," + var1.x + "," + (az.il.co[0] + an.regionBaseX) + ","
                        + (az.il.cv[0] + PlayerComposite.ep) + ",";

                for (var6 = 0; var6 < var1.x && var6 < 50; ++var6) {
                    var5 = var5 + var3.buffer[var6] + ",";
                }

                FloorObject.t(var5, var55);
                ej.fu();
            }

            return true;
        }
    }

    @ObfuscatedName("ha")
    final void ha() {
        boolean var1 = false;

        int var2;
        int var5;
        while (!var1) {
            var1 = true;

            for (var2 = 0; var2 < menuSize - 1; ++var2) {
                if (menuOpcodes[var2] < 1000 && menuOpcodes[var2 + 1] > 1000) {
                    String var3 = menuTargets[var2];
                    menuTargets[var2] = menuTargets[var2 + 1];
                    menuTargets[var2 + 1] = var3;
                    String var4 = menuActions[var2];
                    menuActions[var2] = menuActions[var2 + 1];
                    menuActions[var2 + 1] = var4;
                    var5 = menuOpcodes[var2];
                    menuOpcodes[var2] = menuOpcodes[var2 + 1];
                    menuOpcodes[var2 + 1] = var5;
                    var5 = menuArg1[var2];
                    menuArg1[var2] = menuArg1[var2 + 1];
                    menuArg1[var2 + 1] = var5;
                    var5 = menuArg2[var2];
                    menuArg2[var2] = menuArg2[var2 + 1];
                    menuArg2[var2 + 1] = var5;
                    var5 = ka[var2];
                    ka[var2] = ka[var2 + 1];
                    ka[var2 + 1] = var5;
                    boolean var6 = kc[var2];
                    kc[var2] = kc[var2 + 1];
                    kc[var2 + 1] = var6;
                    var1 = false;
                }
            }
        }

        if (ak.ik == null) {
            if (ln == null) {
                int var19 = bs.j;
                int var7;
                int var15;
                int var20;
                if (menuOpen) {
                    int var14;
                    if (var19 != 1 && (er.ch || var19 != 4)) {
                        var2 = bs.n;
                        var14 = bs.x;
                        if (var2 < dg.jv - 10 || var2 > dg.jv + y.menuWidth + 10 || var14 < ci.menuY - 10
                                || var14 > ci.menuY + ak.jy + 10) {
                            menuOpen = false;
                            Canvas.hw(dg.jv, ci.menuY, y.menuWidth, ak.jy);
                        }
                    }

                    if (var19 == 1 || !er.ch && var19 == 4) {
                        var2 = dg.jv;
                        var14 = ci.menuY;
                        var15 = y.menuWidth;
                        var5 = bs.cameraZ;
                        var20 = bs.z;
                        var7 = -1;

                        int var8;
                        int var9;
                        for (var8 = 0; var8 < menuSize; ++var8) {
                            var9 = var14 + (menuSize - 1 - var8) * 15 + 31;
                            if (var5 > var2 && var5 < var15 + var2 && var20 > var9 - 13 && var20 < var9 + 3) {
                                var7 = var8;
                            }
                        }

                        if (var7 != -1 && var7 >= 0) {
                            var8 = menuArg1[var7];
                            var9 = menuArg2[var7];
                            int var10 = menuOpcodes[var7];
                            int var11 = ka[var7];
                            String var12 = menuActions[var7];
                            String var13 = menuTargets[var7];
                            a.hu(var8, var9, var10, var11, var12, var13, bs.cameraZ, bs.z);
                        }

                        menuOpen = false;
                        Canvas.hw(dg.jv, ci.menuY, y.menuWidth, ak.jy);
                    }
                } else {
                    var2 = menuSize - 1;
                    if ((var19 == 1 || !er.ch && var19 == 4) && var2 >= 0) {
                        var15 = menuOpcodes[var2];
                        if (var15 == 39 || var15 == 40 || var15 == 41 || var15 == 42 || var15 == 43 || var15 == 33
                                || var15 == 34 || var15 == 35 || var15 == 36 || var15 == 37 || var15 == 38
                                || var15 == 1005) {
                            var5 = menuArg1[var2];
                            var20 = menuArg2[var2];
                            RTComponent var16 = Inflater.t(var20);
                            if (er.i(u.jr(var16)) || eh.a(u.jr(var16))) {
                                if (ak.ik != null && !iw && menuSize > 0 && !this.hg()) {
                                    er.ia(io, ig);
                                }

                                iw = false;
                                ih = 0;
                                if (ak.ik != null) {
                                    GameEngine.jk(ak.ik);
                                }

                                ak.ik = Inflater.t(var20);
                                is = var5;
                                io = bs.cameraZ;
                                ig = bs.z;
                                if (var2 >= 0) {
                                    am.kt = new MenuItemNode();
                                    am.kt.q = menuArg1[var2];
                                    am.kt.i = menuArg2[var2];
                                    am.kt.a = menuOpcodes[var2];
                                    am.kt.l = ka[var2];
                                    am.kt.text = menuActions[var2];
                                }

                                GameEngine.jk(ak.ik);
                                return;
                            }
                        }
                    }

                    if ((var19 == 1 || !er.ch && var19 == 4) && this.hg()) {
                        var19 = 2;
                    }

                    if ((var19 == 1 || !er.ch && var19 == 4) && menuSize > 0 && var2 >= 0) {
                        var15 = menuArg1[var2];
                        var5 = menuArg2[var2];
                        var20 = menuOpcodes[var2];
                        var7 = ka[var2];
                        String var17 = menuActions[var2];
                        String var18 = menuTargets[var2];
                        a.hu(var15, var5, var20, var7, var17, var18, bs.cameraZ, bs.z);
                    }

                    if (var19 == 2 && menuSize > 0) {
                        this.hc(bs.cameraZ, bs.z);
                    }
                }

            }
        }
    }

    @ObfuscatedName("hg")
    final boolean hg() {
        int var1 = menuSize - 1;
        return (jp == 1 && menuSize > 2 || er.hs(var1)) && !kc[var1];
    }

    @ObfuscatedName("hc")
    final void hc(int var1, int var2) {
        int var3 = b.ev.c("Choose Option");

        int var4;
        int var5;
        for (var4 = 0; var4 < menuSize; ++var4) {
            var5 = b.ev.c(aw.hb(var4));
            if (var5 > var3) {
                var3 = var5;
            }
        }

        var3 += 8;
        var4 = menuSize * 15 + 22;
        var5 = var1 - var3 / 2;
        if (var5 + var3 > BoundaryObject.r) {
            var5 = BoundaryObject.r - var3;
        }

        if (var5 < 0) {
            var5 = 0;
        }

        int var6 = var2;
        if (var2 + var4 > GameEngine.h) {
            var6 = GameEngine.h - var4;
        }

        if (var6 < 0) {
            var6 = 0;
        }

        an.loadedRegion.aw(kt.ii, var1, var2, false);
        menuOpen = true;
        dg.jv = var5;
        ci.menuY = var6;
        y.menuWidth = var3;
        ak.jy = menuSize * 15 + 22;
    }

    @ObfuscatedName("is")
    final void is(boolean var1) {
        int var2 = hudIndex;
        int var3 = BoundaryObject.r;
        int var4 = GameEngine.h;
        if (RuneScript.i(var2)) {
            bk.ic(RTComponent.b[var2], -1, var3, var4, var1);
        }

    }

    @ObfuscatedName("io")
    void io(RTComponent var1) {
        RTComponent var2 = var1.parentId == -1 ? null : Inflater.t(var1.parentId);
        int var3;
        int var4;
        if (var2 == null) {
            var3 = BoundaryObject.r;
            var4 = GameEngine.h;
        } else {
            var3 = var2.width;
            var4 = var2.height;
        }

        aw.it(var1, var3, var4, false);
        s.iw(var1, var3, var4);
    }

    @ObfuscatedName("in")
    final void in() {
        GameEngine.jk(ln);
        ++ah.lp;
        if (lg && lk) {
            int var1 = bs.n;
            int var2 = bs.x;
            var1 -= li;
            var2 -= lf;
            if (var1 < la) {
                var1 = la;
            }

            if (var1 + ln.width > la + lm.width) {
                var1 = la + lm.width - ln.width;
            }

            if (var2 < le) {
                var2 = le;
            }

            if (var2 + ln.height > le + lm.height) {
                var2 = le + lm.height - ln.height;
            }

            int var3 = var1 - lo;
            int var4 = var2 - lu;
            int var5 = ln.cj;
            if (ah.lp > ln.cl && (var3 > var5 || var3 < -var5 || var4 > var5 || var4 < -var5)) {
                lr = true;
            }

            int var6 = var1 - la + lm.horizontalScrollbarPosition;
            int var7 = var2 - le + lm.verticalScrollbarPosition;
            ScriptEvent var8;
            if (ln.dh != null && lr) {
                var8 = new ScriptEvent();
                var8.i = ln;
                var8.a = var6;
                var8.l = var7;
                var8.args = ln.dh;
                m.t(var8, 500000);
            }

            if (bs.g == 0) {
                if (lr) {
                    if (ln.dc != null) {
                        var8 = new ScriptEvent();
                        var8.i = ln;
                        var8.a = var6;
                        var8.l = var7;
                        var8.e = ld;
                        var8.args = ln.dc;
                        m.t(var8, 500000);
                    }

                    if (ld != null && RSException.jp(ln) != null) {
                        gd var9 = ap.t(fo.ba, ee.l);
                        var9.i.l(ln.itemId);
                        var9.i.bn(ld.w);
                        var9.i.e(ln.id);
                        var9.i.bu(ld.id);
                        var9.i.bn(ln.w);
                        var9.i.l(ld.itemId);
                        ee.i(var9);
                    }
                } else if (this.hg()) {
                    this.hc(lo + li, lf + lu);
                } else if (menuSize > 0) {
                    er.ia(lo + li, lf + lu);
                }

                ln = null;
            }

        } else {
            if (ah.lp > 1) {
                ln = null;
            }

        }
    }

    @ObfuscatedName("jd")
    public kb jd() {
        return az.il != null ? az.il.t : null;
    }

    @ObfuscatedName("t")
    public static String t(byte[] var0) {
        return em.q(var0, 0, var0.length);
    }

    @ObfuscatedName("q")
    public static VarInfo q(int var0) {
        VarInfo var1 = (VarInfo) VarInfo.i.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = VarInfo.t.i(16, var0);
            var1 = new VarInfo();
            if (var2 != null) {
                var1.i(new ByteBuffer(var2));
            }

            VarInfo.i.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("u")
    static final String u(int var0) {
        if (var0 < 100000) {
            return "<col=ffff00>" + var0 + "</col>";
        } else {
            return var0 < 10000000 ? "<col=ffffff>" + var0 / 1000 + "K" + "</col>" : "<col=00ff80>" + var0 / 1000000
                    + "M" + "</col>";
        }
    }

    @ObfuscatedName("hp")
    static final void hp() {
        Packet var0 = ee.b;
        var0.jc();
        int var1 = var0.jt(8);
        int var2;
        if (var1 < dz) {
            for (var2 = var1; var2 < dz; ++var2) {
                jg[++ju - 1] = npcIndices[var2];
            }
        }

        if (var1 > dz) {
            throw new RuntimeException("");
        } else {
            dz = 0;

            for (var2 = 0; var2 < var1; ++var2) {
                int var3 = npcIndices[var2];
                Npc var4 = loadedNpcs[var3];
                int var5 = var0.jt(1);
                if (var5 == 0) {
                    npcIndices[++dz - 1] = var3;
                    var4.cu = bz;
                } else {
                    int var6 = var0.jt(2);
                    if (var6 == 0) {
                        npcIndices[++dz - 1] = var3;
                        var4.cu = bz;
                        eb[++dm - 1] = var3;
                    } else {
                        int var7;
                        int var8;
                        if (var6 == 1) {
                            npcIndices[++dz - 1] = var3;
                            var4.cu = bz;
                            var7 = var0.jt(3);
                            var4.t(var7, (byte) 1);
                            var8 = var0.jt(1);
                            if (var8 == 1) {
                                eb[++dm - 1] = var3;
                            }
                        } else if (var6 == 2) {
                            npcIndices[++dz - 1] = var3;
                            var4.cu = bz;
                            var7 = var0.jt(3);
                            var4.t(var7, (byte) 2);
                            var8 = var0.jt(3);
                            var4.t(var8, (byte) 2);
                            int var9 = var0.jt(1);
                            if (var9 == 1) {
                                eb[++dm - 1] = var3;
                            }
                        } else if (var6 == 3) {
                            jg[++ju - 1] = var3;
                        }
                    }
                }
            }

        }
    }
}
