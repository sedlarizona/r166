import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ea")
public class ea extends ky {

    @ObfuscatedName("i")
    public static long i;

    @ObfuscatedName("t")
    final boolean t;

    public ea(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (Client.currentWorld == var1.l) {
            if (var2.l != Client.currentWorld) {
                return this.t ? -1 : 1;
            }
        } else if (var2.l == Client.currentWorld) {
            return this.t ? 1 : -1;
        }

        return this.p(var1, var2);
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("a")
    public static void a() {
        au.e.b(5);
        au.x.b(5);
    }
}
