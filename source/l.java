import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("l")
public enum l implements gl {
    @ObfuscatedName("t")
    t(1, 0, Integer.class, new i()), @ObfuscatedName("q")
    q(0, 1, Long.class, new a()), @ObfuscatedName("i")
    i(2, 2, String.class, new b());

    @ObfuscatedName("iz")
    static int iz;

    @ObfuscatedName("a")
    final int a;

    @ObfuscatedName("l")
    final int l;

    l(int var3, int var4, Class var5, t var6) {
        this.a = var3;
        this.l = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.l;
    }

    @ObfuscatedName("a")
    static ji[] a() {
        return new ji[] { ji.q, ji.t, ji.i };
    }

    @ObfuscatedName("ih")
    static final void ih(int var0, int var1, int var2, int var3, int var4) {
        an.fv[0].i(var0, var1);
        an.fv[1].i(var0, var3 + var1 - 16);
        li.dl(var0, var1 + 16, 16, var3 - 32, Client.fx);
        int var5 = var3 * (var3 - 32) / var4;
        if (var5 < 8) {
            var5 = 8;
        }

        int var6 = (var3 - 32 - var5) * var2 / (var4 - var3);
        li.dl(var0, var6 + var1 + 16, 16, var5, Client.fo);
        li.de(var0, var6 + var1 + 16, var5, Client.ga);
        li.de(var0 + 1, var6 + var1 + 16, var5, Client.ga);
        li.du(var0, var6 + var1 + 16, 16, Client.ga);
        li.du(var0, var6 + var1 + 17, 16, Client.ga);
        li.de(var0 + 15, var6 + var1 + 16, var5, Client.gu);
        li.de(var0 + 14, var6 + var1 + 17, var5 - 1, Client.gu);
        li.du(var0, var6 + var5 + var1 + 15, 16, Client.gu);
        li.du(var0 + 1, var5 + var6 + var1 + 14, 15, Client.gu);
    }
}
