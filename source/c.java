import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("c")
final class c implements Comparator {
   @ObfuscatedName("pi")
   static TaskData pi;
   @ObfuscatedName("t")
   static FileSystem t;
   @ObfuscatedName("e")
   public static boolean[] loadedInterfaces;
   @ObfuscatedName("p")
   public static short[] p;
   @ObfuscatedName("if")
   static RTComponent if;
   @ObfuscatedName("mx")
   static int mx;

   @ObfuscatedName("t")
   int t(u var1, u var2) {
      return var1.t < var2.t ? -1 : (var2.t == var1.t ? 0 : 1);
   }

   public int compare(Object var1, Object var2) {
      return this.t((u)var1, (u)var2);
   }

   public boolean equals(Object var1) {
      return super.equals(var1);
   }
}
