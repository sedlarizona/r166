import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ai")
public class ai implements as {

    @ObfuscatedName("j")
    public static int j;

    @ObfuscatedName("bn")
    static lk[] bn;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("t")
    public void t(az var1) {
        if (var1.e > this.l) {
            var1.e = this.l;
        }

        if (var1.x < this.l) {
            var1.x = this.l;
        }

        if (var1.p > this.b) {
            var1.p = this.b;
        }

        if (var1.g < this.b) {
            var1.g = this.b;
        }

    }

    @ObfuscatedName("q")
    public boolean q(int var1, int var2, int var3) {
        if (var1 >= this.t && var1 < this.q + this.t) {
            return var2 >> 6 == this.i && var3 >> 6 == this.a;
        } else {
            return false;
        }
    }

    @ObfuscatedName("i")
    public boolean i(int var1, int var2) {
        return var1 >> 6 == this.l && var2 >> 6 == this.b;
    }

    @ObfuscatedName("a")
    public int[] a(int var1, int var2, int var3) {
        if (!this.q(var1, var2, var3)) {
            return null;
        } else {
            int[] var4 = new int[] { this.l * 64 - this.i * 64 + var2, var3 + (this.b * 64 - this.a * 64) };
            return var4;
        }
    }

    @ObfuscatedName("l")
    public ik l(int var1, int var2) {
        if (!this.i(var1, var2)) {
            return null;
        } else {
            int var3 = this.i * 64 - this.l * 64 + var1;
            int var4 = this.a * 64 - this.b * 64 + var2;
            return new ik(this.t, var3, var4);
        }
    }

    @ObfuscatedName("b")
    public void b(ByteBuffer var1) {
        this.t = var1.av();
        this.q = var1.av();
        this.i = var1.ae();
        this.a = var1.ae();
        this.l = var1.ae();
        this.b = var1.ae();
        this.e();
    }

    @ObfuscatedName("e")
    void e() {
    }

    @ObfuscatedName("ae")
    static void ae(int var0, ik var1, boolean var2) {
        az var3 = ex.ep().ar(var0);
        int var4 = az.il.r;
        int var5 = (az.il.regionX >> 7) + an.regionBaseX;
        int var6 = (az.il.regionY >> 7) + PlayerComposite.ep;
        ik var7 = new ik(var4, var5, var6);
        ex.ep().r(var3, var7, var1, var2);
    }
}
