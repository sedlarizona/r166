import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dz")
public final class FloorObject {

    @ObfuscatedName("t")
    int height;

    @ObfuscatedName("q")
    int regionX;

    @ObfuscatedName("i")
    int regionY;

    @ObfuscatedName("a")
    public Renderable model;

    @ObfuscatedName("l")
    public int hash;

    @ObfuscatedName("b")
    int placement;

    @ObfuscatedName("t")
    public static void t(String var0, Throwable var1) {
        var1.printStackTrace();
    }

    @ObfuscatedName("e")
    static void e() {
        ci.username = ci.username.trim();
        if (ci.username.length() == 0) {
            cx.p("Please enter your username.", "If you created your account after November",
                    "2010, this will be the creation email address.");
        } else {
            long var1 = o.t();
            int var0;
            if (var1 == 0L) {
                var0 = 5;
            } else {
                var0 = Character.q(var1, ci.username);
            }

            switch (var0) {
            case 2:
                cx.p("", "Page has opened in a new window.", "(Please check your popup blocker.)");
                ci.loginState = 6;
                break;
            case 3:
                cx.p("", "Error connecting to server.", "");
                break;
            case 4:
                cx.p("The part of the website you are trying", "to connect to is offline at the moment.",
                        "Please try again later.");
                break;
            case 5:
                cx.p("Sorry, there was an error trying to", "log you in to this part of the website.",
                        "Please try again later.");
                break;
            case 6:
                cx.p("", "Error connecting to server.", "");
                break;
            case 7:
                cx.p("You must enter a valid login to proceed. For accounts",
                        "created after 24th November 2010, please use your",
                        "email address. Otherwise please use your username.");
            }

        }
    }
}
