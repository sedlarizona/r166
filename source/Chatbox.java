import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cz")
public class Chatbox {

    @ObfuscatedName("q")
    ChatboxMessage[] messages = new ChatboxMessage[100];

    @ObfuscatedName("i")
    int messageQuantity;

    @ObfuscatedName("t")
    ChatboxMessage t(int var1, String var2, String var3, String var4) {
        ChatboxMessage var5 = this.messages[99];

        for (int var6 = this.messageQuantity; var6 > 0; --var6) {
            if (var6 != 100) {
                this.messages[var6] = this.messages[var6 - 1];
            }
        }

        if (var5 == null) {
            var5 = new ChatboxMessage(var1, var2, var4, var3);
        } else {
            var5.kc();
            var5.ce();
            var5.t(var1, var2, var4, var3);
        }

        this.messages[0] = var5;
        if (this.messageQuantity < 100) {
            ++this.messageQuantity;
        }

        return var5;
    }

    @ObfuscatedName("q")
    ChatboxMessage q(int var1) {
        return var1 >= 0 && var1 < this.messageQuantity ? this.messages[var1] : null;
    }

    @ObfuscatedName("i")
    int i() {
        return this.messageQuantity;
    }

    @ObfuscatedName("gs")
    static void gs() {
        if (Client.iu) {
            bt.gh(az.il, false);
        }

    }
}
