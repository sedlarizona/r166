import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ja")
public class InventoryDefinition extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static Cache q = new Cache(64);

    @ObfuscatedName("i")
    public int capacity = 0;

    @ObfuscatedName("q")
    public void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 2) {
            this.capacity = var1.ae();
        }

    }

    @ObfuscatedName("gc")
    static final void gc(Character var0, int var1, int var2, int var3, int var4, int var5) {
        if (var0 != null && var0.k()) {
            if (var0 instanceof Npc) {
                NpcDefinition var6 = ((Npc) var0).composite;
                if (var6.ae != null) {
                    var6 = var6.x();
                }

                if (var6 == null) {
                    return;
                }
            }

            int var75 = cx.b;
            int[] var7 = cx.e;
            byte var8 = 0;
            if (var1 < var75 && var0.ah == Client.bz && AreaSoundEmitter.gb((Player) var0)) {
                Player var9 = (Player) var0;
                if (var1 < var75) {
                    av.gt(var0, var0.cs + 15);
                    RSFont var10 = (RSFont) Client.es.get(kq.q);
                    byte var11 = 9;
                    var10.r(var9.t.t(), var2 + Client.hv, var3 + Client.hd - var11, 16777215, 0);
                    var8 = 18;
                }
            }

            int var76 = -2;
            int var15;
            int var22;
            int var23;
            if (!var0.combatBarList.p()) {
                av.gt(var0, var0.cs + 15);

                for (CombatBar var87 = (CombatBar) var0.combatBarList.b(); var87 != null; var87 = (CombatBar) var0.combatBarList
                        .x()) {
                    CombatBarData var77 = var87.q(Client.bz);
                    if (var77 == null) {
                        if (var87.i()) {
                            var87.kc();
                        }
                    } else {
                        CombatBarDefinition var12 = var87.definition;
                        Sprite var13 = var12.l();
                        Sprite var14 = var12.a();
                        int var16 = 0;
                        if (var13 != null && var14 != null) {
                            if (var12.j * 2 < var14.width) {
                                var16 = var12.j;
                            }

                            var15 = var14.width - var16 * 2;
                        } else {
                            var15 = var12.maxWidth;
                        }

                        int var17 = 255;
                        boolean var18 = true;
                        int var19 = Client.bz - var77.initialCycle;
                        int var20 = var15 * var77.currentWidth / var12.maxWidth;
                        int var21;
                        int var92;
                        if (var77.currentCycle > var19) {
                            var21 = var12.n == 0 ? 0 : var12.n * (var19 / var12.n);
                            var22 = var15 * var77.initialWidth / var12.maxWidth;
                            var92 = var21 * (var20 - var22) / var77.currentCycle + var22;
                        } else {
                            var92 = var20;
                            var21 = var77.currentCycle + var12.o - var19;
                            if (var12.g >= 0) {
                                var17 = (var21 << 8) / (var12.o - var12.g);
                            }
                        }

                        if (var77.currentWidth > 0 && var92 < 1) {
                            var92 = 1;
                        }

                        if (var13 != null && var14 != null) {
                            if (var15 == var92) {
                                var92 += var16 * 2;
                            } else {
                                var92 += var16;
                            }

                            var21 = var13.height;
                            var76 += var21;
                            var22 = var2 + Client.hv - (var15 >> 1);
                            var23 = var3 + Client.hd - var76;
                            var22 -= var16;
                            if (var17 >= 0 && var17 < 255) {
                                var13.f(var22, var23, var17);
                                li.db(var22, var23, var92 + var22, var23 + var21);
                                var14.f(var22, var23, var17);
                            } else {
                                var13.u(var22, var23);
                                li.db(var22, var23, var92 + var22, var21 + var23);
                                var14.u(var22, var23);
                            }

                            li.ck(var2, var3, var2 + var4, var3 + var5);
                            var76 += 2;
                        } else {
                            var76 += 5;
                            if (Client.hv > -1) {
                                var21 = var2 + Client.hv - (var15 >> 1);
                                var22 = var3 + Client.hd - var76;
                                li.dl(var21, var22, var92, 5, 65280);
                                li.dl(var21 + var92, var22, var15 - var92, 5, 16711680);
                            }

                            var76 += 2;
                        }
                    }
                }
            }

            if (var76 == -2) {
                var76 += 7;
            }

            var76 += var8;
            if (var1 < var75) {
                Player var88 = (Player) var0;
                if (var88.f) {
                    return;
                }

                if (var88.pkIconIndex != -1 || var88.prayerIconIndex != -1) {
                    av.gt(var0, var0.cs + 15);
                    if (Client.hv > -1) {
                        if (var88.pkIconIndex != -1) {
                            var76 += 25;
                            bn.fr[var88.pkIconIndex].u(var2 + Client.hv - 12, var3 + Client.hd - var76);
                        }

                        if (var88.prayerIconIndex != -1) {
                            var76 += 25;
                            n.fi[var88.prayerIconIndex].u(var2 + Client.hv - 12, var3 + Client.hd - var76);
                        }
                    }
                }

                if (var1 >= 0 && Client.hintArrowType == 10 && var7[var1] == Client.hintPlayerIndex) {
                    av.gt(var0, var0.cs + 15);
                    if (Client.hv > -1) {
                        var76 += ie.fn[1].height;
                        ie.fn[1].u(var2 + Client.hv - 12, var3 + Client.hd - var76);
                    }
                }
            } else {
                NpcDefinition var89 = ((Npc) var0).composite;
                if (var89.ae != null) {
                    var89 = var89.x();
                }

                if (var89.prayerIconIndex >= 0 && var89.prayerIconIndex < n.fi.length) {
                    av.gt(var0, var0.cs + 15);
                    if (Client.hv > -1) {
                        n.fi[var89.prayerIconIndex].u(var2 + Client.hv - 12, var3 + Client.hd - 30);
                    }
                }

                if (Client.hintArrowType == 1 && Client.npcIndices[var1 - var75] == Client.hintNpcIndex
                        && Client.bz % 20 < 10) {
                    av.gt(var0, var0.cs + 15);
                    if (Client.hv > -1) {
                        ie.fn[0].u(var2 + Client.hv - 12, var3 + Client.hd - 28);
                    }
                }
            }

            if (var0.textSpoken != null
                    && (var1 >= var75 || !var0.aq
                            && (Client.np == 4 || !var0.aw
                                    && (Client.np == 0 || Client.np == 3 || Client.np == 1 && ((Player) var0).q())))) {
                av.gt(var0, var0.cs);
                if (Client.hv > -1 && Client.hr < Client.hx) {
                    Client.hn[Client.hr] = b.ev.c(var0.textSpoken) / 2;
                    Client.hw[Client.hr] = b.ev.e;
                    Client.hf[Client.hr] = Client.hv;
                    Client.hh[Client.hr] = Client.hd;
                    Client.ha[Client.hr] = var0.af;
                    Client.hg[Client.hr] = var0.ak;
                    Client.hc[Client.hr] = var0.aa;
                    Client.hs[Client.hr] = var0.textSpoken;
                    ++Client.hr;
                }
            }

            for (int var78 = 0; var78 < 4; ++var78) {
                int var90 = var0.hitsplatCycles[var78];
                int var79 = var0.hitsplatTypes[var78];
                jh var91 = null;
                int var80 = 0;
                if (var79 >= 0) {
                    if (var90 <= Client.bz) {
                        continue;
                    }

                    var91 = bl.t(var0.hitsplatTypes[var78]);
                    var80 = var91.v;
                    if (var91 != null && var91.h != null) {
                        var91 = var91.a();
                        if (var91 == null) {
                            var0.hitsplatCycles[var78] = -1;
                            continue;
                        }
                    }
                } else if (var90 < 0) {
                    continue;
                }

                var15 = var0.ba[var78];
                jh var81 = null;
                if (var15 >= 0) {
                    var81 = bl.t(var15);
                    if (var81 != null && var81.h != null) {
                        var81 = var81.a();
                    }
                }

                if (var90 - var80 <= Client.bz) {
                    if (var91 == null) {
                        var0.hitsplatCycles[var78] = -1;
                    } else {
                        av.gt(var0, var0.cs / 2);
                        if (Client.hv > -1) {
                            if (var78 == 1) {
                                Client.hd -= 20;
                            }

                            if (var78 == 2) {
                                Client.hv -= 15;
                                Client.hd -= 10;
                            }

                            if (var78 == 3) {
                                Client.hv += 15;
                                Client.hd -= 10;
                            }

                            Sprite var82 = null;
                            Sprite var83 = null;
                            Sprite var84 = null;
                            Sprite var85 = null;
                            var22 = 0;
                            var23 = 0;
                            int var24 = 0;
                            int var25 = 0;
                            int var26 = 0;
                            int var27 = 0;
                            int var28 = 0;
                            int var29 = 0;
                            Sprite var30 = null;
                            Sprite var31 = null;
                            Sprite var32 = null;
                            Sprite var33 = null;
                            int var34 = 0;
                            int var35 = 0;
                            int var36 = 0;
                            int var37 = 0;
                            int var38 = 0;
                            int var39 = 0;
                            int var40 = 0;
                            int var41 = 0;
                            int var42 = 0;
                            var82 = var91.b();
                            int var43;
                            if (var82 != null) {
                                var22 = var82.width;
                                var43 = var82.height;
                                if (var43 > var42) {
                                    var42 = var43;
                                }

                                var26 = var82.a;
                            }

                            var83 = var91.e();
                            if (var83 != null) {
                                var23 = var83.width;
                                var43 = var83.height;
                                if (var43 > var42) {
                                    var42 = var43;
                                }

                                var27 = var83.a;
                            }

                            var84 = var91.x();
                            if (var84 != null) {
                                var24 = var84.width;
                                var43 = var84.height;
                                if (var43 > var42) {
                                    var42 = var43;
                                }

                                var28 = var84.a;
                            }

                            var85 = var91.p();
                            if (var85 != null) {
                                var25 = var85.width;
                                var43 = var85.height;
                                if (var43 > var42) {
                                    var42 = var43;
                                }

                                var29 = var85.a;
                            }

                            if (var81 != null) {
                                var30 = var81.b();
                                if (var30 != null) {
                                    var34 = var30.width;
                                    var43 = var30.height;
                                    if (var43 > var42) {
                                        var42 = var43;
                                    }

                                    var38 = var30.a;
                                }

                                var31 = var81.e();
                                if (var31 != null) {
                                    var35 = var31.width;
                                    var43 = var31.height;
                                    if (var43 > var42) {
                                        var42 = var43;
                                    }

                                    var39 = var31.a;
                                }

                                var32 = var81.x();
                                if (var32 != null) {
                                    var36 = var32.width;
                                    var43 = var32.height;
                                    if (var43 > var42) {
                                        var42 = var43;
                                    }

                                    var40 = var32.a;
                                }

                                var33 = var81.p();
                                if (var33 != null) {
                                    var37 = var33.width;
                                    var43 = var33.height;
                                    if (var43 > var42) {
                                        var42 = var43;
                                    }

                                    var41 = var33.a;
                                }
                            }

                            km var86 = var91.o();
                            if (var86 == null) {
                                var86 = fv.ez;
                            }

                            km var44;
                            if (var81 != null) {
                                var44 = var81.o();
                                if (var44 == null) {
                                    var44 = fv.ez;
                                }
                            } else {
                                var44 = fv.ez;
                            }

                            String var45 = null;
                            String var46 = null;
                            boolean var47 = false;
                            int var48 = 0;
                            var45 = var91.l(var0.hitsplatDamages[var78]);
                            int var93 = var86.c(var45);
                            if (var81 != null) {
                                var46 = var81.l(var0.bk[var78]);
                                var48 = var44.c(var46);
                            }

                            int var49 = 0;
                            int var50 = 0;
                            if (var23 > 0) {
                                if (var84 == null && var85 == null) {
                                    var49 = 1;
                                } else {
                                    var49 = var93 / var23 + 1;
                                }
                            }

                            if (var81 != null && var35 > 0) {
                                if (var32 == null && var33 == null) {
                                    var50 = 1;
                                } else {
                                    var50 = var48 / var35 + 1;
                                }
                            }

                            int var51 = 0;
                            int var52 = var51;
                            if (var22 > 0) {
                                var51 += var22;
                            }

                            var51 += 2;
                            int var53 = var51;
                            if (var24 > 0) {
                                var51 += var24;
                            }

                            int var54 = var51;
                            int var55 = var51;
                            int var56;
                            if (var23 > 0) {
                                var56 = var23 * var49;
                                var51 += var56;
                                var55 += (var56 - var93) / 2;
                            } else {
                                var51 += var93;
                            }

                            var56 = var51;
                            if (var25 > 0) {
                                var51 += var25;
                            }

                            int var57 = 0;
                            int var58 = 0;
                            int var59 = 0;
                            int var60 = 0;
                            int var61 = 0;
                            int var62;
                            if (var81 != null) {
                                var51 += 2;
                                var57 = var51;
                                if (var34 > 0) {
                                    var51 += var34;
                                }

                                var51 += 2;
                                var58 = var51;
                                if (var36 > 0) {
                                    var51 += var36;
                                }

                                var59 = var51;
                                var61 = var51;
                                if (var35 > 0) {
                                    var62 = var50 * var35;
                                    var51 += var62;
                                    var61 += (var62 - var48) / 2;
                                } else {
                                    var51 += var48;
                                }

                                var60 = var51;
                                if (var37 > 0) {
                                    var51 += var37;
                                }
                            }

                            var62 = var0.hitsplatCycles[var78] - Client.bz;
                            int var63 = var91.w - var62 * var91.w / var91.v;
                            int var64 = var62 * var91.s / var91.v + -var91.s;
                            int var65 = var63 + (var2 + Client.hv - (var51 >> 1));
                            int var66 = var64 + (var3 + Client.hd - 12);
                            int var67 = var66;
                            int var68 = var66 + var42;
                            int var69 = var66 + var91.y + 15;
                            int var70 = var69 - var86.x;
                            int var71 = var69 + var86.p;
                            if (var70 < var66) {
                                var67 = var70;
                            }

                            if (var71 > var68) {
                                var68 = var71;
                            }

                            int var72 = 0;
                            int var73;
                            int var74;
                            if (var81 != null) {
                                var72 = var66 + var81.y + 15;
                                var73 = var72 - var44.x;
                                var74 = var72 + var44.p;
                                if (var73 < var67) {
                                    ;
                                }

                                if (var74 > var68) {
                                    ;
                                }
                            }

                            var73 = 255;
                            if (var91.d >= 0) {
                                var73 = (var62 << 8) / (var91.v - var91.d);
                            }

                            if (var73 >= 0 && var73 < 255) {
                                if (var82 != null) {
                                    var82.f(var52 + var65 - var26, var66, var73);
                                }

                                if (var84 != null) {
                                    var84.f(var65 + var53 - var28, var66, var73);
                                }

                                if (var83 != null) {
                                    for (var74 = 0; var74 < var49; ++var74) {
                                        var83.f(var74 * var23 + (var54 + var65 - var27), var66, var73);
                                    }
                                }

                                if (var85 != null) {
                                    var85.f(var65 + var56 - var29, var66, var73);
                                }

                                var86.d(var45, var65 + var55, var69, var91.c, 0, var73);
                                if (var81 != null) {
                                    if (var30 != null) {
                                        var30.f(var57 + var65 - var38, var66, var73);
                                    }

                                    if (var32 != null) {
                                        var32.f(var65 + var58 - var40, var66, var73);
                                    }

                                    if (var31 != null) {
                                        for (var74 = 0; var74 < var50; ++var74) {
                                            var31.f(var35 * var74 + (var59 + var65 - var39), var66, var73);
                                        }
                                    }

                                    if (var33 != null) {
                                        var33.f(var65 + var60 - var41, var66, var73);
                                    }

                                    var44.d(var46, var61 + var65, var72, var81.c, 0, var73);
                                }
                            } else {
                                if (var82 != null) {
                                    var82.u(var65 + var52 - var26, var66);
                                }

                                if (var84 != null) {
                                    var84.u(var53 + var65 - var28, var66);
                                }

                                if (var83 != null) {
                                    for (var74 = 0; var74 < var49; ++var74) {
                                        var83.u(var23 * var74 + (var65 + var54 - var27), var66);
                                    }
                                }

                                if (var85 != null) {
                                    var85.u(var65 + var56 - var29, var66);
                                }

                                var86.s(var45, var65 + var55, var69, var91.c | -16777216, 0);
                                if (var81 != null) {
                                    if (var30 != null) {
                                        var30.u(var57 + var65 - var38, var66);
                                    }

                                    if (var32 != null) {
                                        var32.u(var65 + var58 - var40, var66);
                                    }

                                    if (var31 != null) {
                                        for (var74 = 0; var74 < var50; ++var74) {
                                            var31.u(var35 * var74 + (var65 + var59 - var39), var66);
                                        }
                                    }

                                    if (var33 != null) {
                                        var33.u(var60 + var65 - var41, var66);
                                    }

                                    var44.s(var46, var61 + var65, var72, var81.c | -16777216, 0);
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
