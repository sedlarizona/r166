import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jy")
public class ItemDefinition extends hh {

    @ObfuscatedName("b")
    public static FileSystem b;

    @ObfuscatedName("e")
    public static FileSystem e;

    @ObfuscatedName("g")
    public static Cache g = new Cache(64);

    @ObfuscatedName("n")
    public static Cache n = new Cache(50);

    @ObfuscatedName("o")
    public static Cache o = new Cache(200);

    @ObfuscatedName("v")
    public int id;

    @ObfuscatedName("u")
    int u;

    @ObfuscatedName("j")
    public String name = "null";

    @ObfuscatedName("k")
    short[] baseColors;

    @ObfuscatedName("z")
    short[] texturedColors;

    @ObfuscatedName("w")
    short[] w;

    @ObfuscatedName("s")
    short[] s;

    @ObfuscatedName("d")
    public int d = 2000;

    @ObfuscatedName("f")
    public int f = 0;

    @ObfuscatedName("r")
    public int r = 0;

    @ObfuscatedName("y")
    public int y = 0;

    @ObfuscatedName("h")
    public int h = 0;

    @ObfuscatedName("m")
    public int m = 0;

    @ObfuscatedName("ay")
    public int ay = 0;

    @ObfuscatedName("ao")
    public int storeValue = 1;

    @ObfuscatedName("av")
    public boolean membersOnly = false;

    @ObfuscatedName("aj")
    public String[] groundActions = new String[] { null, null, "Take", null, null };

    @ObfuscatedName("ae")
    public String[] inventoryActions = new String[] { null, null, null, null, "Drop" };

    @ObfuscatedName("az")
    int az = -2;

    @ObfuscatedName("ap")
    int ap = -1;

    @ObfuscatedName("ah")
    int ah = -1;

    @ObfuscatedName("au")
    int au = 0;

    @ObfuscatedName("ax")
    int ax = -1;

    @ObfuscatedName("ar")
    int ar = -1;

    @ObfuscatedName("an")
    int an = 0;

    @ObfuscatedName("ai")
    int ai = -1;

    @ObfuscatedName("al")
    int al = -1;

    @ObfuscatedName("at")
    int at = -1;

    @ObfuscatedName("ag")
    int ag = -1;

    @ObfuscatedName("as")
    int as = -1;

    @ObfuscatedName("aw")
    int aw = -1;

    @ObfuscatedName("aq")
    int[] aq;

    @ObfuscatedName("aa")
    int[] aa;

    @ObfuscatedName("af")
    public int af = -1;

    @ObfuscatedName("ak")
    public int ak = -1;

    @ObfuscatedName("ab")
    int ab = 128;

    @ObfuscatedName("ac")
    int ac = 128;

    @ObfuscatedName("ad")
    int ad = 128;

    @ObfuscatedName("bg")
    public int bg = 0;

    @ObfuscatedName("br")
    public int br = 0;

    @ObfuscatedName("ba")
    public int ba = 0;

    @ObfuscatedName("bk")
    FixedSizeDeque bk;

    @ObfuscatedName("be")
    public boolean be = false;

    @ObfuscatedName("bc")
    int bc = -1;

    @ObfuscatedName("bm")
    int bm = -1;

    @ObfuscatedName("bh")
    public int bh = -1;

    @ObfuscatedName("bs")
    public int bs = -1;

    @ObfuscatedName("q")
    void q() {
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.a(var1, var2);
        }
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.u = var1.ae();
        } else if (var2 == 2) {
            this.name = var1.ar();
        } else if (var2 == 4) {
            this.d = var1.ae();
        } else if (var2 == 5) {
            this.f = var1.ae();
        } else if (var2 == 6) {
            this.r = var1.ae();
        } else if (var2 == 7) {
            this.h = var1.ae();
            if (this.h > 32767) {
                this.h -= 65536;
            }
        } else if (var2 == 8) {
            this.m = var1.ae();
            if (this.m > 32767) {
                this.m -= 65536;
            }
        } else if (var2 == 11) {
            this.ay = 1;
        } else if (var2 == 12) {
            this.storeValue = var1.ap();
        } else if (var2 == 16) {
            this.membersOnly = true;
        } else if (var2 == 23) {
            this.ap = var1.ae();
            this.au = var1.av();
        } else if (var2 == 24) {
            this.ah = var1.ae();
        } else if (var2 == 25) {
            this.ax = var1.ae();
            this.an = var1.av();
        } else if (var2 == 26) {
            this.ar = var1.ae();
        } else if (var2 >= 30 && var2 < 35) {
            this.groundActions[var2 - 30] = var1.ar();
            if (this.groundActions[var2 - 30].equalsIgnoreCase("Hidden")) {
                this.groundActions[var2 - 30] = null;
            }
        } else if (var2 >= 35 && var2 < 40) {
            this.inventoryActions[var2 - 35] = var1.ar();
        } else {
            int var3;
            int var4;
            if (var2 == 40) {
                var3 = var1.av();
                this.baseColors = new short[var3];
                this.texturedColors = new short[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.baseColors[var4] = (short) var1.ae();
                    this.texturedColors[var4] = (short) var1.ae();
                }
            } else if (var2 == 41) {
                var3 = var1.av();
                this.w = new short[var3];
                this.s = new short[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.w[var4] = (short) var1.ae();
                    this.s[var4] = (short) var1.ae();
                }
            } else if (var2 == 42) {
                this.az = var1.aj();
            } else if (var2 == 65) {
                this.be = true;
            } else if (var2 == 78) {
                this.ai = var1.ae();
            } else if (var2 == 79) {
                this.al = var1.ae();
            } else if (var2 == 90) {
                this.at = var1.ae();
            } else if (var2 == 91) {
                this.as = var1.ae();
            } else if (var2 == 92) {
                this.ag = var1.ae();
            } else if (var2 == 93) {
                this.aw = var1.ae();
            } else if (var2 == 95) {
                this.y = var1.ae();
            } else if (var2 == 97) {
                this.af = var1.ae();
            } else if (var2 == 98) {
                this.ak = var1.ae();
            } else if (var2 >= 100 && var2 < 110) {
                if (this.aq == null) {
                    this.aq = new int[10];
                    this.aa = new int[10];
                }

                this.aq[var2 - 100] = var1.ae();
                this.aa[var2 - 100] = var1.ae();
            } else if (var2 == 110) {
                this.ab = var1.ae();
            } else if (var2 == 111) {
                this.ac = var1.ae();
            } else if (var2 == 112) {
                this.ad = var1.ae();
            } else if (var2 == 113) {
                this.bg = var1.aj();
            } else if (var2 == 114) {
                this.br = var1.aj();
            } else if (var2 == 115) {
                this.ba = var1.av();
            } else if (var2 == 139) {
                this.bc = var1.ae();
            } else if (var2 == 140) {
                this.bm = var1.ae();
            } else if (var2 == 148) {
                this.bh = var1.ae();
            } else if (var2 == 149) {
                this.bs = var1.ae();
            } else if (var2 == 249) {
                this.bk = i.t(var1, this.bk);
            }
        }

    }

    @ObfuscatedName("l")
    void l(ItemDefinition var1, ItemDefinition var2) {
        this.u = var1.u;
        this.d = var1.d;
        this.f = var1.f;
        this.r = var1.r;
        this.y = var1.y;
        this.h = var1.h;
        this.m = var1.m;
        this.baseColors = var1.baseColors;
        this.texturedColors = var1.texturedColors;
        this.w = var1.w;
        this.s = var1.s;
        this.name = var2.name;
        this.membersOnly = var2.membersOnly;
        this.storeValue = var2.storeValue;
        this.ay = 1;
    }

    @ObfuscatedName("b")
    void b(ItemDefinition var1, ItemDefinition var2) {
        this.u = var1.u;
        this.d = var1.d;
        this.f = var1.f;
        this.r = var1.r;
        this.y = var1.y;
        this.h = var1.h;
        this.m = var1.m;
        this.baseColors = var2.baseColors;
        this.texturedColors = var2.texturedColors;
        this.w = var2.w;
        this.s = var2.s;
        this.name = var2.name;
        this.membersOnly = var2.membersOnly;
        this.ay = var2.ay;
        this.ap = var2.ap;
        this.ah = var2.ah;
        this.ai = var2.ai;
        this.ax = var2.ax;
        this.ar = var2.ar;
        this.al = var2.al;
        this.at = var2.at;
        this.ag = var2.ag;
        this.as = var2.as;
        this.aw = var2.aw;
        this.ba = var2.ba;
        this.groundActions = var2.groundActions;
        this.inventoryActions = new String[5];
        if (var2.inventoryActions != null) {
            for (int var3 = 0; var3 < 4; ++var3) {
                this.inventoryActions[var3] = var2.inventoryActions[var3];
            }
        }

        this.inventoryActions[4] = "Discard";
        this.storeValue = 0;
    }

    @ObfuscatedName("e")
    void e(ItemDefinition var1, ItemDefinition var2) {
        this.u = var1.u;
        this.d = var1.d;
        this.f = var1.f;
        this.r = var1.r;
        this.y = var1.y;
        this.h = var1.h;
        this.m = var1.m;
        this.baseColors = var1.baseColors;
        this.texturedColors = var1.texturedColors;
        this.w = var1.w;
        this.s = var1.s;
        this.ay = var1.ay;
        this.name = var2.name;
        this.storeValue = 0;
        this.membersOnly = false;
        this.be = false;
    }

    @ObfuscatedName("x")
    public final AlternativeModel x(int var1) {
        int var3;
        if (this.aq != null && var1 > 1) {
            int var2 = -1;

            for (var3 = 0; var3 < 10; ++var3) {
                if (var1 >= this.aa[var3] && this.aa[var3] != 0) {
                    var2 = this.aq[var3];
                }
            }

            if (var2 != -1) {
                return cs.loadItemDefinition(var2).x(1);
            }
        }

        AlternativeModel var4 = AlternativeModel.t(e, this.u, 0);
        if (var4 == null) {
            return null;
        } else {
            if (this.ab != 128 || this.ac != 128 || this.ad != 128) {
                var4.d(this.ab, this.ac, this.ad);
            }

            if (this.baseColors != null) {
                for (var3 = 0; var3 < this.baseColors.length; ++var3) {
                    var4.z(this.baseColors[var3], this.texturedColors[var3]);
                }
            }

            if (this.w != null) {
                for (var3 = 0; var3 < this.w.length; ++var3) {
                    var4.w(this.w[var3], this.s[var3]);
                }
            }

            return var4;
        }
    }

    @ObfuscatedName("p")
    public final Model p(int var1) {
        if (this.aq != null && var1 > 1) {
            int var2 = -1;

            for (int var3 = 0; var3 < 10; ++var3) {
                if (var1 >= this.aa[var3] && this.aa[var3] != 0) {
                    var2 = this.aq[var3];
                }
            }

            if (var2 != -1) {
                return cs.loadItemDefinition(var2).p(1);
            }
        }

        Model var5 = (Model) n.t((long) this.id);
        if (var5 != null) {
            return var5;
        } else {
            AlternativeModel var6 = AlternativeModel.t(e, this.u, 0);
            if (var6 == null) {
                return null;
            } else {
                if (this.ab != 128 || this.ac != 128 || this.ad != 128) {
                    var6.d(this.ab, this.ac, this.ad);
                }

                int var4;
                if (this.baseColors != null) {
                    for (var4 = 0; var4 < this.baseColors.length; ++var4) {
                        var6.z(this.baseColors[var4], this.texturedColors[var4]);
                    }
                }

                if (this.w != null) {
                    for (var4 = 0; var4 < this.w.length; ++var4) {
                        var6.w(this.w[var4], this.s[var4]);
                    }
                }

                var5 = var6.av(this.bg + 64, this.br * 5 + 768, -50, -10, -50);
                var5.ay = true;
                n.i(var5, (long) this.id);
                return var5;
            }
        }
    }

    @ObfuscatedName("o")
    public ItemDefinition o(int var1) {
        if (this.aq != null && var1 > 1) {
            int var2 = -1;

            for (int var3 = 0; var3 < 10; ++var3) {
                if (var1 >= this.aa[var3] && this.aa[var3] != 0) {
                    var2 = this.aq[var3];
                }
            }

            if (var2 != -1) {
                return cs.loadItemDefinition(var2);
            }
        }

        return this;
    }

    @ObfuscatedName("k")
    public final boolean k(boolean var1) {
        int var2 = this.ap;
        int var3 = this.ah;
        int var4 = this.ai;
        if (var1) {
            var2 = this.ax;
            var3 = this.ar;
            var4 = this.al;
        }

        if (var2 == -1) {
            return true;
        } else {
            boolean var5 = true;
            if (!e.l(var2, 0)) {
                var5 = false;
            }

            if (var3 != -1 && !e.l(var3, 0)) {
                var5 = false;
            }

            if (var4 != -1 && !e.l(var4, 0)) {
                var5 = false;
            }

            return var5;
        }
    }

    @ObfuscatedName("z")
    public final AlternativeModel z(boolean var1) {
        int var2 = this.ap;
        int var3 = this.ah;
        int var4 = this.ai;
        if (var1) {
            var2 = this.ax;
            var3 = this.ar;
            var4 = this.al;
        }

        if (var2 == -1) {
            return null;
        } else {
            AlternativeModel var5 = AlternativeModel.t(e, var2, 0);
            if (var3 != -1) {
                AlternativeModel var6 = AlternativeModel.t(e, var3, 0);
                if (var4 != -1) {
                    AlternativeModel var7 = AlternativeModel.t(e, var4, 0);
                    AlternativeModel[] var8 = new AlternativeModel[] { var5, var6, var7 };
                    var5 = new AlternativeModel(var8, 3);
                } else {
                    AlternativeModel[] var10 = new AlternativeModel[] { var5, var6 };
                    var5 = new AlternativeModel(var10, 2);
                }
            }

            if (!var1 && this.au != 0) {
                var5.k(0, this.au, 0);
            }

            if (var1 && this.an != 0) {
                var5.k(0, this.an, 0);
            }

            int var9;
            if (this.baseColors != null) {
                for (var9 = 0; var9 < this.baseColors.length; ++var9) {
                    var5.z(this.baseColors[var9], this.texturedColors[var9]);
                }
            }

            if (this.w != null) {
                for (var9 = 0; var9 < this.w.length; ++var9) {
                    var5.w(this.w[var9], this.s[var9]);
                }
            }

            return var5;
        }
    }

    @ObfuscatedName("w")
    public final boolean w(boolean var1) {
        int var2 = this.at;
        int var3 = this.ag;
        if (var1) {
            var2 = this.as;
            var3 = this.aw;
        }

        if (var2 == -1) {
            return true;
        } else {
            boolean var4 = true;
            if (!e.l(var2, 0)) {
                var4 = false;
            }

            if (var3 != -1 && !e.l(var3, 0)) {
                var4 = false;
            }

            return var4;
        }
    }

    @ObfuscatedName("s")
    public final AlternativeModel s(boolean var1) {
        int var2 = this.at;
        int var3 = this.ag;
        if (var1) {
            var2 = this.as;
            var3 = this.aw;
        }

        if (var2 == -1) {
            return null;
        } else {
            AlternativeModel var4 = AlternativeModel.t(e, var2, 0);
            if (var3 != -1) {
                AlternativeModel var5 = AlternativeModel.t(e, var3, 0);
                AlternativeModel[] var6 = new AlternativeModel[] { var4, var5 };
                var4 = new AlternativeModel(var6, 2);
            }

            int var7;
            if (this.baseColors != null) {
                for (var7 = 0; var7 < this.baseColors.length; ++var7) {
                    var4.z(this.baseColors[var7], this.texturedColors[var7]);
                }
            }

            if (this.w != null) {
                for (var7 = 0; var7 < this.w.length; ++var7) {
                    var4.w(this.w[var7], this.s[var7]);
                }
            }

            return var4;
        }
    }

    @ObfuscatedName("d")
    public int d(int var1, int var2) {
        return ky.q(this.bk, var1, var2);
    }

    @ObfuscatedName("f")
    public String f(int var1, String var2) {
        FixedSizeDeque var4 = this.bk;
        String var3;
        if (var4 == null) {
            var3 = var2;
        } else {
            gy var5 = (gy) var4.t((long) var1);
            if (var5 == null) {
                var3 = var2;
            } else {
                var3 = (String) var5.t;
            }
        }

        return var3;
    }

    @ObfuscatedName("r")
    public int r() {
        if (this.az != -1 && this.inventoryActions != null) {
            if (this.az >= 0) {
                return this.inventoryActions[this.az] != null ? this.az : -1;
            } else {
                return "Drop".equalsIgnoreCase(this.inventoryActions[4]) ? 4 : -1;
            }
        } else {
            return -1;
        }
    }

    @ObfuscatedName("t")
    static int t(FileSystem var0, FileSystem var1) {
        int var2 = 0;
        if (var0.am("title.jpg", "")) {
            ++var2;
        }

        if (var1.am("logo", "")) {
            ++var2;
        }

        if (var1.am("logo_deadman_mode", "")) {
            ++var2;
        }

        if (var1.am("titlebox", "")) {
            ++var2;
        }

        if (var1.am("titlebutton", "")) {
            ++var2;
        }

        if (var1.am("runes", "")) {
            ++var2;
        }

        if (var1.am("title_mute", "")) {
            ++var2;
        }

        if (var1.am("options_radio_buttons,0", "")) {
            ++var2;
        }

        if (var1.am("options_radio_buttons,2", "")) {
            ++var2;
        }

        if (var1.am("options_radio_buttons,4", "")) {
            ++var2;
        }

        if (var1.am("options_radio_buttons,6", "")) {
            ++var2;
        }

        var1.am("sl_back", "");
        var1.am("sl_flags", "");
        var1.am("sl_arrows", "");
        var1.am("sl_stars", "");
        var1.am("sl_button", "");
        return var2;
    }
}
