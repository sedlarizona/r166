import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fh")
public class fh {

    @ObfuscatedName("b")
    public static int b;

    @ObfuscatedName("o")
    public static RSRandomAccessFileChannel o = null;

    @ObfuscatedName("c")
    public static RSRandomAccessFileChannel c = null;

    @ObfuscatedName("v")
    public static RSRandomAccessFileChannel v = null;

    @ObfuscatedName("d")
    public static String d;

    @ObfuscatedName("r")
    public static String r;

    @ObfuscatedName("e")
    public static void e() {
        kw.q.a();
    }

    @ObfuscatedName("he")
    static final void he(boolean var0, Packet var1) {
        Client.ju = 0;
        Client.dm = 0;
        Client.hp();

        int var2;
        Npc var4;
        int var5;
        int var6;
        int var7;
        int var8;
        int var9;
        while (var1.jg(Client.ee.x) >= 27) {
            var2 = var1.jt(15);
            if (var2 == 32767) {
                break;
            }

            boolean var3 = false;
            if (Client.loadedNpcs[var2] == null) {
                Client.loadedNpcs[var2] = new Npc();
                var3 = true;
            }

            var4 = Client.loadedNpcs[var2];
            Client.npcIndices[++Client.dz - 1] = var2;
            var4.cu = Client.bz;
            if (var0) {
                var5 = var1.jt(8);
                if (var5 > 127) {
                    var5 -= 256;
                }
            } else {
                var5 = var1.jt(5);
                if (var5 > 15) {
                    var5 -= 32;
                }
            }

            var6 = var1.jt(1);
            var7 = Client.jj[var1.jt(3)];
            if (var3) {
                var4.ct = var4.orientation = var7;
            }

            if (var0) {
                var8 = var1.jt(8);
                if (var8 > 127) {
                    var8 -= 256;
                }
            } else {
                var8 = var1.jt(5);
                if (var8 > 15) {
                    var8 -= 32;
                }
            }

            var4.composite = ho.q(var1.jt(14));
            var9 = var1.jt(1);
            if (var9 == 1) {
                Client.eb[++Client.dm - 1] = var2;
            }

            var4.ap = var4.composite.e;
            var4.ch = var4.composite.aj;
            if (var4.ch == 0) {
                var4.orientation = 0;
            }

            var4.stanceId = var4.composite.c;
            var4.ai = var4.composite.v;
            var4.al = var4.composite.u;
            var4.at = var4.composite.j;
            var4.au = var4.composite.g;
            var4.ax = var4.composite.n;
            var4.ar = var4.composite.o;
            var4.q(az.il.co[0] + var8, az.il.cv[0] + var5, var6 == 1);
        }

        var1.ju();

        int var14;
        for (var2 = 0; var2 < Client.dm; ++var2) {
            var14 = Client.eb[var2];
            var4 = Client.loadedNpcs[var14];
            var5 = var1.av();
            if ((var5 & 32) != 0) {
                var6 = var1.ae();
                var7 = var1.bq();
                var8 = var4.regionX - (var6 - an.regionBaseX - an.regionBaseX) * 64;
                var9 = var4.regionY - (var7 - PlayerComposite.ep - PlayerComposite.ep) * 64;
                if (var8 != 0 || var9 != 0) {
                    var4.bh = (int) (Math.atan2((double) var8, (double) var9) * 325.949D) & 2047;
                }
            }

            if ((var5 & 16) != 0) {
                var4.bx = var1.bq();
                var6 = var1.ap();
                var4.bi = var6 >> 16;
                var4.bv = (var6 & '\uffff') + Client.bz;
                var4.graphicId = 0;
                var4.bo = 0;
                if (var4.bv > Client.bz) {
                    var4.graphicId = -1;
                }

                if (var4.bx == 65535) {
                    var4.bx = -1;
                }
            }

            if ((var5 & 8) != 0) {
                var4.textSpoken = var1.ar();
                var4.aa = 100;
            }

            if ((var5 & 2) != 0) {
                var4.interactingEntityIndex = var1.bz();
                if (var4.interactingEntityIndex == 65535) {
                    var4.interactingEntityIndex = -1;
                }
            }

            if ((var5 & 64) != 0) {
                var4.composite = ho.q(var1.bz());
                var4.ap = var4.composite.e;
                var4.ch = var4.composite.aj;
                var4.stanceId = var4.composite.c;
                var4.ai = var4.composite.v;
                var4.al = var4.composite.u;
                var4.at = var4.composite.j;
                var4.au = var4.composite.g;
                var4.ax = var4.composite.n;
                var4.ar = var4.composite.o;
            }

            if ((var5 & 4) != 0) {
                var6 = var1.av();
                int var10;
                int var11;
                int var12;
                if (var6 > 0) {
                    for (var7 = 0; var7 < var6; ++var7) {
                        var9 = -1;
                        var10 = -1;
                        var11 = -1;
                        var8 = var1.ag();
                        if (var8 == 32767) {
                            var8 = var1.ag();
                            var10 = var1.ag();
                            var9 = var1.ag();
                            var11 = var1.ag();
                        } else if (var8 != 32766) {
                            var10 = var1.ag();
                        } else {
                            var8 = -1;
                        }

                        var12 = var1.ag();
                        var4.ad(var8, var10, var9, var11, Client.bz, var12);
                    }
                }

                var7 = var1.bc();
                if (var7 > 0) {
                    for (var8 = 0; var8 < var7; ++var8) {
                        var9 = var1.ag();
                        var10 = var1.ag();
                        if (var10 != 32767) {
                            var11 = var1.ag();
                            var12 = var1.av();
                            int var13 = var10 > 0 ? var1.bc() : var12;
                            var4.bg(var9, Client.bz, var10, var11, var12, var13);
                        } else {
                            var4.br(var9);
                        }
                    }
                }
            }

            if ((var5 & 1) != 0) {
                var6 = var1.bz();
                if (var6 == 65535) {
                    var6 = -1;
                }

                var7 = var1.av();
                if (var6 == var4.animationId && var6 != -1) {
                    var8 = ff.t(var6).w;
                    if (var8 == 1) {
                        var4.animationFrameIndex = 0;
                        var4.bb = 0;
                        var4.bq = var7;
                        var4.bz = 0;
                    }

                    if (var8 == 2) {
                        var4.bz = 0;
                    }
                } else if (var6 == -1 || var4.animationId == -1 || ff.t(var6).c >= ff.t(var4.animationId).c) {
                    var4.animationId = var6;
                    var4.animationFrameIndex = 0;
                    var4.bb = 0;
                    var4.bq = var7;
                    var4.bz = 0;
                    var4.ci = var4.speed;
                }
            }
        }

        for (var2 = 0; var2 < Client.ju; ++var2) {
            var14 = Client.jg[var2];
            if (Client.loadedNpcs[var14].cu != Client.bz) {
                Client.loadedNpcs[var14].composite = null;
                Client.loadedNpcs[var14] = null;
            }
        }

        if (var1.index != Client.ee.x) {
            throw new RuntimeException(var1.index + "," + Client.ee.x);
        } else {
            for (var2 = 0; var2 < Client.dz; ++var2) {
                if (Client.loadedNpcs[Client.npcIndices[var2]] == null) {
                    throw new RuntimeException(var2 + "," + Client.dz);
                }
            }

        }
    }

    @ObfuscatedName("il")
    static final void il(RTComponent[] var0, int var1) {
        for (int var2 = 0; var2 < var0.length; ++var2) {
            RTComponent var3 = var0[var2];
            if (var3 != null) {
                if (var3.type == 0) {
                    if (var3.cs2components != null) {
                        il(var3.cs2components, var1);
                    }

                    SubWindow var4 = (SubWindow) Client.subWindowTable.t((long) var3.id);
                    if (var4 != null) {
                        g.iz(var4.targetWindowId, var1);
                    }
                }

                ScriptEvent var5;
                if (var1 == 0 && var3.dz != null) {
                    var5 = new ScriptEvent();
                    var5.i = var3;
                    var5.args = var3.dz;
                    m.t(var5, 500000);
                }

                if (var1 == 1 && var3.ds != null) {
                    if (var3.w >= 0) {
                        RTComponent var6 = Inflater.t(var3.id);
                        if (var6 == null || var6.cs2components == null || var3.w >= var6.cs2components.length
                                || var3 != var6.cs2components[var3.w]) {
                            continue;
                        }
                    }

                    var5 = new ScriptEvent();
                    var5.i = var3;
                    var5.args = var3.ds;
                    m.t(var5, 500000);
                }
            }
        }

    }
}
