import java.applet.Applet;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("eq")
public class eq extends ky {

    @ObfuscatedName("km")
    static int km;

    @ObfuscatedName("t")
    final boolean t;

    public eq(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (Client.currentWorld == var1.l && var2.l == Client.currentWorld) {
            return this.t ? var1.r().i(var2.r()) : var2.r().i(var1.r());
        } else {
            return this.p(var1, var2);
        }
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("t")
    public static void t(Applet var0, String var1) {
        bc.t = var0;
        if (var1 != null) {
            bc.q = var1;
        }

    }

    @ObfuscatedName("fd")
    static void fd(kf var0, int var1, int var2, int var3) {
        if (Client.audioEffectCount < 50 && Client.pa != 0) {
            if (var0.p != null && var1 < var0.p.length) {
                int var4 = var0.p[var1];
                if (var4 != 0) {
                    int var5 = var4 >> 8;
                    int var6 = var4 >> 4 & 7;
                    int var7 = var4 & 15;
                    Client.po[Client.audioEffectCount] = var5;
                    Client.pe[Client.audioEffectCount] = var6;
                    Client.pt[Client.audioEffectCount] = 0;
                    Client.audioTracks[Client.audioEffectCount] = null;
                    int var8 = (var2 - 64) / 128;
                    int var9 = (var3 - 64) / 128;
                    Client.pl[Client.audioEffectCount] = var7 + (var9 << 8) + (var8 << 16);
                    ++Client.audioEffectCount;
                }
            }
        }
    }
}
