import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("am")
public class am {

    @ObfuscatedName("gz")
    static int gz;

    @ObfuscatedName("kt")
    static MenuItemNode kt;

    @ObfuscatedName("t")
    final int t;

    @ObfuscatedName("q")
    final int q;

    @ObfuscatedName("i")
    final int i;

    am(int var1, int var2, int var3) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
    }

    @ObfuscatedName("q")
    public static int q(byte[] var0, int var1) {
        return fv.t(var0, 0, var1);
    }
}
