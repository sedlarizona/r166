import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.SyncFailedException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("de")
public final class RSRandomAccessFile {

    @ObfuscatedName("t")
    RandomAccessFile file;

    @ObfuscatedName("q")
    long length;

    @ObfuscatedName("i")
    long position;

    public RSRandomAccessFile(File var1, String var2, long var3) throws IOException {
        if (var3 == -1L) {
            var3 = Long.MAX_VALUE;
        }

        if (var1.length() >= var3) {
            var1.delete();
        }

        this.file = new RandomAccessFile(var1, var2);
        this.length = var3;
        this.position = 0L;
        int var5 = this.file.read();
        if (var5 != -1 && !var2.equals("r")) {
            this.file.seek(0L);
            this.file.write(var5);
        }

        this.file.seek(0L);
    }

    @ObfuscatedName("t")
    final void t(long var1)
            throws IOException {
        this.file.seek(var1);
        this.position = var1;
    }

    @ObfuscatedName("q")
    public final void q(byte[] var1, int var2, int var3)
            throws IOException {
        if (this.position + (long) var3 > this.length) {
            this.file.seek(1L + this.length);
            this.file.write(1);
            throw new EOFException();
        } else {
            this.file.write(var1, var2, var3);
            this.position += (long) var3;
        }
    }

    @ObfuscatedName("i")
    public final void i()
            throws IOException {
        this.a(false);
    }

    @ObfuscatedName("a")
    public final void a(boolean var1)
            throws IOException {
        if (this.file != null) {
            if (var1) {
                try {
                    this.file.getFD().sync();
                } catch (SyncFailedException var3) {
                    ;
                }
            }

            this.file.close();
            this.file = null;
        }

    }

    @ObfuscatedName("l")
    public final long l()
            throws IOException {
        return this.file.length();
    }

    @ObfuscatedName("b")
    public final int b(byte[] var1, int var2, int var3)
            throws IOException {
        int var4 = this.file.read(var1, var2, var3);
        if (var4 > 0) {
            this.position += (long) var4;
        }

        return var4;
    }

    protected void finalize()
            throws Throwable {
        if (this.file != null) {
            System.out.println("");
            this.i();
        }

    }

    @ObfuscatedName("u")
    static final void u() {
        ak.p("You can't add yourself to your own friend list");
    }

    @ObfuscatedName("s")
    static int s(int var0, RuneScript var1, boolean var2) {
        if (var0 == 3300) {
            cs.e[++b.menuX - 1] = Client.bz;
            return 1;
        } else {
            int var3;
            int var4;
            if (var0 == 3301) {
                b.menuX -= 2;
                var3 = cs.e[b.menuX];
                var4 = cs.e[b.menuX + 1];
                cs.e[++b.menuX - 1] = o.t(var3, var4);
                return 1;
            } else if (var0 == 3302) {
                b.menuX -= 2;
                var3 = cs.e[b.menuX];
                var4 = cs.e[b.menuX + 1];
                cs.e[++b.menuX - 1] = bg.q(var3, var4);
                return 1;
            } else if (var0 == 3303) {
                b.menuX -= 2;
                var3 = cs.e[b.menuX];
                var4 = cs.e[b.menuX + 1];
                cs.e[++b.menuX - 1] = o.i(var3, var4);
                return 1;
            } else {
                int var5;
                if (var0 == 3304) {
                    var3 = cs.e[--b.menuX];
                    int[] var9 = cs.e;
                    var5 = ++b.menuX - 1;
                    InventoryDefinition var7 = (InventoryDefinition) InventoryDefinition.q.t((long) var3);
                    InventoryDefinition var6;
                    if (var7 != null) {
                        var6 = var7;
                    } else {
                        byte[] var13 = InventoryDefinition.t.i(5, var3);
                        var7 = new InventoryDefinition();
                        if (var13 != null) {
                            var7.q(new ByteBuffer(var13));
                        }

                        InventoryDefinition.q.i(var7, (long) var3);
                        var6 = var7;
                    }

                    var9[var5] = var6.capacity;
                    return 1;
                } else if (var0 == 3305) {
                    var3 = cs.e[--b.menuX];
                    cs.e[++b.menuX - 1] = Client.jb[var3];
                    return 1;
                } else if (var0 == 3306) {
                    var3 = cs.e[--b.menuX];
                    cs.e[++b.menuX - 1] = Client.baseSkillLevels[var3];
                    return 1;
                } else if (var0 == 3307) {
                    var3 = cs.e[--b.menuX];
                    cs.e[++b.menuX - 1] = Client.skillExperiences[var3];
                    return 1;
                } else if (var0 == 3308) {
                    var3 = kt.ii;
                    var4 = (az.il.regionX >> 7) + an.regionBaseX;
                    var5 = (az.il.regionY >> 7) + PlayerComposite.ep;
                    cs.e[++b.menuX - 1] = (var4 << 14) + var5 + (var3 << 28);
                    return 1;
                } else if (var0 == 3309) {
                    var3 = cs.e[--b.menuX];
                    cs.e[++b.menuX - 1] = var3 >> 14 & 16383;
                    return 1;
                } else if (var0 == 3310) {
                    var3 = cs.e[--b.menuX];
                    cs.e[++b.menuX - 1] = var3 >> 28;
                    return 1;
                } else if (var0 == 3311) {
                    var3 = cs.e[--b.menuX];
                    cs.e[++b.menuX - 1] = var3 & 16383;
                    return 1;
                } else if (var0 == 3312) {
                    cs.e[++b.menuX - 1] = Client.membersWorld ? 1 : 0;
                    return 1;
                } else if (var0 == 3313) {
                    b.menuX -= 2;
                    var3 = cs.e[b.menuX] + '耀';
                    var4 = cs.e[b.menuX + 1];
                    cs.e[++b.menuX - 1] = o.t(var3, var4);
                    return 1;
                } else {
                    int var11;
                    if (var0 != 3314) {
                        if (var0 == 3315) {
                            b.menuX -= 2;
                            var3 = cs.e[b.menuX] + '耀';
                            var4 = cs.e[b.menuX + 1];
                            cs.e[++b.menuX - 1] = o.i(var3, var4);
                            return 1;
                        } else if (var0 == 3316) {
                            if (Client.localPlayerRights >= 2) {
                                cs.e[++b.menuX - 1] = Client.localPlayerRights;
                            } else {
                                cs.e[++b.menuX - 1] = 0;
                            }

                            return 1;
                        } else if (var0 == 3317) {
                            cs.e[++b.menuX - 1] = Client.mouseTrackerIdleTime;
                            return 1;
                        } else if (var0 == 3318) {
                            cs.e[++b.menuX - 1] = Client.currentWorld;
                            return 1;
                        } else if (var0 == 3321) {
                            cs.e[++b.menuX - 1] = Client.lx;
                            return 1;
                        } else if (var0 == 3322) {
                            cs.e[++b.menuX - 1] = Client.ll;
                            return 1;
                        } else if (var0 == 3323) {
                            if (Client.lb) {
                                cs.e[++b.menuX - 1] = 1;
                            } else {
                                cs.e[++b.menuX - 1] = 0;
                            }

                            return 1;
                        } else if (var0 == 3324) {
                            cs.e[++b.menuX - 1] = Client.br;
                            return 1;
                        } else if (var0 == 3325) {
                            b.menuX -= 4;
                            var3 = cs.e[b.menuX];
                            var4 = cs.e[b.menuX + 1];
                            var5 = cs.e[b.menuX + 2];
                            var11 = cs.e[b.menuX + 3];
                            var3 += var4 << 14;
                            var3 += var5 << 28;
                            var3 += var11;
                            cs.e[++b.menuX - 1] = var3;
                            return 1;
                        } else {
                            return 2;
                        }
                    } else {
                        b.menuX -= 2;
                        var3 = cs.e[b.menuX] + '耀';
                        var4 = cs.e[b.menuX + 1];
                        int[] var10 = cs.e;
                        var11 = ++b.menuX - 1;
                        ItemStorage var8 = (ItemStorage) ItemStorage.t.t((long) var3);
                        int var12;
                        if (var8 == null) {
                            var12 = 0;
                        } else if (var4 >= 0 && var4 < var8.stackSizes.length) {
                            var12 = var8.stackSizes[var4];
                        } else {
                            var12 = 0;
                        }

                        var10[var11] = var12;
                        return 1;
                    }
                }
            }
        }
    }

    @ObfuscatedName("kw")
    public static void kw(int var0, int var1, int var2, boolean var3) {
        gd var4 = ap.t(fo.bx, Client.ee.l);
        var4.i.by(var1);
        var4.i.ba(var2);
        var4.i.l(var0);
        var4.i.bu(var3 ? Client.hm : 0);
        Client.ee.i(var4);
    }
}
