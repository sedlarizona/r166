import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cj")
public class RuneScript extends hh {

    @ObfuscatedName("t")
    static Cache t = new Cache(128);

    @ObfuscatedName("q")
    int[] q;

    @ObfuscatedName("i")
    int[] i;

    @ObfuscatedName("a")
    String[] a;

    @ObfuscatedName("l")
    int stringArgCount;

    @ObfuscatedName("b")
    int stringStackCount;

    @ObfuscatedName("e")
    int intArgCount;

    @ObfuscatedName("x")
    int intStackCount;

    @ObfuscatedName("p")
    FixedSizeDeque[] p;

    @ObfuscatedName("q")
    FixedSizeDeque[] q(int var1) {
        return new FixedSizeDeque[var1];
    }

    @ObfuscatedName("t")
    static ho t(FileSystem var0, int var1) {
        byte[] var2 = var0.o(var1);
        return var2 == null ? null : new ho(var2);
    }

    @ObfuscatedName("i")
    public static boolean i(int var0) {
        if (c.loadedInterfaces[var0]) {
            return true;
        } else if (!RTComponent.x.e(var0)) {
            return false;
        } else {
            int var1 = RTComponent.x.w(var0);
            if (var1 == 0) {
                c.loadedInterfaces[var0] = true;
                return true;
            } else {
                if (RTComponent.b[var0] == null) {
                    RTComponent.b[var0] = new RTComponent[var1];
                }

                for (int var2 = 0; var2 < var1; ++var2) {
                    if (RTComponent.b[var0][var2] == null) {
                        byte[] var3 = RTComponent.x.i(var0, var2);
                        if (var3 != null) {
                            RTComponent.b[var0][var2] = new RTComponent();
                            RTComponent.b[var0][var2].id = var2 + (var0 << 16);
                            if (var3[0] == -1) {
                                RTComponent.b[var0][var2].l(new ByteBuffer(var3));
                            } else {
                                RTComponent.b[var0][var2].a(new ByteBuffer(var3));
                            }
                        }
                    }
                }

                c.loadedInterfaces[var0] = true;
                return true;
            }
        }
    }
}
