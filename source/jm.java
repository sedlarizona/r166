import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jm")
public enum jm implements gl {
    @ObfuscatedName("t")
    t(2, 0), @ObfuscatedName("q")
    q(0, 1), @ObfuscatedName("i")
    i(1, 2);

    @ObfuscatedName("a")
    public final int a;

    @ObfuscatedName("l")
    final int l;

    jm(int var3, int var4) {
        this.a = var3;
        this.l = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.l;
    }
}
