import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hs")
public abstract class hs extends hh {

    @ObfuscatedName("q")
    final int q;

    hs(int var1) {
        this.q = var1;
    }

    @ObfuscatedName("t")
    abstract Object t();

    @ObfuscatedName("q")
    abstract boolean q();
}
