import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bx")
public class ScriptEvent extends Node {

    @ObfuscatedName("t")
    Object[] args;

    @ObfuscatedName("q")
    boolean q;

    @ObfuscatedName("i")
    RTComponent i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    RTComponent e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    String name;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    iw o;

    public ScriptEvent() {
        this.o = iw.g;
    }

    @ObfuscatedName("t")
    public void t(Object[] var1) {
        this.args = var1;
    }

    @ObfuscatedName("q")
    public void q(iw var1) {
        this.o = var1;
    }

    @ObfuscatedName("ex")
    static void ex(ju var0, String var1) {
        bn var2 = new bn(var0, var1);
        Client.rk.add(var2);
    }

    @ObfuscatedName("ka")
    static String ka(String var0) {
        il[] var1 = new il[] { il.t, il.b, il.i, il.a, il.q, il.l };
        il[] var2 = var1;

        for (int var3 = 0; var3 < var2.length; ++var3) {
            il var4 = var2[var3];
            if (var4.x != -1 && var0.startsWith(fp.t(var4.x))) {
                var0 = var0.substring(6 + Integer.toString(var4.x).length());
                break;
            }
        }

        return var0;
    }
}
