import java.applet.Applet;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.net.URL;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.rs.Reflection;

@ObfuscatedName("bh")
public abstract class GameEngine extends Applet implements Runnable, FocusListener, WindowListener {

    @ObfuscatedName("t")
    protected static TaskHandler t;

    @ObfuscatedName("q")
    static GameEngine q = null;

    @ObfuscatedName("i")
    static int i = 0;

    @ObfuscatedName("a")
    static long a = 0L;

    @ObfuscatedName("l")
    static boolean l = false;

    @ObfuscatedName("p")
    static int p = 20;

    @ObfuscatedName("g")
    static int g = 1;

    @ObfuscatedName("n")
    protected static int n = 0;

    @ObfuscatedName("o")
    static fr o;

    @ObfuscatedName("v")
    static long[] v = new long[32];

    @ObfuscatedName("j")
    static long[] j = new long[32];

    @ObfuscatedName("h")
    protected static int h;

    @ObfuscatedName("au")
    static int au = 500;

    @ObfuscatedName("aw")
    static volatile boolean aw = true;

    @ObfuscatedName("af")
    protected static long af = -1L;

    @ObfuscatedName("ak")
    protected static long ak = -1L;

    @ObfuscatedName("b")
    boolean b = false;

    @ObfuscatedName("z")
    protected int z;

    @ObfuscatedName("s")
    protected int s;

    @ObfuscatedName("d")
    int d = 0;

    @ObfuscatedName("f")
    int f = 0;

    @ObfuscatedName("m")
    int m;

    @ObfuscatedName("ay")
    int ay;

    @ObfuscatedName("ao")
    int ao;

    @ObfuscatedName("av")
    int av;

    @ObfuscatedName("am")
    Frame am;

    @ObfuscatedName("az")
    java.awt.Canvas canvas;

    @ObfuscatedName("ap")
    volatile boolean dead = true;

    @ObfuscatedName("ax")
    boolean ax = false;

    @ObfuscatedName("ar")
    volatile boolean ar = false;

    @ObfuscatedName("an")
    volatile long an = 0L;

    @ObfuscatedName("at")
    MouseWheelTracker mouseWheelTracker;

    @ObfuscatedName("ag")
    Clipboard ag;

    @ObfuscatedName("as")
    final EventQueue as;

    protected GameEngine() {
        EventQueue var1 = null;

        try {
            var1 = Toolkit.getDefaultToolkit().getSystemEventQueue();
        } catch (Throwable var3) {
            ;
        }

        this.as = var1;
        Server.ae(new bg());
    }

    @ObfuscatedName("b")
    protected final void b(int var1, int var2) {
        if (this.ao != var1 || var2 != this.av) {
            this.aj();
        }

        this.ao = var1;
        this.av = var2;
    }

    @ObfuscatedName("e")
    final void e(Object var1) {
        if (this.as != null) {
            for (int var2 = 0; var2 < 50 && this.as.peekEvent() != null; ++var2) {
                cx.t(1L);
            }

            if (var1 != null) {
                this.as.postEvent(new ActionEvent(var1, 1001, "dummy"));
            }

        }
    }

    @ObfuscatedName("x")
    protected fs x() {
        if (this.mouseWheelTracker == null) {
            this.mouseWheelTracker = new MouseWheelTracker();
            this.mouseWheelTracker.t(this.canvas);
        }

        return this.mouseWheelTracker;
    }

    @ObfuscatedName("p")
    protected void p() {
        this.ag = this.getToolkit().getSystemClipboard();
    }

    @ObfuscatedName("o")
    protected void o(String var1) {
        this.ag.setContents(new StringSelection(var1), (ClipboardOwner) null);
    }

    @ObfuscatedName("c")
    protected final void c() {
        gq.t();
        PlayerComposite.q(this.canvas);
    }

    @ObfuscatedName("u")
    protected final void u() {
        ad.t(this.canvas);
    }

    @ObfuscatedName("k")
    final void k() {
        Container var1 = this.aq();
        if (var1 != null) {
            lb var2 = this.aa();
            this.z = Math.max(var2.i, this.m);
            this.s = Math.max(var2.a, this.ay);
            if (this.z <= 0) {
                this.z = 1;
            }

            if (this.s <= 0) {
                this.s = 1;
            }

            BoundaryObject.r = Math.min(this.z, this.ao);
            h = Math.min(this.s, this.av);
            this.d = (this.z - BoundaryObject.r) / 2;
            this.f = 0;
            this.canvas.setSize(BoundaryObject.r, h);
            ad.interfaceProducer = new ComponentProducer(BoundaryObject.r, h, this.canvas);
            if (var1 == this.am) {
                Insets var3 = this.am.getInsets();
                this.canvas.setLocation(this.d + var3.left, var3.top + this.f);
            } else {
                this.canvas.setLocation(this.d, this.f);
            }

            this.dead = true;
            this.z();
        }
    }

    @ObfuscatedName("z")
    protected abstract void z();

    @ObfuscatedName("w")
    void w() {
        int var1 = this.d;
        int var2 = this.f;
        int var3 = this.z - BoundaryObject.r - var1;
        int var4 = this.s - h - var2;
        if (var1 > 0 || var3 > 0 || var2 > 0 || var4 > 0) {
            try {
                Container var5 = this.aq();
                int var6 = 0;
                int var7 = 0;
                if (var5 == this.am) {
                    Insets var8 = this.am.getInsets();
                    var6 = var8.left;
                    var7 = var8.top;
                }

                Graphics var10 = var5.getGraphics();
                var10.setColor(Color.black);
                if (var1 > 0) {
                    var10.fillRect(var6, var7, var1, this.s);
                }

                if (var2 > 0) {
                    var10.fillRect(var6, var7, this.z, var2);
                }

                if (var3 > 0) {
                    var10.fillRect(var6 + this.z - var3, var7, var3, this.s);
                }

                if (var4 > 0) {
                    var10.fillRect(var6, var7 + this.s - var4, this.z, var4);
                }
            } catch (Exception var9) {
                ;
            }
        }

    }

    @ObfuscatedName("s")
    final void s() {
        java.awt.Canvas var1 = this.canvas;
        var1.removeKeyListener(ad.keyboard);
        var1.removeFocusListener(ad.keyboard);
        ad.cc = -1;
        java.awt.Canvas var2 = this.canvas;
        var2.removeMouseListener(bs.mouse);
        var2.removeMouseMotionListener(bs.mouse);
        var2.removeFocusListener(bs.mouse);
        bs.b = 0;
        if (this.mouseWheelTracker != null) {
            this.mouseWheelTracker.q(this.canvas);
        }

        this.f();
        PlayerComposite.q(this.canvas);
        ad.t(this.canvas);
        if (this.mouseWheelTracker != null) {
            this.mouseWheelTracker.t(this.canvas);
        }

        this.aj();
    }

    @ObfuscatedName("d")
    protected final void d(int var1, int var2, int var3) {
        try {
            if (q != null) {
                ++i;
                if (i >= 3) {
                    this.aw("alreadyloaded");
                    return;
                }

                this.getAppletContext().showDocument(this.getDocumentBase(), "_self");
                return;
            }

            q = this;
            BoundaryObject.r = var1;
            h = var2;
            kx.version = var3;
            RSException.q = this;
            if (t == null) {
                t = new TaskHandler();
            }

            t.a(this, 1);
        } catch (Exception var5) {
            FloorObject.t((String) null, var5);
            this.aw("crash");
        }

    }

    @ObfuscatedName("f")
    final synchronized void f() {
        Container var1 = this.aq();
        if (this.canvas != null) {
            this.canvas.removeFocusListener(this);
            var1.remove(this.canvas);
        }

        BoundaryObject.r = Math.max(var1.getWidth(), this.m);
        h = Math.max(var1.getHeight(), this.ay);
        Insets var2;
        if (this.am != null) {
            var2 = this.am.getInsets();
            BoundaryObject.r -= var2.right + var2.left;
            h -= var2.bottom + var2.top;
        }

        this.canvas = new Canvas(this);
        var1.add(this.canvas);
        this.canvas.setSize(BoundaryObject.r, h);
        this.canvas.setVisible(true);
        this.canvas.setBackground(Color.BLACK);
        if (var1 == this.am) {
            var2 = this.am.getInsets();
            this.canvas.setLocation(var2.left + this.d, this.f + var2.top);
        } else {
            this.canvas.setLocation(this.d, this.f);
        }

        this.canvas.addFocusListener(this);
        this.canvas.requestFocus();
        this.dead = true;
        if (ad.interfaceProducer != null && BoundaryObject.r == ad.interfaceProducer.width
                && h == ad.interfaceProducer.height) {
            ((ComponentProducer) ad.interfaceProducer).t(this.canvas);
            ad.interfaceProducer.q(0, 0);
        } else {
            ad.interfaceProducer = new ComponentProducer(BoundaryObject.r, h, this.canvas);
        }

        this.ar = false;
        this.an = au.t();
    }

    @ObfuscatedName("r")
    protected final boolean r() {
        String var1 = this.getDocumentBase().getHost().toLowerCase();
        if (!var1.equals("jagex.com") && !var1.endsWith(".jagex.com")) {
            if (!var1.equals("runescape.com") && !var1.endsWith(".runescape.com")) {
                if (var1.endsWith("127.0.0.1")) {
                    return true;
                } else {
                    while (var1.length() > 0 && var1.charAt(var1.length() - 1) >= '0'
                            && var1.charAt(var1.length() - 1) <= '9') {
                        var1 = var1.substring(0, var1.length() - 1);
                    }

                    if (var1.endsWith("192.168.1.")) {
                        return true;
                    } else {
                        this.aw("invalidhost");
                        return false;
                    }
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @ObfuscatedName("y")
    void y() {
        long var1 = au.t();
        long var3 = j[jv.k];
        j[jv.k] = var1;
        jv.k = jv.k + 1 & 31;
        if (0L != var3 && var1 > var3) {
            ;
        }

        synchronized (this) {
            ByteBuffer.aq = aw;
        }

        this.ap();
    }

    @ObfuscatedName("h")
    void h() {
        Container var1 = this.aq();
        long var2 = au.t();
        long var4 = v[a.u];
        v[a.u] = var2;
        a.u = a.u + 1 & 31;
        if (0L != var4 && var2 > var4) {
            int var6 = (int) (var2 - var4);
            n = ((var6 >> 1) + 32000) / var6;
        }

        if (++au - 1 > 50) {
            au -= 50;
            this.dead = true;
            this.canvas.setSize(BoundaryObject.r, h);
            this.canvas.setVisible(true);
            if (var1 == this.am) {
                Insets var7 = this.am.getInsets();
                this.canvas.setLocation(this.d + var7.left, this.f + var7.top);
            } else {
                this.canvas.setLocation(this.d, this.f);
            }
        }

        if (this.ar) {
            this.s();
        }

        this.av();
        this.ah(this.dead);
        if (this.dead) {
            this.w();
        }

        this.dead = false;
    }

    @ObfuscatedName("av")
    final void av() {
        lb var1 = this.aa();
        if (this.z != var1.i || this.s != var1.a || this.ax) {
            this.k();
            this.ax = false;
        }

    }

    @ObfuscatedName("aj")
    final void aj() {
        this.ax = true;
    }

    @ObfuscatedName("ae")
    final synchronized void ae() {
        if (!l) {
            l = true;

            try {
                this.canvas.removeFocusListener(this);
            } catch (Exception var5) {
                ;
            }

            try {
                this.at();
            } catch (Exception var4) {
                ;
            }

            if (this.am != null) {
                try {
                    System.exit(0);
                } catch (Throwable var3) {
                    ;
                }
            }

            if (t != null) {
                try {
                    t.t();
                } catch (Exception var2) {
                    ;
                }
            }

            this.ak();
        }
    }

    @ObfuscatedName("az")
    protected abstract void az();

    @ObfuscatedName("ap")
    protected abstract void ap();

    @ObfuscatedName("ah")
    protected abstract void ah(boolean var1);

    @ObfuscatedName("at")
    protected abstract void at();

    @ObfuscatedName("ag")
    protected final void ag(int var1, String var2, boolean var3) {
        try {
            Graphics var4 = this.canvas.getGraphics();
            if (r.aj == null) {
                r.aj = new Font("Helvetica", 1, 13);
                gn.ae = this.canvas.getFontMetrics(r.aj);
            }

            if (var3) {
                var4.setColor(Color.black);
                var4.fillRect(0, 0, BoundaryObject.r, h);
            }

            Color var5 = new Color(140, 17, 17);

            try {
                if (ec.ah == null) {
                    ec.ah = this.canvas.createImage(304, 34);
                }

                Graphics var6 = ec.ah.getGraphics();
                var6.setColor(var5);
                var6.drawRect(0, 0, 303, 33);
                var6.fillRect(2, 2, var1 * 3, 30);
                var6.setColor(Color.black);
                var6.drawRect(1, 1, 301, 31);
                var6.fillRect(var1 * 3 + 2, 2, 300 - var1 * 3, 30);
                var6.setFont(r.aj);
                var6.setColor(Color.white);
                var6.drawString(var2, (304 - gn.ae.stringWidth(var2)) / 2, 22);
                var4.drawImage(ec.ah, BoundaryObject.r / 2 - 152, h / 2 - 18, (ImageObserver) null);
            } catch (Exception var9) {
                int var7 = BoundaryObject.r / 2 - 152;
                int var8 = h / 2 - 18;
                var4.setColor(var5);
                var4.drawRect(var7, var8, 303, 33);
                var4.fillRect(var7 + 2, var8 + 2, var1 * 3, 30);
                var4.setColor(Color.black);
                var4.drawRect(var7 + 1, var8 + 1, 301, 31);
                var4.fillRect(var1 * 3 + var7 + 2, var8 + 2, 300 - var1 * 3, 30);
                var4.setFont(r.aj);
                var4.setColor(Color.white);
                var4.drawString(var2, var7 + (304 - gn.ae.stringWidth(var2)) / 2, var8 + 22);
            }
        } catch (Exception var10) {
            this.canvas.repaint();
        }

    }

    @ObfuscatedName("as")
    protected final void as() {
        ec.ah = null;
        r.aj = null;
        gn.ae = null;
    }

    @ObfuscatedName("aw")
    protected void aw(String var1) {
        if (!this.b) {
            this.b = true;
            System.out.println("error_game_" + var1);

            try {
                this.getAppletContext()
                        .showDocument(new URL(this.getCodeBase(), "error_game_" + var1 + ".ws"), "_self");
            } catch (Exception var3) {
                ;
            }

        }
    }

    @ObfuscatedName("aq")
    Container aq() {
        return (Container) (this.am != null ? this.am : this);
    }

    @ObfuscatedName("aa")
    lb aa() {
        Container var1 = this.aq();
        int var2 = Math.max(var1.getWidth(), this.m);
        int var3 = Math.max(var1.getHeight(), this.ay);
        if (this.am != null) {
            Insets var4 = this.am.getInsets();
            var2 -= var4.right + var4.left;
            var3 -= var4.top + var4.bottom;
        }

        return new lb(var2, var3);
    }

    @ObfuscatedName("af")
    protected final boolean af() {
        return this.am != null;
    }

    @ObfuscatedName("ak")
    protected abstract void ak();

    public final void destroy() {
        if (this == q && !l) {
            a = au.t();
            cx.t(5000L);
            this.ae();
        }
    }

    public final synchronized void paint(Graphics var1) {
        if (this == q && !l) {
            this.dead = true;
            if (au.t() - this.an > 1000L) {
                Rectangle var2 = var1.getClipBounds();
                if (var2 == null || var2.width >= BoundaryObject.r && var2.height >= h) {
                    this.ar = true;
                }
            }

        }
    }

    public final void focusGained(FocusEvent var1) {
        aw = true;
        this.dead = true;
    }

    public final void start() {
        if (this == q && !l) {
            a = 0L;
        }
    }

    public final void windowActivated(WindowEvent var1) {
    }

    public final void windowClosed(WindowEvent var1) {
    }

    public final void windowDeiconified(WindowEvent var1) {
    }

    public final void windowIconified(WindowEvent var1) {
    }

    public final void windowOpened(WindowEvent var1) {
    }

    public abstract void init();

    public final void update(Graphics var1) {
        this.paint(var1);
    }

    public final void focusLost(FocusEvent var1) {
        aw = false;
    }

    public final void stop() {
        if (this == q && !l) {
            a = au.t() + 4000L;
        }
    }

    public final void windowClosing(WindowEvent var1) {
        this.destroy();
    }

    public final void windowDeactivated(WindowEvent var1) {
    }

    public void run() {
        try {
            if (TaskHandler.t != null) {
                String var1 = TaskHandler.t.toLowerCase();
                if (var1.indexOf("sun") != -1 || var1.indexOf("apple") != -1) {
                    String var2 = TaskHandler.q;
                    if (var2.equals("1.1") || var2.startsWith("1.1.") || var2.equals("1.2") || var2.startsWith("1.2.")
                            || var2.equals("1.3") || var2.startsWith("1.3.") || var2.equals("1.4")
                            || var2.startsWith("1.4.") || var2.equals("1.5") || var2.startsWith("1.5.")
                            || var2.equals("1.6.0")) {
                        this.aw("wrongjava");
                        return;
                    }

                    if (var2.startsWith("1.6.0_")) {
                        int var3;
                        for (var3 = 6; var3 < var2.length(); ++var3) {
                            char var5 = var2.charAt(var3);
                            boolean var4 = var5 >= '0' && var5 <= '9';
                            if (!var4) {
                                break;
                            }
                        }

                        String var6 = var2.substring(6, var3);
                        if (fl.q(var6) && ff.i(var6) < 10) {
                            this.aw("wrongjava");
                            return;
                        }
                    }

                    g = 5;
                }
            }

            this.setFocusCycleRoot(true);
            this.f();
            this.az();
            o = au.b();

            while (a == 0L || au.t() < a) {
                go.e = o.q(p, g);

                for (int var7 = 0; var7 < go.e; ++var7) {
                    this.y();
                }

                this.h();
                this.e(this.canvas);
            }
        } catch (Exception var8) {
            FloorObject.t((String) null, var8);
            this.aw("crash");
        }

        this.ae();
    }

    @ObfuscatedName("a")
    static Class a(String var0)
            throws ClassNotFoundException {
        if (var0.equals("B")) {
            return Byte.TYPE;
        } else if (var0.equals("I")) {
            return Integer.TYPE;
        } else if (var0.equals("S")) {
            return Short.TYPE;
        } else if (var0.equals("J")) {
            return Long.TYPE;
        } else if (var0.equals("Z")) {
            return Boolean.TYPE;
        } else if (var0.equals("F")) {
            return Float.TYPE;
        } else if (var0.equals("D")) {
            return Double.TYPE;
        } else if (var0.equals("C")) {
            return java.lang.Character.TYPE;
        } else {
            return var0.equals("void") ? Void.TYPE : Reflection.findClass(var0);
        }
    }

    @ObfuscatedName("u")
    static final int u(int var0, int var1, int var2) {
        int var3 = 256 - var2;
        return ((var1 & '\uff00') * var2 + (var0 & '\uff00') * var3 & 16711680)
                + ((var1 & 16711935) * var2 + var3 * (var0 & 16711935) & -16711936) >> 8;
    }

    @ObfuscatedName("ig")
    static void ig(RTComponent[] var0, RTComponent var1, boolean var2) {
        int var3 = var1.al != 0 ? var1.al : var1.width;
        int var4 = var1.at != 0 ? var1.at : var1.height;
        bk.ic(var0, var1.id, var3, var4, var2);
        if (var1.cs2components != null) {
            bk.ic(var1.cs2components, var1.id, var3, var4, var2);
        }

        SubWindow var5 = (SubWindow) Client.subWindowTable.t((long) var1.id);
        if (var5 != null) {
            int var6 = var5.targetWindowId;
            if (RuneScript.i(var6)) {
                bk.ic(RTComponent.b[var6], -1, var3, var4, var2);
            }
        }

        if (var1.contentType == 1337) {
            ;
        }

    }

    @ObfuscatedName("jk")
    static void jk(RTComponent var0) {
        if (var0.cycle == Client.nq) {
            Client.outdatedInterfaces[var0.arrayIndex] = true;
        }

    }
}
