import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ky")
public abstract class ky implements Comparator {

    @ObfuscatedName("q")
    Comparator q;

    @ObfuscatedName("x")
    final void x(Comparator var1) {
        if (this.q == null) {
            this.q = var1;
        } else if (this.q instanceof ky) {
            ((ky) this.q).x(var1);
        }

    }

    @ObfuscatedName("p")
    protected final int p(kv var1, kv var2) {
        return this.q == null ? 0 : this.q.compare(var1, var2);
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }

    @ObfuscatedName("q")
    static int q(FixedSizeDeque var0, int var1, int var2) {
        if (var0 == null) {
            return var2;
        } else {
            hc var3 = (hc) var0.t((long) var1);
            return var3 == null ? var2 : var3.t;
        }
    }

    @ObfuscatedName("a")
    public static RSRandomAccessFile a(String var0, String var1, boolean var2) {
        File var3 = new File(ep.l, "preferences" + var0 + ".dat");
        if (var3.exists()) {
            try {
                RSRandomAccessFile var10 = new RSRandomAccessFile(var3, "rw", 10000L);
                return var10;
            } catch (IOException var9) {
                ;
            }
        }

        String var4 = "";
        if (ai.j == 33) {
            var4 = "_rc";
        } else if (ai.j == 34) {
            var4 = "_wip";
        }

        File var5 = new File(fh.r, "jagex_" + var1 + "_preferences" + var0 + var4 + ".dat");
        RSRandomAccessFile var6;
        if (!var2 && var5.exists()) {
            try {
                var6 = new RSRandomAccessFile(var5, "rw", 10000L);
                return var6;
            } catch (IOException var8) {
                ;
            }
        }

        try {
            var6 = new RSRandomAccessFile(var3, "rw", 10000L);
            return var6;
        } catch (IOException var7) {
            throw new RuntimeException();
        }
    }
}
