import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hu")
public class hu implements Iterator {

    @ObfuscatedName("t")
    DoublyNode t;

    @ObfuscatedName("q")
    hh q;

    @ObfuscatedName("i")
    hh i = null;

    hu(DoublyNode var1) {
        this.t = var1;
        this.q = this.t.t.cj;
        this.i = null;
    }

    public Object next() {
        hh var1 = this.q;
        if (var1 == this.t.t) {
            var1 = null;
            this.q = null;
        } else {
            this.q = var1.cj;
        }

        this.i = var1;
        return var1;
    }

    public boolean hasNext() {
        return this.t.t != this.q;
    }

    public void remove() {
        if (this.i == null) {
            throw new IllegalStateException();
        } else {
            this.i.ce();
            this.i = null;
        }
    }
}
