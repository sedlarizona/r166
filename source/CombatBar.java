import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cw")
public class CombatBar extends Node {

    @ObfuscatedName("i")
    CombatBarDefinition definition;

    @ObfuscatedName("a")
    SinglyLinkedList map = new SinglyLinkedList();

    CombatBar(CombatBarDefinition var1) {
        this.definition = var1;
    }

    @ObfuscatedName("t")
    void t(int var1, int var2, int var3, int var4) {
        CombatBarData var5 = null;
        int var6 = 0;

        for (CombatBarData var7 = (CombatBarData) this.map.b(); var7 != null; var7 = (CombatBarData) this.map.x()) {
            ++var6;
            if (var7.initialCycle == var1) {
                var7.t(var1, var2, var3, var4);
                return;
            }

            if (var7.initialCycle <= var1) {
                var5 = var7;
            }
        }

        if (var5 == null) {
            if (var6 < 4) {
                this.map.i(new CombatBarData(var1, var2, var3, var4));
            }

        } else {
            SinglyLinkedList.a(new CombatBarData(var1, var2, var3, var4), var5);
            if (var6 >= 4) {
                this.map.b().kc();
            }

        }
    }

    @ObfuscatedName("q")
    CombatBarData q(int var1) {
        CombatBarData var2 = (CombatBarData) this.map.b();
        if (var2 != null && var2.initialCycle <= var1) {
            for (CombatBarData var3 = (CombatBarData) this.map.x(); var3 != null && var3.initialCycle <= var1; var3 = (CombatBarData) this.map
                    .x()) {
                var2.kc();
                var2 = var3;
            }

            if (this.definition.o + var2.currentCycle + var2.initialCycle > var1) {
                return var2;
            } else {
                var2.kc();
                return null;
            }
        } else {
            return null;
        }
    }

    @ObfuscatedName("i")
    boolean i() {
        return this.map.p();
    }

    @ObfuscatedName("e")
    static Server e() {
        return Server.e < Server.b ? Server.l[++Server.e - 1] : null;
    }
}
