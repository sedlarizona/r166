import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ku")
public abstract class ku {

    @ObfuscatedName("a")
    final int a;

    @ObfuscatedName("l")
    int l = 0;

    @ObfuscatedName("b")
    kv[] b;

    @ObfuscatedName("e")
    HashMap e;

    @ObfuscatedName("x")
    HashMap x;

    @ObfuscatedName("p")
    Comparator p = null;

    ku(int var1) {
        this.a = var1;
        this.b = this.q(var1);
        this.e = new HashMap(var1 / 8);
        this.x = new HashMap(var1 / 8);
    }

    @ObfuscatedName("t")
    abstract kv t();

    @ObfuscatedName("q")
    abstract kv[] q(int var1);

    @ObfuscatedName("c")
    public void c() {
        this.l = 0;
        Arrays.fill(this.b, (Object) null);
        this.e.clear();
        this.x.clear();
    }

    @ObfuscatedName("u")
    public int u() {
        return this.l;
    }

    @ObfuscatedName("k")
    public boolean k() {
        return this.a == this.l;
    }

    @ObfuscatedName("z")
    public boolean z(kb var1) {
        if (!var1.q()) {
            return false;
        } else {
            return this.e.containsKey(var1) ? true : this.x.containsKey(var1);
        }
    }

    @ObfuscatedName("w")
    public kv w(kb var1) {
        kv var2 = this.s(var1);
        return var2 != null ? var2 : this.d(var1);
    }

    @ObfuscatedName("s")
    kv s(kb var1) {
        return !var1.q() ? null : (kv) this.e.get(var1);
    }

    @ObfuscatedName("d")
    kv d(kb var1) {
        return !var1.q() ? null : (kv) this.x.get(var1);
    }

    @ObfuscatedName("f")
    public final boolean f(kb var1) {
        kv var2 = this.s(var1);
        if (var2 == null) {
            return false;
        } else {
            this.r(var2);
            return true;
        }
    }

    @ObfuscatedName("r")
    final void r(kv var1) {
        int var2 = this.am(var1);
        if (var2 != -1) {
            this.au(var2);
            this.az(var1);
        }
    }

    @ObfuscatedName("y")
    kv y(kb var1) {
        return this.h(var1, (kb) null);
    }

    @ObfuscatedName("h")
    kv h(kb var1, kb var2) {
        if (this.w(var1) != null) {
            throw new IllegalStateException();
        } else {
            kv var3 = this.t();
            var3.av(var1, var2);
            this.ap(var3);
            this.ah(var3);
            return var3;
        }
    }

    @ObfuscatedName("av")
    public final kv av(int var1) {
        if (var1 >= 0 && var1 < this.l) {
            return this.b[var1];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @ObfuscatedName("aj")
    public final void aj() {
        if (this.p == null) {
            Arrays.sort(this.b, 0, this.l);
        } else {
            Arrays.sort(this.b, 0, this.l, this.p);
        }

    }

    @ObfuscatedName("ae")
    final void ae(kv var1, kb var2, kb var3) {
        this.az(var1);
        var1.av(var2, var3);
        this.ah(var1);
    }

    @ObfuscatedName("am")
    final int am(kv var1) {
        for (int var2 = 0; var2 < this.l; ++var2) {
            if (this.b[var2] == var1) {
                return var2;
            }
        }

        return -1;
    }

    @ObfuscatedName("az")
    final void az(kv var1) {
        if (this.e.remove(var1.i) == null) {
            throw new IllegalStateException();
        } else {
            if (var1.a != null) {
                this.x.remove(var1.a);
            }

        }
    }

    @ObfuscatedName("ap")
    final void ap(kv var1) {
        this.b[++this.l - 1] = var1;
    }

    @ObfuscatedName("ah")
    final void ah(kv var1) {
        this.e.put(var1.i, var1);
        if (var1.a != null) {
            this.x.put(var1.a, var1);
        }

    }

    @ObfuscatedName("au")
    final void au(int var1) {
        --this.l;
        if (var1 < this.l) {
            System.arraycopy(this.b, var1 + 1, this.b, var1, this.l - var1);
        }

    }

    @ObfuscatedName("ax")
    public final void ax() {
        this.p = null;
    }

    @ObfuscatedName("ar")
    public final void ar(Comparator var1) {
        if (this.p == null) {
            this.p = var1;
        } else if (this.p instanceof ky) {
            ((ky) this.p).x(var1);
        }

    }

    @ObfuscatedName("a")
    public static int a(CharSequence var0, int var1) {
        return ju.l(var0, var1, true);
    }
}
