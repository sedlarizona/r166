import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kx")
public class kx {

    @ObfuscatedName("t")
    public static final kx t = new kx();

    @ObfuscatedName("q")
    public static final kx q = new kx();

    @ObfuscatedName("i")
    public static final kx i = new kx();

    @ObfuscatedName("a")
    public static int version;

    @ObfuscatedName("c")
    public static String c(String var0) {
        int var1 = var0.length();
        char[] var2 = new char[var1];
        byte var3 = 2;

        for (int var4 = 0; var4 < var1; ++var4) {
            char var5 = var0.charAt(var4);
            if (var3 == 0) {
                var5 = java.lang.Character.toLowerCase(var5);
            } else if (var3 == 2 || java.lang.Character.isUpperCase(var5)) {
                char var6;
                if (var5 != 181 && var5 != 131) {
                    var6 = java.lang.Character.toTitleCase(var5);
                } else {
                    var6 = var5;
                }

                var5 = var6;
            }

            if (java.lang.Character.isLetter(var5)) {
                var3 = 0;
            } else if (var5 != '.' && var5 != '?' && var5 != '!') {
                if (java.lang.Character.isSpaceChar(var5)) {
                    if (var3 != 2) {
                        var3 = 1;
                    }
                } else {
                    var3 = 1;
                }
            } else {
                var3 = 2;
            }

            var2[var4] = var5;
        }

        return new String(var2);
    }
}
