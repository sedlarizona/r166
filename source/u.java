import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("u")
public class u {
   @ObfuscatedName("do")
   static String do;
   @ObfuscatedName("t")
   public final int t;
   @ObfuscatedName("q")
   public final long q;
   @ObfuscatedName("i")
   public final GrandExchangeOffer i;
   @ObfuscatedName("a")
   String a;
   @ObfuscatedName("l")
   String l;

   u(ByteBuffer var1, byte var2, int var3) {
      this.a = var1.ar();
      this.l = var1.ar();
      this.t = var1.ae();
      this.q = var1.ah();
      int var4 = var1.ap();
      int var5 = var1.ap();
      this.i = new GrandExchangeOffer();
      this.i.l(2);
      this.i.b(var2);
      this.i.itemPrice = var4;
      this.i.totalOfferQuantity = var5;
      this.i.itemsExchanged = 0;
      this.i.coinsExchanged = 0;
      this.i.itemId = var3;
   }

   @ObfuscatedName("t")
   public String t() {
      return this.a;
   }

   @ObfuscatedName("q")
   public String q() {
      return this.l;
   }

   @ObfuscatedName("t")
   public static long t(CharSequence var0) {
      long var1 = 0L;
      int var3 = var0.length();

      for(int var4 = 0; var4 < var3; ++var4) {
         var1 *= 37L;
         char var5 = var0.charAt(var4);
         if (var5 >= 'A' && var5 <= 'Z') {
            var1 += (long)(var5 + 1 - 65);
         } else if (var5 >= 'a' && var5 <= 'z') {
            var1 += (long)(var5 + 1 - 97);
         } else if (var5 >= '0' && var5 <= '9') {
            var1 += (long)(var5 + 27 - 48);
         }

         if (var1 >= 177917621779460413L) {
            break;
         }
      }

      while(0L == var1 % 37L && var1 != 0L) {
         var1 /= 37L;
      }

      return var1;
   }

   @ObfuscatedName("p")
   static int p(int var0, RuneScript var1, boolean var2) {
      RTComponent var3 = var2 ? ho.v : cs.c;
      if (var0 == 1800) {
         int[] var4 = cs.e;
         int var5 = ++b.menuX - 1;
         int var7 = jr(var3);
         int var6 = var7 >> 11 & 63;
         var4[var5] = var6;
         return 1;
      } else if (var0 != 1801) {
         if (var0 == 1802) {
            if (var3.name == null) {
               cs.p[++ly.g - 1] = "";
            } else {
               cs.p[++ly.g - 1] = var3.name;
            }

            return 1;
         } else {
            return 2;
         }
      } else {
         int var8 = cs.e[--b.menuX];
         --var8;
         if (var3.actions != null && var8 < var3.actions.length && var3.actions[var8] != null) {
            cs.p[++ly.g - 1] = var3.actions[var8];
         } else {
            cs.p[++ly.g - 1] = "";
         }

         return 1;
      }
   }

   @ObfuscatedName("jr")
   static int jr(RTComponent var0) {
      hc var1 = (hc)Client.mf.t((long)var0.w + ((long)var0.id << 32));
      return var1 != null ? var1.t : var0.cq;
   }
}
