import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ks")
public class ks extends kp {

    @ObfuscatedName("t")
    boolean t;

    @ObfuscatedName("q")
    boolean q;

    @ObfuscatedName("t")
    int t(ks var1) {
        if (super.l == Client.currentWorld && Client.currentWorld != var1.l) {
            return -1;
        } else if (Client.currentWorld == var1.l && super.l != Client.currentWorld) {
            return 1;
        } else if (super.l != 0 && var1.l == 0) {
            return -1;
        } else if (var1.l != 0 && super.l == 0) {
            return 1;
        } else if (this.t && !var1.t) {
            return -1;
        } else if (!this.t && var1.t) {
            return 1;
        } else if (this.q && !var1.q) {
            return -1;
        } else if (!this.q && var1.q) {
            return 1;
        } else {
            return super.l != 0 ? super.b - var1.b : var1.b - super.b;
        }
    }

    @ObfuscatedName("aj")
    public int aj(kv var1) {
        return this.t((ks) var1);
    }

    public int compareTo(Object var1) {
        return this.t((ks) var1);
    }
}
