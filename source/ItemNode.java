import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ce")
public final class ItemNode extends Renderable {

    @ObfuscatedName("b")
    static Sprite b;

    @ObfuscatedName("t")
    int id;

    @ObfuscatedName("q")
    int stackSize;

    @ObfuscatedName("p")
    protected final Model p() {
        return cs.loadItemDefinition(this.id).p(this.stackSize);
    }

    @ObfuscatedName("i")
    static final boolean i(int var0, int var1, fv var2, CollisionMap var3) {
        int var4 = var0;
        int var5 = var1;
        byte var6 = 64;
        byte var7 = 64;
        int var8 = var0 - var6;
        int var9 = var1 - var7;
        fc.i[var6][var7] = 99;
        fc.a[var6][var7] = 0;
        byte var10 = 0;
        int var11 = 0;
        fc.x[var10] = var0;
        byte var10001 = var10;
        int var18 = var10 + 1;
        fc.p[var10001] = var1;
        int[][] var12 = var3.flags;

        while (var18 != var11) {
            var4 = fc.x[var11];
            var5 = fc.p[var11];
            var11 = var11 + 1 & 4095;
            int var16 = var4 - var8;
            int var17 = var5 - var9;
            int var13 = var4 - var3.widthOffset;
            int var14 = var5 - var3.heightOffset;
            if (var2.t(2, var4, var5, var3)) {
                z.l = var4;
                j.b = var5;
                return true;
            }

            int var15 = fc.a[var16][var17] + 1;
            if (var16 > 0 && fc.i[var16 - 1][var17] == 0 && (var12[var13 - 1][var14] & 19136782) == 0
                    && (var12[var13 - 1][var14 + 1] & 19136824) == 0) {
                fc.x[var18] = var4 - 1;
                fc.p[var18] = var5;
                var18 = var18 + 1 & 4095;
                fc.i[var16 - 1][var17] = 2;
                fc.a[var16 - 1][var17] = var15;
            }

            if (var16 < 126 && fc.i[var16 + 1][var17] == 0 && (var12[var13 + 2][var14] & 19136899) == 0
                    && (var12[var13 + 2][var14 + 1] & 19136992) == 0) {
                fc.x[var18] = var4 + 1;
                fc.p[var18] = var5;
                var18 = var18 + 1 & 4095;
                fc.i[var16 + 1][var17] = 8;
                fc.a[var16 + 1][var17] = var15;
            }

            if (var17 > 0 && fc.i[var16][var17 - 1] == 0 && (var12[var13][var14 - 1] & 19136782) == 0
                    && (var12[var13 + 1][var14 - 1] & 19136899) == 0) {
                fc.x[var18] = var4;
                fc.p[var18] = var5 - 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16][var17 - 1] = 1;
                fc.a[var16][var17 - 1] = var15;
            }

            if (var17 < 126 && fc.i[var16][var17 + 1] == 0 && (var12[var13][var14 + 2] & 19136824) == 0
                    && (var12[var13 + 1][var14 + 2] & 19136992) == 0) {
                fc.x[var18] = var4;
                fc.p[var18] = var5 + 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16][var17 + 1] = 4;
                fc.a[var16][var17 + 1] = var15;
            }

            if (var16 > 0 && var17 > 0 && fc.i[var16 - 1][var17 - 1] == 0 && (var12[var13 - 1][var14] & 19136830) == 0
                    && (var12[var13 - 1][var14 - 1] & 19136782) == 0 && (var12[var13][var14 - 1] & 19136911) == 0) {
                fc.x[var18] = var4 - 1;
                fc.p[var18] = var5 - 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 - 1][var17 - 1] = 3;
                fc.a[var16 - 1][var17 - 1] = var15;
            }

            if (var16 < 126 && var17 > 0 && fc.i[var16 + 1][var17 - 1] == 0
                    && (var12[var13 + 1][var14 - 1] & 19136911) == 0 && (var12[var13 + 2][var14 - 1] & 19136899) == 0
                    && (var12[var13 + 2][var14] & 19136995) == 0) {
                fc.x[var18] = var4 + 1;
                fc.p[var18] = var5 - 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 + 1][var17 - 1] = 9;
                fc.a[var16 + 1][var17 - 1] = var15;
            }

            if (var16 > 0 && var17 < 126 && fc.i[var16 - 1][var17 + 1] == 0
                    && (var12[var13 - 1][var14 + 1] & 19136830) == 0 && (var12[var13 - 1][var14 + 2] & 19136824) == 0
                    && (var12[var13][var14 + 2] & 19137016) == 0) {
                fc.x[var18] = var4 - 1;
                fc.p[var18] = var5 + 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 - 1][var17 + 1] = 6;
                fc.a[var16 - 1][var17 + 1] = var15;
            }

            if (var16 < 126 && var17 < 126 && fc.i[var16 + 1][var17 + 1] == 0
                    && (var12[var13 + 1][var14 + 2] & 19137016) == 0 && (var12[var13 + 2][var14 + 2] & 19136992) == 0
                    && (var12[var13 + 2][var14 + 1] & 19136995) == 0) {
                fc.x[var18] = var4 + 1;
                fc.p[var18] = var5 + 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 + 1][var17 + 1] = 12;
                fc.a[var16 + 1][var17 + 1] = var15;
            }
        }

        z.l = var4;
        j.b = var5;
        return false;
    }

    @ObfuscatedName("c")
    static Sprite c() {
        Sprite var0 = new Sprite();
        var0.b = le.q;
        var0.e = le.i;
        var0.a = ci.a[0];
        var0.l = er.l[0];
        var0.width = le.b[0];
        var0.height = GrandExchangeOffer.e[0];
        int var1 = var0.height * var0.width;
        byte[] var2 = ClassStructureNode.p[0];
        var0.pixels = new int[var1];

        for (int var3 = 0; var3 < var1; ++var3) {
            var0.pixels[var3] = le.x[var2[var3] & 255];
        }

        ly.z();
        return var0;
    }
}
