import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ji")
public enum ji implements gl {
    @ObfuscatedName("t")
    t(1, 0), @ObfuscatedName("q")
    q(0, 1), @ObfuscatedName("i")
    i(2, 2);

    @ObfuscatedName("a")
    public final int a;

    @ObfuscatedName("l")
    final int l;

    ji(int var3, int var4) {
        this.a = var3;
        this.l = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.l;
    }

    @ObfuscatedName("t")
    public static Varpbit t(int var0) {
        Varpbit var1 = (Varpbit) Varpbit.q.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = Varpbit.t.i(14, var0);
            var1 = new Varpbit();
            if (var2 != null) {
                var1.q(new ByteBuffer(var2));
            }

            Varpbit.q.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("o")
    public static boolean o(char var0) {
        return var0 >= '0' && var0 <= '9' || var0 >= 'A' && var0 <= 'Z' || var0 >= 'a' && var0 <= 'z';
    }
}
