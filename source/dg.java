import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dg")
public class dg implements ev {

    @ObfuscatedName("jv")
    static int jv;

    @ObfuscatedName("t")
    ds[] t;

    @ObfuscatedName("q")
    Deque q = new Deque();

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a = 0;

    @ObfuscatedName("l")
    double l = 1.0D;

    @ObfuscatedName("b")
    int b = 128;

    @ObfuscatedName("e")
    FileSystem e;

    public dg(FileSystem var1, FileSystem var2, int var3, double var4, int var6) {
        this.e = var2;
        this.i = var3;
        this.a = this.i;
        this.l = var4;
        this.b = var6;
        int[] var7 = var1.z(0);
        int var8 = var7.length;
        this.t = new ds[var1.w(0)];

        for (int var9 = 0; var9 < var8; ++var9) {
            ByteBuffer var10 = new ByteBuffer(var1.i(0, var7[var9]));
            this.t[var7[var9]] = new ds(var10);
        }

    }

    @ObfuscatedName("t")
    public int t() {
        int var1 = 0;
        int var2 = 0;
        ds[] var3 = this.t;

        for (int var4 = 0; var4 < var3.length; ++var4) {
            ds var5 = var3[var4];
            if (var5 != null && var5.e != null) {
                var1 += var5.e.length;
                int[] var6 = var5.e;

                for (int var7 = 0; var7 < var6.length; ++var7) {
                    int var8 = var6[var7];
                    if (this.e.b(var8)) {
                        ++var2;
                    }
                }
            }
        }

        if (var1 == 0) {
            return 0;
        } else {
            return var2 * 100 / var1;
        }
    }

    @ObfuscatedName("q")
    public void q(double var1) {
        this.l = var1;
        this.e();
    }

    @ObfuscatedName("i")
    public int[] i(int var1) {
        ds var2 = this.t[var1];
        if (var2 != null) {
            if (var2.c != null) {
                this.q.i(var2);
                var2.v = true;
                return var2.c;
            }

            boolean var3 = var2.t(this.l, this.b, this.e);
            if (var3) {
                if (this.a == 0) {
                    ds var4 = (ds) this.q.b();
                    var4.q();
                } else {
                    --this.a;
                }

                this.q.i(var2);
                var2.v = true;
                return var2.c;
            }
        }

        return null;
    }

    @ObfuscatedName("a")
    public int a(int var1) {
        return this.t[var1] != null ? this.t[var1].l : 0;
    }

    @ObfuscatedName("l")
    public boolean l(int var1) {
        return this.t[var1].b;
    }

    @ObfuscatedName("b")
    public boolean b(int var1) {
        return this.b == 64;
    }

    @ObfuscatedName("e")
    public void e() {
        for (int var1 = 0; var1 < this.t.length; ++var1) {
            if (this.t[var1] != null) {
                this.t[var1].q();
            }
        }

        this.q = new Deque();
        this.a = this.i;
    }

    @ObfuscatedName("x")
    public void x(int var1) {
        for (int var2 = 0; var2 < this.t.length; ++var2) {
            ds var3 = this.t[var2];
            if (var3 != null && var3.n != 0 && var3.v) {
                var3.i(var1);
                var3.v = false;
            }
        }

    }
}
