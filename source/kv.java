import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kv")
public class kv implements Comparable {

    @ObfuscatedName("p")
    public static FileSystem p;

    @ObfuscatedName("i")
    kb i;

    @ObfuscatedName("a")
    kb a;

    @ObfuscatedName("r")
    public kb r() {
        return this.i;
    }

    @ObfuscatedName("y")
    public String y() {
        return this.i == null ? "" : this.i.t();
    }

    @ObfuscatedName("h")
    public String h() {
        return this.a == null ? "" : this.a.t();
    }

    @ObfuscatedName("av")
    void av(kb var1, kb var2) {
        if (var1 == null) {
            throw new NullPointerException();
        } else {
            this.i = var1;
            this.a = var2;
        }
    }

    @ObfuscatedName("aj")
    public int aj(kv var1) {
        return this.i.i(var1.i);
    }

    public int compareTo(Object var1) {
        return this.aj((kv) var1);
    }

    @ObfuscatedName("c")
    public static void c() {
        jh.a.a();
        jh.l.a();
        jh.b.a();
    }
}
