import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("en")
public abstract class Renderable extends hh {

    @ObfuscatedName("cc")
    public int modelHeight = 1000;

    @ObfuscatedName("p")
    protected Model p() {
        return null;
    }

    @ObfuscatedName("cd")
    void cd(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        Model var10 = this.p();
        if (var10 != null) {
            this.modelHeight = var10.modelHeight;
            var10.cd(var1, var2, var3, var4, var5, var6, var7, var8, var9);
        }

    }

    @ObfuscatedName("a")
    public static lk a(FileSystem var0, String var1, String var2) {
        int var3 = var0.h(var1);
        int var4 = var0.av(var3, var2);
        byte[] var7 = var0.i(var3, var4);
        boolean var6;
        if (var7 == null) {
            var6 = false;
        } else {
            RTComponent.k(var7);
            var6 = true;
        }

        lk var5;
        if (!var6) {
            var5 = null;
        } else {
            var5 = kt.p();
        }

        return var5;
    }

    @ObfuscatedName("b")
    public static boolean b(int var0) {
        return (var0 >> 31 & 1) != 0;
    }
}
