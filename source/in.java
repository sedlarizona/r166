import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("in")
public enum in implements gl {
    @ObfuscatedName("t")
    t("runescape", "RuneScape", 0), @ObfuscatedName("q")
    q("stellardawn", "Stellar Dawn", 1), @ObfuscatedName("i")
    i("game3", "Game 3", 2), @ObfuscatedName("a")
    a("game4", "Game 4", 3), @ObfuscatedName("l")
    l("game5", "Game 5", 4), @ObfuscatedName("b")
    b("oldscape", "RuneScape 2007", 5);

    @ObfuscatedName("e")
    public final String e;

    @ObfuscatedName("x")
    final int x;

    in(String var3, String var4, int var5) {
        this.e = var3;
        this.x = var5;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.x;
    }

    @ObfuscatedName("t")
    public static void t(boolean var0) {
        if (bg.t != null) {
            try {
                ByteBuffer var1 = new ByteBuffer(4);
                var1.a(var0 ? 2 : 3);
                var1.b(0);
                bg.t.l(var1.buffer, 0, 4);
            } catch (IOException var4) {
                try {
                    bg.t.b();
                } catch (Exception var3) {
                    ;
                }

                ++jg.y;
                bg.t = null;
            }

        }
    }
}
