import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dm")
public class IdentityKit {

    @ObfuscatedName("t")
    static int[] t = new int[500];

    @ObfuscatedName("q")
    static int[] q = new int[500];

    @ObfuscatedName("i")
    static int[] i = new int[500];

    @ObfuscatedName("a")
    static int[] a = new int[500];

    @ObfuscatedName("l")
    Skins skins = null;

    @ObfuscatedName("b")
    int b = -1;

    @ObfuscatedName("e")
    int[] e;

    @ObfuscatedName("x")
    int[] x;

    @ObfuscatedName("p")
    int[] p;

    @ObfuscatedName("g")
    int[] g;

    @ObfuscatedName("n")
    boolean female = false;

    IdentityKit(byte[] var1, Skins var2) {
        this.skins = var2;
        ByteBuffer var3 = new ByteBuffer(var1);
        ByteBuffer var4 = new ByteBuffer(var1);
        var3.index = 2;
        int var5 = var3.av();
        int var6 = -1;
        int var7 = 0;
        var4.index = var5 + var3.index;

        int var8;
        for (var8 = 0; var8 < var5; ++var8) {
            int var9 = var3.av();
            if (var9 > 0) {
                if (this.skins.opcodes[var8] != 0) {
                    for (int var10 = var8 - 1; var10 > var6; --var10) {
                        if (this.skins.opcodes[var10] == 0) {
                            t[var7] = var10;
                            q[var7] = 0;
                            i[var7] = 0;
                            a[var7] = 0;
                            ++var7;
                            break;
                        }
                    }
                }

                t[var7] = var8;
                short var11 = 0;
                if (this.skins.opcodes[var8] == 3) {
                    var11 = 128;
                }

                if ((var9 & 1) != 0) {
                    q[var7] = var4.at();
                } else {
                    q[var7] = var11;
                }

                if ((var9 & 2) != 0) {
                    i[var7] = var4.at();
                } else {
                    i[var7] = var11;
                }

                if ((var9 & 4) != 0) {
                    a[var7] = var4.at();
                } else {
                    a[var7] = var11;
                }

                var6 = var8;
                ++var7;
                if (this.skins.opcodes[var8] == 5) {
                    this.female = true;
                }
            }
        }

        if (var1.length != var4.index) {
            throw new RuntimeException();
        } else {
            this.b = var7;
            this.e = new int[var7];
            this.x = new int[var7];
            this.p = new int[var7];
            this.g = new int[var7];

            for (var8 = 0; var8 < var7; ++var8) {
                this.e[var8] = t[var8];
                this.x[var8] = q[var8];
                this.p[var8] = i[var8];
                this.g[var8] = a[var8];
            }

        }
    }
}
