import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gh")
public class gh {

    @ObfuscatedName("p")
    public static int p;

    @ObfuscatedName("f")
    static int f(int var0, RuneScript var1, boolean var2) {
        int var3;
        if (var0 == 3903) {
            var3 = cs.e[--b.menuX];
            cs.e[++b.menuX - 1] = Client.exchangeOffers[var3].a();
            return 1;
        } else if (var0 == 3904) {
            var3 = cs.e[--b.menuX];
            cs.e[++b.menuX - 1] = Client.exchangeOffers[var3].itemId;
            return 1;
        } else if (var0 == 3905) {
            var3 = cs.e[--b.menuX];
            cs.e[++b.menuX - 1] = Client.exchangeOffers[var3].itemPrice;
            return 1;
        } else if (var0 == 3906) {
            var3 = cs.e[--b.menuX];
            cs.e[++b.menuX - 1] = Client.exchangeOffers[var3].totalOfferQuantity;
            return 1;
        } else if (var0 == 3907) {
            var3 = cs.e[--b.menuX];
            cs.e[++b.menuX - 1] = Client.exchangeOffers[var3].itemsExchanged;
            return 1;
        } else if (var0 == 3908) {
            var3 = cs.e[--b.menuX];
            cs.e[++b.menuX - 1] = Client.exchangeOffers[var3].coinsExchanged;
            return 1;
        } else {
            int var12;
            if (var0 == 3910) {
                var3 = cs.e[--b.menuX];
                var12 = Client.exchangeOffers[var3].i();
                cs.e[++b.menuX - 1] = var12 == 0 ? 1 : 0;
                return 1;
            } else if (var0 == 3911) {
                var3 = cs.e[--b.menuX];
                var12 = Client.exchangeOffers[var3].i();
                cs.e[++b.menuX - 1] = var12 == 2 ? 1 : 0;
                return 1;
            } else if (var0 == 3912) {
                var3 = cs.e[--b.menuX];
                var12 = Client.exchangeOffers[var3].i();
                cs.e[++b.menuX - 1] = var12 == 5 ? 1 : 0;
                return 1;
            } else if (var0 == 3913) {
                var3 = cs.e[--b.menuX];
                var12 = Client.exchangeOffers[var3].i();
                cs.e[++b.menuX - 1] = var12 == 1 ? 1 : 0;
                return 1;
            } else {
                boolean var13;
                if (var0 == 3914) {
                    var13 = cs.e[--b.menuX] == 1;
                    if (es.qe != null) {
                        es.qe.t(v.a, var13);
                    }

                    return 1;
                } else if (var0 == 3915) {
                    var13 = cs.e[--b.menuX] == 1;
                    if (es.qe != null) {
                        es.qe.t(v.i, var13);
                    }

                    return 1;
                } else if (var0 == 3916) {
                    b.menuX -= 2;
                    var13 = cs.e[b.menuX] == 1;
                    boolean var4 = cs.e[b.menuX + 1] == 1;
                    if (es.qe != null) {
                        Client.qz.t = var4;
                        es.qe.t(Client.qz, var13);
                    }

                    return 1;
                } else if (var0 == 3917) {
                    var13 = cs.e[--b.menuX] == 1;
                    if (es.qe != null) {
                        es.qe.t(v.q, var13);
                    }

                    return 1;
                } else if (var0 == 3918) {
                    var13 = cs.e[--b.menuX] == 1;
                    if (es.qe != null) {
                        es.qe.t(v.l, var13);
                    }

                    return 1;
                } else if (var0 == 3919) {
                    cs.e[++b.menuX - 1] = es.qe == null ? 0 : es.qe.t.size();
                    return 1;
                } else {
                    u var11;
                    if (var0 == 3920) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        cs.e[++b.menuX - 1] = var11.t;
                        return 1;
                    } else if (var0 == 3921) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        cs.p[++ly.g - 1] = var11.t();
                        return 1;
                    } else if (var0 == 3922) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        cs.p[++ly.g - 1] = var11.q();
                        return 1;
                    } else if (var0 == 3923) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        long var5 = au.t() - gx.qy - var11.q;
                        int var7 = (int) (var5 / 3600000L);
                        int var8 = (int) ((var5 - (long) (var7 * 3600000)) / 60000L);
                        int var9 = (int) ((var5 - (long) (var7 * 3600000) - (long) (var8 * '\uea60')) / 1000L);
                        String var10 = var7 + ":" + var8 / 10 + var8 % 10 + ":" + var9 / 10 + var9 % 10;
                        cs.p[++ly.g - 1] = var10;
                        return 1;
                    } else if (var0 == 3924) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        cs.e[++b.menuX - 1] = var11.i.totalOfferQuantity;
                        return 1;
                    } else if (var0 == 3925) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        cs.e[++b.menuX - 1] = var11.i.itemPrice;
                        return 1;
                    } else if (var0 == 3926) {
                        var3 = cs.e[--b.menuX];
                        var11 = (u) es.qe.t.get(var3);
                        cs.e[++b.menuX - 1] = var11.i.itemId;
                        return 1;
                    } else {
                        return 2;
                    }
                }
            }
        }
    }

    @ObfuscatedName("im")
   static final void im(RTComponent[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      for(int var8 = 0; var8 < var0.length; ++var8) {
         RTComponent var9 = var0[var8];
         if (var9 != null && (!var9.modern || var9.type == 0 || var9.ca || u.jr(var9) != 0 || var9 == Client.lm || var9.contentType == 1338) && var9.parentId == var1) {
            if (var9.modern) {
               boolean var10 = var9.hidden;
               if (var10) {
                  continue;
               }
            }

            int var43 = var9.relativeX + var6;
            int var11 = var7 + var9.relativeY;
            int var12;
            int var13;
            int var14;
            int var15;
            int var16;
            int var17;
            int var19;
            if (var9.type == 2) {
               var12 = var2;
               var13 = var3;
               var14 = var4;
               var15 = var5;
            } else if (var9.type == 9) {
               var16 = var43;
               var17 = var11;
               int var18 = var43 + var9.width;
               var19 = var11 + var9.height;
               if (var18 < var43) {
                  var16 = var18;
                  var18 = var43;
               }

               if (var19 < var11) {
                  var17 = var19;
                  var19 = var11;
               }

               ++var18;
               ++var19;
               var12 = var16 > var2 ? var16 : var2;
               var13 = var17 > var3 ? var17 : var3;
               var14 = var18 < var4 ? var18 : var4;
               var15 = var19 < var5 ? var19 : var5;
            } else {
               var16 = var43 + var9.width;
               var17 = var11 + var9.height;
               var12 = var43 > var2 ? var43 : var2;
               var13 = var11 > var3 ? var11 : var3;
               var14 = var16 < var4 ? var16 : var4;
               var15 = var17 < var5 ? var17 : var5;
            }

            if (var9 == Client.ln) {
               Client.lg = true;
               Client.lo = var43;
               Client.lu = var11;
            }

            if (!var9.modern || var12 < var14 && var13 < var15) {
               var16 = bs.n;
               var17 = bs.x;
               if (bs.j != 0) {
                  var16 = bs.cameraZ;
                  var17 = bs.z;
               }

               boolean var44 = var16 >= var12 && var17 >= var13 && var16 < var14 && var17 < var15;
               int var20;
               int var22;
               int var23;
               int var24;
               int var25;
               int var26;
               int var47;
               if (var9.contentType == 1337) {
                  if (!Client.bq && !Client.menuOpen && var44) {
                     if (Client.inventoryItemSelectionState == 0 && !Client.interfaceSelected) {
                        fy.hi("Walk here", "", 23, 0, var16 - var12, var17 - var13);
                     }

                     var19 = -1;
                     var20 = -1;

                     for(var47 = 0; var47 < ej.v; ++var47) {
                        var22 = ej.u[var47];
                        var23 = var22 & 127;
                        var24 = var22 >> 7 & 127;
                        var25 = var22 >> 29 & 3;
                        var26 = var22 >> 14 & 32767;
                        if (var20 != var22) {
                           var20 = var22;
                           if (var25 == 2 && an.loadedRegion.ar(kt.ii, var23, var24, var22) >= 0) {
                              ObjectDefinition var27 = jw.q(var26);
                              if (var27.innerObjectIds != null) {
                                 var27 = var27.u();
                              }

                              if (var27 == null) {
                                 continue;
                              }

                              if (Client.inventoryItemSelectionState == 1) {
                                 fy.hi("Use", Client.lastSelectedInventoryItemName + " " + "->" + " " + ar.q(65535) + var27.name, 1, var22, var23, var24);
                              } else if (Client.interfaceSelected) {
                                 if ((eq.km & 4) == 4) {
                                    fy.hi(Client.lj, Client.selectedSpellName + " " + "->" + " " + ar.q(65535) + var27.name, 2, var22, var23, var24);
                                 }
                              } else {
                                 String[] var28 = var27.actions;
                                 if (var28 != null) {
                                    for(int var29 = 4; var29 >= 0; --var29) {
                                       if (var28[var29] != null) {
                                          short var30 = 0;
                                          if (var29 == 0) {
                                             var30 = 3;
                                          }

                                          if (var29 == 1) {
                                             var30 = 4;
                                          }

                                          if (var29 == 2) {
                                             var30 = 5;
                                          }

                                          if (var29 == 3) {
                                             var30 = 6;
                                          }

                                          if (var29 == 4) {
                                             var30 = 1001;
                                          }

                                          fy.hi(var28[var29], ar.q(65535) + var27.name, var30, var22, var23, var24);
                                       }
                                    }
                                 }

                                 fy.hi("Examine", ar.q(65535) + var27.name, 1002, var27.id << 14, var23, var24);
                              }
                           }

                           Player var31;
                           int var38;
                           Npc var39;
                           int var52;
                           int[] var54;
                           if (var25 == 1) {
                              Npc var48 = Client.loadedNpcs[var26];
                              if (var48 == null) {
                                 continue;
                              }

                              if (var48.composite.e == 1 && (var48.regionX & 127) == 64 && (var48.regionY & 127) == 64) {
                                 for(var38 = 0; var38 < Client.dz; ++var38) {
                                    var39 = Client.loadedNpcs[Client.npcIndices[var38]];
                                    if (var39 != null && var48 != var39 && var39.composite.e == 1 && var39.regionX == var48.regionX && var39.regionY == var48.regionY) {
                                       ep.hy(var39.composite, Client.npcIndices[var38], var23, var24);
                                    }
                                 }

                                 var38 = cx.b;
                                 var54 = cx.e;

                                 for(var52 = 0; var52 < var38; ++var52) {
                                    var31 = Client.loadedPlayers[var54[var52]];
                                    if (var31 != null && var31.regionX == var48.regionX && var31.regionY == var48.regionY) {
                                       g.ho(var31, var54[var52], var23, var24);
                                    }
                                 }
                              }

                              ep.hy(var48.composite, var26, var23, var24);
                           }

                           if (var25 == 0) {
                              Player var49 = Client.loadedPlayers[var26];
                              if (var49 == null) {
                                 continue;
                              }

                              if ((var49.regionX & 127) == 64 && (var49.regionY & 127) == 64) {
                                 for(var38 = 0; var38 < Client.dz; ++var38) {
                                    var39 = Client.loadedNpcs[Client.npcIndices[var38]];
                                    if (var39 != null && var39.composite.e == 1 && var39.regionX == var49.regionX && var49.regionY == var39.regionY) {
                                       ep.hy(var39.composite, Client.npcIndices[var38], var23, var24);
                                    }
                                 }

                                 var38 = cx.b;
                                 var54 = cx.e;

                                 for(var52 = 0; var52 < var38; ++var52) {
                                    var31 = Client.loadedPlayers[var54[var52]];
                                    if (var31 != null && var31 != var49 && var49.regionX == var31.regionX && var31.regionY == var49.regionY) {
                                       g.ho(var31, var54[var52], var23, var24);
                                    }
                                 }
                              }

                              if (var26 != Client.jq) {
                                 g.ho(var49, var26, var23, var24);
                              } else {
                                 var19 = var22;
                              }
                           }

                           if (var25 == 3) {
                              Deque var51 = Client.loadedGroundItems[kt.ii][var23][var24];
                              if (var51 != null) {
                                 for(ItemNode var50 = (ItemNode)var51.x(); var50 != null; var50 = (ItemNode)var51.o()) {
                                    ItemDefinition var55 = cs.loadItemDefinition(var50.id);
                                    if (Client.inventoryItemSelectionState == 1) {
                                       fy.hi("Use", Client.lastSelectedInventoryItemName + " " + "->" + " " + ar.q(16748608) + var55.name, 16, var50.id, var23, var24);
                                    } else if (Client.interfaceSelected) {
                                       if ((eq.km & 1) == 1) {
                                          fy.hi(Client.lj, Client.selectedSpellName + " " + "->" + " " + ar.q(16748608) + var55.name, 17, var50.id, var23, var24);
                                       }
                                    } else {
                                       String[] var40 = var55.groundActions;

                                       for(int var41 = 4; var41 >= 0; --var41) {
                                          if (var40 != null && var40[var41] != null) {
                                             byte var32 = 0;
                                             if (var41 == 0) {
                                                var32 = 18;
                                             }

                                             if (var41 == 1) {
                                                var32 = 19;
                                             }

                                             if (var41 == 2) {
                                                var32 = 20;
                                             }

                                             if (var41 == 3) {
                                                var32 = 21;
                                             }

                                             if (var41 == 4) {
                                                var32 = 22;
                                             }

                                             fy.hi(var40[var41], ar.q(16748608) + var55.name, var32, var50.id, var23, var24);
                                          } else if (var41 == 2) {
                                             fy.hi("Take", ar.q(16748608) + var55.name, 20, var50.id, var23, var24);
                                          }
                                       }

                                       fy.hi("Examine", ar.q(16748608) + var55.name, 1004, var50.id, var23, var24);
                                    }
                                 }
                              }
                           }
                        }
                     }

                     if (var19 != -1) {
                        var47 = var19 & 127;
                        var22 = var19 >> 7 & 127;
                        Player var42 = Client.loadedPlayers[Client.jq];
                        g.ho(var42, Client.jq, var47, var22);
                     }
                  }
               } else if (var9.contentType == 1338) {
                  p.fm(var9, var43, var11);
               } else {
                  if (var9.contentType == 1400) {
                     ip.rz.a(bs.n, bs.x, var44, var43, var11, var9.width, var9.height);
                  }

                  if (!Client.menuOpen && var44) {
                     if (var9.contentType == 1400) {
                        ip.rz.bt(var43, var11, var9.width, var9.height, var16, var17);
                     } else {
                        cg.ip(var9, var16 - var43, var17 - var11);
                     }
                  }

                  boolean var45;
                  if (var9.type == 0) {
                     if (!var9.modern) {
                        var45 = var9.hidden;
                        if (var45 && var9 != av.kp) {
                           continue;
                        }
                     }

                     im(var0, var9.id, var12, var13, var14, var15, var43 - var9.horizontalScrollbarPosition, var11 - var9.verticalScrollbarPosition);
                     if (var9.cs2components != null) {
                        im(var9.cs2components, var9.id, var12, var13, var14, var15, var43 - var9.horizontalScrollbarPosition, var11 - var9.verticalScrollbarPosition);
                     }

                     SubWindow var33 = (SubWindow)Client.subWindowTable.t((long)var9.id);
                     if (var33 != null) {
                        if (var33.type == 0 && bs.n >= var12 && bs.x >= var13 && bs.n < var14 && bs.x < var15 && !Client.menuOpen) {
                           for(ScriptEvent var34 = (ScriptEvent)Client.me.e(); var34 != null; var34 = (ScriptEvent)Client.me.p()) {
                              if (var34.q) {
                                 var34.kc();
                                 var34.i.el = false;
                              }
                           }

                           if (ah.lp == 0) {
                              Client.ln = null;
                              Client.lm = null;
                           }

                           if (!Client.menuOpen) {
                              v.hf();
                           }
                        }

                        var20 = var33.targetWindowId;
                        if (RuneScript.i(var20)) {
                           im(RTComponent.b[var20], -1, var12, var13, var14, var15, var43, var11);
                        }
                     }
                  }

                  if (var9.modern) {
                     ScriptEvent var53;
                     if (!var9.er) {
                        if (var9.eh && bs.n >= var12 && bs.x >= var13 && bs.n < var14 && bs.x < var15) {
                           for(var53 = (ScriptEvent)Client.me.e(); var53 != null; var53 = (ScriptEvent)Client.me.p()) {
                              if (var53.q && var53.i.dk == var53.args) {
                                 var53.kc();
                              }
                           }
                        }
                     } else if (bs.n >= var12 && bs.x >= var13 && bs.n < var14 && bs.x < var15) {
                        for(var53 = (ScriptEvent)Client.me.e(); var53 != null; var53 = (ScriptEvent)Client.me.p()) {
                           if (var53.q) {
                              var53.kc();
                              var53.i.el = false;
                           }
                        }

                        if (ah.lp == 0) {
                           Client.ln = null;
                           Client.lm = null;
                        }

                        if (!Client.menuOpen) {
                           v.hf();
                        }
                     }

                     if (bs.n >= var12 && bs.x >= var13 && bs.n < var14 && bs.x < var15) {
                        var44 = true;
                     } else {
                        var44 = false;
                     }

                     var45 = false;
                     if ((bs.g == 1 || !er.ch && bs.g == 4) && var44) {
                        var45 = true;
                     }

                     boolean var46 = false;
                     if ((bs.j == 1 || !er.ch && bs.j == 4) && bs.cameraZ >= var12 && bs.z >= var13 && bs.cameraZ < var14 && bs.z < var15) {
                        var46 = true;
                     }

                     if (var46) {
                        au.iu(var9, bs.cameraZ - var43, bs.z - var11);
                     }

                     if (var9.contentType == 1400) {
                        ip.rz.l(var16, var17, var44 & var45, var44 & var46);
                     }

                     if (Client.ln != null && var9 != Client.ln && var44) {
                        var22 = u.jr(var9);
                        boolean var21 = (var22 >> 20 & 1) != 0;
                        if (var21) {
                           Client.ld = var9;
                        }
                     }

                     if (var9 == Client.lm) {
                        Client.lk = true;
                        Client.la = var43;
                        Client.le = var11;
                     }

                     if (var9.ca) {
                        ScriptEvent var35;
                        if (var44 && Client.mw != 0 && var9.dk != null) {
                           var35 = new ScriptEvent();
                           var35.q = true;
                           var35.i = var9;
                           var35.l = Client.mw;
                           var35.args = var9.dk;
                           Client.me.q(var35);
                        }

                        if (Client.ln != null || ak.ik != null || Client.menuOpen) {
                           var46 = false;
                           var45 = false;
                           var44 = false;
                        }

                        if (!var9.eo && var46) {
                           var9.eo = true;
                           if (var9.cp != null) {
                              var35 = new ScriptEvent();
                              var35.q = true;
                              var35.i = var9;
                              var35.a = bs.cameraZ - var43;
                              var35.l = bs.z - var11;
                              var35.args = var9.cp;
                              Client.me.q(var35);
                           }
                        }

                        if (var9.eo && var45 && var9.ck != null) {
                           var35 = new ScriptEvent();
                           var35.q = true;
                           var35.i = var9;
                           var35.a = bs.n - var43;
                           var35.l = bs.x - var11;
                           var35.args = var9.ck;
                           Client.me.q(var35);
                        }

                        if (var9.eo && !var45) {
                           var9.eo = false;
                           if (var9.db != null) {
                              var35 = new ScriptEvent();
                              var35.q = true;
                              var35.i = var9;
                              var35.a = bs.n - var43;
                              var35.l = bs.x - var11;
                              var35.args = var9.db;
                              Client.ml.q(var35);
                           }
                        }

                        if (var45 && var9.dp != null) {
                           var35 = new ScriptEvent();
                           var35.q = true;
                           var35.i = var9;
                           var35.a = bs.n - var43;
                           var35.l = bs.x - var11;
                           var35.args = var9.dp;
                           Client.me.q(var35);
                        }

                        if (!var9.el && var44) {
                           var9.el = true;
                           if (var9.da != null) {
                              var35 = new ScriptEvent();
                              var35.q = true;
                              var35.i = var9;
                              var35.a = bs.n - var43;
                              var35.l = bs.x - var11;
                              var35.args = var9.da;
                              Client.me.q(var35);
                           }
                        }

                        if (var9.el && var44 && var9.dr != null) {
                           var35 = new ScriptEvent();
                           var35.q = true;
                           var35.i = var9;
                           var35.a = bs.n - var43;
                           var35.l = bs.x - var11;
                           var35.args = var9.dr;
                           Client.me.q(var35);
                        }

                        if (var9.el && !var44) {
                           var9.el = false;
                           if (var9.dj != null) {
                              var35 = new ScriptEvent();
                              var35.q = true;
                              var35.i = var9;
                              var35.a = bs.n - var43;
                              var35.l = bs.x - var11;
                              var35.args = var9.dj;
                              Client.ml.q(var35);
                           }
                        }

                        if (var9.dd != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.dd;
                           Client.mh.q(var35);
                        }

                        ScriptEvent var36;
                        if (var9.dq != null && Client.ma > var9.ep) {
                           if (var9.dt != null && Client.ma - var9.ep <= 32) {
                              label1202:
                              for(var47 = var9.ep; var47 < Client.ma; ++var47) {
                                 var22 = Client.mk[var47 & 31];

                                 for(var23 = 0; var23 < var9.dt.length; ++var23) {
                                    if (var22 == var9.dt[var23]) {
                                       var36 = new ScriptEvent();
                                       var36.i = var9;
                                       var36.args = var9.dq;
                                       Client.me.q(var36);
                                       break label1202;
                                    }
                                 }
                              }
                           } else {
                              var35 = new ScriptEvent();
                              var35.i = var9;
                              var35.args = var9.dq;
                              Client.me.q(var35);
                           }

                           var9.ep = Client.ma;
                        }

                        if (var9.dy != null && Client.mz > var9.ef) {
                           if (var9.dn != null && Client.mz - var9.ef <= 32) {
                              label1178:
                              for(var47 = var9.ef; var47 < Client.mz; ++var47) {
                                 var22 = Client.mu[var47 & 31];

                                 for(var23 = 0; var23 < var9.dn.length; ++var23) {
                                    if (var22 == var9.dn[var23]) {
                                       var36 = new ScriptEvent();
                                       var36.i = var9;
                                       var36.args = var9.dy;
                                       Client.me.q(var36);
                                       break label1178;
                                    }
                                 }
                              }
                           } else {
                              var35 = new ScriptEvent();
                              var35.i = var9;
                              var35.args = var9.dy;
                              Client.me.q(var35);
                           }

                           var9.ef = Client.mz;
                        }

                        if (var9.do != null && Client.my > var9.eq) {
                           if (var9.dw != null && Client.my - var9.eq <= 32) {
                              label1154:
                              for(var47 = var9.eq; var47 < Client.my; ++var47) {
                                 var22 = Client.mg[var47 & 31];

                                 for(var23 = 0; var23 < var9.dw.length; ++var23) {
                                    if (var22 == var9.dw[var23]) {
                                       var36 = new ScriptEvent();
                                       var36.i = var9;
                                       var36.args = var9.do;
                                       Client.me.q(var36);
                                       break label1154;
                                    }
                                 }
                              }
                           } else {
                              var35 = new ScriptEvent();
                              var35.i = var9;
                              var35.args = var9.do;
                              Client.me.q(var35);
                           }

                           var9.eq = Client.my;
                        }

                        if (Client.mc > var9.ec && var9.de != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.de;
                           Client.me.q(var35);
                        }

                        if (Client.ms > var9.ec && var9.dx != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.dx;
                           Client.me.q(var35);
                        }

                        if (Client.mj > var9.ec && var9.di != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.di;
                           Client.me.q(var35);
                        }

                        if (Client.mo > var9.ec && var9.eb != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.eb;
                           Client.me.q(var35);
                        }

                        if (Client.mi > var9.ec && var9.ek != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.ek;
                           Client.me.q(var35);
                        }

                        if (Client.mv > var9.ec && var9.dv != null) {
                           var35 = new ScriptEvent();
                           var35.i = var9;
                           var35.args = var9.dv;
                           Client.me.q(var35);
                        }

                        var9.ec = Client.mn;
                        if (var9.dg != null) {
                           for(var47 = 0; var47 < Client.og; ++var47) {
                              ScriptEvent var37 = new ScriptEvent();
                              var37.i = var9;
                              var37.x = Client.ol[var47];
                              var37.p = Client.or[var47];
                              var37.args = var9.dg;
                              Client.me.q(var37);
                           }
                        }
                     }
                  }

                  if (!var9.modern && Client.ln == null && ak.ik == null && !Client.menuOpen) {
                     if ((var9.ei >= 0 || var9.aw != 0) && bs.n >= var12 && bs.x >= var13 && bs.n < var14 && bs.x < var15) {
                        if (var9.ei >= 0) {
                           av.kp = var0[var9.ei];
                        } else {
                           av.kp = var9;
                        }
                     }

                     if (var9.type == 8 && bs.n >= var12 && bs.x >= var13 && bs.n < var14 && bs.x < var15) {
                        jk.kz = var9;
                     }

                     if (var9.at > var9.height) {
                        var19 = var43 + var9.width;
                        var20 = var9.height;
                        var47 = var9.at;
                        var22 = bs.n;
                        var23 = bs.x;
                        if (Client.gd) {
                           Client.gg = 32;
                        } else {
                           Client.gg = 0;
                        }

                        Client.gd = false;
                        if (bs.g == 1 || !er.ch && bs.g == 4) {
                           if (var22 >= var19 && var22 < var19 + 16 && var23 >= var11 && var23 < var11 + 16) {
                              var9.verticalScrollbarPosition -= 4;
                              GameEngine.jk(var9);
                           } else if (var22 >= var19 && var22 < var19 + 16 && var23 >= var20 + var11 - 16 && var23 < var11 + var20) {
                              var9.verticalScrollbarPosition += 4;
                              GameEngine.jk(var9);
                           } else if (var22 >= var19 - Client.gg && var22 < var19 + Client.gg + 16 && var23 >= var11 + 16 && var23 < var11 + var20 - 16) {
                              var24 = var20 * (var20 - 32) / var47;
                              if (var24 < 8) {
                                 var24 = 8;
                              }

                              var25 = var23 - var11 - 16 - var24 / 2;
                              var26 = var20 - 32 - var24;
                              var9.verticalScrollbarPosition = var25 * (var47 - var20) / var26;
                              GameEngine.jk(var9);
                              Client.gd = true;
                           }
                        }

                        if (Client.mw != 0) {
                           var24 = var9.width;
                           if (var22 >= var19 - var24 && var23 >= var11 && var22 < var19 + 16 && var23 <= var20 + var11) {
                              var9.verticalScrollbarPosition += Client.mw * 45;
                              GameEngine.jk(var9);
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   }
}
