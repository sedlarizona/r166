import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bf")
public class MouseTracker implements Runnable {

    @ObfuscatedName("ie")
    static RTComponent ie;

    @ObfuscatedName("t")
    boolean alive = true;

    @ObfuscatedName("q")
    Object lock = new Object();

    @ObfuscatedName("i")
    int index = 0;

    @ObfuscatedName("a")
    int[] trackingX = new int[500];

    @ObfuscatedName("l")
    int[] trackingY = new int[500];

    public void run() {
        for (; this.alive; cx.t(50L)) {
            Object var1 = this.lock;
            synchronized (this.lock) {
                if (this.index < 500) {
                    this.trackingX[this.index] = bs.n;
                    this.trackingY[this.index] = bs.x;
                    ++this.index;
                }
            }
        }

    }

    @ObfuscatedName("o")
    static final int o(int var0, int var1) {
        int var2 = var1 * 57 + var0;
        var2 ^= var2 << 13;
        int var3 = var2 * (var2 * var2 * 15731 + 789221) + 1376312589 & Integer.MAX_VALUE;
        return var3 >> 19 & 255;
    }

    @ObfuscatedName("jq")
    static final void jq() {
        for (int var0 = 0; var0 < cx.b; ++var0) {
            Player var1 = Client.loadedPlayers[cx.e[var0]];
            var1.i();
        }

        ay.b();
        if (ad.ot != null) {
            ad.ot.cx();
        }

    }
}
