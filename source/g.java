import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("g")
public abstract class g extends Node {

    @ObfuscatedName("mt")
    static RuneScriptVM vm;

    @ObfuscatedName("g")
    static int[] g;

    @ObfuscatedName("t")
    public abstract void t();

    @ObfuscatedName("q")
    static final void q(int var0) {
        ej.u[++ej.v - 1] = var0;
    }

    @ObfuscatedName("b")
    static int b(Server var0, Server var1, int var2, boolean var3) {
        if (var2 == 1) {
            int var4 = var0.population;
            int var5 = var1.population;
            if (!var3) {
                if (var4 == -1) {
                    var4 = 2001;
                }

                if (var5 == -1) {
                    var5 = 2001;
                }
            }

            return var4 - var5;
        } else if (var2 == 2) {
            return var0.location - var1.location;
        } else if (var2 == 3) {
            if (var0.activity.equals("-")) {
                if (var1.activity.equals("-")) {
                    return 0;
                } else {
                    return var3 ? -1 : 1;
                }
            } else if (var1.activity.equals("-")) {
                return var3 ? 1 : -1;
            } else {
                return var0.activity.compareTo(var1.activity);
            }
        } else if (var2 == 4) {
            return var0.c() ? (var1.c() ? 0 : 1) : (var1.c() ? -1 : 0);
        } else if (var2 == 5) {
            return var0.p() ? (var1.p() ? 0 : 1) : (var1.p() ? -1 : 0);
        } else if (var2 == 6) {
            return var0.o() ? (var1.o() ? 0 : 1) : (var1.o() ? -1 : 0);
        } else if (var2 == 7) {
            return var0.x() ? (var1.x() ? 0 : 1) : (var1.x() ? -1 : 0);
        } else {
            return var0.number - var1.number;
        }
    }

    @ObfuscatedName("ho")
    static final void ho(Player var0, int var1, int var2, int var3) {
        if (az.il != var0) {
            if (Client.menuSize < 400) {
                String var4;
                if (var0.totalLevel == 0) {
                    var4 = var0.b[0] + var0.t + var0.b[1] + iw.ie(var0.combatLevel, az.il.combatLevel) + " " + " ("
                            + "level-" + var0.combatLevel + ")" + var0.b[2];
                } else {
                    var4 = var0.b[0] + var0.t + var0.b[1] + " " + " (" + "skill-" + var0.totalLevel + ")" + var0.b[2];
                }

                int var5;
                if (Client.inventoryItemSelectionState == 1) {
                    fy.hi("Use", Client.lastSelectedInventoryItemName + " " + "->" + " " + ar.q(16777215) + var4, 14,
                            var1, var2, var3);
                } else if (Client.interfaceSelected) {
                    if ((eq.km & 8) == 8) {
                        fy.hi(Client.lj, Client.selectedSpellName + " " + "->" + " " + ar.q(16777215) + var4, 15, var1,
                                var2, var3);
                    }
                } else {
                    for (var5 = 7; var5 >= 0; --var5) {
                        if (Client.playerActions[var5] != null) {
                            short var6 = 0;
                            if (Client.playerActions[var5].equalsIgnoreCase("Attack")) {
                                if (Client.cr == cq.a) {
                                    continue;
                                }

                                if (Client.cr == cq.q || Client.cr == cq.t && var0.combatLevel > az.il.combatLevel) {
                                    var6 = 2000;
                                }

                                if (az.il.team != 0 && var0.team != 0) {
                                    if (var0.team == az.il.team) {
                                        var6 = 2000;
                                    } else {
                                        var6 = 0;
                                    }
                                }
                            } else if (Client.jn[var5]) {
                                var6 = 2000;
                            }

                            boolean var7 = false;
                            int var8 = Client.ji[var5] + var6;
                            fy.hi(Client.playerActions[var5], ar.q(16777215) + var4, var8, var1, var2, var3);
                        }
                    }
                }

                for (var5 = 0; var5 < Client.menuSize; ++var5) {
                    if (Client.menuOpcodes[var5] == 23) {
                        Client.menuTargets[var5] = ar.q(16777215) + var4;
                        break;
                    }
                }

            }
        }
    }

    @ObfuscatedName("iz")
    static final void iz(int var0, int var1) {
        if (RuneScript.i(var0)) {
            fh.il(RTComponent.b[var0], var1);
        }
    }
}
