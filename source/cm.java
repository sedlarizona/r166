import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cm")
public class cm {

    @ObfuscatedName("a")
    final lu a;

    @ObfuscatedName("l")
    public final ke l;

    @ObfuscatedName("b")
    public final kj b;

    @ObfuscatedName("e")
    int e = 0;

    cm(lu var1) {
        this.a = var1;
        this.l = new ke(var1);
        this.b = new kj(var1);
    }

    @ObfuscatedName("t")
    boolean t() {
        return this.e == 2;
    }

    @ObfuscatedName("q")
    final void q() {
        this.e = 1;
    }

    @ObfuscatedName("i")
    final void i(ByteBuffer var1, int var2) {
        this.l.p(var1, var2);
        this.e = 2;
        MouseTracker.jq();
    }

    @ObfuscatedName("a")
    final void a() {
        for (kr var1 = (kr) this.l.n.q(); var1 != null; var1 = (kr) this.l.n.i()) {
            if ((long) var1.i < au.t() / 1000L - 5L) {
                if (var1.l > 0) {
                    j.t(5, "", var1.a + " has logged in.");
                }

                if (var1.l == 0) {
                    j.t(5, "", var1.a + " has logged out.");
                }

                var1.t();
            }
        }

    }

    @ObfuscatedName("l")
    final void l() {
        this.e = 0;
        this.l.c();
        this.b.c();
    }

    @ObfuscatedName("b")
    final boolean b(kb var1, boolean var2) {
        if (var1 == null) {
            return false;
        } else if (var1.equals(az.il.t)) {
            return true;
        } else {
            return this.l.i(var1, var2);
        }
    }

    @ObfuscatedName("e")
    final boolean e(kb var1) {
        if (var1 == null) {
            return false;
        } else {
            return this.b.z(var1);
        }
    }

    @ObfuscatedName("x")
    final void x(String var1) {
        if (var1 != null) {
            kb var2 = new kb(var1, this.a);
            if (var2.q()) {
                if (this.k()) {
                    aq.o();
                } else if (az.il.t.equals(var2)) {
                    RSRandomAccessFile.u();
                } else if (this.b(var2, false)) {
                    ak.p(var1 + " is already on your friend list");
                } else if (this.e(var2)) {
                    i.c(var1);
                } else {
                    gd var3 = ap.t(fo.ac, Client.ee.l);
                    var3.i.a(z.c(var1));
                    var3.i.u(var1);
                    Client.ee.i(var3);
                }
            }
        }
    }

    @ObfuscatedName("k")
    final boolean k() {
        return this.l.k() || this.l.u() >= 200 && Client.im != 1;
    }

    @ObfuscatedName("z")
    final void z(String var1) {
        if (var1 != null) {
            kb var2 = new kb(var1, this.a);
            if (var2.q()) {
                if (this.f()) {
                    p.w();
                } else if (az.il.t.equals(var2)) {
                    ak.p("You can't add yourself to your own ignore list");
                } else if (this.e(var2)) {
                    jj.s(var1);
                } else if (this.b(var2, false)) {
                    hi.d(var1);
                } else {
                    gd var3 = ap.t(fo.l, Client.ee.l);
                    var3.i.a(z.c(var1));
                    var3.i.u(var1);
                    Client.ee.i(var3);
                }
            }
        }
    }

    @ObfuscatedName("f")
    final boolean f() {
        return this.b.k() || this.b.u() >= 100 && Client.im != 1;
    }

    @ObfuscatedName("r")
    final void r(String var1) {
        if (var1 != null) {
            kb var2 = new kb(var1, this.a);
            if (var2.q()) {
                if (this.l.f(var2)) {
                    ak.jw();
                    gd var3 = ap.t(fo.ce, Client.ee.l);
                    var3.i.a(z.c(var1));
                    var3.i.u(var1);
                    Client.ee.i(var3);
                }

                MouseTracker.jq();
            }
        }
    }

    @ObfuscatedName("y")
    final void y(String var1) {
        if (var1 != null) {
            kb var2 = new kb(var1, this.a);
            if (var2.q()) {
                if (this.b.f(var2)) {
                    ak.jw();
                    gd var3 = ap.t(fo.bb, Client.ee.l);
                    var3.i.a(z.c(var1));
                    var3.i.u(var1);
                    Client.ee.i(var3);
                }

                h.e();
                if (ad.ot != null) {
                    ad.ot.cy();
                }

            }
        }
    }

    @ObfuscatedName("h")
    final boolean h(kb var1) {
        ks var2 = (ks) this.l.w(var1);
        return var2 != null && var2.br();
    }

    @ObfuscatedName("i")
    static synchronized void i(byte[] var0) {
        if (var0.length == 100 && gc.t < 1000) {
            gc.a[++gc.t - 1] = var0;
        } else if (var0.length == 5000 && gc.q < 250) {
            gc.l[++gc.q - 1] = var0;
        } else if (var0.length == 30000 && gc.i < 50) {
            gc.b[++gc.i - 1] = var0;
        } else {
            if (gc.p != null) {
                for (int var1 = 0; var1 < CombatBarData.e.length; ++var1) {
                    if (var0.length == CombatBarData.e[var1] && gc.x[var1] < gc.p[var1].length) {
                        gc.p[var1][gc.x[var1]++] = var0;
                        return;
                    }
                }
            }

        }
    }

    @ObfuscatedName("c")
    static final int c(int var0, int var1) {
        if (var0 == -1) {
            return 12345678;
        } else {
            var1 = (var0 & 127) * var1 / 128;
            if (var1 < 2) {
                var1 = 2;
            } else if (var1 > 126) {
                var1 = 126;
            }

            return (var0 & 'ﾀ') + var1;
        }
    }
}
