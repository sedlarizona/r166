import java.util.zip.CRC32;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ju")
public class ju extends FileSystem {
   @ObfuscatedName("av")
   static CRC32 av = new CRC32();
   @ObfuscatedName("r")
   fn r;
   @ObfuscatedName("y")
   fn y;
   @ObfuscatedName("h")
   int h;
   @ObfuscatedName("m")
   volatile boolean m = false;
   @ObfuscatedName("ay")
   boolean ay = false;
   @ObfuscatedName("ao")
   volatile boolean[] ao;
   @ObfuscatedName("aj")
   int aj;
   @ObfuscatedName("ae")
   int ae;
   @ObfuscatedName("am")
   int am = -1;

   public ju(fn var1, fn var2, int var3, boolean var4, boolean var5, boolean var6) {
      super(var4, var5);
      this.r = var1;
      this.y = var2;
      this.h = var3;
      this.ay = var6;
      int var8 = this.h;
      if (em.s != null) {
         em.s.index = var8 * 8 + 5;
         int var9 = em.s.ap();
         int var10 = em.s.ap();
         this.dl(var9, var10);
      } else {
         TaskHandler.i((ju)null, 255, 255, 0, (byte)0, true);
         jg.d[var8] = this;
      }

   }

   @ObfuscatedName("q")
   void q(int var1) {
      IdentityKitNode.a(this.h, var1);
   }

   @ObfuscatedName("p")
   int p(int var1) {
      if (super.entryBuffers[var1] != null) {
         return 100;
      } else if (this.ao[var1]) {
         return 100;
      } else {
         int var3 = this.h;
         long var4 = (long)((var3 << 16) + var1);
         int var2;
         if (TaskHandler.v != null && TaskHandler.v.uid == var4) {
            var2 = RSRandomAccessFileChannel.j.index * 99 / (RSRandomAccessFileChannel.j.buffer.length - TaskHandler.v.i) + 1;
         } else {
            var2 = 0;
         }

         return var2;
      }
   }

   @ObfuscatedName("k")
   void k(int var1) {
      if (this.r != null && this.ao != null && this.ao[var1]) {
         TileModel.t(var1, this.r, this);
      } else {
         TaskHandler.i(this, this.h, var1, super.entryCrcs[var1], (byte)2, true);
      }

   }

   @ObfuscatedName("dc")
   public int dc() {
      if (this.m) {
         return 100;
      } else if (super.entryBuffers != null) {
         return 99;
      } else {
         int var2 = this.h;
         long var3 = (long)(var2 + 16711680);
         int var1;
         if (TaskHandler.v != null && var3 == TaskHandler.v.uid) {
            var1 = RSRandomAccessFileChannel.j.index * 99 / (RSRandomAccessFileChannel.j.buffer.length - TaskHandler.v.i) + 1;
         } else {
            var1 = 0;
         }

         int var5 = var1;
         if (var1 >= 100) {
            var5 = 99;
         }

         return var5;
      }
   }

   @ObfuscatedName("dl")
   public void dl(int var1, int var2) {
      this.aj = var1;
      this.ae = var2;
      if (this.y != null) {
         TileModel.t(this.h, this.y, this);
      } else {
         TaskHandler.i(this, 255, this.h, this.aj, (byte)0, true);
      }

   }

   @ObfuscatedName("df")
   public void df(int var1, byte[] var2, boolean var3, boolean var4) {
      if (var3) {
         if (this.m) {
            throw new RuntimeException();
         }

         if (this.y != null) {
            int var5 = this.h;
            fn var6 = this.y;
            ia var7 = new ia();
            var7.t = 0;
            var7.uid = (long)var5;
            var7.q = var2;
            var7.i = var6;
            Deque var8 = jt.q;
            synchronized(jt.q) {
               jt.q.q(var7);
            }

            Object var21 = jt.l;
            synchronized(jt.l) {
               if (jt.a == 0) {
                  jt.b = new Thread(new jt());
                  jt.b.setDaemon(true);
                  jt.b.start();
                  jt.b.setPriority(5);
               }

               jt.a = 600;
            }
         }

         this.t(var2);
         this.dt();
      } else {
         var2[var2.length - 2] = (byte)(super.childIndexCounts[var1] >> 8);
         var2[var2.length - 1] = (byte)super.childIndexCounts[var1];
         if (this.r != null) {
            fn var13 = this.r;
            ia var18 = new ia();
            var18.t = 0;
            var18.uid = (long)var1;
            var18.q = var2;
            var18.i = var13;
            Deque var19 = jt.q;
            synchronized(jt.q) {
               jt.q.q(var18);
            }

            Object var20 = jt.l;
            synchronized(jt.l) {
               if (jt.a == 0) {
                  jt.b = new Thread(new jt());
                  jt.b.setDaemon(true);
                  jt.b.start();
                  jt.b.setPriority(5);
               }

               jt.a = 600;
            }

            this.ao[var1] = true;
         }

         if (var4) {
            super.entryBuffers[var1] = hi.x(var2, false);
         }
      }

   }

   @ObfuscatedName("dq")
   public void dq(fn var1, int var2, byte[] var3, boolean var4) {
      int var5;
      if (var1 == this.y) {
         if (this.m) {
            throw new RuntimeException();
         } else if (var3 == null) {
            TaskHandler.i(this, 255, this.h, this.aj, (byte)0, true);
         } else {
            av.reset();
            av.update(var3, 0, var3.length);
            var5 = (int)av.getValue();
            if (var5 != this.aj) {
               TaskHandler.i(this, 255, this.h, this.aj, (byte)0, true);
            } else {
               ByteBuffer var9 = new ByteBuffer(gq.au(var3));
               int var7 = var9.av();
               if (var7 != 5 && var7 != 6) {
                  throw new RuntimeException(var7 + "," + this.h + "," + var2);
               } else {
                  int var8 = 0;
                  if (var7 >= 6) {
                     var8 = var9.ap();
                  }

                  if (var8 != this.ae) {
                     TaskHandler.i(this, 255, this.h, this.aj, (byte)0, true);
                  } else {
                     this.t(var3);
                     this.dt();
                  }
               }
            }
         }
      } else {
         if (!var4 && var2 == this.am) {
            this.m = true;
         }

         if (var3 != null && var3.length > 2) {
            av.reset();
            av.update(var3, 0, var3.length - 2);
            var5 = (int)av.getValue();
            int var6 = ((var3[var3.length - 2] & 255) << 8) + (var3[var3.length - 1] & 255);
            if (var5 == super.entryCrcs[var2] && var6 == super.childIndexCounts[var2]) {
               this.ao[var2] = true;
               if (var4) {
                  super.entryBuffers[var2] = hi.x(var3, false);
               }

            } else {
               this.ao[var2] = false;
               if (this.ay || var4) {
                  TaskHandler.i(this, this.h, var2, super.entryCrcs[var2], (byte)2, var4);
               }

            }
         } else {
            this.ao[var2] = false;
            if (this.ay || var4) {
               TaskHandler.i(this, this.h, var2, super.entryCrcs[var2], (byte)2, var4);
            }

         }
      }
   }

   @ObfuscatedName("dt")
   void dt() {
      this.ao = new boolean[super.entryBuffers.length];

      int var1;
      for(var1 = 0; var1 < this.ao.length; ++var1) {
         this.ao[var1] = false;
      }

      if (this.r == null) {
         this.m = true;
      } else {
         this.am = -1;

         for(var1 = 0; var1 < this.ao.length; ++var1) {
            if (super.entryChildCounts[var1] > 0) {
               fn var2 = this.r;
               ia var4 = new ia();
               var4.t = 1;
               var4.uid = (long)var1;
               var4.i = var2;
               var4.a = this;
               Deque var5 = jt.q;
               synchronized(jt.q) {
                  jt.q.q(var4);
               }

               Object var10 = jt.l;
               synchronized(jt.l) {
                  if (jt.a == 0) {
                     jt.b = new Thread(new jt());
                     jt.b.setDaemon(true);
                     jt.b.start();
                     jt.b.setPriority(5);
                  }

                  jt.a = 600;
               }

               this.am = var1;
            }
         }

         if (this.am == -1) {
            this.m = true;
         }

      }
   }

   @ObfuscatedName("dy")
   public boolean dy(int var1) {
      return this.ao[var1];
   }

   @ObfuscatedName("dn")
   public boolean dn(int var1) {
      return this.z(var1) != null;
   }

   @ObfuscatedName("do")
   public int do() {
      int var1 = 0;
      int var2 = 0;

      int var3;
      for(var3 = 0; var3 < super.entryBuffers.length; ++var3) {
         if (super.entryChildCounts[var3] > 0) {
            var1 += 100;
            var2 += this.p(var3);
         }
      }

      if (var1 == 0) {
         return 100;
      } else {
         var3 = var2 * 100 / var1;
         return var3;
      }
   }

   @ObfuscatedName("l")
   static int l(CharSequence var0, int var1, boolean var2) {
      if (var1 >= 2 && var1 <= 36) {
         boolean var3 = false;
         boolean var4 = false;
         int var5 = 0;
         int var6 = var0.length();

         for(int var7 = 0; var7 < var6; ++var7) {
            char var8 = var0.charAt(var7);
            if (var7 == 0) {
               if (var8 == '-') {
                  var3 = true;
                  continue;
               }

               if (var8 == '+') {
                  continue;
               }
            }

            int var10;
            if (var8 >= '0' && var8 <= '9') {
               var10 = var8 - 48;
            } else if (var8 >= 'A' && var8 <= 'Z') {
               var10 = var8 - 55;
            } else {
               if (var8 < 'a' || var8 > 'z') {
                  throw new NumberFormatException();
               }

               var10 = var8 - 87;
            }

            if (var10 >= var1) {
               throw new NumberFormatException();
            }

            if (var3) {
               var10 = -var10;
            }

            int var9 = var5 * var1 + var10;
            if (var9 / var1 != var5) {
               throw new NumberFormatException();
            }

            var5 = var9;
            var4 = true;
         }

         if (!var4) {
            throw new NumberFormatException();
         } else {
            return var5;
         }
      } else {
         throw new IllegalArgumentException("");
      }
   }
}
