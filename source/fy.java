import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fy")
public abstract class fy {

    @ObfuscatedName("z")
    public static String[] z;

    @ObfuscatedName("t")
    public abstract boolean t(int var1)
            throws IOException;

    @ObfuscatedName("q")
    public abstract int q()
            throws IOException;

    @ObfuscatedName("i")
    public abstract int i()
            throws IOException;

    @ObfuscatedName("a")
    public abstract int a(byte[] var1, int var2, int var3)
            throws IOException;

    @ObfuscatedName("l")
    public abstract void l(byte[] var1, int var2, int var3)
            throws IOException;

    @ObfuscatedName("b")
    public abstract void b();

    @ObfuscatedName("q")
    public static int q(int var0, int var1) {
        int var2;
        if (var1 > var0) {
            var2 = var0;
            var0 = var1;
            var1 = var2;
        }

        while (var1 != 0) {
            var2 = var0 % var1;
            var0 = var1;
            var1 = var2;
        }

        return var0;
    }

    @ObfuscatedName("hi")
    public static final void hi(String var0, String var1, int var2, int var3, int var4, int var5) {
        h.addMenuItem(var0, var1, var2, var3, var4, var5, false);
    }
}
