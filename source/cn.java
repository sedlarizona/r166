import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cn")
public class cn {

    @ObfuscatedName("ry")
    public static jo ry;

    @ObfuscatedName("t")
    fy t;

    @ObfuscatedName("q")
    SinglyLinkedList q = new SinglyLinkedList();

    @ObfuscatedName("i")
    int i = 0;

    @ObfuscatedName("a")
    ByteBuffer a = new ByteBuffer(5000);

    @ObfuscatedName("l")
    public gv l;

    @ObfuscatedName("b")
    Packet b = new Packet(40000);

    @ObfuscatedName("e")
    fx e = null;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    boolean p = true;

    @ObfuscatedName("g")
    int g = 0;

    @ObfuscatedName("n")
    int n = 0;

    @ObfuscatedName("o")
    fx o;

    @ObfuscatedName("c")
    fx c;

    @ObfuscatedName("v")
    fx v;

    @ObfuscatedName("t")
    final void t() {
        this.q.t();
        this.i = 0;
    }

    @ObfuscatedName("q")
    final void q()
            throws IOException {
        if (this.t != null && this.i > 0) {
            this.a.index = 0;

            while (true) {
                gd var1 = (gd) this.q.b();
                if (var1 == null || var1.a > this.a.buffer.length - this.a.index) {
                    this.t.l(this.a.buffer, 0, this.a.index);
                    this.n = 0;
                    break;
                }

                this.a.s(var1.i.buffer, 0, var1.a);
                this.i -= var1.a;
                var1.kc();
                var1.i.i();
                var1.i();
            }
        }

    }

    @ObfuscatedName("i")
    public final void i(gd var1) {
        this.q.q(var1);
        var1.a = var1.i.index;
        var1.i.index = 0;
        this.i += var1.a;
    }

    @ObfuscatedName("a")
    void a(fy var1) {
        this.t = var1;
    }

    @ObfuscatedName("l")
    void l() {
        if (this.t != null) {
            this.t.b();
            this.t = null;
        }

    }

    @ObfuscatedName("b")
    void b() {
        this.t = null;
    }

    @ObfuscatedName("e")
    fy e() {
        return this.t;
    }

    @ObfuscatedName("ai")
    static final void ai(TaskDataNode var0) {
        var0.hasArray = false;
        if (var0.integerNode != null) {
            var0.integerNode.number = 0;
        }

        for (TaskDataNode var1 = var0.b(); var1 != null; var1 = var0.e()) {
            ai(var1);
        }

    }
}
