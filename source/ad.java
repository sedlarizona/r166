import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.RandomAccessFile;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ad")
public final class ad implements KeyListener, FocusListener {

    @ObfuscatedName("ot")
    static kl ot;

    @ObfuscatedName("t")
    static ad keyboard = new ad();

    @ObfuscatedName("al")
    public static Producer interfaceProducer;

    @ObfuscatedName("bc")
    static lu bc;

    @ObfuscatedName("co")
    public static boolean[] pressedKeys = new boolean[112];

    @ObfuscatedName("cq")
    static int[] cq = new int[128];

    @ObfuscatedName("ci")
    static int ci = 0;

    @ObfuscatedName("cc")
    static int cc = 0;

    @ObfuscatedName("ce")
    static char[] ce = new char[128];

    @ObfuscatedName("cx")
    static int[] cx = new int[128];

    @ObfuscatedName("cy")
    public static int[] cy = new int[128];

    @ObfuscatedName("cg")
    public static int cg = 0;

    @ObfuscatedName("cj")
    static int cj = 0;

    @ObfuscatedName("cl")
    static int cl = 0;

    @ObfuscatedName("cz")
    static int cz = 0;

    @ObfuscatedName("cn")
    static volatile int cn = 0;

    @ObfuscatedName("ca")
    static int[] ca = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, 85, 80, 84, -1, 91, -1, -1, -1, 81, 82, 86, -1, -1,
            -1, -1, -1, -1, -1, -1, 13, -1, -1, -1, -1, 83, 104, 105, 103, 102, 96, 98, 97, 99, -1, -1, -1, -1, -1, -1,
            -1, 25, 16, 17, 18, 19, 20, 21, 22, 23, 24, -1, -1, -1, -1, -1, -1, -1, 48, 68, 66, 50, 34, 51, 52, 53, 39,
            54, 55, 56, 70, 69, 40, 41, 32, 35, 49, 36, 38, 67, 33, 65, 37, 64, -1, -1, -1, -1, -1, 228, 231, 227, 233,
            224, 219, 225, 230, 226, 232, 89, 87, -1, 88, 229, 90, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -1, -1,
            101, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

    public final synchronized void focusLost(FocusEvent var1) {
        if (keyboard != null) {
            cc = -1;
        }

    }

    public final void keyTyped(KeyEvent var1) {
        if (keyboard != null) {
            char var2 = var1.getKeyChar();
            if (var2 != 0 && var2 != '\uffff') {
                boolean var3;
                if (var2 > 0 && var2 < 128 || var2 >= 160 && var2 <= 255) {
                    var3 = true;
                } else {
                    label59: {
                        if (var2 != 0) {
                            char[] var7 = lv.t;

                            for (int var5 = 0; var5 < var7.length; ++var5) {
                                char var6 = var7[var5];
                                if (var6 == var2) {
                                    var3 = true;
                                    break label59;
                                }
                            }
                        }

                        var3 = false;
                    }
                }

                if (var3) {
                    int var4 = cl + 1 & 127;
                    if (var4 != cj) {
                        cx[cl] = -1;
                        ce[cl] = var2;
                        cl = var4;
                    }
                }
            }
        }

        var1.consume();
    }

    public final void focusGained(FocusEvent var1) {
    }

    public final synchronized void keyReleased(KeyEvent var1) {
        if (keyboard != null) {
            int var2 = var1.getKeyCode();
            if (var2 >= 0 && var2 < ca.length) {
                var2 = ca[var2] & -129;
            } else {
                var2 = -1;
            }

            if (cc >= 0 && var2 >= 0) {
                cq[cc] = ~var2;
                cc = cc + 1 & 127;
                if (cc == ci) {
                    cc = -1;
                }
            }
        }

        var1.consume();
    }

    public final synchronized void keyPressed(KeyEvent var1) {
        if (keyboard != null) {
            int var2 = var1.getKeyCode();
            if (var2 >= 0 && var2 < ca.length) {
                var2 = ca[var2];
                if ((var2 & 128) != 0) {
                    var2 = -1;
                }
            } else {
                var2 = -1;
            }

            if (cc >= 0 && var2 >= 0) {
                cq[cc] = var2;
                cc = cc + 1 & 127;
                if (ci == cc) {
                    cc = -1;
                }
            }

            int var3;
            if (var2 >= 0) {
                var3 = cl + 1 & 127;
                if (var3 != cj) {
                    cx[cl] = var2;
                    ce[cl] = 0;
                    cl = var3;
                }
            }

            var3 = var1.getModifiers();
            if ((var3 & 10) != 0 || var2 == 85 || var2 == 10) {
                var1.consume();
            }
        }

    }

    @ObfuscatedName("t")
    static void t(Component var0) {
        var0.addMouseListener(bs.mouse);
        var0.addMouseMotionListener(bs.mouse);
        var0.addFocusListener(bs.mouse);
    }

    @ObfuscatedName("i")
    public static boolean i(File var0, boolean var1) {
        try {
            RandomAccessFile var2 = new RandomAccessFile(var0, "rw");
            int var3 = var2.read();
            var2.seek(0L);
            var2.write(var3);
            var2.seek(0L);
            var2.close();
            if (var1) {
                var0.delete();
            }

            return true;
        } catch (Exception var4) {
            return false;
        }
    }

    @ObfuscatedName("i")
    public static int i(int var0, int var1, int var2, int var3, int var4, int var5) {
        if ((var5 & 1) == 1) {
            int var6 = var3;
            var3 = var4;
            var4 = var6;
        }

        var2 &= 3;
        if (var2 == 0) {
            return var1;
        } else if (var2 == 1) {
            return 7 - var0 - (var3 - 1);
        } else {
            return var2 == 2 ? 7 - var1 - (var4 - 1) : var0;
        }
    }
}
