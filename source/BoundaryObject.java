import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("eg")
public final class BoundaryObject {

    @ObfuscatedName("qi")
    public static cm qi;

    @ObfuscatedName("r")
    public static int r;

    @ObfuscatedName("t")
    int plane;

    @ObfuscatedName("q")
    int regionX;

    @ObfuscatedName("i")
    int regionY;

    @ObfuscatedName("a")
    int orientationA;

    @ObfuscatedName("l")
    int orientationB;

    @ObfuscatedName("b")
    public Renderable modelA;

    @ObfuscatedName("e")
    public Renderable modelB;

    @ObfuscatedName("x")
    public int hash = 0;

    @ObfuscatedName("p")
    int placement = 0;
}
