import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("s")
final class s implements Comparator {

    @ObfuscatedName("k")
    public static String[] k;

    @ObfuscatedName("t")
    int t(u var1, u var2) {
        return var1.q < var2.q ? -1 : (var1.q == var2.q ? 0 : 1);
    }

    public int compare(Object var1, Object var2) {
        return this.t((u) var1, (u) var2);
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }

    @ObfuscatedName("x")
    static final int x(int var0, int var1, int var2, int var3) {
        int var4 = 65536 - eu.ay[var2 * 1024 / var3] >> 1;
        return ((65536 - var4) * var0 >> 16) + (var4 * var1 >> 16);
    }

    @ObfuscatedName("p")
    public static boolean p(char var0) {
        return var0 >= 'A' && var0 <= 'Z' || var0 >= 'a' && var0 <= 'z';
    }

    @ObfuscatedName("iw")
    static void iw(RTComponent var0, int var1, int var2) {
        if (var0.r == 0) {
            var0.relativeX = var0.ay;
        } else if (var0.r == 1) {
            var0.relativeX = var0.ay + (var1 - var0.width) / 2;
        } else if (var0.r == 2) {
            var0.relativeX = var1 - var0.width - var0.ay;
        } else if (var0.r == 3) {
            var0.relativeX = var0.ay * var1 >> 14;
        } else if (var0.r == 4) {
            var0.relativeX = (var0.ay * var1 >> 14) + (var1 - var0.width) / 2;
        } else {
            var0.relativeX = var1 - var0.width - (var0.ay * var1 >> 14);
        }

        if (var0.y == 0) {
            var0.relativeY = var0.ao;
        } else if (var0.y == 1) {
            var0.relativeY = (var2 - var0.height) / 2 + var0.ao;
        } else if (var0.y == 2) {
            var0.relativeY = var2 - var0.height - var0.ao;
        } else if (var0.y == 3) {
            var0.relativeY = var2 * var0.ao >> 14;
        } else if (var0.y == 4) {
            var0.relativeY = (var2 - var0.height) / 2 + (var2 * var0.ao >> 14);
        } else {
            var0.relativeY = var2 - var0.height - (var2 * var0.ao >> 14);
        }

    }
}
