import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bb")
public class ItemStorage extends Node {

    @ObfuscatedName("t")
    static HashTable t = new HashTable(32);

    @ObfuscatedName("f")
    static int[] f;

    @ObfuscatedName("mr")
    static int mr;

    @ObfuscatedName("q")
    int[] ids = new int[] { -1 };

    @ObfuscatedName("i")
    int[] stackSizes = new int[] { 0 };

    @ObfuscatedName("hn")
    static final void hn(int var0, int var1, int var2, int var3) {
        for (int var4 = 0; var4 < Client.nm; ++var4) {
            if (Client.interfaceWidths[var4] + Client.interfaceXPositions[var4] > var0
                    && Client.interfaceXPositions[var4] < var0 + var2
                    && Client.interfaceYPositions[var4] + Client.interfaceHeights[var4] > var1
                    && Client.interfaceYPositions[var4] < var3 + var1) {
                Client.nt[var4] = true;
            }
        }

    }
}
