import java.util.HashMap;
import java.util.Map;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cg")
public class cg {

    @ObfuscatedName("t")
    static final Map chatboxes = new HashMap();

    @ObfuscatedName("q")
    static final FixedSizeDeque q = new FixedSizeDeque(1024);

    @ObfuscatedName("i")
    static final DoublyNode i = new DoublyNode();

    @ObfuscatedName("a")
    static int a = 0;

    @ObfuscatedName("l")
    public static boolean l(int var0) {
        return (var0 >> 30 & 1) != 0;
    }

    @ObfuscatedName("ip")
   static final void ip(RTComponent var0, int var1, int var2) {
      if (var0.d == 1) {
         fy.hi(var0.buttonAction, "", 24, 0, 0, var0.id);
      }

      String var3;
      if (var0.d == 2 && !Client.interfaceSelected) {
         var3 = f.jl(var0);
         if (var3 != null) {
            fy.hi(var3, ar.q(65280) + var0.ed, 25, 0, -1, var0.id);
         }
      }

      if (var0.d == 3) {
         fy.hi("Close", "", 26, 0, 0, var0.id);
      }

      if (var0.d == 4) {
         fy.hi(var0.buttonAction, "", 28, 0, 0, var0.id);
      }

      if (var0.d == 5) {
         fy.hi(var0.buttonAction, "", 29, 0, 0, var0.id);
      }

      if (var0.d == 6 && Client.ls == null) {
         fy.hi(var0.buttonAction, "", 30, 0, -1, var0.id);
      }

      int var4;
      int var5;
      int var13;
      if (var0.type == 2) {
         var13 = 0;

         for(var4 = 0; var4 < var0.height; ++var4) {
            for(var5 = 0; var5 < var0.width; ++var5) {
               int var6 = (var0.cw + 32) * var5;
               int var7 = (var0.ch + 32) * var4;
               if (var13 < 20) {
                  var6 += var0.cr[var13];
                  var7 += var0.co[var13];
               }

               if (var1 >= var6 && var2 >= var7 && var1 < var6 + 32 && var2 < var7 + 32) {
                  Client.ic = var13;
                  c.if = var0;
                  if (var0.itemIds[var13] > 0) {
                     ItemDefinition var8 = cs.loadItemDefinition(var0.itemIds[var13] - 1);
                     if (Client.inventoryItemSelectionState == 1 && l(u.jr(var0))) {
                        if (var0.id != bg.ix || var13 != jz.ks) {
                           fy.hi("Use", Client.lastSelectedInventoryItemName + " " + "->" + " " + ar.q(16748608) + var8.name, 31, var8.id, var13, var0.id);
                        }
                     } else if (Client.interfaceSelected && l(u.jr(var0))) {
                        if ((eq.km & 16) == 16) {
                           fy.hi(Client.lj, Client.selectedSpellName + " " + "->" + " " + ar.q(16748608) + var8.name, 32, var8.id, var13, var0.id);
                        }
                     } else {
                        String[] var9 = var8.inventoryActions;
                        int var10 = -1;
                        if (Client.ki) {
                           boolean var11 = Client.kv || ad.pressedKeys[81];
                           if (var11) {
                              var10 = var8.r();
                           }
                        }

                        int var18;
                        if (l(u.jr(var0))) {
                           for(var18 = 4; var18 >= 3; --var18) {
                              if (var10 != var18) {
                                 ChatboxMessage.ib(var0, var8, var13, var18, false);
                              }
                           }
                        }

                        if (Renderable.b(u.jr(var0))) {
                           fy.hi("Use", ar.q(16748608) + var8.name, 38, var8.id, var13, var0.id);
                        }

                        if (l(u.jr(var0))) {
                           for(var18 = 2; var18 >= 0; --var18) {
                              if (var18 != var10) {
                                 ChatboxMessage.ib(var0, var8, var13, var18, false);
                              }
                           }

                           if (var10 >= 0) {
                              ChatboxMessage.ib(var0, var8, var13, var10, true);
                           }
                        }

                        var9 = var0.tableActions;
                        if (var9 != null) {
                           for(var18 = 4; var18 >= 0; --var18) {
                              if (var9[var18] != null) {
                                 byte var12 = 0;
                                 if (var18 == 0) {
                                    var12 = 39;
                                 }

                                 if (var18 == 1) {
                                    var12 = 40;
                                 }

                                 if (var18 == 2) {
                                    var12 = 41;
                                 }

                                 if (var18 == 3) {
                                    var12 = 42;
                                 }

                                 if (var18 == 4) {
                                    var12 = 43;
                                 }

                                 fy.hi(var9[var18], ar.q(16748608) + var8.name, var12, var8.id, var13, var0.id);
                              }
                           }
                        }

                        fy.hi("Examine", ar.q(16748608) + var8.name, 1005, var8.id, var13, var0.id);
                     }
                  }
               }

               ++var13;
            }
         }
      }

      if (var0.modern) {
         if (Client.interfaceSelected) {
            var4 = u.jr(var0);
            boolean var19 = (var4 >> 21 & 1) != 0;
            if (var19 && (eq.km & 32) == 32) {
               fy.hi(Client.lj, Client.selectedSpellName + " " + "->" + " " + var0.name, 58, 0, var0.w, var0.id);
            }
         } else {
            for(var13 = 9; var13 >= 5; --var13) {
               String var14;
               if (!cs.t(u.jr(var0), var13) && var0.du == null) {
                  var14 = null;
               } else if (var0.actions != null && var0.actions.length > var13 && var0.actions[var13] != null && var0.actions[var13].trim().length() != 0) {
                  var14 = var0.actions[var13];
               } else {
                  var14 = null;
               }

               if (var14 != null) {
                  fy.hi(var14, var0.name, 1007, var13 + 1, var0.w, var0.id);
               }
            }

            var3 = f.jl(var0);
            if (var3 != null) {
               fy.hi(var3, var0.name, 25, 0, var0.w, var0.id);
            }

            for(var4 = 4; var4 >= 0; --var4) {
               String var15;
               if (!cs.t(u.jr(var0), var4) && var0.du == null) {
                  var15 = null;
               } else if (var0.actions != null && var0.actions.length > var4 && var0.actions[var4] != null && var0.actions[var4].trim().length() != 0) {
                  var15 = var0.actions[var4];
               } else {
                  var15 = null;
               }

               if (var15 != null) {
                  fy.hi(var15, var0.name, 57, var4 + 1, var0.w, var0.id);
               }
            }

            var5 = u.jr(var0);
            boolean var17 = (var5 & 1) != 0;
            if (var17) {
               fy.hi("Continue", "", 30, 0, var0.w, var0.id);
            }
         }
      }

   }
}
