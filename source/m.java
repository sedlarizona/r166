import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("m")
public class m {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    ik q;

    m(int var1, ik var2) {
        this.t = var1;
        this.q = var2;
    }

    @ObfuscatedName("t")
    public static void t(ScriptEvent var0, int var1) {
        Object[] var2 = var0.args;
        RuneScript var3;
        RuneScript var5;
        int var7;
        int var8;
        int var9;
        int var10;
        RuneScript var12;
        int var16;
        int var19;
        RuneScript var35;
        if (Inflater.a(var0.o)) {
            v.k = (ak) var2[0];
            jj var4 = jj.q[v.k.t];
            iw var6 = var0.o;
            var7 = var4.l;
            var8 = var4.r;
            var9 = (var7 << 8) + var6.n;
            var12 = (RuneScript) RuneScript.t.t((long) (var9 << 16));
            RuneScript var11;
            if (var12 != null) {
                var11 = var12;
            } else {
                String var13 = String.valueOf(var9);
                int var14 = p.da.h(var13);
                if (var14 == -1) {
                    var11 = null;
                } else {
                    label568: {
                        byte[] var15 = p.da.o(var14);
                        if (var15 != null) {
                            if (var15.length <= 1) {
                                var11 = null;
                                break label568;
                            }

                            var12 = q.t(var15);
                            if (var12 != null) {
                                RuneScript.t.i(var12, (long) (var9 << 16));
                                var11 = var12;
                                break label568;
                            }
                        }

                        var11 = null;
                    }
                }
            }

            if (var11 != null) {
                var5 = var11;
            } else {
                var10 = ItemPile.t(var8, var6);
                RuneScript var18 = (RuneScript) RuneScript.t.t((long) (var10 << 16));
                if (var18 != null) {
                    var35 = var18;
                } else {
                    String var39 = String.valueOf(var10);
                    var16 = p.da.h(var39);
                    if (var16 == -1) {
                        var35 = null;
                    } else {
                        label513: {
                            byte[] var17 = p.da.o(var16);
                            if (var17 != null) {
                                if (var17.length <= 1) {
                                    var35 = null;
                                    break label513;
                                }

                                var18 = q.t(var17);
                                if (var18 != null) {
                                    RuneScript.t.i(var18, (long) (var10 << 16));
                                    var35 = var18;
                                    break label513;
                                }
                            }

                            var35 = null;
                        }
                    }
                }

                if (var35 != null) {
                    var5 = var35;
                } else {
                    var5 = null;
                }
            }

            var3 = var5;
        } else {
            var19 = (Integer) var2[0];
            RuneScript var29 = (RuneScript) RuneScript.t.t((long) var19);
            if (var29 != null) {
                var5 = var29;
            } else {
                byte[] var20 = p.da.i(var19, 0);
                if (var20 == null) {
                    var5 = null;
                } else {
                    var29 = q.t(var20);
                    RuneScript.t.i(var29, (long) var19);
                    var5 = var29;
                }
            }

            var3 = var5;
        }

        if (var3 != null) {
            b.menuX = 0;
            ly.g = 0;
            var19 = -1;
            int[] var32 = var3.q;
            int[] var30 = var3.i;
            byte var31 = -1;
            cs.n = 0;

            try {
                bk.i = new int[var3.stringArgCount];
                var8 = 0;
                gk.a = new String[var3.stringStackCount];
                var9 = 0;

                int var21;
                String var34;
                for (var10 = 1; var10 < var2.length; ++var10) {
                    if (var2[var10] instanceof Integer) {
                        var21 = (Integer) var2[var10];
                        if (var21 == -2147483647) {
                            var21 = var0.a;
                        }

                        if (var21 == -2147483646) {
                            var21 = var0.l;
                        }

                        if (var21 == -2147483645) {
                            var21 = var0.i != null ? var0.i.id : -1;
                        }

                        if (var21 == -2147483644) {
                            var21 = var0.b;
                        }

                        if (var21 == -2147483643) {
                            var21 = var0.i != null ? var0.i.w : -1;
                        }

                        if (var21 == -2147483642) {
                            var21 = var0.e != null ? var0.e.id : -1;
                        }

                        if (var21 == -2147483641) {
                            var21 = var0.e != null ? var0.e.w : -1;
                        }

                        if (var21 == -2147483640) {
                            var21 = var0.x;
                        }

                        if (var21 == -2147483639) {
                            var21 = var0.p;
                        }

                        bk.i[var8++] = var21;
                    } else if (var2[var10] instanceof String) {
                        var34 = (String) var2[var10];
                        if (var34.equals("event_opbase")) {
                            var34 = var0.name;
                        }

                        gk.a[var9++] = var34;
                    }
                }

                var10 = 0;
                cs.z = var0.n;

                while (true) {
                    while (true) {
                        while (true) {
                            while (true) {
                                while (true) {
                                    while (true) {
                                        while (true) {
                                            while (true) {
                                                while (true) {
                                                    while (true) {
                                                        while (true) {
                                                            while (true) {
                                                                while (true) {
                                                                    while (true) {
                                                                        while (true) {
                                                                            while (true) {
                                                                                while (true) {
                                                                                    while (true) {
                                                                                        while (true) {
                                                                                            while (true) {
                                                                                                while (true) {
                                                                                                    while (true) {
                                                                                                        while (true) {
                                                                                                            while (true) {
                                                                                                                while (true) {
                                                                                                                    label340: while (true) {
                                                                                                                        ++var10;
                                                                                                                        if (var10 > var1) {
                                                                                                                            throw new RuntimeException();
                                                                                                                        }

                                                                                                                        ++var19;
                                                                                                                        var7 = var32[var19];
                                                                                                                        int var23;
                                                                                                                        if (var7 < 100) {
                                                                                                                            if (var7 != 0) {
                                                                                                                                if (var7 != 1) {
                                                                                                                                    if (var7 != 2) {
                                                                                                                                        if (var7 != 3) {
                                                                                                                                            if (var7 != 6) {
                                                                                                                                                if (var7 != 7) {
                                                                                                                                                    if (var7 != 8) {
                                                                                                                                                        if (var7 != 9) {
                                                                                                                                                            if (var7 != 10) {
                                                                                                                                                                if (var7 != 21) {
                                                                                                                                                                    if (var7 != 25) {
                                                                                                                                                                        if (var7 != 27) {
                                                                                                                                                                            if (var7 != 31) {
                                                                                                                                                                                if (var7 != 32) {
                                                                                                                                                                                    if (var7 != 33) {
                                                                                                                                                                                        if (var7 != 34) {
                                                                                                                                                                                            if (var7 != 35) {
                                                                                                                                                                                                if (var7 != 36) {
                                                                                                                                                                                                    if (var7 != 37) {
                                                                                                                                                                                                        if (var7 != 38) {
                                                                                                                                                                                                            if (var7 != 39) {
                                                                                                                                                                                                                if (var7 != 40) {
                                                                                                                                                                                                                    if (var7 != 42) {
                                                                                                                                                                                                                        if (var7 != 43) {
                                                                                                                                                                                                                            if (var7 == 44) {
                                                                                                                                                                                                                                var21 = var30[var19] >> 16;
                                                                                                                                                                                                                                var23 = var30[var19] & '\uffff';
                                                                                                                                                                                                                                int var24 = cs.e[--b.menuX];
                                                                                                                                                                                                                                if (var24 >= 0
                                                                                                                                                                                                                                        && var24 <= 5000) {
                                                                                                                                                                                                                                    cs.l[var21] = var24;
                                                                                                                                                                                                                                    byte var37 = -1;
                                                                                                                                                                                                                                    if (var23 == 105) {
                                                                                                                                                                                                                                        var37 = 0;
                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                    int var25 = 0;

                                                                                                                                                                                                                                    while (true) {
                                                                                                                                                                                                                                        if (var25 >= var24) {
                                                                                                                                                                                                                                            continue label340;
                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                        cs.b[var21][var25] = var37;
                                                                                                                                                                                                                                        ++var25;
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                throw new RuntimeException();
                                                                                                                                                                                                                            } else if (var7 == 45) {
                                                                                                                                                                                                                                var21 = var30[var19];
                                                                                                                                                                                                                                var23 = cs.e[--b.menuX];
                                                                                                                                                                                                                                if (var23 < 0
                                                                                                                                                                                                                                        || var23 >= cs.l[var21]) {
                                                                                                                                                                                                                                    throw new RuntimeException();
                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                cs.e[++b.menuX - 1] = cs.b[var21][var23];
                                                                                                                                                                                                                            } else if (var7 == 46) {
                                                                                                                                                                                                                                var21 = var30[var19];
                                                                                                                                                                                                                                b.menuX -= 2;
                                                                                                                                                                                                                                var23 = cs.e[b.menuX];
                                                                                                                                                                                                                                if (var23 < 0
                                                                                                                                                                                                                                        || var23 >= cs.l[var21]) {
                                                                                                                                                                                                                                    throw new RuntimeException();
                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                cs.b[var21][var23] = cs.e[b.menuX + 1];
                                                                                                                                                                                                                            } else if (var7 == 47) {
                                                                                                                                                                                                                                var34 = g.vm
                                                                                                                                                                                                                                        .a(var30[var19]);
                                                                                                                                                                                                                                if (var34 == null) {
                                                                                                                                                                                                                                    var34 = "null";
                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                cs.p[++ly.g - 1] = var34;
                                                                                                                                                                                                                            } else if (var7 == 48) {
                                                                                                                                                                                                                                g.vm.i(var30[var19],
                                                                                                                                                                                                                                        cs.p[--ly.g]);
                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                if (var7 != 60) {
                                                                                                                                                                                                                                    throw new IllegalStateException();
                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                FixedSizeDeque var40 = var3.p[var30[var19]];
                                                                                                                                                                                                                                hc var38 = (hc) var40
                                                                                                                                                                                                                                        .t((long) cs.e[--b.menuX]);
                                                                                                                                                                                                                                if (var38 != null) {
                                                                                                                                                                                                                                    var19 += var38.t;
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                            g.vm.t(var30[var19],
                                                                                                                                                                                                                                    cs.e[--b.menuX]);
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                        cs.e[++b.menuX - 1] = g.vm
                                                                                                                                                                                                                                .q(var30[var19]);
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    var21 = var30[var19];
                                                                                                                                                                                                                    var35 = (RuneScript) RuneScript.t
                                                                                                                                                                                                                            .t((long) var21);
                                                                                                                                                                                                                    if (var35 != null) {
                                                                                                                                                                                                                        var12 = var35;
                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                        byte[] var42 = p.da
                                                                                                                                                                                                                                .i(var21,
                                                                                                                                                                                                                                        0);
                                                                                                                                                                                                                        if (var42 == null) {
                                                                                                                                                                                                                            var12 = null;
                                                                                                                                                                                                                        } else {
                                                                                                                                                                                                                            var35 = q
                                                                                                                                                                                                                                    .t(var42);
                                                                                                                                                                                                                            RuneScript.t
                                                                                                                                                                                                                                    .i(var35,
                                                                                                                                                                                                                                            (long) var21);
                                                                                                                                                                                                                            var12 = var35;
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }

                                                                                                                                                                                                                    var35 = var12;
                                                                                                                                                                                                                    int[] var43 = new int[var12.stringArgCount];
                                                                                                                                                                                                                    String[] var41 = new String[var12.stringStackCount];

                                                                                                                                                                                                                    for (var16 = 0; var16 < var35.intArgCount; ++var16) {
                                                                                                                                                                                                                        var43[var16] = cs.e[var16
                                                                                                                                                                                                                                + (b.menuX - var35.intArgCount)];
                                                                                                                                                                                                                    }

                                                                                                                                                                                                                    for (var16 = 0; var16 < var35.intStackCount; ++var16) {
                                                                                                                                                                                                                        var41[var16] = cs.p[var16
                                                                                                                                                                                                                                + (ly.g - var35.intStackCount)];
                                                                                                                                                                                                                    }

                                                                                                                                                                                                                    b.menuX -= var35.intArgCount;
                                                                                                                                                                                                                    ly.g -= var35.intStackCount;
                                                                                                                                                                                                                    RuneScriptStackItem var22 = new RuneScriptStackItem();
                                                                                                                                                                                                                    var22.script = var3;
                                                                                                                                                                                                                    var22.q = var19;
                                                                                                                                                                                                                    var22.i = bk.i;
                                                                                                                                                                                                                    var22.a = gk.a;
                                                                                                                                                                                                                    cs.o[++cs.n - 1] = var22;
                                                                                                                                                                                                                    var3 = var35;
                                                                                                                                                                                                                    var32 = var35.q;
                                                                                                                                                                                                                    var30 = var35.i;
                                                                                                                                                                                                                    var19 = -1;
                                                                                                                                                                                                                    bk.i = var43;
                                                                                                                                                                                                                    gk.a = var41;
                                                                                                                                                                                                                }
                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                --ly.g;
                                                                                                                                                                                                            }
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            --b.menuX;
                                                                                                                                                                                                        }
                                                                                                                                                                                                    } else {
                                                                                                                                                                                                        var21 = var30[var19];
                                                                                                                                                                                                        ly.g -= var21;
                                                                                                                                                                                                        String var33 = kp
                                                                                                                                                                                                                .t(cs.p,
                                                                                                                                                                                                                        ly.g,
                                                                                                                                                                                                                        var21);
                                                                                                                                                                                                        cs.p[++ly.g - 1] = var33;
                                                                                                                                                                                                    }
                                                                                                                                                                                                } else {
                                                                                                                                                                                                    gk.a[var30[var19]] = cs.p[--ly.g];
                                                                                                                                                                                                }
                                                                                                                                                                                            } else {
                                                                                                                                                                                                cs.p[++ly.g - 1] = gk.a[var30[var19]];
                                                                                                                                                                                            }
                                                                                                                                                                                        } else {
                                                                                                                                                                                            bk.i[var30[var19]] = cs.e[--b.menuX];
                                                                                                                                                                                        }
                                                                                                                                                                                    } else {
                                                                                                                                                                                        cs.e[++b.menuX - 1] = bk.i[var30[var19]];
                                                                                                                                                                                    }
                                                                                                                                                                                } else {
                                                                                                                                                                                    b.menuX -= 2;
                                                                                                                                                                                    if (cs.e[b.menuX] >= cs.e[b.menuX + 1]) {
                                                                                                                                                                                        var19 += var30[var19];
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            } else {
                                                                                                                                                                                b.menuX -= 2;
                                                                                                                                                                                if (cs.e[b.menuX] <= cs.e[b.menuX + 1]) {
                                                                                                                                                                                    var19 += var30[var19];
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        } else {
                                                                                                                                                                            var21 = var30[var19];
                                                                                                                                                                            fr.q(var21,
                                                                                                                                                                                    cs.e[--b.menuX]);
                                                                                                                                                                        }
                                                                                                                                                                    } else {
                                                                                                                                                                        var21 = var30[var19];
                                                                                                                                                                        cs.e[++b.menuX - 1] = Server
                                                                                                                                                                                .t(var21);
                                                                                                                                                                    }
                                                                                                                                                                } else {
                                                                                                                                                                    if (cs.n == 0) {
                                                                                                                                                                        return;
                                                                                                                                                                    }

                                                                                                                                                                    RuneScriptStackItem var36 = cs.o[--cs.n];
                                                                                                                                                                    var3 = var36.script;
                                                                                                                                                                    var32 = var3.q;
                                                                                                                                                                    var30 = var3.i;
                                                                                                                                                                    var19 = var36.q;
                                                                                                                                                                    bk.i = var36.i;
                                                                                                                                                                    gk.a = var36.a;
                                                                                                                                                                }
                                                                                                                                                            } else {
                                                                                                                                                                b.menuX -= 2;
                                                                                                                                                                if (cs.e[b.menuX] > cs.e[b.menuX + 1]) {
                                                                                                                                                                    var19 += var30[var19];
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        } else {
                                                                                                                                                            b.menuX -= 2;
                                                                                                                                                            if (cs.e[b.menuX] < cs.e[b.menuX + 1]) {
                                                                                                                                                                var19 += var30[var19];
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        b.menuX -= 2;
                                                                                                                                                        if (cs.e[b.menuX] == cs.e[b.menuX + 1]) {
                                                                                                                                                            var19 += var30[var19];
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    b.menuX -= 2;
                                                                                                                                                    if (cs.e[b.menuX] != cs.e[b.menuX + 1]) {
                                                                                                                                                        var19 += var30[var19];
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                var19 += var30[var19];
                                                                                                                                            }
                                                                                                                                        } else {
                                                                                                                                            cs.p[++ly.g - 1] = var3.a[var19];
                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        var21 = var30[var19];
                                                                                                                                        iv.varps[var21] = cs.e[--b.menuX];
                                                                                                                                        b.jg(var21);
                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    var21 = var30[var19];
                                                                                                                                    cs.e[++b.menuX - 1] = iv.varps[var21];
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                cs.e[++b.menuX - 1] = var30[var19];
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            boolean var44;
                                                                                                                            if (var3.i[var19] == 1) {
                                                                                                                                var44 = true;
                                                                                                                            } else {
                                                                                                                                var44 = false;
                                                                                                                            }

                                                                                                                            var23 = bk
                                                                                                                                    .q(var7,
                                                                                                                                            var3,
                                                                                                                                            var44);
                                                                                                                            switch (var23) {
                                                                                                                            case 0:
                                                                                                                                return;
                                                                                                                            case 1:
                                                                                                                            default:
                                                                                                                                break;
                                                                                                                            case 2:
                                                                                                                                throw new IllegalStateException();
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception var28) {
                StringBuilder var27 = new StringBuilder(30);
                var27.append("").append(var3.uid).append(" ");

                for (var10 = cs.n - 1; var10 >= 0; --var10) {
                    var27.append("").append(cs.o[var10].script.uid).append(" ");
                }

                var27.append("").append(var31);
                FloorObject.t(var27.toString(), var28);
            }
        }
    }

    @ObfuscatedName("i")
    public static String i(long var0) {
        if (var0 > 0L && var0 < 6582952005840035281L) {
            if (var0 % 37L == 0L) {
                return null;
            } else {
                int var2 = 0;

                for (long var3 = var0; var3 != 0L; var3 /= 37L) {
                    ++var2;
                }

                StringBuilder var5;
                char var8;
                for (var5 = new StringBuilder(var2); var0 != 0L; var5.append(var8)) {
                    long var6 = var0;
                    var0 /= 37L;
                    var8 = ly.t[(int) (var6 - 37L * var0)];
                    if (var8 == '_') {
                        int var9 = var5.length() - 1;
                        var5.setCharAt(var9, java.lang.Character.toUpperCase(var5.charAt(var9)));
                        var8 = 160;
                    }
                }

                var5.reverse();
                var5.setCharAt(0, java.lang.Character.toUpperCase(var5.charAt(0)));
                return var5.toString();
            }
        } else {
            return null;
        }
    }

    @ObfuscatedName("b")
    static void b(int var0) {
        ItemStorage var1 = (ItemStorage) ItemStorage.t.t((long) var0);
        if (var1 != null) {
            var1.kc();
        }
    }

    @ObfuscatedName("c")
    public static final Sprite createSprite(int var0, int var1, int var2, int var3, int var4, boolean var5) {
        if (var1 == -1) {
            var4 = 0;
        } else if (var4 == 2 && var1 != 1) {
            var4 = 1;
        }

        long var6 = ((long) var3 << 42) + (long) var0 + ((long) var1 << 16) + ((long) var2 << 38) + ((long) var4 << 40);
        Sprite var8;
        if (!var5) {
            var8 = (Sprite) ItemDefinition.o.t(var6);
            if (var8 != null) {
                return var8;
            }
        }

        ItemDefinition var9 = cs.loadItemDefinition(var0);
        if (var1 > 1 && var9.aq != null) {
            int var10 = -1;

            for (int var11 = 0; var11 < 10; ++var11) {
                if (var1 >= var9.aa[var11] && var9.aa[var11] != 0) {
                    var10 = var9.aq[var11];
                }
            }

            if (var10 != -1) {
                var9 = cs.loadItemDefinition(var10);
            }
        }

        Model var19 = var9.p(1);
        if (var19 == null) {
            return null;
        } else {
            Sprite var20 = null;
            if (var9.ak != -1) {
                var20 = createSprite(var9.af, 10, 1, 0, 0, true);
                if (var20 == null) {
                    return null;
                }
            } else if (var9.bm != -1) {
                var20 = createSprite(var9.bc, var1, var2, var3, 0, false);
                if (var20 == null) {
                    return null;
                }
            } else if (var9.bs != -1) {
                var20 = createSprite(var9.bh, var1, 0, 0, 0, false);
                if (var20 == null) {
                    return null;
                }
            }

            int[] var12 = li.ao;
            int var13 = li.av;
            int var14 = li.aj;
            int[] var15 = new int[4];
            li.dp(var15);
            var8 = new Sprite(36, 32);
            li.cf(var8.pixels, 36, 32);
            li.dr();
            eu.t();
            eu.a(16, 16);
            eu.a = false;
            if (var9.bs != -1) {
                var20.u(0, 0);
            }

            int var16 = var9.d;
            if (var5) {
                var16 = (int) ((double) var16 * 1.5D);
            } else if (var2 == 2) {
                var16 = (int) ((double) var16 * 1.04D);
            }

            int var17 = var16 * eu.m[var9.f] >> 16;
            int var18 = var16 * eu.ay[var9.f] >> 16;
            var19.b();
            var19.y(0, var9.r, var9.y, var9.f, var9.h, var19.modelHeight / 2 + var17 + var9.m, var18 + var9.m);
            if (var9.bm != -1) {
                var20.u(0, 0);
            }

            if (var2 >= 1) {
                var8.x(1);
            }

            if (var2 >= 2) {
                var8.x(16777215);
            }

            if (var3 != 0) {
                var8.p(var3);
            }

            li.cf(var8.pixels, 36, 32);
            if (var9.ak != -1) {
                var20.u(0, 0);
            }

            if (var4 == 1 || var4 == 2 && var9.ay == 1) {
                dt.c.s(Client.u(var1), 0, 9, 16776960, 1);
            }

            if (!var5) {
                ItemDefinition.o.i(var8, var6);
            }

            li.cf(var12, var13, var14);
            li.da(var15);
            eu.t();
            eu.a = true;
            return var8;
        }
    }

    @ObfuscatedName("gr")
    static final void gr(int var0, int var1, int var2) {
        if (var0 >= 128 && var1 >= 128 && var0 <= 13056 && var1 <= 13056) {
            int var3 = ef.gn(var0, var1, kt.ii) - var2;
            var0 -= RuneScriptVM.go;
            var3 -= fk.gs;
            var1 -= n.gp;
            int var4 = eu.m[ap.gi];
            int var5 = eu.ay[ap.gi];
            int var6 = eu.m[bt.cameraYaw];
            int var7 = eu.ay[bt.cameraYaw];
            int var8 = var6 * var1 + var0 * var7 >> 16;
            var1 = var7 * var1 - var0 * var6 >> 16;
            var0 = var8;
            var8 = var3 * var5 - var4 * var1 >> 16;
            var1 = var4 * var3 + var5 * var1 >> 16;
            if (var1 >= 50) {
                Client.hv = var0 * Client.viewportScale / var1 + Client.viewportWidth / 2;
                Client.hd = var8 * Client.viewportScale / var1 + Client.viewportHeight / 2;
            } else {
                Client.hv = -1;
                Client.hd = -1;
            }

        } else {
            Client.hv = -1;
            Client.hd = -1;
        }
    }
}
