import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dr")
public class TaskData {

    @ObfuscatedName("l")
    public static int l;

    @ObfuscatedName("b")
    public static boolean b;

    @ObfuscatedName("g")
    static dh g;

    @ObfuscatedName("c")
    static cf c;

    @ObfuscatedName("v")
    protected int[] v;

    @ObfuscatedName("u")
    TaskDataNode dataNode;

    @ObfuscatedName("j")
    int j = 32;

    @ObfuscatedName("k")
    long k = au.t();

    @ObfuscatedName("z")
    int z;

    @ObfuscatedName("w")
    int w;

    @ObfuscatedName("s")
    int s;

    @ObfuscatedName("d")
    long d = 0L;

    @ObfuscatedName("f")
    int f = 0;

    @ObfuscatedName("r")
    int r = 0;

    @ObfuscatedName("y")
    int y = 0;

    @ObfuscatedName("h")
    long h = 0L;

    @ObfuscatedName("m")
    boolean m = true;

    @ObfuscatedName("am")
    int am = 0;

    @ObfuscatedName("az")
    TaskDataNode[] cachedDataNodes = new TaskDataNode[8];

    @ObfuscatedName("ap")
    TaskDataNode[] dataNodes = new TaskDataNode[8];

    @ObfuscatedName("t")
    protected void t()
            throws Exception {
    }

    @ObfuscatedName("q")
    protected void q(int var1)
            throws Exception {
    }

    @ObfuscatedName("i")
    protected int i()
            throws Exception {
        return this.z;
    }

    @ObfuscatedName("a")
    protected void a()
            throws Exception {
    }

    @ObfuscatedName("l")
    protected void l() {
    }

    @ObfuscatedName("b")
    protected void b()
            throws Exception {
    }

    @ObfuscatedName("az")
    public final synchronized void az(TaskDataNode var1) {
        this.dataNode = var1;
    }

    @ObfuscatedName("ap")
    public final synchronized void ap() {
        if (this.v != null) {
            long var1 = au.t();

            try {
                if (this.d != 0L) {
                    if (var1 < this.d) {
                        return;
                    }

                    this.q(this.z);
                    this.d = 0L;
                    this.m = true;
                }

                int var3 = this.i();
                if (this.y - var3 > this.f) {
                    this.f = this.y - var3;
                }

                int var4 = this.w + this.s;
                if (var4 + 256 > 16384) {
                    var4 = 16128;
                }

                if (var4 + 256 > this.z) {
                    this.z += 1024;
                    if (this.z > 16384) {
                        this.z = 16384;
                    }

                    this.l();
                    this.q(this.z);
                    var3 = 0;
                    this.m = true;
                    if (var4 + 256 > this.z) {
                        var4 = this.z - 256;
                        this.s = var4 - this.w;
                    }
                }

                while (var3 < var4) {
                    this.an(this.v, 256);
                    this.a();
                    var3 += 256;
                }

                if (var1 > this.h) {
                    if (!this.m) {
                        if (this.f == 0 && this.r == 0) {
                            this.l();
                            this.d = var1 + 2000L;
                            return;
                        }

                        this.s = Math.min(this.r, this.f);
                        this.r = this.f;
                    } else {
                        this.m = false;
                    }

                    this.f = 0;
                    this.h = var1 + 2000L;
                }

                this.y = var3;
            } catch (Exception var7) {
                this.l();
                this.d = 2000L + var1;
            }

            try {
                if (var1 > 500000L + this.k) {
                    var1 = this.k;
                }

                while (var1 > 5000L + this.k) {
                    this.ar(256);
                    this.k += (long) (256000 / l);
                }
            } catch (Exception var6) {
                this.k = var1;
            }

        }
    }

    @ObfuscatedName("ah")
    public final void ah() {
        this.m = true;
    }

    @ObfuscatedName("au")
    public final synchronized void au() {
        this.m = true;

        try {
            this.b();
        } catch (Exception var2) {
            this.l();
            this.d = au.t() + 2000L;
        }

    }

    @ObfuscatedName("ax")
    public final synchronized void ax() {
        if (g != null) {
            boolean var1 = true;

            for (int var2 = 0; var2 < 2; ++var2) {
                if (this == g.t[var2]) {
                    g.t[var2] = null;
                }

                if (g.t[var2] != null) {
                    var1 = false;
                }
            }

            if (var1) {
                CombatBarData.x.shutdownNow();
                CombatBarData.x = null;
                g = null;
            }
        }

        this.l();
        this.v = null;
    }

    @ObfuscatedName("ar")
    final void ar(int var1) {
        this.am -= var1;
        if (this.am < 0) {
            this.am = 0;
        }

        if (this.dataNode != null) {
            this.dataNode.c(var1);
        }

    }

    @ObfuscatedName("an")
    final void an(int[] var1, int var2) {
        int var3 = var2;
        if (b) {
            var3 = var2 << 1;
        }

        gj.o(var1, 0, var3);
        this.am -= var2;
        if (this.dataNode != null && this.am <= 0) {
            this.am += l >> 4;
            cn.ai(this.dataNode);
            this.al(this.dataNode, this.dataNode.aw());
            int var4 = 0;
            int var5 = 255;

            int var6;
            TaskDataNode var10;
            label104: for (var6 = 7; var5 != 0; --var6) {
                int var7;
                int var8;
                if (var6 < 0) {
                    var7 = var6 & 3;
                    var8 = -(var6 >> 2);
                } else {
                    var7 = var6;
                    var8 = 0;
                }

                for (int var9 = var5 >>> var7 & 286331153; var9 != 0; var9 >>>= 4) {
                    if ((var9 & 1) != 0) {
                        var5 &= ~(1 << var7);
                        var10 = null;
                        TaskDataNode var11 = this.cachedDataNodes[var7];

                        label98: while (true) {
                            while (true) {
                                if (var11 == null) {
                                    break label98;
                                }

                                IntegerNode var12 = var11.integerNode;
                                if (var12 != null && var12.number > var8) {
                                    var5 |= 1 << var7;
                                    var10 = var11;
                                    var11 = var11.node;
                                } else {
                                    var11.hasArray = true;
                                    int var13 = var11.x();
                                    var4 += var13;
                                    if (var12 != null) {
                                        var12.number += var13;
                                    }

                                    if (var4 >= this.j) {
                                        break label104;
                                    }

                                    TaskDataNode var14 = var11.b();
                                    if (var14 != null) {
                                        for (int var15 = var11.taskUid; var14 != null; var14 = var11.e()) {
                                            this.al(var14, var15 * var14.aw() >> 8);
                                        }
                                    }

                                    TaskDataNode var18 = var11.node;
                                    var11.node = null;
                                    if (var10 == null) {
                                        this.cachedDataNodes[var7] = var18;
                                    } else {
                                        var10.node = var18;
                                    }

                                    if (var18 == null) {
                                        this.dataNodes[var7] = var10;
                                    }

                                    var11 = var18;
                                }
                            }
                        }
                    }

                    var7 += 4;
                    ++var8;
                }
            }

            for (var6 = 0; var6 < 8; ++var6) {
                TaskDataNode var16 = this.cachedDataNodes[var6];
                TaskDataNode[] var17 = this.cachedDataNodes;
                this.dataNodes[var6] = null;

                for (var17[var6] = null; var16 != null; var16 = var10) {
                    var10 = var16.node;
                    var16.node = null;
                }
            }
        }

        if (this.am < 0) {
            this.am = 0;
        }

        if (this.dataNode != null) {
            this.dataNode.p(var1, 0, var2);
        }

        this.k = au.t();
    }

    @ObfuscatedName("al")
    final void al(TaskDataNode var1, int var2) {
        int var3 = var2 >> 5;
        TaskDataNode var4 = this.dataNodes[var3];
        if (var4 == null) {
            this.cachedDataNodes[var3] = var1;
        } else {
            var4.node = var1;
        }

        this.dataNodes[var3] = var1;
        var1.taskUid = var2;
    }

    @ObfuscatedName("t")
    public static void t(FileSystem var0, FileSystem var1, boolean var2) {
        ObjectDefinition.q = var0;
        f.i = var1;
        ObjectDefinition.t = var2;
    }

    @ObfuscatedName("q")
    public static km q(FileSystem var0, FileSystem var1, int var2, int var3) {
        byte[] var5 = var0.i(var2, var3);
        boolean var4;
        if (var5 == null) {
            var4 = false;
        } else {
            RTComponent.k(var5);
            var4 = true;
        }

        if (!var4) {
            return null;
        } else {
            byte[] var6 = var1.i(var2, var3);
            km var8;
            if (var6 == null) {
                var8 = null;
            } else {
                km var7 = new km(var6, ci.a, er.l, le.b, GrandExchangeOffer.e, le.x, ClassStructureNode.p);
                ly.z();
                var8 = var7;
            }

            return var8;
        }
    }

    @ObfuscatedName("b")
    static int b(int var0, RuneScript var1, boolean var2) {
        RTComponent var3;
        if (var0 >= 2000) {
            var0 -= 1000;
            var3 = Inflater.t(cs.e[--b.menuX]);
        } else {
            var3 = var2 ? ho.v : cs.c;
        }

        GameEngine.jk(var3);
        if (var0 != 1200 && var0 != 1205 && var0 != 1212) {
            if (var0 == 1201) {
                var3.entityType = 2;
                var3.entityId = cs.e[--b.menuX];
                return 1;
            } else if (var0 == 1202) {
                var3.entityType = 3;
                var3.entityId = az.il.composite.p();
                return 1;
            } else {
                return 2;
            }
        } else {
            b.menuX -= 2;
            int var4 = cs.e[b.menuX];
            int var5 = cs.e[b.menuX + 1];
            var3.itemId = var4;
            var3.itemQuantity = var5;
            ItemDefinition var6 = cs.loadItemDefinition(var4);
            var3.bx = var6.f;
            var3.bf = var6.r;
            var3.bo = var6.y;
            var3.bq = var6.h;
            var3.bz = var6.m;
            var3.bv = var6.d;
            if (var0 == 1205) {
                var3.bw = 0;
            } else if (var0 == 1212 | var6.ay == 1) {
                var3.bw = 1;
            } else {
                var3.bw = 2;
            }

            if (var3.bi > 0) {
                var3.bv = var3.bv * 32 / var3.bi;
            } else if (var3.av > 0) {
                var3.bv = var3.bv * 32 / var3.av;
            }

            return 1;
        }
    }

    @ObfuscatedName("fw")
    static final void fw() {
        if (RuneScriptStackItem.oi) {
            if (ad.ot != null) {
                ad.ot.aj();
            }

            i.jz();
            RuneScriptStackItem.oi = false;
        }

    }
}
