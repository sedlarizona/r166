import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ds")
public class ds extends Node {

    @ObfuscatedName("u")
    static int[] u;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    boolean b;

    @ObfuscatedName("e")
    int[] e;

    @ObfuscatedName("x")
    int[] x;

    @ObfuscatedName("p")
    int[] p;

    @ObfuscatedName("g")
    int[] g;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int[] c;

    @ObfuscatedName("v")
    boolean v = false;

    ds(ByteBuffer var1) {
        this.l = var1.ae();
        this.b = var1.av() == 1;
        int var2 = var1.av();
        if (var2 >= 1 && var2 <= 4) {
            this.e = new int[var2];

            int var3;
            for (var3 = 0; var3 < var2; ++var3) {
                this.e[var3] = var1.ae();
            }

            if (var2 > 1) {
                this.x = new int[var2 - 1];

                for (var3 = 0; var3 < var2 - 1; ++var3) {
                    this.x[var3] = var1.av();
                }
            }

            if (var2 > 1) {
                this.p = new int[var2 - 1];

                for (var3 = 0; var3 < var2 - 1; ++var3) {
                    this.p[var3] = var1.av();
                }
            }

            this.g = new int[var2];

            for (var3 = 0; var3 < var2; ++var3) {
                this.g[var3] = var1.ap();
            }

            this.n = var1.av();
            this.o = var1.av();
            this.c = null;
        } else {
            throw new RuntimeException();
        }
    }

    @ObfuscatedName("t")
    boolean t(double var1, int var3, FileSystem var4) {
        int var5;
        for (var5 = 0; var5 < this.e.length; ++var5) {
            if (var4.u(this.e[var5]) == null) {
                return false;
            }
        }

        var5 = var3 * var3;
        this.c = new int[var5];

        for (int var6 = 0; var6 < this.e.length; ++var6) {
            lk var7 = jl.e(var4, this.e[var6]);
            var7.t();
            byte[] var8 = var7.t;
            int[] var9 = var7.q;
            int var10 = this.g[var6];
            if ((var10 & -16777216) == 16777216) {
                ;
            }

            if ((var10 & -16777216) == 33554432) {
                ;
            }

            int var11;
            int var12;
            int var13;
            int var14;
            if ((var10 & -16777216) == 50331648) {
                var11 = var10 & 16711935;
                var12 = var10 >> 8 & 255;

                for (var13 = 0; var13 < var9.length; ++var13) {
                    var14 = var9[var13];
                    if (var14 >> 8 == (var14 & '\uffff')) {
                        var14 &= 255;
                        var9[var13] = var11 * var14 >> 8 & 16711935 | var12 * var14 & '\uff00';
                    }
                }
            }

            for (var11 = 0; var11 < var9.length; ++var11) {
                var9[var11] = eu.x(var9[var11], var1);
            }

            if (var6 == 0) {
                var11 = 0;
            } else {
                var11 = this.x[var6 - 1];
            }

            if (var11 == 0) {
                if (var3 == var7.i) {
                    for (var12 = 0; var12 < var5; ++var12) {
                        this.c[var12] = var9[var8[var12] & 255];
                    }
                } else if (var7.i == 64 && var3 == 128) {
                    var12 = 0;

                    for (var13 = 0; var13 < var3; ++var13) {
                        for (var14 = 0; var14 < var3; ++var14) {
                            this.c[var12++] = var9[var8[(var13 >> 1 << 6) + (var14 >> 1)] & 255];
                        }
                    }
                } else {
                    if (var7.i != 128 || var3 != 64) {
                        throw new RuntimeException();
                    }

                    var12 = 0;

                    for (var13 = 0; var13 < var3; ++var13) {
                        for (var14 = 0; var14 < var3; ++var14) {
                            this.c[var12++] = var9[var8[(var14 << 1) + (var13 << 1 << 7)] & 255];
                        }
                    }
                }
            }

            if (var11 == 1) {
                ;
            }

            if (var11 == 2) {
                ;
            }

            if (var11 == 3) {
                ;
            }
        }

        return true;
    }

    @ObfuscatedName("q")
    void q() {
        this.c = null;
    }

    @ObfuscatedName("i")
    void i(int var1) {
        if (this.c != null) {
            short var2;
            int var3;
            int var4;
            int var5;
            int var6;
            int var7;
            int[] var10;
            if (this.n == 1 || this.n == 3) {
                if (u == null || u.length < this.c.length) {
                    u = new int[this.c.length];
                }

                if (this.c.length == 4096) {
                    var2 = 64;
                } else {
                    var2 = 128;
                }

                var3 = this.c.length;
                var4 = var2 * this.o * var1;
                var5 = var3 - 1;
                if (this.n == 1) {
                    var4 = -var4;
                }

                for (var6 = 0; var6 < var3; ++var6) {
                    var7 = var6 + var4 & var5;
                    u[var6] = this.c[var7];
                }

                var10 = this.c;
                this.c = u;
                u = var10;
            }

            if (this.n == 2 || this.n == 4) {
                if (u == null || u.length < this.c.length) {
                    u = new int[this.c.length];
                }

                if (this.c.length == 4096) {
                    var2 = 64;
                } else {
                    var2 = 128;
                }

                var3 = this.c.length;
                var4 = this.o * var1;
                var5 = var2 - 1;
                if (this.n == 2) {
                    var4 = -var4;
                }

                for (var6 = 0; var6 < var3; var6 += var2) {
                    for (var7 = 0; var7 < var2; ++var7) {
                        int var8 = var6 + var7;
                        int var9 = var6 + (var7 + var4 & var5);
                        u[var8] = this.c[var9];
                    }
                }

                var10 = this.c;
                this.c = u;
                u = var10;
            }

        }
    }
}
