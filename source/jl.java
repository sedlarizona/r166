import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jl")
public class jl extends hh {

    @ObfuscatedName("t")
    static FileSystem t;

    @ObfuscatedName("q")
    static Cache q = new Cache(64);

    @ObfuscatedName("ac")
    static int ac;

    @ObfuscatedName("i")
    FixedSizeDeque i;

    @ObfuscatedName("i")
    void i() {
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.l(var1, var2);
        }
    }

    @ObfuscatedName("l")
    void l(ByteBuffer var1, int var2) {
        if (var2 == 249) {
            this.i = i.t(var1, this.i);
        }

    }

    @ObfuscatedName("b")
    public int b(int var1, int var2) {
        return ky.q(this.i, var1, var2);
    }

    @ObfuscatedName("e")
    public String e(int var1, String var2) {
        FixedSizeDeque var4 = this.i;
        String var3;
        if (var4 == null) {
            var3 = var2;
        } else {
            gy var5 = (gy) var4.t((long) var1);
            if (var5 == null) {
                var3 = var2;
            } else {
                var3 = (String) var5.t;
            }
        }

        return var3;
    }

    @ObfuscatedName("e")
    public static lk e(FileSystem var0, int var1) {
        if (!AreaSoundEmitter.u(var0, var1)) {
            return null;
        } else {
            lk var3 = new lk();
            var3.e = le.q;
            var3.x = le.i;
            var3.l = ci.a[0];
            var3.b = er.l[0];
            var3.i = le.b[0];
            var3.a = GrandExchangeOffer.e[0];
            var3.q = le.x;
            var3.t = ClassStructureNode.p[0];
            ly.z();
            return var3;
        }
    }

    @ObfuscatedName("gq")
    static boolean gq() {
        return (Client.jt & 2) != 0;
    }
}
