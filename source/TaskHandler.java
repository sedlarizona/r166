import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fd")
public class TaskHandler implements Runnable {

    @ObfuscatedName("t")
    public static String t;

    @ObfuscatedName("q")
    public static String q;

    @ObfuscatedName("v")
    public static jk v;

    @ObfuscatedName("i")
    Task task = null;

    @ObfuscatedName("a")
    Task cachedTask = null;

    @ObfuscatedName("l")
    Thread thread;

    @ObfuscatedName("b")
    boolean killed = false;

    public TaskHandler() {
        t = "Unknown";
        q = "1.6";

        try {
            t = System.getProperty("java.vendor");
            q = System.getProperty("java.version");
        } catch (Exception var2) {
            ;
        }

        this.killed = false;
        this.thread = new Thread(this);
        this.thread.setPriority(10);
        this.thread.setDaemon(true);
        this.thread.start();
    }

    @ObfuscatedName("t")
    public final void t() {
        synchronized (this) {
            this.killed = true;
            this.notifyAll();
        }

        try {
            this.thread.join();
        } catch (InterruptedException var3) {
            ;
        }

    }

    @ObfuscatedName("q")
    final Task q(int var1, int var2, int var3, Object var4) {
        Task var5 = new Task();
        var5.b = var1;
        var5.e = var2;
        var5.x = var4;
        synchronized (this) {
            if (this.cachedTask != null) {
                this.cachedTask.task = var5;
                this.cachedTask = var5;
            } else {
                this.cachedTask = this.task = var5;
            }

            this.notify();
            return var5;
        }
    }

    @ObfuscatedName("i")
    public final Task i(String var1, int var2) {
        return this.q(1, var2, 0, var1);
    }

    @ObfuscatedName("a")
    public final Task a(Runnable var1, int var2) {
        return this.q(2, var2, 0, var1);
    }

    public final void run() {
        while (true) {
            Task var1;
            synchronized (this) {
                while (true) {
                    if (this.killed) {
                        return;
                    }

                    if (this.task != null) {
                        var1 = this.task;
                        this.task = this.task.task;
                        if (this.task == null) {
                            this.cachedTask = null;
                        }
                        break;
                    }

                    try {
                        this.wait();
                    } catch (InterruptedException var8) {
                        ;
                    }
                }
            }

            try {
                int var5 = var1.b;
                if (var5 == 1) {
                    var1.p = new Socket(InetAddress.getByName((String) var1.x), var1.e);
                } else if (var5 == 2) {
                    Thread var3 = new Thread((Runnable) var1.x);
                    var3.setDaemon(true);
                    var3.start();
                    var3.setPriority(var1.e);
                    var1.p = var3;
                } else if (var5 == 4) {
                    var1.p = new DataInputStream(((URL) var1.x).openStream());
                }

                var1.l = 1;
            } catch (ThreadDeath var6) {
                throw var6;
            } catch (Throwable var7) {
                var1.l = 2;
            }
        }
    }

    @ObfuscatedName("q")
    public static void q(fy var0, boolean var1) {
        if (bg.t != null) {
            try {
                bg.t.b();
            } catch (Exception var6) {
                ;
            }

            bg.t = null;
        }

        bg.t = var0;
        in.t(var1);
        jg.u.index = 0;
        v = null;
        RSRandomAccessFileChannel.j = null;
        jg.k = 0;

        while (true) {
            jk var2 = (jk) jg.b.a();
            if (var2 == null) {
                while (true) {
                    var2 = (jk) jg.n.a();
                    if (var2 == null) {
                        if (jg.f != 0) {
                            try {
                                ByteBuffer var7 = new ByteBuffer(4);
                                var7.a(4);
                                var7.a(jg.f);
                                var7.l(0);
                                bg.t.l(var7.buffer, 0, 4);
                            } catch (IOException var5) {
                                try {
                                    bg.t.b();
                                } catch (Exception var4) {
                                    ;
                                }

                                ++jg.y;
                                bg.t = null;
                            }
                        }

                        jg.q = 0;
                        ea.i = au.t();
                        return;
                    }

                    jg.x.q(var2);
                    jg.p.q(var2, var2.uid);
                    ++jg.g;
                    --jg.o;
                }
            }

            jg.a.q(var2, var2.uid);
            ++jg.l;
            --jg.e;
        }
    }

    @ObfuscatedName("i")
    static void i(ju var0, int var1, int var2, int var3, byte var4, boolean var5) {
        long var6 = (long) ((var1 << 16) + var2);
        jk var8 = (jk) jg.a.t(var6);
        if (var8 == null) {
            var8 = (jk) jg.b.t(var6);
            if (var8 == null) {
                var8 = (jk) jg.p.t(var6);
                if (var8 != null) {
                    if (var5) {
                        var8.ce();
                        jg.a.q(var8, var6);
                        --jg.g;
                        ++jg.l;
                    }

                } else {
                    if (!var5) {
                        var8 = (jk) jg.n.t(var6);
                        if (var8 != null) {
                            return;
                        }
                    }

                    var8 = new jk();
                    var8.t = var0;
                    var8.q = var3;
                    var8.i = var4;
                    if (var5) {
                        jg.a.q(var8, var6);
                        ++jg.l;
                    } else {
                        jg.x.t(var8);
                        jg.p.q(var8, var6);
                        ++jg.g;
                    }

                }
            }
        }
    }

    @ObfuscatedName("a")
    static int a(char var0, int var1) {
        int var2 = var0 << 4;
        if (java.lang.Character.isUpperCase(var0) || java.lang.Character.isTitleCase(var0)) {
            var0 = java.lang.Character.toLowerCase(var0);
            var2 = (var0 << 4) + 1;
        }

        return var2;
    }

    @ObfuscatedName("u")
    static int u(int var0, RuneScript var1, boolean var2) {
        RTComponent var3 = Inflater.t(cs.e[--b.menuX]);
        if (var0 == 2600) {
            cs.e[++b.menuX - 1] = var3.horizontalScrollbarPosition;
            return 1;
        } else if (var0 == 2601) {
            cs.e[++b.menuX - 1] = var3.verticalScrollbarPosition;
            return 1;
        } else if (var0 == 2602) {
            cs.p[++ly.g - 1] = var3.text;
            return 1;
        } else if (var0 == 2603) {
            cs.e[++b.menuX - 1] = var3.al;
            return 1;
        } else if (var0 == 2604) {
            cs.e[++b.menuX - 1] = var3.at;
            return 1;
        } else if (var0 == 2605) {
            cs.e[++b.menuX - 1] = var3.bv;
            return 1;
        } else if (var0 == 2606) {
            cs.e[++b.menuX - 1] = var3.bx;
            return 1;
        } else if (var0 == 2607) {
            cs.e[++b.menuX - 1] = var3.bo;
            return 1;
        } else if (var0 == 2608) {
            cs.e[++b.menuX - 1] = var3.bf;
            return 1;
        } else if (var0 == 2609) {
            cs.e[++b.menuX - 1] = var3.alpha;
            return 1;
        } else if (var0 == 2610) {
            cs.e[++b.menuX - 1] = var3.ab;
            return 1;
        } else if (var0 == 2611) {
            cs.e[++b.menuX - 1] = var3.color;
            return 1;
        } else if (var0 == 2612) {
            cs.e[++b.menuX - 1] = var3.as;
            return 1;
        } else if (var0 == 2613) {
            cs.e[++b.menuX - 1] = var3.af.t();
            return 1;
        } else {
            return 2;
        }
    }
}
