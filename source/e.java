import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("e")
public class e implements gl, mq {
   @ObfuscatedName("t")
   public static final e t;
   @ObfuscatedName("q")
   static final e q;
   @ObfuscatedName("i")
   static final e i;
   @ObfuscatedName("a")
   static final e a;
   @ObfuscatedName("l")
   static final e l;
   @ObfuscatedName("b")
   static final e b;
   @ObfuscatedName("e")
   static final e e;
   @ObfuscatedName("x")
   static final e x;
   @ObfuscatedName("p")
   static final e p;
   @ObfuscatedName("g")
   static final e g;
   @ObfuscatedName("n")
   static final e n;
   @ObfuscatedName("o")
   static final e o;
   @ObfuscatedName("c")
   static final e c;
   @ObfuscatedName("v")
   static final e v;
   @ObfuscatedName("u")
   static final e u;
   @ObfuscatedName("j")
   static final e j;
   @ObfuscatedName("k")
   static final e k;
   @ObfuscatedName("z")
   static final e z;
   @ObfuscatedName("w")
   static final e w;
   @ObfuscatedName("s")
   static final e s;
   @ObfuscatedName("d")
   static final e d;
   @ObfuscatedName("f")
   static final e f;
   @ObfuscatedName("r")
   static final e r;
   @ObfuscatedName("y")
   static final e y;
   @ObfuscatedName("h")
   static final e h;
   @ObfuscatedName("m")
   static final e m;
   @ObfuscatedName("ay")
   static final e ay;
   @ObfuscatedName("ao")
   static final e ao;
   @ObfuscatedName("av")
   static final e av;
   @ObfuscatedName("aj")
   static final e aj;
   @ObfuscatedName("ae")
   static final e ae;
   @ObfuscatedName("am")
   static final e am;
   @ObfuscatedName("az")
   static final e az;
   @ObfuscatedName("ap")
   static final e ap;
   @ObfuscatedName("ah")
   static final e ah;
   @ObfuscatedName("au")
   static final e au;
   @ObfuscatedName("ax")
   static final e ax;
   @ObfuscatedName("ar")
   static final e ar;
   @ObfuscatedName("an")
   static final e an;
   @ObfuscatedName("ai")
   static final e ai;
   @ObfuscatedName("al")
   static final e al;
   @ObfuscatedName("at")
   static final e at;
   @ObfuscatedName("ag")
   static final e ag;
   @ObfuscatedName("as")
   static final e as;
   @ObfuscatedName("aw")
   static final e aw;
   @ObfuscatedName("aq")
   static final e aq;
   @ObfuscatedName("aa")
   static final e aa;
   @ObfuscatedName("af")
   static final e af;
   @ObfuscatedName("ak")
   static final e ak;
   @ObfuscatedName("ab")
   static final e ab;
   @ObfuscatedName("ac")
   static final e ac;
   @ObfuscatedName("ad")
   static final e ad;
   @ObfuscatedName("bg")
   static final e bg;
   @ObfuscatedName("br")
   static final e br;
   @ObfuscatedName("ba")
   static final e ba;
   @ObfuscatedName("bk")
   static final e bk;
   @ObfuscatedName("be")
   static final e be;
   @ObfuscatedName("bc")
   static final e bc;
   @ObfuscatedName("bm")
   static final e bm;
   @ObfuscatedName("bh")
   static final e bh;
   @ObfuscatedName("bs")
   static final e bs;
   @ObfuscatedName("bj")
   static final e bj;
   @ObfuscatedName("bt")
   static final e bt;
   @ObfuscatedName("by")
   static final e by;
   @ObfuscatedName("bn")
   static final e bn;
   @ObfuscatedName("bb")
   static final e bb;
   @ObfuscatedName("bq")
   static final e bq;
   @ObfuscatedName("bz")
   static final e bz;
   @ObfuscatedName("bx")
   static final e bx;
   @ObfuscatedName("bf")
   static final e bf;
   @ObfuscatedName("bo")
   static final e bo;
   @ObfuscatedName("bv")
   static final e bv;
   @ObfuscatedName("bi")
   static final e bi;
   @ObfuscatedName("bu")
   static final e bu;
   @ObfuscatedName("bl")
   static final e bl;
   @ObfuscatedName("bw")
   static final e bw;
   @ObfuscatedName("bp")
   static final e bp;
   @ObfuscatedName("bd")
   static final e bd;
   @ObfuscatedName("cb")
   static final e cb;
   @ObfuscatedName("cm")
   static final e cm;
   @ObfuscatedName("cu")
   static final e cu;
   @ObfuscatedName("cs")
   static final e cs;
   @ObfuscatedName("ct")
   static final e ct;
   @ObfuscatedName("cw")
   static final e cw;
   @ObfuscatedName("ch")
   static final e ch;
   @ObfuscatedName("cr")
   static final e cr;
   @ObfuscatedName("co")
   static final e co;
   @ObfuscatedName("cv")
   static final e cv;
   @ObfuscatedName("cd")
   static final e cd;
   @ObfuscatedName("cq")
   static final e cq;
   @ObfuscatedName("ci")
   static final e ci;
   @ObfuscatedName("cc")
   static final e cc;
   @ObfuscatedName("ce")
   static final e ce;
   @ObfuscatedName("cx")
   static final e cx;
   @ObfuscatedName("cy")
   static final e cy;
   @ObfuscatedName("cg")
   static final e cg;
   @ObfuscatedName("cj")
   static final e cj;
   @ObfuscatedName("cl")
   static final e cl;
   @ObfuscatedName("cz")
   static final e cz;
   @ObfuscatedName("cn")
   static final e cn;
   @ObfuscatedName("ca")
   static final e ca;
   @ObfuscatedName("cf")
   static final e cf;
   @ObfuscatedName("cp")
   static final e cp;
   @ObfuscatedName("ck")
   static final e ck;
   @ObfuscatedName("db")
   static final e db;
   @ObfuscatedName("dp")
   static final e dp;
   @ObfuscatedName("da")
   static final e da;
   @ObfuscatedName("dr")
   static final e dr;
   @ObfuscatedName("dj")
   static final e dj;
   @ObfuscatedName("dh")
   static final e dh;
   @ObfuscatedName("dc")
   static final e dc;
   @ObfuscatedName("dl")
   static final e dl;
   @ObfuscatedName("df")
   static final e df;
   @ObfuscatedName("dq")
   static final e dq;
   @ObfuscatedName("dt")
   static final e dt;
   @ObfuscatedName("dy")
   static final e dy;
   @ObfuscatedName("dn")
   static final e dn;
   @ObfuscatedName("do")
   static final e do;
   @ObfuscatedName("dw")
   static final e dw;
   @ObfuscatedName("dd")
   static final e dd;
   @ObfuscatedName("du")
   static final e du;
   @ObfuscatedName("dk")
   static final e dk;
   @ObfuscatedName("de")
   static final e de;
   @ObfuscatedName("dg")
   static final e dg;
   @ObfuscatedName("dx")
   static final e dx;
   @ObfuscatedName("di")
   static final e di;
   @ObfuscatedName("dv")
   static final e dv;
   @ObfuscatedName("dz")
   static final e dz;
   @ObfuscatedName("ds")
   static final e ds;
   @ObfuscatedName("dm")
   static final e dm;
   @ObfuscatedName("eb")
   static final e eb;
   @ObfuscatedName("ek")
   static final e ek;
   @ObfuscatedName("ej")
   static final e ej;
   @ObfuscatedName("ee")
   static final e ee;
   @ObfuscatedName("eu")
   static final e eu;
   @ObfuscatedName("ei")
   static final e ei;
   @ObfuscatedName("ed")
   static final e ed;
   @ObfuscatedName("ew")
   static final e ew;
   @ObfuscatedName("ey")
   static final e ey;
   @ObfuscatedName("en")
   static final e en;
   @ObfuscatedName("eg")
   static final e eg;
   @ObfuscatedName("ez")
   static final e ez;
   @ObfuscatedName("et")
   static final e et;
   @ObfuscatedName("ev")
   static final e ev;
   @ObfuscatedName("es")
   static final e es;
   @ObfuscatedName("el")
   static final e el;
   @ObfuscatedName("eo")
   static final e eo;
   @ObfuscatedName("ec")
   static final e ec;
   @ObfuscatedName("ep")
   static final e ep;
   @ObfuscatedName("ef")
   static final e ef;
   @ObfuscatedName("eq")
   static final e eq;
   @ObfuscatedName("ea")
   static final e ea;
   @ObfuscatedName("em")
   static final e em;
   @ObfuscatedName("er")
   static final e er;
   @ObfuscatedName("eh")
   static final e eh;
   @ObfuscatedName("ex")
   static final e ex;
   @ObfuscatedName("fg")
   static final e fg;
   @ObfuscatedName("fp")
   static final e fp;
   @ObfuscatedName("ff")
   static final e ff;
   @ObfuscatedName("fe")
   static final e fe;
   @ObfuscatedName("fk")
   static final e fk;
   @ObfuscatedName("fu")
   static final e fu;
   @ObfuscatedName("fz")
   static final e fz;
   @ObfuscatedName("fl")
   static final e fl;
   @ObfuscatedName("ft")
   static final e ft;
   @ObfuscatedName("fb")
   static final e fb;
   @ObfuscatedName("fd")
   static final e fd;
   @ObfuscatedName("fh")
   static final e fh;
   @ObfuscatedName("fs")
   static final e fs;
   @ObfuscatedName("fy")
   static final e fy;
   @ObfuscatedName("fw")
   static final e fw;
   @ObfuscatedName("fm")
   static final e fm;
   @ObfuscatedName("fr")
   static final e fr;
   @ObfuscatedName("fi")
   static final e fi;
   @ObfuscatedName("fn")
   static final e fn;
   @ObfuscatedName("fj")
   static final e fj;
   @ObfuscatedName("fa")
   static final e fa;
   @ObfuscatedName("fc")
   static final e fc;
   @ObfuscatedName("fv")
   static final e fv;
   @ObfuscatedName("fq")
   static final e fq;
   @ObfuscatedName("fx")
   static final e fx;
   @ObfuscatedName("fo")
   static final e fo;
   @ObfuscatedName("gu")
   static final e gu;
   @ObfuscatedName("ga")
   static final e ga;
   @ObfuscatedName("gd")
   static final e gd;
   @ObfuscatedName("gs")
   static e[] gs;
   @ObfuscatedName("gg")
   final int gg;
   @ObfuscatedName("go")
   final char go;

   static {
      t = new e(0, 'i', l.t, 0, new q[]{q.t, q.q, q.i, q.a, q.l, q.b, q.e});
      q = new e(1, '1', l.t, 0, new q[]{q.t, q.q, q.i, q.a, q.l, q.b, q.e});
      i = new e(2, '2', l.t, -1, new q[]{q.t, q.q});
      a = new e(3, ':', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      l = new e(4, ';', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      b = new e(5, '@', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      e = new e(6, 'A', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      x = new e(7, 'C', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      p = new e(8, 'H', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      g = new e(9, 'I', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      n = new e(10, 'K', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      o = new e(11, 'M', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      c = new e(12, 'N', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      v = new e(13, 'O', l.t, -1, new q[0]);
      u = new e(14, 'P', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      j = new e(15, 'Q', l.t, -1, new q[]{q.t, q.q});
      k = new e(16, 'R', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      z = new e(17, 'S', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.e, q.b});
      w = new e(18, 'T', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      s = new e(19, 'V', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      d = new e(20, '^', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      f = new e(21, '`', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.b});
      r = new e(22, 'c', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.l, q.b, q.e});
      y = new e(23, 'd', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      h = new e(24, 'e', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      m = new e(25, 'f', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      ay = new e(26, 'g', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      ao = new e(27, 'h', l.t, -1, new q[]{q.t, q.q, q.i, q.a});
      av = new e(28, 'j', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      aj = new e(29, 'k', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      ae = new e(30, 'l', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      am = new e(31, 'm', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      az = new e(32, 'n', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.e, q.b});
      ap = new e(33, 'o', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.l, q.b, q.e});
      ah = new e(34, 'p', l.t, -1, new q[]{q.t, q.q, q.i, q.b});
      au = new e(35, 'r', l.q, -1L, new q[]{q.t, q.i, q.e});
      ax = new e(36, 's', l.i, "", new q[]{q.t, q.i, q.a, q.l, q.b});
      ar = new e(37, 't', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      an = new e(38, 'u', l.t, -1, new q[]{q.t, q.q, q.i, q.b});
      ai = new e(39, 'v', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      al = new e(40, 'x', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      at = new e(41, 'y', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      ag = new e(42, 'z', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      as = new e(43, '|', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      aw = new e(44, '€', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      aq = new e(45, 'ƒ', l.t, -1, new q[]{q.t, q.q, q.i, q.a});
      aa = new e(46, '‡', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      af = new e(47, '‰', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      ak = new e(48, 'Š', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      ab = new e(49, 'Œ', l.q, -1L, new q[]{q.t, q.i, q.l, q.b, q.e});
      ac = new e(51, 'š', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      ad = new e(53, '¡', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      bg = new e(54, '¢', l.t, -1, new q[]{q.t, q.q, q.a});
      br = new e(55, '£', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.b});
      ba = new e(56, '§', l.q, -1L, new q[]{q.t, q.i, q.a, q.b});
      bk = new e(57, '«', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      be = new e(58, '®', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      bc = new e(59, 'µ', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      bm = new e(60, '¶', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      bh = new e(61, 'Æ', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.b});
      bs = new e(62, '×', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      bj = new e(63, 'Þ', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      bt = new e(64, 'á', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.b});
      by = new e(65, 'æ', l.t, -1, new q[]{q.t, q.q, q.i});
      bn = new e(66, 'é', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.b});
      bb = new e(67, 'í', l.t, -1, "", new q[]{q.t, q.q, q.i, q.a, q.l, q.b});
      bq = new e(68, 'î', l.t, -1, new q[]{q.t, q.q, q.a, q.l, q.b});
      bz = new e(69, 'ó', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      bx = new e(70, 'ú', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      bf = new e(71, 'û', l.q, -1L, new q[]{q.t, q.i, q.l, q.i, q.b, q.e});
      bo = new e(72, 'Î', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b});
      bv = new e(73, 'J', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b, q.e});
      bi = new e(74, 'Ð', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.b, q.e});
      bu = new e(75, '¤', l.t, -1, new q[]{q.t, q.q, q.i, q.a});
      bl = new e(76, '¥', l.t, -1, new q[]{q.t, q.q, q.i, q.a});
      bw = new e(77, 'è', l.t, -1, new q[]{q.t, q.q, q.a, q.e});
      bp = new e(78, '¹', l.t, -1, new q[]{q.t, q.q, q.a});
      bd = new e(79, '°', l.t, -1, new q[]{q.t, q.q, q.a});
      cb = new e(80, 'ì', l.t, -1, "", new q[]{q.t, q.q, q.a});
      cm = new e(81, 'ë', l.t, -1, new q[]{q.t, q.q, q.a});
      cu = new e(83, 'þ', l.t, -1, new q[]{q.t, q.a});
      cs = new e(84, 'ý', l.t, -1, new q[]{q.t});
      ct = new e(85, 'ÿ', l.t, -1, new q[]{q.t});
      cw = new e(86, 'õ', l.t, -1, new q[]{q.t});
      ch = new e(87, 'ô', l.t, -1, new q[]{q.t});
      cr = new e(88, 'ö', l.t, -1, new q[]{q.t});
      co = new e(89, 'ò', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      cv = new e(90, 'Ü', l.t, -1, new q[]{q.t, q.q, q.e, q.a});
      cd = new e(91, 'ù', l.t, -1, new q[]{q.t, q.q, q.e, q.a});
      cq = new e(92, 'ï', l.t, -1, new q[]{q.t, q.q, q.e, q.a});
      ci = new e(93, '¯', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a});
      cc = new e(94, 'ê', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      ce = new e(95, 'ð', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      cx = new e(96, 'å', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      cy = new e(97, 'a', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      cg = new e(98, 'F', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      cj = new e(99, 'L', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      cl = new e(100, '©', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      cz = new e(101, 'Ý', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      cn = new e(102, '¬', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      ca = new e(103, 'ø', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      cf = new e(104, 'ä', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      cp = new e(105, 'ã', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      ck = new e(106, 'â', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      db = new e(107, 'à', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      dp = new e(108, 'À', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      da = new e(109, 'Ò', l.t, -1, "", new q[]{q.t, q.q, q.a, q.b});
      dr = new e(110, 'Ï', l.q, 0L, new q[]{q.t, q.i, q.a, q.l, q.b, q.e});
      dj = new e(111, 'Ì', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      dh = new e(112, 'É', l.t, -1, new q[]{q.t, q.q, q.e, q.a, q.b});
      dc = new e(113, 'Ê', l.t, -1, "", new q[]{q.t, q.q, q.e, q.a, q.b});
      dl = new e(114, '÷', l.t, -1, new q[]{q.t, q.q, q.i, q.a, q.l, q.b, q.e});
      df = new e(115, '¼', l.q, -1L, new q[]{q.t, q.i, q.a});
      dq = new e(116, '½', l.q, -1L, new q[]{q.t, q.i, q.a});
      dt = new e(117, '•', l.t, -1, new q[]{q.t, q.q, q.a, q.b});
      dy = new e(118, 'Â', l.q, -1L, new q[]{q.t, q.i, q.l, q.b, q.e});
      dn = new e(119, 'Ã', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      do = new e(120, 'Å', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      dw = new e(121, 'Ë', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      dd = new e(122, 'Í', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      du = new e(123, 'Õ', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      dk = new e(124, '²', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      de = new e(125, 'ª', l.t, -1, new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      dg = new e(126, '\u0000', l.t, 0, new q[]{q.t, q.q, q.i, q.l, q.e, q.a, q.b});
      dx = new e(127, '\u0000', l.t, -1, "", new q[]{q.t, q.q, q.i, q.e, q.a, q.b});
      di = new e(128, '\u0000', l.t, -1, new q[]{q.t, q.q, q.i, q.a});
      dv = new e(200, 'X', l.t, -1, new q[0]);
      dz = new e(201, 'W', l.t, -1, new q[0]);
      ds = new e(202, 'b', l.t, -1, new q[]{q.t});
      dm = new e(203, 'B', l.t, -1, new q[]{q.t});
      eb = new e(204, '4', l.t, -1, new q[]{q.t});
      ek = new e(205, 'w', l.t, -1, new q[]{q.t});
      ej = new e(206, 'q', l.t, -1, new q[]{q.t});
      ee = new e(207, '0', l.t, -1, new q[]{q.t, q.a});
      eu = new e(208, '6', l.t, -1, new q[]{q.t});
      ei = new e(l.t, -1, '#');
      ed = new e(l.t, -1, '(');
      ew = new e(l.t, -1, '%');
      ey = new e(l.t, -1, '&');
      en = new e(l.t, -1, ')');
      eg = new e(l.t, -1, '3');
      ez = new e(l.t, -1, '5');
      et = new e(l.t, -1, '7');
      ev = new e(l.t, -1, '8');
      es = new e(l.t, -1, '9');
      el = new e(l.t, -1, 'D');
      eo = new e(l.t, -1, 'G');
      ec = new e(l.t, -1, 'U');
      ep = new e(l.t, -1, 'Á');
      ef = new e(l.t, -1, 'Z');
      eq = new e(l.t, -1, '~');
      ea = new e(l.t, -1, '±');
      em = new e(l.t, -1, '»');
      er = new e(l.t, -1, '¿');
      eh = new e(l.t, -1, 'Ç');
      ex = new e(l.t, -1, 'Ø');
      fg = new e(l.t, -1, 'Ñ');
      fp = new e(l.t, -1, 'ñ');
      ff = new e(l.t, -1, 'Ù');
      fe = new e(l.t, -1, 'ß');
      fk = new e(l.t, -1, 'E');
      fu = new e(l.t, -1, 'Y');
      fz = new e(l.t, -1, 'Ä');
      fl = new e(l.t, -1, 'ü');
      ft = new e(l.t, -1, 'Ú');
      fb = new e(l.t, -1, 'Û');
      fd = new e(l.t, -1, 'Ó');
      fh = new e(l.t, -1, 'È');
      fs = new e(l.t, -1, 'Ô');
      fy = new e(l.t, -1, '¾');
      fw = new e(l.t, -1, 'Ö');
      fm = new e(l.t, -1, '³');
      fr = new e(l.t, -1, '·');
      fi = new e(l.t, -1, '\u0000');
      fn = new e(l.t, -1, '\u0000');
      fj = new e(l.t, -1, '\u0000');
      fa = new e(l.t, -1, 'º');
      fc = new e(l.t, -1, '\u0000');
      fv = new e(l.t, -1, '\u0000');
      fq = new e(l.t, -1, '\u0000');
      fx = new e((l)null, -1, '!');
      fo = new e((l)null, -1, '$');
      gu = new e((l)null, -1, '?');
      ga = new e((l)null, -1, 'ç');
      gd = new e((l)null, -1, '*');
   }

   e(int var1, char var2, l var3, Object var4, String var5, q[] var6) {
      this.gg = var1;
      this.go = var2;
      if (var5 != null && var5.length() > 0) {
         ;
      }

      a(this);
   }

   e(int var1, char var2, l var3, Object var4, q[] var5) {
      this(var1, var2, var3, var4, (String)null, var5);
   }

   e(l var1, Object var2, char var3) {
      this(-1, var3, var1, var2, new q[0]);
   }

   @ObfuscatedName("t")
   public int t() {
      return this.gg;
   }

   @ObfuscatedName("a")
   static void a(e var0) {
      if (gs == null) {
         gs = new e[256];
      }

      gs[ko.t(var0.go) & 255] = var0;
   }

   @ObfuscatedName("fk")
   static void fk(int var0) {
      if (var0 == -3) {
         cx.p("Connection timed out.", "Please try using a different world.", "");
      } else if (var0 == -2) {
         cx.p("", "Error connecting to server.", "");
      } else if (var0 == -1) {
         cx.p("No response from server.", "Please try using a different world.", "");
      } else if (var0 == 3) {
         ci.loginState = 3;
      } else if (var0 == 4) {
         cx.p("Your account has been disabled.", "Please check your message-centre for details.", "");
      } else if (var0 == 5) {
         cx.p("Your account has not logged out from its last", "session or the server is too busy right now.", "Please try again in a few minutes.");
      } else if (var0 == 6) {
         cx.p("RuneScape has been updated!", "Please reload this page.", "");
      } else if (var0 == 7) {
         cx.p("This world is full.", "Please use a different world.", "");
      } else if (var0 == 8) {
         cx.p("Unable to connect.", "Login server offline.", "");
      } else if (var0 == 9) {
         cx.p("Login limit exceeded.", "Too many connections from your address.", "");
      } else if (var0 == 10) {
         cx.p("Unable to connect.", "Bad session id.", "");
      } else if (var0 == 11) {
         cx.p("We suspect someone knows your password.", "Press 'change your password' on front page.", "");
      } else if (var0 == 12) {
         cx.p("You need a members account to login to this world.", "Please subscribe, or use a different world.", "");
      } else if (var0 == 13) {
         cx.p("Could not complete login.", "Please try using a different world.", "");
      } else if (var0 == 14) {
         cx.p("The server is being updated.", "Please wait 1 minute and try again.", "");
      } else if (var0 == 16) {
         cx.p("Too many login attempts.", "Please wait a few minutes before trying again.", "");
      } else if (var0 == 17) {
         cx.p("You are standing in a members-only area.", "To play on this world move to a free area first", "");
      } else if (var0 == 18) {
         cx.p("Account locked as we suspect it has been stolen.", "Press 'recover a locked account' on front page.", "");
      } else if (var0 == 19) {
         cx.p("This world is running a closed Beta.", "Sorry invited players only.", "Please use a different world.");
      } else if (var0 == 20) {
         cx.p("Invalid loginserver requested.", "Please try using a different world.", "");
      } else if (var0 == 22) {
         cx.p("Malformed login packet.", "Please try again.", "");
      } else if (var0 == 23) {
         cx.p("No reply from loginserver.", "Please wait 1 minute and try again.", "");
      } else if (var0 == 24) {
         cx.p("Error loading your profile.", "Please contact customer support.", "");
      } else if (var0 == 25) {
         cx.p("Unexpected loginserver response.", "Please try using a different world.", "");
      } else if (var0 == 26) {
         cx.p("This computers address has been blocked", "as it was used to break our rules.", "");
      } else if (var0 == 27) {
         cx.p("", "Service unavailable.", "");
      } else if (var0 == 31) {
         cx.p("Your account must have a displayname set", "in order to play the game.  Please set it", "via the website, or the main game.");
      } else if (var0 == 32) {
         cx.p("Your attempt to log into your account was", "unsuccessful.  Don't worry, you can sort", "this out by visiting the billing system.");
      } else if (var0 == 37) {
         cx.p("Your account is currently inaccessible.", "Please try again in a few minutes.", "");
      } else if (var0 == 38) {
         cx.p("You need to vote to play!", "Visit runescape.com and vote,", "and then come back here!");
      } else if (var0 == 55) {
         cx.p("Sorry, but your account is not eligible to", "play this version of the game.  Please try", "playing the main game instead!");
      } else {
         if (var0 == 56) {
            cx.p("Enter the 6-digit code generated by your", "authenticator app.", "");
            d.ea(11);
            return;
         }

         if (var0 == 57) {
            cx.p("The code you entered was incorrect.", "Please try again.", "");
            d.ea(11);
            return;
         }

         cx.p("Unexpected server response", "Please try using a different world.", "");
      }

      d.ea(10);
   }

   @ObfuscatedName("gd")
   static final void gd(int var0, int var1, int var2, int var3) {
      ++Client.hu;
      fm.go();
      Chatbox.gs();
      cx.gp();
      al.gk(true);
      jv.gi();
      al.gk(false);

      int var6;
      for(Projectile var4 = (Projectile)Client.loadedProjectiles.e(); var4 != null; var4 = (Projectile)Client.loadedProjectiles.p()) {
         if (var4.plane == kt.ii && Client.bz <= var4.endCycle) {
            if (Client.bz >= var4.startCycle) {
               if (var4.targetIndex > 0) {
                  Npc var33 = Client.loadedNpcs[var4.targetIndex - 1];
                  if (var33 != null && var33.regionX >= 0 && var33.regionX < 13312 && var33.regionY >= 0 && var33.regionY < 13312) {
                     var4.t(var33.regionX, var33.regionY, ef.gn(var33.regionX, var33.regionY, var4.plane) - var4.endHeight, Client.bz);
                  }
               }

               if (var4.targetIndex < 0) {
                  var6 = -var4.targetIndex - 1;
                  Player var37;
                  if (var6 == Client.localPlayerIndex) {
                     var37 = az.il;
                  } else {
                     var37 = Client.loadedPlayers[var6];
                  }

                  if (var37 != null && var37.regionX >= 0 && var37.regionX < 13312 && var37.regionY >= 0 && var37.regionY < 13312) {
                     var4.t(var37.regionX, var37.regionY, ef.gn(var37.regionX, var37.regionY, var4.plane) - var4.endHeight, Client.bz);
                  }
               }

               var4.q(Client.fs);
               an.loadedRegion.u(kt.ii, (int)var4.c, (int)var4.v, (int)var4.u, 60, var4, var4.d, -1, false);
            }
         } else {
            var4.kc();
         }
      }

      for(AnimableObject var34 = (AnimableObject)Client.jf.e(); var34 != null; var34 = (AnimableObject)Client.jf.p()) {
         if (var34.plane == kt.ii && !var34.finished) {
            if (Client.bz >= var34.startCycle) {
               var34.t(Client.fs);
               if (var34.finished) {
                  var34.kc();
               } else {
                  an.loadedRegion.u(var34.plane, var34.regionX, var34.regionY, var34.height, 60, var34, 0, -1, false);
               }
            }
         } else {
            var34.kc();
         }
      }

      ap.gg(var0, var1, var2, var3, true);
      var0 = Client.qq;
      var1 = Client.screenState;
      var2 = Client.viewportWidth;
      var3 = Client.viewportHeight;
      li.ck(var0, var1, var0 + var2, var3 + var1);
      eu.t();
      int var5;
      int var7;
      int var8;
      int var9;
      int var10;
      int var11;
      int var12;
      int var13;
      int var14;
      int var15;
      int var16;
      int var28;
      if (!Client.px) {
         var28 = Client.gq;
         if (Client.hp / 256 > var28) {
            var28 = Client.hp / 256;
         }

         if (Client.pf[4] && Client.qh[4] + 128 > var28) {
            var28 = Client.qh[4] + 128;
         }

         var5 = Client.hintPlane & 2047;
         var6 = am.gz;
         var7 = ez.gv;
         var8 = gf.gw;
         var9 = var28 * 3 + 600;
         var10 = 2048 - var28 & 2047;
         var11 = 2048 - var5 & 2047;
         var12 = 0;
         var13 = 0;
         var14 = var9;
         int var17;
         if (var10 != 0) {
            var15 = eu.m[var10];
            var16 = eu.ay[var10];
            var17 = var16 * var13 - var15 * var9 >> 16;
            var14 = var13 * var15 + var16 * var9 >> 16;
            var13 = var17;
         }

         if (var11 != 0) {
            var15 = eu.m[var11];
            var16 = eu.ay[var11];
            var17 = var16 * var12 + var15 * var14 >> 16;
            var14 = var16 * var14 - var15 * var12 >> 16;
            var12 = var17;
         }

         RuneScriptVM.go = var6 - var12;
         fk.gs = var7 - var13;
         n.gp = var8 - var14;
         ap.gi = var28;
         bt.cameraYaw = var5;
         if (Client.gn == 1 && Client.localPlayerRights >= 2 && Client.bz % 50 == 0 && (am.gz >> 7 != az.il.regionX >> 7 || gf.gw >> 7 != az.il.regionY >> 7)) {
            var15 = az.il.r;
            var16 = (am.gz >> 7) + an.regionBaseX;
            var17 = (gf.gw >> 7) + PlayerComposite.ep;
            RSRandomAccessFile.kw(var16, var17, var15, true);
         }
      }

      if (!Client.px) {
         if (al.qp.roofsDisabled) {
            var5 = kt.ii;
         } else {
            label772: {
               var6 = 3;
               if (ap.gi < 310) {
                  if (Client.gn == 1) {
                     var7 = am.gz >> 7;
                     var8 = gf.gw >> 7;
                  } else {
                     var7 = az.il.regionX >> 7;
                     var8 = az.il.regionY >> 7;
                  }

                  var9 = RuneScriptVM.go >> 7;
                  var10 = n.gp >> 7;
                  if (var9 < 0 || var10 < 0 || var9 >= 104 || var10 >= 104) {
                     var5 = kt.ii;
                     break label772;
                  }

                  if (var7 < 0 || var8 < 0 || var7 >= 104 || var8 >= 104) {
                     var5 = kt.ii;
                     break label772;
                  }

                  if ((bt.landscapeData[kt.ii][var9][var10] & 4) != 0) {
                     var6 = kt.ii;
                  }

                  if (var7 > var9) {
                     var11 = var7 - var9;
                  } else {
                     var11 = var9 - var7;
                  }

                  if (var8 > var10) {
                     var12 = var8 - var10;
                  } else {
                     var12 = var10 - var8;
                  }

                  if (var11 > var12) {
                     var13 = var12 * 65536 / var11;
                     var14 = 32768;

                     while(var9 != var7) {
                        if (var9 < var7) {
                           ++var9;
                        } else if (var9 > var7) {
                           --var9;
                        }

                        if ((bt.landscapeData[kt.ii][var9][var10] & 4) != 0) {
                           var6 = kt.ii;
                        }

                        var14 += var13;
                        if (var14 >= 65536) {
                           var14 -= 65536;
                           if (var10 < var8) {
                              ++var10;
                           } else if (var10 > var8) {
                              --var10;
                           }

                           if ((bt.landscapeData[kt.ii][var9][var10] & 4) != 0) {
                              var6 = kt.ii;
                           }
                        }
                     }
                  } else if (var12 > 0) {
                     var13 = var11 * 65536 / var12;
                     var14 = 32768;

                     while(var10 != var8) {
                        if (var10 < var8) {
                           ++var10;
                        } else if (var10 > var8) {
                           --var10;
                        }

                        if ((bt.landscapeData[kt.ii][var9][var10] & 4) != 0) {
                           var6 = kt.ii;
                        }

                        var14 += var13;
                        if (var14 >= 65536) {
                           var14 -= 65536;
                           if (var9 < var7) {
                              ++var9;
                           } else if (var9 > var7) {
                              --var9;
                           }

                           if ((bt.landscapeData[kt.ii][var9][var10] & 4) != 0) {
                              var6 = kt.ii;
                           }
                        }
                     }
                  }
               }

               if (az.il.regionX >= 0 && az.il.regionY >= 0 && az.il.regionX < 13312 && az.il.regionY < 13312) {
                  if ((bt.landscapeData[kt.ii][az.il.regionX >> 7][az.il.regionY >> 7] & 4) != 0) {
                     var6 = kt.ii;
                  }

                  var5 = var6;
               } else {
                  var5 = kt.ii;
               }
            }
         }

         var28 = var5;
      } else {
         if (al.qp.roofsDisabled) {
            var5 = kt.ii;
         } else {
            var6 = ef.gn(RuneScriptVM.go, n.gp, kt.ii);
            if (var6 - fk.gs < 800 && (bt.landscapeData[kt.ii][RuneScriptVM.go >> 7][n.gp >> 7] & 4) != 0) {
               var5 = kt.ii;
            } else {
               var5 = 3;
            }
         }

         var28 = var5;
      }

      var5 = RuneScriptVM.go;
      var6 = fk.gs;
      var7 = n.gp;
      var8 = ap.gi;
      var9 = bt.cameraYaw;

      for(var10 = 0; var10 < 5; ++var10) {
         if (Client.pf[var10]) {
            var11 = (int)(Math.random() * (double)(Client.pb[var10] * 2 + 1) - (double)Client.pb[var10] + Math.sin((double)Client.currentSkillLevels[var10] / 100.0D * (double)Client.qs[var10]) * (double)Client.qh[var10]);
            if (var10 == 0) {
               RuneScriptVM.go += var11;
            }

            if (var10 == 1) {
               fk.gs += var11;
            }

            if (var10 == 2) {
               n.gp += var11;
            }

            if (var10 == 3) {
               bt.cameraYaw = var11 + bt.cameraYaw & 2047;
            }

            if (var10 == 4) {
               ap.gi += var11;
               if (ap.gi < 128) {
                  ap.gi = 128;
               }

               if (ap.gi > 383) {
                  ap.gi = 383;
               }
            }
         }
      }

      var10 = bs.n;
      var11 = bs.x;
      if (bs.j != 0) {
         var10 = bs.cameraZ;
         var11 = bs.z;
      }

      if (var10 >= var0 && var10 < var0 + var2 && var11 >= var1 && var11 < var3 + var1) {
         ClientPreferences.t(var10 - var0, var11 - var1);
      } else {
         ej.t = false;
         ej.v = 0;
      }

      al.fb();
      li.dl(var0, var1, var2, var3, 0);
      al.fb();
      var12 = eu.o;
      eu.o = Client.viewportScale;
      an.loadedRegion.ak(RuneScriptVM.go, fk.gs, n.gp, ap.gi, bt.cameraYaw, var28);

      while(true) {
         g var29 = (g)x.l.l();
         if (var29 == null) {
            eu.o = var12;
            al.fb();
            an.loadedRegion.w();
            Client.hr = 0;
            boolean var35 = false;
            var14 = -1;
            var15 = -1;
            var16 = cx.b;
            int[] var30 = cx.e;

            int var18;
            for(var18 = 0; var18 < var16 + Client.dz; ++var18) {
               Object var19;
               if (var18 < var16) {
                  var19 = Client.loadedPlayers[var30[var18]];
                  if (var30[var18] == Client.jq) {
                     var35 = true;
                     var14 = var18;
                     continue;
                  }

                  if (var19 == az.il) {
                     var15 = var18;
                     continue;
                  }
               } else {
                  var19 = Client.loadedNpcs[Client.npcIndices[var18 - var16]];
               }

               InventoryDefinition.gc((Character)var19, var18, var0, var1, var2, var3);
            }

            if (Client.iu && var15 != -1) {
               InventoryDefinition.gc(az.il, var15, var0, var1, var2, var3);
            }

            if (var35) {
               InventoryDefinition.gc(Client.loadedPlayers[Client.jq], var14, var0, var1, var2, var3);
            }

            for(var18 = 0; var18 < Client.hr; ++var18) {
               int var31 = Client.hf[var18];
               int var20 = Client.hh[var18];
               int var21 = Client.hn[var18];
               int var22 = Client.hw[var18];
               boolean var23 = true;

               while(var23) {
                  var23 = false;

                  for(int var24 = 0; var24 < var18; ++var24) {
                     if (var20 + 2 > Client.hh[var24] - Client.hw[var24] && var20 - var22 < Client.hh[var24] + 2 && var31 - var21 < Client.hf[var24] + Client.hn[var24] && var21 + var31 > Client.hf[var24] - Client.hn[var24] && Client.hh[var24] - Client.hw[var24] < var20) {
                        var20 = Client.hh[var24] - Client.hw[var24];
                        var23 = true;
                     }
                  }
               }

               Client.hv = Client.hf[var18];
               Client.hd = Client.hh[var18] = var20;
               String var32 = Client.hs[var18];
               if (Client.ly == 0) {
                  int var25 = 16776960;
                  if (Client.ha[var18] < 6) {
                     var25 = Client.ne[Client.ha[var18]];
                  }

                  if (Client.ha[var18] == 6) {
                     var25 = Client.hu % 20 < 10 ? 16711680 : 16776960;
                  }

                  if (Client.ha[var18] == 7) {
                     var25 = Client.hu % 20 < 10 ? 255 : '\uffff';
                  }

                  if (Client.ha[var18] == 8) {
                     var25 = Client.hu % 20 < 10 ? '뀀' : 8454016;
                  }

                  int var26;
                  if (Client.ha[var18] == 9) {
                     var26 = 150 - Client.hc[var18];
                     if (var26 < 50) {
                        var25 = var26 * 1280 + 16711680;
                     } else if (var26 < 100) {
                        var25 = 16776960 - (var26 - 50) * 327680;
                     } else if (var26 < 150) {
                        var25 = (var26 - 100) * 5 + '\uff00';
                     }
                  }

                  if (Client.ha[var18] == 10) {
                     var26 = 150 - Client.hc[var18];
                     if (var26 < 50) {
                        var25 = var26 * 5 + 16711680;
                     } else if (var26 < 100) {
                        var25 = 16711935 - (var26 - 50) * 327680;
                     } else if (var26 < 150) {
                        var25 = (var26 - 100) * 327680 + 255 - (var26 - 100) * 5;
                     }
                  }

                  if (Client.ha[var18] == 11) {
                     var26 = 150 - Client.hc[var18];
                     if (var26 < 50) {
                        var25 = 16777215 - var26 * 327685;
                     } else if (var26 < 100) {
                        var25 = (var26 - 50) * 327685 + '\uff00';
                     } else if (var26 < 150) {
                        var25 = 16777215 - (var26 - 100) * 327680;
                     }
                  }

                  if (Client.hg[var18] == 0) {
                     b.ev.r(var32, var0 + Client.hv, Client.hd + var1, var25, 0);
                  }

                  if (Client.hg[var18] == 1) {
                     b.ev.h(var32, var0 + Client.hv, Client.hd + var1, var25, 0, Client.hu);
                  }

                  if (Client.hg[var18] == 2) {
                     b.ev.av(var32, var0 + Client.hv, Client.hd + var1, var25, 0, Client.hu);
                  }

                  if (Client.hg[var18] == 3) {
                     b.ev.aj(var32, var0 + Client.hv, Client.hd + var1, var25, 0, Client.hu, 150 - Client.hc[var18]);
                  }

                  if (Client.hg[var18] == 4) {
                     var26 = (150 - Client.hc[var18]) * (b.ev.c(var32) + 100) / 150;
                     li.db(var0 + Client.hv - 50, var1, var0 + Client.hv + 50, var3 + var1);
                     b.ev.s(var32, var0 + Client.hv + 50 - var26, Client.hd + var1, var25, 0);
                     li.ck(var0, var1, var0 + var2, var3 + var1);
                  }

                  if (Client.hg[var18] == 5) {
                     var26 = 150 - Client.hc[var18];
                     int var27 = 0;
                     if (var26 < 25) {
                        var27 = var26 - 25;
                     } else if (var26 > 125) {
                        var27 = var26 - 125;
                     }

                     li.db(var0, Client.hd + var1 - b.ev.e - 1, var0 + var2, Client.hd + var1 + 5);
                     b.ev.r(var32, var0 + Client.hv, var27 + Client.hd + var1, var25, 0);
                     li.ck(var0, var1, var0 + var2, var3 + var1);
                  }
               } else {
                  b.ev.r(var32, var0 + Client.hv, Client.hd + var1, 16776960, 0);
               }
            }

            jt.ge(var0, var1);
            ((dg)eu.r).x(Client.fs);
            if (Client.ho) {
               if (Client.mouseCrosshairState == 1) {
                  f.fa[Client.hb / 100].u(Client.lastSelectedInterfaceUID - 8, Client.lastSelectedComponentId - 8);
               }

               if (Client.mouseCrosshairState == 2) {
                  f.fa[Client.hb / 100 + 4].u(Client.lastSelectedInterfaceUID - 8, Client.lastSelectedComponentId - 8);
               }
            }

            bg.gf();
            RuneScriptVM.go = var5;
            fk.gs = var6;
            n.gp = var7;
            ap.gi = var8;
            bt.cameraYaw = var9;
            if (Client.bq) {
               byte var36 = 0;
               var14 = var36 + jg.e + jg.l;
               if (var14 == 0) {
                  Client.bq = false;
               }
            }

            if (Client.bq) {
               li.dl(var0, var1, var2, var3, 0);
               f.ga("Loading - please wait.", false);
            }

            return;
         }

         var29.t();
      }
   }

   @ObfuscatedName("jm")
   static void jm(ByteBuffer var0) {
      if (Client.de != null) {
         var0.s(Client.de, 0, Client.de.length);
      } else {
         byte[] var2 = new byte[24];

         try {
            fh.o.q(0L);
            fh.o.a(var2);

            int var3;
            for(var3 = 0; var3 < 24 && var2[var3] == 0; ++var3) {
               ;
            }

            if (var3 >= 24) {
               throw new IOException();
            }
         } catch (Exception var6) {
            for(int var4 = 0; var4 < 24; ++var4) {
               var2[var4] = -1;
            }
         }

         var0.s(var2, 0, var2.length);
      }
   }
}
