import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hf")
public class Deque {

    @ObfuscatedName("t")
    public Node sentinel = new Node();

    @ObfuscatedName("q")
    Node tail;

    public Deque() {
        this.sentinel.next = this.sentinel;
        this.sentinel.previous = this.sentinel;
    }

    @ObfuscatedName("t")
    public void t() {
        while (true) {
            Node var1 = this.sentinel.next;
            if (var1 == this.sentinel) {
                this.tail = null;
                return;
            }

            var1.kc();
        }
    }

    @ObfuscatedName("q")
    public void q(Node var1) {
        if (var1.previous != null) {
            var1.kc();
        }

        var1.previous = this.sentinel.previous;
        var1.next = this.sentinel;
        var1.previous.next = var1;
        var1.next.previous = var1;
    }

    @ObfuscatedName("i")
    public void i(Node var1) {
        if (var1.previous != null) {
            var1.kc();
        }

        var1.previous = this.sentinel;
        var1.next = this.sentinel.next;
        var1.previous.next = var1;
        var1.next.previous = var1;
    }

    @ObfuscatedName("l")
    public Node l() {
        Node var1 = this.sentinel.next;
        if (var1 == this.sentinel) {
            return null;
        } else {
            var1.kc();
            return var1;
        }
    }

    @ObfuscatedName("b")
    public Node b() {
        Node var1 = this.sentinel.previous;
        if (var1 == this.sentinel) {
            return null;
        } else {
            var1.kc();
            return var1;
        }
    }

    @ObfuscatedName("e")
    public Node e() {
        Node var1 = this.sentinel.next;
        if (var1 == this.sentinel) {
            this.tail = null;
            return null;
        } else {
            this.tail = var1.next;
            return var1;
        }
    }

    @ObfuscatedName("x")
    public Node x() {
        Node var1 = this.sentinel.previous;
        if (var1 == this.sentinel) {
            this.tail = null;
            return null;
        } else {
            this.tail = var1.previous;
            return var1;
        }
    }

    @ObfuscatedName("p")
    public Node p() {
        Node var1 = this.tail;
        if (var1 == this.sentinel) {
            this.tail = null;
            return null;
        } else {
            this.tail = var1.next;
            return var1;
        }
    }

    @ObfuscatedName("o")
    public Node o() {
        Node var1 = this.tail;
        if (var1 == this.sentinel) {
            this.tail = null;
            return null;
        } else {
            this.tail = var1.previous;
            return var1;
        }
    }

    @ObfuscatedName("a")
    public static void a(Node var0, Node var1) {
        if (var0.previous != null) {
            var0.kc();
        }

        var0.previous = var1.previous;
        var0.next = var1;
        var0.previous.next = var0;
        var0.next.previous = var0;
    }
}
