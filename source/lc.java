import java.util.Comparator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lc")
public class lc implements Comparator {

    @ObfuscatedName("t")
    final boolean t;

    public lc(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kv var1, kv var2) {
        return this.t ? var1.r().i(var2.r()) : var2.r().i(var1.r());
    }

    public int compare(Object var1, Object var2) {
        return this.t((kv) var1, (kv) var2);
    }

    public boolean equals(Object var1) {
        return super.equals(var1);
    }
}
