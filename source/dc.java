import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dc")
public class dc {

    @ObfuscatedName("dw")
    static int dw;

    @ObfuscatedName("t")
    FileSystem t;

    @ObfuscatedName("q")
    FileSystem q;

    @ObfuscatedName("i")
    HashTable i = new HashTable(256);

    @ObfuscatedName("a")
    HashTable a = new HashTable(256);

    public dc(FileSystem var1, FileSystem var2) {
        this.t = var1;
        this.q = var2;
    }

    @ObfuscatedName("t")
    DataRequestNode t(int var1, int var2, int[] var3) {
        int var4 = var2 ^ (var1 << 4 & '\uffff' | var1 >>> 12);
        var4 |= var1 << 16;
        long var5 = (long) var4;
        DataRequestNode var7 = (DataRequestNode) this.a.t(var5);
        if (var7 != null) {
            return var7;
        } else if (var3 != null && var3[0] <= 0) {
            return null;
        } else {
            AudioTrack var8 = AudioTrack.t(this.t, var1, var2);
            if (var8 == null) {
                return null;
            } else {
                var7 = var8.q();
                this.a.q(var7, var5);
                if (var3 != null) {
                    var3[0] -= var7.buffer.length;
                }

                return var7;
            }
        }
    }

    @ObfuscatedName("q")
    DataRequestNode q(int var1, int var2, int[] var3) {
        int var4 = var2 ^ (var1 << 4 & '\uffff' | var1 >>> 12);
        var4 |= var1 << 16;
        long var5 = (long) var4 ^ 4294967296L;
        DataRequestNode var7 = (DataRequestNode) this.a.t(var5);
        if (var7 != null) {
            return var7;
        } else if (var3 != null && var3[0] <= 0) {
            return null;
        } else {
            db var8 = (db) this.i.t(var5);
            if (var8 == null) {
                var8 = db.p(this.q, var1, var2);
                if (var8 == null) {
                    return null;
                }

                this.i.q(var8, var5);
            }

            var7 = var8.o(var3);
            if (var7 == null) {
                return null;
            } else {
                var8.kc();
                this.a.q(var7, var5);
                return var7;
            }
        }
    }

    @ObfuscatedName("i")
    public DataRequestNode i(int var1, int[] var2) {
        if (this.t.s() == 1) {
            return this.t(0, var1, var2);
        } else if (this.t.w(var1) == 1) {
            return this.t(var1, 0, var2);
        } else {
            throw new RuntimeException();
        }
    }

    @ObfuscatedName("a")
    public DataRequestNode a(int var1, int[] var2) {
        if (this.q.s() == 1) {
            return this.q(0, var1, var2);
        } else if (this.q.w(var1) == 1) {
            return this.q(var1, 0, var2);
        } else {
            throw new RuntimeException();
        }
    }

    @ObfuscatedName("ir")
    static final int ir(RTComponent var0, int var1) {
        if (var0.ej != null && var1 < var0.ej.length) {
            try {
                int[] var2 = var0.ej[var1];
                int var3 = 0;
                int var4 = 0;
                byte var5 = 0;

                while (true) {
                    int var6 = var2[var4++];
                    int var7 = 0;
                    byte var8 = 0;
                    if (var6 == 0) {
                        return var3;
                    }

                    if (var6 == 1) {
                        var7 = Client.jb[var2[var4++]];
                    }

                    if (var6 == 2) {
                        var7 = Client.baseSkillLevels[var2[var4++]];
                    }

                    if (var6 == 3) {
                        var7 = Client.skillExperiences[var2[var4++]];
                    }

                    int var9;
                    RTComponent var10;
                    int var11;
                    int var12;
                    if (var6 == 4) {
                        var9 = var2[var4++] << 16;
                        var9 += var2[var4++];
                        var10 = Inflater.t(var9);
                        var11 = var2[var4++];
                        if (var11 != -1 && (!cs.loadItemDefinition(var11).membersOnly || Client.membersWorld)) {
                            for (var12 = 0; var12 < var10.itemIds.length; ++var12) {
                                if (var11 + 1 == var10.itemIds[var12]) {
                                    var7 += var10.itemStackSizes[var12];
                                }
                            }
                        }
                    }

                    if (var6 == 5) {
                        var7 = iv.varps[var2[var4++]];
                    }

                    if (var6 == 6) {
                        var7 = id.i[Client.baseSkillLevels[var2[var4++]] - 1];
                    }

                    if (var6 == 7) {
                        var7 = iv.varps[var2[var4++]] * 100 / '뜛';
                    }

                    if (var6 == 8) {
                        var7 = az.il.combatLevel;
                    }

                    if (var6 == 9) {
                        for (var9 = 0; var9 < 25; ++var9) {
                            if (id.q[var9]) {
                                var7 += Client.baseSkillLevels[var9];
                            }
                        }
                    }

                    if (var6 == 10) {
                        var9 = var2[var4++] << 16;
                        var9 += var2[var4++];
                        var10 = Inflater.t(var9);
                        var11 = var2[var4++];
                        if (var11 != -1 && (!cs.loadItemDefinition(var11).membersOnly || Client.membersWorld)) {
                            for (var12 = 0; var12 < var10.itemIds.length; ++var12) {
                                if (var11 + 1 == var10.itemIds[var12]) {
                                    var7 = 999999999;
                                    break;
                                }
                            }
                        }
                    }

                    if (var6 == 11) {
                        var7 = Client.lx;
                    }

                    if (var6 == 12) {
                        var7 = Client.ll;
                    }

                    if (var6 == 13) {
                        var9 = iv.varps[var2[var4++]];
                        int var13 = var2[var4++];
                        var7 = (var9 & 1 << var13) != 0 ? 1 : 0;
                    }

                    if (var6 == 14) {
                        var9 = var2[var4++];
                        var7 = Server.t(var9);
                    }

                    if (var6 == 15) {
                        var8 = 1;
                    }

                    if (var6 == 16) {
                        var8 = 2;
                    }

                    if (var6 == 17) {
                        var8 = 3;
                    }

                    if (var6 == 18) {
                        var7 = (az.il.regionX >> 7) + an.regionBaseX;
                    }

                    if (var6 == 19) {
                        var7 = (az.il.regionY >> 7) + PlayerComposite.ep;
                    }

                    if (var6 == 20) {
                        var7 = var2[var4++];
                    }

                    if (var8 == 0) {
                        if (var5 == 0) {
                            var3 += var7;
                        }

                        if (var5 == 1) {
                            var3 -= var7;
                        }

                        if (var5 == 2 && var7 != 0) {
                            var3 /= var7;
                        }

                        if (var5 == 3) {
                            var3 *= var7;
                        }

                        var5 = 0;
                    } else {
                        var5 = var8;
                    }
                }
            } catch (Exception var14) {
                return -1;
            }
        } else {
            return -2;
        }
    }

    @ObfuscatedName("jt")
    static RTComponent jt(RTComponent var0) {
        RTComponent var1 = RSException.jp(var0);
        if (var1 == null) {
            var1 = var0.dragParent;
        }

        return var1;
    }
}
