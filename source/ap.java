import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ap")
public final class ap {

    @ObfuscatedName("gi")
    static int gi;

    @ObfuscatedName("q")
    final int[] q = new int[4096];

    @ObfuscatedName("t")
    final void t(aw var1) {
        for (int var2 = 0; var2 < 64; ++var2) {
            for (int var3 = 0; var3 < 64; ++var3) {
                this.q[var2 * 64 + var3] = var1.q(var2, var3) | -16777216;
            }
        }

    }

    @ObfuscatedName("q")
    final int q(int var1, int var2) {
        return this.q[var2 + var1 * 64];
    }

    @ObfuscatedName("t")
    public static String t(CharSequence var0, lu var1) {
        if (var0 == null) {
            return null;
        } else {
            int var2 = 0;

            int var3;
            boolean var4;
            char var5;
            for (var3 = var0.length(); var2 < var3; ++var2) {
                var5 = var0.charAt(var2);
                var4 = var5 == 160 || var5 == ' ' || var5 == '_' || var5 == '-';
                if (!var4) {
                    break;
                }
            }

            while (var3 > var2) {
                var5 = var0.charAt(var3 - 1);
                var4 = var5 == 160 || var5 == ' ' || var5 == '_' || var5 == '-';
                if (!var4) {
                    break;
                }

                --var3;
            }

            int var14 = var3 - var2;
            if (var14 >= 1) {
                byte var6;
                if (var1 == null) {
                    var6 = 12;
                } else {
                    switch (var1.g) {
                    case 5:
                        var6 = 20;
                        break;
                    default:
                        var6 = 12;
                    }
                }

                if (var14 <= var6) {
                    StringBuilder var12 = new StringBuilder(var14);

                    for (int var15 = var2; var15 < var3; ++var15) {
                        char var7 = var0.charAt(var15);
                        boolean var8;
                        if (java.lang.Character.isISOControl(var7)) {
                            var8 = false;
                        } else if (ji.o(var7)) {
                            var8 = true;
                        } else {
                            char[] var13 = lt.i;
                            int var10 = 0;

                            label120: while (true) {
                                char var11;
                                if (var10 >= var13.length) {
                                    var13 = lt.a;

                                    for (var10 = 0; var10 < var13.length; ++var10) {
                                        var11 = var13[var10];
                                        if (var11 == var7) {
                                            var8 = true;
                                            break label120;
                                        }
                                    }

                                    var8 = false;
                                    break;
                                }

                                var11 = var13[var10];
                                if (var7 == var11) {
                                    var8 = true;
                                    break;
                                }

                                ++var10;
                            }
                        }

                        if (var8) {
                            char var9;
                            switch (var7) {
                            case ' ':
                            case '-':
                            case '_':
                            case ' ':
                                var9 = '_';
                                break;
                            case '#':
                            case '[':
                            case ']':
                                var9 = var7;
                                break;
                            case 'À':
                            case 'Á':
                            case 'Â':
                            case 'Ã':
                            case 'Ä':
                            case 'à':
                            case 'á':
                            case 'â':
                            case 'ã':
                            case 'ä':
                                var9 = 'a';
                                break;
                            case 'Ç':
                            case 'ç':
                                var9 = 'c';
                                break;
                            case 'È':
                            case 'É':
                            case 'Ê':
                            case 'Ë':
                            case 'è':
                            case 'é':
                            case 'ê':
                            case 'ë':
                                var9 = 'e';
                                break;
                            case 'Í':
                            case 'Î':
                            case 'Ï':
                            case 'í':
                            case 'î':
                            case 'ï':
                                var9 = 'i';
                                break;
                            case 'Ñ':
                            case 'ñ':
                                var9 = 'n';
                                break;
                            case 'Ò':
                            case 'Ó':
                            case 'Ô':
                            case 'Õ':
                            case 'Ö':
                            case 'ò':
                            case 'ó':
                            case 'ô':
                            case 'õ':
                            case 'ö':
                                var9 = 'o';
                                break;
                            case 'Ù':
                            case 'Ú':
                            case 'Û':
                            case 'Ü':
                            case 'ù':
                            case 'ú':
                            case 'û':
                            case 'ü':
                                var9 = 'u';
                                break;
                            case 'ß':
                                var9 = 'b';
                                break;
                            case 'ÿ':
                            case 'Ÿ':
                                var9 = 'y';
                                break;
                            default:
                                var9 = java.lang.Character.toLowerCase(var7);
                            }

                            if (var9 != 0) {
                                var12.append(var9);
                            }
                        }
                    }

                    if (var12.length() == 0) {
                        return null;
                    }

                    return var12.toString();
                }
            }

            return null;
        }
    }

    @ObfuscatedName("t")
    public static gd t(fo var0, gv var1) {
        gd var2;
        if (gd.b == 0) {
            var2 = new gd();
        } else {
            var2 = gd.l[--gd.b];
        }

        var2.t = var0;
        var2.q = var0.cn;
        if (var2.q == -1) {
            var2.i = new Packet(260);
        } else if (var2.q == -2) {
            var2.i = new Packet(10000);
        } else if (var2.q <= 18) {
            var2.i = new Packet(20);
        } else if (var2.q <= 98) {
            var2.i = new Packet(100);
        } else {
            var2.i = new Packet(260);
        }

        var2.i.il(var1);
        var2.i.iu(var2.t.cz);
        var2.a = 0;
        return var2;
    }

    @ObfuscatedName("t")
    public static void t(FileSystem var0) {
        VarInfo.t = var0;
        VarInfo.q = VarInfo.t.w(16);
    }

    @ObfuscatedName("i")
    public static void i(int var0) {
        bs.l = var0;
    }

    @ObfuscatedName("gg")
    static final void gg(int var0, int var1, int var2, int var3, boolean var4) {
        if (var2 < 1) {
            var2 = 1;
        }

        if (var3 < 1) {
            var3 = 1;
        }

        int var5 = var3 - 334;
        if (var5 < 0) {
            var5 = 0;
        } else if (var5 > 100) {
            var5 = 100;
        }

        int var6 = (Client.qk - Client.qw) * var5 / 100 + Client.qw;
        int var7 = var3 * var6 * 512 / (var2 * 334);
        int var8;
        int var9;
        short var15;
        if (var7 < Client.qf) {
            var15 = Client.qf;
            var6 = var15 * var2 * 334 / (var3 * 512);
            if (var6 > Client.qd) {
                var6 = Client.qd;
                var8 = var3 * var6 * 512 / (var15 * 334);
                var9 = (var2 - var8) / 2;
                if (var4) {
                    li.cp();
                    li.dl(var0, var1, var9, var3, -16777216);
                    li.dl(var0 + var2 - var9, var1, var9, var3, -16777216);
                }

                var0 += var9;
                var2 -= var9 * 2;
            }
        } else if (var7 > Client.qa) {
            var15 = Client.qa;
            var6 = var15 * var2 * 334 / (var3 * 512);
            if (var6 < Client.qm) {
                var6 = Client.qm;
                var8 = var15 * var2 * 334 / (var6 * 512);
                var9 = (var3 - var8) / 2;
                if (var4) {
                    li.cp();
                    li.dl(var0, var1, var2, var9, -16777216);
                    li.dl(var0, var3 + var1 - var9, var2, var9, -16777216);
                }

                var1 += var9;
                var3 -= var9 * 2;
            }
        }

        var8 = (Client.qu - Client.qr) * var5 / 100 + Client.qr;
        Client.viewportScale = var3 * var6 * var8 / 85504 << 1;
        if (var2 != Client.viewportWidth || var3 != Client.viewportHeight) {
            int[] var14 = new int[9];

            for (int var10 = 0; var10 < 9; ++var10) {
                int var11 = var10 * 32 + 15 + 128;
                int var12 = var11 * 3 + 600;
                int var13 = eu.m[var11];
                var14[var10] = var13 * var12 >> 16;
            }

            Region.ag(var14, 500, 800, var2, var3);
        }

        Client.qq = var0;
        Client.screenState = var1;
        Client.viewportWidth = var2;
        Client.viewportHeight = var3;
    }

    @ObfuscatedName("ii")
    static void ii(boolean var0) {
        Client.kv = var0;
    }
}
