import java.io.File;
import java.net.URL;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ep")
public class ep {

    @ObfuscatedName("l")
    public static File l;

    @ObfuscatedName("t")
    final URL t;

    @ObfuscatedName("q")
    volatile boolean q;

    @ObfuscatedName("i")
    volatile byte[] i;

    ep(URL var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    public boolean t() {
        return this.q;
    }

    @ObfuscatedName("q")
    public byte[] q() {
        return this.i;
    }

    @ObfuscatedName("q")
    public static gd q() {
        gd var0;
        if (gd.b == 0) {
            var0 = new gd();
        } else {
            var0 = gd.l[--gd.b];
        }

        var0.t = null;
        var0.q = 0;
        var0.i = new Packet(5000);
        return var0;
    }

    @ObfuscatedName("hy")
    static final void hy(NpcDefinition var0, int var1, int var2, int var3) {
        if (Client.menuSize < 400) {
            if (var0.ae != null) {
                var0 = var0.x();
            }

            if (var0 != null) {
                if (var0.ap) {
                    if (!var0.au || Client.lc == var1) {
                        String var4 = var0.name;
                        if (var0.level != 0) {
                            var4 = var4 + iw.ie(var0.level, az.il.combatLevel) + " " + " (" + "level-" + var0.level
                                    + ")";
                        }

                        if (var0.au && Client.kg) {
                            fy.hi("Examine", ar.q(16776960) + var4, 1003, var1, var2, var3);
                        }

                        if (Client.inventoryItemSelectionState == 1) {
                            fy.hi("Use", Client.lastSelectedInventoryItemName + " " + "->" + " " + ar.q(16776960)
                                    + var4, 7, var1, var2, var3);
                        } else if (Client.interfaceSelected) {
                            if ((eq.km & 2) == 2) {
                                fy.hi(Client.lj, Client.selectedSpellName + " " + "->" + " " + ar.q(16776960) + var4,
                                        8, var1, var2, var3);
                            }
                        } else {
                            int var5 = var0.au && Client.kg ? 2000 : 0;
                            String[] var6 = var0.actions;
                            int var7;
                            int var8;
                            if (var6 != null) {
                                for (var7 = 4; var7 >= 0; --var7) {
                                    if (var6[var7] != null && !var6[var7].equalsIgnoreCase("Attack")) {
                                        var8 = 0;
                                        if (var7 == 0) {
                                            var8 = var5 + 9;
                                        }

                                        if (var7 == 1) {
                                            var8 = var5 + 10;
                                        }

                                        if (var7 == 2) {
                                            var8 = var5 + 11;
                                        }

                                        if (var7 == 3) {
                                            var8 = var5 + 12;
                                        }

                                        if (var7 == 4) {
                                            var8 = var5 + 13;
                                        }

                                        fy.hi(var6[var7], ar.q(16776960) + var4, var8, var1, var2, var3);
                                    }
                                }
                            }

                            if (var6 != null) {
                                for (var7 = 4; var7 >= 0; --var7) {
                                    if (var6[var7] != null && var6[var7].equalsIgnoreCase("Attack")) {
                                        short var9 = 0;
                                        if (cq.a != Client.co) {
                                            if (Client.co == cq.q || cq.t == Client.co
                                                    && var0.level > az.il.combatLevel) {
                                                var9 = 2000;
                                            }

                                            var8 = 0;
                                            if (var7 == 0) {
                                                var8 = var9 + 9;
                                            }

                                            if (var7 == 1) {
                                                var8 = var9 + 10;
                                            }

                                            if (var7 == 2) {
                                                var8 = var9 + 11;
                                            }

                                            if (var7 == 3) {
                                                var8 = var9 + 12;
                                            }

                                            if (var7 == 4) {
                                                var8 = var9 + 13;
                                            }

                                            fy.hi(var6[var7], ar.q(16776960) + var4, var8, var1, var2, var3);
                                        }
                                    }
                                }
                            }

                            if (!var0.au || !Client.kg) {
                                fy.hi("Examine", ar.q(16776960) + var4, 1003, var1, var2, var3);
                            }
                        }

                    }
                }
            }
        }
    }
}
