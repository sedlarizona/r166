import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.imageio.ImageIO;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.rs.Reflection;

@ObfuscatedName("ag")
public final class ag {

    @ObfuscatedName("u")
    public static RSRandomAccessFileChannel[] u;

    @ObfuscatedName("t")
    boolean t = false;

    @ObfuscatedName("q")
    boolean q = false;

    @ObfuscatedName("i")
    aq i;

    @ObfuscatedName("a")
    Sprite a;

    @ObfuscatedName("l")
    HashMap l;

    @ObfuscatedName("b")
    au[][] b;

    @ObfuscatedName("e")
    HashMap e = new HashMap();

    @ObfuscatedName("x")
    lk[] x;

    @ObfuscatedName("p")
    final HashMap p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    public int v = 0;

    public ag(lk[] var1, HashMap var2) {
        this.x = var1;
        this.p = var2;
    }

    @ObfuscatedName("t")
    public void t(FileSystem var1, String var2, boolean var3) {
        if (!this.q) {
            this.t = false;
            this.q = true;
            System.nanoTime();
            int var4 = var1.h(AppletParameter.t.value);
            int var5 = var1.av(var4, var2);
            ByteBuffer var6 = new ByteBuffer(var1.ae(AppletParameter.t.value, var2));
            ByteBuffer var7 = new ByteBuffer(var1.ae(AppletParameter.q.value, var2));
            ByteBuffer var8 = new ByteBuffer(var1.ae(var2, AppletParameter.a.value));
            System.nanoTime();
            System.nanoTime();
            this.i = new aq();

            try {
                this.i.cu(var6, var8, var7, var5, var3);
            } catch (IllegalStateException var24) {
                return;
            }

            this.i.f();
            this.i.r();
            this.i.y();
            this.g = this.i.z() * 64;
            this.n = this.i.s() * 64;
            this.o = (this.i.w() - this.i.z() + 1) * 64;
            this.c = (this.i.d() - this.i.s() + 1) * 64;
            int var18 = this.i.w() - this.i.z() + 1;
            int var10 = this.i.d() - this.i.s() + 1;
            System.nanoTime();
            System.nanoTime();
            au.e.e();
            au.x.e();
            this.b = new au[var18][var10];
            Iterator var11 = this.i.c.iterator();

            int var14;
            int var15;
            while (var11.hasNext()) {
                r var12 = (r) var11.next();
                int var13 = var12.t;
                var14 = var12.q;
                var15 = var13 - this.i.z();
                int var16 = var14 - this.i.s();
                this.b[var15][var16] = new au(var13, var14, this.i.u(), this.p);
                this.b[var15][var16].b(var12, this.i.u);
            }

            for (int var19 = 0; var19 < var18; ++var19) {
                for (int var20 = 0; var20 < var10; ++var20) {
                    if (this.b[var19][var20] == null) {
                        this.b[var19][var20] = new au(this.i.z() + var19, this.i.s() + var20, this.i.u(), this.p);
                        this.b[var19][var20].e(this.i.v, this.i.u);
                    }
                }
            }

            System.nanoTime();
            System.nanoTime();
            if (var1.aj(AppletParameter.i.value, var2)) {
                byte[] var27 = var1.ae(AppletParameter.i.value, var2);
                BufferedImage var21 = null;

                Sprite var28;
                label49: {
                    try {
                        var21 = ImageIO.read(new ByteArrayInputStream(var27));
                        var14 = var21.getWidth();
                        var15 = var21.getHeight();
                        int[] var22 = new int[var15 * var14];
                        PixelGrabber var17 = new PixelGrabber(var21, 0, 0, var14, var15, var22, 0, var14);
                        var17.grabPixels();
                        var28 = new Sprite(var22, var14, var15);
                        break label49;
                    } catch (IOException var25) {
                        ;
                    } catch (InterruptedException var26) {
                        ;
                    }

                    var28 = new Sprite(0, 0);
                }

                this.a = var28;
            }

            System.nanoTime();
            var1.d();
            var1.r();
            this.t = true;
        }
    }

    @ObfuscatedName("q")
    public final void q() {
        this.l = null;
    }

    @ObfuscatedName("i")
    public final void i(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        int[] var9 = li.ao;
        int var10 = li.av;
        int var11 = li.aj;
        int[] var12 = new int[4];
        li.dp(var12);
        aj var13 = this.x(var1, var2, var3, var4);
        float var14 = this.u(var7 - var5, var3 - var1);
        int var15 = (int) Math.ceil((double) var14);
        this.v = var15;
        if (!this.e.containsKey(var15)) {
            af var16 = new af(var15);
            var16.a();
            this.e.put(var15, var16);
        }

        au[] var22 = new au[8];

        int var17;
        int var18;
        for (var17 = var13.i; var17 < var13.t + var13.i; ++var17) {
            for (var18 = var13.a; var18 < var13.q + var13.a; ++var18) {
                this.l(var17, var18, var22);
                this.b[var17][var18].o(var15, (af) this.e.get(var15), var22, this.x);
            }
        }

        li.cf(var9, var10, var11);
        li.da(var12);
        var17 = (int) (var14 * 64.0F);
        var18 = this.g + var1;
        int var19 = var2 + this.n;

        for (int var20 = var13.i; var20 < var13.i + var13.t; ++var20) {
            for (int var21 = var13.a; var21 < var13.a + var13.q; ++var21) {
                this.b[var20][var21].l(var5 + var17 * (this.b[var20][var21].p * 64 - var18) / 64, var8 - var17
                        * (this.b[var20][var21].g * 64 - var19 + 64) / 64, var17);
            }
        }

    }

    @ObfuscatedName("a")
    public final void a(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, HashSet var9,
            HashSet var10, int var11, int var12, boolean var13) {
        aj var14 = this.x(var1, var2, var3, var4);
        float var15 = this.u(var7 - var5, var3 - var1);
        int var16 = (int) (64.0F * var15);
        int var17 = this.g + var1;
        int var18 = var2 + this.n;

        int var19;
        int var20;
        for (var19 = var14.i; var19 < var14.i + var14.t; ++var19) {
            for (var20 = var14.a; var20 < var14.q + var14.a; ++var20) {
                if (var13) {
                    this.b[var19][var20].ai();
                }

                this.b[var19][var20].c(var5 + var16 * (this.b[var19][var20].p * 64 - var17) / 64, var8 - var16
                        * (this.b[var19][var20].g * 64 - var18 + 64) / 64, var16, var9);
            }
        }

        if (var10 != null && var11 > 0) {
            for (var19 = var14.i; var19 < var14.i + var14.t; ++var19) {
                for (var20 = var14.a; var20 < var14.a + var14.q; ++var20) {
                    this.b[var19][var20].u(var10, var11, var12);
                }
            }
        }

    }

    @ObfuscatedName("l")
    void l(int var1, int var2, au[] var3) {
        boolean var4 = var1 <= 0;
        boolean var5 = var1 >= this.b.length - 1;
        boolean var6 = var2 <= 0;
        boolean var7 = var2 >= this.b[0].length - 1;
        if (var7) {
            var3[im.t.t()] = null;
        } else {
            var3[im.t.t()] = this.b[var1][var2 + 1];
        }

        var3[im.q.t()] = !var7 && !var5 ? this.b[var1 + 1][var2 + 1] : null;
        var3[im.x.t()] = !var7 && !var4 ? this.b[var1 - 1][var2 + 1] : null;
        var3[im.i.t()] = var5 ? null : this.b[var1 + 1][var2];
        var3[im.e.t()] = var4 ? null : this.b[var1 - 1][var2];
        var3[im.l.t()] = var6 ? null : this.b[var1][var2 - 1];
        var3[im.a.t()] = !var6 && !var5 ? this.b[var1 + 1][var2 - 1] : null;
        var3[im.b.t()] = !var6 && !var4 ? this.b[var1 - 1][var2 - 1] : null;
    }

    @ObfuscatedName("b")
    public void b(int var1, int var2, int var3, int var4, HashSet var5, int var6, int var7) {
        if (this.a != null) {
            this.a.z(var1, var2, var3, var4);
            if (var6 > 0 && var6 % var7 < var7 / 2) {
                if (this.l == null) {
                    this.c();
                }

                Iterator var8 = var5.iterator();

                while (true) {
                    List var10;
                    do {
                        if (!var8.hasNext()) {
                            return;
                        }

                        int var9 = (Integer) var8.next();
                        var10 = (List) this.l.get(var9);
                    } while (var10 == null);

                    Iterator var11 = var10.iterator();

                    while (var11.hasNext()) {
                        al var12 = (al) var11.next();
                        int var13 = var3 * (var12.q.q - this.g) / this.o;
                        int var14 = var4 - (var12.q.i - this.n) * var4 / this.c;
                        li.dh(var13 + var1, var14 + var2, 2, 16776960, 256);
                    }
                }
            }
        }
    }

    @ObfuscatedName("e")
    public List e(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
        LinkedList var11 = new LinkedList();
        if (!this.t) {
            return var11;
        } else {
            aj var12 = this.x(var1, var2, var3, var4);
            float var13 = this.u(var7, var3 - var1);
            int var14 = (int) (var13 * 64.0F);
            int var15 = this.g + var1;
            int var16 = var2 + this.n;

            for (int var17 = var12.i; var17 < var12.i + var12.t; ++var17) {
                for (int var18 = var12.a; var18 < var12.a + var12.q; ++var18) {
                    List var19 = this.b[var17][var18].aa(var5 + var14 * (this.b[var17][var18].p * 64 - var15) / 64,
                            var8 + var6 - var14 * (this.b[var17][var18].g * 64 - var16 + 64) / 64, var14, var9, var10);
                    if (!var19.isEmpty()) {
                        var11.addAll(var19);
                    }
                }
            }

            return var11;
        }
    }

    @ObfuscatedName("x")
    aj x(int var1, int var2, int var3, int var4) {
        aj var5 = new aj(this);
        int var6 = this.g + var1;
        int var7 = var2 + this.n;
        int var8 = var3 + this.g;
        int var9 = var4 + this.n;
        int var10 = var6 / 64;
        int var11 = var7 / 64;
        int var12 = var8 / 64;
        int var13 = var9 / 64;
        var5.t = var12 - var10 + 1;
        var5.q = var13 - var11 + 1;
        var5.i = var10 - this.i.z();
        var5.a = var11 - this.i.s();
        if (var5.i < 0) {
            var5.t += var5.i;
            var5.i = 0;
        }

        if (var5.i > this.b.length - var5.t) {
            var5.t = this.b.length - var5.i;
        }

        if (var5.a < 0) {
            var5.q += var5.a;
            var5.a = 0;
        }

        if (var5.a > this.b[0].length - var5.q) {
            var5.q = this.b[0].length - var5.a;
        }

        var5.t = Math.min(var5.t, this.b.length);
        var5.q = Math.min(var5.q, this.b[0].length);
        return var5;
    }

    @ObfuscatedName("p")
    public boolean p() {
        return this.t;
    }

    @ObfuscatedName("o")
    public HashMap o() {
        this.c();
        return this.l;
    }

    @ObfuscatedName("c")
    void c() {
        if (this.l == null) {
            this.l = new HashMap();
        }

        this.l.clear();

        for (int var1 = 0; var1 < this.b.length; ++var1) {
            for (int var2 = 0; var2 < this.b[var1].length; ++var2) {
                List var3 = this.b[var1][var2].af();
                Iterator var4 = var3.iterator();

                while (var4.hasNext()) {
                    al var5 = (al) var4.next();
                    if (!this.l.containsKey(var5.t)) {
                        LinkedList var6 = new LinkedList();
                        var6.add(var5);
                        this.l.put(var5.t, var6);
                    } else {
                        List var7 = (List) this.l.get(var5.t);
                        var7.add(var5);
                    }
                }
            }
        }

    }

    @ObfuscatedName("u")
    float u(int var1, int var2) {
        float var3 = (float) var1 / (float) var2;
        if (var3 > 8.0F) {
            return 8.0F;
        } else if (var3 < 1.0F) {
            return 1.0F;
        } else {
            int var4 = Math.round(var3);
            return Math.abs((float) var4 - var3) < 0.05F ? (float) var4 : var3;
        }
    }

    @ObfuscatedName("q")
    public static void q(Packet var0) {
        ClassStructureNode var1 = (ClassStructureNode) lm.t.b();
        if (var1 != null) {
            int var2 = var0.index;
            var0.e(var1.t);

            for (int var3 = 0; var3 < var1.q; ++var3) {
                if (var1.a[var3] != 0) {
                    var0.a(var1.a[var3]);
                } else {
                    try {
                        int var4 = var1.i[var3];
                        Field var5;
                        int var6;
                        if (var4 == 0) {
                            var5 = var1.fields[var3];
                            var6 = Reflection.getInt(var5, (Object) null);
                            var0.a(0);
                            var0.e(var6);
                        } else if (var4 == 1) {
                            var5 = var1.fields[var3];
                            Reflection.setInt(var5, (Object) null, var1.b[var3]);
                            var0.a(0);
                        } else if (var4 == 2) {
                            var5 = var1.fields[var3];
                            var6 = var5.getModifiers();
                            var0.a(0);
                            var0.e(var6);
                        }

                        Method var25;
                        if (var4 != 3) {
                            if (var4 == 4) {
                                var25 = var1.methods[var3];
                                var6 = var25.getModifiers();
                                var0.a(0);
                                var0.e(var6);
                            }
                        } else {
                            var25 = var1.methods[var3];
                            byte[][] var10 = var1.methodArguments[var3];
                            Object[] var7 = new Object[var10.length];

                            for (int var8 = 0; var8 < var10.length; ++var8) {
                                ObjectInputStream var9 = new ObjectInputStream(new ByteArrayInputStream(var10[var8]));
                                var7[var8] = var9.readObject();
                            }

                            Object var11 = Reflection.invoke(var25, (Object) null, var7);
                            if (var11 == null) {
                                var0.a(0);
                            } else if (var11 instanceof Number) {
                                var0.a(1);
                                var0.p(((Number) var11).longValue());
                            } else if (var11 instanceof String) {
                                var0.a(2);
                                var0.u((String) var11);
                            } else {
                                var0.a(4);
                            }
                        }
                    } catch (ClassNotFoundException var13) {
                        var0.a(-10);
                    } catch (InvalidClassException var14) {
                        var0.a(-11);
                    } catch (StreamCorruptedException var15) {
                        var0.a(-12);
                    } catch (OptionalDataException var16) {
                        var0.a(-13);
                    } catch (IllegalAccessException var17) {
                        var0.a(-14);
                    } catch (IllegalArgumentException var18) {
                        var0.a(-15);
                    } catch (InvocationTargetException var19) {
                        var0.a(-16);
                    } catch (SecurityException var20) {
                        var0.a(-17);
                    } catch (IOException var21) {
                        var0.a(-18);
                    } catch (NullPointerException var22) {
                        var0.a(-19);
                    } catch (Exception var23) {
                        var0.a(-20);
                    } catch (Throwable var24) {
                        var0.a(-21);
                    }
                }
            }

            var0.ad(var2);
            var1.kc();
        }
    }

    @ObfuscatedName("q")
    static int q(Packet var0) {
        int var1 = var0.jt(2);
        int var2;
        if (var1 == 0) {
            var2 = 0;
        } else if (var1 == 1) {
            var2 = var0.jt(5);
        } else if (var1 == 2) {
            var2 = var0.jt(8);
        } else {
            var2 = var0.jt(11);
        }

        return var2;
    }

    @ObfuscatedName("iy")
    static boolean iy() {
        return Client.kv;
    }

    @ObfuscatedName("ja")
    static final boolean ja(RTComponent var0) {
        int var1 = var0.contentType;
        if (var1 == 205) {
            Client.ei = 250;
            return true;
        } else {
            int var2;
            int var3;
            if (var1 >= 300 && var1 <= 313) {
                var2 = (var1 - 300) / 2;
                var3 = var1 & 1;
                Client.qv.q(var2, var3 == 1);
            }

            if (var1 >= 314 && var1 <= 323) {
                var2 = (var1 - 314) / 2;
                var3 = var1 & 1;
                Client.qv.i(var2, var3 == 1);
            }

            if (var1 == 324) {
                Client.qv.a(false);
            }

            if (var1 == 325) {
                Client.qv.a(true);
            }

            if (var1 == 326) {
                gd var4 = ap.t(fo.al, Client.ee.l);
                Client.qv.l(var4.i);
                Client.ee.i(var4);
                return true;
            } else {
                return false;
            }
        }
    }
}
