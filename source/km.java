import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("km")
public final class km extends RSFont {

    public km(byte[] var1, int[] var2, int[] var3, int[] var4, int[] var5, int[] var6, byte[][] var7) {
        super(var1, var2, var3, var4, var5, var6, var7);
    }

    public km(byte[] var1) {
        super(var1);
    }

    @ObfuscatedName("t")
    final void t(byte[] var1, int var2, int var3, int var4, int var5, int var6) {
        int var7 = var3 * li.av + var2;
        int var8 = li.av - var4;
        int var9 = 0;
        int var10 = 0;
        int var11;
        if (var3 < li.ae) {
            var11 = li.ae - var3;
            var5 -= var11;
            var3 = li.ae;
            var10 += var11 * var4;
            var7 += var11 * li.av;
        }

        if (var3 + var5 > li.am) {
            var5 -= var3 + var5 - li.am;
        }

        if (var2 < li.az) {
            var11 = li.az - var2;
            var4 -= var11;
            var2 = li.az;
            var10 += var11;
            var7 += var11;
            var9 += var11;
            var8 += var11;
        }

        if (var2 + var4 > li.ap) {
            var11 = var2 + var4 - li.ap;
            var4 -= var11;
            var9 += var11;
            var8 += var11;
        }

        if (var4 > 0 && var5 > 0) {
            RSFont.ar(li.ao, var1, var6, var10, var7, var4, var5, var8, var9);
        }
    }

    @ObfuscatedName("q")
    final void q(byte[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int var8 = var3 * li.av + var2;
        int var9 = li.av - var4;
        int var10 = 0;
        int var11 = 0;
        int var12;
        if (var3 < li.ae) {
            var12 = li.ae - var3;
            var5 -= var12;
            var3 = li.ae;
            var11 += var12 * var4;
            var8 += var12 * li.av;
        }

        if (var3 + var5 > li.am) {
            var5 -= var3 + var5 - li.am;
        }

        if (var2 < li.az) {
            var12 = li.az - var2;
            var4 -= var12;
            var2 = li.az;
            var11 += var12;
            var8 += var12;
            var10 += var12;
            var9 += var12;
        }

        if (var2 + var4 > li.ap) {
            var12 = var2 + var4 - li.ap;
            var4 -= var12;
            var10 += var12;
            var9 += var12;
        }

        if (var4 > 0 && var5 > 0) {
            RSFont.ai(li.ao, var1, var6, var11, var8, var4, var5, var9, var10, var7);
        }
    }
}
