import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("du")
public abstract class IntegerNode extends Node {

    @ObfuscatedName("b")
    int number;
}
