import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("js")
public class js extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static Cache q = new Cache(64);

    @ObfuscatedName("i")
    int i = 0;

    @ObfuscatedName("a")
    public int a;

    @ObfuscatedName("l")
    public int l;

    @ObfuscatedName("b")
    public int b;

    @ObfuscatedName("e")
    public int e;

    @ObfuscatedName("q")
    void q() {
        this.l(this.i);
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        while (true) {
            int var3 = var1.av();
            if (var3 == 0) {
                return;
            }

            this.a(var1, var3, var2);
        }
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1, int var2, int var3) {
        if (var2 == 1) {
            this.i = var1.az();
        }

    }

    @ObfuscatedName("l")
    void l(int var1) {
        double var2 = (double) (var1 >> 16 & 255) / 256.0D;
        double var4 = (double) (var1 >> 8 & 255) / 256.0D;
        double var6 = (double) (var1 & 255) / 256.0D;
        double var8 = var2;
        if (var4 < var2) {
            var8 = var4;
        }

        if (var6 < var8) {
            var8 = var6;
        }

        double var10 = var2;
        if (var4 > var2) {
            var10 = var4;
        }

        if (var6 > var10) {
            var10 = var6;
        }

        double var12 = 0.0D;
        double var14 = 0.0D;
        double var16 = (var10 + var8) / 2.0D;
        if (var8 != var10) {
            if (var16 < 0.5D) {
                var14 = (var10 - var8) / (var8 + var10);
            }

            if (var16 >= 0.5D) {
                var14 = (var10 - var8) / (2.0D - var10 - var8);
            }

            if (var2 == var10) {
                var12 = (var4 - var6) / (var10 - var8);
            } else if (var10 == var4) {
                var12 = 2.0D + (var6 - var2) / (var10 - var8);
            } else if (var10 == var6) {
                var12 = 4.0D + (var2 - var4) / (var10 - var8);
            }
        }

        var12 /= 6.0D;
        this.l = (int) (var14 * 256.0D);
        this.b = (int) (var16 * 256.0D);
        if (this.l < 0) {
            this.l = 0;
        } else if (this.l > 255) {
            this.l = 255;
        }

        if (this.b < 0) {
            this.b = 0;
        } else if (this.b > 255) {
            this.b = 255;
        }

        if (var16 > 0.5D) {
            this.e = (int) (var14 * (1.0D - var16) * 512.0D);
        } else {
            this.e = (int) (var14 * var16 * 512.0D);
        }

        if (this.e < 1) {
            this.e = 1;
        }

        this.a = (int) ((double) this.e * var12);
    }

    @ObfuscatedName("o")
    static final void o(lk var0) {
        short var1 = 256;

        int var2;
        for (var2 = 0; var2 < ao.ao.length; ++var2) {
            ao.ao[var2] = 0;
        }

        int var3;
        for (var2 = 0; var2 < 5000; ++var2) {
            var3 = (int) (Math.random() * 128.0D * (double) var1);
            ao.ao[var3] = (int) (Math.random() * 256.0D);
        }

        int var4;
        int var5;
        for (var2 = 0; var2 < 20; ++var2) {
            for (var3 = 1; var3 < var1 - 1; ++var3) {
                for (var4 = 1; var4 < 127; ++var4) {
                    var5 = var4 + (var3 << 7);
                    TileModel.av[var5] = (ao.ao[var5 + 128] + ao.ao[var5 - 128] + ao.ao[var5 + 1] + ao.ao[var5 - 1]) / 4;
                }
            }

            int[] var8 = ao.ao;
            ao.ao = TileModel.av;
            TileModel.av = var8;
        }

        if (var0 != null) {
            var2 = 0;

            for (var3 = 0; var3 < var0.a; ++var3) {
                for (var4 = 0; var4 < var0.i; ++var4) {
                    if (var0.t[var2++] != 0) {
                        var5 = var4 + var0.l + 16;
                        int var6 = var3 + var0.b + 16;
                        int var7 = var5 + (var6 << 7);
                        ao.ao[var7] = 0;
                    }
                }
            }
        }

    }
}
