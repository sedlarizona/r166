import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ia")
public class ia extends Node {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    public byte[] q;

    @ObfuscatedName("i")
    public fn i;

    @ObfuscatedName("a")
    public ju a;

    @ObfuscatedName("i")
    static String i() {
        return al.qp.e ? kn.k(ci.username) : ci.username;
    }
}
