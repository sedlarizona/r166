import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lg")
public class lg {

    @ObfuscatedName("b")
    String b;

    @ObfuscatedName("e")
    FileSystem e;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    boolean p = false;

    lg(FileSystem var1) {
        this.e = var1;
    }

    @ObfuscatedName("t")
    void t(String var1) {
        if (var1 != null && !var1.isEmpty()) {
            if (var1 != this.b) {
                this.b = var1;
                this.x = 0;
                this.p = false;
                this.q();
            }
        }
    }

    @ObfuscatedName("q")
    int q() {
        if (this.x < 25) {
            if (!this.e.am(AppletParameter.q.value, this.b)) {
                return this.x;
            }

            this.x = 25;
        }

        if (this.x == 25) {
            if (!this.e.am(this.b, AppletParameter.a.value)) {
                return 25 + this.e.ah(this.b) * 25 / 100;
            }

            this.x = 50;
        }

        if (this.x == 50) {
            if (this.e.aj(AppletParameter.i.value, this.b) && !this.e.am(AppletParameter.i.value, this.b)) {
                return 50;
            }

            this.x = 75;
        }

        if (this.x == 75) {
            if (!this.e.am(this.b, AppletParameter.l.value)) {
                return 75;
            }

            this.x = 100;
            this.p = true;
        }

        return this.x;
    }

    @ObfuscatedName("i")
    boolean i() {
        return this.p;
    }

    @ObfuscatedName("a")
    int a() {
        return this.x;
    }
}
