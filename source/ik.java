import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ik")
public class ik {

    @ObfuscatedName("w")
    static int w;

    @ObfuscatedName("ba")
    static String ba;

    @ObfuscatedName("t")
    public int t;

    @ObfuscatedName("q")
    public int q;

    @ObfuscatedName("i")
    public int i;

    public ik(ik var1) {
        this.t = var1.t;
        this.q = var1.q;
        this.i = var1.i;
    }

    public ik(int var1, int var2, int var3) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
    }

    public ik() {
        this.t = -1;
    }

    public ik(int var1) {
        if (var1 == -1) {
            this.t = -1;
        } else {
            this.t = var1 >> 28 & 3;
            this.q = var1 >> 14 & 16383;
            this.i = var1 & 16383;
        }

    }

    @ObfuscatedName("t")
    public void t(int var1, int var2, int var3) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
    }

    @ObfuscatedName("q")
    public int q() {
        return this.t << 28 | this.q << 14 | this.i;
    }

    @ObfuscatedName("i")
    boolean i(ik var1) {
        if (this.t != var1.t) {
            return false;
        } else if (this.q != var1.q) {
            return false;
        } else {
            return this.i == var1.i;
        }
    }

    @ObfuscatedName("a")
    String a(String var1) {
        return this.t + var1 + (this.q >> 6) + var1 + (this.i >> 6) + var1 + (this.q & 63) + var1 + (this.i & 63);
    }

    public int hashCode() {
        return this.q();
    }

    public String toString() {
        return this.a(",");
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else {
            return !(var1 instanceof ik) ? false : this.i((ik) var1);
        }
    }
}
