import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bi")
public final class Player extends Character {

    @ObfuscatedName("dd")
    static int dd;

    @ObfuscatedName("t")
    kb t;

    @ObfuscatedName("q")
    PlayerComposite composite;

    @ObfuscatedName("i")
    int pkIconIndex = -1;

    @ObfuscatedName("a")
    int prayerIconIndex = -1;

    @ObfuscatedName("b")
    String[] b = new String[3];

    @ObfuscatedName("e")
    int combatLevel;

    @ObfuscatedName("x")
    int totalLevel;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int transformObjectStartCycle;

    @ObfuscatedName("n")
    int transformObjectEndCycle;

    @ObfuscatedName("o")
    int transformObjectX;

    @ObfuscatedName("c")
    int transformObjectZ;

    @ObfuscatedName("v")
    int transformObjectY;

    @ObfuscatedName("u")
    Model transformObjectModel;

    @ObfuscatedName("j")
    int j;

    @ObfuscatedName("k")
    int k;

    @ObfuscatedName("z")
    int z;

    @ObfuscatedName("w")
    int w;

    @ObfuscatedName("s")
    boolean intransformable;

    @ObfuscatedName("d")
    int team;

    @ObfuscatedName("f")
    boolean f;

    @ObfuscatedName("r")
    int r;

    @ObfuscatedName("y")
    int y;

    @ObfuscatedName("h")
    kx h;

    @ObfuscatedName("m")
    kx m;

    @ObfuscatedName("ay")
    boolean ay;

    @ObfuscatedName("ao")
    int ao;

    @ObfuscatedName("av")
    int av;

    Player() {
        for (int var1 = 0; var1 < 3; ++var1) {
            this.b[var1] = "";
        }

        this.combatLevel = 0;
        this.totalLevel = 0;
        this.transformObjectStartCycle = 0;
        this.transformObjectEndCycle = 0;
        this.intransformable = false;
        this.team = 0;
        this.f = false;
        this.h = kx.t;
        this.m = kx.t;
        this.ay = false;
    }

    @ObfuscatedName("t")
    final void t(ByteBuffer var1) {
        var1.index = 0;
        int var2 = var1.av();
        this.pkIconIndex = var1.aj();
        this.prayerIconIndex = var1.aj();
        int var3 = -1;
        this.team = 0;
        int[] var4 = new int[12];

        int var6;
        int var7;
        for (int var5 = 0; var5 < 12; ++var5) {
            var6 = var1.av();
            if (var6 == 0) {
                var4[var5] = 0;
            } else {
                var7 = var1.av();
                var4[var5] = var7 + (var6 << 8);
                if (var5 == 0 && var4[0] == 65535) {
                    var3 = var1.ae();
                    break;
                }

                if (var4[var5] >= 512) {
                    int var8 = cs.loadItemDefinition(var4[var5] - 512).ba;
                    if (var8 != 0) {
                        this.team = var8;
                    }
                }
            }
        }

        int[] var9 = new int[5];

        for (var6 = 0; var6 < 5; ++var6) {
            var7 = var1.av();
            if (var7 < 0 || var7 >= PlayerComposite.x[var6].length) {
                var7 = 0;
            }

            var9[var6] = var7;
        }

        super.au = var1.ae();
        if (super.au == 65535) {
            super.au = -1;
        }

        super.ax = var1.ae();
        if (super.ax == 65535) {
            super.ax = -1;
        }

        super.ar = super.ax;
        super.stanceId = var1.ae();
        if (super.stanceId == 65535) {
            super.stanceId = -1;
        }

        super.ai = var1.ae();
        if (super.ai == 65535) {
            super.ai = -1;
        }

        super.al = var1.ae();
        if (super.al == 65535) {
            super.al = -1;
        }

        super.at = var1.ae();
        if (super.at == 65535) {
            super.at = -1;
        }

        super.ag = var1.ae();
        if (super.ag == 65535) {
            super.ag = -1;
        }

        this.t = new kb(var1.ar(), ad.bc);
        this.i();
        this.b();
        if (this == az.il) {
            bj.i = this.t.t();
        }

        this.combatLevel = var1.av();
        this.totalLevel = var1.ae();
        this.f = var1.av() == 1;
        if (Client.playerWeight == 0 && Client.localPlayerRights >= 2) {
            this.f = false;
        }

        if (this.composite == null) {
            this.composite = new PlayerComposite();
        }

        this.composite.t(var4, var9, var2 == 1, var3);
    }

    @ObfuscatedName("q")
    boolean q() {
        if (this.h == kx.t) {
            this.a();
        }

        return this.h == kx.q;
    }

    @ObfuscatedName("i")
    void i() {
        this.h = kx.t;
    }

    @ObfuscatedName("a")
    void a() {
        this.h = BoundaryObject.qi.h(this.t) ? kx.q : kx.i;
    }

    @ObfuscatedName("l")
    boolean l() {
        if (this.m == kx.t) {
            this.e();
        }

        return this.m == kx.q;
    }

    @ObfuscatedName("b")
    void b() {
        this.m = kx.t;
    }

    @ObfuscatedName("e")
    void e() {
        this.m = ad.ot != null && ad.ot.z(this.t) ? kx.q : kx.i;
    }

    @ObfuscatedName("x")
    int x() {
        return this.composite != null && this.composite.npcId != -1 ? ho.q(this.composite.npcId).e : 1;
    }

    @ObfuscatedName("p")
    protected final Model p() {
        if (this.composite == null) {
            return null;
        } else {
            kf var1 = super.animationId != -1 && super.bq == 0 ? ff.t(super.animationId) : null;
            kf var2 = super.bs == -1 || this.intransformable || super.au == super.bs && var1 != null ? null : ff
                    .t(super.bs);
            Model var3 = this.composite.e(var1, super.animationFrameIndex, var2, super.stanceFrameIndex);
            if (var3 == null) {
                return null;
            } else {
                var3.b();
                super.cs = var3.modelHeight;
                Model var4;
                Model[] var5;
                if (!this.intransformable && super.bx != -1 && super.graphicId != -1) {
                    var4 = ah.t(super.bx).a(super.graphicId);
                    if (var4 != null) {
                        var4.f(0, -super.bi, 0);
                        var5 = new Model[] { var3, var4 };
                        var3 = new Model(var5, 2);
                    }
                }

                if (!this.intransformable && this.transformObjectModel != null) {
                    if (Client.bz >= this.transformObjectEndCycle) {
                        this.transformObjectModel = null;
                    }

                    if (Client.bz >= this.transformObjectStartCycle && Client.bz < this.transformObjectEndCycle) {
                        var4 = this.transformObjectModel;
                        var4.f(this.transformObjectX - super.regionX, this.transformObjectZ - this.p,
                                this.transformObjectY - super.regionY);
                        if (super.ct == 512) {
                            var4.z();
                            var4.z();
                            var4.z();
                        } else if (super.ct == 1024) {
                            var4.z();
                            var4.z();
                        } else if (super.ct == 1536) {
                            var4.z();
                        }

                        var5 = new Model[] { var3, var4 };
                        var3 = new Model(var5, 2);
                        if (super.ct == 512) {
                            var4.z();
                        } else if (super.ct == 1024) {
                            var4.z();
                            var4.z();
                        } else if (super.ct == 1536) {
                            var4.z();
                            var4.z();
                            var4.z();
                        }

                        var4.f(super.regionX - this.transformObjectX, this.p - this.transformObjectZ, super.regionY
                                - this.transformObjectY);
                    }
                }

                var3.ay = true;
                return var3;
            }
        }
    }

    @ObfuscatedName("o")
    final void o(int var1, int var2, byte var3) {
        if (super.animationId != -1 && ff.t(super.animationId).z == 1) {
            super.animationId = -1;
        }

        super.bh = -1;
        if (var1 >= 0 && var1 < 104 && var2 >= 0 && var2 < 104) {
            if (super.co[0] >= 0 && super.co[0] < 104 && super.cv[0] >= 0 && super.cv[0] < 104) {
                if (var3 == 2) {
                    Player var4 = this;
                    int var5 = super.co[0];
                    int var6 = super.cv[0];
                    int var7 = this.x();
                    if (var5 >= var7 && var5 < 104 - var7 && var6 >= var7 && var6 < 104 - var7 && var1 >= var7
                            && var1 < 104 - var7 && var2 >= var7 && var2 < 104 - var7) {
                        int var8 = em.t(var5, var6, this.x(), Inflater.hr(var1, var2), Client.collisionMaps[this.r],
                                true, Client.rp, Client.re);
                        if (var8 >= 1) {
                            for (int var9 = 0; var9 < var8 - 1; ++var9) {
                                var4.u(Client.rp[var9], Client.re[var9], (byte) 2);
                            }
                        }
                    }
                }

                this.u(var1, var2, var3);
            } else {
                this.c(var1, var2);
            }
        } else {
            this.c(var1, var2);
        }

    }

    @ObfuscatedName("c")
    void c(int var1, int var2) {
        super.speed = 0;
        super.ci = 0;
        super.cq = 0;
        super.co[0] = var1;
        super.cv[0] = var2;
        int var3 = this.x();
        super.regionX = var3 * 64 + super.co[0] * 128;
        super.regionY = var3 * 64 + super.cv[0] * 128;
    }

    @ObfuscatedName("u")
    final void u(int var1, int var2, byte var3) {
        if (super.speed < 9) {
            ++super.speed;
        }

        for (int var4 = super.speed; var4 > 0; --var4) {
            super.co[var4] = super.co[var4 - 1];
            super.cv[var4] = super.cv[var4 - 1];
            super.cd[var4] = super.cd[var4 - 1];
        }

        super.co[0] = var1;
        super.cv[0] = var2;
        super.cd[0] = var3;
    }

    @ObfuscatedName("k")
    final boolean k() {
        return this.composite != null;
    }

    @ObfuscatedName("fl")
    static final void fl(boolean var0) {
        if (var0) {
            Client.dn = ci.bk ? ff.q : ff.a;
        } else {
            Client.dn = al.qp.map.containsKey(kc.x(ci.username)) ? ff.t : ff.i;
        }

    }

    @ObfuscatedName("gw")
    static final void gw() {
        hb.gv(false);
        Client.ea = 0;
        boolean var0 = true;

        int var1;
        for (var1 = 0; var1 < gx.fk.length; ++var1) {
            if (bq.fp[var1] != -1 && gx.fk[var1] == null) {
                gx.fk[var1] = ee.cn.i(bq.fp[var1], 0);
                if (gx.fk[var1] == null) {
                    var0 = false;
                    ++Client.ea;
                }
            }

            if (AnimableObject.ff[var1] != -1 && cs.locationFileBytes[var1] == null) {
                cs.locationFileBytes[var1] = ee.cn.a(AnimableObject.ff[var1], 0, DevelopmentStage.fe[var1]);
                if (cs.locationFileBytes[var1] == null) {
                    var0 = false;
                    ++Client.ea;
                }
            }
        }

        if (!var0) {
            Client.ex = 1;
        } else {
            Client.er = 0;
            var0 = true;

            int var3;
            int var4;
            ByteBuffer var8;
            int var9;
            int var10;
            int var11;
            int var13;
            int var14;
            int var15;
            int var16;
            int var17;
            int var18;
            for (var1 = 0; var1 < gx.fk.length; ++var1) {
                byte[] var2 = cs.locationFileBytes[var1];
                if (var2 != null) {
                    var3 = (FileSystem.fg[var1] >> 8) * 64 - an.regionBaseX;
                    var4 = (FileSystem.fg[var1] & 255) * 64 - PlayerComposite.ep;
                    if (Client.dynamicRegion) {
                        var3 = 10;
                        var4 = 10;
                    }

                    boolean var7 = true;
                    var8 = new ByteBuffer(var2);
                    var9 = -1;

                    label1329: while (true) {
                        var10 = var8.ag();
                        if (var10 == 0) {
                            var0 &= var7;
                            break;
                        }

                        var9 += var10;
                        var11 = 0;
                        boolean var12 = false;

                        while (true) {
                            while (!var12) {
                                var13 = var8.ag();
                                if (var13 == 0) {
                                    continue label1329;
                                }

                                var11 += var13 - 1;
                                var14 = var11 & 63;
                                var15 = var11 >> 6 & 63;
                                var16 = var8.av() >> 2;
                                var17 = var3 + var15;
                                var18 = var4 + var14;
                                if (var17 > 0 && var18 > 0 && var17 < 103 && var18 < 103) {
                                    ObjectDefinition var19 = jw.q(var9);
                                    if (var16 != 22 || !Client.bh || var19.d != 0 || var19.w == 1 || var19.ai) {
                                        if (!var19.e()) {
                                            ++Client.er;
                                            var7 = false;
                                        }

                                        var12 = true;
                                    }
                                }
                            }

                            var13 = var8.ag();
                            if (var13 == 0) {
                                break;
                            }

                            var8.av();
                        }
                    }
                }
            }

            if (!var0) {
                Client.ex = 2;
            } else {
                if (Client.ex != 0) {
                    f.ga("Loading - please wait." + "<br>" + " (" + 100 + "%" + ")", true);
                }

                al.fb();
                an.loadedRegion.t();

                for (var1 = 0; var1 < 4; ++var1) {
                    Client.collisionMaps[var1].t();
                }

                int var44;
                for (var1 = 0; var1 < 4; ++var1) {
                    for (var44 = 0; var44 < 104; ++var44) {
                        for (var3 = 0; var3 < 104; ++var3) {
                            bt.landscapeData[var1][var44][var3] = 0;
                        }
                    }
                }

                al.fb();
                bt.i = 99;
                bt.a = new byte[4][104][104];
                bt.l = new byte[4][104][104];
                bt.b = new byte[4][104][104];
                Skins.e = new byte[4][104][104];
                bt.u = new int[4][105][105];
                bt.x = new byte[4][105][105];
                bt.p = new int[105][105];
                g.g = new int[104];
                ax.n = new int[104];
                r.o = new int[104];
                gn.c = new int[104];
                ax.v = new int[104];
                var1 = gx.fk.length;

                for (AreaSoundEmitter var59 = (AreaSoundEmitter) AreaSoundEmitter.t.e(); var59 != null; var59 = (AreaSoundEmitter) AreaSoundEmitter.t
                        .p()) {
                    if (var59.p != null) {
                        kq.pd.q(var59.p);
                        var59.p = null;
                    }

                    if (var59.v != null) {
                        kq.pd.q(var59.v);
                        var59.v = null;
                    }
                }

                AreaSoundEmitter.t.t();
                hb.gv(true);
                int var5;
                int var6;
                int var20;
                int var21;
                int var54;
                int var62;
                int var64;
                if (!Client.dynamicRegion) {
                    byte[] var45;
                    for (var44 = 0; var44 < var1; ++var44) {
                        var3 = (FileSystem.fg[var44] >> 8) * 64 - an.regionBaseX;
                        var4 = (FileSystem.fg[var44] & 255) * 64 - PlayerComposite.ep;
                        var45 = gx.fk[var44];
                        if (var45 != null) {
                            al.fb();
                            var6 = ab.ef * 8 - 48;
                            var62 = gd.eq * 8 - 48;
                            CollisionMap[] var63 = Client.collisionMaps;

                            for (var9 = 0; var9 < 4; ++var9) {
                                for (var10 = 0; var10 < 64; ++var10) {
                                    for (var11 = 0; var11 < 64; ++var11) {
                                        if (var3 + var10 > 0 && var3 + var10 < 103 && var4 + var11 > 0
                                                && var11 + var4 < 103) {
                                            var63[var9].flags[var10 + var3][var4 + var11] &= -16777217;
                                        }
                                    }
                                }
                            }

                            ByteBuffer var46 = new ByteBuffer(var45);

                            for (var10 = 0; var10 < 4; ++var10) {
                                for (var11 = 0; var11 < 64; ++var11) {
                                    for (var64 = 0; var64 < 64; ++var64) {
                                        AreaSoundEmitter.l(var46, var10, var11 + var3, var64 + var4, var6, var62, 0);
                                    }
                                }
                            }
                        }
                    }

                    for (var44 = 0; var44 < var1; ++var44) {
                        var3 = (FileSystem.fg[var44] >> 8) * 64 - an.regionBaseX;
                        var4 = (FileSystem.fg[var44] & 255) * 64 - PlayerComposite.ep;
                        var45 = gx.fk[var44];
                        if (var45 == null && gd.eq < 800) {
                            al.fb();
                            az.q(var3, var4, 64, 64);
                        }
                    }

                    hb.gv(true);

                    for (var44 = 0; var44 < var1; ++var44) {
                        byte[] var48 = cs.locationFileBytes[var44];
                        if (var48 != null) {
                            var4 = (FileSystem.fg[var44] >> 8) * 64 - an.regionBaseX;
                            var5 = (FileSystem.fg[var44] & 255) * 64 - PlayerComposite.ep;
                            al.fb();
                            Region var49 = an.loadedRegion;
                            CollisionMap[] var50 = Client.collisionMaps;
                            var8 = new ByteBuffer(var48);
                            var9 = -1;

                            while (true) {
                                var10 = var8.ag();
                                if (var10 == 0) {
                                    break;
                                }

                                var9 += var10;
                                var11 = 0;

                                while (true) {
                                    var64 = var8.ag();
                                    if (var64 == 0) {
                                        break;
                                    }

                                    var11 += var64 - 1;
                                    var13 = var11 & 63;
                                    var14 = var11 >> 6 & 63;
                                    var15 = var11 >> 12;
                                    var16 = var8.av();
                                    var17 = var16 >> 2;
                                    var18 = var16 & 3;
                                    var54 = var4 + var14;
                                    var20 = var13 + var5;
                                    if (var54 > 0 && var20 > 0 && var54 < 103 && var20 < 103) {
                                        var21 = var15;
                                        if ((bt.landscapeData[1][var54][var20] & 2) == 2) {
                                            var21 = var15 - 1;
                                        }

                                        CollisionMap var55 = null;
                                        if (var21 >= 0) {
                                            var55 = var50[var21];
                                        }

                                        RuneScriptStackItem.b(var15, var54, var20, var9, var18, var17, var49, var55);
                                    }
                                }
                            }
                        }
                    }
                }

                int var22;
                int var23;
                int var24;
                int var25;
                int var26;
                int var27;
                int var28;
                int var29;
                int var31;
                int var32;
                int var33;
                int var47;
                if (Client.dynamicRegion) {
                    for (var44 = 0; var44 < 4; ++var44) {
                        al.fb();

                        for (var3 = 0; var3 < 13; ++var3) {
                            for (var4 = 0; var4 < 13; ++var4) {
                                boolean var61 = false;
                                var6 = Client.dynamicRegionFlags[var44][var3][var4];
                                if (var6 != -1) {
                                    var62 = var6 >> 24 & 3;
                                    var47 = var6 >> 1 & 3;
                                    var9 = var6 >> 14 & 1023;
                                    var10 = var6 >> 3 & 2047;
                                    var11 = (var9 / 8 << 8) + var10 / 8;

                                    for (var64 = 0; var64 < FileSystem.fg.length; ++var64) {
                                        if (FileSystem.fg[var64] == var11 && gx.fk[var64] != null) {
                                            p.i(gx.fk[var64], var44, var3 * 8, var4 * 8, var62, (var9 & 7) * 8,
                                                    (var10 & 7) * 8, var47, Client.collisionMaps);
                                            var61 = true;
                                            break;
                                        }
                                    }
                                }

                                if (!var61) {
                                    Projectile.a(var44, var3 * 8, var4 * 8);
                                }
                            }
                        }
                    }

                    for (var44 = 0; var44 < 13; ++var44) {
                        for (var3 = 0; var3 < 13; ++var3) {
                            var4 = Client.dynamicRegionFlags[0][var44][var3];
                            if (var4 == -1) {
                                az.q(var44 * 8, var3 * 8, 8, 8);
                            }
                        }
                    }

                    hb.gv(true);

                    for (var44 = 0; var44 < 4; ++var44) {
                        al.fb();

                        for (var3 = 0; var3 < 13; ++var3) {
                            label1149: for (var4 = 0; var4 < 13; ++var4) {
                                var5 = Client.dynamicRegionFlags[var44][var3][var4];
                                if (var5 != -1) {
                                    var6 = var5 >> 24 & 3;
                                    var62 = var5 >> 1 & 3;
                                    var47 = var5 >> 14 & 1023;
                                    var9 = var5 >> 3 & 2047;
                                    var10 = (var47 / 8 << 8) + var9 / 8;

                                    for (var11 = 0; var11 < FileSystem.fg.length; ++var11) {
                                        if (FileSystem.fg[var11] == var10 && cs.locationFileBytes[var11] != null) {
                                            byte[] var51 = cs.locationFileBytes[var11];
                                            var13 = var3 * 8;
                                            var14 = var4 * 8;
                                            var15 = (var47 & 7) * 8;
                                            var16 = (var9 & 7) * 8;
                                            Region var52 = an.loadedRegion;
                                            CollisionMap[] var53 = Client.collisionMaps;
                                            ByteBuffer var65 = new ByteBuffer(var51);
                                            var20 = -1;

                                            while (true) {
                                                var21 = var65.ag();
                                                if (var21 == 0) {
                                                    continue label1149;
                                                }

                                                var20 += var21;
                                                var22 = 0;

                                                while (true) {
                                                    var23 = var65.ag();
                                                    if (var23 == 0) {
                                                        break;
                                                    }

                                                    var22 += var23 - 1;
                                                    var24 = var22 & 63;
                                                    var25 = var22 >> 6 & 63;
                                                    var26 = var22 >> 12;
                                                    var27 = var65.av();
                                                    var28 = var27 >> 2;
                                                    var29 = var27 & 3;
                                                    if (var6 == var26 && var25 >= var15 && var25 < var15 + 8
                                                            && var24 >= var16 && var24 < var16 + 8) {
                                                        ObjectDefinition var30 = jw.q(var20);
                                                        var31 = var13
                                                                + bk.q(var25 & 7, var24 & 7, var62, var30.width,
                                                                        var30.height, var29);
                                                        var32 = var14
                                                                + ad.i(var25 & 7, var24 & 7, var62, var30.width,
                                                                        var30.height, var29);
                                                        if (var31 > 0 && var32 > 0 && var31 < 103 && var32 < 103) {
                                                            var33 = var44;
                                                            if ((bt.landscapeData[1][var31][var32] & 2) == 2) {
                                                                var33 = var44 - 1;
                                                            }

                                                            CollisionMap var34 = null;
                                                            if (var33 >= 0) {
                                                                var34 = var53[var33];
                                                            }

                                                            RuneScriptStackItem.b(var44, var31, var32, var20, var29
                                                                    + var62 & 3, var28, var52, var34);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                hb.gv(true);
                al.fb();
                Region var60 = an.loadedRegion;
                CollisionMap[] var71 = Client.collisionMaps;

                for (var4 = 0; var4 < 4; ++var4) {
                    for (var5 = 0; var5 < 104; ++var5) {
                        for (var6 = 0; var6 < 104; ++var6) {
                            if ((bt.landscapeData[var4][var5][var6] & 1) == 1) {
                                var62 = var4;
                                if ((bt.landscapeData[1][var5][var6] & 2) == 2) {
                                    var62 = var4 - 1;
                                }

                                if (var62 >= 0) {
                                    var71[var62].a(var5, var6);
                                }
                            }
                        }
                    }
                }

                bt.h += (int) (Math.random() * 5.0D) - 2;
                if (bt.h < -8) {
                    bt.h = -8;
                }

                if (bt.h > 8) {
                    bt.h = 8;
                }

                bt.m += (int) (Math.random() * 5.0D) - 2;
                if (bt.m < -16) {
                    bt.m = -16;
                }

                if (bt.m > 16) {
                    bt.m = 16;
                }

                for (var4 = 0; var4 < 4; ++var4) {
                    byte[][] var68 = bt.x[var4];
                    var11 = (int) Math.sqrt(5100.0D);
                    var64 = var11 * 768 >> 8;

                    for (var13 = 1; var13 < 103; ++var13) {
                        for (var14 = 1; var14 < 103; ++var14) {
                            var15 = bt.tileHeights[var4][var14 + 1][var13] - bt.tileHeights[var4][var14 - 1][var13];
                            var16 = bt.tileHeights[var4][var14][var13 + 1] - bt.tileHeights[var4][var14][var13 - 1];
                            var17 = (int) Math.sqrt((double) (var16 * var16 + var15 * var15 + 65536));
                            var18 = (var15 << 8) / var17;
                            var54 = 65536 / var17;
                            var20 = (var16 << 8) / var17;
                            var21 = (var20 * -50 + var18 * -50 + var54 * -10) / var64 + 96;
                            var22 = (var68[var14][var13 + 1] >> 3) + (var68[var14 - 1][var13] >> 2)
                                    + (var68[var14][var13 - 1] >> 2) + (var68[var14 + 1][var13] >> 3)
                                    + (var68[var14][var13] >> 1);
                            bt.p[var14][var13] = var21 - var22;
                        }
                    }

                    for (var13 = 0; var13 < 104; ++var13) {
                        g.g[var13] = 0;
                        ax.n[var13] = 0;
                        r.o[var13] = 0;
                        gn.c[var13] = 0;
                        ax.v[var13] = 0;
                    }

                    for (var13 = -5; var13 < 109; ++var13) {
                        for (var14 = 0; var14 < 104; ++var14) {
                            var15 = var13 + 5;
                            if (var15 >= 0 && var15 < 104) {
                                var16 = bt.a[var4][var15][var14] & 255;
                                if (var16 > 0) {
                                    js var72 = DevelopmentStage.t(var16 - 1);
                                    g.g[var14] += var72.a;
                                    ax.n[var14] += var72.l;
                                    r.o[var14] += var72.b;
                                    gn.c[var14] += var72.e;
                                    ++ax.v[var14];
                                }
                            }

                            var16 = var13 - 5;
                            if (var16 >= 0 && var16 < 104) {
                                var17 = bt.a[var4][var16][var14] & 255;
                                if (var17 > 0) {
                                    js var73 = DevelopmentStage.t(var17 - 1);
                                    g.g[var14] -= var73.a;
                                    ax.n[var14] -= var73.l;
                                    r.o[var14] -= var73.b;
                                    gn.c[var14] -= var73.e;
                                    --ax.v[var14];
                                }
                            }
                        }

                        if (var13 >= 1 && var13 < 103) {
                            var14 = 0;
                            var15 = 0;
                            var16 = 0;
                            var17 = 0;
                            var18 = 0;

                            for (var54 = -5; var54 < 109; ++var54) {
                                var20 = var54 + 5;
                                if (var20 >= 0 && var20 < 104) {
                                    var14 += g.g[var20];
                                    var15 += ax.n[var20];
                                    var16 += r.o[var20];
                                    var17 += gn.c[var20];
                                    var18 += ax.v[var20];
                                }

                                var21 = var54 - 5;
                                if (var21 >= 0 && var21 < 104) {
                                    var14 -= g.g[var21];
                                    var15 -= ax.n[var21];
                                    var16 -= r.o[var21];
                                    var17 -= gn.c[var21];
                                    var18 -= ax.v[var21];
                                }

                                if (var54 >= 1
                                        && var54 < 103
                                        && (!Client.bh || (bt.landscapeData[0][var13][var54] & 2) != 0 || (bt.landscapeData[var4][var13][var54] & 16) == 0)) {
                                    if (var4 < bt.i) {
                                        bt.i = var4;
                                    }

                                    var22 = bt.a[var4][var13][var54] & 255;
                                    var23 = bt.l[var4][var13][var54] & 255;
                                    if (var22 > 0 || var23 > 0) {
                                        var24 = bt.tileHeights[var4][var13][var54];
                                        var25 = bt.tileHeights[var4][var13 + 1][var54];
                                        var26 = bt.tileHeights[var4][var13 + 1][var54 + 1];
                                        var27 = bt.tileHeights[var4][var13][var54 + 1];
                                        var28 = bt.p[var13][var54];
                                        var29 = bt.p[var13 + 1][var54];
                                        int var56 = bt.p[var13 + 1][var54 + 1];
                                        var31 = bt.p[var13][var54 + 1];
                                        var32 = -1;
                                        var33 = -1;
                                        int var35;
                                        int var57;
                                        if (var22 > 0) {
                                            var57 = var14 * 256 / var17;
                                            var35 = var15 / var18;
                                            int var36 = var16 / var18;
                                            var32 = ee.k(var57, var35, var36);
                                            var57 = var57 + bt.h & 255;
                                            var36 += bt.m;
                                            if (var36 < 0) {
                                                var36 = 0;
                                            } else if (var36 > 255) {
                                                var36 = 255;
                                            }

                                            var33 = ee.k(var57, var35, var36);
                                        }

                                        if (var4 > 0) {
                                            boolean var74 = true;
                                            if (var22 == 0 && bt.b[var4][var13][var54] != 0) {
                                                var74 = false;
                                            }

                                            if (var23 > 0 && !ga.loadObjectDefinition(var23 - 1).l) {
                                                var74 = false;
                                            }

                                            if (var74 && var25 == var24 && var26 == var24 && var27 == var24) {
                                                bt.u[var4][var13][var54] |= 2340;
                                            }
                                        }

                                        var57 = 0;
                                        if (var33 != -1) {
                                            var57 = eu.f[cm.c(var33, 96)];
                                        }

                                        if (var23 == 0) {
                                            var60.b(var4, var13, var54, 0, 0, -1, var24, var25, var26, var27,
                                                    cm.c(var32, var28), cm.c(var32, var29), cm.c(var32, var56),
                                                    cm.c(var32, var31), 0, 0, 0, 0, var57, 0);
                                        } else {
                                            var35 = bt.b[var4][var13][var54] + 1;
                                            byte var67 = Skins.e[var4][var13][var54];
                                            kw var37 = ga.loadObjectDefinition(var23 - 1);
                                            int var38 = var37.a;
                                            int var39;
                                            int var40;
                                            int var41;
                                            int var42;
                                            if (var38 >= 0) {
                                                var40 = eu.r.a(var38);
                                                var39 = -1;
                                            } else if (var37.i == 16711935) {
                                                var39 = -2;
                                                var38 = -1;
                                                var40 = -2;
                                            } else {
                                                var39 = ee.k(var37.e, var37.x, var37.p);
                                                var41 = var37.e + bt.h & 255;
                                                var42 = var37.p + bt.m;
                                                if (var42 < 0) {
                                                    var42 = 0;
                                                } else if (var42 > 255) {
                                                    var42 = 255;
                                                }

                                                var40 = ee.k(var41, var37.x, var42);
                                            }

                                            var41 = 0;
                                            if (var40 != -2) {
                                                var41 = eu.f[GrandExchangeOffer.u(var40, 96)];
                                            }

                                            if (var37.b != -1) {
                                                var42 = var37.g + bt.h & 255;
                                                int var43 = var37.o + bt.m;
                                                if (var43 < 0) {
                                                    var43 = 0;
                                                } else if (var43 > 255) {
                                                    var43 = 255;
                                                }

                                                var40 = ee.k(var42, var37.n, var43);
                                                var41 = eu.f[GrandExchangeOffer.u(var40, 96)];
                                            }

                                            var60.b(var4, var13, var54, var35, var67, var38, var24, var25, var26,
                                                    var27, cm.c(var32, var28), cm.c(var32, var29), cm.c(var32, var56),
                                                    cm.c(var32, var31), GrandExchangeOffer.u(var39, var28),
                                                    GrandExchangeOffer.u(var39, var29),
                                                    GrandExchangeOffer.u(var39, var56),
                                                    GrandExchangeOffer.u(var39, var31), var57, var41);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (var13 = 1; var13 < 103; ++var13) {
                        for (var14 = 1; var14 < 103; ++var14) {
                            if ((bt.landscapeData[var4][var14][var13] & 8) != 0) {
                                var54 = 0;
                            } else if (var4 > 0 && (bt.landscapeData[1][var14][var13] & 2) != 0) {
                                var54 = var4 - 1;
                            } else {
                                var54 = var4;
                            }

                            var60.l(var4, var14, var13, var54);
                        }
                    }

                    bt.a[var4] = null;
                    bt.l[var4] = null;
                    bt.b[var4] = null;
                    Skins.e[var4] = null;
                    bt.x[var4] = null;
                }

                var60.an(-50, -10, -50);

                for (var4 = 0; var4 < 104; ++var4) {
                    for (var5 = 0; var5 < 104; ++var5) {
                        if ((bt.landscapeData[1][var4][var5] & 2) == 2) {
                            var60.i(var4, var5);
                        }
                    }
                }

                var4 = 1;
                var5 = 2;
                var6 = 4;

                for (var62 = 0; var62 < 4; ++var62) {
                    if (var62 > 0) {
                        var4 <<= 3;
                        var5 <<= 3;
                        var6 <<= 3;
                    }

                    for (var47 = 0; var47 <= var62; ++var47) {
                        for (var9 = 0; var9 <= 104; ++var9) {
                            for (var10 = 0; var10 <= 104; ++var10) {
                                short var66;
                                if ((bt.u[var47][var10][var9] & var4) != 0) {
                                    var11 = var9;
                                    var64 = var9;
                                    var13 = var47;

                                    for (var14 = var47; var11 > 0 && (bt.u[var47][var10][var11 - 1] & var4) != 0; --var11) {
                                        ;
                                    }

                                    while (var64 < 104 && (bt.u[var47][var10][var64 + 1] & var4) != 0) {
                                        ++var64;
                                    }

                                    label877: while (var13 > 0) {
                                        for (var15 = var11; var15 <= var64; ++var15) {
                                            if ((bt.u[var13 - 1][var10][var15] & var4) == 0) {
                                                break label877;
                                            }
                                        }

                                        --var13;
                                    }

                                    label866: while (var14 < var62) {
                                        for (var15 = var11; var15 <= var64; ++var15) {
                                            if ((bt.u[var14 + 1][var10][var15] & var4) == 0) {
                                                break label866;
                                            }
                                        }

                                        ++var14;
                                    }

                                    var15 = (var14 + 1 - var13) * (var64 - var11 + 1);
                                    if (var15 >= 8) {
                                        var66 = 240;
                                        var17 = bt.tileHeights[var14][var10][var11] - var66;
                                        var18 = bt.tileHeights[var13][var10][var11];
                                        Region.a(var62, 1, var10 * 128, var10 * 128, var11 * 128, var64 * 128 + 128,
                                                var17, var18);

                                        for (var54 = var13; var54 <= var14; ++var54) {
                                            for (var20 = var11; var20 <= var64; ++var20) {
                                                bt.u[var54][var10][var20] &= ~var4;
                                            }
                                        }
                                    }
                                }

                                if ((bt.u[var47][var10][var9] & var5) != 0) {
                                    var11 = var10;
                                    var64 = var10;
                                    var13 = var47;

                                    for (var14 = var47; var11 > 0 && (bt.u[var47][var11 - 1][var9] & var5) != 0; --var11) {
                                        ;
                                    }

                                    while (var64 < 104 && (bt.u[var47][var64 + 1][var9] & var5) != 0) {
                                        ++var64;
                                    }

                                    label930: while (var13 > 0) {
                                        for (var15 = var11; var15 <= var64; ++var15) {
                                            if ((bt.u[var13 - 1][var15][var9] & var5) == 0) {
                                                break label930;
                                            }
                                        }

                                        --var13;
                                    }

                                    label919: while (var14 < var62) {
                                        for (var15 = var11; var15 <= var64; ++var15) {
                                            if ((bt.u[var14 + 1][var15][var9] & var5) == 0) {
                                                break label919;
                                            }
                                        }

                                        ++var14;
                                    }

                                    var15 = (var64 - var11 + 1) * (var14 + 1 - var13);
                                    if (var15 >= 8) {
                                        var66 = 240;
                                        var17 = bt.tileHeights[var14][var11][var9] - var66;
                                        var18 = bt.tileHeights[var13][var11][var9];
                                        Region.a(var62, 2, var11 * 128, var64 * 128 + 128, var9 * 128, var9 * 128,
                                                var17, var18);

                                        for (var54 = var13; var54 <= var14; ++var54) {
                                            for (var20 = var11; var20 <= var64; ++var20) {
                                                bt.u[var54][var20][var9] &= ~var5;
                                            }
                                        }
                                    }
                                }

                                if ((bt.u[var47][var10][var9] & var6) != 0) {
                                    var11 = var10;
                                    var64 = var10;
                                    var13 = var9;

                                    for (var14 = var9; var13 > 0 && (bt.u[var47][var10][var13 - 1] & var6) != 0; --var13) {
                                        ;
                                    }

                                    while (var14 < 104 && (bt.u[var47][var10][var14 + 1] & var6) != 0) {
                                        ++var14;
                                    }

                                    label983: while (var11 > 0) {
                                        for (var15 = var13; var15 <= var14; ++var15) {
                                            if ((bt.u[var47][var11 - 1][var15] & var6) == 0) {
                                                break label983;
                                            }
                                        }

                                        --var11;
                                    }

                                    label972: while (var64 < 104) {
                                        for (var15 = var13; var15 <= var14; ++var15) {
                                            if ((bt.u[var47][var64 + 1][var15] & var6) == 0) {
                                                break label972;
                                            }
                                        }

                                        ++var64;
                                    }

                                    if ((var14 - var13 + 1) * (var64 - var11 + 1) >= 4) {
                                        var15 = bt.tileHeights[var47][var11][var13];
                                        Region.a(var62, 4, var11 * 128, var64 * 128 + 128, var13 * 128,
                                                var14 * 128 + 128, var15, var15);

                                        for (var16 = var11; var16 <= var64; ++var16) {
                                            for (var17 = var13; var17 <= var14; ++var17) {
                                                bt.u[var47][var16][var17] &= ~var6;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                hb.gv(true);
                var4 = bt.i;
                if (var4 > kt.ii) {
                    var4 = kt.ii;
                }

                if (var4 < kt.ii - 1) {
                    var4 = kt.ii - 1;
                }

                if (Client.bh) {
                    an.loadedRegion.q(bt.i);
                } else {
                    an.loadedRegion.q(0);
                }

                for (var5 = 0; var5 < 104; ++var5) {
                    for (var6 = 0; var6 < 104; ++var6) {
                        Canvas.hj(var5, var6);
                    }
                }

                al.fb();

                for (bl var69 = (bl) Client.jw.e(); var69 != null; var69 = (bl) Client.jw.p()) {
                    if (var69.o == -1) {
                        var69.n = 0;
                        Inflater.hq(var69);
                    } else {
                        var69.kc();
                    }
                }

                ObjectDefinition.l.a();
                gd var70;
                if (ir.ac.af()) {
                    var70 = ap.t(fo.ay, Client.ee.l);
                    var70.i.e(1057001181);
                    Client.ee.i(var70);
                }

                if (!Client.dynamicRegion) {
                    var5 = (ab.ef - 6) / 8;
                    var6 = (ab.ef + 6) / 8;
                    var62 = (gd.eq - 6) / 8;
                    var47 = (gd.eq + 6) / 8;

                    for (var9 = var5 - 1; var9 <= var6 + 1; ++var9) {
                        for (var10 = var62 - 1; var10 <= var47 + 1; ++var10) {
                            if (var9 < var5 || var9 > var6 || var10 < var62 || var10 > var47) {
                                ee.cn.ap("m" + var9 + "_" + var10);
                                ee.cn.ap("l" + var9 + "_" + var10);
                            }
                        }
                    }
                }

                d.ea(30);
                al.fb();
                ao.t();
                var70 = ap.t(fo.ab, Client.ee.l);
                Client.ee.i(var70);
                p.am();
            }
        }
    }
}
