import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gt")
public class gt {

    @ObfuscatedName("g")
    static final int[] g = new int[2048];

    @ObfuscatedName("n")
    static final int[] n = new int[2048];

    static {
        double var0 = 0.0030679615757712823D;

        for (int var2 = 0; var2 < 2048; ++var2) {
            g[var2] = (int) (65536.0D * Math.sin((double) var2 * var0));
            n[var2] = (int) (65536.0D * Math.cos((double) var2 * var0));
        }

    }

    @ObfuscatedName("o")
    public static void o() {
        PlayerComposite.o.a();
    }

    @ObfuscatedName("o")
    static int o(int var0, RuneScript var1, boolean var2) {
        RTComponent var3;
        if (var0 >= 2000) {
            var0 -= 1000;
            var3 = Inflater.t(cs.e[--b.menuX]);
        } else {
            var3 = var2 ? ho.v : cs.c;
        }

        if (var0 == 1927) {
            if (cs.z >= 10) {
                throw new RuntimeException();
            } else if (var3.dm == null) {
                return 0;
            } else {
                ScriptEvent var4 = new ScriptEvent();
                var4.i = var3;
                var4.args = var3.dm;
                var4.n = cs.z + 1;
                Client.me.q(var4);
                return 1;
            }
        } else {
            return 2;
        }
    }
}
