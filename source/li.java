import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("li")
public class li extends hh {
   @ObfuscatedName("ao")
   public static int[] ao;
   @ObfuscatedName("av")
   public static int av;
   @ObfuscatedName("aj")
   public static int aj;
   @ObfuscatedName("ae")
   public static int ae = 0;
   @ObfuscatedName("am")
   public static int am = 0;
   @ObfuscatedName("az")
   public static int az = 0;
   @ObfuscatedName("ap")
   protected static int ap = 0;

   @ObfuscatedName("cf")
   public static void cf(int[] var0, int var1, int var2) {
      ao = var0;
      av = var1;
      aj = var2;
      ck(0, 0, var1, var2);
   }

   @ObfuscatedName("cp")
   public static void cp() {
      az = 0;
      ae = 0;
      ap = av;
      am = aj;
   }

   @ObfuscatedName("ck")
   public static void ck(int var0, int var1, int var2, int var3) {
      if (var0 < 0) {
         var0 = 0;
      }

      if (var1 < 0) {
         var1 = 0;
      }

      if (var2 > av) {
         var2 = av;
      }

      if (var3 > aj) {
         var3 = aj;
      }

      az = var0;
      ae = var1;
      ap = var2;
      am = var3;
   }

   @ObfuscatedName("db")
   public static void db(int var0, int var1, int var2, int var3) {
      if (az < var0) {
         az = var0;
      }

      if (ae < var1) {
         ae = var1;
      }

      if (ap > var2) {
         ap = var2;
      }

      if (am > var3) {
         am = var3;
      }

   }

   @ObfuscatedName("dp")
   public static void dp(int[] var0) {
      var0[0] = az;
      var0[1] = ae;
      var0[2] = ap;
      var0[3] = am;
   }

   @ObfuscatedName("da")
   public static void da(int[] var0) {
      az = var0[0];
      ae = var0[1];
      ap = var0[2];
      am = var0[3];
   }

   @ObfuscatedName("dr")
   public static void dr() {
      int var0 = 0;

      int var1;
      for(var1 = av * aj - 7; var0 < var1; ao[var0++] = 0) {
         ao[var0++] = 0;
         ao[var0++] = 0;
         ao[var0++] = 0;
         ao[var0++] = 0;
         ao[var0++] = 0;
         ao[var0++] = 0;
         ao[var0++] = 0;
      }

      for(var1 += 7; var0 < var1; ao[var0++] = 0) {
         ;
      }

   }

   @ObfuscatedName("dj")
   static void dj(int var0, int var1, int var2, int var3) {
      if (var2 == 0) {
         di(var0, var1, var3);
      } else {
         if (var2 < 0) {
            var2 = -var2;
         }

         int var4 = var1 - var2;
         if (var4 < ae) {
            var4 = ae;
         }

         int var5 = var2 + var1 + 1;
         if (var5 > am) {
            var5 = am;
         }

         int var6 = var4;
         int var7 = var2 * var2;
         int var8 = 0;
         int var9 = var1 - var4;
         int var10 = var9 * var9;
         int var11 = var10 - var9;
         if (var1 > var5) {
            var1 = var5;
         }

         int var12;
         int var13;
         int var14;
         int var15;
         while(var6 < var1) {
            while(var11 <= var7 || var10 <= var7) {
               var10 = var10 + var8 + var8;
               var11 += var8++ + var8;
            }

            var12 = var0 - var8 + 1;
            if (var12 < az) {
               var12 = az;
            }

            var13 = var0 + var8;
            if (var13 > ap) {
               var13 = ap;
            }

            var14 = var12 + var6 * av;

            for(var15 = var12; var15 < var13; ++var15) {
               ao[var14++] = var3;
            }

            ++var6;
            var10 -= var9-- + var9;
            var11 -= var9 + var9;
         }

         var8 = var2;
         var9 = var6 - var1;
         var11 = var7 + var9 * var9;
         var10 = var11 - var2;

         for(var11 -= var9; var6 < var5; var10 += var9++ + var9) {
            while(var11 > var7 && var10 > var7) {
               var11 -= var8-- + var8;
               var10 -= var8 + var8;
            }

            var12 = var0 - var8;
            if (var12 < az) {
               var12 = az;
            }

            var13 = var0 + var8;
            if (var13 > ap - 1) {
               var13 = ap - 1;
            }

            var14 = var12 + var6 * av;

            for(var15 = var12; var15 <= var13; ++var15) {
               ao[var14++] = var3;
            }

            ++var6;
            var11 = var11 + var9 + var9;
         }

      }
   }

   @ObfuscatedName("dh")
   public static void dh(int var0, int var1, int var2, int var3, int var4) {
      if (var4 != 0) {
         if (var4 == 256) {
            dj(var0, var1, var2, var3);
         } else {
            if (var2 < 0) {
               var2 = -var2;
            }

            int var5 = 256 - var4;
            int var6 = (var3 >> 16 & 255) * var4;
            int var7 = (var3 >> 8 & 255) * var4;
            int var8 = var4 * (var3 & 255);
            int var12 = var1 - var2;
            if (var12 < ae) {
               var12 = ae;
            }

            int var13 = var2 + var1 + 1;
            if (var13 > am) {
               var13 = am;
            }

            int var14 = var12;
            int var15 = var2 * var2;
            int var16 = 0;
            int var17 = var1 - var12;
            int var18 = var17 * var17;
            int var19 = var18 - var17;
            if (var1 > var13) {
               var1 = var13;
            }

            int var9;
            int var10;
            int var11;
            int var20;
            int var21;
            int var22;
            int var23;
            int var24;
            while(var14 < var1) {
               while(var19 <= var15 || var18 <= var15) {
                  var18 = var18 + var16 + var16;
                  var19 += var16++ + var16;
               }

               var20 = var0 - var16 + 1;
               if (var20 < az) {
                  var20 = az;
               }

               var21 = var0 + var16;
               if (var21 > ap) {
                  var21 = ap;
               }

               var22 = var20 + var14 * av;

               for(var23 = var20; var23 < var21; ++var23) {
                  var9 = var5 * (ao[var22] >> 16 & 255);
                  var10 = (ao[var22] >> 8 & 255) * var5;
                  var11 = var5 * (ao[var22] & 255);
                  var24 = (var8 + var11 >> 8) + (var6 + var9 >> 8 << 16) + (var7 + var10 >> 8 << 8);
                  ao[var22++] = var24;
               }

               ++var14;
               var18 -= var17-- + var17;
               var19 -= var17 + var17;
            }

            var16 = var2;
            var17 = -var17;
            var19 = var15 + var17 * var17;
            var18 = var19 - var2;

            for(var19 -= var17; var14 < var13; var18 += var17++ + var17) {
               while(var19 > var15 && var18 > var15) {
                  var19 -= var16-- + var16;
                  var18 -= var16 + var16;
               }

               var20 = var0 - var16;
               if (var20 < az) {
                  var20 = az;
               }

               var21 = var0 + var16;
               if (var21 > ap - 1) {
                  var21 = ap - 1;
               }

               var22 = var20 + var14 * av;

               for(var23 = var20; var23 <= var21; ++var23) {
                  var9 = var5 * (ao[var22] >> 16 & 255);
                  var10 = (ao[var22] >> 8 & 255) * var5;
                  var11 = var5 * (ao[var22] & 255);
                  var24 = (var8 + var11 >> 8) + (var6 + var9 >> 8 << 16) + (var7 + var10 >> 8 << 8);
                  ao[var22++] = var24;
               }

               ++var14;
               var19 = var19 + var17 + var17;
            }

         }
      }
   }

   @ObfuscatedName("dc")
   public static void dc(int var0, int var1, int var2, int var3, int var4, int var5) {
      if (var0 < az) {
         var2 -= az - var0;
         var0 = az;
      }

      if (var1 < ae) {
         var3 -= ae - var1;
         var1 = ae;
      }

      if (var0 + var2 > ap) {
         var2 = ap - var0;
      }

      if (var3 + var1 > am) {
         var3 = am - var1;
      }

      var4 = (var5 * (var4 & 16711935) >> 8 & 16711935) + (var5 * (var4 & '\uff00') >> 8 & '\uff00');
      int var6 = 256 - var5;
      int var7 = av - var2;
      int var8 = var0 + av * var1;

      for(int var9 = 0; var9 < var3; ++var9) {
         for(int var10 = -var2; var10 < 0; ++var10) {
            int var11 = ao[var8];
            var11 = ((var11 & 16711935) * var6 >> 8 & 16711935) + (var6 * (var11 & '\uff00') >> 8 & '\uff00');
            ao[var8++] = var11 + var4;
         }

         var8 += var7;
      }

   }

   @ObfuscatedName("dl")
   public static void dl(int var0, int var1, int var2, int var3, int var4) {
      if (var0 < az) {
         var2 -= az - var0;
         var0 = az;
      }

      if (var1 < ae) {
         var3 -= ae - var1;
         var1 = ae;
      }

      if (var0 + var2 > ap) {
         var2 = ap - var0;
      }

      if (var3 + var1 > am) {
         var3 = am - var1;
      }

      int var5 = av - var2;
      int var6 = var0 + av * var1;

      for(int var7 = -var3; var7 < 0; ++var7) {
         for(int var8 = -var2; var8 < 0; ++var8) {
            ao[var6++] = var4;
         }

         var6 += var5;
      }

   }

   @ObfuscatedName("df")
   public static void df(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if (var2 > 0 && var3 > 0) {
         int var8 = 0;
         int var9 = var5 == var4 && var7 == var6 ? -1 : 65536 / var3;
         int var10 = var6;
         int var11 = 256 - var6;
         int var12 = var4;
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var1 < ae) {
            var8 += (ae - var1) * var9;
            var3 -= ae - var1;
            var1 = ae;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         if (var3 + var1 > am) {
            var3 = am - var1;
         }

         int var13 = av - var2;
         int var14 = var0 + av * var1;

         for(int var15 = -var3; var15 < 0; ++var15) {
            int var16;
            int var17;
            for(var16 = -var2; var16 < 0; ++var16) {
               var17 = ao[var14];
               int var18 = var12 + var17;
               int var19 = (var12 & 16711935) + (var17 & 16711935);
               int var20 = (var19 & 16777472) + (var18 - var19 & 65536);
               if (var11 == 0) {
                  ao[var14++] = var18 - var20 | var20 - (var20 >>> 8);
               } else {
                  int var21 = var18 - var20 | var20 - (var20 >>> 8);
                  ao[var14++] = ((var21 & 16711935) * var10 >> 8 & 16711935) + ((var17 & 16711935) * var11 >> 8 & 16711935) + (var10 * (var21 & '\uff00') >> 8 & '\uff00') + (var11 * (var17 & '\uff00') >> 8 & '\uff00');
               }
            }

            if (var9 > 0) {
               var8 += var9;
               var16 = 65536 - var8 >> 8;
               var17 = var8 >> 8;
               if (var7 != var6) {
                  var10 = (65536 - var8) * var6 + var8 * var7 >> 16;
                  var11 = 256 - var10;
               }

               if (var5 != var4) {
                  var12 = (var17 * (var5 & 16711935) + var16 * (var4 & 16711935) & -16711936) + (var17 * (var5 & '\uff00') + var16 * (var4 & '\uff00') & 16711680) >>> 8;
               }
            }

            var14 += var13;
         }

      }
   }

   @ObfuscatedName("dq")
   public static void dq(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if (var2 > 0 && var3 > 0) {
         int var8 = 0;
         int var9 = var5 == var4 && var7 == var6 ? -1 : 65536 / var3;
         int var10 = var6;
         int var11 = 256 - var6;
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var1 < ae) {
            var8 += (ae - var1) * var9;
            var3 -= ae - var1;
            var1 = ae;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         if (var3 + var1 > am) {
            var3 = am - var1;
         }

         int var12 = var4 >> 16;
         int var13 = (var4 & '\uff00') >> 8;
         int var14 = var4 & 255;
         int var18 = av - var2;
         int var19 = var0 + av * var1;

         for(int var20 = 0; var20 < var3; ++var20) {
            int var21;
            int var22;
            int var23;
            for(var21 = -var2; var21 < 0; ++var21) {
               var22 = ao[var19];
               var23 = var22 >> 16;
               int var24 = (var22 & '\uff00') >> 8;
               int var25 = var22 & 255;
               int var15;
               int var16;
               int var17;
               if (var11 == 0) {
                  var15 = var23 < 127 ? var12 * var23 >> 7 : 255 - ((255 - var12) * (255 - var23) >> 7);
                  var16 = var24 < 127 ? var13 * var24 >> 7 : 255 - ((255 - var13) * (255 - var24) >> 7);
                  var17 = var25 < 127 ? var14 * var25 >> 7 : 255 - ((255 - var14) * (255 - var25) >> 7);
               } else {
                  var15 = var23 < 127 ? var23 * var11 + (var12 * var23 * var10 >> 7) >> 8 : var10 * (255 - ((255 - var12) * (255 - var23) >> 7)) + var23 * var11 >> 8;
                  var16 = var24 < 127 ? var24 * var11 + (var13 * var24 * var10 >> 7) >> 8 : var10 * (255 - ((255 - var13) * (255 - var24) >> 7)) + var24 * var11 >> 8;
                  var17 = var25 < 127 ? var25 * var11 + (var14 * var25 * var10 >> 7) >> 8 : var10 * (255 - ((255 - var14) * (255 - var25) >> 7)) + var25 * var11 >> 8;
               }

               ao[var19++] = var17 + (var16 << 8) + (var15 << 16);
            }

            if (var9 > 0) {
               var8 += var9;
               var21 = 65536 - var8 >> 8;
               var22 = var8 >> 8;
               if (var7 != var6) {
                  var10 = (65536 - var8) * var6 + var8 * var7 >> 16;
                  var11 = 256 - var10;
               }

               if (var5 != var4) {
                  var23 = (var22 * (var5 & 16711935) + var21 * (var4 & 16711935) & -16711936) + (var22 * (var5 & '\uff00') + var21 * (var4 & '\uff00') & 16711680) >>> 8;
                  var12 = var23 >> 16;
                  var13 = (var23 & '\uff00') >> 8;
                  var14 = var23 & 255;
               }
            }

            var19 += var18;
         }

      }
   }

   @ObfuscatedName("dt")
   public static void dt(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if (var2 > 0 && var3 > 0) {
         int var8 = 0;
         int var9 = 65536 / var3;
         int var10 = var6;
         int var11 = 256 - var6;
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var1 < ae) {
            var8 += (ae - var1) * var9;
            var3 -= ae - var1;
            var1 = ae;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         if (var3 + var1 > am) {
            var3 = am - var1;
         }

         int var12 = var4 & 16711680;
         int var13 = var4 & '\uff00';
         int var14 = var4 & 255;
         int var15 = var12 * var6 >> 8;
         int var16 = var13 * var6 >> 8;
         int var17 = var14 * var6 >> 8;
         int var18 = av - var2;
         int var19 = var0 + av * var1;

         for(int var20 = 0; var20 < var3; ++var20) {
            int var21;
            int var22;
            int var23;
            for(var21 = -var2; var21 < 0; ++var21) {
               var22 = ao[var19];
               var23 = var22 & 16711680;
               int var24 = var23 <= var12 ? var23 : (var11 == 0 ? var12 : var15 + (var23 * var11 >> 8) & 16711680);
               int var25 = var22 & '\uff00';
               int var26 = var25 <= var13 ? var25 : (var11 == 0 ? var13 : var16 + (var25 * var11 >> 8) & '\uff00');
               int var27 = var22 & 255;
               int var28 = var27 <= var14 ? var27 : (var11 == 0 ? var14 : var17 + (var27 * var11 >> 8));
               ao[var19++] = var24 + var26 + var28;
            }

            if (var9 > 0) {
               var8 += var9;
               var21 = 65536 - var8 >> 8;
               var22 = var8 >> 8;
               if (var7 != var6) {
                  var10 = (65536 - var8) * var6 + var8 * var7 >> 16;
                  var11 = 256 - var10;
               }

               if (var5 != var4) {
                  var23 = (var22 * (var5 & 16711935) + var21 * (var4 & 16711935) & -16711936) + (var22 * (var5 & '\uff00') + var21 * (var4 & '\uff00') & 16711680) >>> 8;
                  var12 = var23 & 16711680;
                  var13 = var23 & '\uff00';
                  var14 = var23 & 255;
                  var15 = var12 * var10 >> 8;
                  var16 = var13 * var10 >> 8;
                  var17 = var14 * var10 >> 8;
               }
            }

            var19 += var18;
         }

      }
   }

   @ObfuscatedName("dy")
   public static void dy(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if (var3 > 0 && var2 > 0) {
         int var8 = 0;
         int var9 = 65536 / var3;
         int var10 = var6;
         int var11 = 256 - var6;
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var1 < ae) {
            var8 += (ae - var1) * var9;
            var3 -= ae - var1;
            var1 = ae;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         if (var3 + var1 > am) {
            var3 = am - var1;
         }

         int var12 = var4 & 16711680;
         int var13 = var4 & '\uff00';
         int var14 = var4 & 255;
         int var15 = var12 * var6 >> 8;
         int var16 = var13 * var6 >> 8;
         int var17 = var14 * var6 >> 8;
         int var18 = av - var2;
         int var19 = var0 + av * var1;

         for(int var20 = 0; var20 < var3; ++var20) {
            int var21;
            int var22;
            int var23;
            for(var21 = -var2; var21 < 0; ++var21) {
               var22 = ao[var19];
               var23 = var22 & 16711680;
               int var24 = var23 >= var12 ? var23 : (var11 == 0 ? var12 : var15 + (var23 * var11 >> 8) & 16711680);
               int var25 = var22 & '\uff00';
               int var26 = var25 >= var13 ? var25 : (var11 == 0 ? var13 : var16 + (var25 * var11 >> 8) & '\uff00');
               int var27 = var22 & 255;
               int var28 = var27 >= var14 ? var27 : (var11 == 0 ? var14 : var17 + (var27 * var11 >> 8));
               ao[var19++] = var24 + var26 + var28;
            }

            if (var9 > 0) {
               var8 += var9;
               var21 = 65536 - var8 >> 8;
               var22 = var8 >> 8;
               if (var7 != var6) {
                  var10 = (65536 - var8) * var6 + var8 * var7 >> 16;
                  var11 = 256 - var10;
               }

               if (var5 != var4) {
                  var23 = (var22 * (var5 & 16711935) + var21 * (var4 & 16711935) & -16711936) + (var22 * (var5 & '\uff00') + var21 * (var4 & '\uff00') & 16711680) >>> 8;
                  var12 = var23 & 16711680;
                  var13 = var23 & '\uff00';
                  var14 = var23 & 255;
                  var15 = var12 * var10 >> 8;
                  var16 = var13 * var10 >> 8;
                  var17 = var14 * var10 >> 8;
               }
            }

            var19 += var18;
         }

      }
   }

   @ObfuscatedName("dn")
   public static void dn(int var0, int var1, int var2, int var3, int var4, int var5) {
      if (var2 > 0 && var3 > 0) {
         int var6 = 0;
         int var7 = 65536 / var3;
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var1 < ae) {
            var6 += (ae - var1) * var7;
            var3 -= ae - var1;
            var1 = ae;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         if (var3 + var1 > am) {
            var3 = am - var1;
         }

         int var8 = av - var2;
         int var9 = var0 + av * var1;

         for(int var10 = -var3; var10 < 0; ++var10) {
            int var11 = 65536 - var6 >> 8;
            int var12 = var6 >> 8;
            int var13 = (var12 * (var5 & 16711935) + var11 * (var4 & 16711935) & -16711936) + (var12 * (var5 & '\uff00') + var11 * (var4 & '\uff00') & 16711680) >>> 8;

            for(int var14 = -var2; var14 < 0; ++var14) {
               ao[var9++] = var13;
            }

            var9 += var8;
            var6 += var7;
         }

      }
   }

   @ObfuscatedName("do")
   public static void do(int var0, int var1, int var2, int var3, int var4, int var5, byte[] var6, int var7) {
      if (var0 + var2 >= 0 && var3 + var1 >= 0) {
         if (var0 < av && var1 < aj) {
            int var8 = 0;
            int var9 = 0;
            if (var0 < 0) {
               var8 -= var0;
               var2 += var0;
            }

            if (var1 < 0) {
               var9 -= var1;
               var3 += var1;
            }

            if (var0 + var2 > av) {
               var2 = av - var0;
            }

            if (var3 + var1 > aj) {
               var3 = aj - var1;
            }

            int var10 = var6.length / var7;
            int var11 = av - var2;
            int var12 = var4 >>> 24;
            int var13 = var5 >>> 24;
            int var14;
            int var15;
            int var16;
            int var17;
            int var18;
            if (var12 == 255 && var13 == 255) {
               var14 = var0 + var8 + (var9 + var1) * av;

               for(var15 = var9 + var1; var15 < var3 + var9 + var1; ++var15) {
                  for(var16 = var0 + var8; var16 < var0 + var8 + var2; ++var16) {
                     var17 = (var15 - var1) % var10;
                     var18 = (var16 - var0) % var7;
                     if (var6[var18 + var17 * var7] != 0) {
                        ao[var14++] = var5;
                     } else {
                        ao[var14++] = var4;
                     }
                  }

                  var14 += var11;
               }
            } else {
               var14 = var0 + var8 + (var9 + var1) * av;

               for(var15 = var9 + var1; var15 < var3 + var9 + var1; ++var15) {
                  for(var16 = var0 + var8; var16 < var0 + var8 + var2; ++var16) {
                     var17 = (var15 - var1) % var10;
                     var18 = (var16 - var0) % var7;
                     int var19 = var4;
                     if (var6[var18 + var17 * var7] != 0) {
                        var19 = var5;
                     }

                     int var20 = var19 >>> 24;
                     int var21 = 255 - var20;
                     int var22 = ao[var14];
                     int var23 = ((var19 & 16711935) * var20 + (var22 & 16711935) * var21 & -16711936) + (var20 * (var19 & '\uff00') + var21 * (var22 & '\uff00') & 16711680) >> 8;
                     ao[var14++] = var23;
                  }

                  var14 += var11;
               }
            }

         }
      }
   }

   @ObfuscatedName("dw")
   public static void dw(int var0, int var1, int var2, int var3, int var4) {
      du(var0, var1, var2, var4);
      du(var0, var3 + var1 - 1, var2, var4);
      de(var0, var1, var3, var4);
      de(var0 + var2 - 1, var1, var3, var4);
   }

   @ObfuscatedName("dd")
   public static void dd(int var0, int var1, int var2, int var3, int var4, int var5) {
      dk(var0, var1, var2, var4, var5);
      dk(var0, var3 + var1 - 1, var2, var4, var5);
      if (var3 >= 3) {
         dg(var0, var1 + 1, var3 - 2, var4, var5);
         dg(var0 + var2 - 1, var1 + 1, var3 - 2, var4, var5);
      }

   }

   @ObfuscatedName("du")
   public static void du(int var0, int var1, int var2, int var3) {
      if (var1 >= ae && var1 < am) {
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         int var4 = var0 + av * var1;

         for(int var5 = 0; var5 < var2; ++var5) {
            ao[var4 + var5] = var3;
         }

      }
   }

   @ObfuscatedName("dk")
   static void dk(int var0, int var1, int var2, int var3, int var4) {
      if (var1 >= ae && var1 < am) {
         if (var0 < az) {
            var2 -= az - var0;
            var0 = az;
         }

         if (var0 + var2 > ap) {
            var2 = ap - var0;
         }

         int var5 = 256 - var4;
         int var6 = (var3 >> 16 & 255) * var4;
         int var7 = (var3 >> 8 & 255) * var4;
         int var8 = var4 * (var3 & 255);
         int var12 = var0 + av * var1;

         for(int var13 = 0; var13 < var2; ++var13) {
            int var9 = var5 * (ao[var12] >> 16 & 255);
            int var10 = (ao[var12] >> 8 & 255) * var5;
            int var11 = var5 * (ao[var12] & 255);
            int var14 = (var8 + var11 >> 8) + (var6 + var9 >> 8 << 16) + (var7 + var10 >> 8 << 8);
            ao[var12++] = var14;
         }

      }
   }

   @ObfuscatedName("de")
   public static void de(int var0, int var1, int var2, int var3) {
      if (var0 >= az && var0 < ap) {
         if (var1 < ae) {
            var2 -= ae - var1;
            var1 = ae;
         }

         if (var2 + var1 > am) {
            var2 = am - var1;
         }

         int var4 = var0 + av * var1;

         for(int var5 = 0; var5 < var2; ++var5) {
            ao[var4 + var5 * av] = var3;
         }

      }
   }

   @ObfuscatedName("dg")
   static void dg(int var0, int var1, int var2, int var3, int var4) {
      if (var0 >= az && var0 < ap) {
         if (var1 < ae) {
            var2 -= ae - var1;
            var1 = ae;
         }

         if (var2 + var1 > am) {
            var2 = am - var1;
         }

         int var5 = 256 - var4;
         int var6 = (var3 >> 16 & 255) * var4;
         int var7 = (var3 >> 8 & 255) * var4;
         int var8 = var4 * (var3 & 255);
         int var12 = var0 + av * var1;

         for(int var13 = 0; var13 < var2; ++var13) {
            int var9 = var5 * (ao[var12] >> 16 & 255);
            int var10 = (ao[var12] >> 8 & 255) * var5;
            int var11 = var5 * (ao[var12] & 255);
            int var14 = (var8 + var11 >> 8) + (var6 + var9 >> 8 << 16) + (var7 + var10 >> 8 << 8);
            ao[var12] = var14;
            var12 += av;
         }

      }
   }

   @ObfuscatedName("dx")
   public static void dx(int var0, int var1, int var2, int var3, int var4) {
      var2 -= var0;
      var3 -= var1;
      if (var3 == 0) {
         if (var2 >= 0) {
            du(var0, var1, var2 + 1, var4);
         } else {
            du(var0 + var2, var1, -var2 + 1, var4);
         }

      } else if (var2 == 0) {
         if (var3 >= 0) {
            de(var0, var1, var3 + 1, var4);
         } else {
            de(var0, var3 + var1, -var3 + 1, var4);
         }

      } else {
         if (var3 + var2 < 0) {
            var0 += var2;
            var2 = -var2;
            var1 += var3;
            var3 = -var3;
         }

         int var5;
         int var6;
         if (var2 > var3) {
            var1 <<= 16;
            var1 += 32768;
            var3 <<= 16;
            var5 = (int)Math.floor((double)var3 / (double)var2 + 0.5D);
            var2 += var0;
            if (var0 < az) {
               var1 += var5 * (az - var0);
               var0 = az;
            }

            if (var2 >= ap) {
               var2 = ap - 1;
            }

            while(var0 <= var2) {
               var6 = var1 >> 16;
               if (var6 >= ae && var6 < am) {
                  ao[var0 + var6 * av] = var4;
               }

               var1 += var5;
               ++var0;
            }
         } else {
            var0 <<= 16;
            var0 += 32768;
            var2 <<= 16;
            var5 = (int)Math.floor((double)var2 / (double)var3 + 0.5D);
            var3 += var1;
            if (var1 < ae) {
               var0 += (ae - var1) * var5;
               var1 = ae;
            }

            if (var3 >= am) {
               var3 = am - 1;
            }

            while(var1 <= var3) {
               var6 = var0 >> 16;
               if (var6 >= az && var6 < ap) {
                  ao[var6 + av * var1] = var4;
               }

               var0 += var5;
               ++var1;
            }
         }

      }
   }

   @ObfuscatedName("di")
   static void di(int var0, int var1, int var2) {
      if (var0 >= az && var1 >= ae && var0 < ap && var1 < am) {
         ao[var0 + av * var1] = var2;
      }
   }

   @ObfuscatedName("dv")
   public static void dv(int var0, int var1, int var2, int[] var3, int[] var4) {
      int var5 = var0 + av * var1;

      for(var1 = 0; var1 < var3.length; ++var1) {
         int var6 = var5 + var3[var1];

         for(var0 = -var4[var1]; var0 < 0; ++var0) {
            ao[var6++] = var2;
         }

         var5 += av;
      }

   }
}
