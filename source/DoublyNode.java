import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hv")
public class DoublyNode implements Iterable {

    @ObfuscatedName("t")
    public hh t = new hh();

    @ObfuscatedName("q")
    hh nextDoublyNode;

    public DoublyNode() {
        this.t.cj = this.t;
        this.t.cl = this.t;
    }

    @ObfuscatedName("t")
    public void t() {
        while (this.t.cj != this.t) {
            this.t.cj.ce();
        }

    }

    @ObfuscatedName("q")
    public void q(hh var1) {
        if (var1.cl != null) {
            var1.ce();
        }

        var1.cl = this.t.cl;
        var1.cj = this.t;
        var1.cl.cj = var1;
        var1.cj.cl = var1;
    }

    @ObfuscatedName("i")
    hh i() {
        hh var1 = this.t.cj;
        if (var1 == this.t) {
            return null;
        } else {
            var1.ce();
            return var1;
        }
    }

    @ObfuscatedName("a")
    hh a() {
        return this.l((hh) null);
    }

    @ObfuscatedName("l")
    hh l(hh var1) {
        hh var2;
        if (var1 == null) {
            var2 = this.t.cj;
        } else {
            var2 = var1;
        }

        if (var2 == this.t) {
            this.nextDoublyNode = null;
            return null;
        } else {
            this.nextDoublyNode = var2.cj;
            return var2;
        }
    }

    @ObfuscatedName("b")
    hh b() {
        hh var1 = this.nextDoublyNode;
        if (var1 == this.t) {
            this.nextDoublyNode = null;
            return null;
        } else {
            this.nextDoublyNode = var1.cj;
            return var1;
        }
    }

    public Iterator iterator() {
        return new hu(this);
    }
}
