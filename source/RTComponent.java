import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ig")
public class RTComponent extends Node {
   @ObfuscatedName("b")
   public static RTComponent[][] b;
   @ObfuscatedName("x")
   public static FileSystem x;
   @ObfuscatedName("o")
   static Cache o = new Cache(200);
   @ObfuscatedName("c")
   static Cache c = new Cache(50);
   @ObfuscatedName("v")
   static Cache v = new Cache(20);
   @ObfuscatedName("u")
   static Cache u = new Cache(8);
   @ObfuscatedName("j")
   public static boolean j = false;
   @ObfuscatedName("k")
   public boolean modern = false;
   @ObfuscatedName("z")
   public int id = -1;
   @ObfuscatedName("w")
   public int w = -1;
   @ObfuscatedName("s")
   public int type;
   @ObfuscatedName("d")
   public int d = 0;
   @ObfuscatedName("f")
   public int contentType = 0;
   @ObfuscatedName("r")
   public int r = 0;
   @ObfuscatedName("y")
   public int y = 0;
   @ObfuscatedName("h")
   public int h = 0;
   @ObfuscatedName("m")
   public int m = 0;
   @ObfuscatedName("ay")
   public int ay = 0;
   @ObfuscatedName("ao")
   public int ao = 0;
   @ObfuscatedName("av")
   public int av = 0;
   @ObfuscatedName("aj")
   public int aj = 0;
   @ObfuscatedName("ae")
   public int relativeX = 0;
   @ObfuscatedName("am")
   public int relativeY = 0;
   @ObfuscatedName("az")
   public int width = 0;
   @ObfuscatedName("ap")
   public int height = 0;
   @ObfuscatedName("ah")
   public int ah = 1;
   @ObfuscatedName("au")
   public int au = 1;
   @ObfuscatedName("ax")
   public int parentId = -1;
   @ObfuscatedName("ar")
   public boolean hidden = false;
   @ObfuscatedName("an")
   public int horizontalScrollbarPosition = 0;
   @ObfuscatedName("ai")
   public int verticalScrollbarPosition = 0;
   @ObfuscatedName("al")
   public int al = 0;
   @ObfuscatedName("at")
   public int at = 0;
   @ObfuscatedName("ag")
   public int color = 0;
   @ObfuscatedName("as")
   public int as = 0;
   @ObfuscatedName("aw")
   public int aw = 0;
   @ObfuscatedName("aq")
   public int aq = 0;
   @ObfuscatedName("aa")
   public boolean aa = false;
   @ObfuscatedName("af")
   public ld af;
   @ObfuscatedName("ak")
   public int alpha;
   @ObfuscatedName("ab")
   public int ab;
   @ObfuscatedName("ac")
   public int ac;
   @ObfuscatedName("ad")
   public boolean ad;
   @ObfuscatedName("bg")
   public int spriteId;
   @ObfuscatedName("br")
   public int br;
   @ObfuscatedName("ba")
   public int spriteRotation;
   @ObfuscatedName("bk")
   public boolean bk;
   @ObfuscatedName("be")
   public int borderThickness;
   @ObfuscatedName("bc")
   public int shadowColor;
   @ObfuscatedName("bm")
   public boolean bm;
   @ObfuscatedName("bh")
   public boolean bh;
   @ObfuscatedName("bs")
   public int entityType;
   @ObfuscatedName("bj")
   public int entityId;
   @ObfuscatedName("bt")
   int bt;
   @ObfuscatedName("by")
   int by;
   @ObfuscatedName("bn")
   public int animationId;
   @ObfuscatedName("bb")
   public int bb;
   @ObfuscatedName("bq")
   public int bq;
   @ObfuscatedName("bz")
   public int bz;
   @ObfuscatedName("bx")
   public int bx;
   @ObfuscatedName("bf")
   public int bf;
   @ObfuscatedName("bo")
   public int bo;
   @ObfuscatedName("bv")
   public int bv;
   @ObfuscatedName("bi")
   public int bi;
   @ObfuscatedName("bu")
   public int bu;
   @ObfuscatedName("bl")
   public boolean bl;
   @ObfuscatedName("bw")
   public int bw;
   @ObfuscatedName("bp")
   public int fontId;
   @ObfuscatedName("bd")
   public String text;
   @ObfuscatedName("cb")
   public String cb;
   @ObfuscatedName("cm")
   public int cm;
   @ObfuscatedName("cu")
   public int cu;
   @ObfuscatedName("cs")
   public int cs;
   @ObfuscatedName("ct")
   public boolean ct;
   @ObfuscatedName("cw")
   public int cw;
   @ObfuscatedName("ch")
   public int ch;
   @ObfuscatedName("cr")
   public int[] cr;
   @ObfuscatedName("co")
   public int[] co;
   @ObfuscatedName("cv")
   public int[] cv;
   @ObfuscatedName("cd")
   public String[] tableActions;
   @ObfuscatedName("cq")
   public int cq;
   @ObfuscatedName("ci")
   public String name;
   @ObfuscatedName("cc")
   public String[] actions;
   @ObfuscatedName("cg")
   public RTComponent dragParent;
   @ObfuscatedName("cj")
   public int cj;
   @ObfuscatedName("cl")
   public int cl;
   @ObfuscatedName("cz")
   public boolean cz;
   @ObfuscatedName("cn")
   public String cn;
   @ObfuscatedName("ca")
   public boolean ca;
   @ObfuscatedName("cf")
   public Object[] cf;
   @ObfuscatedName("cp")
   public Object[] cp;
   @ObfuscatedName("ck")
   public Object[] ck;
   @ObfuscatedName("db")
   public Object[] db;
   @ObfuscatedName("dp")
   public Object[] dp;
   @ObfuscatedName("da")
   public Object[] da;
   @ObfuscatedName("dr")
   public Object[] dr;
   @ObfuscatedName("dj")
   public Object[] dj;
   @ObfuscatedName("dh")
   public Object[] dh;
   @ObfuscatedName("dc")
   public Object[] dc;
   @ObfuscatedName("dl")
   public Object[] dl;
   @ObfuscatedName("df")
   public Object[] df;
   @ObfuscatedName("dq")
   public Object[] dq;
   @ObfuscatedName("dt")
   public int[] dt;
   @ObfuscatedName("dy")
   public Object[] dy;
   @ObfuscatedName("dn")
   public int[] dn;
   @ObfuscatedName("do")
   public Object[] do;
   @ObfuscatedName("dw")
   public int[] dw;
   @ObfuscatedName("dd")
   public Object[] dd;
   @ObfuscatedName("du")
   public Object[] du;
   @ObfuscatedName("dk")
   public Object[] dk;
   @ObfuscatedName("de")
   public Object[] de;
   @ObfuscatedName("dg")
   public Object[] dg;
   @ObfuscatedName("dx")
   public Object[] dx;
   @ObfuscatedName("di")
   public Object[] di;
   @ObfuscatedName("dv")
   public Object[] dv;
   @ObfuscatedName("dz")
   public Object[] dz;
   @ObfuscatedName("ds")
   public Object[] ds;
   @ObfuscatedName("dm")
   public Object[] dm;
   @ObfuscatedName("eb")
   public Object[] eb;
   @ObfuscatedName("ek")
   public Object[] ek;
   @ObfuscatedName("ej")
   public int[][] ej;
   @ObfuscatedName("ee")
   public int[] ee;
   @ObfuscatedName("eu")
   public int[] eu;
   @ObfuscatedName("ei")
   public int ei;
   @ObfuscatedName("ed")
   public String ed;
   @ObfuscatedName("ew")
   public String buttonAction;
   @ObfuscatedName("ey")
   public int[] itemIds;
   @ObfuscatedName("en")
   public int[] itemStackSizes;
   @ObfuscatedName("eg")
   public int itemId;
   @ObfuscatedName("ez")
   public int itemQuantity;
   @ObfuscatedName("et")
   public int et;
   @ObfuscatedName("ev")
   public int ev;
   @ObfuscatedName("es")
   public RTComponent[] cs2components;
   @ObfuscatedName("el")
   public boolean el;
   @ObfuscatedName("eo")
   public boolean eo;
   @ObfuscatedName("ec")
   public int ec;
   @ObfuscatedName("ep")
   public int ep;
   @ObfuscatedName("ef")
   public int ef;
   @ObfuscatedName("eq")
   public int eq;
   @ObfuscatedName("ea")
   public int arrayIndex;
   @ObfuscatedName("em")
   public int cycle;
   @ObfuscatedName("er")
   public boolean er;
   @ObfuscatedName("eh")
   public boolean eh;

   public RTComponent() {
      this.af = ld.t;
      this.alpha = 0;
      this.ab = 0;
      this.ac = 1;
      this.ad = false;
      this.spriteId = -1;
      this.br = -1;
      this.spriteRotation = 0;
      this.bk = false;
      this.borderThickness = 0;
      this.shadowColor = 0;
      this.entityType = 1;
      this.entityId = -1;
      this.bt = 1;
      this.by = -1;
      this.animationId = -1;
      this.bb = -1;
      this.bq = 0;
      this.bz = 0;
      this.bx = 0;
      this.bf = 0;
      this.bo = 0;
      this.bv = 100;
      this.bi = 0;
      this.bu = 0;
      this.bl = false;
      this.bw = 2;
      this.fontId = -1;
      this.text = "";
      this.cb = "";
      this.cm = 0;
      this.cu = 0;
      this.cs = 0;
      this.ct = false;
      this.cw = 0;
      this.ch = 0;
      this.cq = 0;
      this.name = "";
      this.dragParent = null;
      this.cj = 0;
      this.cl = 0;
      this.cz = false;
      this.cn = "";
      this.ca = false;
      this.ei = -1;
      this.ed = "";
      this.buttonAction = "Ok";
      this.itemId = -1;
      this.itemQuantity = 0;
      this.et = 0;
      this.ev = 0;
      this.el = false;
      this.eo = false;
      this.ec = -1;
      this.ep = 0;
      this.ef = 0;
      this.eq = 0;
      this.arrayIndex = -1;
      this.cycle = -1;
      this.er = false;
      this.eh = false;
   }

   @ObfuscatedName("a")
   void a(ByteBuffer var1) {
      this.modern = false;
      this.type = var1.av();
      this.d = var1.av();
      this.contentType = var1.ae();
      this.ay = var1.am();
      this.ao = var1.am();
      this.av = var1.ae();
      this.aj = var1.ae();
      this.alpha = var1.av();
      this.parentId = var1.ae();
      if (this.parentId == 65535) {
         this.parentId = -1;
      } else {
         this.parentId += this.id & -65536;
      }

      this.ei = var1.ae();
      if (this.ei == 65535) {
         this.ei = -1;
      }

      int var2 = var1.av();
      int var3;
      if (var2 > 0) {
         this.ee = new int[var2];
         this.eu = new int[var2];

         for(var3 = 0; var3 < var2; ++var3) {
            this.ee[var3] = var1.av();
            this.eu[var3] = var1.ae();
         }
      }

      var3 = var1.av();
      int var4;
      int var5;
      int var6;
      if (var3 > 0) {
         this.ej = new int[var3][];

         for(var4 = 0; var4 < var3; ++var4) {
            var5 = var1.ae();
            this.ej[var4] = new int[var5];

            for(var6 = 0; var6 < var5; ++var6) {
               this.ej[var4][var6] = var1.ae();
               if (this.ej[var4][var6] == 65535) {
                  this.ej[var4][var6] = -1;
               }
            }
         }
      }

      if (this.type == 0) {
         this.at = var1.ae();
         this.hidden = var1.av() == 1;
      }

      if (this.type == 1) {
         var1.ae();
         var1.av();
      }

      if (this.type == 2) {
         this.itemIds = new int[this.av * this.aj];
         this.itemStackSizes = new int[this.av * this.aj];
         var4 = var1.av();
         if (var4 == 1) {
            this.cq |= 268435456;
         }

         var5 = var1.av();
         if (var5 == 1) {
            this.cq |= 1073741824;
         }

         var6 = var1.av();
         if (var6 == 1) {
            this.cq |= Integer.MIN_VALUE;
         }

         int var7 = var1.av();
         if (var7 == 1) {
            this.cq |= 536870912;
         }

         this.cw = var1.av();
         this.ch = var1.av();
         this.cr = new int[20];
         this.co = new int[20];
         this.cv = new int[20];

         int var8;
         for(var8 = 0; var8 < 20; ++var8) {
            int var9 = var1.av();
            if (var9 == 1) {
               this.cr[var8] = var1.am();
               this.co[var8] = var1.am();
               this.cv[var8] = var1.ap();
            } else {
               this.cv[var8] = -1;
            }
         }

         this.tableActions = new String[5];

         for(var8 = 0; var8 < 5; ++var8) {
            String var10 = var1.ar();
            if (var10.length() > 0) {
               this.tableActions[var8] = var10;
               this.cq |= 1 << var8 + 23;
            }
         }
      }

      if (this.type == 3) {
         this.aa = var1.av() == 1;
      }

      if (this.type == 4 || this.type == 1) {
         this.cu = var1.av();
         this.cs = var1.av();
         this.cm = var1.av();
         this.fontId = var1.ae();
         if (this.fontId == 65535) {
            this.fontId = -1;
         }

         this.ct = var1.av() == 1;
      }

      if (this.type == 4) {
         this.text = var1.ar();
         this.cb = var1.ar();
      }

      if (this.type == 1 || this.type == 3 || this.type == 4) {
         this.color = var1.ap();
      }

      if (this.type == 3 || this.type == 4) {
         this.as = var1.ap();
         this.aw = var1.ap();
         this.aq = var1.ap();
      }

      if (this.type == 5) {
         this.spriteId = var1.ap();
         this.br = var1.ap();
      }

      if (this.type == 6) {
         this.entityType = 1;
         this.entityId = var1.ae();
         if (this.entityId == 65535) {
            this.entityId = -1;
         }

         this.bt = 1;
         this.by = var1.ae();
         if (this.by == 65535) {
            this.by = -1;
         }

         this.animationId = var1.ae();
         if (this.animationId == 65535) {
            this.animationId = -1;
         }

         this.bb = var1.ae();
         if (this.bb == 65535) {
            this.bb = -1;
         }

         this.bv = var1.ae();
         this.bx = var1.ae();
         this.bf = var1.ae();
      }

      if (this.type == 7) {
         this.itemIds = new int[this.av * this.aj];
         this.itemStackSizes = new int[this.av * this.aj];
         this.cu = var1.av();
         this.fontId = var1.ae();
         if (this.fontId == 65535) {
            this.fontId = -1;
         }

         this.ct = var1.av() == 1;
         this.color = var1.ap();
         this.cw = var1.am();
         this.ch = var1.am();
         var4 = var1.av();
         if (var4 == 1) {
            this.cq |= 1073741824;
         }

         this.tableActions = new String[5];

         for(var5 = 0; var5 < 5; ++var5) {
            String var11 = var1.ar();
            if (var11.length() > 0) {
               this.tableActions[var5] = var11;
               this.cq |= 1 << var5 + 23;
            }
         }
      }

      if (this.type == 8) {
         this.text = var1.ar();
      }

      if (this.d == 2 || this.type == 2) {
         this.cn = var1.ar();
         this.ed = var1.ar();
         var4 = var1.ae() & 63;
         this.cq |= var4 << 11;
      }

      if (this.d == 1 || this.d == 4 || this.d == 5 || this.d == 6) {
         this.buttonAction = var1.ar();
         if (this.buttonAction.length() == 0) {
            if (this.d == 1) {
               this.buttonAction = "Ok";
            }

            if (this.d == 4) {
               this.buttonAction = "Select";
            }

            if (this.d == 5) {
               this.buttonAction = "Select";
            }

            if (this.d == 6) {
               this.buttonAction = "Continue";
            }
         }
      }

      if (this.d == 1 || this.d == 4 || this.d == 5) {
         this.cq |= 4194304;
      }

      if (this.d == 6) {
         this.cq |= 1;
      }

   }

   @ObfuscatedName("l")
   void l(ByteBuffer var1) {
      var1.av();
      this.modern = true;
      this.type = var1.av();
      this.contentType = var1.ae();
      this.ay = var1.am();
      this.ao = var1.am();
      this.av = var1.ae();
      if (this.type == 9) {
         this.aj = var1.am();
      } else {
         this.aj = var1.ae();
      }

      this.h = var1.aj();
      this.m = var1.aj();
      this.r = var1.aj();
      this.y = var1.aj();
      this.parentId = var1.ae();
      if (this.parentId == 65535) {
         this.parentId = -1;
      } else {
         this.parentId += this.id & -65536;
      }

      this.hidden = var1.av() == 1;
      if (this.type == 0) {
         this.al = var1.ae();
         this.at = var1.ae();
         this.er = var1.av() == 1;
      }

      if (this.type == 5) {
         this.spriteId = var1.ap();
         this.spriteRotation = var1.ae();
         this.bk = var1.av() == 1;
         this.alpha = var1.av();
         this.borderThickness = var1.av();
         this.shadowColor = var1.ap();
         this.bm = var1.av() == 1;
         this.bh = var1.av() == 1;
      }

      if (this.type == 6) {
         this.entityType = 1;
         this.entityId = var1.ae();
         if (this.entityId == 65535) {
            this.entityId = -1;
         }

         this.bq = var1.am();
         this.bz = var1.am();
         this.bx = var1.ae();
         this.bf = var1.ae();
         this.bo = var1.ae();
         this.bv = var1.ae();
         this.animationId = var1.ae();
         if (this.animationId == 65535) {
            this.animationId = -1;
         }

         this.bl = var1.av() == 1;
         var1.ae();
         if (this.h != 0) {
            this.bi = var1.ae();
         }

         if (this.m != 0) {
            var1.ae();
         }
      }

      if (this.type == 4) {
         this.fontId = var1.ae();
         if (this.fontId == 65535) {
            this.fontId = -1;
         }

         this.text = var1.ar();
         this.cm = var1.av();
         this.cu = var1.av();
         this.cs = var1.av();
         this.ct = var1.av() == 1;
         this.color = var1.ap();
      }

      if (this.type == 3) {
         this.color = var1.ap();
         this.aa = var1.av() == 1;
         this.alpha = var1.av();
      }

      if (this.type == 9) {
         this.ac = var1.av();
         this.color = var1.ap();
         this.ad = var1.av() == 1;
      }

      this.cq = var1.az();
      this.name = var1.ar();
      int var2 = var1.av();
      if (var2 > 0) {
         this.actions = new String[var2];

         for(int var3 = 0; var3 < var2; ++var3) {
            this.actions[var3] = var1.ar();
         }
      }

      this.cj = var1.av();
      this.cl = var1.av();
      this.cz = var1.av() == 1;
      this.cn = var1.ar();
      this.cf = this.b(var1);
      this.da = this.b(var1);
      this.dj = this.b(var1);
      this.df = this.b(var1);
      this.dl = this.b(var1);
      this.dq = this.b(var1);
      this.dy = this.b(var1);
      this.do = this.b(var1);
      this.dd = this.b(var1);
      this.du = this.b(var1);
      this.dr = this.b(var1);
      this.cp = this.b(var1);
      this.ck = this.b(var1);
      this.db = this.b(var1);
      this.dp = this.b(var1);
      this.dh = this.b(var1);
      this.dc = this.b(var1);
      this.dk = this.b(var1);
      this.dt = this.e(var1);
      this.dn = this.e(var1);
      this.dw = this.e(var1);
   }

   @ObfuscatedName("b")
   Object[] b(ByteBuffer var1) {
      int var2 = var1.av();
      if (var2 == 0) {
         return null;
      } else {
         Object[] var3 = new Object[var2];

         for(int var4 = 0; var4 < var2; ++var4) {
            int var5 = var1.av();
            if (var5 == 0) {
               var3[var4] = new Integer(var1.ap());
            } else if (var5 == 1) {
               var3[var4] = var1.ar();
            }
         }

         this.ca = true;
         return var3;
      }
   }

   @ObfuscatedName("e")
   int[] e(ByteBuffer var1) {
      int var2 = var1.av();
      if (var2 == 0) {
         return null;
      } else {
         int[] var3 = new int[var2];

         for(int var4 = 0; var4 < var2; ++var4) {
            var3[var4] = var1.ap();
         }

         return var3;
      }
   }

   @ObfuscatedName("x")
   public void x(int var1, int var2) {
      int var3 = this.itemIds[var2];
      this.itemIds[var2] = this.itemIds[var1];
      this.itemIds[var1] = var3;
      var3 = this.itemStackSizes[var2];
      this.itemStackSizes[var2] = this.itemStackSizes[var1];
      this.itemStackSizes[var1] = var3;
   }

   @ObfuscatedName("p")
   public Sprite p(boolean var1) {
      j = false;
      int var2;
      if (var1) {
         var2 = this.br;
      } else {
         var2 = this.spriteId;
      }

      if (var2 == -1) {
         return null;
      } else {
         long var3 = ((long)this.shadowColor << 40) + ((this.bh ? 1L : 0L) << 39) + ((this.bm ? 1L : 0L) << 38) + ((long)this.borderThickness << 36) + (long)var2;
         Sprite var5 = (Sprite)o.t(var3);
         if (var5 != null) {
            return var5;
         } else {
            var5 = q.t(RuneScriptVM.g, var2, 0);
            if (var5 == null) {
               j = true;
               return null;
            } else {
               if (this.bm) {
                  var5.e();
               }

               if (this.bh) {
                  var5.b();
               }

               if (this.borderThickness > 0) {
                  var5.l(this.borderThickness);
               }

               if (this.borderThickness >= 1) {
                  var5.x(1);
               }

               if (this.borderThickness >= 2) {
                  var5.x(16777215);
               }

               if (this.shadowColor != 0) {
                  var5.p(this.shadowColor);
               }

               o.i(var5, var3);
               return var5;
            }
         }
      }
   }

   @ObfuscatedName("o")
   public km o() {
      j = false;
      if (this.fontId == -1) {
         return null;
      } else {
         km var1 = (km)v.t((long)this.fontId);
         if (var1 != null) {
            return var1;
         } else {
            var1 = TaskData.q(RuneScriptVM.g, bg.n, this.fontId, 0);
            if (var1 != null) {
               v.i(var1, (long)this.fontId);
            } else {
               j = true;
            }

            return var1;
         }
      }
   }

   @ObfuscatedName("c")
   public Sprite c(int var1) {
      j = false;
      if (var1 >= 0 && var1 < this.cv.length) {
         int var2 = this.cv[var1];
         if (var2 == -1) {
            return null;
         } else {
            Sprite var3 = (Sprite)o.t((long)var2);
            if (var3 != null) {
               return var3;
            } else {
               var3 = q.t(RuneScriptVM.g, var2, 0);
               if (var3 != null) {
                  o.i(var3, (long)var2);
               } else {
                  j = true;
               }

               return var3;
            }
         }
      } else {
         return null;
      }
   }

   @ObfuscatedName("u")
   public Model u(kf var1, int var2, boolean var3, PlayerComposite var4) {
      j = false;
      int var5;
      int var6;
      if (var3) {
         var5 = this.bt;
         var6 = this.by;
      } else {
         var5 = this.entityType;
         var6 = this.entityId;
      }

      if (var5 == 0) {
         return null;
      } else if (var5 == 1 && var6 == -1) {
         return null;
      } else {
         Model var7 = (Model)c.t((long)(var6 + (var5 << 16)));
         if (var7 == null) {
            AlternativeModel var8;
            if (var5 == 1) {
               var8 = AlternativeModel.t(kv.p, var6, 0);
               if (var8 == null) {
                  j = true;
                  return null;
               }

               var7 = var8.av(64, 768, -50, -10, -50);
            }

            if (var5 == 2) {
               var8 = ho.q(var6).e();
               if (var8 == null) {
                  j = true;
                  return null;
               }

               var7 = var8.av(64, 768, -50, -10, -50);
            }

            if (var5 == 3) {
               if (var4 == null) {
                  return null;
               }

               var8 = var4.x();
               if (var8 == null) {
                  j = true;
                  return null;
               }

               var7 = var8.av(64, 768, -50, -10, -50);
            }

            if (var5 == 4) {
               ItemDefinition var9 = cs.loadItemDefinition(var6);
               var8 = var9.x(10);
               if (var8 == null) {
                  j = true;
                  return null;
               }

               var7 = var8.av(var9.bg + 64, var9.br * 5 + 768, -50, -10, -50);
            }

            c.i(var7, (long)(var6 + (var5 << 16)));
         }

         if (var1 != null) {
            var7 = var1.p(var7, var2);
         }

         return var7;
      }
   }

   @ObfuscatedName("k")
   public iq k(boolean var1) {
      if (this.br == -1) {
         var1 = false;
      }

      int var2 = var1 ? this.br : this.spriteId;
      if (var2 == -1) {
         return null;
      } else {
         long var3 = ((long)this.shadowColor << 40) + ((this.bh ? 1L : 0L) << 39) + (long)var2 + ((long)this.borderThickness << 36) + ((this.bm ? 1L : 0L) << 38);
         iq var5 = (iq)u.t(var3);
         if (var5 != null) {
            return var5;
         } else {
            Sprite var6 = this.p(var1);
            if (var6 == null) {
               return null;
            } else {
               Sprite var7 = var6.q();
               int[] var8 = new int[var7.height];
               int[] var9 = new int[var7.height];

               for(int var10 = 0; var10 < var7.height; ++var10) {
                  int var11 = 0;
                  int var12 = var7.width;

                  int var13;
                  for(var13 = 0; var13 < var7.width; ++var13) {
                     if (var7.pixels[var13 + var10 * var7.width] == 0) {
                        var11 = var13;
                        break;
                     }
                  }

                  for(var13 = var7.width - 1; var13 >= var11; --var13) {
                     if (var7.pixels[var13 + var10 * var7.width] == 0) {
                        var12 = var13 + 1;
                        break;
                     }
                  }

                  var8[var10] = var11;
                  var9[var10] = var12 - var11;
               }

               var5 = new iq(var7.width, var7.height, var9, var8, var2);
               u.i(var5, var3);
               return var5;
            }
         }
      }
   }

   @ObfuscatedName("w")
   public void w(int var1, String var2) {
      if (this.actions == null || this.actions.length <= var1) {
         String[] var3 = new String[var1 + 1];
         if (this.actions != null) {
            for(int var4 = 0; var4 < this.actions.length; ++var4) {
               var3[var4] = this.actions[var4];
            }
         }

         this.actions = var3;
      }

      this.actions[var1] = var2;
   }

   @ObfuscatedName("k")
   static void k(byte[] var0) {
      ByteBuffer var1 = new ByteBuffer(var0);
      var1.index = var0.length - 2;
      le.t = var1.ae();
      ci.a = new int[le.t];
      er.l = new int[le.t];
      le.b = new int[le.t];
      GrandExchangeOffer.e = new int[le.t];
      ClassStructureNode.p = new byte[le.t][];
      var1.index = var0.length - 7 - le.t * 8;
      le.q = var1.ae();
      le.i = var1.ae();
      int var2 = (var1.av() & 255) + 1;

      int var3;
      for(var3 = 0; var3 < le.t; ++var3) {
         ci.a[var3] = var1.ae();
      }

      for(var3 = 0; var3 < le.t; ++var3) {
         er.l[var3] = var1.ae();
      }

      for(var3 = 0; var3 < le.t; ++var3) {
         le.b[var3] = var1.ae();
      }

      for(var3 = 0; var3 < le.t; ++var3) {
         GrandExchangeOffer.e[var3] = var1.ae();
      }

      var1.index = var0.length - 7 - le.t * 8 - (var2 - 1) * 3;
      le.x = new int[var2];

      for(var3 = 1; var3 < var2; ++var3) {
         le.x[var3] = var1.az();
         if (le.x[var3] == 0) {
            le.x[var3] = 1;
         }
      }

      var1.index = 0;

      for(var3 = 0; var3 < le.t; ++var3) {
         int var4 = le.b[var3];
         int var5 = GrandExchangeOffer.e[var3];
         int var6 = var5 * var4;
         byte[] var7 = new byte[var6];
         ClassStructureNode.p[var3] = var7;
         int var8 = var1.av();
         int var9;
         if (var8 == 0) {
            for(var9 = 0; var9 < var6; ++var9) {
               var7[var9] = var1.aj();
            }
         } else if (var8 == 1) {
            for(var9 = 0; var9 < var4; ++var9) {
               for(int var10 = 0; var10 < var5; ++var10) {
                  var7[var9 + var10 * var4] = var1.aj();
               }
            }
         }
      }

   }
}
