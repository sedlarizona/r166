import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("il")
public enum il implements gl {
    @ObfuscatedName("t")
    t(0, -1, true, false, true), @ObfuscatedName("q")
    q(1, 0, true, true, true), @ObfuscatedName("i")
    i(2, 1, true, true, false), @ObfuscatedName("a")
    a(3, 2, false, false, true), @ObfuscatedName("l")
    l(4, 3, false, false, true), @ObfuscatedName("b")
    b(5, 10, false, false, true);

    @ObfuscatedName("e")
    final int e;

    @ObfuscatedName("x")
    public final int x;

    @ObfuscatedName("p")
    public final boolean p;

    @ObfuscatedName("g")
    public final boolean g;

    il(int var3, int var4, boolean var5, boolean var6, boolean var7) {
        this.e = var3;
        this.x = var4;
        this.p = var6;
        this.g = var7;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.e;
    }
}
