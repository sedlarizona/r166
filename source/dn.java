import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dn")
public class dn {

    @ObfuscatedName("b")
    static float[][] b = new float[2][8];

    @ObfuscatedName("e")
    static int[][] e = new int[2][8];

    @ObfuscatedName("x")
    static float x;

    @ObfuscatedName("p")
    static int p;

    @ObfuscatedName("t")
    int[] t = new int[2];

    @ObfuscatedName("i")
    int[][][] i = new int[2][2][4];

    @ObfuscatedName("a")
    int[][][] a = new int[2][2][4];

    @ObfuscatedName("l")
    int[] l = new int[2];

    @ObfuscatedName("t")
    float t(int var1, int var2, float var3) {
        float var4 = (float) this.a[var1][0][var2] + var3 * (float) (this.a[var1][1][var2] - this.a[var1][0][var2]);
        var4 *= 0.0015258789F;
        return 1.0F - (float) Math.pow(10.0D, (double) (-var4 / 20.0F));
    }

    @ObfuscatedName("i")
    float i(int var1, int var2, float var3) {
        float var4 = (float) this.i[var1][0][var2] + var3 * (float) (this.i[var1][1][var2] - this.i[var1][0][var2]);
        var4 *= 1.2207031E-4F;
        return q(var4);
    }

    @ObfuscatedName("a")
    int a(int var1, float var2) {
        float var3;
        if (var1 == 0) {
            var3 = (float) this.l[0] + (float) (this.l[1] - this.l[0]) * var2;
            var3 *= 0.0030517578F;
            x = (float) Math.pow(0.1D, (double) (var3 / 20.0F));
            p = (int) (x * 65536.0F);
        }

        if (this.t[var1] == 0) {
            return 0;
        } else {
            var3 = this.t(var1, 0, var2);
            b[var1][0] = -2.0F * var3 * (float) Math.cos((double) this.i(var1, 0, var2));
            b[var1][1] = var3 * var3;

            int var4;
            for (var4 = 1; var4 < this.t[var1]; ++var4) {
                var3 = this.t(var1, var4, var2);
                float var5 = -2.0F * var3 * (float) Math.cos((double) this.i(var1, var4, var2));
                float var6 = var3 * var3;
                b[var1][var4 * 2 + 1] = b[var1][var4 * 2 - 1] * var6;
                b[var1][var4 * 2] = b[var1][var4 * 2 - 1] * var5 + b[var1][var4 * 2 - 2] * var6;

                for (int var7 = var4 * 2 - 1; var7 >= 2; --var7) {
                    b[var1][var7] += b[var1][var7 - 1] * var5 + b[var1][var7 - 2] * var6;
                }

                b[var1][1] += b[var1][0] * var5 + var6;
                b[var1][0] += var5;
            }

            if (var1 == 0) {
                for (var4 = 0; var4 < this.t[0] * 2; ++var4) {
                    b[0][var4] *= x;
                }
            }

            for (var4 = 0; var4 < this.t[var1] * 2; ++var4) {
                e[var1][var4] = (int) (b[var1][var4] * 65536.0F);
            }

            return this.t[var1] * 2;
        }
    }

    @ObfuscatedName("l")
    final void l(ByteBuffer var1, AudioEnvelope var2) {
        int var3 = var1.av();
        this.t[0] = var3 >> 4;
        this.t[1] = var3 & 15;
        if (var3 != 0) {
            this.l[0] = var1.ae();
            this.l[1] = var1.ae();
            int var4 = var1.av();

            int var5;
            int var6;
            for (var5 = 0; var5 < 2; ++var5) {
                for (var6 = 0; var6 < this.t[var5]; ++var6) {
                    this.i[var5][0][var6] = var1.ae();
                    this.a[var5][0][var6] = var1.ae();
                }
            }

            for (var5 = 0; var5 < 2; ++var5) {
                for (var6 = 0; var6 < this.t[var5]; ++var6) {
                    if ((var4 & 1 << var5 * 4 << var6) != 0) {
                        this.i[var5][1][var6] = var1.ae();
                        this.a[var5][1][var6] = var1.ae();
                    } else {
                        this.i[var5][1][var6] = this.i[var5][0][var6];
                        this.a[var5][1][var6] = this.a[var5][0][var6];
                    }
                }
            }

            if (var4 != 0 || this.l[1] != this.l[0]) {
                var2.q(var1);
            }
        } else {
            int[] var7 = this.l;
            this.l[1] = 0;
            var7[0] = 0;
        }

    }

    @ObfuscatedName("q")
    static float q(float var0) {
        float var1 = 32.703197F * (float) Math.pow(2.0D, (double) var0);
        return var1 * 3.1415927F / 11025.0F;
    }
}
