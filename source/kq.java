import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kq")
public class kq {

    @ObfuscatedName("pd")
    static ca pd;

    @ObfuscatedName("t")
    public static final kq t = new kq("PLAIN11", "p11_full");

    @ObfuscatedName("q")
    public static final kq q = new kq("PLAIN12", "p12_full");

    @ObfuscatedName("i")
    public static final kq i = new kq("BOLD12", "b12_full");

    @ObfuscatedName("a")
    public static final kq a = new kq("VERDANA11", "verdana_11pt_regular");

    @ObfuscatedName("l")
    public static final kq l = new kq("VERDANA13", "verdana_13pt_regular");

    @ObfuscatedName("b")
    public static final kq b = new kq("VERDANA15", "verdana_15pt_regular");

    @ObfuscatedName("dj")
    static ju dj;

    @ObfuscatedName("e")
    final String e;

    @ObfuscatedName("x")
    String x;

    kq(String var1, String var2) {
        this.e = var1;
        this.x = var2;
    }

    @ObfuscatedName("t")
    public static kq[] t() {
        return new kq[] { a, q, t, b, l, i };
    }

    @ObfuscatedName("p")
    public static boolean p() {
        try {
            if (hi.audioTrackId == 2) {
                if (fv.o == null) {
                    fv.o = ix.t(hi.b, fm.e, hi.x);
                    if (fv.o == null) {
                        return false;
                    }
                }

                if (jq.c == null) {
                    jq.c = new dc(hi.i, ic.q);
                }

                if (hi.audioTask.i(fv.o, c.t, jq.c, 22050)) {
                    hi.audioTask.a();
                    hi.audioTask.t(hi.p);
                    hi.audioTask.o(fv.o, cr.n);
                    hi.audioTrackId = 0;
                    fv.o = null;
                    jq.c = null;
                    hi.b = null;
                    return true;
                }
            }
        } catch (Exception var1) {
            var1.printStackTrace();
            hi.audioTask.u();
            hi.audioTrackId = 0;
            fv.o = null;
            jq.c = null;
            hi.b = null;
        }

        return false;
    }

    @ObfuscatedName("o")
    static Sprite[] o() {
        Sprite[] var0 = new Sprite[le.t];

        for (int var1 = 0; var1 < le.t; ++var1) {
            Sprite var2 = var0[var1] = new Sprite();
            var2.b = le.q;
            var2.e = le.i;
            var2.a = ci.a[var1];
            var2.l = er.l[var1];
            var2.width = le.b[var1];
            var2.height = GrandExchangeOffer.e[var1];
            int var3 = var2.height * var2.width;
            byte[] var4 = ClassStructureNode.p[var1];
            var2.pixels = new int[var3];

            for (int var5 = 0; var5 < var3; ++var5) {
                var2.pixels[var5] = le.x[var4[var5] & 255];
            }
        }

        ly.z();
        return var0;
    }

    @ObfuscatedName("ix")
    static final void ix(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        if (RuneScript.i(var0)) {
            i.mq = null;
            jk.iq(RTComponent.b[var0], -1, var1, var2, var3, var4, var5, var6, var7);
            if (i.mq != null) {
                jk.iq(i.mq, -1412584499, var1, var2, var3, var4, ItemStorage.mr, c.mx, var7);
                i.mq = null;
            }

        } else {
            if (var7 != -1) {
                Client.outdatedInterfaces[var7] = true;
            } else {
                for (int var8 = 0; var8 < 100; ++var8) {
                    Client.outdatedInterfaces[var8] = true;
                }
            }

        }
    }
}
