import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fx")
public class fx {

    @ObfuscatedName("t")
    public static final fx t = new fx(0, -1);

    @ObfuscatedName("q")
    public static final fx q = new fx(1, 1);

    @ObfuscatedName("i")
    public static final fx i = new fx(2, -2);

    @ObfuscatedName("a")
    public static final fx a = new fx(3, 5);

    @ObfuscatedName("l")
    public static final fx l = new fx(4, 4);

    @ObfuscatedName("b")
    public static final fx b = new fx(5, 2);

    @ObfuscatedName("e")
    public static final fx e = new fx(6, 6);

    @ObfuscatedName("x")
    public static final fx x = new fx(7, 7);

    @ObfuscatedName("p")
    public static final fx p = new fx(8, 2);

    @ObfuscatedName("g")
    public static final fx g = new fx(9, 2);

    @ObfuscatedName("n")
    public static final fx n = new fx(10, -2);

    @ObfuscatedName("o")
    public static final fx o = new fx(11, 4);

    @ObfuscatedName("c")
    public static final fx c = new fx(12, 5);

    @ObfuscatedName("v")
    public static final fx v = new fx(13, -2);

    @ObfuscatedName("u")
    public static final fx u = new fx(14, 4);

    @ObfuscatedName("j")
    public static final fx j = new fx(15, 12);

    @ObfuscatedName("k")
    public static final fx k = new fx(16, 0);

    @ObfuscatedName("z")
    public static final fx z = new fx(17, -2);

    @ObfuscatedName("w")
    public static final fx w = new fx(18, -2);

    @ObfuscatedName("s")
    public static final fx s = new fx(19, 8);

    @ObfuscatedName("d")
    public static final fx d = new fx(20, -1);

    @ObfuscatedName("f")
    public static final fx f = new fx(21, 5);

    @ObfuscatedName("r")
    public static final fx r = new fx(22, 0);

    @ObfuscatedName("y")
    public static final fx y = new fx(23, -2);

    @ObfuscatedName("h")
    public static final fx h = new fx(24, -2);

    @ObfuscatedName("m")
    public static final fx m = new fx(25, 2);

    @ObfuscatedName("ay")
    public static final fx ay = new fx(26, 14);

    @ObfuscatedName("ao")
    public static final fx ao = new fx(27, -2);

    @ObfuscatedName("av")
    public static final fx av = new fx(28, 0);

    @ObfuscatedName("aj")
    public static final fx aj = new fx(29, -1);

    @ObfuscatedName("ae")
    public static final fx ae = new fx(30, 0);

    @ObfuscatedName("am")
    public static final fx am = new fx(31, 15);

    @ObfuscatedName("az")
    public static final fx az = new fx(32, 6);

    @ObfuscatedName("ap")
    public static final fx ap = new fx(33, 6);

    @ObfuscatedName("ah")
    public static final fx ah = new fx(34, 4);

    @ObfuscatedName("au")
    public static final fx au = new fx(35, 3);

    @ObfuscatedName("ax")
    public static final fx ax = new fx(36, 1);

    @ObfuscatedName("ar")
    public static final fx ar = new fx(37, 2);

    @ObfuscatedName("an")
    public static final fx an = new fx(38, -2);

    @ObfuscatedName("ai")
    public static final fx ai = new fx(39, -2);

    @ObfuscatedName("al")
    public static final fx al = new fx(40, 2);

    @ObfuscatedName("at")
    public static final fx at = new fx(41, 8);

    @ObfuscatedName("ag")
    public static final fx ag = new fx(42, 8);

    @ObfuscatedName("as")
    public static final fx as = new fx(43, 7);

    @ObfuscatedName("aw")
    public static final fx aw = new fx(44, 2);

    @ObfuscatedName("aq")
    public static final fx aq = new fx(45, 6);

    @ObfuscatedName("aa")
    public static final fx aa = new fx(46, 6);

    @ObfuscatedName("af")
    public static final fx af = new fx(47, -1);

    @ObfuscatedName("ak")
    public static final fx ak = new fx(48, -2);

    @ObfuscatedName("ab")
    public static final fx ab = new fx(49, -1);

    @ObfuscatedName("ac")
    public static final fx ac = new fx(50, 10);

    @ObfuscatedName("ad")
    public static final fx ad = new fx(51, 6);

    @ObfuscatedName("bg")
    public static final fx bg = new fx(52, 0);

    @ObfuscatedName("br")
    public static final fx br = new fx(53, 4);

    @ObfuscatedName("ba")
    public static final fx ba = new fx(54, 6);

    @ObfuscatedName("bk")
    public static final fx bk = new fx(55, 8);

    @ObfuscatedName("be")
    public static final fx be = new fx(56, 3);

    @ObfuscatedName("bc")
    public static final fx bc = new fx(57, 5);

    @ObfuscatedName("bm")
    public static final fx bm = new fx(58, 20);

    @ObfuscatedName("bh")
    public static final fx bh = new fx(59, 6);

    @ObfuscatedName("bs")
    public static final fx bs = new fx(60, -2);

    @ObfuscatedName("bj")
    public static final fx bj = new fx(61, -2);

    @ObfuscatedName("bt")
    public static final fx bt = new fx(62, 4);

    @ObfuscatedName("by")
    public static final fx by = new fx(63, 6);

    @ObfuscatedName("bn")
    public static final fx bn = new fx(64, 1);

    @ObfuscatedName("bb")
    public static final fx bb = new fx(65, 4);

    @ObfuscatedName("bq")
    public static final fx bq = new fx(66, 6);

    @ObfuscatedName("bz")
    public static final fx bz = new fx(67, -2);

    @ObfuscatedName("bx")
    public static final fx bx = new fx(68, 10);

    @ObfuscatedName("bf")
    public static final fx bf = new fx(69, -2);

    @ObfuscatedName("bo")
    public static final fx bo = new fx(70, 2);

    @ObfuscatedName("bv")
    public static final fx bv = new fx(71, 0);

    @ObfuscatedName("bi")
    public static final fx bi = new fx(72, 1);

    @ObfuscatedName("bu")
    public static final fx bu = new fx(73, 0);

    @ObfuscatedName("bl")
    public static final fx bl = new fx(74, 2);

    @ObfuscatedName("bw")
    public static final fx bw = new fx(75, 5);

    @ObfuscatedName("bp")
    public static final fx bp = new fx(76, 2);

    @ObfuscatedName("bd")
    public static final fx bd = new fx(77, 28);

    @ObfuscatedName("cb")
    public static final fx cb = new fx(78, -1);

    @ObfuscatedName("cm")
    public static final fx cm = new fx(79, 6);

    @ObfuscatedName("cu")
    public static final fx cu = new fx(80, -2);

    @ObfuscatedName("cs")
    public static final fx cs = new fx(81, -2);

    @ObfuscatedName("ct")
    public static final fx ct = new fx(82, -2);

    @ObfuscatedName("cw")
    public static final fx cw = new fx(83, 1);

    @ObfuscatedName("ch")
    public final int ch;

    @ObfuscatedName("cr")
    public final int cr;

    fx(int var1, int var2) {
        this.ch = var1;
        this.cr = var2;
    }
}
