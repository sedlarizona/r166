import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("at")
public class AppletParameter {

    @ObfuscatedName("pp")
    static dt pp;

    @ObfuscatedName("t")
    public static final AppletParameter t = new AppletParameter("details");

    @ObfuscatedName("q")
    public static final AppletParameter q = new AppletParameter("compositemap");

    @ObfuscatedName("i")
    public static final AppletParameter i = new AppletParameter("compositetexture");

    @ObfuscatedName("a")
    public static final AppletParameter a = new AppletParameter("area");

    @ObfuscatedName("l")
    public static final AppletParameter l = new AppletParameter("labels");

    @ObfuscatedName("b")
    public final String value;

    AppletParameter(String var1) {
        this.value = var1;
    }

    @ObfuscatedName("fj")
    static final void fj(Character var0, int var1) {
        int var2;
        kf var11;
        if (var0.bd > Client.bz) {
            o.fa(var0);
        } else {
            int var3;
            int var4;
            int var5;
            int var7;
            if (var0.cb >= Client.bz) {
                if (var0.cb == Client.bz || var0.animationId == -1 || var0.bq != 0
                        || var0.bb + 1 > ff.t(var0.animationId).x[var0.animationFrameIndex]) {
                    var2 = var0.cb - var0.bd;
                    var3 = Client.bz - var0.bd;
                    var4 = var0.bu * 128 + var0.ap * 64;
                    var5 = var0.bw * 128 + var0.ap * 64;
                    int var6 = var0.bl * 128 + var0.ap * 64;
                    var7 = var0.bp * 128 + var0.ap * 64;
                    var0.regionX = (var3 * var6 + var4 * (var2 - var3)) / var2;
                    var0.regionY = (var7 * var3 + var5 * (var2 - var3)) / var2;
                }

                var0.cq = 0;
                var0.ct = var0.cm;
                var0.orientation = var0.ct;
            } else {
                var0.bs = var0.au;
                if (var0.speed == 0) {
                    var0.cq = 0;
                } else {
                    label465: {
                        if (var0.animationId != -1 && var0.bq == 0) {
                            var11 = ff.t(var0.animationId);
                            if (var0.ci > 0 && var11.k == 0) {
                                ++var0.cq;
                                break label465;
                            }

                            if (var0.ci <= 0 && var11.z == 0) {
                                ++var0.cq;
                                break label465;
                            }
                        }

                        var2 = var0.regionX;
                        var3 = var0.regionY;
                        var4 = var0.co[var0.speed - 1] * 128 + var0.ap * 64;
                        var5 = var0.cv[var0.speed - 1] * 128 + var0.ap * 64;
                        if (var2 < var4) {
                            if (var3 < var5) {
                                var0.ct = 1280;
                            } else if (var3 > var5) {
                                var0.ct = 1792;
                            } else {
                                var0.ct = 1536;
                            }
                        } else if (var2 > var4) {
                            if (var3 < var5) {
                                var0.ct = 768;
                            } else if (var3 > var5) {
                                var0.ct = 256;
                            } else {
                                var0.ct = 512;
                            }
                        } else if (var3 < var5) {
                            var0.ct = 1024;
                        } else if (var3 > var5) {
                            var0.ct = 0;
                        }

                        byte var13 = var0.cd[var0.speed - 1];
                        if (var4 - var2 <= 256 && var4 - var2 >= -256 && var5 - var3 <= 256 && var5 - var3 >= -256) {
                            var7 = var0.ct - var0.orientation & 2047;
                            if (var7 > 1024) {
                                var7 -= 2048;
                            }

                            int var8 = var0.ai;
                            if (var7 >= -256 && var7 <= 256) {
                                var8 = var0.stanceId;
                            } else if (var7 >= 256 && var7 < 768) {
                                var8 = var0.at;
                            } else if (var7 >= -768 && var7 <= -256) {
                                var8 = var0.al;
                            }

                            if (var8 == -1) {
                                var8 = var0.stanceId;
                            }

                            var0.bs = var8;
                            int var9 = 4;
                            boolean var10 = true;
                            if (var0 instanceof Npc) {
                                var10 = ((Npc) var0).composite.ah;
                            }

                            if (var10) {
                                if (var0.orientation != var0.ct && var0.interactingEntityIndex == -1 && var0.ch != 0) {
                                    var9 = 2;
                                }

                                if (var0.speed > 2) {
                                    var9 = 6;
                                }

                                if (var0.speed > 3) {
                                    var9 = 8;
                                }

                                if (var0.cq > 0 && var0.speed > 1) {
                                    var9 = 8;
                                    --var0.cq;
                                }
                            } else {
                                if (var0.speed > 1) {
                                    var9 = 6;
                                }

                                if (var0.speed > 2) {
                                    var9 = 8;
                                }

                                if (var0.cq > 0 && var0.speed > 1) {
                                    var9 = 8;
                                    --var0.cq;
                                }
                            }

                            if (var13 == 2) {
                                var9 <<= 1;
                            }

                            if (var9 >= 8 && var0.stanceId == var0.bs && var0.ag != -1) {
                                var0.bs = var0.ag;
                            }

                            if (var2 != var4 || var5 != var3) {
                                if (var2 < var4) {
                                    var0.regionX += var9;
                                    if (var0.regionX > var4) {
                                        var0.regionX = var4;
                                    }
                                } else if (var2 > var4) {
                                    var0.regionX -= var9;
                                    if (var0.regionX < var4) {
                                        var0.regionX = var4;
                                    }
                                }

                                if (var3 < var5) {
                                    var0.regionY += var9;
                                    if (var0.regionY > var5) {
                                        var0.regionY = var5;
                                    }
                                } else if (var3 > var5) {
                                    var0.regionY -= var9;
                                    if (var0.regionY < var5) {
                                        var0.regionY = var5;
                                    }
                                }
                            }

                            if (var4 == var0.regionX && var5 == var0.regionY) {
                                --var0.speed;
                                if (var0.ci > 0) {
                                    --var0.ci;
                                }
                            }
                        } else {
                            var0.regionX = var4;
                            var0.regionY = var5;
                            --var0.speed;
                            if (var0.ci > 0) {
                                --var0.ci;
                            }
                        }
                    }
                }
            }
        }

        if (var0.regionX < 128 || var0.regionY < 128 || var0.regionX >= 13184 || var0.regionY >= 13184) {
            var0.animationId = -1;
            var0.bx = -1;
            var0.bd = 0;
            var0.cb = 0;
            var0.regionX = var0.co[0] * 128 + var0.ap * 64;
            var0.regionY = var0.cv[0] * 128 + var0.ap * 64;
            var0.ac();
        }

        if (az.il == var0
                && (var0.regionX < 1536 || var0.regionY < 1536 || var0.regionX >= 11776 || var0.regionY >= 11776)) {
            var0.animationId = -1;
            var0.bx = -1;
            var0.bd = 0;
            var0.cb = 0;
            var0.regionX = var0.co[0] * 128 + var0.ap * 64;
            var0.regionY = var0.cv[0] * 128 + var0.ap * 64;
            var0.ac();
        }

        fc(var0);
        var0.animatingHeldItems = false;
        if (var0.bs != -1) {
            var11 = ff.t(var0.bs);
            if (var11 != null && var11.b != null) {
                ++var0.bt;
                if (var0.stanceFrameIndex < var11.b.length && var0.bt > var11.x[var0.stanceFrameIndex]) {
                    var0.bt = 1;
                    ++var0.stanceFrameIndex;
                    eq.fd(var11, var0.stanceFrameIndex, var0.regionX, var0.regionY);
                }

                if (var0.stanceFrameIndex >= var11.b.length) {
                    var0.bt = 0;
                    var0.stanceFrameIndex = 0;
                    eq.fd(var11, var0.stanceFrameIndex, var0.regionX, var0.regionY);
                }
            } else {
                var0.bs = -1;
            }
        }

        if (var0.bx != -1 && Client.bz >= var0.bv) {
            if (var0.graphicId < 0) {
                var0.graphicId = 0;
            }

            var2 = ah.t(var0.bx).e;
            if (var2 != -1) {
                kf var12 = ff.t(var2);
                if (var12 != null && var12.b != null) {
                    ++var0.bo;
                    if (var0.graphicId < var12.b.length && var0.bo > var12.x[var0.graphicId]) {
                        var0.bo = 1;
                        ++var0.graphicId;
                        eq.fd(var12, var0.graphicId, var0.regionX, var0.regionY);
                    }

                    if (var0.graphicId >= var12.b.length && (var0.graphicId < 0 || var0.graphicId >= var12.b.length)) {
                        var0.bx = -1;
                    }
                } else {
                    var0.bx = -1;
                }
            } else {
                var0.bx = -1;
            }
        }

        if (var0.animationId != -1 && var0.bq <= 1) {
            var11 = ff.t(var0.animationId);
            if (var11.k == 1 && var0.ci > 0 && var0.bd <= Client.bz && var0.cb < Client.bz) {
                var0.bq = 1;
                return;
            }
        }

        if (var0.animationId != -1 && var0.bq == 0) {
            var11 = ff.t(var0.animationId);
            if (var11 != null && var11.b != null) {
                ++var0.bb;
                if (var0.animationFrameIndex < var11.b.length && var0.bb > var11.x[var0.animationFrameIndex]) {
                    var0.bb = 1;
                    ++var0.animationFrameIndex;
                    eq.fd(var11, var0.animationFrameIndex, var0.regionX, var0.regionY);
                }

                if (var0.animationFrameIndex >= var11.b.length) {
                    var0.animationFrameIndex -= var11.g;
                    ++var0.bz;
                    if (var0.bz >= var11.j) {
                        var0.animationId = -1;
                    } else if (var0.animationFrameIndex >= 0 && var0.animationFrameIndex < var11.b.length) {
                        eq.fd(var11, var0.animationFrameIndex, var0.regionX, var0.regionY);
                    } else {
                        var0.animationId = -1;
                    }
                }

                var0.animatingHeldItems = var11.o;
            } else {
                var0.animationId = -1;
            }
        }

        if (var0.bq > 0) {
            --var0.bq;
        }

    }

    @ObfuscatedName("fc")
    static final void fc(Character var0) {
        if (var0.ch != 0) {
            if (var0.interactingEntityIndex != -1) {
                Object var1 = null;
                if (var0.interactingEntityIndex < 32768) {
                    var1 = Client.loadedNpcs[var0.interactingEntityIndex];
                } else if (var0.interactingEntityIndex >= 32768) {
                    var1 = Client.loadedPlayers[var0.interactingEntityIndex - '耀'];
                }

                if (var1 != null) {
                    int var2 = var0.regionX - ((Character) var1).regionX;
                    int var3 = var0.regionY - ((Character) var1).regionY;
                    if (var2 != 0 || var3 != 0) {
                        var0.ct = (int) (Math.atan2((double) var2, (double) var3) * 325.949D) & 2047;
                    }
                } else if (var0.bm) {
                    var0.interactingEntityIndex = -1;
                    var0.bm = false;
                }
            }

            if (var0.bh != -1 && (var0.speed == 0 || var0.cq > 0)) {
                var0.ct = var0.bh;
                var0.bh = -1;
            }

            int var4 = var0.ct - var0.orientation & 2047;
            if (var4 == 0 && var0.bm) {
                var0.interactingEntityIndex = -1;
                var0.bm = false;
            }

            if (var4 != 0) {
                ++var0.cw;
                boolean var6;
                if (var4 > 1024) {
                    var0.orientation -= var0.ch;
                    var6 = true;
                    if (var4 < var0.ch || var4 > 2048 - var0.ch) {
                        var0.orientation = var0.ct;
                        var6 = false;
                    }

                    if (var0.au == var0.bs && (var0.cw > 25 || var6)) {
                        if (var0.ax != -1) {
                            var0.bs = var0.ax;
                        } else {
                            var0.bs = var0.stanceId;
                        }
                    }
                } else {
                    var0.orientation += var0.ch;
                    var6 = true;
                    if (var4 < var0.ch || var4 > 2048 - var0.ch) {
                        var0.orientation = var0.ct;
                        var6 = false;
                    }

                    if (var0.au == var0.bs && (var0.cw > 25 || var6)) {
                        if (var0.ar != -1) {
                            var0.bs = var0.ar;
                        } else {
                            var0.bs = var0.stanceId;
                        }
                    }
                }

                var0.orientation &= 2047;
            } else {
                var0.cw = 0;
            }

        }
    }

    @ObfuscatedName("kf")
    static void kf(int var0) {
        Client.gn = var0;
    }
}
