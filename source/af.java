import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("af")
public class af {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    byte[][][] q;

    af(int var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
   void t(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      if (var7 != 0 && this.t != 0 && this.q != null) {
         var8 = this.q(var8, var7);
         var7 = this.i(var7);
         li.do(var1, var2, var5, var6, var3, var4, this.q[var7 - 1][var8], this.t);
      }
   }

    @ObfuscatedName("q")
    int q(int var1, int var2) {
        if (var2 == 9) {
            var1 = var1 + 1 & 3;
        }

        if (var2 == 10) {
            var1 = var1 + 3 & 3;
        }

        if (var2 == 11) {
            var1 = var1 + 3 & 3;
        }

        return var1;
    }

    @ObfuscatedName("i")
    int i(int var1) {
        if (var1 != 9 && var1 != 10) {
            return var1 == 11 ? 8 : var1;
        } else {
            return 1;
        }
    }

    @ObfuscatedName("a")
    void a() {
        if (this.q == null) {
            this.q = new byte[8][4][];
            this.l();
            this.b();
            this.e();
            this.x();
            this.p();
            this.o();
            this.c();
            this.u();
        }
    }

    @ObfuscatedName("l")
    void l() {
        byte[] var1 = new byte[this.t * this.t];
        int var2 = 0;

        int var3;
        int var4;
        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[0][0] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[0][1] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[0][2] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[0][3] = var1;
    }

    @ObfuscatedName("b")
    void b() {
        byte[] var1 = new byte[this.t * this.t];
        int var2 = 0;

        int var3;
        int var4;
        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[1][0] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var2 >= 0 && var2 < var1.length) {
                    if (var4 >= var3 << 1) {
                        var1[var2] = -1;
                    }

                    ++var2;
                } else {
                    ++var2;
                }
            }
        }

        this.q[1][1] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 <= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[1][2] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 >= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[1][3] = var1;
    }

    @ObfuscatedName("e")
    void e() {
        byte[] var1 = new byte[this.t * this.t];
        int var2 = 0;

        int var3;
        int var4;
        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 <= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[2][0] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[2][1] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[2][2] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 >= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[2][3] = var1;
    }

    @ObfuscatedName("x")
    void x() {
        byte[] var1 = new byte[this.t * this.t];
        int var2 = 0;

        int var3;
        int var4;
        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[3][0] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[3][1] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 >= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[3][2] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 <= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[3][3] = var1;
    }

    @ObfuscatedName("p")
    void p() {
        byte[] var1 = new byte[this.t * this.t];
        int var2 = 0;

        int var3;
        int var4;
        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 >= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[4][0] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[4][1] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3 >> 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[4][2] = var1;
        var1 = new byte[this.t * this.t];
        var2 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 <= var3 << 1) {
                    var1[var2] = -1;
                }

                ++var2;
            }
        }

        this.q[4][3] = var1;
    }

    @ObfuscatedName("o")
    void o() {
        byte[] var1 = new byte[this.t * this.t];
        boolean var2 = false;
        var1 = new byte[this.t * this.t];
        int var5 = 0;

        int var3;
        int var4;
        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[5][0] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var3 <= this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[5][1] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[5][2] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var3 >= this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[5][3] = var1;
    }

    @ObfuscatedName("c")
    void c() {
        byte[] var1 = new byte[this.t * this.t];
        boolean var2 = false;
        var1 = new byte[this.t * this.t];
        int var5 = 0;

        int var3;
        int var4;
        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[6][0] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 <= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[6][1] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 <= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[6][2] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 <= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[6][3] = var1;
    }

    @ObfuscatedName("u")
    void u() {
        byte[] var1 = new byte[this.t * this.t];
        boolean var2 = false;
        var1 = new byte[this.t * this.t];
        int var5 = 0;

        int var3;
        int var4;
        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[7][0] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = 0; var4 < this.t; ++var4) {
                if (var4 >= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[7][1] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = this.t - 1; var3 >= 0; --var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 >= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[7][2] = var1;
        var1 = new byte[this.t * this.t];
        var5 = 0;

        for (var3 = 0; var3 < this.t; ++var3) {
            for (var4 = this.t - 1; var4 >= 0; --var4) {
                if (var4 >= var3 - this.t / 2) {
                    var1[var5] = -1;
                }

                ++var5;
            }
        }

        this.q[7][3] = var1;
    }

    @ObfuscatedName("t")
    public static void t(FileSystem var0) {
        jz.t = var0;
    }

    @ObfuscatedName("b")
    static void b(boolean var0) {
        ci.loginLine1 = "";
        ci.loginLine2 = "Enter your username/email & password.";
        ci.loginLine3 = "";
        ci.loginState = 2;
        if (var0) {
            ci.password = "";
        }

        Tile.a();
        if (ci.aw && ci.username != null && ci.username.length() > 0) {
            ci.be = 1;
        } else {
            ci.be = 0;
        }

    }
}
