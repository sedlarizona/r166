import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lq")
public class lq {

    @ObfuscatedName("t")
    static char[] t = new char[64];

    @ObfuscatedName("q")
    static char[] q;

    @ObfuscatedName("i")
    static int[] i;

    static {
        int var0;
        for (var0 = 0; var0 < 26; ++var0) {
            t[var0] = (char) (var0 + 65);
        }

        for (var0 = 26; var0 < 52; ++var0) {
            t[var0] = (char) (var0 + 97 - 26);
        }

        for (var0 = 52; var0 < 62; ++var0) {
            t[var0] = (char) (var0 + 48 - 52);
        }

        t[62] = '+';
        t[63] = '/';
        q = new char[64];

        for (var0 = 0; var0 < 26; ++var0) {
            q[var0] = (char) (var0 + 65);
        }

        for (var0 = 26; var0 < 52; ++var0) {
            q[var0] = (char) (var0 + 97 - 26);
        }

        for (var0 = 52; var0 < 62; ++var0) {
            q[var0] = (char) (var0 + 48 - 52);
        }

        q[62] = '*';
        q[63] = '-';
        i = new int[128];

        for (var0 = 0; var0 < i.length; ++var0) {
            i[var0] = -1;
        }

        for (var0 = 65; var0 <= 90; ++var0) {
            i[var0] = var0 - 65;
        }

        for (var0 = 97; var0 <= 122; ++var0) {
            i[var0] = var0 - 97 + 26;
        }

        for (var0 = 48; var0 <= 57; ++var0) {
            i[var0] = var0 - 48 + 52;
        }

        int[] var2 = i;
        i[43] = 62;
        var2[42] = 62;
        int[] var1 = i;
        i[47] = 63;
        var1[45] = 63;
    }
}
