import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cx")
public class cx {

    @ObfuscatedName("i")
    static byte[] i = new byte[2048];

    @ObfuscatedName("a")
    static byte[] a = new byte[2048];

    @ObfuscatedName("l")
    static ByteBuffer[] l = new ByteBuffer[2048];

    @ObfuscatedName("b")
    static int b = 0;

    @ObfuscatedName("e")
    static int[] e = new int[2048];

    @ObfuscatedName("x")
    static int x = 0;

    @ObfuscatedName("p")
    static int[] p = new int[2048];

    @ObfuscatedName("g")
    static int[] g = new int[2048];

    @ObfuscatedName("n")
    static int[] n = new int[2048];

    @ObfuscatedName("o")
    static int[] o = new int[2048];

    @ObfuscatedName("c")
    static int c = 0;

    @ObfuscatedName("v")
    static int[] v = new int[2048];

    @ObfuscatedName("u")
    static ByteBuffer u = new ByteBuffer(new byte[5000]);

    @ObfuscatedName("t")
    public static final void t(long var0) {
        if (var0 > 0L) {
            if (0L == var0 % 10L) {
                long var2 = var0 - 1L;

                try {
                    Thread.sleep(var2);
                } catch (InterruptedException var8) {
                    ;
                }

                try {
                    Thread.sleep(1L);
                } catch (InterruptedException var7) {
                    ;
                }
            } else {
                try {
                    Thread.sleep(var0);
                } catch (InterruptedException var6) {
                    ;
                }
            }

        }
    }

    @ObfuscatedName("p")
    static void p(String var0, String var1, String var2) {
        ci.loginLine1 = var0;
        ci.loginLine2 = var1;
        ci.loginLine3 = var2;
    }

    @ObfuscatedName("gp")
    static void gp() {
        if (Client.jq >= 0 && Client.loadedPlayers[Client.jq] != null) {
            bt.gh(Client.loadedPlayers[Client.jq], false);
        }

    }

    @ObfuscatedName("ji")
    static final void ji(SubWindow var0, boolean var1) {
        int var2 = var0.targetWindowId;
        int var3 = (int) var0.uid;
        var0.kc();
        int var5;
        if (var1 && var2 != -1 && c.loadedInterfaces[var2]) {
            RTComponent.x.f(var2);
            if (RTComponent.b[var2] != null) {
                boolean var9 = true;

                for (var5 = 0; var5 < RTComponent.b[var2].length; ++var5) {
                    if (RTComponent.b[var2][var5] != null) {
                        if (RTComponent.b[var2][var5].type != 2) {
                            RTComponent.b[var2][var5] = null;
                        } else {
                            var9 = false;
                        }
                    }
                }

                if (var9) {
                    RTComponent.b[var2] = null;
                }

                c.loadedInterfaces[var2] = false;
            }
        }

        ao.jx(var2);
        RTComponent var4 = Inflater.t(var3);
        if (var4 != null) {
            GameEngine.jk(var4);
        }

        for (var5 = 0; var5 < Client.menuSize; ++var5) {
            int var7 = Client.menuOpcodes[var5];
            boolean var6 = var7 == 57 || var7 == 58 || var7 == 1007 || var7 == 25 || var7 == 30;
            if (var6) {
                if (var5 < Client.menuSize - 1) {
                    for (int var8 = var5; var8 < Client.menuSize - 1; ++var8) {
                        Client.menuActions[var8] = Client.menuActions[var8 + 1];
                        Client.menuTargets[var8] = Client.menuTargets[var8 + 1];
                        Client.menuOpcodes[var8] = Client.menuOpcodes[var8 + 1];
                        Client.ka[var8] = Client.ka[var8 + 1];
                        Client.menuArg1[var8] = Client.menuArg1[var8 + 1];
                        Client.menuArg2[var8] = Client.menuArg2[var8 + 1];
                        Client.kc[var8] = Client.kc[var8 + 1];
                    }
                }

                --Client.menuSize;
            }
        }

        if (Client.hudIndex != -1) {
            g.iz(Client.hudIndex, 1);
        }

    }
}
