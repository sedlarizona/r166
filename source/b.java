import java.lang.management.GarbageCollectorMXBean;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("b")
final class b implements t {

    @ObfuscatedName("l")
    static int l;

    @ObfuscatedName("x")
    static int menuX;

    @ObfuscatedName("aa")
    protected static GarbageCollectorMXBean aa;

    @ObfuscatedName("ev")
    static km ev;

    @ObfuscatedName("t")
    public static int t(int var0, int var1) {
        int var2;
        for (var2 = 1; var1 > 1; var1 >>= 1) {
            if ((var1 & 1) != 0) {
                var2 = var0 * var2;
            }

            var0 *= var0;
        }

        if (var1 == 1) {
            return var0 * var2;
        } else {
            return var2;
        }
    }

    @ObfuscatedName("jg")
    static final void jg(int var0) {
        an.jc();

        for (AreaSoundEmitter var1 = (AreaSoundEmitter) AreaSoundEmitter.t.e(); var1 != null; var1 = (AreaSoundEmitter) AreaSoundEmitter.t
                .p()) {
            if (var1.emittingObjectsDefinition != null) {
                var1.t();
            }
        }

        int var4 = Client.q(var0).control;
        if (var4 != 0) {
            int var2 = iv.varps[var0];
            if (var4 == 1) {
                if (var2 == 1) {
                    eu.b(0.9D);
                    ((dg) eu.r).q(0.9D);
                }

                if (var2 == 2) {
                    eu.b(0.8D);
                    ((dg) eu.r).q(0.8D);
                }

                if (var2 == 3) {
                    eu.b(0.7D);
                    ((dg) eu.r).q(0.7D);
                }

                if (var2 == 4) {
                    eu.b(0.6D);
                    ((dg) eu.r).q(0.6D);
                }

                ItemDefinition.o.a();
            }

            if (var4 == 3) {
                short var3 = 0;
                if (var2 == 0) {
                    var3 = 255;
                }

                if (var2 == 1) {
                    var3 = 192;
                }

                if (var2 == 2) {
                    var3 = 128;
                }

                if (var2 == 3) {
                    var3 = 64;
                }

                if (var2 == 4) {
                    var3 = 0;
                }

                if (var3 != Client.os) {
                    if (Client.os == 0 && Client.ox != -1) {
                        CombatBarData.i(Varpbit.ca, Client.ox, 0, var3, false);
                        Client.oe = false;
                    } else if (var3 == 0) {
                        RuneScriptStackItem.l();
                        Client.oe = false;
                    } else {
                        gv.a(var3);
                    }

                    Client.os = var3;
                }
            }

            if (var4 == 4) {
                if (var2 == 0) {
                    Client.ow = 127;
                }

                if (var2 == 1) {
                    Client.ow = 96;
                }

                if (var2 == 2) {
                    Client.ow = 64;
                }

                if (var2 == 3) {
                    Client.ow = 32;
                }

                if (var2 == 4) {
                    Client.ow = 0;
                }
            }

            if (var4 == 5) {
                Client.jp = var2;
            }

            if (var4 == 6) {
                Client.ly = var2;
            }

            if (var4 == 9) {
                Client.lq = var2;
            }

            if (var4 == 10) {
                if (var2 == 0) {
                    Client.pa = 127;
                }

                if (var2 == 1) {
                    Client.pa = 96;
                }

                if (var2 == 2) {
                    Client.pa = 64;
                }

                if (var2 == 3) {
                    Client.pa = 32;
                }

                if (var2 == 4) {
                    Client.pa = 0;
                }
            }

            if (var4 == 17) {
                Client.lc = var2 & '\uffff';
            }

            if (var4 == 18) {
                Client.cr = (cq) aj.t(hi.transform(), var2);
                if (Client.cr == null) {
                    Client.cr = cq.t;
                }
            }

            if (var4 == 19) {
                if (var2 == -1) {
                    Client.jq = -1;
                } else {
                    Client.jq = var2 & 2047;
                }
            }

            if (var4 == 22) {
                Client.co = (cq) aj.t(hi.transform(), var2);
                if (Client.co == null) {
                    Client.co = cq.t;
                }
            }

        }
    }
}
