import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.URI;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bs")
public class bs implements MouseListener, MouseMotionListener, FocusListener {

    @ObfuscatedName("a")
    public static bs mouse = new bs();

    @ObfuscatedName("l")
    public static volatile int l = 0;

    @ObfuscatedName("b")
    static volatile int b = 0;

    @ObfuscatedName("e")
    static volatile int e = -1;

    @ObfuscatedName("x")
    public static int x = 0;

    @ObfuscatedName("p")
    static volatile int p = -1;

    @ObfuscatedName("g")
    public static int g = 0;

    @ObfuscatedName("n")
    public static int n = 0;

    @ObfuscatedName("o")
    static volatile int o = 0;

    @ObfuscatedName("c")
    static volatile int c = 0;

    @ObfuscatedName("v")
    static volatile int v = 0;

    @ObfuscatedName("u")
    static volatile long u = 0L;

    @ObfuscatedName("j")
    public static int j = 0;

    @ObfuscatedName("k")
    public static int cameraZ = 0;

    @ObfuscatedName("z")
    public static int z = 0;

    @ObfuscatedName("w")
    public static long w = 0L;

    @ObfuscatedName("a")
    final int a(MouseEvent var1) {
        int var2 = var1.getButton();
        if (!var1.isAltDown() && var2 != 2) {
            return !var1.isMetaDown() && var2 != 3 ? 1 : 2;
        } else {
            return 4;
        }
    }

    public final synchronized void mousePressed(MouseEvent var1) {
        if (mouse != null) {
            l = 0;
            c = var1.getX();
            v = var1.getY();
            u = au.t();
            o = this.a(var1);
            if (o != 0) {
                b = o;
            }
        }

        if (var1.isPopupTrigger()) {
            var1.consume();
        }

    }

    public final synchronized void mouseReleased(MouseEvent var1) {
        if (mouse != null) {
            l = 0;
            b = 0;
        }

        if (var1.isPopupTrigger()) {
            var1.consume();
        }

    }

    public final void mouseClicked(MouseEvent var1) {
        if (var1.isPopupTrigger()) {
            var1.consume();
        }

    }

    public final synchronized void mouseEntered(MouseEvent var1) {
        if (mouse != null) {
            l = 0;
            e = var1.getX();
            p = var1.getY();
        }

    }

    public final synchronized void mouseDragged(MouseEvent var1) {
        if (mouse != null) {
            l = 0;
            e = var1.getX();
            p = var1.getY();
        }

    }

    public final void focusGained(FocusEvent var1) {
    }

    public final synchronized void mouseMoved(MouseEvent var1) {
        if (mouse != null) {
            l = 0;
            e = var1.getX();
            p = var1.getY();
        }

    }

    public final synchronized void mouseExited(MouseEvent var1) {
        if (mouse != null) {
            l = 0;
            e = -1;
            p = -1;
        }

    }

    public final synchronized void focusLost(FocusEvent var1) {
        if (mouse != null) {
            b = 0;
        }

    }

    @ObfuscatedName("q")
    public static void q(String var0, boolean var1, boolean var2) {
        if (var1) {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Action.BROWSE)) {
                try {
                    Desktop.getDesktop().browse(new URI(var0));
                    return;
                } catch (Exception var4) {
                    ;
                }
            }

            if (bc.q.startsWith("win")) {
                bg.i(var0, 0, "openjs");
            } else if (bc.q.startsWith("mac")) {
                bg.i(var0, 1, "openjs");
            } else {
                bg.i(var0, 2, "openjs");
            }
        } else {
            bg.i(var0, 3, "openjs");
        }

    }

    @ObfuscatedName("c")
    static final void c(int var0) {
        short var1 = 256;
        ci.am += var0 * 128;
        int var2;
        if (ci.am > ao.ao.length) {
            ci.am -= ao.ao.length;
            var2 = (int) (Math.random() * 12.0D);
            js.o(io.l[var2]);
        }

        var2 = 0;
        int var3 = var0 * 128;
        int var4 = (var1 - var0) * 128;

        int var5;
        int var6;
        for (var5 = 0; var5 < var4; ++var5) {
            var6 = hd.aj[var2 + var3] - var0 * ao.ao[var2 + ci.am & ao.ao.length - 1] / 6;
            if (var6 < 0) {
                var6 = 0;
            }

            hd.aj[var2++] = var6;
        }

        int var7;
        int var8;
        for (var5 = var1 - var0; var5 < var1; ++var5) {
            var6 = var5 * 128;

            for (var7 = 0; var7 < 128; ++var7) {
                var8 = (int) (Math.random() * 100.0D);
                if (var8 < 50 && var7 > 10 && var7 < 118) {
                    hd.aj[var6 + var7] = 255;
                } else {
                    hd.aj[var6 + var7] = 0;
                }
            }
        }

        if (ci.m > 0) {
            ci.m -= var0 * 4;
        }

        if (ci.ay > 0) {
            ci.ay -= var0 * 4;
        }

        if (ci.m == 0 && ci.ay == 0) {
            var5 = (int) (Math.random() * (double) (2000 / var0));
            if (var5 == 0) {
                ci.m = 1024;
            }

            if (var5 == 1) {
                ci.ay = 1024;
            }
        }

        for (var5 = 0; var5 < var1 - var0; ++var5) {
            ci.d[var5] = ci.d[var0 + var5];
        }

        for (var5 = var1 - var0; var5 < var1; ++var5) {
            ci.d[var5] = (int) (Math.sin((double) ci.ah / 14.0D) * 16.0D + Math.sin((double) ci.ah / 15.0D) * 14.0D + Math
                    .sin((double) ci.ah / 16.0D) * 12.0D);
            ++ci.ah;
        }

        ci.az += var0;
        var5 = (var0 + (Client.bz & 1)) / 2;
        if (var5 > 0) {
            for (var6 = 0; var6 < ci.az * 100; ++var6) {
                var7 = (int) (Math.random() * 124.0D) + 2;
                var8 = (int) (Math.random() * 128.0D) + 128;
                hd.aj[var7 + (var8 << 7)] = 192;
            }

            ci.az = 0;

            int var9;
            for (var6 = 0; var6 < var1; ++var6) {
                var7 = 0;
                var8 = var6 * 128;

                for (var9 = -var5; var9 < 128; ++var9) {
                    if (var9 + var5 < 128) {
                        var7 += hd.aj[var9 + var8 + var5];
                    }

                    if (var9 - (var5 + 1) >= 0) {
                        var7 -= hd.aj[var9 + var8 - (var5 + 1)];
                    }

                    if (var9 >= 0) {
                        bk.ae[var8 + var9] = var7 / (var5 * 2 + 1);
                    }
                }
            }

            for (var6 = 0; var6 < 128; ++var6) {
                var7 = 0;

                for (var8 = -var5; var8 < var1; ++var8) {
                    var9 = var8 * 128;
                    if (var5 + var8 < var1) {
                        var7 += bk.ae[var9 + var6 + var5 * 128];
                    }

                    if (var8 - (var5 + 1) >= 0) {
                        var7 -= bk.ae[var6 + var9 - (var5 + 1) * 128];
                    }

                    if (var8 >= 0) {
                        hd.aj[var9 + var6] = var7 / (var5 * 2 + 1);
                    }
                }
            }
        }

    }

    @ObfuscatedName("w")
    static int w(int var0, RuneScript var1, boolean var2) {
        String var3;
        if (var0 == 3100) {
            var3 = cs.p[--ly.g];
            j.t(0, "", var3);
            return 1;
        } else if (var0 == 3101) {
            b.menuX -= 2;
            Inflater.fv(az.il, cs.e[b.menuX], cs.e[b.menuX + 1]);
            return 1;
        } else if (var0 == 3103) {
            gd var15 = ap.t(fo.ax, Client.ee.l);
            Client.ee.i(var15);

            for (SubWindow var18 = (SubWindow) Client.subWindowTable.a(); var18 != null; var18 = (SubWindow) Client.subWindowTable
                    .l()) {
                if (var18.type == 0 || var18.type == 3) {
                    cx.ji(var18, true);
                }
            }

            if (Client.ls != null) {
                GameEngine.jk(Client.ls);
                Client.ls = null;
            }

            return 1;
        } else {
            int var22;
            if (var0 == 3104) {
                var3 = cs.p[--ly.g];
                var22 = 0;
                if (fl.q(var3)) {
                    var22 = ff.i(var3);
                }

                gd var19 = ap.t(fo.r, Client.ee.l);
                var19.i.e(var22);
                Client.ee.i(var19);
                return 1;
            } else {
                gd var16;
                if (var0 == 3105) {
                    var3 = cs.p[--ly.g];
                    var16 = ap.t(fo.ai, Client.ee.l);
                    var16.i.a(var3.length() + 1);
                    var16.i.u(var3);
                    Client.ee.i(var16);
                    return 1;
                } else if (var0 == 3106) {
                    var3 = cs.p[--ly.g];
                    var16 = ap.t(fo.bp, Client.ee.l);
                    var16.i.a(var3.length() + 1);
                    var16.i.u(var3);
                    Client.ee.i(var16);
                    return 1;
                } else {
                    String var4;
                    int var13;
                    int var14;
                    if (var0 != 3107) {
                        if (var0 == 3108) {
                            b.menuX -= 3;
                            var13 = cs.e[b.menuX];
                            var22 = cs.e[b.menuX + 1];
                            var14 = cs.e[b.menuX + 2];
                            RTComponent var21 = Inflater.t(var14);
                            au.iu(var21, var13, var22);
                            return 1;
                        } else if (var0 == 3109) {
                            b.menuX -= 2;
                            var13 = cs.e[b.menuX];
                            var22 = cs.e[b.menuX + 1];
                            RTComponent var17 = var2 ? ho.v : cs.c;
                            au.iu(var17, var13, var22);
                            return 1;
                        } else if (var0 == 3110) {
                            er.ch = cs.e[--b.menuX] == 1;
                            return 1;
                        } else if (var0 == 3111) {
                            cs.e[++b.menuX - 1] = al.qp.roofsDisabled ? 1 : 0;
                            return 1;
                        } else if (var0 == 3112) {
                            al.qp.roofsDisabled = cs.e[--b.menuX] == 1;
                            ar.i();
                            return 1;
                        } else if (var0 == 3113) {
                            var3 = cs.p[--ly.g];
                            boolean var12 = cs.e[--b.menuX] == 1;
                            q(var3, var12, false);
                            return 1;
                        } else if (var0 == 3115) {
                            var13 = cs.e[--b.menuX];
                            var16 = ap.t(fo.bw, Client.ee.l);
                            var16.i.l(var13);
                            Client.ee.i(var16);
                            return 1;
                        } else if (var0 == 3116) {
                            var13 = cs.e[--b.menuX];
                            ly.g -= 2;
                            var4 = cs.p[ly.g];
                            String var5 = cs.p[ly.g + 1];
                            if (var4.length() > 500) {
                                return 1;
                            } else if (var5.length() > 500) {
                                return 1;
                            } else {
                                gd var20 = ap.t(fo.at, Client.ee.l);
                                var20.i.l(1 + z.c(var4) + z.c(var5));
                                var20.i.br(var13);
                                var20.i.u(var5);
                                var20.i.u(var4);
                                Client.ee.i(var20);
                                return 1;
                            }
                        } else if (var0 == 3117) {
                            Client.ki = cs.e[--b.menuX] == 1;
                            return 1;
                        } else if (var0 == 3118) {
                            Client.ky = cs.e[--b.menuX] == 1;
                            return 1;
                        } else if (var0 == 3119) {
                            Client.iu = cs.e[--b.menuX] == 1;
                            return 1;
                        } else if (var0 == 3120) {
                            if (cs.e[--b.menuX] == 1) {
                                Client.jt |= 1;
                            } else {
                                Client.jt &= -2;
                            }

                            return 1;
                        } else if (var0 == 3121) {
                            if (cs.e[--b.menuX] == 1) {
                                Client.jt |= 2;
                            } else {
                                Client.jt &= -3;
                            }

                            return 1;
                        } else if (var0 == 3122) {
                            if (cs.e[--b.menuX] == 1) {
                                Client.jt |= 4;
                            } else {
                                Client.jt &= -5;
                            }

                            return 1;
                        } else if (var0 == 3123) {
                            if (cs.e[--b.menuX] == 1) {
                                Client.jt |= 8;
                            } else {
                                Client.jt &= -9;
                            }

                            return 1;
                        } else if (var0 == 3124) {
                            Client.jt = 0;
                            return 1;
                        } else if (var0 == 3125) {
                            Client.ho = cs.e[--b.menuX] == 1;
                            return 1;
                        } else if (var0 == 3126) {
                            Client.ir = cs.e[--b.menuX] == 1;
                            return 1;
                        } else if (var0 == 3127) {
                            ap.ii(cs.e[--b.menuX] == 1);
                            return 1;
                        } else if (var0 == 3128) {
                            cs.e[++b.menuX - 1] = ag.iy() ? 1 : 0;
                            return 1;
                        } else if (var0 == 3129) {
                            b.menuX -= 2;
                            Client.ht = cs.e[b.menuX];
                            Client.hq = cs.e[b.menuX + 1];
                            return 1;
                        } else {
                            return 2;
                        }
                    } else {
                        var13 = cs.e[--b.menuX];
                        var4 = cs.p[--ly.g];
                        var14 = cx.b;
                        int[] var6 = cx.e;
                        boolean var7 = false;
                        kb var8 = new kb(var4, ad.bc);

                        for (int var9 = 0; var9 < var14; ++var9) {
                            Player var10 = Client.loadedPlayers[var6[var9]];
                            if (var10 != null && var10 != az.il && var10.t != null && var10.t.equals(var8)) {
                                gd var11;
                                if (var13 == 1) {
                                    var11 = ap.t(fo.bo, Client.ee.l);
                                    var11.i.ba(0);
                                    var11.i.bn(var6[var9]);
                                    Client.ee.i(var11);
                                } else if (var13 == 4) {
                                    var11 = ap.t(fo.v, Client.ee.l);
                                    var11.i.br(0);
                                    var11.i.bn(var6[var9]);
                                    Client.ee.i(var11);
                                } else if (var13 == 6) {
                                    var11 = ap.t(fo.a, Client.ee.l);
                                    var11.i.a(0);
                                    var11.i.l(var6[var9]);
                                    Client.ee.i(var11);
                                } else if (var13 == 7) {
                                    var11 = ap.t(fo.co, Client.ee.l);
                                    var11.i.br(0);
                                    var11.i.by(var6[var9]);
                                    Client.ee.i(var11);
                                }

                                var7 = true;
                                break;
                            }
                        }

                        if (!var7) {
                            j.t(4, "", "Unable to find " + var4);
                        }

                        return 1;
                    }
                }
            }
        }
    }
}
