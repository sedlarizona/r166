import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ll")
public class ll {

    @ObfuscatedName("t")
    public static final ll t = new ll(0);

    @ObfuscatedName("q")
    public static final ll q = new ll(1);

    @ObfuscatedName("i")
    public static final ll i = new ll(2);

    @ObfuscatedName("a")
    public final int a;

    ll(int var1) {
        this.a = var1;
    }
}
