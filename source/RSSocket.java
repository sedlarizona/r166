import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fi")
public final class RSSocket extends fy implements Runnable {

    @ObfuscatedName("t")
    InputStream input;

    @ObfuscatedName("q")
    OutputStream output;

    @ObfuscatedName("i")
    Socket socket;

    @ObfuscatedName("a")
    boolean closed = false;

    @ObfuscatedName("l")
    TaskHandler taskHandler;

    @ObfuscatedName("b")
    Task task;

    @ObfuscatedName("e")
    byte[] buffer;

    @ObfuscatedName("x")
    int length = 0;

    @ObfuscatedName("p")
    int position = 0;

    @ObfuscatedName("g")
    boolean threwException = false;

    @ObfuscatedName("n")
    final int n;

    @ObfuscatedName("o")
    final int o;

    public RSSocket(Socket var1, TaskHandler var2, int var3) throws IOException {
        this.taskHandler = var2;
        this.socket = var1;
        this.n = var3;
        this.o = var3 - 100;
        this.socket.setSoTimeout(30000);
        this.socket.setTcpNoDelay(true);
        this.socket.setReceiveBufferSize(65536);
        this.socket.setSendBufferSize(65536);
        this.input = this.socket.getInputStream();
        this.output = this.socket.getOutputStream();
    }

    @ObfuscatedName("t")
    public boolean t(int var1)
            throws IOException {
        if (this.closed) {
            return false;
        } else {
            return this.input.available() >= var1;
        }
    }

    @ObfuscatedName("q")
    public int q()
            throws IOException {
        return this.closed ? 0 : this.input.available();
    }

    @ObfuscatedName("i")
    public int i()
            throws IOException {
        return this.closed ? 0 : this.input.read();
    }

    @ObfuscatedName("a")
    public int a(byte[] var1, int var2, int var3)
            throws IOException {
        if (this.closed) {
            return 0;
        } else {
            int var4;
            int var5;
            for (var4 = var3; var3 > 0; var3 -= var5) {
                var5 = this.input.read(var1, var2, var3);
                if (var5 <= 0) {
                    throw new EOFException();
                }

                var2 += var5;
            }

            return var4;
        }
    }

    @ObfuscatedName("l")
    public void l(byte[] var1, int var2, int var3)
            throws IOException {
        this.av(var1, var2, var3);
    }

    @ObfuscatedName("b")
    public void b() {
        if (!this.closed) {
            synchronized (this) {
                this.closed = true;
                this.notifyAll();
            }

            if (this.task != null) {
                while (this.task.l == 0) {
                    cx.t(1L);
                }

                if (this.task.l == 1) {
                    try {
                        ((Thread) this.task.p).join();
                    } catch (InterruptedException var3) {
                        ;
                    }
                }
            }

            this.task = null;
        }
    }

    @ObfuscatedName("av")
    void av(byte[] var1, int var2, int var3)
            throws IOException {
        if (!this.closed) {
            if (this.threwException) {
                this.threwException = false;
                throw new IOException();
            } else {
                if (this.buffer == null) {
                    this.buffer = new byte[this.n];
                }

                synchronized (this) {
                    for (int var5 = 0; var5 < var3; ++var5) {
                        this.buffer[this.position] = var1[var5 + var2];
                        this.position = (this.position + 1) % this.n;
                        if ((this.o + this.length) % this.n == this.position) {
                            throw new IOException();
                        }
                    }

                    if (this.task == null) {
                        this.task = this.taskHandler.a(this, 3);
                    }

                    this.notifyAll();
                }
            }
        }
    }

    protected void finalize() {
        this.b();
    }

    public void run() {
        try {
            while (true) {
                label84: {
                    int var1;
                    int var2;
                    synchronized (this) {
                        if (this.position == this.length) {
                            if (this.closed) {
                                break label84;
                            }

                            try {
                                this.wait();
                            } catch (InterruptedException var10) {
                                ;
                            }
                        }

                        var2 = this.length;
                        if (this.position >= this.length) {
                            var1 = this.position - this.length;
                        } else {
                            var1 = this.n - this.length;
                        }
                    }

                    if (var1 <= 0) {
                        continue;
                    }

                    try {
                        this.output.write(this.buffer, var2, var1);
                    } catch (IOException var9) {
                        this.threwException = true;
                    }

                    this.length = (var1 + this.length) % this.n;

                    try {
                        if (this.position == this.length) {
                            this.output.flush();
                        }
                    } catch (IOException var8) {
                        this.threwException = true;
                    }
                    continue;
                }

                try {
                    if (this.input != null) {
                        this.input.close();
                    }

                    if (this.output != null) {
                        this.output.close();
                    }

                    if (this.socket != null) {
                        this.socket.close();
                    }
                } catch (IOException var7) {
                    ;
                }

                this.buffer = null;
                break;
            }
        } catch (Exception var12) {
            FloorObject.t((String) null, var12);
        }

    }

    @ObfuscatedName("l")
    static void l() {
        cg.chatboxes.clear();
        cg.q.i();
        cg.i.t();
        cg.a = 0;
    }
}
