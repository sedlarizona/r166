import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hb")
public class hb extends Node {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    ho q;

    @ObfuscatedName("i")
    DataRequestNode i;

    @ObfuscatedName("a")
    hd a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    int v;

    @ObfuscatedName("u")
    int u;

    @ObfuscatedName("j")
    int j;

    @ObfuscatedName("k")
    int k;

    @ObfuscatedName("z")
    int z;

    @ObfuscatedName("w")
    dy w;

    @ObfuscatedName("s")
    int s;

    @ObfuscatedName("f")
    int f;

    @ObfuscatedName("t")
    void t() {
        this.q = null;
        this.i = null;
        this.a = null;
        this.w = null;
    }

    @ObfuscatedName("gv")
    static final void gv(boolean var0) {
        al.fb();
        ++Client.ee.n;
        if (Client.ee.n >= 50 || var0) {
            Client.ee.n = 0;
            if (!Client.ed && Client.ee.e() != null) {
                gd var1 = ap.t(fo.m, Client.ee.l);
                Client.ee.i(var1);

                try {
                    Client.ee.q();
                } catch (IOException var3) {
                    Client.ed = true;
                }
            }

        }
    }
}
