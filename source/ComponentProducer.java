import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Hashtable;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bm")
public final class ComponentProducer extends Producer {

    @ObfuscatedName("t")
    Component component;

    @ObfuscatedName("q")
    Image image;

    ComponentProducer(int var1, int var2, Component var3) {
        super.width = var1;
        super.height = var2;
        super.pixels = new int[var2 * var1 + 1];
        DataBufferInt var4 = new DataBufferInt(super.pixels, super.pixels.length);
        DirectColorModel var5 = new DirectColorModel(32, 16711680, 65280, 255);
        WritableRaster var6 = Raster.createWritableRaster(var5.createCompatibleSampleModel(super.width, super.height),
                var4, (Point) null);
        this.image = new BufferedImage(var5, var6, false, new Hashtable());
        this.t(var3);
        this.aj();
    }

    @ObfuscatedName("t")
    final void t(Component var1) {
        this.component = var1;
    }

    @ObfuscatedName("q")
    public final void q(int var1, int var2) {
        this.a(this.component.getGraphics(), var1, var2);
    }

    @ObfuscatedName("i")
    public final void i(int var1, int var2, int var3, int var4) {
        this.l(this.component.getGraphics(), var1, var2, var3, var4);
    }

    @ObfuscatedName("a")
    final void a(Graphics var1, int var2, int var3) {
        try {
            var1.drawImage(this.image, var2, var3, this.component);
        } catch (Exception var5) {
            this.component.repaint();
        }

    }

    @ObfuscatedName("l")
    final void l(Graphics var1, int var2, int var3, int var4, int var5) {
        try {
            Shape var6 = var1.getClip();
            var1.clipRect(var2, var3, var4, var5);
            var1.drawImage(this.image, 0, 0, this.component);
            var1.setClip(var6);
        } catch (Exception var7) {
            this.component.repaint();
        }

    }
}
