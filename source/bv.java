import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bv")
public class bv extends fv {

    @ObfuscatedName("t")
    protected boolean t(int var1, int var2, int var3, CollisionMap var4) {
        return var2 == super.t && var3 == super.q;
    }

    @ObfuscatedName("a")
    public static final boolean a(Model var0, int var1, int var2, int var3) {
        boolean var4 = ej.t;
        if (!var4) {
            return false;
        } else {
            ci.i();
            int var5 = var0.az + var1;
            int var6 = var2 + var0.ap;
            int var7 = var3 + var0.ah;
            int var8 = var0.au;
            int var9 = var0.ax;
            int var10 = var0.ar;
            int var11 = b.l - var5;
            int var12 = ej.b - var6;
            int var13 = bj.e - var7;
            if (Math.abs(var11) > var8 + lm.n) {
                return false;
            } else if (Math.abs(var12) > var9 + ej.o) {
                return false;
            } else if (Math.abs(var13) > var10 + ej.c) {
                return false;
            } else if (Math.abs(var13 * VarInfo.p - var12 * hd.g) > var9 * ej.c + var10 * ej.o) {
                return false;
            } else if (Math.abs(var11 * hd.g - var13 * Npc.x) > var10 * lm.n + var8 * ej.c) {
                return false;
            } else {
                return Math.abs(var12 * Npc.x - var11 * VarInfo.p) <= var9 * lm.n + var8 * ej.o;
            }
        }
    }

    @ObfuscatedName("a")
    static final boolean a(int var0, int var1, int var2, fv var3, CollisionMap var4) {
        int var5 = var0;
        int var6 = var1;
        byte var7 = 64;
        byte var8 = 64;
        int var9 = var0 - var7;
        int var10 = var1 - var8;
        fc.i[var7][var8] = 99;
        fc.a[var7][var8] = 0;
        byte var11 = 0;
        int var12 = 0;
        fc.x[var11] = var0;
        int var20 = var11 + 1;
        fc.p[var11] = var1;
        int[][] var13 = var4.flags;

        while (true) {
            label313: while (true) {
                int var14;
                int var15;
                int var16;
                int var17;
                int var18;
                int var19;
                do {
                    do {
                        do {
                            label290: do {
                                if (var20 == var12) {
                                    z.l = var5;
                                    j.b = var6;
                                    return false;
                                }

                                var5 = fc.x[var12];
                                var6 = fc.p[var12];
                                var12 = var12 + 1 & 4095;
                                var18 = var5 - var9;
                                var19 = var6 - var10;
                                var14 = var5 - var4.widthOffset;
                                var15 = var6 - var4.heightOffset;
                                if (var3.t(var2, var5, var6, var4)) {
                                    z.l = var5;
                                    j.b = var6;
                                    return true;
                                }

                                var16 = fc.a[var18][var19] + 1;
                                if (var18 > 0 && fc.i[var18 - 1][var19] == 0
                                        && (var13[var14 - 1][var15] & 19136782) == 0
                                        && (var13[var14 - 1][var15 + var2 - 1] & 19136824) == 0) {
                                    var17 = 1;

                                    while (true) {
                                        if (var17 >= var2 - 1) {
                                            fc.x[var20] = var5 - 1;
                                            fc.p[var20] = var6;
                                            var20 = var20 + 1 & 4095;
                                            fc.i[var18 - 1][var19] = 2;
                                            fc.a[var18 - 1][var19] = var16;
                                            break;
                                        }

                                        if ((var13[var14 - 1][var17 + var15] & 19136830) != 0) {
                                            break;
                                        }

                                        ++var17;
                                    }
                                }

                                if (var18 < 128 - var2 && fc.i[var18 + 1][var19] == 0
                                        && (var13[var14 + var2][var15] & 19136899) == 0
                                        && (var13[var14 + var2][var15 + var2 - 1] & 19136992) == 0) {
                                    var17 = 1;

                                    while (true) {
                                        if (var17 >= var2 - 1) {
                                            fc.x[var20] = var5 + 1;
                                            fc.p[var20] = var6;
                                            var20 = var20 + 1 & 4095;
                                            fc.i[var18 + 1][var19] = 8;
                                            fc.a[var18 + 1][var19] = var16;
                                            break;
                                        }

                                        if ((var13[var14 + var2][var15 + var17] & 19136995) != 0) {
                                            break;
                                        }

                                        ++var17;
                                    }
                                }

                                if (var19 > 0 && fc.i[var18][var19 - 1] == 0
                                        && (var13[var14][var15 - 1] & 19136782) == 0
                                        && (var13[var14 + var2 - 1][var15 - 1] & 19136899) == 0) {
                                    var17 = 1;

                                    while (true) {
                                        if (var17 >= var2 - 1) {
                                            fc.x[var20] = var5;
                                            fc.p[var20] = var6 - 1;
                                            var20 = var20 + 1 & 4095;
                                            fc.i[var18][var19 - 1] = 1;
                                            fc.a[var18][var19 - 1] = var16;
                                            break;
                                        }

                                        if ((var13[var14 + var17][var15 - 1] & 19136911) != 0) {
                                            break;
                                        }

                                        ++var17;
                                    }
                                }

                                if (var19 < 128 - var2 && fc.i[var18][var19 + 1] == 0
                                        && (var13[var14][var15 + var2] & 19136824) == 0
                                        && (var13[var14 + var2 - 1][var15 + var2] & 19136992) == 0) {
                                    var17 = 1;

                                    while (true) {
                                        if (var17 >= var2 - 1) {
                                            fc.x[var20] = var5;
                                            fc.p[var20] = var6 + 1;
                                            var20 = var20 + 1 & 4095;
                                            fc.i[var18][var19 + 1] = 4;
                                            fc.a[var18][var19 + 1] = var16;
                                            break;
                                        }

                                        if ((var13[var17 + var14][var15 + var2] & 19137016) != 0) {
                                            break;
                                        }

                                        ++var17;
                                    }
                                }

                                if (var18 > 0 && var19 > 0 && fc.i[var18 - 1][var19 - 1] == 0
                                        && (var13[var14 - 1][var15 - 1] & 19136782) == 0) {
                                    var17 = 1;

                                    while (true) {
                                        if (var17 >= var2) {
                                            fc.x[var20] = var5 - 1;
                                            fc.p[var20] = var6 - 1;
                                            var20 = var20 + 1 & 4095;
                                            fc.i[var18 - 1][var19 - 1] = 3;
                                            fc.a[var18 - 1][var19 - 1] = var16;
                                            break;
                                        }

                                        if ((var13[var14 - 1][var17 + (var15 - 1)] & 19136830) != 0
                                                || (var13[var17 + (var14 - 1)][var15 - 1] & 19136911) != 0) {
                                            break;
                                        }

                                        ++var17;
                                    }
                                }

                                if (var18 < 128 - var2 && var19 > 0 && fc.i[var18 + 1][var19 - 1] == 0
                                        && (var13[var14 + var2][var15 - 1] & 19136899) == 0) {
                                    var17 = 1;

                                    while (true) {
                                        if (var17 >= var2) {
                                            fc.x[var20] = var5 + 1;
                                            fc.p[var20] = var6 - 1;
                                            var20 = var20 + 1 & 4095;
                                            fc.i[var18 + 1][var19 - 1] = 9;
                                            fc.a[var18 + 1][var19 - 1] = var16;
                                            break;
                                        }

                                        if ((var13[var14 + var2][var17 + (var15 - 1)] & 19136995) != 0
                                                || (var13[var14 + var17][var15 - 1] & 19136911) != 0) {
                                            break;
                                        }

                                        ++var17;
                                    }
                                }

                                if (var18 > 0 && var19 < 128 - var2 && fc.i[var18 - 1][var19 + 1] == 0
                                        && (var13[var14 - 1][var15 + var2] & 19136824) == 0) {
                                    for (var17 = 1; var17 < var2; ++var17) {
                                        if ((var13[var14 - 1][var15 + var17] & 19136830) != 0
                                                || (var13[var17 + (var14 - 1)][var15 + var2] & 19137016) != 0) {
                                            continue label290;
                                        }
                                    }

                                    fc.x[var20] = var5 - 1;
                                    fc.p[var20] = var6 + 1;
                                    var20 = var20 + 1 & 4095;
                                    fc.i[var18 - 1][var19 + 1] = 6;
                                    fc.a[var18 - 1][var19 + 1] = var16;
                                }
                            } while (var18 >= 128 - var2);
                        } while (var19 >= 128 - var2);
                    } while (fc.i[var18 + 1][var19 + 1] != 0);
                } while ((var13[var14 + var2][var15 + var2] & 19136992) != 0);

                for (var17 = 1; var17 < var2; ++var17) {
                    if ((var13[var14 + var17][var15 + var2] & 19137016) != 0
                            || (var13[var14 + var2][var17 + var15] & 19136995) != 0) {
                        continue label313;
                    }
                }

                fc.x[var20] = var5 + 1;
                fc.p[var20] = var6 + 1;
                var20 = var20 + 1 & 4095;
                fc.i[var18 + 1][var19 + 1] = 12;
                fc.a[var18 + 1][var19 + 1] = var16;
            }
        }
    }

    @ObfuscatedName("x")
    public static void x() {
        jl.q.a();
    }

    @ObfuscatedName("x")
    static int x(int var0) {
        ChatboxMessage var1 = (ChatboxMessage) cg.q.t((long) var0);
        if (var1 == null) {
            return -1;
        } else {
            return var1.cl == cg.i.t ? -1 : ((ChatboxMessage) var1.cl).index;
        }
    }
}
