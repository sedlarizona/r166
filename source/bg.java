import java.net.URL;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bg")
public class bg implements cf {

    @ObfuscatedName("t")
    public static fy t;

    @ObfuscatedName("n")
    public static FileSystem n;

    @ObfuscatedName("ix")
    static int ix;

    @ObfuscatedName("t")
    public TaskData t() {
        return new AudioTaskData();
    }

    @ObfuscatedName("q")
    static int q(int var0, int var1) {
        ItemStorage var2 = (ItemStorage) ItemStorage.t.t((long) var0);
        if (var2 == null) {
            return 0;
        } else {
            return var1 >= 0 && var1 < var2.stackSizes.length ? var2.stackSizes[var1] : 0;
        }
    }

    @ObfuscatedName("i")
    static boolean i(String var0, int var1, String var2) {
        if (var1 == 0) {
            try {
                if (!bc.q.startsWith("win")) {
                    throw new Exception();
                } else if (!var0.startsWith("http://") && !var0.startsWith("https://")) {
                    throw new Exception();
                } else {
                    String var10 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?&=,.%+-_#:/*";

                    for (int var4 = 0; var4 < var0.length(); ++var4) {
                        if (var10.indexOf(var0.charAt(var4)) == -1) {
                            throw new Exception();
                        }
                    }

                    Runtime.getRuntime().exec("cmd /c start \"j\" \"" + var0 + "\"");
                    return true;
                }
            } catch (Throwable var5) {
                return false;
            }
        } else if (var1 == 1) {
            try {
                Object var3 = br.q(bc.t, var2, new Object[] { (new URL(bc.t.getCodeBase(), var0)).toString() });
                return var3 != null;
            } catch (Throwable var6) {
                return false;
            }
        } else if (var1 == 2) {
            try {
                bc.t.getAppletContext().showDocument(new URL(bc.t.getCodeBase(), var0), "_blank");
                return true;
            } catch (Exception var7) {
                return false;
            }
        } else if (var1 == 3) {
            try {
                br.t(bc.t, "loggedout");
            } catch (Throwable var9) {
                ;
            }

            try {
                bc.t.getAppletContext().showDocument(new URL(bc.t.getCodeBase(), var0), "_top");
                return true;
            } catch (Exception var8) {
                return false;
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @ObfuscatedName("e")
    static int e(int var0, RuneScript var1, boolean var2) {
        RTComponent var3;
        if (var0 >= 2000) {
            var0 -= 1000;
            var3 = Inflater.t(cs.e[--b.menuX]);
        } else {
            var3 = var2 ? ho.v : cs.c;
        }

        int var4;
        if (var0 == 1300) {
            var4 = cs.e[--b.menuX] - 1;
            if (var4 >= 0 && var4 <= 9) {
                var3.w(var4, cs.p[--ly.g]);
                return 1;
            } else {
                --ly.g;
                return 1;
            }
        } else if (var0 == 1301) {
            b.menuX -= 2;
            var4 = cs.e[b.menuX];
            int var5 = cs.e[b.menuX + 1];
            var3.dragParent = CollisionMap.q(var4, var5);
            return 1;
        } else if (var0 == 1302) {
            var3.cz = cs.e[--b.menuX] == 1;
            return 1;
        } else if (var0 == 1303) {
            var3.cj = cs.e[--b.menuX];
            return 1;
        } else if (var0 == 1304) {
            var3.cl = cs.e[--b.menuX];
            return 1;
        } else if (var0 == 1305) {
            var3.name = cs.p[--ly.g];
            return 1;
        } else if (var0 == 1306) {
            var3.cn = cs.p[--ly.g];
            return 1;
        } else if (var0 == 1307) {
            var3.actions = null;
            return 1;
        } else {
            return 2;
        }
    }

    @ObfuscatedName("fq")
    static int fq() {
        return Client.ny ? 2 : 1;
    }

    @ObfuscatedName("gf")
    static final void gf() {
        Client.ij = 0;
        int var0 = (az.il.regionX >> 7) + an.regionBaseX;
        int var1 = (az.il.regionY >> 7) + PlayerComposite.ep;
        if (var0 >= 3053 && var0 <= 3156 && var1 >= 3056 && var1 <= 3136) {
            Client.ij = 1;
        }

        if (var0 >= 3072 && var0 <= 3118 && var1 >= 9492 && var1 <= 9535) {
            Client.ij = 1;
        }

        if (Client.ij == 1 && var0 >= 3139 && var0 <= 3199 && var1 >= 3008 && var1 <= 3062) {
            Client.ij = 0;
        }

    }
}
