import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("d")
public class d {

    @ObfuscatedName("q")
    public static void q() {
        bs var0 = bs.mouse;
        synchronized (bs.mouse) {
            bs.g = bs.b;
            bs.n = bs.e;
            bs.x = bs.p;
            bs.j = bs.o;
            bs.cameraZ = bs.c;
            bs.z = bs.v;
            bs.w = bs.u;
            bs.o = 0;
        }
    }

    @ObfuscatedName("ea")
    static void ea(int var0) {
        if (var0 != Client.packet) {
            if (Client.packet == 0) {
                ir.ac.as();
            }

            if (var0 == 20 || var0 == 40 || var0 == 45) {
                Client.df = 0;
                Client.dq = 0;
                Client.dt = 0;
                Client.ey.i(var0);
                if (var0 != 20) {
                    Player.fl(false);
                }
            }

            if (var0 != 20 && var0 != 40 && ah.eu != null) {
                ah.eu.b();
                ah.eu = null;
            }

            if (Client.packet == 25) {
                Client.ex = 0;
                Client.ea = 0;
                Client.em = 1;
                Client.er = 0;
                Client.eh = 1;
            }

            if (var0 != 5 && var0 != 10) {
                if (var0 == 20) {
                    v.q(jk.db, au.cp, true, Client.packet == 11 ? 4 : 0);
                } else if (var0 == 11) {
                    v.q(jk.db, au.cp, false, 4);
                } else if (ci.t) {
                    ci.i = null;
                    ib.a = null;
                    io.l = null;
                    ItemNode.b = null;
                    iv.e = null;
                    ii.x = null;
                    ci.p = null;
                    ez.g = null;
                    ci.o = null;
                    jg.bj = null;
                    fr.bt = null;
                    av.by = null;
                    ai.bn = null;
                    fg.bb = null;
                    ah.r = null;
                    ho.y = null;
                    fz.h = null;
                    ItemStorage.f = null;
                    ao.ao = null;
                    TileModel.av = null;
                    hd.aj = null;
                    bk.ae = null;
                    hi.audioTrackId = 1;
                    hi.b = null;
                    fm.e = -1;
                    hi.x = -1;
                    hi.p = 0;
                    cr.n = false;
                    i.g = 2;
                    in.t(true);
                    ci.t = false;
                }
            } else {
                v.q(jk.db, au.cp, true, 0);
            }

            Client.packet = var0;
        }
    }
}
