import java.lang.reflect.Field;
import java.lang.reflect.Method;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.rs.Reflection;

@ObfuscatedName("cq")
public enum cq implements gl {
    @ObfuscatedName("t")
    t(0), @ObfuscatedName("q")
    q(1), @ObfuscatedName("i")
    i(2), @ObfuscatedName("a")
    a(3);

    @ObfuscatedName("l")
    final int l;

    cq(int var3) {
        this.l = var3;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.l;
    }

    @ObfuscatedName("i")
    public static void i(ByteBuffer var0, int var1) {
        ClassStructureNode var2 = new ClassStructureNode();
        var2.q = var0.av();
        var2.t = var0.ap();
        var2.i = new int[var2.q];
        var2.a = new int[var2.q];
        var2.fields = new Field[var2.q];
        var2.b = new int[var2.q];
        var2.methods = new Method[var2.q];
        var2.methodArguments = new byte[var2.q][][];

        for (int var3 = 0; var3 < var2.q; ++var3) {
            try {
                int var4 = var0.av();
                String var5;
                String var6;
                int var7;
                if (var4 != 0 && var4 != 1 && var4 != 2) {
                    if (var4 == 3 || var4 == 4) {
                        var5 = var0.ar();
                        var6 = var0.ar();
                        var7 = var0.av();
                        String[] var8 = new String[var7];

                        for (int var9 = 0; var9 < var7; ++var9) {
                            var8[var9] = var0.ar();
                        }

                        String var20 = var0.ar();
                        byte[][] var10 = new byte[var7][];
                        int var12;
                        if (var4 == 3) {
                            for (int var11 = 0; var11 < var7; ++var11) {
                                var12 = var0.ap();
                                var10[var11] = new byte[var12];
                                var0.al(var10[var11], 0, var12);
                            }
                        }

                        var2.i[var3] = var4;
                        Class[] var21 = new Class[var7];

                        for (var12 = 0; var12 < var7; ++var12) {
                            var21[var12] = GameEngine.a(var8[var12]);
                        }

                        Class var22 = GameEngine.a(var20);
                        if (GameEngine.a(var5).getClassLoader() == null) {
                            throw new SecurityException();
                        }

                        Method[] var13 = GameEngine.a(var5).getDeclaredMethods();
                        Method[] var14 = var13;

                        for (int var15 = 0; var15 < var14.length; ++var15) {
                            Method var16 = var14[var15];
                            if (Reflection.getMethodName(var16).equals(var6)) {
                                Class[] var17 = Reflection.getParameterTypes(var16);
                                if (var21.length == var17.length) {
                                    boolean var18 = true;

                                    for (int var19 = 0; var19 < var21.length; ++var19) {
                                        if (var17[var19] != var21[var19]) {
                                            var18 = false;
                                            break;
                                        }
                                    }

                                    if (var18 && var22 == var16.getReturnType()) {
                                        var2.methods[var3] = var16;
                                    }
                                }
                            }
                        }

                        var2.methodArguments[var3] = var10;
                    }
                } else {
                    var5 = var0.ar();
                    var6 = var0.ar();
                    var7 = 0;
                    if (var4 == 1) {
                        var7 = var0.ap();
                    }

                    var2.i[var3] = var4;
                    var2.b[var3] = var7;
                    if (GameEngine.a(var5).getClassLoader() == null) {
                        throw new SecurityException();
                    }

                    var2.fields[var3] = Reflection.findField(GameEngine.a(var5), var6);
                }
            } catch (ClassNotFoundException var24) {
                var2.a[var3] = -1;
            } catch (SecurityException var25) {
                var2.a[var3] = -2;
            } catch (NullPointerException var26) {
                var2.a[var3] = -3;
            } catch (Exception var27) {
                var2.a[var3] = -4;
            } catch (Throwable var28) {
                var2.a[var3] = -5;
            }
        }

        lm.t.q(var2);
    }
}
