import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ek")
public class Model extends Renderable {

    @ObfuscatedName("t")
    static Model t = new Model();

    @ObfuscatedName("q")
    static byte[] q = new byte[1];

    @ObfuscatedName("i")
    static Model i = new Model();

    @ObfuscatedName("a")
    static byte[] a = new byte[1];

    @ObfuscatedName("ai")
    static boolean[] ai = new boolean[4700];

    @ObfuscatedName("al")
    static boolean[] al = new boolean[4700];

    @ObfuscatedName("at")
    static int[] at = new int[4700];

    @ObfuscatedName("ag")
    static int[] ag = new int[4700];

    @ObfuscatedName("as")
    static int[] as = new int[4700];

    @ObfuscatedName("aw")
    static int[] aw = new int[4700];

    @ObfuscatedName("aq")
    static int[] aq = new int[4700];

    @ObfuscatedName("aa")
    static int[] aa = new int[4700];

    @ObfuscatedName("ak")
    static int[] ak = new int[1600];

    @ObfuscatedName("ab")
    static int[][] ab = new int[1600][512];

    @ObfuscatedName("ac")
    static int[] ac = new int[12];

    @ObfuscatedName("ad")
    static int[][] ad = new int[12][2000];

    @ObfuscatedName("bg")
    static int[] bg = new int[2000];

    @ObfuscatedName("br")
    static int[] br = new int[2000];

    @ObfuscatedName("ba")
    static int[] ba = new int[12];

    @ObfuscatedName("bk")
    static int[] bk = new int[10];

    @ObfuscatedName("be")
    static int[] be = new int[10];

    @ObfuscatedName("bc")
    static int[] bc = new int[10];

    @ObfuscatedName("bm")
    static int bm;

    @ObfuscatedName("bh")
    static int bh;

    @ObfuscatedName("bs")
    static int bs;

    @ObfuscatedName("bj")
    static boolean bj = true;

    @ObfuscatedName("bn")
    static int[] bn;

    @ObfuscatedName("bb")
    static int[] bb;

    @ObfuscatedName("bq")
    static int[] bq;

    @ObfuscatedName("bz")
    static int[] bz;

    @ObfuscatedName("l")
    int indexCount = 0;

    @ObfuscatedName("b")
    int[] xVertices;

    @ObfuscatedName("e")
    int[] yVertices;

    @ObfuscatedName("x")
    int[] zVertices;

    @ObfuscatedName("p")
    int texturedVertexCount = 0;

    @ObfuscatedName("g")
    int[] aIndices;

    @ObfuscatedName("n")
    int[] bIndices;

    @ObfuscatedName("o")
    int[] cIndices;

    @ObfuscatedName("c")
    int[] c;

    @ObfuscatedName("v")
    int[] v;

    @ObfuscatedName("u")
    int[] u;

    @ObfuscatedName("j")
    byte[] j;

    @ObfuscatedName("k")
    byte[] k;

    @ObfuscatedName("z")
    byte[] z;

    @ObfuscatedName("w")
    short[] w;

    @ObfuscatedName("s")
    byte s = 0;

    @ObfuscatedName("d")
    int vertexCount = 0;

    @ObfuscatedName("f")
    int[] f;

    @ObfuscatedName("r")
    int[] r;

    @ObfuscatedName("y")
    int[] y;

    @ObfuscatedName("h")
    int[][] h;

    @ObfuscatedName("m")
    int[][] m;

    @ObfuscatedName("ay")
    public boolean ay = false;

    @ObfuscatedName("ao")
    int ao;

    @ObfuscatedName("av")
    int av;

    @ObfuscatedName("aj")
    int aj;

    @ObfuscatedName("ae")
    int ae;

    @ObfuscatedName("am")
    int am;

    @ObfuscatedName("az")
    public int az;

    @ObfuscatedName("ap")
    public int ap;

    @ObfuscatedName("ah")
    public int ah;

    @ObfuscatedName("au")
    public int au = -1;

    @ObfuscatedName("ax")
    public int ax = -1;

    @ObfuscatedName("ar")
    public int ar = -1;

    static {
        bn = eu.m;
        bb = eu.ay;
        bq = eu.f;
        bz = eu.h;
    }

    Model() {
    }

    public Model(Model[] var1, int var2) {
        boolean var3 = false;
        boolean var4 = false;
        boolean var5 = false;
        boolean var6 = false;
        this.indexCount = 0;
        this.texturedVertexCount = 0;
        this.vertexCount = 0;
        this.s = -1;

        int var7;
        Model var8;
        for (var7 = 0; var7 < var2; ++var7) {
            var8 = var1[var7];
            if (var8 != null) {
                this.indexCount += var8.indexCount;
                this.texturedVertexCount += var8.texturedVertexCount;
                this.vertexCount += var8.vertexCount;
                if (var8.j != null) {
                    var3 = true;
                } else {
                    if (this.s == -1) {
                        this.s = var8.s;
                    }

                    if (this.s != var8.s) {
                        var3 = true;
                    }
                }

                var4 |= var8.k != null;
                var5 |= var8.w != null;
                var6 |= var8.z != null;
            }
        }

        this.xVertices = new int[this.indexCount];
        this.yVertices = new int[this.indexCount];
        this.zVertices = new int[this.indexCount];
        this.aIndices = new int[this.texturedVertexCount];
        this.bIndices = new int[this.texturedVertexCount];
        this.cIndices = new int[this.texturedVertexCount];
        this.c = new int[this.texturedVertexCount];
        this.v = new int[this.texturedVertexCount];
        this.u = new int[this.texturedVertexCount];
        if (var3) {
            this.j = new byte[this.texturedVertexCount];
        }

        if (var4) {
            this.k = new byte[this.texturedVertexCount];
        }

        if (var5) {
            this.w = new short[this.texturedVertexCount];
        }

        if (var6) {
            this.z = new byte[this.texturedVertexCount];
        }

        if (this.vertexCount > 0) {
            this.f = new int[this.vertexCount];
            this.r = new int[this.vertexCount];
            this.y = new int[this.vertexCount];
        }

        this.indexCount = 0;
        this.texturedVertexCount = 0;
        this.vertexCount = 0;

        for (var7 = 0; var7 < var2; ++var7) {
            var8 = var1[var7];
            if (var8 != null) {
                int var9;
                for (var9 = 0; var9 < var8.texturedVertexCount; ++var9) {
                    this.aIndices[this.texturedVertexCount] = this.indexCount + var8.aIndices[var9];
                    this.bIndices[this.texturedVertexCount] = this.indexCount + var8.bIndices[var9];
                    this.cIndices[this.texturedVertexCount] = this.indexCount + var8.cIndices[var9];
                    this.c[this.texturedVertexCount] = var8.c[var9];
                    this.v[this.texturedVertexCount] = var8.v[var9];
                    this.u[this.texturedVertexCount] = var8.u[var9];
                    if (var3) {
                        if (var8.j != null) {
                            this.j[this.texturedVertexCount] = var8.j[var9];
                        } else {
                            this.j[this.texturedVertexCount] = var8.s;
                        }
                    }

                    if (var4 && var8.k != null) {
                        this.k[this.texturedVertexCount] = var8.k[var9];
                    }

                    if (var5) {
                        if (var8.w != null) {
                            this.w[this.texturedVertexCount] = var8.w[var9];
                        } else {
                            this.w[this.texturedVertexCount] = -1;
                        }
                    }

                    if (var6) {
                        if (var8.z != null && var8.z[var9] != -1) {
                            this.z[this.texturedVertexCount] = (byte) (this.vertexCount + var8.z[var9]);
                        } else {
                            this.z[this.texturedVertexCount] = -1;
                        }
                    }

                    ++this.texturedVertexCount;
                }

                for (var9 = 0; var9 < var8.vertexCount; ++var9) {
                    this.f[this.vertexCount] = this.indexCount + var8.f[var9];
                    this.r[this.vertexCount] = this.indexCount + var8.r[var9];
                    this.y[this.vertexCount] = this.indexCount + var8.y[var9];
                    ++this.vertexCount;
                }

                for (var9 = 0; var9 < var8.indexCount; ++var9) {
                    this.xVertices[this.indexCount] = var8.xVertices[var9];
                    this.yVertices[this.indexCount] = var8.yVertices[var9];
                    this.zVertices[this.indexCount] = var8.zVertices[var9];
                    ++this.indexCount;
                }
            }
        }

    }

    @ObfuscatedName("t")
    public Model t(int[][] var1, int var2, int var3, int var4, boolean var5, int var6) {
        this.b();
        int var7 = var2 - this.aj;
        int var8 = var2 + this.aj;
        int var9 = var4 - this.aj;
        int var10 = var4 + this.aj;
        if (var7 >= 0 && var8 + 128 >> 7 < var1.length && var9 >= 0 && var10 + 128 >> 7 < var1[0].length) {
            var7 >>= 7;
            var8 = var8 + 127 >> 7;
            var9 >>= 7;
            var10 = var10 + 127 >> 7;
            if (var3 == var1[var7][var9] && var3 == var1[var8][var9] && var3 == var1[var7][var10]
                    && var3 == var1[var8][var10]) {
                return this;
            } else {
                Model var11;
                if (var5) {
                    var11 = new Model();
                    var11.indexCount = this.indexCount;
                    var11.texturedVertexCount = this.texturedVertexCount;
                    var11.vertexCount = this.vertexCount;
                    var11.xVertices = this.xVertices;
                    var11.zVertices = this.zVertices;
                    var11.aIndices = this.aIndices;
                    var11.bIndices = this.bIndices;
                    var11.cIndices = this.cIndices;
                    var11.c = this.c;
                    var11.v = this.v;
                    var11.u = this.u;
                    var11.j = this.j;
                    var11.k = this.k;
                    var11.z = this.z;
                    var11.w = this.w;
                    var11.s = this.s;
                    var11.f = this.f;
                    var11.r = this.r;
                    var11.y = this.y;
                    var11.h = this.h;
                    var11.m = this.m;
                    var11.ay = this.ay;
                    var11.yVertices = new int[var11.indexCount];
                } else {
                    var11 = this;
                }

                int var12;
                int var13;
                int var14;
                int var15;
                int var16;
                int var17;
                int var18;
                int var19;
                int var20;
                int var21;
                if (var6 == 0) {
                    for (var12 = 0; var12 < var11.indexCount; ++var12) {
                        var13 = var2 + this.xVertices[var12];
                        var14 = var4 + this.zVertices[var12];
                        var15 = var13 & 127;
                        var16 = var14 & 127;
                        var17 = var13 >> 7;
                        var18 = var14 >> 7;
                        var19 = var1[var17][var18] * (128 - var15) + var1[var17 + 1][var18] * var15 >> 7;
                        var20 = var1[var17][var18 + 1] * (128 - var15) + var15 * var1[var17 + 1][var18 + 1] >> 7;
                        var21 = var19 * (128 - var16) + var20 * var16 >> 7;
                        var11.yVertices[var12] = var21 + this.yVertices[var12] - var3;
                    }
                } else {
                    for (var12 = 0; var12 < var11.indexCount; ++var12) {
                        var13 = (-this.yVertices[var12] << 16) / super.modelHeight;
                        if (var13 < var6) {
                            var14 = var2 + this.xVertices[var12];
                            var15 = var4 + this.zVertices[var12];
                            var16 = var14 & 127;
                            var17 = var15 & 127;
                            var18 = var14 >> 7;
                            var19 = var15 >> 7;
                            var20 = var1[var18][var19] * (128 - var16) + var1[var18 + 1][var19] * var16 >> 7;
                            var21 = var1[var18][var19 + 1] * (128 - var16) + var16 * var1[var18 + 1][var19 + 1] >> 7;
                            int var22 = var20 * (128 - var17) + var21 * var17 >> 7;
                            var11.yVertices[var12] = (var6 - var13) * (var22 - var3) / var6 + this.yVertices[var12];
                        }
                    }
                }

                var11.o();
                return var11;
            }
        } else {
            return this;
        }
    }

    @ObfuscatedName("q")
    public Model q(boolean var1) {
        if (!var1 && q.length < this.texturedVertexCount) {
            q = new byte[this.texturedVertexCount + 100];
        }

        return this.a(var1, t, q);
    }

    @ObfuscatedName("i")
    public Model i(boolean var1) {
        if (!var1 && a.length < this.texturedVertexCount) {
            a = new byte[this.texturedVertexCount + 100];
        }

        return this.a(var1, i, a);
    }

    @ObfuscatedName("a")
    Model a(boolean var1, Model var2, byte[] var3) {
        var2.indexCount = this.indexCount;
        var2.texturedVertexCount = this.texturedVertexCount;
        var2.vertexCount = this.vertexCount;
        if (var2.xVertices == null || var2.xVertices.length < this.indexCount) {
            var2.xVertices = new int[this.indexCount + 100];
            var2.yVertices = new int[this.indexCount + 100];
            var2.zVertices = new int[this.indexCount + 100];
        }

        int var4;
        for (var4 = 0; var4 < this.indexCount; ++var4) {
            var2.xVertices[var4] = this.xVertices[var4];
            var2.yVertices[var4] = this.yVertices[var4];
            var2.zVertices[var4] = this.zVertices[var4];
        }

        if (var1) {
            var2.k = this.k;
        } else {
            var2.k = var3;
            if (this.k == null) {
                for (var4 = 0; var4 < this.texturedVertexCount; ++var4) {
                    var2.k[var4] = 0;
                }
            } else {
                for (var4 = 0; var4 < this.texturedVertexCount; ++var4) {
                    var2.k[var4] = this.k[var4];
                }
            }
        }

        var2.aIndices = this.aIndices;
        var2.bIndices = this.bIndices;
        var2.cIndices = this.cIndices;
        var2.c = this.c;
        var2.v = this.v;
        var2.u = this.u;
        var2.j = this.j;
        var2.z = this.z;
        var2.w = this.w;
        var2.s = this.s;
        var2.f = this.f;
        var2.r = this.r;
        var2.y = this.y;
        var2.h = this.h;
        var2.m = this.m;
        var2.ay = this.ay;
        var2.o();
        return var2;
    }

    @ObfuscatedName("l")
    void l(int var1) {
        if (this.au == -1) {
            int var2 = 0;
            int var3 = 0;
            int var4 = 0;
            int var5 = 0;
            int var6 = 0;
            int var7 = 0;
            int var8 = bb[var1];
            int var9 = bn[var1];

            for (int var10 = 0; var10 < this.indexCount; ++var10) {
                int var11 = eu.r(this.xVertices[var10], this.zVertices[var10], var8, var9);
                int var12 = this.yVertices[var10];
                int var13 = eu.y(this.xVertices[var10], this.zVertices[var10], var8, var9);
                if (var11 < var2) {
                    var2 = var11;
                }

                if (var11 > var5) {
                    var5 = var11;
                }

                if (var12 < var3) {
                    var3 = var12;
                }

                if (var12 > var6) {
                    var6 = var12;
                }

                if (var13 < var4) {
                    var4 = var13;
                }

                if (var13 > var7) {
                    var7 = var13;
                }
            }

            this.az = (var5 + var2) / 2;
            this.ap = (var6 + var3) / 2;
            this.ah = (var7 + var4) / 2;
            this.au = (var5 - var2 + 1) / 2;
            this.ax = (var6 - var3 + 1) / 2;
            this.ar = (var7 - var4 + 1) / 2;
            if (this.au < 32) {
                this.au = 32;
            }

            if (this.ar < 32) {
                this.ar = 32;
            }

            if (this.ay) {
                this.au += 8;
                this.ar += 8;
            }

        }
    }

    @ObfuscatedName("b")
    public void b() {
        if (this.ao != 1) {
            this.ao = 1;
            super.modelHeight = 0;
            this.av = 0;
            this.aj = 0;

            for (int var1 = 0; var1 < this.indexCount; ++var1) {
                int var2 = this.xVertices[var1];
                int var3 = this.yVertices[var1];
                int var4 = this.zVertices[var1];
                if (-var3 > super.modelHeight) {
                    super.modelHeight = -var3;
                }

                if (var3 > this.av) {
                    this.av = var3;
                }

                int var5 = var2 * var2 + var4 * var4;
                if (var5 > this.aj) {
                    this.aj = var5;
                }
            }

            this.aj = (int) (Math.sqrt((double) this.aj) + 0.99D);
            this.am = (int) (Math.sqrt((double) (this.aj * this.aj + super.modelHeight * super.modelHeight)) + 0.99D);
            this.ae = this.am + (int) (Math.sqrt((double) (this.aj * this.aj + this.av * this.av)) + 0.99D);
        }
    }

    @ObfuscatedName("e")
    void e() {
        if (this.ao != 2) {
            this.ao = 2;
            this.aj = 0;

            for (int var1 = 0; var1 < this.indexCount; ++var1) {
                int var2 = this.xVertices[var1];
                int var3 = this.yVertices[var1];
                int var4 = this.zVertices[var1];
                int var5 = var2 * var2 + var4 * var4 + var3 * var3;
                if (var5 > this.aj) {
                    this.aj = var5;
                }
            }

            this.aj = (int) (Math.sqrt((double) this.aj) + 0.99D);
            this.am = this.aj;
            this.ae = this.aj + this.aj;
        }
    }

    @ObfuscatedName("x")
    public int x() {
        this.b();
        return this.aj;
    }

    @ObfuscatedName("o")
    void o() {
        this.ao = 0;
        this.au = -1;
    }

    @ObfuscatedName("c")
    public void c(IdentityKitNode var1, int var2) {
        if (this.h != null) {
            if (var2 != -1) {
                IdentityKit var3 = var1.kits[var2];
                Skins var4 = var3.skins;
                bm = 0;
                bh = 0;
                bs = 0;

                for (int var5 = 0; var5 < var3.b; ++var5) {
                    int var6 = var3.e[var5];
                    this.k(var4.opcodes[var6], var4.skinList[var6], var3.x[var5], var3.p[var5], var3.g[var5]);
                }

                this.o();
            }
        }
    }

    @ObfuscatedName("u")
    public void u(IdentityKitNode var1, int var2, IdentityKitNode var3, int var4, int[] var5) {
        if (var2 != -1) {
            if (var5 != null && var4 != -1) {
                IdentityKit var6 = var1.kits[var2];
                IdentityKit var7 = var3.kits[var4];
                Skins var8 = var6.skins;
                bm = 0;
                bh = 0;
                bs = 0;
                byte var9 = 0;
                int var13 = var9 + 1;
                int var10 = var5[var9];

                int var11;
                int var12;
                for (var11 = 0; var11 < var6.b; ++var11) {
                    for (var12 = var6.e[var11]; var12 > var10; var10 = var5[var13++]) {
                        ;
                    }

                    if (var12 != var10 || var8.opcodes[var12] == 0) {
                        this.k(var8.opcodes[var12], var8.skinList[var12], var6.x[var11], var6.p[var11], var6.g[var11]);
                    }
                }

                bm = 0;
                bh = 0;
                bs = 0;
                var9 = 0;
                var13 = var9 + 1;
                var10 = var5[var9];

                for (var11 = 0; var11 < var7.b; ++var11) {
                    for (var12 = var7.e[var11]; var12 > var10; var10 = var5[var13++]) {
                        ;
                    }

                    if (var12 == var10 || var8.opcodes[var12] == 0) {
                        this.k(var8.opcodes[var12], var8.skinList[var12], var7.x[var11], var7.p[var11], var7.g[var11]);
                    }
                }

                this.o();
            } else {
                this.c(var1, var2);
            }
        }
    }

    @ObfuscatedName("k")
    void k(int var1, int[] var2, int var3, int var4, int var5) {
        int var6 = var2.length;
        int var7;
        int var8;
        int var11;
        int var12;
        if (var1 == 0) {
            var7 = 0;
            bm = 0;
            bh = 0;
            bs = 0;

            for (var8 = 0; var8 < var6; ++var8) {
                int var9 = var2[var8];
                if (var9 < this.h.length) {
                    int[] var10 = this.h[var9];

                    for (var11 = 0; var11 < var10.length; ++var11) {
                        var12 = var10[var11];
                        bm += this.xVertices[var12];
                        bh += this.yVertices[var12];
                        bs += this.zVertices[var12];
                        ++var7;
                    }
                }
            }

            if (var7 > 0) {
                bm = var3 + bm / var7;
                bh = var4 + bh / var7;
                bs = var5 + bs / var7;
            } else {
                bm = var3;
                bh = var4;
                bs = var5;
            }

        } else {
            int[] var18;
            int var19;
            if (var1 == 1) {
                for (var7 = 0; var7 < var6; ++var7) {
                    var8 = var2[var7];
                    if (var8 < this.h.length) {
                        var18 = this.h[var8];

                        for (var19 = 0; var19 < var18.length; ++var19) {
                            var11 = var18[var19];
                            this.xVertices[var11] += var3;
                            this.yVertices[var11] += var4;
                            this.zVertices[var11] += var5;
                        }
                    }
                }

            } else if (var1 == 2) {
                for (var7 = 0; var7 < var6; ++var7) {
                    var8 = var2[var7];
                    if (var8 < this.h.length) {
                        var18 = this.h[var8];

                        for (var19 = 0; var19 < var18.length; ++var19) {
                            var11 = var18[var19];
                            this.xVertices[var11] -= bm;
                            this.yVertices[var11] -= bh;
                            this.zVertices[var11] -= bs;
                            var12 = (var3 & 255) * 8;
                            int var13 = (var4 & 255) * 8;
                            int var14 = (var5 & 255) * 8;
                            int var15;
                            int var16;
                            int var17;
                            if (var14 != 0) {
                                var15 = bn[var14];
                                var16 = bb[var14];
                                var17 = var15 * this.yVertices[var11] + var16 * this.xVertices[var11] >> 16;
                                this.yVertices[var11] = var16 * this.yVertices[var11] - var15 * this.xVertices[var11] >> 16;
                                this.xVertices[var11] = var17;
                            }

                            if (var12 != 0) {
                                var15 = bn[var12];
                                var16 = bb[var12];
                                var17 = var16 * this.yVertices[var11] - var15 * this.zVertices[var11] >> 16;
                                this.zVertices[var11] = var15 * this.yVertices[var11] + var16 * this.zVertices[var11] >> 16;
                                this.yVertices[var11] = var17;
                            }

                            if (var13 != 0) {
                                var15 = bn[var13];
                                var16 = bb[var13];
                                var17 = var15 * this.zVertices[var11] + var16 * this.xVertices[var11] >> 16;
                                this.zVertices[var11] = var16 * this.zVertices[var11] - var15 * this.xVertices[var11] >> 16;
                                this.xVertices[var11] = var17;
                            }

                            this.xVertices[var11] += bm;
                            this.yVertices[var11] += bh;
                            this.zVertices[var11] += bs;
                        }
                    }
                }

            } else if (var1 == 3) {
                for (var7 = 0; var7 < var6; ++var7) {
                    var8 = var2[var7];
                    if (var8 < this.h.length) {
                        var18 = this.h[var8];

                        for (var19 = 0; var19 < var18.length; ++var19) {
                            var11 = var18[var19];
                            this.xVertices[var11] -= bm;
                            this.yVertices[var11] -= bh;
                            this.zVertices[var11] -= bs;
                            this.xVertices[var11] = var3 * this.xVertices[var11] / 128;
                            this.yVertices[var11] = var4 * this.yVertices[var11] / 128;
                            this.zVertices[var11] = var5 * this.zVertices[var11] / 128;
                            this.xVertices[var11] += bm;
                            this.yVertices[var11] += bh;
                            this.zVertices[var11] += bs;
                        }
                    }
                }

            } else if (var1 == 5) {
                if (this.m != null && this.k != null) {
                    for (var7 = 0; var7 < var6; ++var7) {
                        var8 = var2[var7];
                        if (var8 < this.m.length) {
                            var18 = this.m[var8];

                            for (var19 = 0; var19 < var18.length; ++var19) {
                                var11 = var18[var19];
                                var12 = (this.k[var11] & 255) + var3 * 8;
                                if (var12 < 0) {
                                    var12 = 0;
                                } else if (var12 > 255) {
                                    var12 = 255;
                                }

                                this.k[var11] = (byte) var12;
                            }
                        }
                    }
                }

            }
        }
    }

    @ObfuscatedName("z")
    public void z() {
        for (int var1 = 0; var1 < this.indexCount; ++var1) {
            int var2 = this.xVertices[var1];
            this.xVertices[var1] = this.zVertices[var1];
            this.zVertices[var1] = -var2;
        }

        this.o();
    }

    @ObfuscatedName("w")
    public void w() {
        for (int var1 = 0; var1 < this.indexCount; ++var1) {
            this.xVertices[var1] = -this.xVertices[var1];
            this.zVertices[var1] = -this.zVertices[var1];
        }

        this.o();
    }

    @ObfuscatedName("s")
    public void s() {
        for (int var1 = 0; var1 < this.indexCount; ++var1) {
            int var2 = this.zVertices[var1];
            this.zVertices[var1] = this.xVertices[var1];
            this.xVertices[var1] = -var2;
        }

        this.o();
    }

    @ObfuscatedName("d")
    public void d(int var1) {
        int var2 = bn[var1];
        int var3 = bb[var1];

        for (int var4 = 0; var4 < this.indexCount; ++var4) {
            int var5 = var3 * this.yVertices[var4] - var2 * this.zVertices[var4] >> 16;
            this.zVertices[var4] = var2 * this.yVertices[var4] + var3 * this.zVertices[var4] >> 16;
            this.yVertices[var4] = var5;
        }

        this.o();
    }

    @ObfuscatedName("f")
    public void f(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < this.indexCount; ++var4) {
            this.xVertices[var4] += var1;
            this.yVertices[var4] += var2;
            this.zVertices[var4] += var3;
        }

        this.o();
    }

    @ObfuscatedName("r")
    public void r(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < this.indexCount; ++var4) {
            this.xVertices[var4] = this.xVertices[var4] * var1 / 128;
            this.yVertices[var4] = var2 * this.yVertices[var4] / 128;
            this.zVertices[var4] = var3 * this.zVertices[var4] / 128;
        }

        this.o();
    }

    @ObfuscatedName("y")
    public final void y(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        ak[0] = -1;
        if (this.ao != 2 && this.ao != 1) {
            this.e();
        }

        int var8 = eu.c;
        int var9 = eu.v;
        int var10 = bn[var1];
        int var11 = bb[var1];
        int var12 = bn[var2];
        int var13 = bb[var2];
        int var14 = bn[var3];
        int var15 = bb[var3];
        int var16 = bn[var4];
        int var17 = bb[var4];
        int var18 = var16 * var6 + var17 * var7 >> 16;

        for (int var19 = 0; var19 < this.indexCount; ++var19) {
            int var20 = this.xVertices[var19];
            int var21 = this.yVertices[var19];
            int var22 = this.zVertices[var19];
            int var23;
            if (var3 != 0) {
                var23 = var21 * var14 + var20 * var15 >> 16;
                var21 = var21 * var15 - var20 * var14 >> 16;
                var20 = var23;
            }

            if (var1 != 0) {
                var23 = var21 * var11 - var22 * var10 >> 16;
                var22 = var21 * var10 + var22 * var11 >> 16;
                var21 = var23;
            }

            if (var2 != 0) {
                var23 = var22 * var12 + var20 * var13 >> 16;
                var22 = var22 * var13 - var20 * var12 >> 16;
                var20 = var23;
            }

            var20 += var5;
            var21 += var6;
            var22 += var7;
            var23 = var21 * var17 - var22 * var16 >> 16;
            var22 = var21 * var16 + var22 * var17 >> 16;
            as[var19] = var22 - var18;
            at[var19] = var20 * eu.o / var22 + var8;
            ag[var19] = var23 * eu.o / var22 + var9;
            if (this.vertexCount > 0) {
                aw[var19] = var20;
                aq[var19] = var23;
                aa[var19] = var22;
            }
        }

        try {
            this.av(false, false, false, 0);
        } catch (Exception var25) {
            ;
        }

    }

    @ObfuscatedName("h")
    public final void h(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        ak[0] = -1;
        if (this.ao != 2 && this.ao != 1) {
            this.e();
        }

        int var9 = eu.c;
        int var10 = eu.v;
        int var11 = bn[var1];
        int var12 = bb[var1];
        int var13 = bn[var2];
        int var14 = bb[var2];
        int var15 = bn[var3];
        int var16 = bb[var3];
        int var17 = bn[var4];
        int var18 = bb[var4];
        int var19 = var17 * var6 + var18 * var7 >> 16;

        for (int var20 = 0; var20 < this.indexCount; ++var20) {
            int var21 = this.xVertices[var20];
            int var22 = this.yVertices[var20];
            int var23 = this.zVertices[var20];
            int var24;
            if (var3 != 0) {
                var24 = var22 * var15 + var21 * var16 >> 16;
                var22 = var22 * var16 - var21 * var15 >> 16;
                var21 = var24;
            }

            if (var1 != 0) {
                var24 = var22 * var12 - var23 * var11 >> 16;
                var23 = var22 * var11 + var23 * var12 >> 16;
                var22 = var24;
            }

            if (var2 != 0) {
                var24 = var23 * var13 + var21 * var14 >> 16;
                var23 = var23 * var14 - var21 * var13 >> 16;
                var21 = var24;
            }

            var21 += var5;
            var22 += var6;
            var23 += var7;
            var24 = var22 * var18 - var23 * var17 >> 16;
            var23 = var22 * var17 + var23 * var18 >> 16;
            as[var20] = var23 - var19;
            at[var20] = var9 + var21 * eu.o / var8;
            ag[var20] = var10 + var24 * eu.o / var8;
            if (this.vertexCount > 0) {
                aw[var20] = var21;
                aq[var20] = var24;
                aa[var20] = var23;
            }
        }

        try {
            this.av(false, false, false, 0);
        } catch (Exception var26) {
            ;
        }

    }

    @ObfuscatedName("av")
    final void av(boolean var1, boolean var2, boolean var3, int var4) {
        if (this.ae < 1600) {
            int var5;
            for (var5 = 0; var5 < this.ae; ++var5) {
                ak[var5] = 0;
            }

            var5 = var3 ? 20 : 5;
            int var7;
            int var9;
            int var10;
            int var11;
            int var12;
            int var13;
            int var14;
            int var15;
            int var16;
            int var17;
            int var18;
            int var19;
            int var20;
            int var27;
            if (x.i && var2) {
                Model var6 = this;

                for (var7 = 0; var7 < var6.texturedVertexCount; ++var7) {
                    if (var6.u[var7] != -2) {
                        var27 = var6.aIndices[var7];
                        var9 = var6.bIndices[var7];
                        var10 = var6.cIndices[var7];
                        var11 = at[var27];
                        var12 = at[var9];
                        var13 = at[var10];
                        var14 = ag[var27];
                        var15 = ag[var9];
                        var16 = ag[var10];
                        var17 = Math.min(var11, Math.min(var12, var13)) - var5;
                        var18 = Math.max(var11, Math.max(var12, var13)) + var5;
                        var19 = Math.min(var14, Math.min(var15, var16)) - var5;
                        var20 = Math.max(var14, Math.max(var15, var16)) + var5;
                        v.i(var17, var19, var18, var20, -49088);
                    }
                }
            }

            int var26;
            for (var26 = 0; var26 < this.texturedVertexCount; ++var26) {
                if (this.u[var26] != -2) {
                    var7 = this.aIndices[var26];
                    var27 = this.bIndices[var26];
                    var9 = this.cIndices[var26];
                    var10 = at[var7];
                    var11 = at[var27];
                    var12 = at[var9];
                    if (var1 && (var10 == -5000 || var11 == -5000 || var12 == -5000)) {
                        var13 = aw[var7];
                        var14 = aw[var27];
                        var15 = aw[var9];
                        var16 = aq[var7];
                        var17 = aq[var27];
                        var18 = aq[var9];
                        var19 = aa[var7];
                        var20 = aa[var27];
                        int var21 = aa[var9];
                        var13 -= var14;
                        var15 -= var14;
                        var16 -= var17;
                        var18 -= var17;
                        var19 -= var20;
                        var21 -= var20;
                        int var22 = var16 * var21 - var19 * var18;
                        int var23 = var19 * var15 - var13 * var21;
                        int var24 = var13 * var18 - var16 * var15;
                        if (var14 * var22 + var17 * var23 + var20 * var24 > 0) {
                            al[var26] = true;
                            int var25 = (as[var7] + as[var27] + as[var9]) / 3 + this.am;
                            ab[var25][ak[var25]++] = var26;
                        }
                    } else {
                        if (var2) {
                            var14 = ag[var7];
                            var15 = ag[var27];
                            var16 = ag[var9];
                            var17 = var5 + ej.i;
                            boolean var32;
                            if (var17 < var14 && var17 < var15 && var17 < var16) {
                                var32 = false;
                            } else {
                                var17 = ej.i - var5;
                                if (var17 > var14 && var17 > var15 && var17 > var16) {
                                    var32 = false;
                                } else {
                                    var17 = var5 + ej.q;
                                    if (var17 < var10 && var17 < var11 && var17 < var12) {
                                        var32 = false;
                                    } else {
                                        var17 = ej.q - var5;
                                        if (var17 > var10 && var17 > var11 && var17 > var12) {
                                            var32 = false;
                                        } else {
                                            var32 = true;
                                        }
                                    }
                                }
                            }

                            if (var32) {
                                g.q(var4);
                                var2 = false;
                            }
                        }

                        if ((var10 - var11) * (ag[var9] - ag[var27]) - (var12 - var11) * (ag[var7] - ag[var27]) > 0) {
                            al[var26] = false;
                            if (var10 >= 0 && var11 >= 0 && var12 >= 0 && var10 <= eu.u && var11 <= eu.u
                                    && var12 <= eu.u) {
                                ai[var26] = false;
                            } else {
                                ai[var26] = true;
                            }

                            var13 = (as[var7] + as[var27] + as[var9]) / 3 + this.am;
                            ab[var13][ak[var13]++] = var26;
                        }
                    }
                }
            }

            int[] var8;
            if (this.j == null) {
                for (var26 = this.ae - 1; var26 >= 0; --var26) {
                    var7 = ak[var26];
                    if (var7 > 0) {
                        var8 = ab[var26];

                        for (var9 = 0; var9 < var7; ++var9) {
                            this.aj(var8[var9]);
                        }
                    }
                }

            } else {
                for (var26 = 0; var26 < 12; ++var26) {
                    ac[var26] = 0;
                    ba[var26] = 0;
                }

                for (var26 = this.ae - 1; var26 >= 0; --var26) {
                    var7 = ak[var26];
                    if (var7 > 0) {
                        var8 = ab[var26];

                        for (var9 = 0; var9 < var7; ++var9) {
                            var10 = var8[var9];
                            byte var31 = this.j[var10];
                            var12 = ac[var31]++;
                            ad[var31][var12] = var10;
                            if (var31 < 10) {
                                ba[var31] += var26;
                            } else if (var31 == 10) {
                                bg[var12] = var26;
                            } else {
                                br[var12] = var26;
                            }
                        }
                    }
                }

                var26 = 0;
                if (ac[1] > 0 || ac[2] > 0) {
                    var26 = (ba[1] + ba[2]) / (ac[1] + ac[2]);
                }

                var7 = 0;
                if (ac[3] > 0 || ac[4] > 0) {
                    var7 = (ba[3] + ba[4]) / (ac[3] + ac[4]);
                }

                var27 = 0;
                if (ac[6] > 0 || ac[8] > 0) {
                    var27 = (ba[8] + ba[6]) / (ac[8] + ac[6]);
                }

                var10 = 0;
                var11 = ac[10];
                int[] var28 = ad[10];
                int[] var29 = bg;
                if (var10 == var11) {
                    var10 = 0;
                    var11 = ac[11];
                    var28 = ad[11];
                    var29 = br;
                }

                if (var10 < var11) {
                    var9 = var29[var10];
                } else {
                    var9 = -1000;
                }

                for (var14 = 0; var14 < 10; ++var14) {
                    while (var14 == 0 && var9 > var26) {
                        this.aj(var28[var10++]);
                        if (var10 == var11 && var28 != ad[11]) {
                            var10 = 0;
                            var11 = ac[11];
                            var28 = ad[11];
                            var29 = br;
                        }

                        if (var10 < var11) {
                            var9 = var29[var10];
                        } else {
                            var9 = -1000;
                        }
                    }

                    while (var14 == 3 && var9 > var7) {
                        this.aj(var28[var10++]);
                        if (var10 == var11 && var28 != ad[11]) {
                            var10 = 0;
                            var11 = ac[11];
                            var28 = ad[11];
                            var29 = br;
                        }

                        if (var10 < var11) {
                            var9 = var29[var10];
                        } else {
                            var9 = -1000;
                        }
                    }

                    while (var14 == 5 && var9 > var27) {
                        this.aj(var28[var10++]);
                        if (var10 == var11 && var28 != ad[11]) {
                            var10 = 0;
                            var11 = ac[11];
                            var28 = ad[11];
                            var29 = br;
                        }

                        if (var10 < var11) {
                            var9 = var29[var10];
                        } else {
                            var9 = -1000;
                        }
                    }

                    var15 = ac[var14];
                    int[] var30 = ad[var14];

                    for (var17 = 0; var17 < var15; ++var17) {
                        this.aj(var30[var17]);
                    }
                }

                while (var9 != -1000) {
                    this.aj(var28[var10++]);
                    if (var10 == var11 && var28 != ad[11]) {
                        var10 = 0;
                        var28 = ad[11];
                        var11 = ac[11];
                        var29 = br;
                    }

                    if (var10 < var11) {
                        var9 = var29[var10];
                    } else {
                        var9 = -1000;
                    }
                }

            }
        }
    }

    @ObfuscatedName("aj")
    final void aj(int var1) {
        if (al[var1]) {
            this.ae(var1);
        } else {
            int var2 = this.aIndices[var1];
            int var3 = this.bIndices[var1];
            int var4 = this.cIndices[var1];
            eu.t = ai[var1];
            if (this.k == null) {
                eu.l = 0;
            } else {
                eu.l = this.k[var1] & 255;
            }

            if (this.w != null && this.w[var1] != -1) {
                int var5;
                int var6;
                int var7;
                if (this.z != null && this.z[var1] != -1) {
                    int var8 = this.z[var1] & 255;
                    var5 = this.f[var8];
                    var6 = this.r[var8];
                    var7 = this.y[var8];
                } else {
                    var5 = var2;
                    var6 = var3;
                    var7 = var4;
                }

                if (this.u[var1] == -1) {
                    eu.z(ag[var2], ag[var3], ag[var4], at[var2], at[var3], at[var4], this.c[var1], this.c[var1],
                            this.c[var1], aw[var5], aw[var6], aw[var7], aq[var5], aq[var6], aq[var7], aa[var5],
                            aa[var6], aa[var7], this.w[var1]);
                } else {
                    eu.z(ag[var2], ag[var3], ag[var4], at[var2], at[var3], at[var4], this.c[var1], this.v[var1],
                            this.u[var1], aw[var5], aw[var6], aw[var7], aq[var5], aq[var6], aq[var7], aa[var5],
                            aa[var6], aa[var7], this.w[var1]);
                }
            } else if (this.u[var1] == -1) {
                eu.u(ag[var2], ag[var3], ag[var4], at[var2], at[var3], at[var4], bq[this.c[var1]]);
            } else {
                eu.o(ag[var2], ag[var3], ag[var4], at[var2], at[var3], at[var4], this.c[var1], this.v[var1],
                        this.u[var1]);
            }

        }
    }

    @ObfuscatedName("ae")
    final void ae(int var1) {
        int var2 = eu.c;
        int var3 = eu.v;
        int var4 = 0;
        int var5 = this.aIndices[var1];
        int var6 = this.bIndices[var1];
        int var7 = this.cIndices[var1];
        int var8 = aa[var5];
        int var9 = aa[var6];
        int var10 = aa[var7];
        if (this.k == null) {
            eu.l = 0;
        } else {
            eu.l = this.k[var1] & 255;
        }

        int var11;
        int var12;
        int var13;
        int var14;
        if (var8 >= 50) {
            bk[var4] = at[var5];
            be[var4] = ag[var5];
            bc[var4++] = this.c[var1];
        } else {
            var11 = aw[var5];
            var12 = aq[var5];
            var13 = this.c[var1];
            if (var10 >= 50) {
                var14 = bz[var10 - var8] * (50 - var8);
                bk[var4] = var2 + eu.o * (var11 + ((aw[var7] - var11) * var14 >> 16)) / 50;
                be[var4] = var3 + eu.o * (var12 + ((aq[var7] - var12) * var14 >> 16)) / 50;
                bc[var4++] = var13 + ((this.u[var1] - var13) * var14 >> 16);
            }

            if (var9 >= 50) {
                var14 = bz[var9 - var8] * (50 - var8);
                bk[var4] = var2 + eu.o * (var11 + ((aw[var6] - var11) * var14 >> 16)) / 50;
                be[var4] = var3 + eu.o * (var12 + ((aq[var6] - var12) * var14 >> 16)) / 50;
                bc[var4++] = var13 + ((this.v[var1] - var13) * var14 >> 16);
            }
        }

        if (var9 >= 50) {
            bk[var4] = at[var6];
            be[var4] = ag[var6];
            bc[var4++] = this.v[var1];
        } else {
            var11 = aw[var6];
            var12 = aq[var6];
            var13 = this.v[var1];
            if (var8 >= 50) {
                var14 = bz[var8 - var9] * (50 - var9);
                bk[var4] = var2 + eu.o * (var11 + ((aw[var5] - var11) * var14 >> 16)) / 50;
                be[var4] = var3 + eu.o * (var12 + ((aq[var5] - var12) * var14 >> 16)) / 50;
                bc[var4++] = var13 + ((this.c[var1] - var13) * var14 >> 16);
            }

            if (var10 >= 50) {
                var14 = bz[var10 - var9] * (50 - var9);
                bk[var4] = var2 + eu.o * (var11 + ((aw[var7] - var11) * var14 >> 16)) / 50;
                be[var4] = var3 + eu.o * (var12 + ((aq[var7] - var12) * var14 >> 16)) / 50;
                bc[var4++] = var13 + ((this.u[var1] - var13) * var14 >> 16);
            }
        }

        if (var10 >= 50) {
            bk[var4] = at[var7];
            be[var4] = ag[var7];
            bc[var4++] = this.u[var1];
        } else {
            var11 = aw[var7];
            var12 = aq[var7];
            var13 = this.u[var1];
            if (var9 >= 50) {
                var14 = bz[var9 - var10] * (50 - var10);
                bk[var4] = var2 + eu.o * (var11 + ((aw[var6] - var11) * var14 >> 16)) / 50;
                be[var4] = var3 + eu.o * (var12 + ((aq[var6] - var12) * var14 >> 16)) / 50;
                bc[var4++] = var13 + ((this.v[var1] - var13) * var14 >> 16);
            }

            if (var8 >= 50) {
                var14 = bz[var8 - var10] * (50 - var10);
                bk[var4] = var2 + eu.o * (var11 + ((aw[var5] - var11) * var14 >> 16)) / 50;
                be[var4] = var3 + eu.o * (var12 + ((aq[var5] - var12) * var14 >> 16)) / 50;
                bc[var4++] = var13 + ((this.c[var1] - var13) * var14 >> 16);
            }
        }

        var11 = bk[0];
        var12 = bk[1];
        var13 = bk[2];
        var14 = be[0];
        int var15 = be[1];
        int var16 = be[2];
        eu.t = false;
        int var17;
        int var18;
        int var19;
        int var20;
        if (var4 == 3) {
            if (var11 < 0 || var12 < 0 || var13 < 0 || var11 > eu.u || var12 > eu.u || var13 > eu.u) {
                eu.t = true;
            }

            if (this.w != null && this.w[var1] != -1) {
                if (this.z != null && this.z[var1] != -1) {
                    var20 = this.z[var1] & 255;
                    var17 = this.f[var20];
                    var18 = this.r[var20];
                    var19 = this.y[var20];
                } else {
                    var17 = var5;
                    var18 = var6;
                    var19 = var7;
                }

                if (this.u[var1] == -1) {
                    eu.z(var14, var15, var16, var11, var12, var13, this.c[var1], this.c[var1], this.c[var1], aw[var17],
                            aw[var18], aw[var19], aq[var17], aq[var18], aq[var19], aa[var17], aa[var18], aa[var19],
                            this.w[var1]);
                } else {
                    eu.z(var14, var15, var16, var11, var12, var13, bc[0], bc[1], bc[2], aw[var17], aw[var18],
                            aw[var19], aq[var17], aq[var18], aq[var19], aa[var17], aa[var18], aa[var19], this.w[var1]);
                }
            } else if (this.u[var1] == -1) {
                eu.u(var14, var15, var16, var11, var12, var13, bq[this.c[var1]]);
            } else {
                eu.o(var14, var15, var16, var11, var12, var13, bc[0], bc[1], bc[2]);
            }
        }

        if (var4 == 4) {
            if (var11 < 0 || var12 < 0 || var13 < 0 || var11 > eu.u || var12 > eu.u || var13 > eu.u || bk[3] < 0
                    || bk[3] > eu.u) {
                eu.t = true;
            }

            if (this.w != null && this.w[var1] != -1) {
                if (this.z != null && this.z[var1] != -1) {
                    var20 = this.z[var1] & 255;
                    var17 = this.f[var20];
                    var18 = this.r[var20];
                    var19 = this.y[var20];
                } else {
                    var17 = var5;
                    var18 = var6;
                    var19 = var7;
                }

                short var21 = this.w[var1];
                if (this.u[var1] == -1) {
                    eu.z(var14, var15, var16, var11, var12, var13, this.c[var1], this.c[var1], this.c[var1], aw[var17],
                            aw[var18], aw[var19], aq[var17], aq[var18], aq[var19], aa[var17], aa[var18], aa[var19],
                            var21);
                    eu.z(var14, var16, be[3], var11, var13, bk[3], this.c[var1], this.c[var1], this.c[var1], aw[var17],
                            aw[var18], aw[var19], aq[var17], aq[var18], aq[var19], aa[var17], aa[var18], aa[var19],
                            var21);
                } else {
                    eu.z(var14, var15, var16, var11, var12, var13, bc[0], bc[1], bc[2], aw[var17], aw[var18],
                            aw[var19], aq[var17], aq[var18], aq[var19], aa[var17], aa[var18], aa[var19], var21);
                    eu.z(var14, var16, be[3], var11, var13, bk[3], bc[0], bc[2], bc[3], aw[var17], aw[var18],
                            aw[var19], aq[var17], aq[var18], aq[var19], aa[var17], aa[var18], aa[var19], var21);
                }
            } else if (this.u[var1] == -1) {
                var17 = bq[this.c[var1]];
                eu.u(var14, var15, var16, var11, var12, var13, var17);
                eu.u(var14, var16, be[3], var11, var13, bk[3], var17);
            } else {
                eu.o(var14, var15, var16, var11, var12, var13, bc[0], bc[1], bc[2]);
                eu.o(var14, var16, be[3], var11, var13, bk[3], bc[0], bc[2], bc[3]);
            }
        }

    }

    @ObfuscatedName("cd")
    void cd(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        ak[0] = -1;
        if (this.ao != 1) {
            this.b();
        }

        this.l(var1);
        int var10 = var5 * var8 - var4 * var6 >> 16;
        int var11 = var2 * var7 + var3 * var10 >> 16;
        int var12 = var3 * this.aj >> 16;
        int var13 = var11 + var12;
        if (var13 > 50 && var11 < 3500) {
            int var14 = var8 * var4 + var5 * var6 >> 16;
            int var15 = (var14 - this.aj) * eu.o;
            if (var15 / var13 < eu.z) {
                int var16 = (var14 + this.aj) * eu.o;
                if (var16 / var13 > eu.k) {
                    int var17 = var3 * var7 - var10 * var2 >> 16;
                    int var18 = var2 * this.aj >> 16;
                    int var19 = (var17 + var18) * eu.o;
                    if (var19 / var13 > eu.w) {
                        int var20 = (var3 * super.modelHeight >> 16) + var18;
                        int var21 = (var17 - var20) * eu.o;
                        if (var21 / var13 < eu.s) {
                            int var22 = var12 + (var2 * super.modelHeight >> 16);
                            boolean var23 = false;
                            boolean var24 = false;
                            if (var11 - var22 <= 50) {
                                var24 = true;
                            }

                            boolean var25 = var24 || this.vertexCount > 0;
                            int var26 = ej.q;
                            int var28 = ej.i;
                            boolean var30 = ej.t;
                            if (x.t && var9 > 0) {
                                fl.t(this, var6, var7, var8);
                            }

                            int var33;
                            int var34;
                            int var35;
                            int var36;
                            int var37;
                            int var38;
                            int var39;
                            if (x.q && var9 > 0) {
                                int var32 = var11 - var12;
                                if (var32 <= 50) {
                                    var32 = 50;
                                }

                                if (var14 > 0) {
                                    var33 = var15 / var13;
                                    var34 = var16 / var32;
                                } else {
                                    var34 = var16 / var13;
                                    var33 = var15 / var32;
                                }

                                if (var17 > 0) {
                                    var35 = var21 / var13;
                                    var36 = var19 / var32;
                                } else {
                                    var36 = var19 / var13;
                                    var35 = var21 / var32;
                                }

                                var37 = -8355840;
                                var38 = var26 - eu.c;
                                var39 = var28 - eu.v;
                                if (var38 > var33 && var38 < var34 && var39 > var35 && var39 < var36) {
                                    var37 = -256;
                                }

                                v.i(var33 + eu.c, var35 + eu.v, var34 + eu.c, var36 + eu.v, var37);
                            }

                            boolean var44 = false;
                            if (var9 > 0 && var30) {
                                boolean var45 = false;
                                if (bj) {
                                    var45 = bv.a(this, var6, var7, var8);
                                } else {
                                    var34 = var11 - var12;
                                    if (var34 <= 50) {
                                        var34 = 50;
                                    }

                                    if (var14 > 0) {
                                        var15 /= var13;
                                        var16 /= var34;
                                    } else {
                                        var16 /= var13;
                                        var15 /= var34;
                                    }

                                    if (var17 > 0) {
                                        var21 /= var13;
                                        var19 /= var34;
                                    } else {
                                        var19 /= var13;
                                        var21 /= var34;
                                    }

                                    var35 = var26 - eu.c;
                                    var36 = var28 - eu.v;
                                    if (var35 > var15 && var35 < var16 && var36 > var21 && var36 < var19) {
                                        var45 = true;
                                    }
                                }

                                if (var45) {
                                    if (this.ay) {
                                        g.q(var9);
                                    } else {
                                        var44 = true;
                                    }
                                }
                            }

                            var33 = eu.c;
                            var34 = eu.v;
                            var35 = 0;
                            var36 = 0;
                            if (var1 != 0) {
                                var35 = bn[var1];
                                var36 = bb[var1];
                            }

                            for (var37 = 0; var37 < this.indexCount; ++var37) {
                                var38 = this.xVertices[var37];
                                var39 = this.yVertices[var37];
                                int var40 = this.zVertices[var37];
                                int var41;
                                if (var1 != 0) {
                                    var41 = var40 * var35 + var38 * var36 >> 16;
                                    var40 = var40 * var36 - var38 * var35 >> 16;
                                    var38 = var41;
                                }

                                var38 += var6;
                                var39 += var7;
                                var40 += var8;
                                var41 = var40 * var4 + var5 * var38 >> 16;
                                var40 = var5 * var40 - var38 * var4 >> 16;
                                var38 = var41;
                                var41 = var3 * var39 - var40 * var2 >> 16;
                                var40 = var39 * var2 + var3 * var40 >> 16;
                                as[var37] = var40 - var11;
                                if (var40 >= 50) {
                                    at[var37] = var38 * eu.o / var40 + var33;
                                    ag[var37] = var41 * eu.o / var40 + var34;
                                } else {
                                    at[var37] = -5000;
                                    var23 = true;
                                }

                                if (var25) {
                                    aw[var37] = var38;
                                    aq[var37] = var41;
                                    aa[var37] = var40;
                                }
                            }

                            try {
                                this.av(var23, var44, this.ay, var9);
                            } catch (Exception var43) {
                                ;
                            }

                        }
                    }
                }
            }
        }
    }
}
