import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ab")
public class ab implements as {

    @ObfuscatedName("rj")
    static short[] rj;

    @ObfuscatedName("ef")
    static int ef;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("t")
    public void t(az var1) {
        if (var1.e > this.l) {
            var1.e = this.l;
        }

        if (var1.x < this.l) {
            var1.x = this.l;
        }

        if (var1.p > this.b) {
            var1.p = this.b;
        }

        if (var1.g < this.b) {
            var1.g = this.b;
        }

    }

    @ObfuscatedName("q")
    public boolean q(int var1, int var2, int var3) {
        if (var1 >= this.t && var1 < this.t + this.q) {
            return var2 >= (this.i << 6) + (this.e << 3) && var2 <= (this.i << 6) + (this.e << 3) + 7
                    && var3 >= (this.a << 6) + (this.x << 3) && var3 <= (this.a << 6) + (this.x << 3) + 7;
        } else {
            return false;
        }
    }

    @ObfuscatedName("i")
    public boolean i(int var1, int var2) {
        return var1 >= (this.l << 6) + (this.p << 3) && var1 <= (this.l << 6) + (this.p << 3) + 7
                && var2 >= (this.b << 6) + (this.g << 3) && var2 <= (this.b << 6) + (this.g << 3) + 7;
    }

    @ObfuscatedName("a")
    public int[] a(int var1, int var2, int var3) {
        if (!this.q(var1, var2, var3)) {
            return null;
        } else {
            int[] var4 = new int[] { this.l * 64 - this.i * 64 + var2 + (this.p * 8 - this.e * 8),
                    var3 + (this.b * 64 - this.a * 64) + (this.g * 8 - this.x * 8) };
            return var4;
        }
    }

    @ObfuscatedName("l")
    public ik l(int var1, int var2) {
        if (!this.i(var1, var2)) {
            return null;
        } else {
            int var3 = this.i * 64 - this.l * 64 + (this.e * 8 - this.p * 8) + var1;
            int var4 = this.a * 64 - this.b * 64 + var2 + (this.x * 8 - this.g * 8);
            return new ik(this.t, var3, var4);
        }
    }

    @ObfuscatedName("b")
    public void b(ByteBuffer var1) {
        this.t = var1.av();
        this.q = var1.av();
        this.i = var1.ae();
        this.e = var1.av();
        this.a = var1.ae();
        this.x = var1.av();
        this.l = var1.ae();
        this.p = var1.av();
        this.b = var1.ae();
        this.g = var1.av();
        this.e();
    }

    @ObfuscatedName("e")
    void e() {
    }

    @ObfuscatedName("x")
    static void x(km var0, km var1, km var2, boolean var3) {
        if (var3) {
            ci.worldCount = (BoundaryObject.r - 765) / 2;
            ci.z = ci.worldCount + 202;
            ik.w = ci.z + 180;
        }

        byte var4;
        int var5;
        int var7;
        int var8;
        int var11;
        int var12;
        int var29;
        int var31;
        int var34;
        int var35;
        int var38;
        int var40;
        if (ci.isWorldSelectorOpen) {
            if (jg.bj == null) {
                jg.bj = CombatBarDefinition.l(au.cp, "sl_back", "");
            }

            if (fr.bt == null) {
                fr.bt = az.i(au.cp, "sl_flags", "");
            }

            if (av.by == null) {
                av.by = az.i(au.cp, "sl_arrows", "");
            }

            if (ai.bn == null) {
                ai.bn = az.i(au.cp, "sl_stars", "");
            }

            li.dl(ci.worldCount, 23, 765, 480, 0);
            li.dn(ci.worldCount, 0, 125, 23, 12425273, 9135624);
            li.dn(ci.worldCount + 125, 0, 640, 23, 5197647, 2697513);
            var0.r("Select a world", ci.worldCount + 62, 15, 0, -1);
            if (ai.bn != null) {
                ai.bn[1].i(ci.worldCount + 140, 1);
                var1.s("Members only world", ci.worldCount + 152, 10, 16777215, -1);
                ai.bn[0].i(ci.worldCount + 140, 12);
                var1.s("Free world", ci.worldCount + 152, 21, 16777215, -1);
            }

            if (av.by != null) {
                var29 = ci.worldCount + 280;
                if (Server.p[0] == 0 && Server.x[0] == 0) {
                    av.by[2].i(var29, 4);
                } else {
                    av.by[0].i(var29, 4);
                }

                if (Server.p[0] == 0 && Server.x[0] == 1) {
                    av.by[3].i(var29 + 15, 4);
                } else {
                    av.by[1].i(var29 + 15, 4);
                }

                var0.s("World", var29 + 32, 17, 16777215, -1);
                var5 = ci.worldCount + 390;
                if (Server.p[0] == 1 && Server.x[0] == 0) {
                    av.by[2].i(var5, 4);
                } else {
                    av.by[0].i(var5, 4);
                }

                if (Server.p[0] == 1 && Server.x[0] == 1) {
                    av.by[3].i(var5 + 15, 4);
                } else {
                    av.by[1].i(var5 + 15, 4);
                }

                var0.s("Players", var5 + 32, 17, 16777215, -1);
                var31 = ci.worldCount + 500;
                if (Server.p[0] == 2 && Server.x[0] == 0) {
                    av.by[2].i(var31, 4);
                } else {
                    av.by[0].i(var31, 4);
                }

                if (Server.p[0] == 2 && Server.x[0] == 1) {
                    av.by[3].i(var31 + 15, 4);
                } else {
                    av.by[1].i(var31 + 15, 4);
                }

                var0.s("Location", var31 + 32, 17, 16777215, -1);
                var7 = ci.worldCount + 610;
                if (Server.p[0] == 3 && Server.x[0] == 0) {
                    av.by[2].i(var7, 4);
                } else {
                    av.by[0].i(var7, 4);
                }

                if (Server.p[0] == 3 && Server.x[0] == 1) {
                    av.by[3].i(var7 + 15, 4);
                } else {
                    av.by[1].i(var7 + 15, 4);
                }

                var0.s("Type", var7 + 32, 17, 16777215, -1);
            }

            li.dl(ci.worldCount + 708, 4, 50, 16, 0);
            var1.r("Cancel", ci.worldCount + 708 + 25, 16, 16777215, -1);
            ci.bq = -1;
            if (jg.bj != null) {
                var4 = 88;
                byte var39 = 19;
                var31 = 765 / (var4 + 1);
                var7 = 480 / (var39 + 1);

                do {
                    var8 = var7;
                    var34 = var31;
                    if (var7 * (var31 - 1) >= Server.b) {
                        --var31;
                    }

                    if (var31 * (var7 - 1) >= Server.b) {
                        --var7;
                    }

                    if (var31 * (var7 - 1) >= Server.b) {
                        --var7;
                    }
                } while (var8 != var7 || var34 != var31);

                var8 = (765 - var31 * var4) / (var31 + 1);
                if (var8 > 5) {
                    var8 = 5;
                }

                var34 = (480 - var7 * var39) / (var7 + 1);
                if (var34 > 5) {
                    var34 = 5;
                }

                var35 = (765 - var31 * var4 - var8 * (var31 - 1)) / 2;
                var11 = (480 - var39 * var7 - var34 * (var7 - 1)) / 2;
                var12 = var11 + 23;
                var38 = var35 + ci.worldCount;
                var40 = 0;
                boolean var41 = false;

                int var42;
                for (var42 = 0; var42 < Server.b; ++var42) {
                    Server var17 = Server.l[var42];
                    boolean var18 = true;
                    String var19 = Integer.toString(var17.population);
                    if (var17.population == -1) {
                        var19 = "OFF";
                        var18 = false;
                    } else if (var17.population > 1980) {
                        var19 = "FULL";
                        var18 = false;
                    }

                    int var21 = 0;
                    byte var20;
                    if (var17.k()) {
                        if (var17.x()) {
                            var20 = 7;
                        } else {
                            var20 = 6;
                        }
                    } else if (var17.u()) {
                        var21 = 16711680;
                        if (var17.x()) {
                            var20 = 5;
                        } else {
                            var20 = 4;
                        }
                    } else if (var17.o()) {
                        if (var17.x()) {
                            var20 = 3;
                        } else {
                            var20 = 2;
                        }
                    } else if (var17.x()) {
                        var20 = 1;
                    } else {
                        var20 = 0;
                    }

                    if (bs.n >= var38 && bs.x >= var12 && bs.n < var38 + var4 && bs.x < var39 + var12 && var18) {
                        ci.bq = var42;
                        jg.bj[var20].s(var38, var12, 128, 16777215);
                        var41 = true;
                    } else {
                        jg.bj[var20].o(var38, var12);
                    }

                    if (fr.bt != null) {
                        fr.bt[(var17.x() ? 8 : 0) + var17.location].i(var38 + 29, var12);
                    }

                    var0.r(Integer.toString(var17.number), var38 + 15, var39 / 2 + var12 + 5, var21, -1);
                    var1.r(var19, var38 + 60, var39 / 2 + var12 + 5, 268435455, -1);
                    var12 = var12 + var34 + var39;
                    ++var40;
                    if (var40 >= var7) {
                        var12 = var11 + 23;
                        var38 = var38 + var8 + var4;
                        var40 = 0;
                    }
                }

                if (var41) {
                    var42 = var1.c(Server.l[ci.bq].activity) + 6;
                    int var27 = var1.e + 8;
                    li.dl(bs.n - var42 / 2, bs.x + 20 + 5, var42, var27, 16777120);
                    li.dw(bs.n - var42 / 2, bs.x + 20 + 5, var42, var27, 0);
                    var1.r(Server.l[ci.bq].activity, bs.n, bs.x + var1.e + 20 + 5 + 4, 0, -1);
                }
            }

            ad.interfaceProducer.q(0, 0);
        } else {
            if (var3) {
                ItemNode.b.o(ci.worldCount, 0);
                iv.e.o(ci.worldCount + 382, 0);
                ii.x.i(ci.worldCount + 382 - ii.x.i / 2, 18);
            }

            if (Client.packet == 0 || Client.packet == 5) {
                var4 = 20;
                var0.r("RuneScape is loading - please wait...", ci.z + 180, 245 - var4, 16777215, -1);
                var5 = 253 - var4;
                li.dw(ci.z + 180 - 152, var5, 304, 34, 9179409);
                li.dw(ci.z + 180 - 151, var5 + 1, 302, 32, 0);
                li.dl(ci.z + 180 - 150, var5 + 2, ci.au * 3, 30, 9179409);
                li.dl(ci.au * 3 + (ci.z + 180 - 150), var5 + 2, 300 - ci.au * 3, 30, 0);
                var0.r(ci.ax, ci.z + 180, 276 - var4, 16777215, -1);
            }

            String var22;
            short var28;
            short var30;
            if (Client.packet == 20) {
                ci.i.i(ci.z + 180 - ci.i.i / 2, 271 - ci.i.a / 2);
                var28 = 201;
                var0.r(ci.loginLine1, ci.z + 180, var28, 16776960, 0);
                var29 = var28 + 15;
                var0.r(ci.loginLine2, ci.z + 180, var29, 16776960, 0);
                var29 += 15;
                var0.r(ci.loginLine3, ci.z + 180, var29, 16776960, 0);
                var29 += 15;
                var29 += 7;
                if (ci.loginState != 4) {
                    var0.s("Login: ", ci.z + 180 - 110, var29, 16777215, 0);
                    var30 = 200;

                    for (var22 = ia.i(); var0.c(var22) > var30; var22 = var22.substring(0, var22.length() - 1)) {
                        ;
                    }

                    var0.s(RSFont.w(var22), ci.z + 180 - 70, var29, 16777215, 0);
                    var29 += 15;
                    var0.s("Password: " + kn.k(ci.password), ci.z + 180 - 108, var29, 16777215, 0);
                    var29 += 15;
                }
            }

            if (Client.packet == 10 || Client.packet == 11) {
                ci.i.i(ci.z, 171);
                short var6;
                if (ci.loginState == 0) {
                    var28 = 251;
                    var0.r("Welcome to RuneScape", ci.z + 180, var28, 16776960, 0);
                    var29 = var28 + 30;
                    var5 = ci.z + 180 - 80;
                    var6 = 291;
                    ib.a.i(var5 - 73, var6 - 20);
                    var0.y("New User", var5 - 73, var6 - 20, 144, 40, 16777215, 0, 1, 1, 0);
                    var5 = ci.z + 180 + 80;
                    ib.a.i(var5 - 73, var6 - 20);
                    var0.y("Existing User", var5 - 73, var6 - 20, 144, 40, 16777215, 0, 1, 1, 0);
                } else if (ci.loginState == 1) {
                    var0.r(ci.an, ci.z + 180, 201, 16776960, 0);
                    var28 = 236;
                    var0.r(ci.loginLine1, ci.z + 180, var28, 16777215, 0);
                    var29 = var28 + 15;
                    var0.r(ci.loginLine2, ci.z + 180, var29, 16777215, 0);
                    var29 += 15;
                    var0.r(ci.loginLine3, ci.z + 180, var29, 16777215, 0);
                    var29 += 15;
                    var5 = ci.z + 180 - 80;
                    var6 = 321;
                    ib.a.i(var5 - 73, var6 - 20);
                    var0.r("Continue", var5, var6 + 5, 16777215, 0);
                    var5 = ci.z + 180 + 80;
                    ib.a.i(var5 - 73, var6 - 20);
                    var0.r("Cancel", var5, var6 + 5, 16777215, 0);
                } else if (ci.loginState == 2) {
                    var28 = 201;
                    var0.r(ci.loginLine1, ik.w, var28, 16776960, 0);
                    var29 = var28 + 15;
                    var0.r(ci.loginLine2, ik.w, var29, 16776960, 0);
                    var29 += 15;
                    var0.r(ci.loginLine3, ik.w, var29, 16776960, 0);
                    var29 += 15;
                    var29 += 7;
                    var0.s("Login: ", ik.w - 110, var29, 16777215, 0);
                    var30 = 200;

                    for (var22 = ia.i(); var0.c(var22) > var30; var22 = var22.substring(1)) {
                        ;
                    }

                    var0.s(RSFont.w(var22) + (ci.be == 0 & Client.bz % 40 < 20 ? ar.q(16776960) + "|" : ""), ik.w - 70,
                            var29, 16777215, 0);
                    var29 += 15;
                    var0.s("Password: " + kn.k(ci.password)
                            + (ci.be == 1 & Client.bz % 40 < 20 ? ar.q(16776960) + "|" : ""), ik.w - 108, var29,
                            16777215, 0);
                    var29 += 15;
                    var28 = 277;
                    var7 = ik.w + -117;
                    boolean var9 = ci.aw;
                    boolean var10 = ci.ad;
                    lk var23 = var9 ? (var10 ? ci.c : ci.o) : (var10 ? fk.n : ez.g);
                    var23.i(var7, var28);
                    var7 = var7 + var23.i + 5;
                    var1.s("Remember username", var7, var28 + 13, 16776960, 0);
                    var7 = ik.w + 24;
                    boolean var13 = al.qp.e;
                    boolean var14 = ci.bg;
                    lk var25 = var13 ? (var14 ? ci.c : ci.o) : (var14 ? fk.n : ez.g);
                    var25.i(var7, var28);
                    var7 = var7 + var25.i + 5;
                    var1.s("Hide username", var7, var28 + 13, 16776960, 0);
                    var29 = var28 + 15;
                    int var15 = ik.w - 80;
                    short var16 = 321;
                    ib.a.i(var15 - 73, var16 - 20);
                    var0.r("Login", var15, var16 + 5, 16777215, 0);
                    var15 = ik.w + 80;
                    ib.a.i(var15 - 73, var16 - 20);
                    var0.r("Cancel", var15, var16 + 5, 16777215, 0);
                    var28 = 357;
                    var1.r("Forgotten your password? <col=ffffff>Click here.", ik.w, var28, 16776960, 0);
                } else if (ci.loginState == 3) {
                    var28 = 201;
                    var0.r("Invalid username or password.", ci.z + 180, var28, 16776960, 0);
                    var29 = var28 + 20;
                    var1.r("For accounts created after 24th November 2010, please use your", ci.z + 180, var29,
                            16776960, 0);
                    var29 += 15;
                    var1.r("email address to login. Otherwise please login with your username.", ci.z + 180, var29,
                            16776960, 0);
                    var29 += 15;
                    var5 = ci.z + 180;
                    var6 = 276;
                    ib.a.i(var5 - 73, var6 - 20);
                    var2.r("Try again", var5, var6 + 5, 16777215, 0);
                    var5 = ci.z + 180;
                    var6 = 326;
                    ib.a.i(var5 - 73, var6 - 20);
                    var2.r("Forgotten password?", var5, var6 + 5, 16777215, 0);
                } else if (ci.loginState == 4) {
                    var0.r("Authenticator", ci.z + 180, 201, 16776960, 0);
                    var28 = 236;
                    var0.r(ci.loginLine1, ci.z + 180, var28, 16777215, 0);
                    var29 = var28 + 15;
                    var0.r(ci.loginLine2, ci.z + 180, var29, 16777215, 0);
                    var29 += 15;
                    var0.r(ci.loginLine3, ci.z + 180, var29, 16777215, 0);
                    var29 += 15;
                    var0.s("PIN: " + kn.k(ik.ba) + (Client.bz % 40 < 20 ? ar.q(16776960) + "|" : ""), ci.z + 180 - 108,
                            var29, 16777215, 0);
                    var29 -= 8;
                    var0.s("Trust this computer", ci.z + 180 - 9, var29, 16776960, 0);
                    var29 += 15;
                    var0.s("for 30 days: ", ci.z + 180 - 9, var29, 16776960, 0);
                    var5 = 180 + ci.z - 9 + var0.c("for 30 days: ") + 15;
                    var31 = var29 - var0.e;
                    lk var26;
                    if (ci.bk) {
                        var26 = ci.o;
                    } else {
                        var26 = ez.g;
                    }

                    var26.i(var5, var31);
                    var29 += 15;
                    var8 = ci.z + 180 - 80;
                    short var33 = 321;
                    ib.a.i(var8 - 73, var33 - 20);
                    var0.r("Continue", var8, var33 + 5, 16777215, 0);
                    var8 = ci.z + 180 + 80;
                    ib.a.i(var8 - 73, var33 - 20);
                    var0.r("Cancel", var8, var33 + 5, 16777215, 0);
                    var1.r("<u=ff>Can't Log In?</u>", ci.z + 180, var33 + 36, 255, 0);
                } else if (ci.loginState == 5) {
                    var0.r("Forgotten your password?", ci.z + 180, 201, 16776960, 0);
                    var28 = 221;
                    var2.r(ci.loginLine1, ci.z + 180, var28, 16776960, 0);
                    var29 = var28 + 15;
                    var2.r(ci.loginLine2, ci.z + 180, var29, 16776960, 0);
                    var29 += 15;
                    var2.r(ci.loginLine3, ci.z + 180, var29, 16776960, 0);
                    var29 += 15;
                    var29 += 14;
                    var0.s("Username/email: ", ci.z + 180 - 145, var29, 16777215, 0);
                    var30 = 174;

                    for (var22 = ia.i(); var0.c(var22) > var30; var22 = var22.substring(1)) {
                        ;
                    }

                    var0.s(RSFont.w(var22) + (Client.bz % 40 < 20 ? ar.q(16776960) + "|" : ""), ci.z + 180 - 34, var29,
                            16777215, 0);
                    var29 += 15;
                    var7 = ci.z + 180 - 80;
                    short var32 = 321;
                    ib.a.i(var7 - 73, var32 - 20);
                    var0.r("Recover", var7, var32 + 5, 16777215, 0);
                    var7 = ci.z + 180 + 80;
                    ib.a.i(var7 - 73, var32 - 20);
                    var0.r("Back", var7, var32 + 5, 16777215, 0);
                } else if (ci.loginState == 6) {
                    var28 = 201;
                    var0.r(ci.loginLine1, ci.z + 180, var28, 16776960, 0);
                    var29 = var28 + 15;
                    var0.r(ci.loginLine2, ci.z + 180, var29, 16776960, 0);
                    var29 += 15;
                    var0.r(ci.loginLine3, ci.z + 180, var29, 16776960, 0);
                    var29 += 15;
                    var5 = ci.z + 180;
                    var6 = 321;
                    ib.a.i(var5 - 73, var6 - 20);
                    var0.r("Back", var5, var6 + 5, 16777215, 0);
                }
            }

            if (ci.ap > 0) {
                bs.c(ci.ap);
                ci.ap = 0;
            }

            var28 = 256;
            if (ci.m > 0) {
                for (var5 = 0; var5 < 256; ++var5) {
                    if (ci.m > 768) {
                        ItemStorage.f[var5] = GameEngine.u(ah.r[var5], ho.y[var5], 1024 - ci.m);
                    } else if (ci.m > 256) {
                        ItemStorage.f[var5] = ho.y[var5];
                    } else {
                        ItemStorage.f[var5] = GameEngine.u(ho.y[var5], ah.r[var5], 256 - ci.m);
                    }
                }
            } else if (ci.ay > 0) {
                for (var5 = 0; var5 < 256; ++var5) {
                    if (ci.ay > 768) {
                        ItemStorage.f[var5] = GameEngine.u(ah.r[var5], fz.h[var5], 1024 - ci.ay);
                    } else if (ci.ay > 256) {
                        ItemStorage.f[var5] = fz.h[var5];
                    } else {
                        ItemStorage.f[var5] = GameEngine.u(fz.h[var5], ah.r[var5], 256 - ci.ay);
                    }
                }
            } else {
                for (var5 = 0; var5 < 256; ++var5) {
                    ItemStorage.f[var5] = ah.r[var5];
                }
            }

            li.ck(ci.worldCount, 9, ci.worldCount + 128, var28 + 7);
            ItemNode.b.o(ci.worldCount, 0);
            li.cp();
            var5 = 0;
            var31 = ad.interfaceProducer.width * 9 + ci.worldCount;

            for (var7 = 1; var7 < var28 - 1; ++var7) {
                var8 = ci.d[var7] * (var28 - var7) / var28;
                var34 = var8 + 22;
                if (var34 < 0) {
                    var34 = 0;
                }

                var5 += var34;

                for (var35 = var34; var35 < 128; ++var35) {
                    var11 = hd.aj[var5++];
                    if (var11 != 0) {
                        var12 = var11;
                        var38 = 256 - var11;
                        var11 = ItemStorage.f[var11];
                        var40 = ad.interfaceProducer.pixels[var31];
                        ad.interfaceProducer.pixels[var31++] = ((var40 & 16711935) * var38 + (var11 & 16711935) * var12 & -16711936)
                                + (var38 * (var40 & '\uff00') + var12 * (var11 & '\uff00') & 16711680) >> 8;
                    } else {
                        ++var31;
                    }
                }

                var31 += var34 + ad.interfaceProducer.width - 128;
            }

            li.ck(ci.worldCount + 765 - 128, 9, ci.worldCount + 765, var28 + 7);
            iv.e.o(ci.worldCount + 382, 0);
            li.cp();
            var5 = 0;
            var31 = ad.interfaceProducer.width * 9 + ci.worldCount + 637 + 24;

            for (var7 = 1; var7 < var28 - 1; ++var7) {
                var8 = ci.d[var7] * (var28 - var7) / var28;
                var34 = 103 - var8;
                var31 += var8;

                for (var35 = 0; var35 < var34; ++var35) {
                    var11 = hd.aj[var5++];
                    if (var11 != 0) {
                        var12 = var11;
                        var38 = 256 - var11;
                        var11 = ItemStorage.f[var11];
                        var40 = ad.interfaceProducer.pixels[var31];
                        ad.interfaceProducer.pixels[var31++] = ((var40 & 16711935) * var38 + (var11 & 16711935) * var12 & -16711936)
                                + (var38 * (var40 & '\uff00') + var12 * (var11 & '\uff00') & 16711680) >> 8;
                    } else {
                        ++var31;
                    }
                }

                var5 += 128 - var34;
                var31 += ad.interfaceProducer.width - var34 - var8;
            }

            ci.p[al.qp.loadingAudioDisabled ? 1 : 0].i(ci.worldCount + 765 - 40, 463);
            if (Client.packet > 5 && Client.bs == 0) {
                if (fg.bb != null) {
                    var29 = ci.worldCount + 5;
                    var30 = 463;
                    byte var37 = 100;
                    byte var36 = 35;
                    fg.bb.i(var29, var30);
                    var0.r("World" + " " + Client.currentWorld, var37 / 2 + var29, var36 / 2 + var30 - 2, 16777215, 0);
                    if (Server.g != null) {
                        var1.r("Loading...", var37 / 2 + var29, var36 / 2 + var30 + 12, 16777215, 0);
                    } else {
                        var1.r("Click to switch", var37 / 2 + var29, var36 / 2 + var30 + 12, 16777215, 0);
                    }
                } else {
                    fg.bb = Renderable.a(au.cp, "sl_button", "");
                }
            }

        }
    }

    @ObfuscatedName("r")
    static int r(int var0, RuneScript var1, boolean var2) {
        if (var0 == 5306) {
            cs.e[++b.menuX - 1] = bg.fq();
            return 1;
        } else {
            int var3;
            if (var0 == 5307) {
                var3 = cs.e[--b.menuX];
                if (var3 == 1 || var3 == 2) {
                    al.fx(var3);
                }

                return 1;
            } else if (var0 == 5308) {
                cs.e[++b.menuX - 1] = al.qp.l;
                return 1;
            } else if (var0 != 5309) {
                return 2;
            } else {
                var3 = cs.e[--b.menuX];
                if (var3 == 1 || var3 == 2) {
                    al.qp.l = var3;
                    ar.i();
                }

                return 1;
            }
        }
    }

    @ObfuscatedName("ko")
    static void ko() {
        if (cn.ry != null) {
            Client.rh = Client.bz;
            cn.ry.a();

            for (int var0 = 0; var0 < Client.loadedPlayers.length; ++var0) {
                if (Client.loadedPlayers[var0] != null) {
                    cn.ry.i((Client.loadedPlayers[var0].regionX >> 7) + an.regionBaseX,
                            (Client.loadedPlayers[var0].regionY >> 7) + PlayerComposite.ep);
                }
            }
        }

    }
}
