import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bw")
public abstract class Character extends Renderable {

    @ObfuscatedName("aj")
    int regionX;

    @ObfuscatedName("ae")
    int regionY;

    @ObfuscatedName("am")
    int orientation;

    @ObfuscatedName("az")
    boolean animatingHeldItems = false;

    @ObfuscatedName("ap")
    int ap = 1;

    @ObfuscatedName("ah")
    int ah;

    @ObfuscatedName("au")
    int au = -1;

    @ObfuscatedName("ax")
    int ax = -1;

    @ObfuscatedName("ar")
    int ar = -1;

    @ObfuscatedName("an")
    int stanceId = -1;

    @ObfuscatedName("ai")
    int ai = -1;

    @ObfuscatedName("al")
    int al = -1;

    @ObfuscatedName("at")
    int at = -1;

    @ObfuscatedName("ag")
    int ag = -1;

    @ObfuscatedName("as")
    String textSpoken = null;

    @ObfuscatedName("aw")
    boolean aw;

    @ObfuscatedName("aq")
    boolean aq = false;

    @ObfuscatedName("aa")
    int aa = 100;

    @ObfuscatedName("af")
    int af = 0;

    @ObfuscatedName("ak")
    int ak = 0;

    @ObfuscatedName("ac")
    byte ac = 0;

    @ObfuscatedName("ad")
    int[] hitsplatTypes = new int[4];

    @ObfuscatedName("bg")
    int[] hitsplatDamages = new int[4];

    @ObfuscatedName("br")
    int[] hitsplatCycles = new int[4];

    @ObfuscatedName("ba")
    int[] ba = new int[4];

    @ObfuscatedName("bk")
    int[] bk = new int[4];

    @ObfuscatedName("be")
    SinglyLinkedList combatBarList = new SinglyLinkedList();

    @ObfuscatedName("bc")
    int interactingEntityIndex = -1;

    @ObfuscatedName("bm")
    boolean bm = false;

    @ObfuscatedName("bh")
    int bh = -1;

    @ObfuscatedName("bs")
    int bs = -1;

    @ObfuscatedName("bj")
    int stanceFrameIndex = 0;

    @ObfuscatedName("bt")
    int bt = 0;

    @ObfuscatedName("by")
    int animationId = -1;

    @ObfuscatedName("bn")
    int animationFrameIndex = 0;

    @ObfuscatedName("bb")
    int bb = 0;

    @ObfuscatedName("bq")
    int bq = 0;

    @ObfuscatedName("bz")
    int bz = 0;

    @ObfuscatedName("bx")
    int bx = -1;

    @ObfuscatedName("bf")
    int graphicId = 0;

    @ObfuscatedName("bo")
    int bo = 0;

    @ObfuscatedName("bv")
    int bv;

    @ObfuscatedName("bi")
    int bi;

    @ObfuscatedName("bu")
    int bu;

    @ObfuscatedName("bl")
    int bl;

    @ObfuscatedName("bw")
    int bw;

    @ObfuscatedName("bp")
    int bp;

    @ObfuscatedName("bd")
    int bd;

    @ObfuscatedName("cb")
    int cb;

    @ObfuscatedName("cm")
    int cm;

    @ObfuscatedName("cu")
    int cu = 0;

    @ObfuscatedName("cs")
    int cs = 200;

    @ObfuscatedName("ct")
    int ct;

    @ObfuscatedName("cw")
    int cw = 0;

    @ObfuscatedName("ch")
    int ch = 32;

    @ObfuscatedName("cr")
    int speed = 0;

    @ObfuscatedName("co")
    int[] co = new int[10];

    @ObfuscatedName("cv")
    int[] cv = new int[10];

    @ObfuscatedName("cd")
    byte[] cd = new byte[10];

    @ObfuscatedName("cq")
    int cq = 0;

    @ObfuscatedName("ci")
    int ci = 0;

    @ObfuscatedName("k")
    boolean k() {
        return false;
    }

    @ObfuscatedName("ac")
    final void ac() {
        this.speed = 0;
        this.ci = 0;
    }

    @ObfuscatedName("ad")
    final void ad(int var1, int var2, int var3, int var4, int var5, int var6) {
        boolean var7 = true;
        boolean var8 = true;

        int var9;
        for (var9 = 0; var9 < 4; ++var9) {
            if (this.hitsplatCycles[var9] > var5) {
                var7 = false;
            } else {
                var8 = false;
            }
        }

        var9 = -1;
        int var10 = -1;
        int var11 = 0;
        if (var1 >= 0) {
            jh var12 = bl.t(var1);
            var10 = var12.r;
            var11 = var12.v;
        }

        int var14;
        if (var8) {
            if (var10 == -1) {
                return;
            }

            var9 = 0;
            var14 = 0;
            if (var10 == 0) {
                var14 = this.hitsplatCycles[0];
            } else if (var10 == 1) {
                var14 = this.hitsplatDamages[0];
            }

            for (int var13 = 1; var13 < 4; ++var13) {
                if (var10 == 0) {
                    if (this.hitsplatCycles[var13] < var14) {
                        var9 = var13;
                        var14 = this.hitsplatCycles[var13];
                    }
                } else if (var10 == 1 && this.hitsplatDamages[var13] < var14) {
                    var9 = var13;
                    var14 = this.hitsplatDamages[var13];
                }
            }

            if (var10 == 1 && var14 >= var2) {
                return;
            }
        } else {
            if (var7) {
                this.ac = 0;
            }

            for (var14 = 0; var14 < 4; ++var14) {
                byte var15 = this.ac;
                this.ac = (byte) ((this.ac + 1) % 4);
                if (this.hitsplatCycles[var15] <= var5) {
                    var9 = var15;
                    break;
                }
            }
        }

        if (var9 >= 0) {
            this.hitsplatTypes[var9] = var1;
            this.hitsplatDamages[var9] = var2;
            this.ba[var9] = var3;
            this.bk[var9] = var4;
            this.hitsplatCycles[var9] = var5 + var11 + var6;
        }
    }

    @ObfuscatedName("bg")
    final void bg(int var1, int var2, int var3, int var4, int var5, int var6) {
        CombatBarDefinition var8 = (CombatBarDefinition) CombatBarDefinition.i.t((long) var1);
        CombatBarDefinition var7;
        if (var8 != null) {
            var7 = var8;
        } else {
            byte[] var9 = CombatBarDefinition.t.i(33, var1);
            var8 = new CombatBarDefinition();
            if (var9 != null) {
                var8.q(new ByteBuffer(var9));
            }

            CombatBarDefinition.i.i(var8, (long) var1);
            var7 = var8;
        }

        var8 = var7;
        CombatBar var14 = null;
        CombatBar var10 = null;
        int var11 = var7.p;
        int var12 = 0;

        CombatBar var13;
        for (var13 = (CombatBar) this.combatBarList.b(); var13 != null; var13 = (CombatBar) this.combatBarList.x()) {
            ++var12;
            if (var13.definition.l == var8.l) {
                var13.t(var2 + var4, var5, var6, var3);
                return;
            }

            if (var13.definition.x <= var8.x) {
                var14 = var13;
            }

            if (var13.definition.p > var11) {
                var10 = var13;
                var11 = var13.definition.p;
            }
        }

        if (var10 != null || var12 < 4) {
            var13 = new CombatBar(var8);
            if (var14 == null) {
                this.combatBarList.i(var13);
            } else {
                SinglyLinkedList.a(var13, var14);
            }

            var13.t(var2 + var4, var5, var6, var3);
            if (var12 >= 4) {
                var10.kc();
            }

        }
    }

    @ObfuscatedName("br")
    final void br(int var1) {
        CombatBarDefinition var3 = (CombatBarDefinition) CombatBarDefinition.i.t((long) var1);
        CombatBarDefinition var2;
        if (var3 != null) {
            var2 = var3;
        } else {
            byte[] var4 = CombatBarDefinition.t.i(33, var1);
            var3 = new CombatBarDefinition();
            if (var4 != null) {
                var3.q(new ByteBuffer(var4));
            }

            CombatBarDefinition.i.i(var3, (long) var1);
            var2 = var3;
        }

        var3 = var2;

        for (CombatBar var5 = (CombatBar) this.combatBarList.b(); var5 != null; var5 = (CombatBar) this.combatBarList
                .x()) {
            if (var3 == var5.definition) {
                var5.kc();
                return;
            }
        }

    }

    @ObfuscatedName("q")
    static final int q(long var0, String var2) {
        Random var3 = new Random();
        ByteBuffer var4 = new ByteBuffer(128);
        ByteBuffer var5 = new ByteBuffer(128);
        int[] var6 = new int[] { var3.nextInt(), var3.nextInt(), (int) (var0 >> 32), (int) var0 };
        var4.a(10);

        int var7;
        for (var7 = 0; var7 < 4; ++var7) {
            var4.e(var3.nextInt());
        }

        var4.e(var6[0]);
        var4.e(var6[1]);
        var4.p(var0);
        var4.p(0L);

        for (var7 = 0; var7 < 4; ++var7) {
            var4.e(var3.nextInt());
        }

        var4.ac(cr.l, cr.b);
        var5.a(10);

        for (var7 = 0; var7 < 3; ++var7) {
            var5.e(var3.nextInt());
        }

        var5.p(var3.nextLong());
        var5.x(var3.nextLong());
        if (Client.de != null) {
            var5.s(Client.de, 0, Client.de.length);
        } else {
            byte[] var8 = new byte[24];

            try {
                fh.o.q(0L);
                fh.o.a(var8);

                int var9;
                for (var9 = 0; var9 < 24 && var8[var9] == 0; ++var9) {
                    ;
                }

                if (var9 >= 24) {
                    throw new IOException();
                }
            } catch (Exception var24) {
                for (int var10 = 0; var10 < 24; ++var10) {
                    var8[var10] = -1;
                }
            }

            var5.s(var8, 0, var8.length);
        }

        var5.p(var3.nextLong());
        var5.ac(cr.l, cr.b);
        var7 = z.c(var2);
        if (var7 % 8 != 0) {
            var7 += 8 - var7 % 8;
        }

        ByteBuffer var25 = new ByteBuffer(var7);
        var25.u(var2);
        var25.index = var7;
        var25.aa(var6);
        ByteBuffer var18 = new ByteBuffer(var4.index + var5.index + var25.index + 5);
        var18.a(2);
        var18.a(var4.index);
        var18.s(var4.buffer, 0, var4.index);
        var18.a(var5.index);
        var18.s(var5.buffer, 0, var5.index);
        var18.l(var25.index);
        var18.s(var25.buffer, 0, var25.index);
        String var20 = Client.t(var18.buffer);

        try {
            URL var11 = new URL(ki.jv("services", false) + "m=accountappeal/login.ws");
            URLConnection var12 = var11.openConnection();
            var12.setDoInput(true);
            var12.setDoOutput(true);
            var12.setConnectTimeout(5000);
            OutputStreamWriter var13 = new OutputStreamWriter(var12.getOutputStream());
            var13.write("data2=" + lh.t(var20) + "&dest=" + lh.t("passwordchoice.ws"));
            var13.flush();
            InputStream var14 = var12.getInputStream();
            var18 = new ByteBuffer(new byte[1000]);

            do {
                int var15 = var14.read(var18.buffer, var18.index, 1000 - var18.index);
                if (var15 == -1) {
                    var13.close();
                    var14.close();
                    String var21 = new String(var18.buffer);
                    if (var21.startsWith("OFFLINE")) {
                        return 4;
                    } else if (var21.startsWith("WRONG")) {
                        return 7;
                    } else if (var21.startsWith("RELOAD")) {
                        return 3;
                    } else if (var21.startsWith("Not permitted for social network accounts.")) {
                        return 6;
                    } else {
                        var18.af(var6);

                        while (var18.index > 0 && var18.buffer[var18.index - 1] == 0) {
                            --var18.index;
                        }

                        var21 = new String(var18.buffer, 0, var18.index);
                        boolean var16;
                        if (var21 == null) {
                            var16 = false;
                        } else {
                            label106: {
                                try {
                                    new URL(var21);
                                } catch (MalformedURLException var22) {
                                    var16 = false;
                                    break label106;
                                }

                                var16 = true;
                            }
                        }

                        if (var16) {
                            bs.q(var21, true, false);
                            return 2;
                        } else {
                            return 5;
                        }
                    }
                }

                var18.index += var15;
            } while (var18.index < 1000);

            return 5;
        } catch (Throwable var23) {
            var23.printStackTrace();
            return 5;
        }
    }

    @ObfuscatedName("p")
    static int p(int var0) {
        ChatboxMessage var1 = (ChatboxMessage) cg.q.t((long) var0);
        if (var1 == null) {
            return -1;
        } else {
            return var1.cj == cg.i.t ? -1 : ((ChatboxMessage) var1.cj).index;
        }
    }
}
