import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gq")
public final class gq {

    @ObfuscatedName("t")
    final int t = 4096;

    @ObfuscatedName("q")
    final int q = 16;

    @ObfuscatedName("i")
    final int i = 258;

    @ObfuscatedName("a")
    final int a = 6;

    @ObfuscatedName("l")
    final int l = 50;

    @ObfuscatedName("b")
    final int b = 18002;

    @ObfuscatedName("e")
    byte[] e;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    byte[] g;

    @ObfuscatedName("n")
    int n = 0;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    byte v;

    @ObfuscatedName("u")
    int u;

    @ObfuscatedName("j")
    int j;

    @ObfuscatedName("k")
    int k;

    @ObfuscatedName("z")
    int z;

    @ObfuscatedName("w")
    int w;

    @ObfuscatedName("s")
    int s;

    @ObfuscatedName("d")
    int d;

    @ObfuscatedName("f")
    int[] f = new int[256];

    @ObfuscatedName("r")
    int r;

    @ObfuscatedName("y")
    int[] y = new int[257];

    @ObfuscatedName("m")
    int m;

    @ObfuscatedName("ay")
    boolean[] ay = new boolean[256];

    @ObfuscatedName("ao")
    boolean[] ao = new boolean[16];

    @ObfuscatedName("av")
    byte[] av = new byte[256];

    @ObfuscatedName("aj")
    byte[] aj = new byte[4096];

    @ObfuscatedName("ae")
    int[] ae = new int[16];

    @ObfuscatedName("am")
    byte[] am = new byte[18002];

    @ObfuscatedName("az")
    byte[] az = new byte[18002];

    @ObfuscatedName("ap")
    byte[][] ap = new byte[6][258];

    @ObfuscatedName("ah")
    int[][] ah = new int[6][258];

    @ObfuscatedName("au")
    int[][] au = new int[6][258];

    @ObfuscatedName("ax")
    int[][] ax = new int[6][258];

    @ObfuscatedName("ar")
    int[] ar = new int[6];

    @ObfuscatedName("an")
    int an;

    @ObfuscatedName("t")
    static void t() {
        if (TaskHandler.t.toLowerCase().indexOf("microsoft") != -1) {
            ad.ca[186] = 57;
            ad.ca[187] = 27;
            ad.ca[188] = 71;
            ad.ca[189] = 26;
            ad.ca[190] = 72;
            ad.ca[191] = 73;
            ad.ca[192] = 58;
            ad.ca[219] = 42;
            ad.ca[220] = 74;
            ad.ca[221] = 43;
            ad.ca[222] = 59;
            ad.ca[223] = 28;
        } else {
            ad.ca[44] = 71;
            ad.ca[45] = 26;
            ad.ca[46] = 72;
            ad.ca[47] = 73;
            ad.ca[59] = 57;
            ad.ca[61] = 27;
            ad.ca[91] = 42;
            ad.ca[92] = 74;
            ad.ca[93] = 43;
            ad.ca[192] = 28;
            ad.ca[222] = 58;
            ad.ca[520] = 59;
        }

    }

    @ObfuscatedName("au")
    static final byte[] au(byte[] var0) {
        ByteBuffer var1 = new ByteBuffer(var0);
        int var2 = var1.av();
        int var3 = var1.ap();
        if (var3 < 0 || FileSystem.z != 0 && var3 > FileSystem.z) {
            throw new RuntimeException();
        } else if (var2 == 0) {
            byte[] var4 = new byte[var3];
            var1.al(var4, 0, var3);
            return var4;
        } else {
            int var6 = var1.ap();
            if (var6 < 0 || FileSystem.z != 0 && var6 > FileSystem.z) {
                throw new RuntimeException();
            } else {
                byte[] var5 = new byte[var6];
                if (var2 == 1) {
                    gp.t(var5, var6, var0, var3, 9);
                } else {
                    FileSystem.v.t(var1, var5);
                }

                return var5;
            }
        }
    }
}
