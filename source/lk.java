import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lk")
public final class lk extends li {

    @ObfuscatedName("t")
    public byte[] t;

    @ObfuscatedName("q")
    public int[] q;

    @ObfuscatedName("i")
    public int i;

    @ObfuscatedName("a")
    public int a;

    @ObfuscatedName("l")
    public int l;

    @ObfuscatedName("b")
    public int b;

    @ObfuscatedName("e")
    public int e;

    @ObfuscatedName("x")
    public int x;

    @ObfuscatedName("t")
    public void t() {
        if (this.i != this.e || this.a != this.x) {
            byte[] var1 = new byte[this.e * this.x];
            int var2 = 0;

            for (int var3 = 0; var3 < this.a; ++var3) {
                for (int var4 = 0; var4 < this.i; ++var4) {
                    var1[var4 + (var3 + this.b) * this.e + this.l] = this.t[var2++];
                }
            }

            this.t = var1;
            this.i = this.e;
            this.a = this.x;
            this.l = 0;
            this.b = 0;
        }
    }

    @ObfuscatedName("q")
    public void q(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < this.q.length; ++var4) {
            int var5 = this.q[var4] >> 16 & 255;
            var5 += var1;
            if (var5 < 0) {
                var5 = 0;
            } else if (var5 > 255) {
                var5 = 255;
            }

            int var6 = this.q[var4] >> 8 & 255;
            var6 += var2;
            if (var6 < 0) {
                var6 = 0;
            } else if (var6 > 255) {
                var6 = 255;
            }

            int var7 = this.q[var4] & 255;
            var7 += var3;
            if (var7 < 0) {
                var7 = 0;
            } else if (var7 > 255) {
                var7 = 255;
            }

            this.q[var4] = var7 + (var6 << 8) + (var5 << 16);
        }

    }

    @ObfuscatedName("i")
    public void i(int var1, int var2) {
        var1 += this.l;
        var2 += this.b;
        int var3 = var1 + var2 * li.av;
        int var4 = 0;
        int var5 = this.a;
        int var6 = this.i;
        int var7 = li.av - var6;
        int var8 = 0;
        int var9;
        if (var2 < li.ae) {
            var9 = li.ae - var2;
            var5 -= var9;
            var2 = li.ae;
            var4 += var9 * var6;
            var3 += var9 * li.av;
        }

        if (var5 + var2 > li.am) {
            var5 -= var5 + var2 - li.am;
        }

        if (var1 < li.az) {
            var9 = li.az - var1;
            var6 -= var9;
            var1 = li.az;
            var4 += var9;
            var3 += var9;
            var8 += var9;
            var7 += var9;
        }

        if (var6 + var1 > li.ap) {
            var9 = var6 + var1 - li.ap;
            var6 -= var9;
            var8 += var9;
            var7 += var9;
        }

        if (var6 > 0 && var5 > 0) {
            a(li.ao, this.t, this.q, var4, var3, var6, var5, var7, var8);
        }
    }

    @ObfuscatedName("l")
    public void l(int var1, int var2, int var3, int var4) {
        int var5 = this.i;
        int var6 = this.a;
        int var7 = 0;
        int var8 = 0;
        int var9 = this.e;
        int var10 = this.x;
        int var11 = (var9 << 16) / var3;
        int var12 = (var10 << 16) / var4;
        int var13;
        if (this.l > 0) {
            var13 = (var11 + (this.l << 16) - 1) / var11;
            var1 += var13;
            var7 += var13 * var11 - (this.l << 16);
        }

        if (this.b > 0) {
            var13 = (var12 + (this.b << 16) - 1) / var12;
            var2 += var13;
            var8 += var13 * var12 - (this.b << 16);
        }

        if (var5 < var9) {
            var3 = (var11 + ((var5 << 16) - var7) - 1) / var11;
        }

        if (var6 < var10) {
            var4 = (var12 + ((var6 << 16) - var8) - 1) / var12;
        }

        var13 = var1 + var2 * li.av;
        int var14 = li.av - var3;
        if (var2 + var4 > li.am) {
            var4 -= var2 + var4 - li.am;
        }

        int var15;
        if (var2 < li.ae) {
            var15 = li.ae - var2;
            var4 -= var15;
            var13 += var15 * li.av;
            var8 += var12 * var15;
        }

        if (var3 + var1 > li.ap) {
            var15 = var3 + var1 - li.ap;
            var3 -= var15;
            var14 += var15;
        }

        if (var1 < li.az) {
            var15 = li.az - var1;
            var3 -= var15;
            var13 += var15;
            var7 += var11 * var15;
            var14 += var15;
        }

        b(li.ao, this.t, this.q, var7, var8, var13, var14, var3, var4, var11, var12, var5);
    }

    @ObfuscatedName("a")
    static void a(int[] var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        int var9 = -(var5 >> 2);
        var5 = -(var5 & 3);

        for (int var10 = -var6; var10 < 0; ++var10) {
            int var11;
            byte var12;
            for (var11 = var9; var11 < 0; ++var11) {
                var12 = var1[var3++];
                if (var12 != 0) {
                    var0[var4++] = var2[var12 & 255];
                } else {
                    ++var4;
                }

                var12 = var1[var3++];
                if (var12 != 0) {
                    var0[var4++] = var2[var12 & 255];
                } else {
                    ++var4;
                }

                var12 = var1[var3++];
                if (var12 != 0) {
                    var0[var4++] = var2[var12 & 255];
                } else {
                    ++var4;
                }

                var12 = var1[var3++];
                if (var12 != 0) {
                    var0[var4++] = var2[var12 & 255];
                } else {
                    ++var4;
                }
            }

            for (var11 = var5; var11 < 0; ++var11) {
                var12 = var1[var3++];
                if (var12 != 0) {
                    var0[var4++] = var2[var12 & 255];
                } else {
                    ++var4;
                }
            }

            var4 += var7;
            var3 += var8;
        }

    }

    @ObfuscatedName("b")
    static void b(int[] var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8,
            int var9, int var10, int var11) {
        int var12 = var3;

        for (int var13 = -var8; var13 < 0; ++var13) {
            int var14 = var11 * (var4 >> 16);

            for (int var15 = -var7; var15 < 0; ++var15) {
                byte var16 = var1[(var3 >> 16) + var14];
                if (var16 != 0) {
                    var0[var5++] = var2[var16 & 255];
                } else {
                    ++var5;
                }

                var3 += var9;
            }

            var4 += var10;
            var3 = var12;
            var5 += var6;
        }

    }
}
