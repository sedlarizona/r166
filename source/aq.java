import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("aq")
public class aq extends az {

    @ObfuscatedName("mb")
    static fs mb;

    @ObfuscatedName("fw")
    static Sprite fw;

    @ObfuscatedName("c")
    HashSet c;

    @ObfuscatedName("v")
    HashSet v;

    @ObfuscatedName("u")
    List u;

    @ObfuscatedName("cu")
    void cu(ByteBuffer var1, ByteBuffer var2, ByteBuffer var3, int var4, boolean var5) {
        this.t(var1, var4);
        int var6 = var3.ae();
        this.c = new HashSet(var6);

        int var7;
        for (var7 = 0; var7 < var6; ++var7) {
            r var8 = new r();

            try {
                var8.t(var2, var3);
            } catch (IllegalStateException var13) {
                continue;
            }

            this.c.add(var8);
        }

        var7 = var3.ae();
        this.v = new HashSet(var7);

        for (int var11 = 0; var11 < var7; ++var11) {
            aa var9 = new aa();

            try {
                var9.t(var2, var3);
            } catch (IllegalStateException var12) {
                continue;
            }

            this.v.add(var9);
        }

        this.cs(var2, var5);
    }

    @ObfuscatedName("cs")
    void cs(ByteBuffer var1, boolean var2) {
        this.u = new LinkedList();
        int var3 = var1.ae();

        for (int var4 = 0; var4 < var3; ++var4) {
            int var5 = var1.aw();
            ik var6 = new ik(var1.ap());
            boolean var7 = var1.av() == 1;
            if (var2 || !var7) {
                this.u.add(new m(var5, var6));
            }
        }

    }

    @ObfuscatedName("o")
    static final void o() {
        ak.p("Your friend list is full. Max of 200 for free users, and 400 for members");
    }

    @ObfuscatedName("k")
   static void k(Server var0) {
      if (var0.x() != Client.membersWorld) {
         Client.membersWorld = var0.x();
         is.y(var0.x());
      }

      u.do = var0.domain;
      Client.currentWorld = var0.number;
      Client.br = var0.type;
      dc.dw = Client.playerWeight == 0 ? 'ꩊ' : var0.number + '鱀';
      Player.dd = Client.playerWeight == 0 ? 443 : var0.number + '썐';
      cr.du = dc.dw;
   }
}
