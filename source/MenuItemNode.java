import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cv")
public class MenuItemNode {

    @ObfuscatedName("cg")
    static ju cg;

    @ObfuscatedName("t")
    String text;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;
}
