import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ih")
public class ih {

    @ObfuscatedName("cx")
    static long cx;

    @ObfuscatedName("a")
    public static in[] a() {
        return new in[] { in.a, in.t, in.i, in.l, in.q, in.b };
    }
}
