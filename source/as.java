import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("as")
public interface as {

    @ObfuscatedName("t")
    void t(az var1);

    @ObfuscatedName("q")
    boolean q(int var1, int var2, int var3);

    @ObfuscatedName("i")
    boolean i(int var1, int var2);

    @ObfuscatedName("a")
    int[] a(int var1, int var2, int var3);

    @ObfuscatedName("l")
    ik l(int var1, int var2);

    @ObfuscatedName("b")
    void b(ByteBuffer var1);
}
