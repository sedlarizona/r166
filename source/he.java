import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("he")
public final class he {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    FixedSizeDeque i;

    @ObfuscatedName("a")
    DoublyNode a = new DoublyNode();

    public he(int var1, int var2) {
        this.t = var1;
        this.q = var1;

        int var3;
        for (var3 = 1; var3 + var3 < var1 && var3 < var2; var3 += var3) {
            ;
        }

        this.i = new FixedSizeDeque(var3);
    }

    @ObfuscatedName("t")
    public Object t(long var1) {
        hs var3 = (hs) this.i.t(var1);
        if (var3 == null) {
            return null;
        } else {
            Object var4 = var3.t();
            if (var4 == null) {
                var3.kc();
                var3.ce();
                this.q += var3.q;
                return null;
            } else {
                if (var3.q()) {
                    ht var5 = new ht(var4, var3.q);
                    this.i.q(var5, var3.uid);
                    this.a.q(var5);
                    var5.cg = 0L;
                    var3.kc();
                    var3.ce();
                } else {
                    this.a.q(var3);
                    var3.cg = 0L;
                }

                return var4;
            }
        }
    }

    @ObfuscatedName("q")
    void q(long var1) {
        hs var3 = (hs) this.i.t(var1);
        this.i(var3);
    }

    @ObfuscatedName("i")
    void i(hs var1) {
        if (var1 != null) {
            var1.kc();
            var1.ce();
            this.q += var1.q;
        }

    }

    @ObfuscatedName("a")
    public void a(Object var1, long var2) {
        this.l(var1, var2, 1);
    }

    @ObfuscatedName("l")
    public void l(Object var1, long var2, int var4) {
        if (var4 > this.t) {
            throw new IllegalStateException();
        } else {
            this.q(var2);
            this.q -= var4;

            while (this.q < 0) {
                hs var5 = (hs) this.a.i();
                this.i(var5);
            }

            ht var6 = new ht(var1, var4);
            this.i.q(var6, var2);
            this.a.q(var6);
            var6.cg = 0L;
        }
    }

    @ObfuscatedName("b")
    public void b(int var1) {
        for (hs var2 = (hs) this.a.a(); var2 != null; var2 = (hs) this.a.b()) {
            if (var2.q()) {
                if (var2.t() == null) {
                    var2.kc();
                    var2.ce();
                    this.q += var2.q;
                }
            } else if (++var2.cg > (long) var1) {
                IterableDoublyNode var3 = new IterableDoublyNode(var2.t(), var2.q);
                this.i.q(var3, var2.uid);
                Queue.i(var3, var2);
                var2.kc();
                var2.ce();
            }
        }

    }

    @ObfuscatedName("e")
    public void e() {
        this.a.t();
        this.i.i();
        this.q = this.t;
    }
}
