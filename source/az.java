import java.util.Iterator;
import java.util.LinkedList;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("az")
public class az {

    @ObfuscatedName("il")
    static Player il;

    @ObfuscatedName("t")
    int t = -1;

    @ObfuscatedName("q")
    String q;

    @ObfuscatedName("i")
    String i;

    @ObfuscatedName("a")
    int a = -1;

    @ObfuscatedName("l")
    int l = -1;

    @ObfuscatedName("b")
    ik b = null;

    @ObfuscatedName("e")
    int e = Integer.MAX_VALUE;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    int p = Integer.MAX_VALUE;

    @ObfuscatedName("g")
    int g = 0;

    @ObfuscatedName("n")
    boolean n = false;

    @ObfuscatedName("o")
    LinkedList o;

    @ObfuscatedName("t")
    public void t(ByteBuffer var1, int var2) {
        this.t = var2;
        this.q = var1.ar();
        this.i = var1.ar();
        this.b = new ik(var1.ap());
        this.a = var1.ap();
        var1.av();
        this.n = var1.av() == 1;
        this.l = var1.av();
        int var3 = var1.av();
        this.o = new LinkedList();

        for (int var4 = 0; var4 < var3; ++var4) {
            this.o.add(this.q(var1));
        }

        this.e();
    }

    @ObfuscatedName("q")
    as q(ByteBuffer var1) {
        int var2 = var1.av();
        ao[] var3 = new ao[] { ao.i, ao.a, ao.t, ao.q };
        ao var4 = (ao) aj.t(var3, var2);
        Object var5 = null;
        switch (var4.l) {
        case 0:
            var5 = new ai();
            break;
        case 1:
            var5 = new ae();
            break;
        case 2:
            var5 = new ab();
            break;
        case 3:
            var5 = new ay();
            break;
        default:
            throw new IllegalStateException("");
        }

        ((as) var5).b(var1);
        return (as) var5;
    }

    @ObfuscatedName("i")
    public boolean i(int var1, int var2, int var3) {
        Iterator var4 = this.o.iterator();

        as var5;
        do {
            if (!var4.hasNext()) {
                return false;
            }

            var5 = (as) var4.next();
        } while (!var5.q(var1, var2, var3));

        return true;
    }

    @ObfuscatedName("a")
    public boolean a(int var1, int var2) {
        int var3 = var1 / 64;
        int var4 = var2 / 64;
        if (var3 >= this.e && var3 <= this.x) {
            if (var4 >= this.p && var4 <= this.g) {
                Iterator var5 = this.o.iterator();

                as var6;
                do {
                    if (!var5.hasNext()) {
                        return false;
                    }

                    var6 = (as) var5.next();
                } while (!var6.i(var1, var2));

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @ObfuscatedName("l")
    public int[] l(int var1, int var2, int var3) {
        Iterator var4 = this.o.iterator();

        as var5;
        do {
            if (!var4.hasNext()) {
                return null;
            }

            var5 = (as) var4.next();
        } while (!var5.q(var1, var2, var3));

        return var5.a(var1, var2, var3);
    }

    @ObfuscatedName("b")
    public ik b(int var1, int var2) {
        Iterator var3 = this.o.iterator();

        as var4;
        do {
            if (!var3.hasNext()) {
                return null;
            }

            var4 = (as) var3.next();
        } while (!var4.i(var1, var2));

        return var4.l(var1, var2);
    }

    @ObfuscatedName("e")
    void e() {
        // $FF: Couldn't be decompiled
    }

    @ObfuscatedName("x")
    public int x() {
        return this.t;
    }

    @ObfuscatedName("p")
    public boolean p() {
        return this.n;
    }

    @ObfuscatedName("o")
    public String o() {
        return this.q;
    }

    @ObfuscatedName("c")
    public String c() {
        return this.i;
    }

    @ObfuscatedName("u")
    int u() {
        return this.a;
    }

    @ObfuscatedName("k")
    public int k() {
        return this.l;
    }

    @ObfuscatedName("z")
    public int z() {
        return this.e;
    }

    @ObfuscatedName("w")
    public int w() {
        return this.x;
    }

    @ObfuscatedName("s")
    public int s() {
        return this.p;
    }

    @ObfuscatedName("d")
    public int d() {
        return this.g;
    }

    @ObfuscatedName("f")
    public int f() {
        return this.b.q;
    }

    @ObfuscatedName("r")
    public int r() {
        return this.b.t;
    }

    @ObfuscatedName("y")
    public int y() {
        return this.b.i;
    }

    @ObfuscatedName("h")
    public ik h() {
        return new ik(this.b);
    }

    @ObfuscatedName("q")
    static final void q(int var0, int var1, int var2, int var3) {
        for (int var4 = var1; var4 <= var3 + var1; ++var4) {
            for (int var5 = var0; var5 <= var0 + var2; ++var5) {
                if (var5 >= 0 && var5 < 104 && var4 >= 0 && var4 < 104) {
                    bt.x[0][var5][var4] = 127;
                    if (var0 == var5 && var5 > 0) {
                        bt.tileHeights[0][var5][var4] = bt.tileHeights[0][var5 - 1][var4];
                    }

                    if (var0 + var2 == var5 && var5 < 103) {
                        bt.tileHeights[0][var5][var4] = bt.tileHeights[0][var5 + 1][var4];
                    }

                    if (var4 == var1 && var4 > 0) {
                        bt.tileHeights[0][var5][var4] = bt.tileHeights[0][var5][var4 - 1];
                    }

                    if (var4 == var3 + var1 && var4 < 103) {
                        bt.tileHeights[0][var5][var4] = bt.tileHeights[0][var5][var4 + 1];
                    }
                }
            }
        }

    }

    @ObfuscatedName("i")
    public static lk[] i(FileSystem var0, String var1, String var2) {
        int var3 = var0.h(var1);
        int var4 = var0.av(var3, var2);
        byte[] var7 = var0.i(var3, var4);
        boolean var6;
        if (var7 == null) {
            var6 = false;
        } else {
            RTComponent.k(var7);
            var6 = true;
        }

        lk[] var5;
        if (!var6) {
            var5 = null;
        } else {
            var5 = ke.x();
        }

        return var5;
    }

    @ObfuscatedName("e")
    public static void e() {
        // $FF: Couldn't be decompiled
    }

    @ObfuscatedName("aj")
    static void aj(int var0) {
        if (var0 != -1) {
            if (RuneScript.i(var0)) {
                RTComponent[] var1 = RTComponent.b[var0];

                for (int var2 = 0; var2 < var1.length; ++var2) {
                    RTComponent var3 = var1[var2];
                    if (var3.cf != null) {
                        ScriptEvent var4 = new ScriptEvent();
                        var4.i = var3;
                        var4.args = var3.cf;
                        m.t(var4, 5000000);
                    }
                }

            }
        }
    }

    @ObfuscatedName("fs")
    static void fs(int var0) {
        if (var0 == -1 && !Client.oe) {
            RuneScriptStackItem.l();
        } else if (var0 != -1 && var0 != Client.ox && Client.os != 0 && !Client.oe) {
            Npc.e(2, Varpbit.ca, var0, 0, Client.os, false);
        }

        Client.ox = var0;
    }
}
