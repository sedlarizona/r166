import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gp")
public final class gp {

    @ObfuscatedName("x")
    static gq x = new gq();

    @ObfuscatedName("t")
    public static int t(byte[] var0, int var1, byte[] var2, int var3, int var4) {
        gq var5 = x;
        synchronized (x) {
            x.e = var2;
            x.x = var4;
            x.g = var0;
            x.n = 0;
            x.o = var1;
            x.k = 0;
            x.j = 0;
            x.p = 0;
            x.c = 0;
            i(x);
            var1 -= x.o;
            x.e = null;
            x.g = null;
            return var1;
        }
    }

    @ObfuscatedName("q")
    static void q(gq var0) {
        byte var2 = var0.v;
        int var3 = var0.u;
        int var4 = var0.r;
        int var5 = var0.d;
        int[] var6 = gu.h;
        int var7 = var0.s;
        byte[] var8 = var0.g;
        int var9 = var0.n;
        int var10 = var0.o;
        int var12 = var0.an + 1;

        label65: while (true) {
            if (var3 > 0) {
                while (true) {
                    if (var10 == 0) {
                        break label65;
                    }

                    if (var3 == 1) {
                        if (var10 == 0) {
                            var3 = 1;
                            break label65;
                        }

                        var8[var9] = var2;
                        ++var9;
                        --var10;
                        break;
                    }

                    var8[var9] = var2;
                    --var3;
                    ++var9;
                    --var10;
                }
            }

            boolean var14 = true;

            byte var1;
            while (var14) {
                var14 = false;
                if (var4 == var12) {
                    var3 = 0;
                    break label65;
                }

                var2 = (byte) var5;
                var7 = var6[var7];
                var1 = (byte) (var7 & 255);
                var7 >>= 8;
                ++var4;
                if (var1 != var5) {
                    var5 = var1;
                    if (var10 == 0) {
                        var3 = 1;
                        break label65;
                    }

                    var8[var9] = var2;
                    ++var9;
                    --var10;
                    var14 = true;
                } else if (var4 == var12) {
                    if (var10 == 0) {
                        var3 = 1;
                        break label65;
                    }

                    var8[var9] = var2;
                    ++var9;
                    --var10;
                    var14 = true;
                }
            }

            var3 = 2;
            var7 = var6[var7];
            var1 = (byte) (var7 & 255);
            var7 >>= 8;
            ++var4;
            if (var4 != var12) {
                if (var1 != var5) {
                    var5 = var1;
                } else {
                    var3 = 3;
                    var7 = var6[var7];
                    var1 = (byte) (var7 & 255);
                    var7 >>= 8;
                    ++var4;
                    if (var4 != var12) {
                        if (var1 != var5) {
                            var5 = var1;
                        } else {
                            var7 = var6[var7];
                            var1 = (byte) (var7 & 255);
                            var7 >>= 8;
                            ++var4;
                            var3 = (var1 & 255) + 4;
                            var7 = var6[var7];
                            var5 = (byte) (var7 & 255);
                            var7 >>= 8;
                            ++var4;
                        }
                    }
                }
            }
        }

        int var13 = var0.c;
        var0.c += var10 - var10;
        if (var0.c < var13) {
            ;
        }

        var0.v = var2;
        var0.u = var3;
        var0.r = var4;
        var0.d = var5;
        gu.h = var6;
        var0.s = var7;
        var0.g = var8;
        var0.n = var9;
        var0.o = var10;
    }

    @ObfuscatedName("i")
    static void i(gq var0) {
        boolean var4 = false;
        boolean var5 = false;
        boolean var6 = false;
        boolean var7 = false;
        boolean var8 = false;
        boolean var9 = false;
        boolean var10 = false;
        boolean var11 = false;
        boolean var12 = false;
        boolean var13 = false;
        boolean var14 = false;
        boolean var15 = false;
        boolean var16 = false;
        boolean var17 = false;
        boolean var18 = false;
        boolean var19 = false;
        boolean var20 = false;
        boolean var21 = false;
        int var22 = 0;
        int[] var23 = null;
        int[] var24 = null;
        int[] var25 = null;
        var0.z = 1;
        if (gu.h == null) {
            gu.h = new int[var0.z * 100000];
        }

        boolean var26 = true;

        while (true) {
            while (var26) {
                byte var1 = a(var0);
                if (var1 == 23) {
                    return;
                }

                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = a(var0);
                var1 = l(var0);
                if (var1 != 0) {
                    ;
                }

                var0.w = 0;
                var1 = a(var0);
                var0.w = var0.w << 8 | var1 & 255;
                var1 = a(var0);
                var0.w = var0.w << 8 | var1 & 255;
                var1 = a(var0);
                var0.w = var0.w << 8 | var1 & 255;

                int var36;
                for (var36 = 0; var36 < 16; ++var36) {
                    var1 = l(var0);
                    if (var1 == 1) {
                        var0.ao[var36] = true;
                    } else {
                        var0.ao[var36] = false;
                    }
                }

                for (var36 = 0; var36 < 256; ++var36) {
                    var0.ay[var36] = false;
                }

                int var37;
                for (var36 = 0; var36 < 16; ++var36) {
                    if (var0.ao[var36]) {
                        for (var37 = 0; var37 < 16; ++var37) {
                            var1 = l(var0);
                            if (var1 == 1) {
                                var0.ay[var37 + var36 * 16] = true;
                            }
                        }
                    }
                }

                e(var0);
                int var39 = var0.m + 2;
                int var40 = b(3, var0);
                int var41 = b(15, var0);

                for (var36 = 0; var36 < var41; ++var36) {
                    var37 = 0;

                    while (true) {
                        var1 = l(var0);
                        if (var1 == 0) {
                            var0.az[var36] = (byte) var37;
                            break;
                        }

                        ++var37;
                    }
                }

                byte[] var27 = new byte[6];

                byte var29;
                for (var29 = 0; var29 < var40; var27[var29] = var29++) {
                    ;
                }

                for (var36 = 0; var36 < var41; ++var36) {
                    var29 = var0.az[var36];

                    byte var28;
                    for (var28 = var27[var29]; var29 > 0; --var29) {
                        var27[var29] = var27[var29 - 1];
                    }

                    var27[0] = var28;
                    var0.am[var36] = var28;
                }

                int var38;
                for (var38 = 0; var38 < var40; ++var38) {
                    int var50 = b(5, var0);

                    for (var36 = 0; var36 < var39; ++var36) {
                        while (true) {
                            var1 = l(var0);
                            if (var1 == 0) {
                                var0.ap[var38][var36] = (byte) var50;
                                break;
                            }

                            var1 = l(var0);
                            if (var1 == 0) {
                                ++var50;
                            } else {
                                --var50;
                            }
                        }
                    }
                }

                for (var38 = 0; var38 < var40; ++var38) {
                    byte var2 = 32;
                    byte var3 = 0;

                    for (var36 = 0; var36 < var39; ++var36) {
                        if (var0.ap[var38][var36] > var3) {
                            var3 = var0.ap[var38][var36];
                        }

                        if (var0.ap[var38][var36] < var2) {
                            var2 = var0.ap[var38][var36];
                        }
                    }

                    x(var0.ah[var38], var0.au[var38], var0.ax[var38], var0.ap[var38], var2, var3, var39);
                    var0.ar[var38] = var2;
                }

                int var42 = var0.m + 1;
                int var43 = -1;
                byte var44 = 0;

                for (var36 = 0; var36 <= 255; ++var36) {
                    var0.f[var36] = 0;
                }

                int var56 = 4095;

                int var35;
                int var55;
                for (var35 = 15; var35 >= 0; --var35) {
                    for (var55 = 15; var55 >= 0; --var55) {
                        var0.aj[var56] = (byte) (var55 + var35 * 16);
                        --var56;
                    }

                    var0.ae[var35] = var56 + 1;
                }

                int var47 = 0;
                byte var54;
                if (var44 == 0) {
                    ++var43;
                    var44 = 50;
                    var54 = var0.am[var43];
                    var22 = var0.ar[var54];
                    var23 = var0.ah[var54];
                    var25 = var0.ax[var54];
                    var24 = var0.au[var54];
                }

                int var45 = var44 - 1;
                int var51 = var22;

                int var52;
                byte var53;
                for (var52 = b(var22, var0); var52 > var23[var51]; var52 = var52 << 1 | var53) {
                    ++var51;
                    var53 = l(var0);
                }

                int var46 = var25[var52 - var24[var51]];

                while (true) {
                    while (var46 != var42) {
                        if (var46 != 0 && var46 != 1) {
                            int var33 = var46 - 1;
                            int var30;
                            if (var33 < 16) {
                                var30 = var0.ae[0];

                                for (var1 = var0.aj[var30 + var33]; var33 > 3; var33 -= 4) {
                                    int var34 = var30 + var33;
                                    var0.aj[var34] = var0.aj[var34 - 1];
                                    var0.aj[var34 - 1] = var0.aj[var34 - 2];
                                    var0.aj[var34 - 2] = var0.aj[var34 - 3];
                                    var0.aj[var34 - 3] = var0.aj[var34 - 4];
                                }

                                while (var33 > 0) {
                                    var0.aj[var30 + var33] = var0.aj[var30 + var33 - 1];
                                    --var33;
                                }

                                var0.aj[var30] = var1;
                            } else {
                                int var31 = var33 / 16;
                                int var32 = var33 % 16;
                                var30 = var0.ae[var31] + var32;

                                for (var1 = var0.aj[var30]; var30 > var0.ae[var31]; --var30) {
                                    var0.aj[var30] = var0.aj[var30 - 1];
                                }

                                ++var0.ae[var31];

                                while (var31 > 0) {
                                    --var0.ae[var31];
                                    var0.aj[var0.ae[var31]] = var0.aj[var0.ae[var31 - 1] + 16 - 1];
                                    --var31;
                                }

                                --var0.ae[0];
                                var0.aj[var0.ae[0]] = var1;
                                if (var0.ae[0] == 0) {
                                    var56 = 4095;

                                    for (var35 = 15; var35 >= 0; --var35) {
                                        for (var55 = 15; var55 >= 0; --var55) {
                                            var0.aj[var56] = var0.aj[var0.ae[var35] + var55];
                                            --var56;
                                        }

                                        var0.ae[var35] = var56 + 1;
                                    }
                                }
                            }

                            ++var0.f[var0.av[var1 & 255] & 255];
                            gu.h[var47] = var0.av[var1 & 255] & 255;
                            ++var47;
                            if (var45 == 0) {
                                ++var43;
                                var45 = 50;
                                var54 = var0.am[var43];
                                var22 = var0.ar[var54];
                                var23 = var0.ah[var54];
                                var25 = var0.ax[var54];
                                var24 = var0.au[var54];
                            }

                            --var45;
                            var51 = var22;

                            for (var52 = b(var22, var0); var52 > var23[var51]; var52 = var52 << 1 | var53) {
                                ++var51;
                                var53 = l(var0);
                            }

                            var46 = var25[var52 - var24[var51]];
                        } else {
                            int var48 = -1;
                            int var49 = 1;

                            do {
                                if (var46 == 0) {
                                    var48 += var49;
                                } else if (var46 == 1) {
                                    var48 += var49 * 2;
                                }

                                var49 *= 2;
                                if (var45 == 0) {
                                    ++var43;
                                    var45 = 50;
                                    var54 = var0.am[var43];
                                    var22 = var0.ar[var54];
                                    var23 = var0.ah[var54];
                                    var25 = var0.ax[var54];
                                    var24 = var0.au[var54];
                                }

                                --var45;
                                var51 = var22;

                                for (var52 = b(var22, var0); var52 > var23[var51]; var52 = var52 << 1 | var53) {
                                    ++var51;
                                    var53 = l(var0);
                                }

                                var46 = var25[var52 - var24[var51]];
                            } while (var46 == 0 || var46 == 1);

                            ++var48;
                            var1 = var0.av[var0.aj[var0.ae[0]] & 255];

                            for (var0.f[var1 & 255] += var48; var48 > 0; --var48) {
                                gu.h[var47] = var1 & 255;
                                ++var47;
                            }
                        }
                    }

                    var0.u = 0;
                    var0.v = 0;
                    var0.y[0] = 0;

                    for (var36 = 1; var36 <= 256; ++var36) {
                        var0.y[var36] = var0.f[var36 - 1];
                    }

                    for (var36 = 1; var36 <= 256; ++var36) {
                        var0.y[var36] += var0.y[var36 - 1];
                    }

                    for (var36 = 0; var36 < var47; ++var36) {
                        var1 = (byte) (gu.h[var36] & 255);
                        gu.h[var0.y[var1 & 255]] |= var36 << 8;
                        ++var0.y[var1 & 255];
                    }

                    var0.s = gu.h[var0.w] >> 8;
                    var0.r = 0;
                    var0.s = gu.h[var0.s];
                    var0.d = (byte) (var0.s & 255);
                    var0.s >>= 8;
                    ++var0.r;
                    var0.an = var47;
                    q(var0);
                    if (var0.an + 1 == var0.r && var0.u == 0) {
                        var26 = true;
                        break;
                    }

                    var26 = false;
                    break;
                }
            }

            return;
        }
    }

    @ObfuscatedName("a")
    static byte a(gq var0) {
        return (byte) b(8, var0);
    }

    @ObfuscatedName("l")
    static byte l(gq var0) {
        return (byte) b(1, var0);
    }

    @ObfuscatedName("b")
    static int b(int var0, gq var1) {
        while (var1.k < var0) {
            var1.j = var1.j << 8 | var1.e[var1.x] & 255;
            var1.k += 8;
            ++var1.x;
            ++var1.p;
            if (var1.p == 0) {
                ;
            }
        }

        int var3 = var1.j >> var1.k - var0 & (1 << var0) - 1;
        var1.k -= var0;
        return var3;
    }

    @ObfuscatedName("e")
    static void e(gq var0) {
        var0.m = 0;

        for (int var1 = 0; var1 < 256; ++var1) {
            if (var0.ay[var1]) {
                var0.av[var0.m] = (byte) var1;
                ++var0.m;
            }
        }

    }

    @ObfuscatedName("x")
    static void x(int[] var0, int[] var1, int[] var2, byte[] var3, int var4, int var5, int var6) {
        int var7 = 0;

        int var8;
        for (var8 = var4; var8 <= var5; ++var8) {
            for (int var9 = 0; var9 < var6; ++var9) {
                if (var8 == var3[var9]) {
                    var2[var7] = var9;
                    ++var7;
                }
            }
        }

        for (var8 = 0; var8 < 23; ++var8) {
            var1[var8] = 0;
        }

        for (var8 = 0; var8 < var6; ++var8) {
            ++var1[var3[var8] + 1];
        }

        for (var8 = 1; var8 < 23; ++var8) {
            var1[var8] += var1[var8 - 1];
        }

        for (var8 = 0; var8 < 23; ++var8) {
            var0[var8] = 0;
        }

        int var10 = 0;

        for (var8 = var4; var8 <= var5; ++var8) {
            var10 += var1[var8 + 1] - var1[var8];
            var0[var8] = var10 - 1;
            var10 <<= 1;
        }

        for (var8 = var4 + 1; var8 <= var5; ++var8) {
            var1[var8] = (var0[var8 - 1] + 1 << 1) - var1[var8];
        }

    }
}
