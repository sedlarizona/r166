import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jw")
public class jw extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static FileSystem q;

    @ObfuscatedName("i")
    public static Cache i = new Cache(64);

    @ObfuscatedName("a")
    public static Cache a = new Cache(30);

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    public int e = -1;

    @ObfuscatedName("x")
    short[] x;

    @ObfuscatedName("p")
    short[] p;

    @ObfuscatedName("g")
    short[] g;

    @ObfuscatedName("n")
    short[] n;

    @ObfuscatedName("o")
    int o = 128;

    @ObfuscatedName("c")
    int c = 128;

    @ObfuscatedName("v")
    int v = 0;

    @ObfuscatedName("u")
    int u = 0;

    @ObfuscatedName("j")
    int j = 0;

    @ObfuscatedName("q")
    void q(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.i(var1, var2);
        }
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.b = var1.ae();
        } else if (var2 == 2) {
            this.e = var1.ae();
        } else if (var2 == 4) {
            this.o = var1.ae();
        } else if (var2 == 5) {
            this.c = var1.ae();
        } else if (var2 == 6) {
            this.v = var1.ae();
        } else if (var2 == 7) {
            this.u = var1.av();
        } else if (var2 == 8) {
            this.j = var1.av();
        } else {
            int var3;
            int var4;
            if (var2 == 40) {
                var3 = var1.av();
                this.x = new short[var3];
                this.p = new short[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.x[var4] = (short) var1.ae();
                    this.p[var4] = (short) var1.ae();
                }
            } else if (var2 == 41) {
                var3 = var1.av();
                this.g = new short[var3];
                this.n = new short[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.g[var4] = (short) var1.ae();
                    this.n[var4] = (short) var1.ae();
                }
            }
        }

    }

    @ObfuscatedName("a")
    public final Model a(int var1) {
        Model var2 = (Model) a.t((long) this.l);
        if (var2 == null) {
            AlternativeModel var3 = AlternativeModel.t(q, this.b, 0);
            if (var3 == null) {
                return null;
            }

            int var4;
            if (this.x != null) {
                for (var4 = 0; var4 < this.x.length; ++var4) {
                    var3.z(this.x[var4], this.p[var4]);
                }
            }

            if (this.g != null) {
                for (var4 = 0; var4 < this.g.length; ++var4) {
                    var3.w(this.g[var4], this.n[var4]);
                }
            }

            var2 = var3.av(this.u + 64, this.j + 850, -30, -50, -30);
            a.i(var2, (long) this.l);
        }

        Model var5;
        if (this.e != -1 && var1 != -1) {
            var5 = ff.t(this.e).e(var2, var1);
        } else {
            var5 = var2.i(true);
        }

        if (this.o != 128 || this.c != 128) {
            var5.r(this.o, this.c, this.o);
        }

        if (this.v != 0) {
            if (this.v == 90) {
                var5.z();
            }

            if (this.v == 180) {
                var5.z();
                var5.z();
            }

            if (this.v == 270) {
                var5.z();
                var5.z();
                var5.z();
            }
        }

        return var5;
    }

    @ObfuscatedName("q")
    public static ObjectDefinition q(int var0) {
        ObjectDefinition var1 = (ObjectDefinition) ObjectDefinition.a.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = ObjectDefinition.q.i(6, var0);
            var1 = new ObjectDefinition();
            var1.id = var0;
            if (var2 != null) {
                var1.a(new ByteBuffer(var2));
            }

            var1.i();
            if (var1.al) {
                var1.w = 0;
                var1.s = false;
            }

            ObjectDefinition.a.i(var1, (long) var0);
            return var1;
        }
    }
}
