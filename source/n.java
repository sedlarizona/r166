import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("n")
public final class n extends g {

    @ObfuscatedName("rd")
    static int rd;

    @ObfuscatedName("fi")
    static Sprite[] fi;

    @ObfuscatedName("gp")
    static int gp;

    @ObfuscatedName("t")
    final int t;

    @ObfuscatedName("q")
    final int q;

    @ObfuscatedName("i")
    final int i;

    @ObfuscatedName("a")
    final int a;

    @ObfuscatedName("l")
    final int l;

    @ObfuscatedName("b")
    final int b;

    @ObfuscatedName("e")
    final int e;

    n(Model var1, int var2, int var3, int var4, int var5) {
        this.t = var2 + var1.az - var1.au;
        this.q = var3 + var1.ap - var1.ax;
        this.i = var4 + var1.ah - var1.ar;
        this.a = var2 + var1.au + var1.az;
        this.l = var3 + var1.ap + var1.ax;
        this.b = var4 + var1.ar + var1.ah;
        this.e = var5;
    }

    @ObfuscatedName("t")
    public final void t() {
        for (int var4 = 0; var4 < 8; ++var4) {
            int var1 = (var4 & 1) == 0 ? this.t : this.a;
            int var2 = (var4 & 2) == 0 ? this.q : this.l;
            int var3 = (var4 & 4) == 0 ? this.i : this.b;
            if ((var4 & 1) == 0) {
                Canvas.b(var1, var2, var3, this.a, var2, var3, this.e);
            }

            if ((var4 & 2) == 0) {
                Canvas.b(var1, var2, var3, var1, this.l, var3, this.e);
            }

            if ((var4 & 4) == 0) {
                Canvas.b(var1, var2, var3, var1, var2, this.b, this.e);
            }
        }

    }

    @ObfuscatedName("gj")
    static final void gj(int var0, int var1, int var2, int var3, int var4) {
        int var5 = an.loadedRegion.ap(var0, var1, var2);
        int var6;
        int var7;
        int var8;
        int var9;
        int var11;
        int var12;
        if (var5 != 0) {
            var6 = an.loadedRegion.ar(var0, var1, var2, var5);
            var7 = var6 >> 6 & 3;
            var8 = var6 & 31;
            var9 = var3;
            if (var5 > 0) {
                var9 = var4;
            }

            int[] var10 = GrandExchangeOffer.on.pixels;
            var11 = var1 * 4 + (103 - var2) * 2048 + 24624;
            var12 = var5 >> 14 & 32767;
            ObjectDefinition var13 = jw.q(var12);
            if (var13.ae != -1) {
                lk var14 = ls.fm[var13.ae];
                if (var14 != null) {
                    int var15 = (var13.width * 4 - var14.i) / 2;
                    int var16 = (var13.height * 4 - var14.a) / 2;
                    var14.i(var1 * 4 + var15 + 48, (104 - var2 - var13.height) * 4 + var16 + 48);
                }
            } else {
                if (var8 == 0 || var8 == 2) {
                    if (var7 == 0) {
                        var10[var11] = var9;
                        var10[var11 + 512] = var9;
                        var10[var11 + 1024] = var9;
                        var10[var11 + 1536] = var9;
                    } else if (var7 == 1) {
                        var10[var11] = var9;
                        var10[var11 + 1] = var9;
                        var10[var11 + 2] = var9;
                        var10[var11 + 3] = var9;
                    } else if (var7 == 2) {
                        var10[var11 + 3] = var9;
                        var10[var11 + 512 + 3] = var9;
                        var10[var11 + 1024 + 3] = var9;
                        var10[var11 + 1536 + 3] = var9;
                    } else if (var7 == 3) {
                        var10[var11 + 1536] = var9;
                        var10[var11 + 1536 + 1] = var9;
                        var10[var11 + 1536 + 2] = var9;
                        var10[var11 + 1536 + 3] = var9;
                    }
                }

                if (var8 == 3) {
                    if (var7 == 0) {
                        var10[var11] = var9;
                    } else if (var7 == 1) {
                        var10[var11 + 3] = var9;
                    } else if (var7 == 2) {
                        var10[var11 + 1536 + 3] = var9;
                    } else if (var7 == 3) {
                        var10[var11 + 1536] = var9;
                    }
                }

                if (var8 == 2) {
                    if (var7 == 3) {
                        var10[var11] = var9;
                        var10[var11 + 512] = var9;
                        var10[var11 + 1024] = var9;
                        var10[var11 + 1536] = var9;
                    } else if (var7 == 0) {
                        var10[var11] = var9;
                        var10[var11 + 1] = var9;
                        var10[var11 + 2] = var9;
                        var10[var11 + 3] = var9;
                    } else if (var7 == 1) {
                        var10[var11 + 3] = var9;
                        var10[var11 + 512 + 3] = var9;
                        var10[var11 + 1024 + 3] = var9;
                        var10[var11 + 1536 + 3] = var9;
                    } else if (var7 == 2) {
                        var10[var11 + 1536] = var9;
                        var10[var11 + 1536 + 1] = var9;
                        var10[var11 + 1536 + 2] = var9;
                        var10[var11 + 1536 + 3] = var9;
                    }
                }
            }
        }

        var5 = an.loadedRegion.au(var0, var1, var2);
        if (var5 != 0) {
            var6 = an.loadedRegion.ar(var0, var1, var2, var5);
            var7 = var6 >> 6 & 3;
            var8 = var6 & 31;
            var9 = var5 >> 14 & 32767;
            ObjectDefinition var23 = jw.q(var9);
            int var18;
            if (var23.ae != -1) {
                lk var17 = ls.fm[var23.ae];
                if (var17 != null) {
                    var12 = (var23.width * 4 - var17.i) / 2;
                    var18 = (var23.height * 4 - var17.a) / 2;
                    var17.i(var1 * 4 + var12 + 48, var18 + (104 - var2 - var23.height) * 4 + 48);
                }
            } else if (var8 == 9) {
                var11 = 15658734;
                if (var5 > 0) {
                    var11 = 15597568;
                }

                int[] var22 = GrandExchangeOffer.on.pixels;
                var18 = var1 * 4 + (103 - var2) * 2048 + 24624;
                if (var7 != 0 && var7 != 2) {
                    var22[var18] = var11;
                    var22[var18 + 1 + 512] = var11;
                    var22[var18 + 1024 + 2] = var11;
                    var22[var18 + 1536 + 3] = var11;
                } else {
                    var22[var18 + 1536] = var11;
                    var22[var18 + 1 + 1024] = var11;
                    var22[var18 + 512 + 2] = var11;
                    var22[var18 + 3] = var11;
                }
            }
        }

        var5 = an.loadedRegion.ax(var0, var1, var2);
        if (var5 != 0) {
            var6 = var5 >> 14 & 32767;
            ObjectDefinition var19 = jw.q(var6);
            if (var19.ae != -1) {
                lk var20 = ls.fm[var19.ae];
                if (var20 != null) {
                    var9 = (var19.width * 4 - var20.i) / 2;
                    int var21 = (var19.height * 4 - var20.a) / 2;
                    var20.i(var9 + var1 * 4 + 48, var21 + (104 - var2 - var19.height) * 4 + 48);
                }
            }
        }

    }
}
