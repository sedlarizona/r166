import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hi")
public class hi {

    @ObfuscatedName("q")
    static FileSystem q;

    @ObfuscatedName("i")
    static FileSystem i;

    @ObfuscatedName("a")
    public static AudioTask audioTask;

    @ObfuscatedName("l")
    public static int audioTrackId = 0;

    @ObfuscatedName("b")
    public static FileSystem b;

    @ObfuscatedName("x")
    public static int x;

    @ObfuscatedName("p")
    public static int p;

    @ObfuscatedName("a")
    static cq[] transform() {
        return new cq[] { cq.q, cq.t, cq.i, cq.a };
    }

    @ObfuscatedName("l")
    static void l() {
        cx.b = 0;

        for (int var0 = 0; var0 < 2048; ++var0) {
            cx.l[var0] = null;
            cx.a[var0] = 1;
        }

    }

    @ObfuscatedName("x")
    public static Object x(byte[] var0, boolean var1) {
        if (var0 == null) {
            return null;
        } else {
            if (var0.length > 136 && !gr.q) {
                try {
                    BasicByteBuffer var2 = new BasicByteBuffer();
                    var2.q(var0);
                    return var2;
                } catch (Throwable var3) {
                    gr.q = true;
                }
            }

            return var0;
        }
    }

    @ObfuscatedName("d")
    static final void d(String var0) {
        ak.p("Please remove " + var0 + " from your friend list first");
    }
}
