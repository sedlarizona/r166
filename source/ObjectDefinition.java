import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("je")
public class ObjectDefinition extends hh {

    @ObfuscatedName("t")
    static boolean t = false;

    @ObfuscatedName("q")
    static FileSystem q;

    @ObfuscatedName("a")
    public static Cache a = new Cache(4096);

    @ObfuscatedName("l")
    public static Cache l = new Cache(500);

    @ObfuscatedName("b")
    public static Cache b = new Cache(30);

    @ObfuscatedName("e")
    public static Cache e = new Cache(30);

    @ObfuscatedName("x")
    static AlternativeModel[] x = new AlternativeModel[4];

    @ObfuscatedName("p")
    public int id;

    @ObfuscatedName("g")
    int[] g;

    @ObfuscatedName("n")
    int[] n;

    @ObfuscatedName("o")
    public String name = "null";

    @ObfuscatedName("c")
    short[] baseColors;

    @ObfuscatedName("v")
    short[] texturedColors;

    @ObfuscatedName("u")
    short[] u;

    @ObfuscatedName("j")
    short[] j;

    @ObfuscatedName("k")
    public int width = 1;

    @ObfuscatedName("z")
    public int height = 1;

    @ObfuscatedName("w")
    public int w = 2;

    @ObfuscatedName("s")
    public boolean s = true;

    @ObfuscatedName("d")
    public int d = -1;

    @ObfuscatedName("f")
    int clipType = -1;

    @ObfuscatedName("r")
    boolean r = false;

    @ObfuscatedName("y")
    public boolean clipped = false;

    @ObfuscatedName("h")
    public int h = -1;

    @ObfuscatedName("m")
    public int m = 16;

    @ObfuscatedName("ay")
    int ay = 0;

    @ObfuscatedName("ao")
    int ao = 0;

    @ObfuscatedName("av")
    public String[] actions = new String[5];

    @ObfuscatedName("aj")
    public int mapFunction = -1;

    @ObfuscatedName("ae")
    public int ae = -1;

    @ObfuscatedName("am")
    boolean am = false;

    @ObfuscatedName("az")
    public boolean az = true;

    @ObfuscatedName("ap")
    int ap = 128;

    @ObfuscatedName("ah")
    int ah = 128;

    @ObfuscatedName("au")
    int au = 128;

    @ObfuscatedName("ax")
    int ax = 0;

    @ObfuscatedName("ar")
    int ar = 0;

    @ObfuscatedName("an")
    int an = 0;

    @ObfuscatedName("ai")
    public boolean ai = false;

    @ObfuscatedName("al")
    boolean al = false;

    @ObfuscatedName("at")
    public int at = -1;

    @ObfuscatedName("ag")
    public int[] innerObjectIds;

    @ObfuscatedName("as")
    int innerObjectExpressionId = -1;

    @ObfuscatedName("aw")
    int innerObjectVarpIndex = -1;

    @ObfuscatedName("aq")
    public int aq = -1;

    @ObfuscatedName("aa")
    public int aa = 0;

    @ObfuscatedName("af")
    public int af = 0;

    @ObfuscatedName("ak")
    public int ak = 0;

    @ObfuscatedName("ab")
    public int[] ab;

    @ObfuscatedName("ac")
    FixedSizeDeque ac;

    @ObfuscatedName("i")
    void i() {
        if (this.d == -1) {
            this.d = 0;
            if (this.g != null && (this.n == null || this.n[0] == 10)) {
                this.d = 1;
            }

            for (int var1 = 0; var1 < 5; ++var1) {
                if (this.actions[var1] != null) {
                    this.d = 1;
                }
            }
        }

        if (this.at == -1) {
            this.at = this.w != 0 ? 1 : 0;
        }

    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.l(var1, var2);
        }
    }

    @ObfuscatedName("l")
    void l(ByteBuffer var1, int var2) {
        int var3;
        int var4;
        if (var2 == 1) {
            var3 = var1.av();
            if (var3 > 0) {
                if (this.g != null && !t) {
                    var1.index += 3 * var3;
                } else {
                    this.n = new int[var3];
                    this.g = new int[var3];

                    for (var4 = 0; var4 < var3; ++var4) {
                        this.g[var4] = var1.ae();
                        this.n[var4] = var1.av();
                    }
                }
            }
        } else if (var2 == 2) {
            this.name = var1.ar();
        } else if (var2 == 5) {
            var3 = var1.av();
            if (var3 > 0) {
                if (this.g != null && !t) {
                    var1.index += var3 * 2;
                } else {
                    this.n = null;
                    this.g = new int[var3];

                    for (var4 = 0; var4 < var3; ++var4) {
                        this.g[var4] = var1.ae();
                    }
                }
            }
        } else if (var2 == 14) {
            this.width = var1.av();
        } else if (var2 == 15) {
            this.height = var1.av();
        } else if (var2 == 17) {
            this.w = 0;
            this.s = false;
        } else if (var2 == 18) {
            this.s = false;
        } else if (var2 == 19) {
            this.d = var1.av();
        } else if (var2 == 21) {
            this.clipType = 0;
        } else if (var2 == 22) {
            this.r = true;
        } else if (var2 == 23) {
            this.clipped = true;
        } else if (var2 == 24) {
            this.h = var1.ae();
            if (this.h == 65535) {
                this.h = -1;
            }
        } else if (var2 == 27) {
            this.w = 1;
        } else if (var2 == 28) {
            this.m = var1.av();
        } else if (var2 == 29) {
            this.ay = var1.aj();
        } else if (var2 == 39) {
            this.ao = var1.aj();
        } else if (var2 >= 30 && var2 < 35) {
            this.actions[var2 - 30] = var1.ar();
            if (this.actions[var2 - 30].equalsIgnoreCase("Hidden")) {
                this.actions[var2 - 30] = null;
            }
        } else if (var2 == 40) {
            var3 = var1.av();
            this.baseColors = new short[var3];
            this.texturedColors = new short[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.baseColors[var4] = (short) var1.ae();
                this.texturedColors[var4] = (short) var1.ae();
            }
        } else if (var2 == 41) {
            var3 = var1.av();
            this.u = new short[var3];
            this.j = new short[var3];

            for (var4 = 0; var4 < var3; ++var4) {
                this.u[var4] = (short) var1.ae();
                this.j[var4] = (short) var1.ae();
            }
        } else if (var2 == 62) {
            this.am = true;
        } else if (var2 == 64) {
            this.az = false;
        } else if (var2 == 65) {
            this.ap = var1.ae();
        } else if (var2 == 66) {
            this.ah = var1.ae();
        } else if (var2 == 67) {
            this.au = var1.ae();
        } else if (var2 == 68) {
            this.ae = var1.ae();
        } else if (var2 == 69) {
            var1.av();
        } else if (var2 == 70) {
            this.ax = var1.am();
        } else if (var2 == 71) {
            this.ar = var1.am();
        } else if (var2 == 72) {
            this.an = var1.am();
        } else if (var2 == 73) {
            this.ai = true;
        } else if (var2 == 74) {
            this.al = true;
        } else if (var2 == 75) {
            this.at = var1.av();
        } else if (var2 != 77 && var2 != 92) {
            if (var2 == 78) {
                this.aq = var1.ae();
                this.aa = var1.av();
            } else if (var2 == 79) {
                this.af = var1.ae();
                this.ak = var1.ae();
                this.aa = var1.av();
                var3 = var1.av();
                this.ab = new int[var3];

                for (var4 = 0; var4 < var3; ++var4) {
                    this.ab[var4] = var1.ae();
                }
            } else if (var2 == 81) {
                this.clipType = var1.av() * 256;
            } else if (var2 == 82) {
                this.mapFunction = var1.ae();
            } else if (var2 == 249) {
                this.ac = i.t(var1, this.ac);
            }
        } else {
            this.innerObjectExpressionId = var1.ae();
            if (this.innerObjectExpressionId == 65535) {
                this.innerObjectExpressionId = -1;
            }

            this.innerObjectVarpIndex = var1.ae();
            if (this.innerObjectVarpIndex == 65535) {
                this.innerObjectVarpIndex = -1;
            }

            var3 = -1;
            if (var2 == 92) {
                var3 = var1.ae();
                if (var3 == 65535) {
                    var3 = -1;
                }
            }

            var4 = var1.av();
            this.innerObjectIds = new int[var4 + 2];

            for (int var5 = 0; var5 <= var4; ++var5) {
                this.innerObjectIds[var5] = var1.ae();
                if (this.innerObjectIds[var5] == 65535) {
                    this.innerObjectIds[var5] = -1;
                }
            }

            this.innerObjectIds[var4 + 1] = var3;
        }

    }

    @ObfuscatedName("b")
    public final boolean b(int var1) {
        if (this.n != null) {
            for (int var4 = 0; var4 < this.n.length; ++var4) {
                if (this.n[var4] == var1) {
                    return f.i.l(this.g[var4] & '\uffff', 0);
                }
            }

            return true;
        } else if (this.g == null) {
            return true;
        } else if (var1 != 10) {
            return true;
        } else {
            boolean var2 = true;

            for (int var3 = 0; var3 < this.g.length; ++var3) {
                var2 &= f.i.l(this.g[var3] & '\uffff', 0);
            }

            return var2;
        }
    }

    @ObfuscatedName("e")
    public final boolean e() {
        if (this.g == null) {
            return true;
        } else {
            boolean var1 = true;

            for (int var2 = 0; var2 < this.g.length; ++var2) {
                var1 &= f.i.l(this.g[var2] & '\uffff', 0);
            }

            return var1;
        }
    }

    @ObfuscatedName("x")
    public final Renderable x(int var1, int var2, int[][] var3, int var4, int var5, int var6) {
        long var7;
        if (this.n == null) {
            var7 = (long) (var2 + (this.id << 10));
        } else {
            var7 = (long) (var2 + (var1 << 3) + (this.id << 10));
        }

        Object var9 = (Renderable) b.t(var7);
        if (var9 == null) {
            AlternativeModel var10 = this.c(var1, var2);
            if (var10 == null) {
                return null;
            }

            if (!this.r) {
                var9 = var10.av(this.ay + 64, this.ao * 25 + 768, -50, -10, -50);
            } else {
                var10.ah = (short) (this.ay + 64);
                var10.au = (short) (this.ao * 25 + 768);
                var10.f();
                var9 = var10;
            }

            b.i((hh) var9, var7);
        }

        if (this.r) {
            var9 = ((AlternativeModel) var9).l();
        }

        if (this.clipType >= 0) {
            if (var9 instanceof Model) {
                var9 = ((Model) var9).t(var3, var4, var5, var6, true, this.clipType);
            } else if (var9 instanceof AlternativeModel) {
                var9 = ((AlternativeModel) var9).b(var3, var4, var5, var6, true, this.clipType);
            }
        }

        return (Renderable) var9;
    }

    @ObfuscatedName("p")
    public final Model p(int var1, int var2, int[][] var3, int var4, int var5, int var6) {
        long var7;
        if (this.n == null) {
            var7 = (long) (var2 + (this.id << 10));
        } else {
            var7 = (long) (var2 + (var1 << 3) + (this.id << 10));
        }

        Model var9 = (Model) e.t(var7);
        if (var9 == null) {
            AlternativeModel var10 = this.c(var1, var2);
            if (var10 == null) {
                return null;
            }

            var9 = var10.av(this.ay + 64, this.ao * 25 + 768, -50, -10, -50);
            e.i(var9, var7);
        }

        if (this.clipType >= 0) {
            var9 = var9.t(var3, var4, var5, var6, true, this.clipType);
        }

        return var9;
    }

    @ObfuscatedName("o")
    public final Model o(int var1, int var2, int[][] var3, int var4, int var5, int var6, kf var7, int var8) {
        long var9;
        if (this.n == null) {
            var9 = (long) (var2 + (this.id << 10));
        } else {
            var9 = (long) (var2 + (var1 << 3) + (this.id << 10));
        }

        Model var11 = (Model) e.t(var9);
        if (var11 == null) {
            AlternativeModel var12 = this.c(var1, var2);
            if (var12 == null) {
                return null;
            }

            var11 = var12.av(this.ay + 64, this.ao * 25 + 768, -50, -10, -50);
            e.i(var11, var9);
        }

        if (var7 == null && this.clipType == -1) {
            return var11;
        } else {
            if (var7 != null) {
                var11 = var7.b(var11, var8, var2);
            } else {
                var11 = var11.q(true);
            }

            if (this.clipType >= 0) {
                var11 = var11.t(var3, var4, var5, var6, false, this.clipType);
            }

            return var11;
        }
    }

    @ObfuscatedName("c")
    final AlternativeModel c(int var1, int var2) {
        AlternativeModel var3 = null;
        boolean var4;
        int var5;
        int var7;
        if (this.n == null) {
            if (var1 != 10) {
                return null;
            }

            if (this.g == null) {
                return null;
            }

            var4 = this.am;
            if (var1 == 2 && var2 > 3) {
                var4 = !var4;
            }

            var5 = this.g.length;

            for (int var6 = 0; var6 < var5; ++var6) {
                var7 = this.g[var6];
                if (var4) {
                    var7 += 65536;
                }

                var3 = (AlternativeModel) l.t((long) var7);
                if (var3 == null) {
                    var3 = AlternativeModel.t(f.i, var7 & '\uffff', 0);
                    if (var3 == null) {
                        return null;
                    }

                    if (var4) {
                        var3.s();
                    }

                    l.i(var3, (long) var7);
                }

                if (var5 > 1) {
                    x[var6] = var3;
                }
            }

            if (var5 > 1) {
                var3 = new AlternativeModel(x, var5);
            }
        } else {
            int var9 = -1;

            for (var5 = 0; var5 < this.n.length; ++var5) {
                if (this.n[var5] == var1) {
                    var9 = var5;
                    break;
                }
            }

            if (var9 == -1) {
                return null;
            }

            var5 = this.g[var9];
            boolean var10 = this.am ^ var2 > 3;
            if (var10) {
                var5 += 65536;
            }

            var3 = (AlternativeModel) l.t((long) var5);
            if (var3 == null) {
                var3 = AlternativeModel.t(f.i, var5 & '\uffff', 0);
                if (var3 == null) {
                    return null;
                }

                if (var10) {
                    var3.s();
                }

                l.i(var3, (long) var5);
            }
        }

        if (this.ap == 128 && this.ah == 128 && this.au == 128) {
            var4 = false;
        } else {
            var4 = true;
        }

        boolean var11;
        if (this.ax == 0 && this.ar == 0 && this.an == 0) {
            var11 = false;
        } else {
            var11 = true;
        }

        AlternativeModel var8 = new AlternativeModel(var3, var2 == 0 && !var4 && !var11, null == this.baseColors,
                this.u == null, true);
        if (var1 == 4 && var2 > 3) {
            var8.u(256);
            var8.k(45, 0, -45);
        }

        var2 &= 3;
        if (var2 == 1) {
            var8.x();
        } else if (var2 == 2) {
            var8.o();
        } else if (var2 == 3) {
            var8.c();
        }

        if (this.baseColors != null) {
            for (var7 = 0; var7 < this.baseColors.length; ++var7) {
                var8.z(this.baseColors[var7], this.texturedColors[var7]);
            }
        }

        if (this.u != null) {
            for (var7 = 0; var7 < this.u.length; ++var7) {
                var8.w(this.u[var7], this.j[var7]);
            }
        }

        if (var4) {
            var8.d(this.ap, this.ah, this.au);
        }

        if (var11) {
            var8.k(this.ax, this.ar, this.an);
        }

        return var8;
    }

    @ObfuscatedName("u")
    public final ObjectDefinition u() {
        int var1 = -1;
        if (this.innerObjectExpressionId != -1) {
            var1 = Server.t(this.innerObjectExpressionId);
        } else if (this.innerObjectVarpIndex != -1) {
            var1 = iv.varps[this.innerObjectVarpIndex];
        }

        int var2;
        if (var1 >= 0 && var1 < this.innerObjectIds.length - 1) {
            var2 = this.innerObjectIds[var1];
        } else {
            var2 = this.innerObjectIds[this.innerObjectIds.length - 1];
        }

        return var2 != -1 ? jw.q(var2) : null;
    }

    @ObfuscatedName("k")
    public int k(int var1, int var2) {
        return ky.q(this.ac, var1, var2);
    }

    @ObfuscatedName("z")
    public String z(int var1, String var2) {
        FixedSizeDeque var4 = this.ac;
        String var3;
        if (var4 == null) {
            var3 = var2;
        } else {
            gy var5 = (gy) var4.t((long) var1);
            if (var5 == null) {
                var3 = var2;
            } else {
                var3 = (String) var5.t;
            }
        }

        return var3;
    }

    @ObfuscatedName("w")
    public boolean w() {
        if (this.innerObjectIds == null) {
            return this.aq != -1 || this.ab != null;
        } else {
            for (int var1 = 0; var1 < this.innerObjectIds.length; ++var1) {
                if (this.innerObjectIds[var1] != -1) {
                    ObjectDefinition var2 = jw.q(this.innerObjectIds[var1]);
                    if (var2.aq != -1 || var2.ab != null) {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
