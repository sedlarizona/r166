import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("y")
public class y {

    @ObfuscatedName("fy")
    static Sprite fy;

    @ObfuscatedName("je")
    static int menuWidth;

    @ObfuscatedName("gl")
    static final void gl(boolean var0, Packet var1) {
        Client.dynamicRegion = var0;
        int var3;
        int var4;
        int var5;
        int var6;
        int var7;
        int var8;
        if (!Client.dynamicRegion) {
            int var2 = var1.bb();
            var3 = var1.bz();
            var4 = var1.ae();
            DevelopmentStage.fe = new int[var4][4];

            for (var5 = 0; var5 < var4; ++var5) {
                for (var6 = 0; var6 < 4; ++var6) {
                    DevelopmentStage.fe[var5][var6] = var1.ap();
                }
            }

            FileSystem.fg = new int[var4];
            bq.fp = new int[var4];
            AnimableObject.ff = new int[var4];
            gx.fk = new byte[var4][];
            cs.locationFileBytes = new byte[var4][];
            boolean var16 = false;
            if ((var3 / 8 == 48 || var3 / 8 == 49) && var2 / 8 == 48) {
                var16 = true;
            }

            if (var3 / 8 == 48 && var2 / 8 == 148) {
                var16 = true;
            }

            var4 = 0;

            for (var6 = (var3 - 6) / 8; var6 <= (var3 + 6) / 8; ++var6) {
                for (var7 = (var2 - 6) / 8; var7 <= (var2 + 6) / 8; ++var7) {
                    var8 = var7 + (var6 << 8);
                    if (!var16 || var7 != 49 && var7 != 149 && var7 != 147 && var6 != 50 && (var6 != 49 || var7 != 47)) {
                        FileSystem.fg[var4] = var8;
                        bq.fp[var4] = ee.cn.h("m" + var6 + "_" + var7);
                        AnimableObject.ff[var4] = ee.cn.h("l" + var6 + "_" + var7);
                        ++var4;
                    }
                }
            }

            bn.gz(var3, var2, true);
        } else {
            boolean var15 = var1.be() == 1;
            var3 = var1.bz();
            var4 = var1.ae();
            var5 = var1.ae();
            var1.jc();

            int var9;
            for (var6 = 0; var6 < 4; ++var6) {
                for (var7 = 0; var7 < 13; ++var7) {
                    for (var8 = 0; var8 < 13; ++var8) {
                        var9 = var1.jt(1);
                        if (var9 == 1) {
                            Client.dynamicRegionFlags[var6][var7][var8] = var1.jt(26);
                        } else {
                            Client.dynamicRegionFlags[var6][var7][var8] = -1;
                        }
                    }
                }
            }

            var1.ju();
            DevelopmentStage.fe = new int[var5][4];

            for (var6 = 0; var6 < var5; ++var6) {
                for (var7 = 0; var7 < 4; ++var7) {
                    DevelopmentStage.fe[var6][var7] = var1.ap();
                }
            }

            FileSystem.fg = new int[var5];
            bq.fp = new int[var5];
            AnimableObject.ff = new int[var5];
            gx.fk = new byte[var5][];
            cs.locationFileBytes = new byte[var5][];
            var5 = 0;

            for (var6 = 0; var6 < 4; ++var6) {
                for (var7 = 0; var7 < 13; ++var7) {
                    for (var8 = 0; var8 < 13; ++var8) {
                        var9 = Client.dynamicRegionFlags[var6][var7][var8];
                        if (var9 != -1) {
                            int var10 = var9 >> 14 & 1023;
                            int var11 = var9 >> 3 & 2047;
                            int var12 = (var10 / 8 << 8) + var11 / 8;

                            int var13;
                            for (var13 = 0; var13 < var5; ++var13) {
                                if (FileSystem.fg[var13] == var12) {
                                    var12 = -1;
                                    break;
                                }
                            }

                            if (var12 != -1) {
                                FileSystem.fg[var5] = var12;
                                var13 = var12 >> 8 & 255;
                                int var14 = var12 & 255;
                                bq.fp[var5] = ee.cn.h("m" + var13 + "_" + var14);
                                AnimableObject.ff[var5] = ee.cn.h("l" + var13 + "_" + var14);
                                ++var5;
                            }
                        }
                    }
                }
            }

            bn.gz(var4, var3, !var15);
        }

    }
}
