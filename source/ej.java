import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ej")
public class ej {

    @ObfuscatedName("t")
    public static boolean t = false;

    @ObfuscatedName("q")
    static int q = 0;

    @ObfuscatedName("i")
    static int i = 0;

    @ObfuscatedName("a")
    static boolean a = false;

    @ObfuscatedName("b")
    static int b;

    @ObfuscatedName("o")
    static int o;

    @ObfuscatedName("c")
    static int c;

    @ObfuscatedName("v")
    public static int v = 0;

    @ObfuscatedName("u")
    public static int[] u = new int[1000];

    @ObfuscatedName("fu")
    static final void fu() {
        Client.ee.l();
        fh.e();
        js.q.a();
        bk.x();
        ObjectDefinition.a.a();
        ObjectDefinition.l.a();
        ObjectDefinition.b.a();
        ObjectDefinition.e.a();
        jz.u();
        ItemDefinition.g.a();
        ItemDefinition.n.a();
        ItemDefinition.o.a();
        fr.c();
        jw.i.a();
        jw.a.a();
        gx.a();
        VarInfo.i.a();
        kv.c();
        CombatBarDefinition.i.a();
        CombatBarDefinition.a.a();
        bv.x();
        jp.q.a();
        az.e();
        gt.o();
        gg.z();
        ((dg) eu.r).e();
        RuneScript.t.a();
        Server.cy.r();
        MenuItemNode.cg.r();
        ly.cl.r();
        aj.cz.r();
        ee.cn.r();
        Varpbit.ca.r();
        iq.cf.r();
        au.cp.r();
        bt.ck.r();
        jk.db.r();
        bk.dp.r();
        p.da.r();
        an.loadedRegion.t();

        for (int var0 = 0; var0 < 4; ++var0) {
            Client.collisionMaps[var0].t();
        }

        System.gc();
        hi.audioTrackId = 1;
        hi.b = null;
        fm.e = -1;
        hi.x = -1;
        hi.p = 0;
        cr.n = false;
        i.g = 2;
        Client.ox = -1;
        Client.oe = false;

        for (AreaSoundEmitter var1 = (AreaSoundEmitter) AreaSoundEmitter.t.e(); var1 != null; var1 = (AreaSoundEmitter) AreaSoundEmitter.t
                .p()) {
            if (var1.p != null) {
                kq.pd.q(var1.p);
                var1.p = null;
            }

            if (var1.v != null) {
                kq.pd.q(var1.v);
                var1.v = null;
            }
        }

        AreaSoundEmitter.t.t();
        d.ea(10);
    }

    @ObfuscatedName("jo")
    static final SubWindow jo(int var0, int var1, int var2) {
        SubWindow var3 = new SubWindow();
        var3.targetWindowId = var1;
        var3.type = var2;
        Client.subWindowTable.q(var3, (long) var0);
        int var5;
        if (RuneScript.i(var1)) {
            RTComponent[] var4 = RTComponent.b[var1];

            for (var5 = 0; var5 < var4.length; ++var5) {
                RTComponent var9 = var4[var5];
                if (var9 != null) {
                    var9.et = 0;
                    var9.ev = 0;
                }
            }
        }

        RTComponent var10 = Inflater.t(var0);
        GameEngine.jk(var10);
        if (Client.ls != null) {
            GameEngine.jk(Client.ls);
            Client.ls = null;
        }

        for (var5 = 0; var5 < Client.menuSize; ++var5) {
            int var7 = Client.menuOpcodes[var5];
            boolean var6 = var7 == 57 || var7 == 58 || var7 == 1007 || var7 == 25 || var7 == 30;
            if (var6) {
                if (var5 < Client.menuSize - 1) {
                    for (int var8 = var5; var8 < Client.menuSize - 1; ++var8) {
                        Client.menuActions[var8] = Client.menuActions[var8 + 1];
                        Client.menuTargets[var8] = Client.menuTargets[var8 + 1];
                        Client.menuOpcodes[var8] = Client.menuOpcodes[var8 + 1];
                        Client.ka[var8] = Client.ka[var8 + 1];
                        Client.menuArg1[var8] = Client.menuArg1[var8 + 1];
                        Client.menuArg2[var8] = Client.menuArg2[var8 + 1];
                        Client.kc[var8] = Client.kc[var8 + 1];
                    }
                }

                --Client.menuSize;
            }
        }

        GameEngine.ig(RTComponent.b[var0 >> 16], var10, false);
        az.aj(var1);
        if (Client.hudIndex != -1) {
            g.iz(Client.hudIndex, 1);
        }

        return var3;
    }
}
