import java.util.concurrent.ScheduledExecutorService;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bp")
public class CombatBarData extends Node {

    @ObfuscatedName("e")
    public static int[] e;

    @ObfuscatedName("x")
    static ScheduledExecutorService x;

    @ObfuscatedName("t")
    int initialCycle;

    @ObfuscatedName("q")
    int initialWidth;

    @ObfuscatedName("i")
    int currentWidth;

    @ObfuscatedName("a")
    int currentCycle;

    CombatBarData(int var1, int var2, int var3, int var4) {
        this.initialCycle = var1;
        this.initialWidth = var2;
        this.currentWidth = var3;
        this.currentCycle = var4;
    }

    @ObfuscatedName("t")
    void t(int var1, int var2, int var3, int var4) {
        this.initialCycle = var1;
        this.initialWidth = var2;
        this.currentWidth = var3;
        this.currentCycle = var4;
    }

    @ObfuscatedName("q")
    public static jl q(int var0) {
        jl var1 = (jl) jl.q.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = jl.t.i(34, var0);
            var1 = new jl();
            if (var2 != null) {
                var1.a(new ByteBuffer(var2));
            }

            var1.i();
            jl.q.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("i")
    public static void i(FileSystem var0, int var1, int var2, int var3, boolean var4) {
        hi.audioTrackId = 1;
        hi.b = var0;
        fm.e = var1;
        hi.x = var2;
        hi.p = var3;
        cr.n = var4;
        i.g = 10000;
    }

    @ObfuscatedName("y")
    static int y(int var0, RuneScript var1, boolean var2) {
        if (var0 == 6200) {
            b.menuX -= 2;
            Client.qw = (short) cs.e[b.menuX];
            if (Client.qw <= 0) {
                Client.qw = 256;
            }

            Client.qk = (short) cs.e[b.menuX + 1];
            if (Client.qk <= 0) {
                Client.qk = 205;
            }

            return 1;
        } else if (var0 == 6201) {
            b.menuX -= 2;
            Client.qr = (short) cs.e[b.menuX];
            if (Client.qr <= 0) {
                Client.qr = 256;
            }

            Client.qu = (short) cs.e[b.menuX + 1];
            if (Client.qu <= 0) {
                Client.qu = 320;
            }

            return 1;
        } else if (var0 == 6202) {
            b.menuX -= 4;
            Client.qm = (short) cs.e[b.menuX];
            if (Client.qm <= 0) {
                Client.qm = 1;
            }

            Client.qd = (short) cs.e[b.menuX + 1];
            if (Client.qd <= 0) {
                Client.qd = 32767;
            } else if (Client.qd < Client.qm) {
                Client.qd = Client.qm;
            }

            Client.qf = (short) cs.e[b.menuX + 2];
            if (Client.qf <= 0) {
                Client.qf = 1;
            }

            Client.qa = (short) cs.e[b.menuX + 3];
            if (Client.qa <= 0) {
                Client.qa = 32767;
            } else if (Client.qa < Client.qf) {
                Client.qa = Client.qf;
            }

            return 1;
        } else if (var0 == 6203) {
            if (Client.lz != null) {
                ap.gg(0, 0, Client.lz.width, Client.lz.height, false);
                cs.e[++b.menuX - 1] = Client.viewportWidth;
                cs.e[++b.menuX - 1] = Client.viewportHeight;
            } else {
                cs.e[++b.menuX - 1] = -1;
                cs.e[++b.menuX - 1] = -1;
            }

            return 1;
        } else if (var0 == 6204) {
            cs.e[++b.menuX - 1] = Client.qr;
            cs.e[++b.menuX - 1] = Client.qu;
            return 1;
        } else if (var0 == 6205) {
            cs.e[++b.menuX - 1] = Client.qw;
            cs.e[++b.menuX - 1] = Client.qk;
            return 1;
        } else {
            return 2;
        }
    }
}
