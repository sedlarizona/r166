import java.io.IOException;
import java.net.Socket;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fm")
public class fm extends fy {

    @ObfuscatedName("e")
    public static int e;

    @ObfuscatedName("t")
    Socket t;

    @ObfuscatedName("q")
    fl q;

    @ObfuscatedName("i")
    fj i;

    public fm(Socket var1, int var2, int var3) throws IOException {
        this.t = var1;
        this.t.setSoTimeout(30000);
        this.t.setTcpNoDelay(true);
        this.t.setReceiveBufferSize(65536);
        this.t.setSendBufferSize(65536);
        this.q = new fl(this.t.getInputStream(), var2);
        this.i = new fj(this.t.getOutputStream(), var3);
    }

    @ObfuscatedName("t")
    public boolean t(int var1)
            throws IOException {
        return this.q.t(var1);
    }

    @ObfuscatedName("q")
    public int q()
            throws IOException {
        return this.q.q();
    }

    @ObfuscatedName("i")
    public int i()
            throws IOException {
        return this.q.i();
    }

    @ObfuscatedName("a")
    public int a(byte[] var1, int var2, int var3)
            throws IOException {
        return this.q.a(var1, var2, var3);
    }

    @ObfuscatedName("l")
    public void l(byte[] var1, int var2, int var3)
            throws IOException {
        this.i.q(var1, var2, var3);
    }

    @ObfuscatedName("b")
    public void b() {
        this.i.i();

        try {
            this.t.close();
        } catch (IOException var2) {
            ;
        }

        this.q.l();
    }

    protected void finalize() {
        this.b();
    }

    @ObfuscatedName("go")
    static void go() {
        if (az.il.regionX >> 7 == Client.destinationX && az.il.regionY >> 7 == Client.destinationY) {
            Client.destinationX = 0;
        }

    }
}
