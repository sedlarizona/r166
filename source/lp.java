import java.util.Random;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lp")
public class lp extends Node {

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    boolean e;

    @ObfuscatedName("ay")
    int ay;

    @ObfuscatedName("az")
    int az;

    @ObfuscatedName("ap")
    int ap;

    @ObfuscatedName("ah")
    int ah;

    @ObfuscatedName("au")
    int au;

    @ObfuscatedName("ax")
    boolean ax;

    @ObfuscatedName("ar")
    int ar;

    @ObfuscatedName("ai")
    int ai;

    @ObfuscatedName("at")
    int at;

    @ObfuscatedName("ag")
    int ag;

    @ObfuscatedName("as")
    String as;

    @ObfuscatedName("aw")
    String aw;

    @ObfuscatedName("aq")
    String aq;

    @ObfuscatedName("aa")
    String aa;

    @ObfuscatedName("af")
    int af;

    @ObfuscatedName("ak")
    int ak;

    @ObfuscatedName("ab")
    int ab;

    @ObfuscatedName("ac")
    int ac;

    @ObfuscatedName("ad")
    String ad;

    @ObfuscatedName("bg")
    String bg;

    @ObfuscatedName("br")
    int[] br = new int[3];

    @ObfuscatedName("ba")
    int ba;

    public lp(boolean var1) {
        if (fh.d.startsWith("win")) {
            this.b = 1;
        } else if (fh.d.startsWith("mac")) {
            this.b = 2;
        } else if (fh.d.startsWith("linux")) {
            this.b = 3;
        } else {
            this.b = 4;
        }

        String var2;
        try {
            var2 = System.getProperty("os.arch").toLowerCase();
        } catch (Exception var13) {
            var2 = "";
        }

        String var3;
        try {
            var3 = System.getProperty("os.version").toLowerCase();
        } catch (Exception var12) {
            var3 = "";
        }

        String var4 = "Unknown";
        String var5 = "1.1";

        try {
            var4 = System.getProperty("java.vendor");
            var5 = System.getProperty("java.version");
        } catch (Exception var11) {
            ;
        }

        if (!var2.startsWith("amd64") && !var2.startsWith("x86_64")) {
            this.e = false;
        } else {
            this.e = true;
        }

        if (this.b == 1) {
            if (var3.indexOf("4.0") != -1) {
                this.ay = 1;
            } else if (var3.indexOf("4.1") != -1) {
                this.ay = 2;
            } else if (var3.indexOf("4.9") != -1) {
                this.ay = 3;
            } else if (var3.indexOf("5.0") != -1) {
                this.ay = 4;
            } else if (var3.indexOf("5.1") != -1) {
                this.ay = 5;
            } else if (var3.indexOf("5.2") != -1) {
                this.ay = 8;
            } else if (var3.indexOf("6.0") != -1) {
                this.ay = 6;
            } else if (var3.indexOf("6.1") != -1) {
                this.ay = 7;
            } else if (var3.indexOf("6.2") != -1) {
                this.ay = 9;
            } else if (var3.indexOf("6.3") != -1) {
                this.ay = 10;
            } else if (var3.indexOf("10.0") != -1) {
                this.ay = 11;
            }
        } else if (this.b == 2) {
            if (var3.indexOf("10.4") != -1) {
                this.ay = 20;
            } else if (var3.indexOf("10.5") != -1) {
                this.ay = 21;
            } else if (var3.indexOf("10.6") != -1) {
                this.ay = 22;
            } else if (var3.indexOf("10.7") != -1) {
                this.ay = 23;
            } else if (var3.indexOf("10.8") != -1) {
                this.ay = 24;
            } else if (var3.indexOf("10.9") != -1) {
                this.ay = 25;
            } else if (var3.indexOf("10.10") != -1) {
                this.ay = 26;
            } else if (var3.indexOf("10.11") != -1) {
                this.ay = 27;
            }
        }

        if (var4.toLowerCase().indexOf("sun") != -1) {
            this.az = 1;
        } else if (var4.toLowerCase().indexOf("microsoft") != -1) {
            this.az = 2;
        } else if (var4.toLowerCase().indexOf("apple") != -1) {
            this.az = 3;
        } else if (var4.toLowerCase().indexOf("oracle") != -1) {
            this.az = 5;
        } else {
            this.az = 4;
        }

        int var9 = 2;
        int var7 = 0;

        char var8;
        try {
            while (var9 < var5.length()) {
                var8 = var5.charAt(var9);
                if (var8 < '0' || var8 > '9') {
                    break;
                }

                var7 = var8 - 48 + var7 * 10;
                ++var9;
            }
        } catch (Exception var16) {
            ;
        }

        this.ap = var7;
        var9 = var5.indexOf(46, 2) + 1;
        var7 = 0;

        try {
            while (var9 < var5.length()) {
                var8 = var5.charAt(var9);
                if (var8 < '0' || var8 > '9') {
                    break;
                }

                var7 = var8 - 48 + var7 * 10;
                ++var9;
            }
        } catch (Exception var15) {
            ;
        }

        this.ah = var7;
        var9 = var5.indexOf(95, 4) + 1;
        var7 = 0;

        try {
            while (var9 < var5.length()) {
                var8 = var5.charAt(var9);
                if (var8 < '0' || var8 > '9') {
                    break;
                }

                var7 = var8 - 48 + var7 * 10;
                ++var9;
            }
        } catch (Exception var14) {
            ;
        }

        this.au = var7;
        this.ax = false;
        Runtime.getRuntime();
        this.ar = (int) ((long) ((new Random()).nextInt(31457280) + 230686720) / 1048576L) + 1;
        if (this.ap > 3) {
            this.ai = Runtime.getRuntime().availableProcessors();
        } else {
            this.ai = 0;
        }

        this.at = 0;
        if (this.as == null) {
            this.as = "";
        }

        if (this.aw == null) {
            this.aw = "";
        }

        if (this.aq == null) {
            this.aq = "";
        }

        if (this.aa == null) {
            this.aa = "";
        }

        if (this.ad == null) {
            this.ad = "";
        }

        if (this.bg == null) {
            this.bg = "";
        }

        this.t();
    }

    @ObfuscatedName("t")
    void t() {
        if (this.as.length() > 40) {
            this.as = this.as.substring(0, 40);
        }

        if (this.aw.length() > 40) {
            this.aw = this.aw.substring(0, 40);
        }

        if (this.aq.length() > 10) {
            this.aq = this.aq.substring(0, 10);
        }

        if (this.aa.length() > 10) {
            this.aa = this.aa.substring(0, 10);
        }

    }

    @ObfuscatedName("q")
    public void q(ByteBuffer var1) {
        var1.a(6);
        var1.a(this.b);
        var1.a(this.e ? 1 : 0);
        var1.a(this.ay);
        var1.a(this.az);
        var1.a(this.ap);
        var1.a(this.ah);
        var1.a(this.au);
        var1.a(this.ax ? 1 : 0);
        var1.l(this.ar);
        var1.a(this.ai);
        var1.b(this.at);
        var1.l(this.ag);
        var1.z(this.as);
        var1.z(this.aw);
        var1.z(this.aq);
        var1.z(this.aa);
        var1.a(this.ak);
        var1.l(this.af);
        var1.z(this.ad);
        var1.z(this.bg);
        var1.a(this.ab);
        var1.a(this.ac);

        for (int var2 = 0; var2 < this.br.length; ++var2) {
            var1.e(this.br[var2]);
        }

        var1.e(this.ba);
    }

    @ObfuscatedName("i")
    public int i() {
        byte var1 = 38;
        int var2 = var1 + iy.k(this.as);
        var2 += iy.k(this.aw);
        var2 += iy.k(this.aq);
        var2 += iy.k(this.aa);
        var2 += iy.k(this.ad);
        var2 += iy.k(this.bg);
        return var2;
    }
}
