import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ke")
public class ke extends ku {

    @ObfuscatedName("i")
    final lu i;

    @ObfuscatedName("g")
    int g = 1;

    @ObfuscatedName("n")
    public LinkDeque n = new LinkDeque();

    public ke(lu var1) {
        super(400);
        this.i = var1;
    }

    @ObfuscatedName("t")
    kv t() {
        return new ks();
    }

    @ObfuscatedName("q")
    kv[] q(int var1) {
        return new ks[var1];
    }

    @ObfuscatedName("i")
    public boolean i(kb var1, boolean var2) {
        ks var3 = (ks) this.w(var1);
        if (var3 == null) {
            return false;
        } else {
            return !var2 || var3.l != 0;
        }
    }

    @ObfuscatedName("p")
    public void p(ByteBuffer var1, int var2) {
        while (true) {
            if (var1.index < var2) {
                boolean var3 = var1.av() == 1;
                kb var4 = new kb(var1.ar(), this.i);
                kb var5 = new kb(var1.ar(), this.i);
                int var6 = var1.ae();
                int var7 = var1.av();
                int var8 = var1.av();
                boolean var9 = (var8 & 2) != 0;
                boolean var10 = (var8 & 1) != 0;
                if (var6 > 0) {
                    var1.ar();
                    var1.av();
                    var1.ap();
                }

                var1.ar();
                if (var4 != null && var4.q()) {
                    ks var11 = (ks) this.s(var3 ? var5 : var4);
                    if (var11 != null) {
                        this.ae(var11, var4, var5);
                        if (var6 != var11.l) {
                            boolean var12 = true;

                            for (kr var13 = (kr) this.n.q(); var13 != null; var13 = (kr) this.n.i()) {
                                if (var13.a.equals(var4)) {
                                    if (var6 != 0 && var13.l == 0) {
                                        var13.t();
                                        var12 = false;
                                    } else if (var6 == 0 && var13.l != 0) {
                                        var13.t();
                                        var12 = false;
                                    }
                                }
                            }

                            if (var12) {
                                this.n.t(new kr(var4, var6));
                            }
                        }
                    } else {
                        if (this.u() >= 400) {
                            continue;
                        }

                        var11 = (ks) this.h(var4, var5);
                    }

                    if (var6 != var11.l) {
                        var11.b = ++this.g - 1;
                        if (var11.l == -1 && var6 == 0) {
                            var11.b = -(var11.b * 410700773) * -1324713491;
                        }

                        var11.l = var6;
                    }

                    var11.e = var7;
                    var11.t = var9;
                    var11.q = var10;
                    continue;
                }

                throw new IllegalStateException();
            }

            this.aj();
            return;
        }
    }

    @ObfuscatedName("x")
    static lk[] x() {
        lk[] var0 = new lk[le.t];

        for (int var1 = 0; var1 < le.t; ++var1) {
            lk var2 = var0[var1] = new lk();
            var2.e = le.q;
            var2.x = le.i;
            var2.l = ci.a[var1];
            var2.b = er.l[var1];
            var2.i = le.b[var1];
            var2.a = GrandExchangeOffer.e[var1];
            var2.q = le.x;
            var2.t = ClassStructureNode.p[var1];
        }

        ly.z();
        return var0;
    }
}
