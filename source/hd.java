import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hd")
public class hd {

    @ObfuscatedName("g")
    static int g;

    @ObfuscatedName("aj")
    static int[] aj;

    @ObfuscatedName("t")
    byte[] t;

    @ObfuscatedName("q")
    byte[] q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("fi")
    static final void fi() {
        int var0 = cx.b;
        int[] var1 = cx.e;

        for (int var2 = 0; var2 < var0; ++var2) {
            Player var3 = Client.loadedPlayers[var1[var2]];
            if (var3 != null) {
                AppletParameter.fj(var3, 1);
            }
        }

    }
}
