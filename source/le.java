import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("le")
public class le {

    @ObfuscatedName("t")
    static int t;

    @ObfuscatedName("q")
    static int q;

    @ObfuscatedName("i")
    static int i;

    @ObfuscatedName("b")
    static int[] b;

    @ObfuscatedName("x")
    static int[] x;
}
