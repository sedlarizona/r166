import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("aj")
public final class aj {

    @ObfuscatedName("cz")
    static ju cz;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    // $FF: synthetic field
    final ag this$0;

    aj(ag var1) {
        this.this$0 = var1;
    }

    @ObfuscatedName("t")
    public static gl t(gl[] var0, int var1) {
        gl[] var2 = var0;

        for (int var3 = 0; var3 < var2.length; ++var3) {
            gl var4 = var2[var3];
            if (var1 == var4.t()) {
                return var4;
            }
        }

        return null;
    }

    @ObfuscatedName("t")
    public static int t(CharSequence var0, CharSequence var1, int var2) {
        int var3 = var0.length();
        int var4 = var1.length();
        int var5 = 0;
        int var6 = 0;
        char var7 = 0;
        char var8 = 0;

        while (var5 - var7 < var3 || var6 - var8 < var4) {
            if (var5 - var7 >= var3) {
                return -1;
            }

            if (var6 - var8 >= var4) {
                return 1;
            }

            char var9;
            if (var7 != 0) {
                var9 = var7;
                boolean var14 = false;
            } else {
                var9 = var0.charAt(var5++);
            }

            char var10;
            if (var8 != 0) {
                var10 = var8;
                boolean var15 = false;
            } else {
                var10 = var1.charAt(var6++);
            }

            var7 = RuneScriptStackItem.i(var9);
            var8 = RuneScriptStackItem.i(var10);
            var9 = fz.q(var9, var2);
            var10 = fz.q(var10, var2);
            if (var10 != var9 && java.lang.Character.toUpperCase(var9) != java.lang.Character.toUpperCase(var10)) {
                var9 = java.lang.Character.toLowerCase(var9);
                var10 = java.lang.Character.toLowerCase(var10);
                if (var10 != var9) {
                    return TaskHandler.a(var9, var2) - TaskHandler.a(var10, var2);
                }
            }
        }

        int var16 = Math.min(var3, var4);

        char var12;
        int var17;
        for (var17 = 0; var17 < var16; ++var17) {
            char var11 = var0.charAt(var17);
            var12 = var1.charAt(var17);
            if (var11 != var12 && java.lang.Character.toUpperCase(var11) != java.lang.Character.toUpperCase(var12)) {
                var11 = java.lang.Character.toLowerCase(var11);
                var12 = java.lang.Character.toLowerCase(var12);
                if (var11 != var12) {
                    return TaskHandler.a(var11, var2) - TaskHandler.a(var12, var2);
                }
            }
        }

        var17 = var3 - var4;
        if (var17 != 0) {
            return var17;
        } else {
            for (int var18 = 0; var18 < var16; ++var18) {
                var12 = var0.charAt(var18);
                char var13 = var1.charAt(var18);
                if (var12 != var13) {
                    return TaskHandler.a(var12, var2) - TaskHandler.a(var13, var2);
                }
            }

            return 0;
        }
    }

    @ObfuscatedName("h")
    static int h(int var0, RuneScript var1, boolean var2) {
        if (var0 == 6500) {
            cs.e[++b.menuX - 1] = ex.t() ? 1 : 0;
            return 1;
        } else {
            Server var3;
            if (var0 == 6501) {
                Server.e = 0;
                var3 = CombatBar.e();
                if (var3 != null) {
                    cs.e[++b.menuX - 1] = var3.number;
                    cs.e[++b.menuX - 1] = var3.type;
                    cs.p[++ly.g - 1] = var3.activity;
                    cs.e[++b.menuX - 1] = var3.location;
                    cs.e[++b.menuX - 1] = var3.population;
                    cs.p[++ly.g - 1] = var3.domain;
                } else {
                    cs.e[++b.menuX - 1] = -1;
                    cs.e[++b.menuX - 1] = 0;
                    cs.p[++ly.g - 1] = "";
                    cs.e[++b.menuX - 1] = 0;
                    cs.e[++b.menuX - 1] = 0;
                    cs.p[++ly.g - 1] = "";
                }

                return 1;
            } else if (var0 == 6502) {
                var3 = CombatBar.e();
                if (var3 != null) {
                    cs.e[++b.menuX - 1] = var3.number;
                    cs.e[++b.menuX - 1] = var3.type;
                    cs.p[++ly.g - 1] = var3.activity;
                    cs.e[++b.menuX - 1] = var3.location;
                    cs.e[++b.menuX - 1] = var3.population;
                    cs.p[++ly.g - 1] = var3.domain;
                } else {
                    cs.e[++b.menuX - 1] = -1;
                    cs.e[++b.menuX - 1] = 0;
                    cs.p[++ly.g - 1] = "";
                    cs.e[++b.menuX - 1] = 0;
                    cs.e[++b.menuX - 1] = 0;
                    cs.p[++ly.g - 1] = "";
                }

                return 1;
            } else {
                Server var4;
                int var5;
                int var7;
                if (var0 == 6506) {
                    var7 = cs.e[--b.menuX];
                    var4 = null;

                    for (var5 = 0; var5 < Server.b; ++var5) {
                        if (var7 == Server.l[var5].number) {
                            var4 = Server.l[var5];
                            break;
                        }
                    }

                    if (var4 != null) {
                        cs.e[++b.menuX - 1] = var4.number;
                        cs.e[++b.menuX - 1] = var4.type;
                        cs.p[++ly.g - 1] = var4.activity;
                        cs.e[++b.menuX - 1] = var4.location;
                        cs.e[++b.menuX - 1] = var4.population;
                        cs.p[++ly.g - 1] = var4.domain;
                    } else {
                        cs.e[++b.menuX - 1] = -1;
                        cs.e[++b.menuX - 1] = 0;
                        cs.p[++ly.g - 1] = "";
                        cs.e[++b.menuX - 1] = 0;
                        cs.e[++b.menuX - 1] = 0;
                        cs.p[++ly.g - 1] = "";
                    }

                    return 1;
                } else if (var0 == 6507) {
                    b.menuX -= 4;
                    var7 = cs.e[b.menuX];
                    boolean var10 = cs.e[b.menuX + 1] == 1;
                    var5 = cs.e[b.menuX + 2];
                    boolean var6 = cs.e[b.menuX + 3] == 1;
                    GrandExchangeOffer.a(var7, var10, var5, var6);
                    return 1;
                } else if (var0 != 6511) {
                    if (var0 == 6512) {
                        Client.kg = cs.e[--b.menuX] == 1;
                        return 1;
                    } else {
                        int var8;
                        jp var9;
                        if (var0 == 6513) {
                            b.menuX -= 2;
                            var7 = cs.e[b.menuX];
                            var8 = cs.e[b.menuX + 1];
                            var9 = cd.t(var8);
                            if (var9.l()) {
                                cs.p[++ly.g - 1] = ho.q(var7).c(var8, var9.l);
                            } else {
                                cs.e[++b.menuX - 1] = ho.q(var7).o(var8, var9.a);
                            }

                            return 1;
                        } else if (var0 == 6514) {
                            b.menuX -= 2;
                            var7 = cs.e[b.menuX];
                            var8 = cs.e[b.menuX + 1];
                            var9 = cd.t(var8);
                            if (var9.l()) {
                                cs.p[++ly.g - 1] = jw.q(var7).z(var8, var9.l);
                            } else {
                                cs.e[++b.menuX - 1] = jw.q(var7).k(var8, var9.a);
                            }

                            return 1;
                        } else if (var0 == 6515) {
                            b.menuX -= 2;
                            var7 = cs.e[b.menuX];
                            var8 = cs.e[b.menuX + 1];
                            var9 = cd.t(var8);
                            if (var9.l()) {
                                cs.p[++ly.g - 1] = cs.loadItemDefinition(var7).f(var8, var9.l);
                            } else {
                                cs.e[++b.menuX - 1] = cs.loadItemDefinition(var7).d(var8, var9.a);
                            }

                            return 1;
                        } else if (var0 == 6516) {
                            b.menuX -= 2;
                            var7 = cs.e[b.menuX];
                            var8 = cs.e[b.menuX + 1];
                            var9 = cd.t(var8);
                            if (var9.l()) {
                                cs.p[++ly.g - 1] = CombatBarData.q(var7).e(var8, var9.l);
                            } else {
                                cs.e[++b.menuX - 1] = CombatBarData.q(var7).b(var8, var9.a);
                            }

                            return 1;
                        } else if (var0 == 6518) {
                            cs.e[++b.menuX - 1] = 0;
                            return 1;
                        } else if (var0 == 6520) {
                            return 1;
                        } else {
                            return var0 == 6521 ? 1 : 2;
                        }
                    }
                } else {
                    var7 = cs.e[--b.menuX];
                    if (var7 >= 0 && var7 < Server.b) {
                        var4 = Server.l[var7];
                        cs.e[++b.menuX - 1] = var4.number;
                        cs.e[++b.menuX - 1] = var4.type;
                        cs.p[++ly.g - 1] = var4.activity;
                        cs.e[++b.menuX - 1] = var4.location;
                        cs.e[++b.menuX - 1] = var4.population;
                        cs.p[++ly.g - 1] = var4.domain;
                    } else {
                        cs.e[++b.menuX - 1] = -1;
                        cs.e[++b.menuX - 1] = 0;
                        cs.p[++ly.g - 1] = "";
                        cs.e[++b.menuX - 1] = 0;
                        cs.e[++b.menuX - 1] = 0;
                        cs.p[++ly.g - 1] = "";
                    }

                    return 1;
                }
            }
        }
    }
}
