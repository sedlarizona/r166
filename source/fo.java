import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fo")
public class fo implements fq {

    @ObfuscatedName("t")
    public static final fo t = new fo(0, 8);

    @ObfuscatedName("q")
    public static final fo q = new fo(1, -2);

    @ObfuscatedName("i")
    public static final fo i = new fo(2, 6);

    @ObfuscatedName("a")
    public static final fo a = new fo(3, 3);

    @ObfuscatedName("l")
    public static final fo l = new fo(4, -1);

    @ObfuscatedName("b")
    public static final fo b = new fo(5, 1);

    @ObfuscatedName("e")
    public static final fo e = new fo(6, 4);

    @ObfuscatedName("x")
    public static final fo x = new fo(7, 7);

    @ObfuscatedName("p")
    public static final fo p = new fo(8, 8);

    @ObfuscatedName("g")
    public static final fo g = new fo(9, 7);

    @ObfuscatedName("n")
    public static final fo n = new fo(10, 10);

    @ObfuscatedName("o")
    public static final fo o = new fo(11, -1);

    @ObfuscatedName("c")
    public static final fo c = new fo(12, 9);

    @ObfuscatedName("v")
    public static final fo v = new fo(13, 3);

    @ObfuscatedName("u")
    public static final fo u = new fo(14, 3);

    @ObfuscatedName("j")
    public static final fo j = new fo(15, 8);

    @ObfuscatedName("k")
    public static final fo k = new fo(16, 2);

    @ObfuscatedName("z")
    public static final fo z = new fo(17, 8);

    @ObfuscatedName("w")
    public static final fo w = new fo(18, 8);

    @ObfuscatedName("s")
    public static final fo s = new fo(19, 8);

    @ObfuscatedName("d")
    public static final fo d = new fo(20, 8);

    @ObfuscatedName("f")
    public static final fo f = new fo(21, 7);

    @ObfuscatedName("r")
    public static final fo r = new fo(22, 4);

    @ObfuscatedName("y")
    public static final fo y = new fo(23, 7);

    @ObfuscatedName("h")
    public static final fo h = new fo(24, 8);

    @ObfuscatedName("m")
    public static final fo m = new fo(25, 0);

    @ObfuscatedName("ay")
    public static final fo ay = new fo(26, 4);

    @ObfuscatedName("ao")
    public static final fo ao = new fo(27, 7);

    @ObfuscatedName("av")
    public static final fo av = new fo(28, 11);

    @ObfuscatedName("aj")
    public static final fo aj = new fo(29, 8);

    @ObfuscatedName("ae")
    public static final fo ae = new fo(30, 15);

    @ObfuscatedName("am")
    public static final fo am = new fo(31, 3);

    @ObfuscatedName("az")
    public static final fo az = new fo(32, 8);

    @ObfuscatedName("ap")
    public static final fo ap = new fo(33, 8);

    @ObfuscatedName("ah")
    public static final fo ah = new fo(34, 16);

    @ObfuscatedName("au")
    public static final fo au = new fo(35, 14);

    @ObfuscatedName("ax")
    public static final fo ax = new fo(36, 0);

    @ObfuscatedName("ar")
    public static final fo ar = new fo(37, 4);

    @ObfuscatedName("an")
    public static final fo an = new fo(38, 8);

    @ObfuscatedName("ai")
    public static final fo ai = new fo(39, -1);

    @ObfuscatedName("al")
    public static final fo al = new fo(40, 13);

    @ObfuscatedName("at")
    public static final fo at = new fo(41, -2);

    @ObfuscatedName("ag")
    public static final fo ag = new fo(42, 7);

    @ObfuscatedName("as")
    public static final fo as = new fo(43, -1);

    @ObfuscatedName("aw")
    public static final fo aw = new fo(44, 8);

    @ObfuscatedName("aq")
    public static final fo aq = new fo(45, 2);

    @ObfuscatedName("aa")
    public static final fo aa = new fo(46, 11);

    @ObfuscatedName("af")
    public static final fo af = new fo(47, 5);

    @ObfuscatedName("ak")
    public static final fo ak = new fo(48, 8);

    @ObfuscatedName("ab")
    public static final fo ab = new fo(49, 0);

    @ObfuscatedName("ac")
    public static final fo ac = new fo(50, -1);

    @ObfuscatedName("ad")
    public static final fo ad = new fo(51, 3);

    @ObfuscatedName("bg")
    public static final fo bg = new fo(52, 0);

    @ObfuscatedName("br")
    public static final fo br = new fo(53, -1);

    @ObfuscatedName("ba")
    public static final fo ba = new fo(54, 16);

    @ObfuscatedName("bk")
    public static final fo bk = new fo(55, 8);

    @ObfuscatedName("be")
    public static final fo be = new fo(56, 3);

    @ObfuscatedName("bc")
    public static final fo bc = new fo(57, 8);

    @ObfuscatedName("bm")
    public static final fo bm = new fo(58, -2);

    @ObfuscatedName("bh")
    public static final fo bh = new fo(59, -1);

    @ObfuscatedName("bs")
    public static final fo bs = new fo(60, 16);

    @ObfuscatedName("bj")
    public static final fo bj = new fo(61, 7);

    @ObfuscatedName("bt")
    public static final fo bt = new fo(62, -1);

    @ObfuscatedName("by")
    public static final fo by = new fo(63, -1);

    @ObfuscatedName("bn")
    public static final fo bn = new fo(64, 3);

    @ObfuscatedName("bb")
    public static final fo bb = new fo(65, -1);

    @ObfuscatedName("bq")
    public static final fo bq = new fo(66, 13);

    @ObfuscatedName("bz")
    public static final fo bz = new fo(67, 4);

    @ObfuscatedName("bx")
    public static final fo bx = new fo(68, 9);

    @ObfuscatedName("bf")
    public static final fo bf = new fo(69, 8);

    @ObfuscatedName("bo")
    public static final fo bo = new fo(70, 3);

    @ObfuscatedName("bv")
    public static final fo bv = new fo(71, 2);

    @ObfuscatedName("bi")
    public static final fo bi = new fo(72, -1);

    @ObfuscatedName("bu")
    public static final fo bu = new fo(73, 8);

    @ObfuscatedName("bl")
    public static final fo bl = new fo(74, 7);

    @ObfuscatedName("bw")
    public static final fo bw = new fo(75, 2);

    @ObfuscatedName("bp")
    public static final fo bp = new fo(76, -1);

    @ObfuscatedName("bd")
    public static final fo bd = new fo(77, -1);

    @ObfuscatedName("cb")
    public static final fo cb = new fo(78, -1);

    @ObfuscatedName("cm")
    public static final fo cm = new fo(79, 7);

    @ObfuscatedName("cu")
    public static final fo cu = new fo(80, 15);

    @ObfuscatedName("cs")
    public static final fo cs = new fo(81, 3);

    @ObfuscatedName("ct")
    public static final fo ct = new fo(82, 9);

    @ObfuscatedName("cw")
    public static final fo cw = new fo(83, 0);

    @ObfuscatedName("ch")
    public static final fo ch = new fo(84, 9);

    @ObfuscatedName("cr")
    public static final fo cr = new fo(85, -1);

    @ObfuscatedName("co")
    public static final fo co = new fo(86, 3);

    @ObfuscatedName("cv")
    public static final fo cv = new fo(87, 7);

    @ObfuscatedName("cd")
    public static final fo cd = new fo(88, 8);

    @ObfuscatedName("cq")
    public static final fo cq = new fo(89, 3);

    @ObfuscatedName("ci")
    public static final fo ci = new fo(90, 3);

    @ObfuscatedName("cc")
    public static final fo cc = new fo(91, -1);

    @ObfuscatedName("ce")
    public static final fo ce = new fo(92, -1);

    @ObfuscatedName("cx")
    public static final fo cx = new fo(93, 3);

    @ObfuscatedName("cy")
    public static final fo cy = new fo(94, 13);

    @ObfuscatedName("cg")
    public static final fo cg = new fo(95, 6);

    @ObfuscatedName("cj")
    public static final fo cj = new fo(96, 3);

    @ObfuscatedName("cl")
    public static final fo cl = new fo(97, 8);

    @ObfuscatedName("cz")
    final int cz;

    @ObfuscatedName("cn")
    final int cn;

    fo(int var1, int var2) {
        this.cz = var1;
        this.cn = var2;
    }
}
