import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gl")
public interface gl {

    @ObfuscatedName("t")
    int t();
}
