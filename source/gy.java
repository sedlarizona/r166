import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gy")
public class gy extends Node {

    @ObfuscatedName("t")
    public final Object t;

    public gy(Object var1) {
        this.t = var1;
    }
}
