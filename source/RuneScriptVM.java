import java.io.EOFException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cy")
public class RuneScriptVM {

    @ObfuscatedName("g")
    public static FileSystem g;

    @ObfuscatedName("go")
    static int go;

    @ObfuscatedName("i")
    boolean[] i;

    @ObfuscatedName("a")
    boolean[] a;

    @ObfuscatedName("l")
    int[] ints;

    @ObfuscatedName("b")
    String[] strings;

    @ObfuscatedName("e")
    boolean e = false;

    @ObfuscatedName("x")
    long x;

    RuneScriptVM() {
        this.ints = new int[Client.cj.w(19)];
        this.strings = new String[Client.cj.w(15)];
        this.i = new boolean[this.ints.length];

        int var1;
        byte[] var4;
        for (var1 = 0; var1 < this.ints.length; ++var1) {
            jq var3 = (jq) jq.q.t((long) var1);
            jq var2;
            if (var3 != null) {
                var2 = var3;
            } else {
                var4 = jq.t.i(19, var1);
                var3 = new jq();
                if (var4 != null) {
                    var3.t(new ByteBuffer(var4));
                }

                jq.q.i(var3, (long) var1);
                var2 = var3;
            }

            this.i[var1] = var2.i;
        }

        this.a = new boolean[this.strings.length];

        for (var1 = 0; var1 < this.strings.length; ++var1) {
            jz var6 = (jz) jz.q.t((long) var1);
            jz var5;
            if (var6 != null) {
                var5 = var6;
            } else {
                var4 = jz.t.i(15, var1);
                var6 = new jz();
                if (var4 != null) {
                    var6.q(new ByteBuffer(var4));
                }

                jz.q.i(var6, (long) var1);
                var5 = var6;
            }

            this.a[var1] = var5.i;
        }

        for (var1 = 0; var1 < this.ints.length; ++var1) {
            this.ints[var1] = -1;
        }

        this.x();
    }

    @ObfuscatedName("t")
    void t(int var1, int var2) {
        this.ints[var1] = var2;
        if (this.i[var1]) {
            this.e = true;
        }

    }

    @ObfuscatedName("q")
    int q(int var1) {
        return this.ints[var1];
    }

    @ObfuscatedName("i")
    void i(int var1, String var2) {
        this.strings[var1] = var2;
        if (this.a[var1]) {
            this.e = true;
        }

    }

    @ObfuscatedName("a")
    String a(int var1) {
        return this.strings[var1];
    }

    @ObfuscatedName("l")
    void l() {
        int var1;
        for (var1 = 0; var1 < this.ints.length; ++var1) {
            if (!this.i[var1]) {
                this.ints[var1] = -1;
            }
        }

        for (var1 = 0; var1 < this.strings.length; ++var1) {
            if (!this.a[var1]) {
                this.strings[var1] = null;
            }
        }

    }

    @ObfuscatedName("b")
    RSRandomAccessFile b(boolean var1) {
        return ky.a("2", Client.be.e, var1);
    }

    @ObfuscatedName("e")
    void e() {
        RSRandomAccessFile var1 = this.b(true);

        try {
            int var2 = 3;
            int var3 = 0;

            int var4;
            for (var4 = 0; var4 < this.ints.length; ++var4) {
                if (this.i[var4] && this.ints[var4] != -1) {
                    var2 += 6;
                    ++var3;
                }
            }

            var2 += 2;
            var4 = 0;

            for (int var5 = 0; var5 < this.strings.length; ++var5) {
                if (this.a[var5] && this.strings[var5] != null) {
                    var2 += 2 + z.c(this.strings[var5]);
                    ++var4;
                }
            }

            ByteBuffer var9 = new ByteBuffer(var2);
            var9.a(1);
            var9.l(var3);

            int var6;
            for (var6 = 0; var6 < this.ints.length; ++var6) {
                if (this.i[var6] && this.ints[var6] != -1) {
                    var9.l(var6);
                    var9.e(this.ints[var6]);
                }
            }

            var9.l(var4);

            for (var6 = 0; var6 < this.strings.length; ++var6) {
                if (this.a[var6] && this.strings[var6] != null) {
                    var9.l(var6);
                    var9.u(this.strings[var6]);
                }
            }

            var1.q(var9.buffer, 0, var9.index);
        } catch (Exception var17) {
            ;
        } finally {
            try {
                var1.i();
            } catch (Exception var16) {
                ;
            }

        }

        this.e = false;
        this.x = au.t();
    }

    @ObfuscatedName("x")
    void x() {
        RSRandomAccessFile var1 = this.b(false);

        label205: {
            try {
                byte[] var2 = new byte[(int) var1.l()];

                int var4;
                for (int var3 = 0; var3 < var2.length; var3 += var4) {
                    var4 = var1.b(var2, var3, var2.length - var3);
                    if (var4 == -1) {
                        throw new EOFException();
                    }
                }

                ByteBuffer var13 = new ByteBuffer(var2);
                if (var13.buffer.length - var13.index >= 1) {
                    int var14 = var13.av();
                    if (var14 >= 0 && var14 <= 1) {
                        int var15 = var13.ae();

                        int var7;
                        int var8;
                        int var9;
                        for (var7 = 0; var7 < var15; ++var7) {
                            var8 = var13.ae();
                            var9 = var13.ap();
                            if (this.i[var8]) {
                                this.ints[var8] = var9;
                            }
                        }

                        var7 = var13.ae();
                        var8 = 0;

                        while (true) {
                            if (var8 >= var7) {
                                break label205;
                            }

                            var9 = var13.ae();
                            String var10 = var13.ar();
                            if (this.a[var9]) {
                                this.strings[var9] = var10;
                            }

                            ++var8;
                        }
                    }

                    return;
                }
            } catch (Exception var24) {
                break label205;
            } finally {
                try {
                    var1.i();
                } catch (Exception var23) {
                    ;
                }

            }

            return;
        }

        this.e = false;
    }

    @ObfuscatedName("p")
    void p() {
        if (this.e && this.x < au.t() - 60000L) {
            this.e();
        }

    }

    @ObfuscatedName("o")
    boolean o() {
        return this.e;
    }

    @ObfuscatedName("jb")
    static final void jb() {
        gd var0 = ap.t(fo.by, Client.ee.l);
        var0.i.a(0);
        Client.ee.i(var0);
    }
}
