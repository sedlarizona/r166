import java.io.File;
import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ed")
public class Skins extends Node {

    @ObfuscatedName("e")
    static byte[][][] e;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int count;

    @ObfuscatedName("i")
    int[] opcodes;

    @ObfuscatedName("a")
    int[][] skinList;

    Skins(int var1, byte[] var2) {
        this.t = var1;
        ByteBuffer var3 = new ByteBuffer(var2);
        this.count = var3.av();
        this.opcodes = new int[this.count];
        this.skinList = new int[this.count][];

        int var4;
        for (var4 = 0; var4 < this.count; ++var4) {
            this.opcodes[var4] = var3.av();
        }

        for (var4 = 0; var4 < this.count; ++var4) {
            this.skinList[var4] = new int[var3.av()];
        }

        for (var4 = 0; var4 < this.count; ++var4) {
            for (int var5 = 0; var5 < this.skinList[var4].length; ++var5) {
                this.skinList[var4][var5] = var3.av();
            }
        }

    }

    @ObfuscatedName("t")
    public static File t(String var0, String var1, int var2) {
        String var3 = var2 == 0 ? "" : "" + var2;
        ar.a = new File(fh.r, "jagex_cl_" + var0 + "_" + var1 + var3 + ".dat");
        String var4 = null;
        String var5 = null;
        boolean var6 = false;
        File var22;
        if (ar.a.exists()) {
            try {
                RSRandomAccessFile var7 = new RSRandomAccessFile(ar.a, "rw", 10000L);

                ByteBuffer var8;
                int var9;
                for (var8 = new ByteBuffer((int) var7.l()); var8.index < var8.buffer.length; var8.index += var9) {
                    var9 = var7.b(var8.buffer, var8.index, var8.buffer.length - var8.index);
                    if (var9 == -1) {
                        throw new IOException();
                    }
                }

                var8.index = 0;
                var9 = var8.av();
                if (var9 < 1 || var9 > 3) {
                    throw new IOException("" + var9);
                }

                int var10 = 0;
                if (var9 > 1) {
                    var10 = var8.av();
                }

                if (var9 <= 2) {
                    var4 = var8.an();
                    if (var10 == 1) {
                        var5 = var8.an();
                    }
                } else {
                    var4 = var8.ai();
                    if (var10 == 1) {
                        var5 = var8.ai();
                    }
                }

                var7.i();
            } catch (IOException var20) {
                var20.printStackTrace();
            }

            if (var4 != null) {
                var22 = new File(var4);
                if (!var22.exists()) {
                    var4 = null;
                }
            }

            if (var4 != null) {
                var22 = new File(var4, "test.dat");
                if (!ad.i(var22, true)) {
                    var4 = null;
                }
            }
        }

        if (var4 == null && var2 == 0) {
            label123: for (int var15 = 0; var15 < fy.z.length; ++var15) {
                for (int var16 = 0; var16 < s.k.length; ++var16) {
                    File var17 = new File(s.k[var16] + fy.z[var15] + File.separatorChar + var0 + File.separatorChar);
                    if (var17.exists() && ad.i(new File(var17, "test.dat"), true)) {
                        var4 = var17.toString();
                        var6 = true;
                        break label123;
                    }
                }
            }
        }

        if (var4 == null) {
            var4 = fh.r + File.separatorChar + "jagexcache" + var3 + File.separatorChar + var0 + File.separatorChar
                    + var1 + File.separatorChar;
            var6 = true;
        }

        if (var5 != null) {
            File var21 = new File(var5);
            var22 = new File(var4);

            try {
                File[] var23 = var21.listFiles();
                File[] var18 = var23;

                for (int var11 = 0; var11 < var18.length; ++var11) {
                    File var12 = var18[var11];
                    File var13 = new File(var22, var12.getName());
                    boolean var14 = var12.renameTo(var13);
                    if (!var14) {
                        throw new IOException();
                    }
                }
            } catch (Exception var19) {
                var19.printStackTrace();
            }

            var6 = true;
        }

        if (var6) {
            Canvas.q(new File(var4), (File) null);
        }

        return new File(var4);
    }
}
