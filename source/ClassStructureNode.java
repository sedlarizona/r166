import java.lang.reflect.Field;
import java.lang.reflect.Method;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ln")
public class ClassStructureNode extends Node {

    @ObfuscatedName("p")
    static byte[][] p;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int[] i;

    @ObfuscatedName("a")
    int[] a;

    @ObfuscatedName("l")
    Field[] fields;

    @ObfuscatedName("b")
    int[] b;

    @ObfuscatedName("e")
    Method[] methods;

    @ObfuscatedName("x")
    byte[][][] methodArguments;
}
