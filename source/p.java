import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("p")
public final class p extends g {

    @ObfuscatedName("da")
    static ju da;

    @ObfuscatedName("t")
    final int t;

    @ObfuscatedName("q")
    final int q;

    @ObfuscatedName("i")
    final int i;

    @ObfuscatedName("a")
    final int a;

    @ObfuscatedName("l")
    final int l;

    p(int var1, int var2, int var3, int var4, int var5) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
        this.a = var4;
        this.l = var5;
    }

    @ObfuscatedName("t")
    public final void t() {
        li.dw(this.t + li.az, this.q + li.ae, this.i - this.t, this.a - this.q, this.l);
    }

    @ObfuscatedName("i")
    static final void i(byte[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7,
            CollisionMap[] var8) {
        int var10;
        for (int var9 = 0; var9 < 8; ++var9) {
            for (var10 = 0; var10 < 8; ++var10) {
                if (var9 + var2 > 0 && var9 + var2 < 103 && var3 + var10 > 0 && var3 + var10 < 103) {
                    var8[var1].flags[var9 + var2][var3 + var10] &= -16777217;
                }
            }
        }

        ByteBuffer var20 = new ByteBuffer(var0);

        for (var10 = 0; var10 < 4; ++var10) {
            for (int var11 = 0; var11 < 64; ++var11) {
                for (int var12 = 0; var12 < 64; ++var12) {
                    if (var10 == var4 && var11 >= var5 && var11 < var5 + 8 && var12 >= var6 && var12 < var6 + 8) {
                        int var17 = var11 & 7;
                        int var18 = var12 & 7;
                        int var19 = var7 & 3;
                        int var16;
                        if (var19 == 0) {
                            var16 = var17;
                        } else if (var19 == 1) {
                            var16 = var18;
                        } else if (var19 == 2) {
                            var16 = 7 - var17;
                        } else {
                            var16 = 7 - var18;
                        }

                        AreaSoundEmitter.l(var20, var1, var2 + var16, var3 + gr.t(var11 & 7, var12 & 7, var7), 0, 0,
                                var7);
                    } else {
                        AreaSoundEmitter.l(var20, 0, -1, -1, 0, 0, 0);
                    }
                }
            }
        }

    }

    @ObfuscatedName("w")
    static final void w() {
        ak.p("Your ignore list is full. Max of 100 for free users, and 400 for members");
    }

    @ObfuscatedName("am")
    protected static final void am() {
        GameEngine.o.t();

        int var0;
        for (var0 = 0; var0 < 32; ++var0) {
            GameEngine.v[var0] = 0L;
        }

        for (var0 = 0; var0 < 32; ++var0) {
            GameEngine.j[var0] = 0L;
        }

        go.e = 0;
    }

    @ObfuscatedName("fm")
    static final void fm(RTComponent var0, int var1, int var2) {
        if (Client.oo == 0 || Client.oo == 3) {
            if (bs.j == 1 || !er.ch && bs.j == 4) {
                iq var3 = var0.k(true);
                if (var3 == null) {
                    return;
                }

                int var4 = bs.cameraZ - var1;
                int var5 = bs.z - var2;
                if (var3.t(var4, var5)) {
                    var4 -= var3.t / 2;
                    var5 -= var3.q / 2;
                    int var6 = Client.hintPlane & 2047;
                    int var7 = eu.m[var6];
                    int var8 = eu.ay[var6];
                    int var9 = var4 * var8 + var7 * var5 >> 11;
                    int var10 = var8 * var5 - var7 * var4 >> 11;
                    int var11 = var9 + az.il.regionX >> 7;
                    int var12 = az.il.regionY - var10 >> 7;
                    gd var13 = ap.t(fo.cr, Client.ee.l);
                    var13.i.a(18);
                    var13.i.bt(var12 + PlayerComposite.ep);
                    var13.i.bk(ad.pressedKeys[82] ? (ad.pressedKeys[81] ? 2 : 1) : 0);
                    var13.i.bn(var11 + an.regionBaseX);
                    var13.i.a(var4);
                    var13.i.a(var5);
                    var13.i.l(Client.hintPlane);
                    var13.i.a(57);
                    var13.i.a(0);
                    var13.i.a(0);
                    var13.i.a(89);
                    var13.i.l(az.il.regionX);
                    var13.i.l(az.il.regionY);
                    var13.i.a(63);
                    Client.ee.i(var13);
                    Client.destinationX = var11;
                    Client.destinationY = var12;
                }
            }

        }
    }

    @ObfuscatedName("jj")
    static final void jj(int var0, int var1, int var2, int var3, Sprite var4, iq var5) {
        if (var4 != null) {
            int var6 = Client.hintPlane & 2047;
            int var7 = var3 * var3 + var2 * var2;
            if (var7 <= 6400) {
                int var8 = eu.m[var6];
                int var9 = eu.ay[var6];
                int var10 = var9 * var2 + var3 * var8 >> 16;
                int var11 = var3 * var9 - var8 * var2 >> 16;
                if (var7 > 2500) {
                    var4.ah(var10 + var5.t / 2 - var4.b / 2, var5.q / 2 - var11 - var4.e / 2, var0, var1, var5.t,
                            var5.q, var5.a, var5.i);
                } else {
                    var4.u(var0 + var10 + var5.t / 2 - var4.b / 2, var5.q / 2 + var1 - var11 - var4.e / 2);
                }

            }
        }
    }
}
