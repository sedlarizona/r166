import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cu")
public final class AreaSoundEmitter extends Node {

    @ObfuscatedName("t")
    static Deque t = new Deque();

    @ObfuscatedName("br")
    static int br;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int ambientSoundId;

    @ObfuscatedName("p")
    dy p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int[] shufflingSoundIds;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    dy v;

    @ObfuscatedName("u")
    ObjectDefinition emittingObjectsDefinition;

    @ObfuscatedName("t")
    void t() {
        int var1 = this.ambientSoundId;
        ObjectDefinition var2 = this.emittingObjectsDefinition.u();
        if (var2 != null) {
            this.ambientSoundId = var2.aq;
            this.e = var2.aa * 128;
            this.g = var2.af;
            this.n = var2.ak;
            this.shufflingSoundIds = var2.ab;
        } else {
            this.ambientSoundId = -1;
            this.e = 0;
            this.g = 0;
            this.n = 0;
            this.shufflingSoundIds = null;
        }

        if (var1 != this.ambientSoundId && this.p != null) {
            kq.pd.q(this.p);
            this.p = null;
        }

    }

    @ObfuscatedName("l")
    static final void l(ByteBuffer var0, int var1, int var2, int var3, int var4, int var5, int var6) {
        int var7;
        if (var2 >= 0 && var2 < 104 && var3 >= 0 && var3 < 104) {
            bt.landscapeData[var1][var2][var3] = 0;

            while (true) {
                var7 = var0.av();
                if (var7 == 0) {
                    if (var1 == 0) {
                        int[] var8 = bt.tileHeights[0][var2];
                        int var11 = var2 + var4 + 932731;
                        int var12 = var3 + var5 + 556238;
                        int var13 = aa.e(var11 + '넵', 91923 + var12, 4) - 128
                                + (aa.e(10294 + var11, '鎽' + var12, 2) - 128 >> 1) + (aa.e(var11, var12, 1) - 128 >> 2);
                        var13 = (int) (0.3D * (double) var13) + 35;
                        if (var13 < 10) {
                            var13 = 10;
                        } else if (var13 > 60) {
                            var13 = 60;
                        }

                        var8[var3] = -var13 * 8;
                    } else {
                        bt.tileHeights[var1][var2][var3] = bt.tileHeights[var1 - 1][var2][var3] - 240;
                    }
                    break;
                }

                if (var7 == 1) {
                    int var14 = var0.av();
                    if (var14 == 1) {
                        var14 = 0;
                    }

                    if (var1 == 0) {
                        bt.tileHeights[0][var2][var3] = -var14 * 8;
                    } else {
                        bt.tileHeights[var1][var2][var3] = bt.tileHeights[var1 - 1][var2][var3] - var14 * 8;
                    }
                    break;
                }

                if (var7 <= 49) {
                    bt.l[var1][var2][var3] = var0.aj();
                    bt.b[var1][var2][var3] = (byte) ((var7 - 2) / 4);
                    Skins.e[var1][var2][var3] = (byte) (var7 - 2 + var6 & 3);
                } else if (var7 <= 81) {
                    bt.landscapeData[var1][var2][var3] = (byte) (var7 - 49);
                } else {
                    bt.a[var1][var2][var3] = (byte) (var7 - 81);
                }
            }
        } else {
            while (true) {
                var7 = var0.av();
                if (var7 == 0) {
                    break;
                }

                if (var7 == 1) {
                    var0.av();
                    break;
                }

                if (var7 <= 49) {
                    var0.av();
                }
            }
        }

    }

    @ObfuscatedName("u")
    static boolean u(FileSystem var0, int var1) {
        byte[] var2 = var0.o(var1);
        if (var2 == null) {
            return false;
        } else {
            RTComponent.k(var2);
            return true;
        }
    }

    @ObfuscatedName("gb")
    static boolean gb(Player var0) {
        if (Client.jt == 0) {
            return false;
        } else {
            boolean var1;
            if (az.il != var0) {
                var1 = (Client.jt & 4) != 0;
                boolean var2 = var1;
                if (!var1) {
                    boolean var3 = (Client.jt & 1) != 0;
                    var2 = var3 && var0.q();
                }

                return var2 || jl.gq() && var0.l();
            } else {
                var1 = (Client.jt & 8) != 0;
                return var1;
            }
        }
    }
}
