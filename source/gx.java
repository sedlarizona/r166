import java.util.Calendar;
import java.util.TimeZone;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gx")
public class gx {

    @ObfuscatedName("qy")
    static long qy;

    @ObfuscatedName("t")
    public static final String[][] t = new String[][] {
            { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" },
            { "Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez" } };

    @ObfuscatedName("q")
    public static final String[] q = new String[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

    @ObfuscatedName("i")
    public static Calendar i;

    @ObfuscatedName("fk")
    static byte[][] fk;

    static {
        Calendar.getInstance(TimeZone.getTimeZone("Europe/London"));
        i = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    }

    @ObfuscatedName("a")
    public static void a() {
        Varpbit.q.a();
    }

    @ObfuscatedName("js")
    static final void js() {
        Client.mj = Client.mn;
        RuneScriptStackItem.oi = true;
    }
}
