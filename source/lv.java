import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lv")
public class lv {

    @ObfuscatedName("t")
    public static final char[] t = new char[] { '€', '\u0000', '‚', 'ƒ', '„', '…', '†', '‡', 'ˆ', '‰', 'Š', '‹', 'Œ',
            '\u0000', 'Ž', '\u0000', '\u0000', '‘', '’', '“', '”', '•', '–', '—', '˜', '™', 'š', '›', 'œ', '\u0000',
            'ž', 'Ÿ' };
}
