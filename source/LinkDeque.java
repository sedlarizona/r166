import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hn")
public class LinkDeque {

    @ObfuscatedName("t")
    Link sentinel = new Link();

    @ObfuscatedName("q")
    Link tail;

    public LinkDeque() {
        this.sentinel.previous = this.sentinel;
        this.sentinel.next = this.sentinel;
    }

    @ObfuscatedName("t")
    public void t(Link var1) {
        if (var1.next != null) {
            var1.t();
        }

        var1.next = this.sentinel.next;
        var1.previous = this.sentinel;
        var1.next.previous = var1;
        var1.previous.next = var1;
    }

    @ObfuscatedName("q")
    public Link q() {
        Link var1 = this.sentinel.previous;
        if (var1 == this.sentinel) {
            this.tail = null;
            return null;
        } else {
            this.tail = var1.previous;
            return var1;
        }
    }

    @ObfuscatedName("i")
    public Link i() {
        Link var1 = this.tail;
        if (var1 == this.sentinel) {
            this.tail = null;
            return null;
        } else {
            this.tail = var1.previous;
            return var1;
        }
    }
}
