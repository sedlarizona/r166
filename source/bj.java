import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bj")
public final class bj {

    @ObfuscatedName("pc")
    static int pc;

    @ObfuscatedName("i")
    public static String i;

    @ObfuscatedName("e")
    static int e;

    @ObfuscatedName("o")
    static String o() {
        String var0 = "";

        ChatboxMessage var2;
        for (Iterator var1 = cg.q.iterator(); var1.hasNext(); var0 = var0 + var2.sender + ':' + var2.message + '\n') {
            var2 = (ChatboxMessage) var1.next();
        }

        return var0;
    }
}
