import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("eh")
public class eh extends ky {

    @ObfuscatedName("t")
    final boolean t;

    public eh(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (var1.l != 0) {
            if (var2.l == 0) {
                return this.t ? -1 : 1;
            }
        } else if (var2.l != 0) {
            return this.t ? 1 : -1;
        }

        return this.p(var1, var2);
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("q")
    static final boolean q(int var0, int var1, fv var2, CollisionMap var3) {
        int var4 = var0;
        int var5 = var1;
        byte var6 = 64;
        byte var7 = 64;
        int var8 = var0 - var6;
        int var9 = var1 - var7;
        fc.i[var6][var7] = 99;
        fc.a[var6][var7] = 0;
        byte var10 = 0;
        int var11 = 0;
        fc.x[var10] = var0;
        byte var10001 = var10;
        int var18 = var10 + 1;
        fc.p[var10001] = var1;
        int[][] var12 = var3.flags;

        while (var11 != var18) {
            var4 = fc.x[var11];
            var5 = fc.p[var11];
            var11 = var11 + 1 & 4095;
            int var16 = var4 - var8;
            int var17 = var5 - var9;
            int var13 = var4 - var3.widthOffset;
            int var14 = var5 - var3.heightOffset;
            if (var2.t(1, var4, var5, var3)) {
                z.l = var4;
                j.b = var5;
                return true;
            }

            int var15 = fc.a[var16][var17] + 1;
            if (var16 > 0 && fc.i[var16 - 1][var17] == 0 && (var12[var13 - 1][var14] & 19136776) == 0) {
                fc.x[var18] = var4 - 1;
                fc.p[var18] = var5;
                var18 = var18 + 1 & 4095;
                fc.i[var16 - 1][var17] = 2;
                fc.a[var16 - 1][var17] = var15;
            }

            if (var16 < 127 && fc.i[var16 + 1][var17] == 0 && (var12[var13 + 1][var14] & 19136896) == 0) {
                fc.x[var18] = var4 + 1;
                fc.p[var18] = var5;
                var18 = var18 + 1 & 4095;
                fc.i[var16 + 1][var17] = 8;
                fc.a[var16 + 1][var17] = var15;
            }

            if (var17 > 0 && fc.i[var16][var17 - 1] == 0 && (var12[var13][var14 - 1] & 19136770) == 0) {
                fc.x[var18] = var4;
                fc.p[var18] = var5 - 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16][var17 - 1] = 1;
                fc.a[var16][var17 - 1] = var15;
            }

            if (var17 < 127 && fc.i[var16][var17 + 1] == 0 && (var12[var13][var14 + 1] & 19136800) == 0) {
                fc.x[var18] = var4;
                fc.p[var18] = var5 + 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16][var17 + 1] = 4;
                fc.a[var16][var17 + 1] = var15;
            }

            if (var16 > 0 && var17 > 0 && fc.i[var16 - 1][var17 - 1] == 0
                    && (var12[var13 - 1][var14 - 1] & 19136782) == 0 && (var12[var13 - 1][var14] & 19136776) == 0
                    && (var12[var13][var14 - 1] & 19136770) == 0) {
                fc.x[var18] = var4 - 1;
                fc.p[var18] = var5 - 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 - 1][var17 - 1] = 3;
                fc.a[var16 - 1][var17 - 1] = var15;
            }

            if (var16 < 127 && var17 > 0 && fc.i[var16 + 1][var17 - 1] == 0
                    && (var12[var13 + 1][var14 - 1] & 19136899) == 0 && (var12[var13 + 1][var14] & 19136896) == 0
                    && (var12[var13][var14 - 1] & 19136770) == 0) {
                fc.x[var18] = var4 + 1;
                fc.p[var18] = var5 - 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 + 1][var17 - 1] = 9;
                fc.a[var16 + 1][var17 - 1] = var15;
            }

            if (var16 > 0 && var17 < 127 && fc.i[var16 - 1][var17 + 1] == 0
                    && (var12[var13 - 1][var14 + 1] & 19136824) == 0 && (var12[var13 - 1][var14] & 19136776) == 0
                    && (var12[var13][var14 + 1] & 19136800) == 0) {
                fc.x[var18] = var4 - 1;
                fc.p[var18] = var5 + 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 - 1][var17 + 1] = 6;
                fc.a[var16 - 1][var17 + 1] = var15;
            }

            if (var16 < 127 && var17 < 127 && fc.i[var16 + 1][var17 + 1] == 0
                    && (var12[var13 + 1][var14 + 1] & 19136992) == 0 && (var12[var13 + 1][var14] & 19136896) == 0
                    && (var12[var13][var14 + 1] & 19136800) == 0) {
                fc.x[var18] = var4 + 1;
                fc.p[var18] = var5 + 1;
                var18 = var18 + 1 & 4095;
                fc.i[var16 + 1][var17 + 1] = 12;
                fc.a[var16 + 1][var17 + 1] = var15;
            }
        }

        z.l = var4;
        j.b = var5;
        return false;
    }

    @ObfuscatedName("a")
    public static boolean a(int var0) {
        return (var0 >> 29 & 1) != 0;
    }

    @ObfuscatedName("fn")
    static final void fn() {
        for (int var0 = 0; var0 < Client.dz; ++var0) {
            int var1 = Client.npcIndices[var0];
            Npc var2 = Client.loadedNpcs[var1];
            if (var2 != null) {
                AppletParameter.fj(var2, var2.composite.e);
            }
        }

    }
}
