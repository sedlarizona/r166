import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("iz")
public final class iz {

    @ObfuscatedName("t")
    public static void t(FileSystem var0, FileSystem var1) {
        CombatBarDefinition.t = var0;
        hi.q = var1;
    }

    @ObfuscatedName("q")
    static final void q(Model var0, int var1, int var2, int var3, int var4) {
        x.l.q(new n(var0, var1, var2, var3, var4));
    }
}
