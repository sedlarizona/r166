import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kr")
public class kr extends Link {

    @ObfuscatedName("i")
    public int i = (int) (au.t() / 1000L);

    @ObfuscatedName("a")
    public kb a;

    @ObfuscatedName("l")
    public short l;

    kr(kb var1, int var2) {
        this.a = var1;
        this.l = (short) var2;
    }
}
