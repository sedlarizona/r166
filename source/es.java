import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("es")
public final class es {

    @ObfuscatedName("qe")
    static v qe;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    int n;

    @ObfuscatedName("o")
    int o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    int v;

    @ObfuscatedName("u")
    int u;

    @ObfuscatedName("j")
    int j;

    @ObfuscatedName("k")
    int k;

    @ObfuscatedName("z")
    int z;
}
