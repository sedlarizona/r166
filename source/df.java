import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("df")
public abstract class df extends Node {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("t")
    abstract void t();

    @ObfuscatedName("q")
    abstract int q();
}
