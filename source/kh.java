import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kh")
public class kh {

    @ObfuscatedName("t")
    long t = -1L;

    @ObfuscatedName("q")
    long q = -1L;

    @ObfuscatedName("i")
    public boolean i = false;

    @ObfuscatedName("a")
    long a = 0L;

    @ObfuscatedName("l")
    long l = 0L;

    @ObfuscatedName("b")
    long b = 0L;

    @ObfuscatedName("e")
    int e = 0;

    @ObfuscatedName("x")
    int x = 0;

    @ObfuscatedName("p")
    int p = 0;

    @ObfuscatedName("g")
    int g = 0;

    @ObfuscatedName("t")
    public void t() {
        this.t = au.t();
    }

    @ObfuscatedName("q")
    public void q() {
        if (-1L != this.t) {
            this.l = au.t() - this.t;
            this.t = -1L;
        }

    }

    @ObfuscatedName("i")
    public void i(int var1) {
        this.q = au.t();
        this.e = var1;
    }

    @ObfuscatedName("a")
    public void a() {
        if (-1L != this.q) {
            this.a = au.t() - this.q;
            this.q = -1L;
        }

        ++this.p;
        this.i = true;
    }

    @ObfuscatedName("l")
    public void l() {
        this.i = false;
        this.x = 0;
    }

    @ObfuscatedName("b")
    public void b() {
        this.a();
    }

    @ObfuscatedName("e")
    public void e(ByteBuffer var1) {
        long var2 = this.l;
        var2 /= 10L;
        if (var2 < 0L) {
            var2 = 0L;
        } else if (var2 > 65535L) {
            var2 = 65535L;
        }

        var1.l((int) var2);
        long var4 = this.a;
        var4 /= 10L;
        if (var4 < 0L) {
            var4 = 0L;
        } else if (var4 > 65535L) {
            var4 = 65535L;
        }

        var1.l((int) var4);
        long var6 = this.b;
        var6 /= 10L;
        if (var6 < 0L) {
            var6 = 0L;
        } else if (var6 > 65535L) {
            var6 = 65535L;
        }

        var1.l((int) var6);
        var1.l(this.e);
        var1.l(this.x);
        var1.l(this.p);
        var1.l(this.g);
    }

    @ObfuscatedName("q")
    public static jv q(int var0) {
        jv var1 = (jv) jv.q.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = jv.t.i(8, var0);
            var1 = new jv();
            if (var2 != null) {
                var1.i(new ByteBuffer(var2));
            }

            jv.q.i(var1, (long) var0);
            return var1;
        }
    }
}
