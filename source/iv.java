import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("iv")
public class iv {

    @ObfuscatedName("t")
    static int[] t = new int[32];

    @ObfuscatedName("q")
    public static int[] q;

    @ObfuscatedName("i")
    public static int[] varps;

    @ObfuscatedName("e")
    static Sprite e;

    static {
        int var0 = 2;

        for (int var1 = 0; var1 < 32; ++var1) {
            t[var1] = var0 - 1;
            var0 += var0;
        }

        q = new int[2000];
        varps = new int[2000];
    }
}
