import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jn")
public class VarInfo extends hh {

    @ObfuscatedName("t")
    static FileSystem t;

    @ObfuscatedName("q")
    public static int q;

    @ObfuscatedName("i")
    public static Cache i = new Cache(64);

    @ObfuscatedName("p")
    static int p;

    @ObfuscatedName("a")
    public int control = 0;

    @ObfuscatedName("i")
    void i(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.a(var1, var2);
        }
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1, int var2) {
        if (var2 == 5) {
            this.control = var1.ae();
        }

    }
}
