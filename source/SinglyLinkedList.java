import java.util.Iterator;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hr")
public class SinglyLinkedList implements Iterable {

    @ObfuscatedName("t")
    Node sentinel = new Node();

    @ObfuscatedName("q")
    Node current;

    public SinglyLinkedList() {
        this.sentinel.next = this.sentinel;
        this.sentinel.previous = this.sentinel;
    }

    @ObfuscatedName("t")
    public void t() {
        while (this.sentinel.next != this.sentinel) {
            this.sentinel.next.kc();
        }

    }

    @ObfuscatedName("q")
    public void q(Node var1) {
        if (var1.previous != null) {
            var1.kc();
        }

        var1.previous = this.sentinel.previous;
        var1.next = this.sentinel;
        var1.previous.next = var1;
        var1.next.previous = var1;
    }

    @ObfuscatedName("i")
    public void i(Node var1) {
        if (var1.previous != null) {
            var1.kc();
        }

        var1.previous = this.sentinel;
        var1.next = this.sentinel.next;
        var1.previous.next = var1;
        var1.next.previous = var1;
    }

    @ObfuscatedName("l")
    public Node l() {
        Node var1 = this.sentinel.next;
        if (var1 == this.sentinel) {
            return null;
        } else {
            var1.kc();
            return var1;
        }
    }

    @ObfuscatedName("b")
    public Node b() {
        return this.e((Node) null);
    }

    @ObfuscatedName("e")
    Node e(Node var1) {
        Node var2;
        if (var1 == null) {
            var2 = this.sentinel.next;
        } else {
            var2 = var1;
        }

        if (var2 == this.sentinel) {
            this.current = null;
            return null;
        } else {
            this.current = var2.next;
            return var2;
        }
    }

    @ObfuscatedName("x")
    public Node x() {
        Node var1 = this.current;
        if (var1 == this.sentinel) {
            this.current = null;
            return null;
        } else {
            this.current = var1.next;
            return var1;
        }
    }

    @ObfuscatedName("p")
    public boolean p() {
        return this.sentinel.next == this.sentinel;
    }

    public Iterator iterator() {
        return new DoublyNodeIterator(this);
    }

    @ObfuscatedName("a")
    public static void a(Node var0, Node var1) {
        if (var0.previous != null) {
            var0.kc();
        }

        var0.previous = var1;
        var0.next = var1.next;
        var0.previous.next = var0;
        var0.next.previous = var0;
    }
}
