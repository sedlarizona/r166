import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hk")
public class AudioTask extends TaskDataNode {

    @ObfuscatedName("t")
    HashTable table = new HashTable(128);

    @ObfuscatedName("q")
    int q = 256;

    @ObfuscatedName("i")
    int i = 1000000;

    @ObfuscatedName("a")
    int[] a = new int[16];

    @ObfuscatedName("l")
    int[] l = new int[16];

    @ObfuscatedName("b")
    int[] b = new int[16];

    @ObfuscatedName("e")
    int[] e = new int[16];

    @ObfuscatedName("x")
    int[] x = new int[16];

    @ObfuscatedName("p")
    int[] p = new int[16];

    @ObfuscatedName("g")
    int[] g = new int[16];

    @ObfuscatedName("n")
    int[] n = new int[16];

    @ObfuscatedName("o")
    int[] o = new int[16];

    @ObfuscatedName("s")
    int[] s = new int[16];

    @ObfuscatedName("d")
    int[] d = new int[16];

    @ObfuscatedName("f")
    int[] f = new int[16];

    @ObfuscatedName("r")
    int[] r = new int[16];

    @ObfuscatedName("y")
    int[] y = new int[16];

    @ObfuscatedName("h")
    int[] h = new int[16];

    @ObfuscatedName("m")
    hb[][] m = new hb[16][128];

    @ObfuscatedName("ay")
    hb[][] ay = new hb[16][128];

    @ObfuscatedName("ao")
    hy ao = new hy();

    @ObfuscatedName("av")
    boolean av;

    @ObfuscatedName("aj")
    int aj;

    @ObfuscatedName("ae")
    int ae;

    @ObfuscatedName("am")
    long am;

    @ObfuscatedName("az")
    long az;

    @ObfuscatedName("ap")
    ie ap = new ie(this);

    public AudioTask() {
        this.as();
    }

    @ObfuscatedName("t")
    synchronized void t(int var1) {
        this.q = var1;
    }

    @ObfuscatedName("q")
    int q() {
        return this.q;
    }

    @ObfuscatedName("i")
    synchronized boolean i(ix var1, FileSystem var2, dc var3, int var4) {
        var1.q();
        boolean var5 = true;
        int[] var6 = null;
        if (var4 > 0) {
            var6 = new int[] { var4 };
        }

        for (ByteArrayNode var7 = (ByteArrayNode) var1.t.a(); var7 != null; var7 = (ByteArrayNode) var1.t.l()) {
            int var8 = (int) var7.uid;
            ho var9 = (ho) this.table.t((long) var8);
            if (var9 == null) {
                var9 = RuneScript.t(var2, var8);
                if (var9 == null) {
                    var5 = false;
                    continue;
                }

                this.table.q(var9, (long) var8);
            }

            if (!var9.q(var3, var7.bytes, var6)) {
                var5 = false;
            }
        }

        if (var5) {
            var1.i();
        }

        return var5;
    }

    @ObfuscatedName("a")
    synchronized void a() {
        for (ho var1 = (ho) this.table.a(); var1 != null; var1 = (ho) this.table.l()) {
            var1.i();
        }

    }

    @ObfuscatedName("l")
    synchronized void l() {
        for (ho var1 = (ho) this.table.a(); var1 != null; var1 = (ho) this.table.l()) {
            var1.kc();
        }

    }

    @ObfuscatedName("b")
    protected synchronized TaskDataNode b() {
        return this.ap;
    }

    @ObfuscatedName("e")
    protected synchronized TaskDataNode e() {
        return null;
    }

    @ObfuscatedName("x")
    protected synchronized int x() {
        return 0;
    }

    @ObfuscatedName("p")
    protected synchronized void p(int[] var1, int var2, int var3) {
        if (this.ao.i()) {
            int var4 = this.ao.q * this.i / TaskData.l;

            do {
                long var5 = this.am + (long) var3 * (long) var4;
                if (this.az - var5 >= 0L) {
                    this.am = var5;
                    break;
                }

                int var7 = (int) (((long) var4 + (this.az - this.am) - 1L) / (long) var4);
                this.am += (long) var4 * (long) var7;
                this.ap.p(var1, var2, var7);
                var2 += var7;
                var3 -= var7;
                this.bg();
            } while (this.ao.i());
        }

        this.ap.p(var1, var2, var3);
    }

    @ObfuscatedName("o")
    synchronized void o(ix var1, boolean var2) {
        this.u();
        this.ao.t(var1.q);
        this.av = var2;
        this.am = 0L;
        int var3 = this.ao.a();

        for (int var4 = 0; var4 < var3; ++var4) {
            this.ao.l(var4);
            this.ao.x(var4);
            this.ao.b(var4);
        }

        this.aj = this.ao.k();
        this.ae = this.ao.l[this.aj];
        this.az = this.ao.u(this.ae);
    }

    @ObfuscatedName("c")
    protected synchronized void c(int var1) {
        if (this.ao.i()) {
            int var2 = this.ao.q * this.i / TaskData.l;

            do {
                long var3 = this.am + (long) var2 * (long) var1;
                if (this.az - var3 >= 0L) {
                    this.am = var3;
                    break;
                }

                int var5 = (int) (((long) var2 + (this.az - this.am) - 1L) / (long) var2);
                this.am += (long) var5 * (long) var2;
                this.ap.c(var5);
                var1 -= var5;
                this.bg();
            } while (this.ao.i());
        }

        this.ap.c(var1);
    }

    @ObfuscatedName("u")
    synchronized void u() {
        this.ao.q();
        this.as();
    }

    @ObfuscatedName("k")
    public synchronized boolean k() {
        return this.ao.i();
    }

    @ObfuscatedName("z")
    public synchronized void z(int var1, int var2) {
        this.w(var1, var2);
    }

    @ObfuscatedName("w")
    void w(int var1, int var2) {
        this.e[var1] = var2;
        this.p[var1] = var2 & -128;
        this.s(var1, var2);
    }

    @ObfuscatedName("s")
    void s(int var1, int var2) {
        if (var2 != this.x[var1]) {
            this.x[var1] = var2;

            for (int var3 = 0; var3 < 128; ++var3) {
                this.ay[var1][var3] = null;
            }
        }

    }

    @ObfuscatedName("d")
    void d(int var1, int var2, int var3) {
        this.r(var1, var2, 64);
        if ((this.s[var1] & 2) != 0) {
            for (hb var4 = (hb) this.ap.q.x(); var4 != null; var4 = (hb) this.ap.q.o()) {
                if (var4.t == var1 && var4.u < 0) {
                    this.m[var1][var4.b] = null;
                    this.m[var1][var2] = var4;
                    int var5 = (var4.n * var4.g >> 12) + var4.p;
                    var4.p += var2 - var4.b << 8;
                    var4.g = var5 - var4.p;
                    var4.n = 4096;
                    var4.b = var2;
                    return;
                }
            }
        }

        ho var9 = (ho) this.table.t((long) this.x[var1]);
        if (var9 != null) {
            DataRequestNode var8 = var9.q[var2];
            if (var8 != null) {
                hb var6 = new hb();
                var6.t = var1;
                var6.q = var9;
                var6.i = var8;
                var6.a = var9.b[var2];
                var6.l = var9.e[var2];
                var6.b = var2;
                var6.e = var3 * var3 * var9.a[var2] * var9.t + 1024 >> 11;
                var6.x = var9.l[var2] & 255;
                var6.p = (var2 << 8) - (var9.i[var2] & 32767);
                var6.o = 0;
                var6.c = 0;
                var6.v = 0;
                var6.u = -1;
                var6.j = 0;
                if (this.r[var1] == 0) {
                    var6.w = dy.a(var8, this.ab(var6), this.ac(var6), this.ad(var6));
                } else {
                    var6.w = dy.a(var8, this.ab(var6), 0, this.ad(var6));
                    this.f(var6, var9.i[var2] < 0);
                }

                if (var9.i[var2] < 0) {
                    var6.w.o(-1);
                }

                if (var6.l >= 0) {
                    hb var7 = this.ay[var1][var6.l];
                    if (var7 != null && var7.u < 0) {
                        this.m[var1][var7.b] = null;
                        var7.u = 0;
                    }

                    this.ay[var1][var6.l] = var6;
                }

                this.ap.q.q(var6);
                this.m[var1][var2] = var6;
            }
        }
    }

    @ObfuscatedName("f")
    void f(hb var1, boolean var2) {
        int var3 = var1.i.buffer.length;
        int var4;
        if (var2 && var1.i.incomplete) {
            int var5 = var3 + var3 - var1.i.i;
            var4 = (int) ((long) this.r[var1.t] * (long) var5 >> 6);
            var3 <<= 8;
            if (var4 >= var3) {
                var4 = var3 + var3 - 1 - var4;
                var1.w.f();
            }
        } else {
            var4 = (int) ((long) this.r[var1.t] * (long) var3 >> 6);
        }

        var1.w.d(var4);
    }

    @ObfuscatedName("r")
    void r(int var1, int var2, int var3) {
        hb var4 = this.m[var1][var2];
        if (var4 != null) {
            this.m[var1][var2] = null;
            if ((this.s[var1] & 2) != 0) {
                for (hb var5 = (hb) this.ap.q.e(); var5 != null; var5 = (hb) this.ap.q.p()) {
                    if (var4.t == var5.t && var5.u < 0 && var5 != var4) {
                        var4.u = 0;
                        break;
                    }
                }
            } else {
                var4.u = 0;
            }

        }
    }

    @ObfuscatedName("y")
    void y(int var1, int var2, int var3) {
    }

    @ObfuscatedName("h")
    void h(int var1, int var2) {
    }

    @ObfuscatedName("av")
    void av(int var1, int var2) {
        this.g[var1] = var2;
    }

    @ObfuscatedName("an")
    void an(int var1) {
        for (hb var2 = (hb) this.ap.q.e(); var2 != null; var2 = (hb) this.ap.q.p()) {
            if (var1 < 0 || var2.t == var1) {
                if (var2.w != null) {
                    var2.w.av(TaskData.l / 100);
                    if (var2.w.as()) {
                        this.ap.i.t(var2.w);
                    }

                    var2.t();
                }

                if (var2.u < 0) {
                    this.m[var2.t][var2.b] = null;
                }

                var2.kc();
            }
        }

    }

    @ObfuscatedName("ai")
    void ai(int var1) {
        if (var1 >= 0) {
            this.a[var1] = 12800;
            this.l[var1] = 8192;
            this.b[var1] = 16383;
            this.g[var1] = 8192;
            this.n[var1] = 0;
            this.o[var1] = 8192;
            this.aq(var1);
            this.aa(var1);
            this.s[var1] = 0;
            this.d[var1] = 32767;
            this.f[var1] = 256;
            this.r[var1] = 0;
            this.ak(var1, 8192);
        } else {
            for (var1 = 0; var1 < 16; ++var1) {
                this.ai(var1);
            }

        }
    }

    @ObfuscatedName("ag")
    void ag(int var1) {
        for (hb var2 = (hb) this.ap.q.e(); var2 != null; var2 = (hb) this.ap.q.p()) {
            if ((var1 < 0 || var2.t == var1) && var2.u < 0) {
                this.m[var2.t][var2.b] = null;
                var2.u = 0;
            }
        }

    }

    @ObfuscatedName("as")
    void as() {
        this.an(-1);
        this.ai(-1);

        int var1;
        for (var1 = 0; var1 < 16; ++var1) {
            this.x[var1] = this.e[var1];
        }

        for (var1 = 0; var1 < 16; ++var1) {
            this.p[var1] = this.e[var1] & -128;
        }

    }

    @ObfuscatedName("aq")
    void aq(int var1) {
        if ((this.s[var1] & 2) != 0) {
            for (hb var2 = (hb) this.ap.q.e(); var2 != null; var2 = (hb) this.ap.q.p()) {
                if (var2.t == var1 && this.m[var1][var2.b] == null && var2.u < 0) {
                    var2.u = 0;
                }
            }
        }

    }

    @ObfuscatedName("aa")
    void aa(int var1) {
        if ((this.s[var1] & 4) != 0) {
            for (hb var2 = (hb) this.ap.q.e(); var2 != null; var2 = (hb) this.ap.q.p()) {
                if (var2.t == var1) {
                    var2.f = 0;
                }
            }
        }

    }

    @ObfuscatedName("af")
    void af(int var1) {
        int var2 = var1 & 240;
        int var3;
        int var4;
        int var5;
        if (var2 == 128) {
            var3 = var1 & 15;
            var4 = var1 >> 8 & 127;
            var5 = var1 >> 16 & 127;
            this.r(var3, var4, var5);
        } else if (var2 == 144) {
            var3 = var1 & 15;
            var4 = var1 >> 8 & 127;
            var5 = var1 >> 16 & 127;
            if (var5 > 0) {
                this.d(var3, var4, var5);
            } else {
                this.r(var3, var4, 64);
            }

        } else if (var2 == 160) {
            var3 = var1 & 15;
            var4 = var1 >> 8 & 127;
            var5 = var1 >> 16 & 127;
            this.y(var3, var4, var5);
        } else if (var2 == 176) {
            var3 = var1 & 15;
            var4 = var1 >> 8 & 127;
            var5 = var1 >> 16 & 127;
            if (var4 == 0) {
                this.p[var3] = (var5 << 14) + (this.p[var3] & -2080769);
            }

            if (var4 == 32) {
                this.p[var3] = (var5 << 7) + (this.p[var3] & -16257);
            }

            if (var4 == 1) {
                this.n[var3] = (var5 << 7) + (this.n[var3] & -16257);
            }

            if (var4 == 33) {
                this.n[var3] = var5 + (this.n[var3] & -128);
            }

            if (var4 == 5) {
                this.o[var3] = (var5 << 7) + (this.o[var3] & -16257);
            }

            if (var4 == 37) {
                this.o[var3] = var5 + (this.o[var3] & -128);
            }

            if (var4 == 7) {
                this.a[var3] = (var5 << 7) + (this.a[var3] & -16257);
            }

            if (var4 == 39) {
                this.a[var3] = var5 + (this.a[var3] & -128);
            }

            if (var4 == 10) {
                this.l[var3] = (var5 << 7) + (this.l[var3] & -16257);
            }

            if (var4 == 42) {
                this.l[var3] = var5 + (this.l[var3] & -128);
            }

            if (var4 == 11) {
                this.b[var3] = (var5 << 7) + (this.b[var3] & -16257);
            }

            if (var4 == 43) {
                this.b[var3] = var5 + (this.b[var3] & -128);
            }

            if (var4 == 64) {
                if (var5 >= 64) {
                    this.s[var3] |= 1;
                } else {
                    this.s[var3] &= -2;
                }
            }

            if (var4 == 65) {
                if (var5 >= 64) {
                    this.s[var3] |= 2;
                } else {
                    this.aq(var3);
                    this.s[var3] &= -3;
                }
            }

            if (var4 == 99) {
                this.d[var3] = (var5 << 7) + (this.d[var3] & 127);
            }

            if (var4 == 98) {
                this.d[var3] = (this.d[var3] & 16256) + var5;
            }

            if (var4 == 101) {
                this.d[var3] = (var5 << 7) + (this.d[var3] & 127) + 16384;
            }

            if (var4 == 100) {
                this.d[var3] = (this.d[var3] & 16256) + var5 + 16384;
            }

            if (var4 == 120) {
                this.an(var3);
            }

            if (var4 == 121) {
                this.ai(var3);
            }

            if (var4 == 123) {
                this.ag(var3);
            }

            int var6;
            if (var4 == 6) {
                var6 = this.d[var3];
                if (var6 == 16384) {
                    this.f[var3] = (var5 << 7) + (this.f[var3] & -16257);
                }
            }

            if (var4 == 38) {
                var6 = this.d[var3];
                if (var6 == 16384) {
                    this.f[var3] = var5 + (this.f[var3] & -128);
                }
            }

            if (var4 == 16) {
                this.r[var3] = (var5 << 7) + (this.r[var3] & -16257);
            }

            if (var4 == 48) {
                this.r[var3] = var5 + (this.r[var3] & -128);
            }

            if (var4 == 81) {
                if (var5 >= 64) {
                    this.s[var3] |= 4;
                } else {
                    this.aa(var3);
                    this.s[var3] &= -5;
                }
            }

            if (var4 == 17) {
                this.ak(var3, (var5 << 7) + (this.y[var3] & -16257));
            }

            if (var4 == 49) {
                this.ak(var3, var5 + (this.y[var3] & -128));
            }

        } else if (var2 == 192) {
            var3 = var1 & 15;
            var4 = var1 >> 8 & 127;
            this.s(var3, var4 + this.p[var3]);
        } else if (var2 == 208) {
            var3 = var1 & 15;
            var4 = var1 >> 8 & 127;
            this.h(var3, var4);
        } else if (var2 == 224) {
            var3 = var1 & 15;
            var4 = (var1 >> 8 & 127) + (var1 >> 9 & 16256);
            this.av(var3, var4);
        } else {
            var2 = var1 & 255;
            if (var2 == 255) {
                this.as();
            }
        }
    }

    @ObfuscatedName("ak")
    void ak(int var1, int var2) {
        this.y[var1] = var2;
        this.h[var1] = (int) (2097152.0D * Math.pow(2.0D, 5.4931640625E-4D * (double) var2) + 0.5D);
    }

    @ObfuscatedName("ab")
    int ab(hb var1) {
        int var2 = (var1.n * var1.g >> 12) + var1.p;
        var2 += (this.g[var1.t] - 8192) * this.f[var1.t] >> 12;
        hd var3 = var1.a;
        int var4;
        if (var3.x > 0 && (var3.e > 0 || this.n[var1.t] > 0)) {
            var4 = var3.e << 2;
            int var5 = var3.p << 1;
            if (var1.k < var5) {
                var4 = var4 * var1.k / var5;
            }

            var4 += this.n[var1.t] >> 7;
            double var6 = Math.sin(0.01227184630308513D * (double) (var1.z & 511));
            var2 += (int) ((double) var4 * var6);
        }

        var4 = (int) ((double) (var1.i.t * 256) * Math.pow(2.0D, 3.255208333333333E-4D * (double) var2)
                / (double) TaskData.l + 0.5D);
        return var4 < 1 ? 1 : var4;
    }

    @ObfuscatedName("ac")
    int ac(hb var1) {
        hd var2 = var1.a;
        int var3 = this.b[var1.t] * this.a[var1.t] + 4096 >> 13;
        var3 = var3 * var3 + 16384 >> 15;
        var3 = var3 * var1.e + 16384 >> 15;
        var3 = var3 * this.q + 128 >> 8;
        if (var2.i > 0) {
            var3 = (int) ((double) var3 * Math.pow(0.5D, (double) var1.o * 1.953125E-5D * (double) var2.i) + 0.5D);
        }

        int var4;
        int var5;
        int var6;
        int var7;
        if (var2.t != null) {
            var4 = var1.c;
            var5 = var2.t[var1.v + 1];
            if (var1.v < var2.t.length - 2) {
                var6 = (var2.t[var1.v] & 255) << 8;
                var7 = (var2.t[var1.v + 2] & 255) << 8;
                var5 += (var2.t[var1.v + 3] - var5) * (var4 - var6) / (var7 - var6);
            }

            var3 = var3 * var5 + 32 >> 6;
        }

        if (var1.u > 0 && var2.q != null) {
            var4 = var1.u;
            var5 = var2.q[var1.j + 1];
            if (var1.j < var2.q.length - 2) {
                var6 = (var2.q[var1.j] & 255) << 8;
                var7 = (var2.q[var1.j + 2] & 255) << 8;
                var5 += (var2.q[var1.j + 3] - var5) * (var4 - var6) / (var7 - var6);
            }

            var3 = var3 * var5 + 32 >> 6;
        }

        return var3;
    }

    @ObfuscatedName("ad")
    int ad(hb var1) {
        int var2 = this.l[var1.t];
        return var2 < 8192 ? var2 * var1.x + 32 >> 6 : 16384 - ((128 - var1.x) * (16384 - var2) + 32 >> 6);
    }

    @ObfuscatedName("bg")
    void bg() {
        int var1 = this.aj;
        int var2 = this.ae;

        long var3;
        for (var3 = this.az; var2 == this.ae; var3 = this.ao.u(var2)) {
            while (var2 == this.ao.l[var1]) {
                this.ao.l(var1);
                int var5 = this.ao.p(var1);
                if (var5 == 1) {
                    this.ao.e();
                    this.ao.b(var1);
                    if (this.ao.z()) {
                        if (!this.av || var2 == 0) {
                            this.as();
                            this.ao.q();
                            return;
                        }

                        this.ao.w(var3);
                    }
                    break;
                }

                if ((var5 & 128) != 0) {
                    this.af(var5);
                }

                this.ao.x(var1);
                this.ao.b(var1);
            }

            var1 = this.ao.k();
            var2 = this.ao.l[var1];
        }

        this.aj = var1;
        this.ae = var2;
        this.az = var3;
    }

    @ObfuscatedName("br")
    boolean br(hb var1) {
        if (var1.w == null) {
            if (var1.u >= 0) {
                var1.kc();
                if (var1.l > 0 && var1 == this.ay[var1.t][var1.l]) {
                    this.ay[var1.t][var1.l] = null;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    @ObfuscatedName("ba")
    boolean ba(hb var1, int[] var2, int var3, int var4) {
        var1.s = TaskData.l / 100;
        if (var1.u < 0 || var1.w != null && !var1.w.ag()) {
            int var5 = var1.n;
            if (var5 > 0) {
                var5 -= (int) (16.0D * Math.pow(2.0D, (double) this.o[var1.t] * 4.921259842519685E-4D) + 0.5D);
                if (var5 < 0) {
                    var5 = 0;
                }

                var1.n = var5;
            }

            var1.w.an(this.ab(var1));
            hd var6 = var1.a;
            boolean var7 = false;
            ++var1.k;
            var1.z += var6.x;
            double var8 = 5.086263020833333E-6D * (double) ((var1.b - 60 << 8) + (var1.n * var1.g >> 12));
            if (var6.i > 0) {
                if (var6.b > 0) {
                    var1.o += (int) (128.0D * Math.pow(2.0D, (double) var6.b * var8) + 0.5D);
                } else {
                    var1.o += 128;
                }
            }

            if (var6.t != null) {
                if (var6.a > 0) {
                    var1.c += (int) (128.0D * Math.pow(2.0D, (double) var6.a * var8) + 0.5D);
                } else {
                    var1.c += 128;
                }

                while (var1.v < var6.t.length - 2 && var1.c > (var6.t[var1.v + 2] & 255) << 8) {
                    var1.v += 2;
                }

                if (var6.t.length - 2 == var1.v && var6.t[var1.v + 1] == 0) {
                    var7 = true;
                }
            }

            if (var1.u >= 0 && var6.q != null && (this.s[var1.t] & 1) == 0
                    && (var1.l < 0 || var1 != this.ay[var1.t][var1.l])) {
                if (var6.l > 0) {
                    var1.u += (int) (128.0D * Math.pow(2.0D, var8 * (double) var6.l) + 0.5D);
                } else {
                    var1.u += 128;
                }

                while (var1.j < var6.q.length - 2 && var1.u > (var6.q[var1.j + 2] & 255) << 8) {
                    var1.j += 2;
                }

                if (var6.q.length - 2 == var1.j) {
                    var7 = true;
                }
            }

            if (var7) {
                var1.w.av(var1.s);
                if (var2 != null) {
                    var1.w.p(var2, var3, var4);
                } else {
                    var1.w.c(var4);
                }

                if (var1.w.as()) {
                    this.ap.i.t(var1.w);
                }

                var1.t();
                if (var1.u >= 0) {
                    var1.kc();
                    if (var1.l > 0 && var1 == this.ay[var1.t][var1.l]) {
                        this.ay[var1.t][var1.l] = null;
                    }
                }

                return true;
            } else {
                var1.w.h(var1.s, this.ac(var1), this.ad(var1));
                return false;
            }
        } else {
            var1.t();
            var1.kc();
            if (var1.l > 0 && var1 == this.ay[var1.t][var1.l]) {
                this.ay[var1.t][var1.l] = null;
            }

            return true;
        }
    }
}
