import java.awt.Image;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ec")
public class ec implements Runnable {

    @ObfuscatedName("ah")
    static Image ah;

    @ObfuscatedName("t")
    final Thread t = new Thread(this);

    @ObfuscatedName("q")
    volatile boolean q;

    @ObfuscatedName("i")
    java.util.Queue i = new LinkedList();

    public ec() {
        this.t.setPriority(1);
        this.t.start();
    }

    @ObfuscatedName("t")
    public ep t(URL var1) {
        ep var2 = new ep(var1);
        synchronized (this) {
            this.i.add(var2);
            this.notify();
            return var2;
        }
    }

    @ObfuscatedName("q")
    public void q() {
        this.q = true;

        try {
            synchronized (this) {
                this.notify();
            }

            this.t.join();
        } catch (InterruptedException var4) {
            ;
        }

    }

    public void run() {
        while (!this.q) {
            try {
                ep var1;
                synchronized (this) {
                    var1 = (ep) this.i.poll();
                    if (var1 == null) {
                        try {
                            this.wait();
                        } catch (InterruptedException var13) {
                            ;
                        }
                        continue;
                    }
                }

                DataInputStream var2 = null;
                URLConnection var3 = null;

                try {
                    var3 = var1.t.openConnection();
                    var3.setConnectTimeout(5000);
                    var3.setReadTimeout(5000);
                    var3.setUseCaches(false);
                    var3.setRequestProperty("Connection", "close");
                    int var7 = var3.getContentLength();
                    if (var7 >= 0) {
                        byte[] var5 = new byte[var7];
                        var2 = new DataInputStream(var3.getInputStream());
                        var2.readFully(var5);
                        var1.i = var5;
                    }

                    var1.q = true;
                } catch (IOException var14) {
                    var1.q = true;
                } finally {
                    if (var2 != null) {
                        var2.close();
                    }

                    if (var3 != null && var3 instanceof HttpURLConnection) {
                        ((HttpURLConnection) var3).disconnect();
                    }

                }
            } catch (Exception var17) {
                FloorObject.t((String) null, var17);
            }
        }

    }

    @ObfuscatedName("i")
    public static int i(int var0) {
        var0 = (var0 & 1431655765) + (var0 >>> 1 & 1431655765);
        var0 = (var0 >>> 2 & 858993459) + (var0 & 858993459);
        var0 = var0 + (var0 >>> 4) & 252645135;
        var0 += var0 >>> 8;
        var0 += var0 >>> 16;
        return var0 & 255;
    }

    @ObfuscatedName("e")
    static void e() {
        ItemStorage.t = new HashTable(32);
    }
}
