import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bo")
public class ChatboxMessage extends hh {

    @ObfuscatedName("t")
    int index;

    @ObfuscatedName("q")
    int cycle;

    @ObfuscatedName("i")
    int type;

    @ObfuscatedName("a")
    String sender;

    @ObfuscatedName("l")
    kb l;

    @ObfuscatedName("b")
    kx b;

    @ObfuscatedName("e")
    kx e;

    @ObfuscatedName("x")
    String x;

    @ObfuscatedName("p")
    String message;

    ChatboxMessage(int var1, String var2, String var3, String var4) {
        this.b = kx.t;
        this.e = kx.t;
        int var5 = ++cg.a - 1;
        this.index = var5;
        this.cycle = Client.bz;
        this.type = var1;
        this.sender = var2;
        this.x();
        this.x = var3;
        this.message = var4;
    }

    @ObfuscatedName("t")
    void t(int var1, String var2, String var3, String var4) {
        int var5 = ++cg.a - 1;
        this.index = var5;
        this.cycle = Client.bz;
        this.type = var1;
        this.sender = var2;
        this.x();
        this.x = var3;
        this.message = var4;
    }

    @ObfuscatedName("q")
    void q() {
        this.b = kx.t;
    }

    @ObfuscatedName("i")
    final boolean i() {
        if (this.b == kx.t) {
            this.a();
        }

        return this.b == kx.q;
    }

    @ObfuscatedName("a")
    void a() {
        this.b = BoundaryObject.qi.l.z(this.l) ? kx.q : kx.i;
    }

    @ObfuscatedName("l")
    void l() {
        this.e = kx.t;
    }

    @ObfuscatedName("b")
    final boolean b() {
        if (this.e == kx.t) {
            this.e();
        }

        return this.e == kx.q;
    }

    @ObfuscatedName("e")
    void e() {
        this.e = BoundaryObject.qi.b.z(this.l) ? kx.q : kx.i;
    }

    @ObfuscatedName("x")
    final void x() {
        if (this.sender != null) {
            this.l = new kb(ScriptEvent.ka(this.sender), ad.bc);
        } else {
            this.l = null;
        }

    }

    @ObfuscatedName("t")
    static final void t(Packet var0) {
        var0.jc();
        int var1 = Client.localPlayerIndex;
        Player var2 = az.il = Client.loadedPlayers[var1] = new Player();
        var2.y = var1;
        int var3 = var0.jt(30);
        byte var4 = (byte) (var3 >> 28);
        int var5 = var3 >> 14 & 16383;
        int var6 = var3 & 16383;
        var2.co[0] = var5 - an.regionBaseX;
        var2.regionX = (var2.co[0] << 7) + (var2.x() << 6);
        var2.cv[0] = var6 - PlayerComposite.ep;
        var2.regionY = (var2.cv[0] << 7) + (var2.x() << 6);
        kt.ii = var2.r = var4;
        if (cx.l[var1] != null) {
            var2.t(cx.l[var1]);
        }

        cx.b = 0;
        cx.e[++cx.b - 1] = var1;
        cx.i[var1] = 0;
        cx.x = 0;

        for (int var7 = 1; var7 < 2048; ++var7) {
            if (var1 != var7) {
                int var8 = var0.jt(18);
                int var9 = var8 >> 16;
                int var10 = var8 >> 8 & 597;
                int var11 = var8 & 597;
                cx.g[var7] = (var10 << 14) + var11 + (var9 << 28);
                cx.n[var7] = 0;
                cx.o[var7] = -1;
                cx.p[++cx.x - 1] = var7;
                cx.i[var7] = 0;
            }
        }

        var0.ju();
    }

    @ObfuscatedName("a")
    public static String a(byte[] var0, int var1, int var2) {
        char[] var3 = new char[var2];
        int var4 = 0;

        for (int var5 = 0; var5 < var2; ++var5) {
            int var6 = var0[var5 + var1] & 255;
            if (var6 != 0) {
                if (var6 >= 128 && var6 < 160) {
                    char var7 = lv.t[var6 - 128];
                    if (var7 == 0) {
                        var7 = '?';
                    }

                    var6 = var7;
                }

                var3[var4++] = (char) var6;
            }
        }

        return new String(var3, 0, var4);
    }

    @ObfuscatedName("ib")
    static final void ib(RTComponent var0, ItemDefinition var1, int var2, int var3, boolean var4) {
        String[] var5 = var1.inventoryActions;
        byte var6 = -1;
        String var7 = null;
        if (var5 != null && var5[var3] != null) {
            if (var3 == 0) {
                var6 = 33;
            } else if (var3 == 1) {
                var6 = 34;
            } else if (var3 == 2) {
                var6 = 35;
            } else if (var3 == 3) {
                var6 = 36;
            } else {
                var6 = 37;
            }

            var7 = var5[var3];
        } else if (var3 == 4) {
            var6 = 37;
            var7 = "Drop";
        }

        if (var6 != -1 && var7 != null) {
            h.addMenuItem(var7, ar.q(16748608) + var1.name, var6, var1.id, var2, var0.id, var4);
        }

    }
}
