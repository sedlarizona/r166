import java.awt.Component;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ba")
public final class Canvas extends java.awt.Canvas {

    @ObfuscatedName("pz")
    static int pz;

    @ObfuscatedName("t")
    Component component;

    Canvas(Component var1) {
        this.component = var1;
    }

    public final void paint(Graphics var1) {
        this.component.paint(var1);
    }

    public final void update(Graphics var1) {
        this.component.update(var1);
    }

    @ObfuscatedName("q")
    static void q(File var0, File var1) {
        try {
            RSRandomAccessFile var2 = new RSRandomAccessFile(ar.a, "rw", 10000L);
            ByteBuffer var3 = new ByteBuffer(500);
            var3.a(3);
            var3.a(var1 != null ? 1 : 0);
            var3.w(var0.getPath());
            if (var1 != null) {
                var3.w("");
            }

            var2.q(var3.buffer, 0, var3.index);
            var2.i();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }

    @ObfuscatedName("b")
    static final void b(int var0, int var1, int var2, int var3, int var4, int var5, int var6) {
        int[] var7 = Region.bs(var0, var1, var2);
        int[] var8 = Region.bs(var3, var4, var5);
        li.dx(var7[0], var7[1], var8[0], var8[1], var6);
    }

    @ObfuscatedName("hj")
    static final void hj(int var0, int var1) {
        Deque var2 = Client.loadedGroundItems[kt.ii][var0][var1];
        if (var2 == null) {
            an.loadedRegion.av(kt.ii, var0, var1);
        } else {
            long var3 = -99999999L;
            ItemNode var5 = null;

            ItemNode var6;
            for (var6 = (ItemNode) var2.e(); var6 != null; var6 = (ItemNode) var2.p()) {
                ItemDefinition var7 = cs.loadItemDefinition(var6.id);
                long var8 = (long) var7.storeValue;
                if (var7.ay == 1) {
                    var8 *= (long) (var6.stackSize + 1);
                }

                if (var8 > var3) {
                    var3 = var8;
                    var5 = var6;
                }
            }

            if (var5 == null) {
                an.loadedRegion.av(kt.ii, var0, var1);
            } else {
                var2.i(var5);
                ItemNode var11 = null;
                ItemNode var10 = null;

                for (var6 = (ItemNode) var2.e(); var6 != null; var6 = (ItemNode) var2.p()) {
                    if (var5.id != var6.id) {
                        if (var11 == null) {
                            var11 = var6;
                        }

                        if (var11.id != var6.id && var10 == null) {
                            var10 = var6;
                        }
                    }
                }

                int var9 = var0 + (var1 << 7) + 1610612736;
                an.loadedRegion.x(kt.ii, var0, var1, ef.gn(var0 * 128 + 64, var1 * 128 + 64, kt.ii), var5, var9, var11,
                        var10);
            }
        }
    }

    @ObfuscatedName("hw")
    static final void hw(int var0, int var1, int var2, int var3) {
        for (int var4 = 0; var4 < Client.nm; ++var4) {
            if (Client.interfaceXPositions[var4] + Client.interfaceWidths[var4] > var0
                    && Client.interfaceXPositions[var4] < var0 + var2
                    && Client.interfaceHeights[var4] + Client.interfaceYPositions[var4] > var1
                    && Client.interfaceYPositions[var4] < var3 + var1) {
                Client.outdatedInterfaces[var4] = true;
            }
        }

    }
}
