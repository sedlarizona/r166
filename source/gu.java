import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gu")
public class gu implements fq {

    @ObfuscatedName("t")
    public static final gu t = new gu(14, 0);

    @ObfuscatedName("q")
    static final gu q = new gu(15, 4);

    @ObfuscatedName("i")
    public static final gu i = new gu(16, -2);

    @ObfuscatedName("a")
    public static final gu a = new gu(18, -2);

    @ObfuscatedName("b")
    static final gu[] b = new gu[32];

    @ObfuscatedName("h")
    static int[] h;

    @ObfuscatedName("l")
    public final int l;

    static {
        gu[] var0 = kj.t();

        for (int var1 = 0; var1 < var0.length; ++var1) {
            b[var0[var1].l] = var0[var1];
        }

    }

    gu(int var1, int var2) {
        this.l = var1;
    }
}
