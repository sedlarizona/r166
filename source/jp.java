import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jp")
public class jp extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static Cache q = new Cache(64);

    @ObfuscatedName("i")
    char i;

    @ObfuscatedName("a")
    public int a;

    @ObfuscatedName("l")
    public String l;

    @ObfuscatedName("b")
    boolean b = true;

    @ObfuscatedName("q")
    void q() {
    }

    @ObfuscatedName("i")
    void i(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.a(var1, var2);
        }
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.i = go.q(var1.aj());
        } else if (var2 == 2) {
            this.a = var1.ap();
        } else if (var2 == 4) {
            this.b = false;
        } else if (var2 == 5) {
            this.l = var1.ar();
        }

    }

    @ObfuscatedName("l")
    public boolean l() {
        return this.i == 's';
    }
}
