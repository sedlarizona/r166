import java.util.HashMap;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jo")
public class jo {

    @ObfuscatedName("p")
    static int[] p;

    @ObfuscatedName("t")
    final HashMap t = new HashMap();

    @ObfuscatedName("q")
    lb q = new lb(0, 0);

    @ObfuscatedName("i")
    int[] i = new int[2048];

    @ObfuscatedName("a")
    int[] a = new int[2048];

    @ObfuscatedName("l")
    int l = 0;

    public jo() {
        p = new int[2000];
        int var1 = 0;
        int var2 = 240;

        int var4;
        for (byte var3 = 12; var1 < 16; var2 -= var3) {
            var4 = o.t((double) ((float) var2 / 360.0F), 0.9998999834060669D,
                    (double) (0.075F + 0.425F * (float) var1 / 16.0F));
            p[var1] = var4;
            ++var1;
        }

        var2 = 48;

        for (int var6 = var2 / 6; var1 < p.length; var2 -= var6) {
            var4 = var1 * 2;

            for (int var5 = o.t((double) ((float) var2 / 360.0F), 0.9998999834060669D, 0.5D); var1 < var4
                    && var1 < p.length; ++var1) {
                p[var1] = var5;
            }
        }

    }

    @ObfuscatedName("t")
    void t(int var1) {
        int var2 = var1 * 2 + 1;
        double[] var3 = gv.q(0.0D, (double) ((float) var1 / 3.0F), var1);
        double var4 = var3[var1] * var3[var1];
        int[] var6 = new int[var2 * var2];
        boolean var7 = false;

        for (int var8 = 0; var8 < var2; ++var8) {
            for (int var9 = 0; var9 < var2; ++var9) {
                int var10 = var6[var9 + var8 * var2] = (int) (var3[var8] * var3[var9] / var4 * 256.0D);
                if (!var7 && var10 > 0) {
                    var7 = true;
                }
            }
        }

        Sprite var11 = new Sprite(var6, var2, var2);
        this.t.put(var1, var11);
    }

    @ObfuscatedName("q")
    Sprite q(int var1) {
        if (!this.t.containsKey(var1)) {
            this.t(var1);
        }

        return (Sprite) this.t.get(var1);
    }

    @ObfuscatedName("i")
    public final void i(int var1, int var2) {
        if (this.l < this.i.length) {
            this.i[this.l] = var1;
            this.a[this.l] = var2;
            ++this.l;
        }
    }

    @ObfuscatedName("a")
    public final void a() {
        this.l = 0;
    }

    @ObfuscatedName("l")
    public final void l(int var1, int var2, Sprite var3, float var4) {
        int var5 = (int) (var4 * 18.0F);
        Sprite var6 = this.q(var5);
        int var7 = var5 * 2 + 1;
        lb var8 = new lb(0, 0, var3.width, var3.height);
        lb var9 = new lb(0, 0);
        this.q.q(var7, var7);
        System.nanoTime();

        int var10;
        int var11;
        int var12;
        for (var10 = 0; var10 < this.l; ++var10) {
            var11 = this.i[var10];
            var12 = this.a[var10];
            int var13 = (int) (var4 * (float) (var11 - var1)) - var5;
            int var14 = (int) ((float) var3.height - var4 * (float) (var12 - var2)) - var5;
            this.q.t(var13, var14);
            this.q.i(var8, var9);
            this.b(var6, var3, var9);
        }

        System.nanoTime();
        System.nanoTime();

        for (var10 = 0; var10 < var3.pixels.length; ++var10) {
            if (var3.pixels[var10] == 0) {
                var3.pixels[var10] = -16777216;
            } else {
                var11 = (var3.pixels[var10] + 64 - 1) / 256;
                if (var11 <= 0) {
                    var3.pixels[var10] = -16777216;
                } else {
                    if (var11 > p.length) {
                        var11 = p.length;
                    }

                    var12 = p[var11 - 1];
                    var3.pixels[var10] = -16777216 | var12;
                }
            }
        }

        System.nanoTime();
    }

    @ObfuscatedName("b")
    void b(Sprite var1, Sprite var2, lb var3) {
        if (var3.i != 0 && var3.a != 0) {
            int var4 = 0;
            int var5 = 0;
            if (var3.t == 0) {
                var4 = var1.width - var3.i;
            }

            if (var3.q == 0) {
                var5 = var1.height - var3.a;
            }

            int var6 = var4 + var5 * var1.width;
            int var7 = var3.t + var2.width * var3.q;

            for (int var8 = 0; var8 < var3.a; ++var8) {
                for (int var9 = 0; var9 < var3.i; ++var9) {
                    int var10001 = var7++;
                    var2.pixels[var10001] += var1.pixels[var6++];
                }

                var6 += var1.width - var3.i;
                var7 += var2.width - var3.i;
            }

        }
    }
}
