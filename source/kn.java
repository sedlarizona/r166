import java.util.HashMap;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kn")
public class kn {

    @ObfuscatedName("t")
    FileSystem t;

    @ObfuscatedName("q")
    FileSystem q;

    @ObfuscatedName("i")
    HashMap i;

    public kn(FileSystem var1, FileSystem var2) {
        this.t = var1;
        this.q = var2;
        this.i = new HashMap();
    }

    @ObfuscatedName("t")
    public HashMap t(kq[] var1) {
        HashMap var2 = new HashMap();
        kq[] var3 = var1;

        for (int var4 = 0; var4 < var3.length; ++var4) {
            kq var5 = var3[var4];
            if (this.i.containsKey(var5)) {
                var2.put(var5, this.i.get(var5));
            } else {
                km var6 = fz.b(this.t, this.q, var5.x, "");
                if (var6 != null) {
                    this.i.put(var5, var6);
                    var2.put(var5, var6);
                }
            }
        }

        return var2;
    }

    @ObfuscatedName("k")
    public static String k(CharSequence var0) {
        return kb.u('*', var0.length());
    }

    @ObfuscatedName("hv")
    static void hv() {
        if (Client.interfaceSelected) {
            RTComponent var0 = CollisionMap.q(dt.kr, Client.kq);
            if (var0 != null && var0.df != null) {
                ScriptEvent var1 = new ScriptEvent();
                var1.i = var0;
                var1.args = var0.df;
                m.t(var1, 500000);
            }

            Client.interfaceSelected = false;
            GameEngine.jk(var0);
        }
    }
}
