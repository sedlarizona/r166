import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jq")
public class jq extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static Cache q = new Cache(64);

    @ObfuscatedName("c")
    static dc c;

    @ObfuscatedName("i")
    public boolean i = false;

    @ObfuscatedName("t")
    public void t(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.q(var1, var2);
        }
    }

    @ObfuscatedName("q")
    void q(ByteBuffer var1, int var2) {
        if (var2 == 2) {
            this.i = true;
        }

    }

    @ObfuscatedName("hd")
    static void hd(int var0, int var1) {
        gd var2 = ap.t(fo.cg, Client.ee.l);
        var2.i.bn(var1);
        var2.i.bw(var0);
        Client.ee.i(var2);
    }
}
