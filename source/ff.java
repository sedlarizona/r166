import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ff")
public enum ff implements gl {
    @ObfuscatedName("t")
    t(1, 0), @ObfuscatedName("q")
    q(2, 1), @ObfuscatedName("i")
    i(3, 2), @ObfuscatedName("a")
    a(0, 3);

    @ObfuscatedName("fq")
    static lk[] fq;

    @ObfuscatedName("l")
    public final int l;

    @ObfuscatedName("b")
    final int b;

    ff(int var3, int var4) {
        this.l = var3;
        this.b = var4;
    }

    @ObfuscatedName("t")
    public int t() {
        return this.b;
    }

    @ObfuscatedName("t")
    public static kf t(int var0) {
        kf var1 = (kf) kf.a.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = i.t.i(12, var0);
            var1 = new kf();
            if (var2 != null) {
                var1.q(new ByteBuffer(var2));
            }

            var1.a();
            kf.a.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("i")
    public static int i(CharSequence var0) {
        return ju.l(var0, 10, true);
    }
}
