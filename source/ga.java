import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ga")
public class ga {

    @ObfuscatedName("pj")
    static int pj;

    @ObfuscatedName("t")
    public static final ga t = new ga(14);

    @ObfuscatedName("q")
    public static final ga q = new ga(6);

    @ObfuscatedName("i")
    public static final ga i = new ga(2);

    @ObfuscatedName("a")
    public static final ga a = new ga(5);

    @ObfuscatedName("l")
    public static final ga l = new ga(3);

    @ObfuscatedName("b")
    public static final ga b = new ga(4);

    @ObfuscatedName("e")
    public static final ga e = new ga(15);

    @ObfuscatedName("x")
    public static final ga x = new ga(7);

    @ObfuscatedName("p")
    public static final ga p = new ga(4);

    @ObfuscatedName("g")
    public static final ga g = new ga(5);

    ga(int var1) {
    }

    @ObfuscatedName("q")
    public static kw loadObjectDefinition(int var0) {
        kw var1 = (kw) kw.q.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = kw.t.i(4, var0);
            var1 = new kw();
            if (var2 != null) {
                var1.a(new ByteBuffer(var2), var0);
            }

            var1.i();
            kw.q.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("q")
    public static void q(FileSystem var0, String var1, String var2, int var3, boolean var4) {
        int var5 = var0.h(var1);
        int var6 = var0.av(var5, var2);
        CombatBarData.i(var0, var5, var6, var3, var4);
    }

    @ObfuscatedName("hm")
    static final void hm() {
        for (bl var0 = (bl) Client.jw.e(); var0 != null; var0 = (bl) Client.jw.p()) {
            if (var0.o > 0) {
                --var0.o;
            }

            boolean var1;
            int var2;
            int var3;
            ObjectDefinition var4;
            if (var0.o == 0) {
                if (var0.l >= 0) {
                    var2 = var0.l;
                    var3 = var0.e;
                    var4 = jw.q(var2);
                    if (var3 == 11) {
                        var3 = 10;
                    }

                    if (var3 >= 5 && var3 <= 8) {
                        var3 = 4;
                    }

                    var1 = var4.b(var3);
                    if (!var1) {
                        continue;
                    }
                }

                bc.hz(var0.t, var0.q, var0.i, var0.a, var0.l, var0.b, var0.e);
                var0.kc();
            } else {
                if (var0.n > 0) {
                    --var0.n;
                }

                if (var0.n == 0 && var0.i >= 1 && var0.a >= 1 && var0.i <= 102 && var0.a <= 102) {
                    if (var0.x >= 0) {
                        var2 = var0.x;
                        var3 = var0.g;
                        var4 = jw.q(var2);
                        if (var3 == 11) {
                            var3 = 10;
                        }

                        if (var3 >= 5 && var3 <= 8) {
                            var3 = 4;
                        }

                        var1 = var4.b(var3);
                        if (!var1) {
                            continue;
                        }
                    }

                    bc.hz(var0.t, var0.q, var0.i, var0.a, var0.x, var0.p, var0.g);
                    var0.n = -1;
                    if (var0.l == var0.x && var0.l == -1) {
                        var0.kc();
                    } else if (var0.l == var0.x && var0.p == var0.b && var0.e == var0.g) {
                        var0.kc();
                    }
                }
            }
        }

    }
}
