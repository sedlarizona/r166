import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.imageio.ImageIO;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("v")
public class v {

    @ObfuscatedName("q")
    public static Comparator q = new s();

    @ObfuscatedName("i")
    public static Comparator i;

    @ObfuscatedName("a")
    public static Comparator a;

    @ObfuscatedName("l")
    public static Comparator l;

    @ObfuscatedName("k")
    static ak k;

    @ObfuscatedName("t")
    public final List t;

    static {
        new c();
        i = new z();
        a = new w();
        l = new j();
    }

    public v(ByteBuffer var1, boolean var2) {
        int var3 = var1.ae();
        boolean var4 = var1.av() == 1;
        byte var5;
        if (var4) {
            var5 = 1;
        } else {
            var5 = 0;
        }

        int var6 = var1.ae();
        this.t = new ArrayList(var6);

        for (int var7 = 0; var7 < var6; ++var7) {
            this.t.add(new u(var1, var5, var3));
        }

    }

    @ObfuscatedName("t")
    public void t(Comparator var1, boolean var2) {
        if (var2) {
            Collections.sort(this.t, var1);
        } else {
            Collections.sort(this.t, Collections.reverseOrder(var1));
        }

    }

    @ObfuscatedName("q")
    static void q(FileSystem var0, FileSystem var1, boolean var2, int var3) {
        if (ci.t) {
            if (var3 == 4) {
                ci.loginState = 4;
            }

        } else {
            ci.loginState = var3;
            li.dr();
            byte[] var4 = var0.ae("title.jpg", "");
            BufferedImage var6 = null;

            Sprite var5;
            label161: {
                try {
                    var6 = ImageIO.read(new ByteArrayInputStream(var4));
                    int var7 = var6.getWidth();
                    int var8 = var6.getHeight();
                    int[] var9 = new int[var7 * var8];
                    PixelGrabber var10 = new PixelGrabber(var6, 0, 0, var7, var8, var9, 0, var7);
                    var10.grabPixels();
                    var5 = new Sprite(var9, var7, var8);
                    break label161;
                } catch (IOException var13) {
                    ;
                } catch (InterruptedException var14) {
                    ;
                }

                var5 = new Sprite(0, 0);
            }

            ItemNode.b = var5;
            iv.e = ItemNode.b.t();
            if ((Client.br & 536870912) != 0) {
                ii.x = Renderable.a(var1, "logo_deadman_mode", "");
            } else {
                ii.x = Renderable.a(var1, "logo", "");
            }

            ci.i = Renderable.a(var1, "titlebox", "");
            ib.a = Renderable.a(var1, "titlebutton", "");
            io.l = az.i(var1, "runes", "");
            ci.p = az.i(var1, "title_mute", "");
            ez.g = Renderable.a(var1, "options_radio_buttons,0", "");
            fk.n = Renderable.a(var1, "options_radio_buttons,4", "");
            ci.o = Renderable.a(var1, "options_radio_buttons,2", "");
            ci.c = Renderable.a(var1, "options_radio_buttons,6", "");
            x.ab = ez.g.i;
            jl.ac = ez.g.a;
            ah.r = new int[256];

            int var12;
            for (var12 = 0; var12 < 64; ++var12) {
                ah.r[var12] = var12 * 262144;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                ah.r[var12 + 64] = var12 * 1024 + 16711680;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                ah.r[var12 + 128] = var12 * 4 + 16776960;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                ah.r[var12 + 192] = 16777215;
            }

            ho.y = new int[256];

            for (var12 = 0; var12 < 64; ++var12) {
                ho.y[var12] = var12 * 1024;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                ho.y[var12 + 64] = var12 * 4 + '\uff00';
            }

            for (var12 = 0; var12 < 64; ++var12) {
                ho.y[var12 + 128] = var12 * 262144 + '\uffff';
            }

            for (var12 = 0; var12 < 64; ++var12) {
                ho.y[var12 + 192] = 16777215;
            }

            fz.h = new int[256];

            for (var12 = 0; var12 < 64; ++var12) {
                fz.h[var12] = var12 * 4;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                fz.h[var12 + 64] = var12 * 262144 + 255;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                fz.h[var12 + 128] = var12 * 1024 + 16711935;
            }

            for (var12 = 0; var12 < 64; ++var12) {
                fz.h[var12 + 192] = 16777215;
            }

            ItemStorage.f = new int[256];
            ao.ao = new int['耀'];
            TileModel.av = new int['耀'];
            js.o((lk) null);
            hd.aj = new int['耀'];
            bk.ae = new int['耀'];
            if (var2) {
                ci.username = "";
                ci.password = "";
            }

            AreaSoundEmitter.br = 0;
            ik.ba = "";
            ci.bk = true;
            ci.isWorldSelectorOpen = false;
            if (!al.qp.loadingAudioDisabled) {
                bn.b(2, Varpbit.ca, "scape main", "", 255, false);
            } else {
                hi.audioTrackId = 1;
                hi.b = null;
                fm.e = -1;
                hi.x = -1;
                hi.p = 0;
                cr.n = false;
                i.g = 2;
            }

            in.t(false);
            ci.t = true;
            ci.worldCount = (BoundaryObject.r - 765) / 2;
            ci.z = ci.worldCount + 202;
            ik.w = ci.z + 180;
            ItemNode.b.o(ci.worldCount, 0);
            iv.e.o(ci.worldCount + 382, 0);
            ii.x.i(ci.worldCount + 382 - ii.x.i / 2, 18);
        }
    }

    @ObfuscatedName("i")
    public static final void i(int var0, int var1, int var2, int var3, int var4) {
        x.l.q(new p(var0, var1, var2, var3, var4));
    }

    @ObfuscatedName("hf")
    static void hf() {
        z.hx();
        Client.menuActions[0] = "Cancel";
        Client.menuTargets[0] = "";
        Client.menuOpcodes[0] = 1006;
        Client.kc[0] = false;
        Client.menuSize = 1;
    }
}
