import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("kw")
public class kw extends hh {

    @ObfuscatedName("t")
    static FileSystem t;

    @ObfuscatedName("q")
    static Cache q = new Cache(64);

    @ObfuscatedName("i")
    public int i = 0;

    @ObfuscatedName("a")
    public int a = -1;

    @ObfuscatedName("l")
    public boolean l = true;

    @ObfuscatedName("b")
    public int b = -1;

    @ObfuscatedName("e")
    public int e;

    @ObfuscatedName("x")
    public int x;

    @ObfuscatedName("p")
    public int p;

    @ObfuscatedName("g")
    public int g;

    @ObfuscatedName("n")
    public int n;

    @ObfuscatedName("o")
    public int o;

    @ObfuscatedName("i")
    void i() {
        if (this.b != -1) {
            this.b(this.b);
            this.g = this.e;
            this.n = this.x;
            this.o = this.p;
        }

        this.b(this.i);
    }

    @ObfuscatedName("a")
    void a(ByteBuffer var1, int var2) {
        while (true) {
            int var3 = var1.av();
            if (var3 == 0) {
                return;
            }

            this.l(var1, var3, var2);
        }
    }

    @ObfuscatedName("l")
    void l(ByteBuffer var1, int var2, int var3) {
        if (var2 == 1) {
            this.i = var1.az();
        } else if (var2 == 2) {
            this.a = var1.av();
        } else if (var2 == 5) {
            this.l = false;
        } else if (var2 == 7) {
            this.b = var1.az();
        } else if (var2 == 8) {
            ;
        }

    }

    @ObfuscatedName("b")
    void b(int var1) {
        double var2 = (double) (var1 >> 16 & 255) / 256.0D;
        double var4 = (double) (var1 >> 8 & 255) / 256.0D;
        double var6 = (double) (var1 & 255) / 256.0D;
        double var8 = var2;
        if (var4 < var2) {
            var8 = var4;
        }

        if (var6 < var8) {
            var8 = var6;
        }

        double var10 = var2;
        if (var4 > var2) {
            var10 = var4;
        }

        if (var6 > var10) {
            var10 = var6;
        }

        double var12 = 0.0D;
        double var14 = 0.0D;
        double var16 = (var8 + var10) / 2.0D;
        if (var10 != var8) {
            if (var16 < 0.5D) {
                var14 = (var10 - var8) / (var10 + var8);
            }

            if (var16 >= 0.5D) {
                var14 = (var10 - var8) / (2.0D - var10 - var8);
            }

            if (var2 == var10) {
                var12 = (var4 - var6) / (var10 - var8);
            } else if (var4 == var10) {
                var12 = 2.0D + (var6 - var2) / (var10 - var8);
            } else if (var10 == var6) {
                var12 = 4.0D + (var2 - var4) / (var10 - var8);
            }
        }

        var12 /= 6.0D;
        this.e = (int) (256.0D * var12);
        this.x = (int) (var14 * 256.0D);
        this.p = (int) (var16 * 256.0D);
        if (this.x < 0) {
            this.x = 0;
        } else if (this.x > 255) {
            this.x = 255;
        }

        if (this.p < 0) {
            this.p = 0;
        } else if (this.p > 255) {
            this.p = 255;
        }

    }
}
