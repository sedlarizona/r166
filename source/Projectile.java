import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("cc")
public final class Projectile extends Renderable {

    @ObfuscatedName("bt")
    static String bt;

    @ObfuscatedName("t")
    int spotAnimationId;

    @ObfuscatedName("q")
    int plane;

    @ObfuscatedName("i")
    int startRegionX;

    @ObfuscatedName("a")
    int startRegionY;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int endHeight;

    @ObfuscatedName("e")
    int startCycle;

    @ObfuscatedName("x")
    int endCycle;

    @ObfuscatedName("p")
    int slope;

    @ObfuscatedName("g")
    int startTargetDistance;

    @ObfuscatedName("n")
    int targetIndex;

    @ObfuscatedName("o")
    boolean launched = false;

    @ObfuscatedName("c")
    double c;

    @ObfuscatedName("v")
    double v;

    @ObfuscatedName("u")
    double u;

    @ObfuscatedName("j")
    double j;

    @ObfuscatedName("k")
    double k;

    @ObfuscatedName("z")
    double z;

    @ObfuscatedName("w")
    double w;

    @ObfuscatedName("s")
    double s;

    @ObfuscatedName("d")
    int d;

    @ObfuscatedName("f")
    int orientation;

    @ObfuscatedName("r")
    kf animation;

    @ObfuscatedName("y")
    int y = 0;

    @ObfuscatedName("h")
    int h = 0;

    Projectile(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10,
            int var11) {
        this.spotAnimationId = var1;
        this.plane = var2;
        this.startRegionX = var3;
        this.startRegionY = var4;
        this.l = var5;
        this.startCycle = var6;
        this.endCycle = var7;
        this.slope = var8;
        this.startTargetDistance = var9;
        this.targetIndex = var10;
        this.endHeight = var11;
        this.launched = false;
        int var12 = ah.t(this.spotAnimationId).e;
        if (var12 != -1) {
            this.animation = ff.t(var12);
        } else {
            this.animation = null;
        }

    }

    @ObfuscatedName("t")
    final void t(int var1, int var2, int var3, int var4) {
        double var5;
        if (!this.launched) {
            var5 = (double) (var1 - this.startRegionX);
            double var7 = (double) (var2 - this.startRegionY);
            double var9 = Math.sqrt(var7 * var7 + var5 * var5);
            this.c = var5 * (double) this.startTargetDistance / var9 + (double) this.startRegionX;
            this.v = var7 * (double) this.startTargetDistance / var9 + (double) this.startRegionY;
            this.u = (double) this.l;
        }

        var5 = (double) (this.endCycle + 1 - var4);
        this.j = ((double) var1 - this.c) / var5;
        this.k = ((double) var2 - this.v) / var5;
        this.z = Math.sqrt(this.k * this.k + this.j * this.j);
        if (!this.launched) {
            this.w = -this.z * Math.tan((double) this.slope * 0.02454369D);
        }

        this.s = 2.0D * ((double) var3 - this.u - this.w * var5) / (var5 * var5);
    }

    @ObfuscatedName("q")
    final void q(int var1) {
        this.launched = true;
        this.c += (double) var1 * this.j;
        this.v += (double) var1 * this.k;
        this.u += 0.5D * this.s * (double) var1 * (double) var1 + this.w * (double) var1;
        this.w += (double) var1 * this.s;
        this.d = (int) (Math.atan2(this.j, this.k) * 325.949D) + 1024 & 2047;
        this.orientation = (int) (Math.atan2(this.w, this.z) * 325.949D) & 2047;
        if (this.animation != null) {
            this.h += var1;

            while (true) {
                do {
                    do {
                        if (this.h <= this.animation.x[this.y]) {
                            return;
                        }

                        this.h -= this.animation.x[this.y];
                        ++this.y;
                    } while (this.y < this.animation.b.length);

                    this.y -= this.animation.g;
                } while (this.y >= 0 && this.y < this.animation.b.length);

                this.y = 0;
            }
        }
    }

    @ObfuscatedName("p")
    protected final Model p() {
        jw var1 = ah.t(this.spotAnimationId);
        Model var2 = var1.a(this.y);
        if (var2 == null) {
            return null;
        } else {
            var2.d(this.orientation);
            return var2;
        }
    }

    @ObfuscatedName("a")
    public static boolean a(int var0) {
        return var0 >= ij.d.y && var0 <= ij.f.y;
    }

    @ObfuscatedName("a")
    static final void a(int var0, int var1, int var2) {
        int var3;
        for (var3 = 0; var3 < 8; ++var3) {
            for (int var4 = 0; var4 < 8; ++var4) {
                bt.tileHeights[var0][var3 + var1][var4 + var2] = 0;
            }
        }

        if (var1 > 0) {
            for (var3 = 1; var3 < 8; ++var3) {
                bt.tileHeights[var0][var1][var3 + var2] = bt.tileHeights[var0][var1 - 1][var3 + var2];
            }
        }

        if (var2 > 0) {
            for (var3 = 1; var3 < 8; ++var3) {
                bt.tileHeights[var0][var3 + var1][var2] = bt.tileHeights[var0][var3 + var1][var2 - 1];
            }
        }

        if (var1 > 0 && bt.tileHeights[var0][var1 - 1][var2] != 0) {
            bt.tileHeights[var0][var1][var2] = bt.tileHeights[var0][var1 - 1][var2];
        } else if (var2 > 0 && bt.tileHeights[var0][var1][var2 - 1] != 0) {
            bt.tileHeights[var0][var1][var2] = bt.tileHeights[var0][var1][var2 - 1];
        } else if (var1 > 0 && var2 > 0 && bt.tileHeights[var0][var1 - 1][var2 - 1] != 0) {
            bt.tileHeights[var0][var1][var2] = bt.tileHeights[var0][var1 - 1][var2 - 1];
        }

    }
}
