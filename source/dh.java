import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dh")
public class dh implements Runnable {

    @ObfuscatedName("ra")
    static int ra;

    @ObfuscatedName("t")
    volatile TaskData[] t = new TaskData[2];

    public void run() {
        try {
            for (int var1 = 0; var1 < 2; ++var1) {
                TaskData var2 = this.t[var1];
                if (var2 != null) {
                    var2.ap();
                }
            }
        } catch (Exception var4) {
            FloorObject.t((String) null, var4);
        }

    }
}
