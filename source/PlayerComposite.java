import java.awt.Component;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("if")
public class PlayerComposite {

    @ObfuscatedName("e")
    public static short[] e;

    @ObfuscatedName("x")
    public static short[][] x;

    @ObfuscatedName("g")
    public static short[][] g;

    @ObfuscatedName("n")
    static final int[] n = new int[] { 8, 11, 4, 6, 9, 7, 10 };

    @ObfuscatedName("o")
    static Cache o = new Cache(260);

    @ObfuscatedName("ep")
    static int ep;

    @ObfuscatedName("t")
    int[] t;

    @ObfuscatedName("q")
    int[] appearance;

    @ObfuscatedName("i")
    public boolean female;

    @ObfuscatedName("a")
    public int npcId;

    @ObfuscatedName("l")
    long modelHash;

    @ObfuscatedName("b")
    long b;

    @ObfuscatedName("t")
    public void t(int[] var1, int[] var2, boolean var3, int var4) {
        if (var1 == null) {
            var1 = new int[12];

            for (int var5 = 0; var5 < 7; ++var5) {
                for (int var6 = 0; var6 < jf.i; ++var6) {
                    jf var7 = an.regionChanged(var6);
                    if (var7 != null && !var7.o && var7.l == var5 + (var3 ? 7 : 0)) {
                        var1[n[var5]] = var6 + 256;
                        break;
                    }
                }
            }
        }

        this.t = var1;
        this.appearance = var2;
        this.female = var3;
        this.npcId = var4;
        this.b();
    }

    @ObfuscatedName("q")
    public void q(int var1, boolean var2) {
        if (var1 != 1 || !this.female) {
            int var3 = this.t[n[var1]];
            if (var3 != 0) {
                var3 -= 256;

                jf var4;
                do {
                    if (!var2) {
                        --var3;
                        if (var3 < 0) {
                            var3 = jf.i - 1;
                        }
                    } else {
                        ++var3;
                        if (var3 >= jf.i) {
                            var3 = 0;
                        }
                    }

                    var4 = an.regionChanged(var3);
                } while (var4 == null || var4.o || var1 + (this.female ? 7 : 0) != var4.l);

                this.t[n[var1]] = var3 + 256;
                this.b();
            }
        }
    }

    @ObfuscatedName("i")
    public void i(int var1, boolean var2) {
        int var3 = this.appearance[var1];
        if (!var2) {
            do {
                --var3;
                if (var3 < 0) {
                    var3 = x[var1].length - 1;
                }
            } while (!gk.t(var1, var3));
        } else {
            do {
                ++var3;
                if (var3 >= x[var1].length) {
                    var3 = 0;
                }
            } while (!gk.t(var1, var3));
        }

        this.appearance[var1] = var3;
        this.b();
    }

    @ObfuscatedName("a")
    public void a(boolean var1) {
        if (this.female != var1) {
            this.t((int[]) null, this.appearance, var1, -1);
        }
    }

    @ObfuscatedName("l")
    public void l(ByteBuffer var1) {
        var1.a(this.female ? 1 : 0);

        int var2;
        for (var2 = 0; var2 < 7; ++var2) {
            int var3 = this.t[n[var2]];
            if (var3 == 0) {
                var1.a(-1);
            } else {
                var1.a(var3 - 256);
            }
        }

        for (var2 = 0; var2 < 5; ++var2) {
            var1.a(this.appearance[var2]);
        }

    }

    @ObfuscatedName("b")
    void b() {
        long var1 = this.modelHash;
        int var3 = this.t[5];
        int var4 = this.t[9];
        this.t[5] = var4;
        this.t[9] = var3;
        this.modelHash = 0L;

        int var5;
        for (var5 = 0; var5 < 12; ++var5) {
            this.modelHash <<= 4;
            if (this.t[var5] >= 256) {
                this.modelHash += (long) (this.t[var5] - 256);
            }
        }

        if (this.t[0] >= 256) {
            this.modelHash += (long) (this.t[0] - 256 >> 4);
        }

        if (this.t[1] >= 256) {
            this.modelHash += (long) (this.t[1] - 256 >> 8);
        }

        for (var5 = 0; var5 < 5; ++var5) {
            this.modelHash <<= 3;
            this.modelHash += (long) this.appearance[var5];
        }

        this.modelHash <<= 1;
        this.modelHash += (long) (this.female ? 1 : 0);
        this.t[5] = var3;
        this.t[9] = var4;
        if (0L != var1 && this.modelHash != var1) {
            o.q(var1);
        }

    }

    @ObfuscatedName("e")
    public Model e(kf var1, int var2, kf var3, int var4) {
        if (this.npcId != -1) {
            return ho.q(this.npcId).b(var1, var2, var3, var4);
        } else {
            long var5 = this.modelHash;
            int[] var7 = this.t;
            if (var1 != null && (var1.v >= 0 || var1.u >= 0)) {
                var7 = new int[12];

                for (int var15 = 0; var15 < 12; ++var15) {
                    var7[var15] = this.t[var15];
                }

                if (var1.v >= 0) {
                    var5 += (long) (var1.v - this.t[5] << 40);
                    var7[5] = var1.v;
                }

                if (var1.u >= 0) {
                    var5 += (long) (var1.u - this.t[3] << 48);
                    var7[3] = var1.u;
                }
            }

            Model var8 = (Model) o.t(var5);
            if (var8 == null) {
                boolean var9 = false;

                int var11;
                for (int var10 = 0; var10 < 12; ++var10) {
                    var11 = var7[var10];
                    if (var11 >= 256 && var11 < 512 && !an.regionChanged(var11 - 256).a()) {
                        var9 = true;
                    }

                    if (var11 >= 512 && !cs.loadItemDefinition(var11 - 512).k(this.female)) {
                        var9 = true;
                    }
                }

                if (var9) {
                    if (-1L != this.b) {
                        var8 = (Model) o.t(this.b);
                    }

                    if (var8 == null) {
                        return null;
                    }
                }

                if (var8 == null) {
                    AlternativeModel[] var16 = new AlternativeModel[12];
                    var11 = 0;

                    int var13;
                    for (int var12 = 0; var12 < 12; ++var12) {
                        var13 = var7[var12];
                        AlternativeModel var14;
                        if (var13 >= 256 && var13 < 512) {
                            var14 = an.regionChanged(var13 - 256).l();
                            if (var14 != null) {
                                var16[var11++] = var14;
                            }
                        }

                        if (var13 >= 512) {
                            var14 = cs.loadItemDefinition(var13 - 512).z(this.female);
                            if (var14 != null) {
                                var16[var11++] = var14;
                            }
                        }
                    }

                    AlternativeModel var18 = new AlternativeModel(var16, var11);

                    for (var13 = 0; var13 < 5; ++var13) {
                        if (this.appearance[var13] < x[var13].length) {
                            var18.z(e[var13], x[var13][this.appearance[var13]]);
                        }

                        if (this.appearance[var13] < g[var13].length) {
                            var18.z(c.p[var13], g[var13][this.appearance[var13]]);
                        }
                    }

                    var8 = var18.av(64, 850, -30, -50, -30);
                    o.i(var8, var5);
                    this.b = var5;
                }
            }

            if (var1 == null && var3 == null) {
                return var8;
            } else {
                Model var17;
                if (var1 != null && var3 != null) {
                    var17 = var1.x(var8, var2, var3, var4);
                } else if (var1 != null) {
                    var17 = var1.l(var8, var2);
                } else {
                    var17 = var3.l(var8, var4);
                }

                return var17;
            }
        }
    }

    @ObfuscatedName("x")
    AlternativeModel x() {
        if (this.npcId != -1) {
            return ho.q(this.npcId).e();
        } else {
            boolean var1 = false;

            int var3;
            for (int var2 = 0; var2 < 12; ++var2) {
                var3 = this.t[var2];
                if (var3 >= 256 && var3 < 512 && !an.regionChanged(var3 - 256).b()) {
                    var1 = true;
                }

                if (var3 >= 512 && !cs.loadItemDefinition(var3 - 512).w(this.female)) {
                    var1 = true;
                }
            }

            if (var1) {
                return null;
            } else {
                AlternativeModel[] var7 = new AlternativeModel[12];
                var3 = 0;

                int var5;
                for (int var4 = 0; var4 < 12; ++var4) {
                    var5 = this.t[var4];
                    AlternativeModel var6;
                    if (var5 >= 256 && var5 < 512) {
                        var6 = an.regionChanged(var5 - 256).e();
                        if (var6 != null) {
                            var7[var3++] = var6;
                        }
                    }

                    if (var5 >= 512) {
                        var6 = cs.loadItemDefinition(var5 - 512).s(this.female);
                        if (var6 != null) {
                            var7[var3++] = var6;
                        }
                    }
                }

                AlternativeModel var8 = new AlternativeModel(var7, var3);

                for (var5 = 0; var5 < 5; ++var5) {
                    if (this.appearance[var5] < x[var5].length) {
                        var8.z(e[var5], x[var5][this.appearance[var5]]);
                    }

                    if (this.appearance[var5] < g[var5].length) {
                        var8.z(c.p[var5], g[var5][this.appearance[var5]]);
                    }
                }

                return var8;
            }
        }
    }

    @ObfuscatedName("p")
    public int p() {
        return this.npcId == -1 ? (this.t[0] << 15) + this.t[1] + (this.t[11] << 5) + (this.t[8] << 10)
                + (this.appearance[0] << 25) + (this.appearance[4] << 20) : 305419896 + ho.q(this.npcId).id;
    }

    @ObfuscatedName("t")
    public static int t(ByteBuffer var0, String var1) {
        int var2 = var0.index;
        int var4 = var1.length();
        byte[] var5 = new byte[var4];

        for (int var6 = 0; var6 < var4; ++var6) {
            char var7 = var1.charAt(var6);
            if ((var7 <= 0 || var7 >= 128) && (var7 < 160 || var7 > 255)) {
                if (var7 == 8364) {
                    var5[var6] = -128;
                } else if (var7 == 8218) {
                    var5[var6] = -126;
                } else if (var7 == 402) {
                    var5[var6] = -125;
                } else if (var7 == 8222) {
                    var5[var6] = -124;
                } else if (var7 == 8230) {
                    var5[var6] = -123;
                } else if (var7 == 8224) {
                    var5[var6] = -122;
                } else if (var7 == 8225) {
                    var5[var6] = -121;
                } else if (var7 == 710) {
                    var5[var6] = -120;
                } else if (var7 == 8240) {
                    var5[var6] = -119;
                } else if (var7 == 352) {
                    var5[var6] = -118;
                } else if (var7 == 8249) {
                    var5[var6] = -117;
                } else if (var7 == 338) {
                    var5[var6] = -116;
                } else if (var7 == 381) {
                    var5[var6] = -114;
                } else if (var7 == 8216) {
                    var5[var6] = -111;
                } else if (var7 == 8217) {
                    var5[var6] = -110;
                } else if (var7 == 8220) {
                    var5[var6] = -109;
                } else if (var7 == 8221) {
                    var5[var6] = -108;
                } else if (var7 == 8226) {
                    var5[var6] = -107;
                } else if (var7 == 8211) {
                    var5[var6] = -106;
                } else if (var7 == 8212) {
                    var5[var6] = -105;
                } else if (var7 == 732) {
                    var5[var6] = -104;
                } else if (var7 == 8482) {
                    var5[var6] = -103;
                } else if (var7 == 353) {
                    var5[var6] = -102;
                } else if (var7 == 8250) {
                    var5[var6] = -101;
                } else if (var7 == 339) {
                    var5[var6] = -100;
                } else if (var7 == 382) {
                    var5[var6] = -98;
                } else if (var7 == 376) {
                    var5[var6] = -97;
                } else {
                    var5[var6] = 63;
                }
            } else {
                var5[var6] = (byte) var7;
            }
        }

        var0.y(var5.length);
        var0.index += lw.t.t(var5, 0, var5.length, var0.buffer, var0.index);
        return var0.index - var2;
    }

    @ObfuscatedName("q")
    static void q(Component var0) {
        var0.setFocusTraversalKeysEnabled(false);
        var0.addKeyListener(ad.keyboard);
        var0.addFocusListener(ad.keyboard);
    }

    @ObfuscatedName("i")
    static void i(Packet var0, int var1) {
        boolean var2 = var0.jt(1) == 1;
        if (var2) {
            cx.v[++cx.c - 1] = var1;
        }

        int var3 = var0.jt(2);
        Player var4 = Client.loadedPlayers[var1];
        if (var3 == 0) {
            if (var2) {
                var4.ay = false;
            } else if (Client.localPlayerIndex == var1) {
                throw new RuntimeException();
            } else {
                cx.g[var1] = (var4.r << 28) + (an.regionBaseX + var4.co[0] >> 13 << 14) + (ep + var4.cv[0] >> 13);
                if (var4.bh != -1) {
                    cx.n[var1] = var4.bh;
                } else {
                    cx.n[var1] = var4.ct;
                }

                cx.o[var1] = var4.interactingEntityIndex;
                Client.loadedPlayers[var1] = null;
                if (var0.jt(1) != 0) {
                    fl.a(var0, var1);
                }

            }
        } else {
            int var5;
            int var6;
            int var7;
            if (var3 == 1) {
                var5 = var0.jt(3);
                var6 = var4.co[0];
                var7 = var4.cv[0];
                if (var5 == 0) {
                    --var6;
                    --var7;
                } else if (var5 == 1) {
                    --var7;
                } else if (var5 == 2) {
                    ++var6;
                    --var7;
                } else if (var5 == 3) {
                    --var6;
                } else if (var5 == 4) {
                    ++var6;
                } else if (var5 == 5) {
                    --var6;
                    ++var7;
                } else if (var5 == 6) {
                    ++var7;
                } else if (var5 == 7) {
                    ++var6;
                    ++var7;
                }

                if (Client.localPlayerIndex == var1
                        && (var4.regionX < 1536 || var4.regionY < 1536 || var4.regionX >= 11776 || var4.regionY >= 11776)) {
                    var4.c(var6, var7);
                    var4.ay = false;
                } else if (var2) {
                    var4.ay = true;
                    var4.ao = var6;
                    var4.av = var7;
                } else {
                    var4.ay = false;
                    var4.o(var6, var7, cx.a[var1]);
                }

            } else if (var3 == 2) {
                var5 = var0.jt(4);
                var6 = var4.co[0];
                var7 = var4.cv[0];
                if (var5 == 0) {
                    var6 -= 2;
                    var7 -= 2;
                } else if (var5 == 1) {
                    --var6;
                    var7 -= 2;
                } else if (var5 == 2) {
                    var7 -= 2;
                } else if (var5 == 3) {
                    ++var6;
                    var7 -= 2;
                } else if (var5 == 4) {
                    var6 += 2;
                    var7 -= 2;
                } else if (var5 == 5) {
                    var6 -= 2;
                    --var7;
                } else if (var5 == 6) {
                    var6 += 2;
                    --var7;
                } else if (var5 == 7) {
                    var6 -= 2;
                } else if (var5 == 8) {
                    var6 += 2;
                } else if (var5 == 9) {
                    var6 -= 2;
                    ++var7;
                } else if (var5 == 10) {
                    var6 += 2;
                    ++var7;
                } else if (var5 == 11) {
                    var6 -= 2;
                    var7 += 2;
                } else if (var5 == 12) {
                    --var6;
                    var7 += 2;
                } else if (var5 == 13) {
                    var7 += 2;
                } else if (var5 == 14) {
                    ++var6;
                    var7 += 2;
                } else if (var5 == 15) {
                    var6 += 2;
                    var7 += 2;
                }

                if (Client.localPlayerIndex != var1 || var4.regionX >= 1536 && var4.regionY >= 1536
                        && var4.regionX < 11776 && var4.regionY < 11776) {
                    if (var2) {
                        var4.ay = true;
                        var4.ao = var6;
                        var4.av = var7;
                    } else {
                        var4.ay = false;
                        var4.o(var6, var7, cx.a[var1]);
                    }
                } else {
                    var4.c(var6, var7);
                    var4.ay = false;
                }

            } else {
                var5 = var0.jt(1);
                int var8;
                int var9;
                int var10;
                int var11;
                if (var5 == 0) {
                    var6 = var0.jt(12);
                    var7 = var6 >> 10;
                    var8 = var6 >> 5 & 31;
                    if (var8 > 15) {
                        var8 -= 32;
                    }

                    var9 = var6 & 31;
                    if (var9 > 15) {
                        var9 -= 32;
                    }

                    var10 = var8 + var4.co[0];
                    var11 = var9 + var4.cv[0];
                    if (Client.localPlayerIndex != var1 || var4.regionX >= 1536 && var4.regionY >= 1536
                            && var4.regionX < 11776 && var4.regionY < 11776) {
                        if (var2) {
                            var4.ay = true;
                            var4.ao = var10;
                            var4.av = var11;
                        } else {
                            var4.ay = false;
                            var4.o(var10, var11, cx.a[var1]);
                        }
                    } else {
                        var4.c(var10, var11);
                        var4.ay = false;
                    }

                    var4.r = (byte) (var7 + var4.r & 3);
                    if (Client.localPlayerIndex == var1) {
                        kt.ii = var4.r;
                    }

                } else {
                    var6 = var0.jt(30);
                    var7 = var6 >> 28;
                    var8 = var6 >> 14 & 16383;
                    var9 = var6 & 16383;
                    var10 = (var8 + an.regionBaseX + var4.co[0] & 16383) - an.regionBaseX;
                    var11 = (var9 + ep + var4.cv[0] & 16383) - ep;
                    if (Client.localPlayerIndex == var1
                            && (var4.regionX < 1536 || var4.regionY < 1536 || var4.regionX >= 11776 || var4.regionY >= 11776)) {
                        var4.c(var10, var11);
                        var4.ay = false;
                    } else if (var2) {
                        var4.ay = true;
                        var4.ao = var10;
                        var4.av = var11;
                    } else {
                        var4.ay = false;
                        var4.o(var10, var11, cx.a[var1]);
                    }

                    var4.r = (byte) (var7 + var4.r & 3);
                    if (Client.localPlayerIndex == var1) {
                        kt.ii = var4.r;
                    }

                }
            }
        }
    }
}
