import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("au")
public class au {

    @ObfuscatedName("e")
    static he e = new he(37748736, 256);

    @ObfuscatedName("x")
    static he x = new he(256, 256);

    @ObfuscatedName("z")
    static final ik z = new ik();

    @ObfuscatedName("cp")
    static ju cp;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    r n;

    @ObfuscatedName("o")
    LinkedList o;

    @ObfuscatedName("c")
    int c;

    @ObfuscatedName("v")
    int v;

    @ObfuscatedName("u")
    List u;

    @ObfuscatedName("j")
    HashMap j;

    @ObfuscatedName("k")
    final HashMap k;

    au(int var1, int var2, int var3, HashMap var4) {
        this.p = var1;
        this.g = var2;
        this.o = new LinkedList();
        this.u = new LinkedList();
        this.j = new HashMap();
        this.c = var3 | -16777216;
        this.k = var4;
    }

    @ObfuscatedName("l")
    void l(int var1, int var2, int var3) {
        Sprite var4 = h.t(this.p, this.g, this.v);
        if (var4 != null) {
            if (var3 == this.v * 64) {
                var4.o(var1, var2);
            } else {
                var4.ai(var1, var2, var3, var3);
            }

        }
    }

    @ObfuscatedName("b")
    void b(r var1, List var2) {
        this.j.clear();
        this.n = var1;
        this.x(0, 0, 64, 64, this.n);
        this.p(var2);
    }

    @ObfuscatedName("e")
    void e(HashSet var1, List var2) {
        this.j.clear();
        Iterator var3 = var1.iterator();

        while (var3.hasNext()) {
            aa var4 = (aa) var3.next();
            if (var4.o() == this.p && var4.c() == this.g) {
                this.o.add(var4);
                this.x(var4.l() * 8, var4.am() * 8, 8, 8, var4);
            }
        }

        this.p(var2);
    }

    @ObfuscatedName("x")
    void x(int var1, int var2, int var3, int var4, av var5) {
        for (int var6 = var1; var6 < var3 + var1; ++var6) {
            label54: for (int var7 = var2; var7 < var2 + var4; ++var7) {
                ik var8 = new ik(0, var6, var7);

                for (int var9 = 0; var9 < var5.b; ++var9) {
                    am[] var10 = var5.n[var9][var6][var7];
                    if (var10 != null && var10.length != 0) {
                        am[] var11 = var10;

                        for (int var12 = 0; var12 < var11.length; ++var12) {
                            am var13 = var11[var12];
                            jj var14 = this.as(var13.t);
                            if (var14 != null) {
                                ik var15 = new ik(var9, this.p * 64 + var6, this.g * 64 + var7);
                                ik var16 = null;
                                if (this.n != null) {
                                    var16 = new ik(this.n.l + var9, var6 + this.n.t * 64, var7 + this.n.q * 64);
                                } else {
                                    aa var17 = (aa) var5;
                                    var16 = new ik(var9 + var17.l, var17.t * 64 + var6 + var17.i() * 8, var17.q * 64
                                            + var7 + var17.a() * 8);
                                }

                                al var18 = new al(var14.l, var16, var15, this.aq(var14));
                                this.j.put(var8, var18);
                                continue label54;
                            }
                        }
                    }
                }
            }
        }

    }

    @ObfuscatedName("p")
    void p(List var1) {
        Iterator var2 = var1.iterator();

        while (var2.hasNext()) {
            m var3 = (m) var2.next();
            if (var3.q.q >> 6 == this.p && var3.q.i >> 6 == this.g) {
                al var4 = new al(var3.t, var3.q, var3.q, this.aw(var3.t));
                this.u.add(var4);
            }
        }

    }

    @ObfuscatedName("o")
    void o(int var1, af var2, au[] var3, lk[] var4) {
        this.v = var1;
        if (this.n != null || !this.o.isEmpty()) {
            if (h.t(this.p, this.g, var1) == null) {
                ap var5 = this.f(this.p, this.g, var3);
                Sprite var6 = new Sprite(this.v * 64, this.v * 64);
                var6.i();
                if (this.n != null) {
                    this.k(var2, var3, var4, var5);
                } else {
                    this.z(var2, var4, var5);
                }

                an.q(var6, this.p, this.g, this.v);
            }
        }
    }

    @ObfuscatedName("c")
    void c(int var1, int var2, int var3, HashSet var4) {
        if (var4 == null) {
            var4 = new HashSet();
        }

        this.az(var1, var2, var4, var3);
        this.an(var1, var2, var4, var3);
    }

    @ObfuscatedName("u")
    void u(HashSet var1, int var2, int var3) {
        Iterator var4 = this.j.values().iterator();

        while (var4.hasNext()) {
            al var5 = (al) var4.next();
            if (var1.contains(var5.t)) {
                jj var6 = jj.q[var5.t];
                this.ah(var6, var5.e, var5.x, var2, var3);
            }
        }

        this.ap(var1, var2, var3);
    }

    @ObfuscatedName("k")
    void k(af var1, au[] var2, lk[] var3, ap var4) {
        int var5;
        int var6;
        for (var5 = 0; var5 < 64; ++var5) {
            for (var6 = 0; var6 < 64; ++var6) {
                this.s(var5, var6, this.n, var1, var4);
                this.d(var5, var6, this.n, var1);
            }
        }

        for (var5 = 0; var5 < 64; ++var5) {
            for (var6 = 0; var6 < 64; ++var6) {
                this.w(var5, var6, this.n, var1, var3);
            }
        }

    }

    @ObfuscatedName("z")
    void z(af var1, lk[] var2, ap var3) {
        Iterator var4 = this.o.iterator();

        aa var5;
        int var6;
        int var7;
        while (var4.hasNext()) {
            var5 = (aa) var4.next();

            for (var6 = var5.l() * 8; var6 < var5.l() * 8 + 8; ++var6) {
                for (var7 = var5.am() * 8; var7 < var5.am() * 8 + 8; ++var7) {
                    this.s(var6, var7, var5, var1, var3);
                    this.d(var6, var7, var5, var1);
                }
            }
        }

        var4 = this.o.iterator();

        while (var4.hasNext()) {
            var5 = (aa) var4.next();

            for (var6 = var5.l() * 8; var6 < var5.l() * 8 + 8; ++var6) {
                for (var7 = var5.am() * 8; var7 < var5.am() * 8 + 8; ++var7) {
                    this.w(var6, var7, var5, var1, var2);
                }
            }
        }

    }

    @ObfuscatedName("w")
    void w(int var1, int var2, av var3, af var4, lk[] var5) {
        this.am(var1, var2, var3);
        this.ae(var1, var2, var3, var5);
    }

    @ObfuscatedName("s")
    void s(int var1, int var2, av var3, af var4, ap var5) {
        int var6 = var3.e[0][var1][var2] - 1;
        int var7 = var3.x[0][var1][var2] - 1;
        if (var6 == -1 && var7 == -1) {
            li.dl(this.v * var1, this.v * (63 - var2), this.v, this.v, this.c);
        }

        int var8 = 16711935;
        int var9;
        if (var7 != -1) {
            int var10 = this.c;
            kw var11 = ga.loadObjectDefinition(var7);
            if (var11 == null) {
                var9 = var10;
            } else if (var11.b >= 0) {
                var9 = var11.b | -16777216;
            } else {
                int var12;
                if (var11.a >= 0) {
                    var12 = ak.t(eu.r.a(var11.a), 96);
                    var9 = eu.f[var12] | -16777216;
                } else if (var11.i == 16711935) {
                    var9 = var10;
                } else {
                    var12 = f.q(var11.e, var11.x, var11.p);
                    int var13 = ak.t(var12, 96);
                    var9 = eu.f[var13] | -16777216;
                }
            }

            var8 = var9;
        }

        if (var7 > -1 && var3.p[0][var1][var2] == 0) {
            li.dl(this.v * var1, this.v * (63 - var2), this.v, this.v, var8);
        } else {
            var9 = this.aj(var1, var2, var3, var5);
            if (var7 == -1) {
                li.dl(this.v * var1, this.v * (63 - var2), this.v, this.v, var9);
            } else {
                var4.t(this.v * var1, this.v * (63 - var2), var9, var8, this.v, this.v, var3.p[0][var1][var2],
                        var3.g[0][var1][var2]);
            }
        }
    }

    @ObfuscatedName("d")
    void d(int var1, int var2, av var3, af var4) {
        for (int var5 = 1; var5 < var3.b; ++var5) {
            int var6 = var3.x[var5][var1][var2] - 1;
            if (var6 > -1) {
                int var8 = this.c;
                kw var9 = ga.loadObjectDefinition(var6);
                int var7;
                if (var9 == null) {
                    var7 = var8;
                } else if (var9.b >= 0) {
                    var7 = var9.b | -16777216;
                } else {
                    int var10;
                    if (var9.a >= 0) {
                        var10 = ak.t(eu.r.a(var9.a), 96);
                        var7 = eu.f[var10] | -16777216;
                    } else if (var9.i == 16711935) {
                        var7 = var8;
                    } else {
                        var10 = f.q(var9.e, var9.x, var9.p);
                        int var11 = ak.t(var10, 96);
                        var7 = eu.f[var11] | -16777216;
                    }
                }

                if (var3.p[var5][var1][var2] == 0) {
                    li.dl(this.v * var1, this.v * (63 - var2), this.v, this.v, var7);
                } else {
                    var4.t(this.v * var1, this.v * (63 - var2), 0, var7, this.v, this.v, var3.p[var5][var1][var2],
                            var3.g[var5][var1][var2]);
                }
            }
        }

    }

    @ObfuscatedName("f")
    ap f(int var1, int var2, au[] var3) {
        ap var4 = ee.i(var1, var2);
        if (var4 == null) {
            var4 = this.r(var3);
            he var5 = x;
            long var7 = (long) (0 | var1 << 8 | var2);
            var5.a(var4, var7);
        }

        return var4;
    }

    @ObfuscatedName("r")
    ap r(au[] var1) {
        aw var2 = new aw(64, 64);
        if (this.n != null) {
            this.h(0, 0, 64, 64, this.n, var2);
        } else {
            Iterator var3 = this.o.iterator();

            while (var3.hasNext()) {
                aa var4 = (aa) var3.next();
                this.h(var4.l() * 8, var4.am() * 8, 8, 8, var4, var2);
            }
        }

        this.y(var1, var2);
        ap var5 = new ap();
        var5.t(var2);
        return var5;
    }

    @ObfuscatedName("y")
    void y(au[] var1, aw var2) {
        im[] var3 = new im[] { im.a, im.l, im.e, im.t, im.q, im.b, im.x, im.i };
        im[] var5 = var3;

        for (int var6 = 0; var6 < var5.length; ++var6) {
            im var7 = var5[var6];
            if (var1[var7.t()] != null) {
                byte var8 = 0;
                byte var9 = 0;
                byte var10 = 64;
                byte var11 = 64;
                byte var12 = 0;
                byte var13 = 0;
                switch (var7.p) {
                case 0:
                    var8 = 59;
                    var10 = 5;
                    break;
                case 1:
                    var13 = 59;
                    var11 = 5;
                    var8 = 59;
                    var10 = 5;
                    break;
                case 2:
                    var12 = 59;
                    var13 = 59;
                    var10 = 5;
                    var11 = 5;
                    break;
                case 3:
                    var13 = 59;
                    var11 = 5;
                    break;
                case 4:
                    var12 = 59;
                    var10 = 5;
                    break;
                case 5:
                    var9 = 59;
                    var11 = 5;
                    var8 = 59;
                    var10 = 5;
                    break;
                case 6:
                    var9 = 59;
                    var11 = 5;
                    var12 = 59;
                    var10 = 5;
                    break;
                case 7:
                    var9 = 59;
                    var11 = 5;
                }

                this.av(var12, var13, var8, var9, var10, var11, var1[var7.t()], var2);
            }
        }

    }

    @ObfuscatedName("h")
    void h(int var1, int var2, int var3, int var4, av var5, aw var6) {
        for (int var7 = var1; var7 < var3 + var1; ++var7) {
            for (int var8 = var2; var8 < var2 + var4; ++var8) {
                int var9 = var5.e[0][var7][var8] - 1;
                if (var9 != -1) {
                    js var10 = DevelopmentStage.t(var9);
                    var6.t(var7, var8, 5, var10);
                }
            }
        }

    }

    @ObfuscatedName("av")
    void av(int var1, int var2, int var3, int var4, int var5, int var6, au var7, aw var8) {
        for (int var9 = 0; var9 < var5; ++var9) {
            for (int var10 = 0; var10 < var6; ++var10) {
                int var11 = var7.ab(var9 + var1, var10 + var2);
                if (var11 != -1) {
                    js var12 = DevelopmentStage.t(var11);
                    var8.t(var3 + var9, var10 + var4, 5, var12);
                }
            }
        }

    }

    @ObfuscatedName("aj")
    int aj(int var1, int var2, av var3, ap var4) {
        return var3.e[0][var1][var2] == 0 ? this.c : var4.q(var1, var2);
    }

    @ObfuscatedName("ae")
    void ae(int var1, int var2, av var3, lk[] var4) {
        for (int var5 = 0; var5 < var3.b; ++var5) {
            am[] var6 = var3.n[var5][var1][var2];
            if (var6 != null && var6.length != 0) {
                am[] var7 = var6;

                for (int var8 = 0; var8 < var7.length; ++var8) {
                    am var9 = var7[var8];
                    if (!Projectile.a(var9.q)) {
                        int var11 = var9.q;
                        boolean var10 = var11 == ij.r.y;
                        if (!var10) {
                            continue;
                        }
                    }

                    ObjectDefinition var12 = jw.q(var9.t);
                    if (var12.ae != -1) {
                        if (var12.ae != 46 && var12.ae != 52) {
                            var4[var12.ae].l(this.v * var1, this.v * (63 - var2), this.v * 2, this.v * 2);
                        } else {
                            var4[var12.ae].l(this.v * var1, this.v * (63 - var2), this.v * 2 + 1, this.v * 2 + 1);
                        }
                    }
                }
            }
        }

    }

    @ObfuscatedName("am")
    void am(int var1, int var2, av var3) {
        for (int var4 = 0; var4 < var3.b; ++var4) {
            am[] var5 = var3.n[var4][var1][var2];
            if (var5 != null && var5.length != 0) {
                am[] var6 = var5;

                for (int var7 = 0; var7 < var6.length; ++var7) {
                    am var8 = var6[var7];
                    int var10 = var8.q;
                    boolean var9 = var10 >= ij.t.y && var10 <= ij.a.y || var10 == ij.l.y;
                    if (var9) {
                        ObjectDefinition var11 = jw.q(var8.t);
                        int var12 = var11.d != 0 ? -3407872 : -3355444;
                        if (var8.q == ij.t.y) {
                            this.ak(var1, var2, var8.i, var12);
                        }

                        if (var8.q == ij.i.y) {
                            this.ak(var1, var2, var8.i, -3355444);
                            this.ak(var1, var2, var8.i + 1, var12);
                        }

                        if (var8.q == ij.a.y) {
                            if (var8.i == 0) {
                                li.du(this.v * var1, this.v * (63 - var2), 1, var12);
                            }

                            if (var8.i == 1) {
                                li.du(this.v * var1 + this.v - 1, this.v * (63 - var2), 1, var12);
                            }

                            if (var8.i == 2) {
                                li.du(this.v * var1 + this.v - 1, this.v * (63 - var2) + this.v - 1, 1, var12);
                            }

                            if (var8.i == 3) {
                                li.du(this.v * var1, this.v * (63 - var2) + this.v - 1, 1, var12);
                            }
                        }

                        if (var8.q == ij.l.y) {
                            int var13 = var8.i % 2;
                            int var14;
                            if (var13 == 0) {
                                for (var14 = 0; var14 < this.v; ++var14) {
                                    li.du(var14 + this.v * var1, (64 - var2) * this.v - 1 - var14, 1, var12);
                                }
                            } else {
                                for (var14 = 0; var14 < this.v; ++var14) {
                                    li.du(var14 + this.v * var1, var14 + this.v * (63 - var2), 1, var12);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    @ObfuscatedName("az")
    void az(int var1, int var2, HashSet var3, int var4) {
        float var5 = (float) var4 / 64.0F;
        float var6 = var5 / 2.0F;
        Iterator var7 = this.j.entrySet().iterator();

        while (var7.hasNext()) {
            Entry var8 = (Entry) var7.next();
            ik var9 = (ik) var8.getKey();
            int var10 = (int) ((float) var1 + var5 * (float) var9.q - var6);
            int var11 = (int) ((float) (var2 + var4) - var5 * (float) var9.i - var6);
            al var12 = (al) var8.getValue();
            if (var12 != null) {
                var12.e = var10;
                var12.x = var11;
                jj var13 = jj.q[var12.t];
                if (!var3.contains(var13.b())) {
                    this.au(var12, var10, var11, var5);
                }
            }
        }

    }

    @ObfuscatedName("ap")
    void ap(HashSet var1, int var2, int var3) {
        Iterator var4 = this.u.iterator();

        while (var4.hasNext()) {
            al var5 = (al) var4.next();
            jj var6 = jj.q[var5.t];
            if (var6 != null && var1.contains(var6.b())) {
                this.ah(var6, var5.e, var5.x, var2, var3);
            }
        }

    }

    @ObfuscatedName("ah")
    void ah(jj var1, int var2, int var3, int var4, int var5) {
        Sprite var6 = var1.a(false);
        if (var6 != null) {
            var6.u(var2 - var6.width / 2, var3 - var6.height / 2);
            if (var4 % var5 < var5 / 2) {
                li.dh(var2, var3, 15, 16776960, 128);
                li.dh(var2, var3, 7, 16777215, 256);
            }

        }
    }

    @ObfuscatedName("au")
    void au(al var1, int var2, int var3, float var4) {
        jj var5 = jj.q[var1.t];
        this.ax(var5, var2, var3);
        this.ar(var1, var5, var2, var3, var4);
    }

    @ObfuscatedName("ax")
    void ax(jj var1, int var2, int var3) {
        Sprite var4 = var1.a(false);
        if (var4 != null) {
            int var5 = this.at(var4, var1.w);
            int var6 = this.ag(var4, var1.s);
            var4.u(var5 + var2, var3 + var6);
        }

    }

    @ObfuscatedName("ar")
    void ar(al var1, jj var2, int var3, int var4, float var5) {
        if (var1.b != null) {
            if (var1.b.a.t(var5)) {
                km var6 = (km) this.k.get(var1.b.a);
                var6.y(var1.b.t, var3 - var1.b.q / 2, var4, var1.b.q, var1.b.i, -16777216 | var2.p, 0, 1, 0, var6.e / 2);
            }
        }
    }

    @ObfuscatedName("an")
    void an(int var1, int var2, HashSet var3, int var4) {
        float var5 = (float) var4 / 64.0F;
        Iterator var6 = this.u.iterator();

        while (var6.hasNext()) {
            al var7 = (al) var6.next();
            int var8 = var7.q.q % 64;
            int var9 = var7.q.i % 64;
            var7.e = (int) ((float) var8 * var5 + (float) var1);
            var7.x = (int) (var5 * (float) (63 - var9) + (float) var2);
            if (!var3.contains(var7.t)) {
                this.au(var7, var7.e, var7.x, var5);
            }
        }

    }

    @ObfuscatedName("ai")
    void ai() {
        if (this.n != null) {
            for (int var1 = 0; var1 < 64; ++var1) {
                for (int var2 = 0; var2 < 64; ++var2) {
                    this.al(var1, var2, this.n);
                }
            }
        } else {
            Iterator var5 = this.o.iterator();

            while (var5.hasNext()) {
                aa var6 = (aa) var5.next();

                for (int var3 = var6.l() * 8; var3 < var6.l() * 8 + 8; ++var3) {
                    for (int var4 = var6.am() * 8; var4 < var6.am() * 8 + 8; ++var4) {
                        this.al(var3, var4, var6);
                    }
                }
            }
        }

    }

    @ObfuscatedName("al")
    void al(int var1, int var2, av var3) {
        z.t(0, var1, var2);

        for (int var4 = 0; var4 < var3.b; ++var4) {
            am[] var5 = var3.n[var4][var1][var2];
            if (var5 != null && var5.length != 0) {
                am[] var6 = var5;

                for (int var7 = 0; var7 < var6.length; ++var7) {
                    am var8 = var6[var7];
                    jj var9 = this.as(var8.t);
                    if (var9 != null) {
                        al var10 = (al) this.j.get(z);
                        if (var10 != null) {
                            if (var10.t != var9.l) {
                                al var16 = new al(var9.l, var10.i, var10.q, this.aq(var9));
                                this.j.put(new ik(z), var16);
                                var10 = var16;
                            }

                            int var15 = var10.i.t - var10.q.t;
                            var10.q.t = var4;
                            var10.i.t = var4 + var15;
                            return;
                        }

                        ik var11 = new ik(var4, this.p * 64 + var1, this.g * 64 + var2);
                        ik var12 = null;
                        if (this.n != null) {
                            var12 = new ik(this.n.l + var4, this.n.t * 64 + var1, var2 + this.n.q * 64);
                        } else {
                            Iterator var13 = this.o.iterator();

                            while (var13.hasNext()) {
                                aa var14 = (aa) var13.next();
                                if (var14.q(var1, var2)) {
                                    var12 = new ik(var4 + var14.l, var14.t * 64 + var1 + var14.i() * 8, var14.q * 64
                                            + var2 + var14.a() * 8);
                                    break;
                                }
                            }
                        }

                        if (var12 != null) {
                            var10 = new al(var9.l, var12, var11, this.aq(var9));
                            this.j.put(new ik(z), var10);
                            return;
                        }
                    }
                }
            }
        }

        this.j.remove(z);
    }

    @ObfuscatedName("at")
    int at(Sprite var1, jm var2) {
        switch (var2.a) {
        case 0:
            return -var1.width / 2;
        case 2:
            return 0;
        default:
            return -var1.width;
        }
    }

    @ObfuscatedName("ag")
    int ag(Sprite var1, ji var2) {
        switch (var2.a) {
        case 0:
            return -var1.height / 2;
        case 1:
            return 0;
        default:
            return -var1.height;
        }
    }

    @ObfuscatedName("as")
    jj as(int var1) {
        ObjectDefinition var2 = jw.q(var1);
        if (var2.innerObjectIds != null) {
            var2 = var2.u();
            if (var2 == null) {
                return null;
            }
        }

        return var2.mapFunction != -1 ? jj.q[var2.mapFunction] : null;
    }

    @ObfuscatedName("aw")
    ah aw(int var1) {
        jj var2 = jj.q[var1];
        return this.aq(var2);
    }

    @ObfuscatedName("aq")
    ah aq(jj var1) {
        if (var1.x != null && this.k != null && this.k.get(h.t) != null) {
            int var3 = var1.g;
            h[] var4 = new h[] { h.q, h.t, h.i };
            h[] var5 = var4;
            int var6 = 0;

            h var2;
            while (true) {
                if (var6 >= var5.length) {
                    var2 = null;
                    break;
                }

                h var7 = var5[var6];
                if (var3 == var7.b) {
                    var2 = var7;
                    break;
                }

                ++var6;
            }

            if (var2 == null) {
                return null;
            } else {
                km var14 = (km) this.k.get(var2);
                if (var14 == null) {
                    return null;
                } else {
                    var6 = var14.z(var1.x, 1000000);
                    String[] var15 = new String[var6];
                    var14.u(var1.x, (int[]) null, var15);
                    int var8 = var15.length * var14.e / 2;
                    int var9 = 0;
                    String[] var10 = var15;

                    for (int var11 = 0; var11 < var10.length; ++var11) {
                        String var12 = var10[var11];
                        int var13 = var14.c(var12);
                        if (var13 > var9) {
                            var9 = var13;
                        }
                    }

                    return new ah(var1.x, var9, var8, var2);
                }
            }
        } else {
            return null;
        }
    }

    @ObfuscatedName("aa")
    List aa(int var1, int var2, int var3, int var4, int var5) {
        LinkedList var6 = new LinkedList();
        if (var4 >= var1 && var5 >= var2) {
            if (var4 < var3 + var1 && var5 < var3 + var2) {
                Iterator var7 = this.j.values().iterator();

                al var8;
                while (var7.hasNext()) {
                    var8 = (al) var7.next();
                    if (var8.t(var4, var5)) {
                        var6.add(var8);
                    }
                }

                var7 = this.u.iterator();

                while (var7.hasNext()) {
                    var8 = (al) var7.next();
                    if (var8.t(var4, var5)) {
                        var6.add(var8);
                    }
                }

                return var6;
            } else {
                return var6;
            }
        } else {
            return var6;
        }
    }

    @ObfuscatedName("af")
    List af() {
        LinkedList var1 = new LinkedList();
        var1.addAll(this.u);
        var1.addAll(this.j.values());
        return var1;
    }

    @ObfuscatedName("ak")
    void ak(int var1, int var2, int var3, int var4) {
        var3 %= 4;
        if (var3 == 0) {
            li.de(this.v * var1, this.v * (63 - var2), this.v, var4);
        }

        if (var3 == 1) {
            li.du(this.v * var1, this.v * (63 - var2), this.v, var4);
        }

        if (var3 == 2) {
            li.de(this.v * var1 + this.v - 1, this.v * (63 - var2), this.v, var4);
        }

        if (var3 == 3) {
            li.du(this.v * var1, this.v * (63 - var2) + this.v - 1, this.v, var4);
        }

    }

    @ObfuscatedName("ab")
    int ab(int var1, int var2) {
        if (this.n != null) {
            return this.n.p(var1, var2);
        } else {
            if (!this.o.isEmpty()) {
                Iterator var3 = this.o.iterator();

                while (var3.hasNext()) {
                    aa var4 = (aa) var3.next();
                    if (var4.q(var1, var2)) {
                        return var4.p(var1, var2);
                    }
                }
            }

            return -1;
        }
    }

    @ObfuscatedName("t")
    public static synchronized long t() {
        long var0 = System.currentTimeMillis();
        if (var0 < gf.t) {
            gf.q += gf.t - var0;
        }

        gf.t = var0;
        return gf.q + var0;
    }

    @ObfuscatedName("b")
    public static fr b() {
        try {
            return new fu();
        } catch (Throwable var1) {
            return new fz();
        }
    }

    @ObfuscatedName("z")
    static int z(int var0, RuneScript var1, boolean var2) {
        RTComponent var3 = Inflater.t(cs.e[--b.menuX]);
        if (var0 == 2800) {
            int[] var4 = cs.e;
            int var5 = ++b.menuX - 1;
            int var7 = u.jr(var3);
            int var6 = var7 >> 11 & 63;
            var4[var5] = var6;
            return 1;
        } else if (var0 != 2801) {
            if (var0 == 2802) {
                if (var3.name == null) {
                    cs.p[++ly.g - 1] = "";
                } else {
                    cs.p[++ly.g - 1] = var3.name;
                }

                return 1;
            } else {
                return 2;
            }
        } else {
            int var8 = cs.e[--b.menuX];
            --var8;
            if (var3.actions != null && var8 < var3.actions.length && var3.actions[var8] != null) {
                cs.p[++ly.g - 1] = var3.actions[var8];
            } else {
                cs.p[++ly.g - 1] = "";
            }

            return 1;
        }
    }

    @ObfuscatedName("iu")
    static final void iu(RTComponent var0, int var1, int var2) {
        if (Client.ln == null && !Client.menuOpen) {
            if (var0 != null && dc.jt(var0) != null) {
                Client.ln = var0;
                Client.lm = dc.jt(var0);
                Client.li = var1;
                Client.lf = var2;
                ah.lp = 0;
                Client.lr = false;
                int var3 = Client.menuSize - 1;
                if (var3 != -1) {
                    am.kt = new MenuItemNode();
                    am.kt.q = Client.menuArg1[var3];
                    am.kt.i = Client.menuArg2[var3];
                    am.kt.a = Client.menuOpcodes[var3];
                    am.kt.l = Client.ka[var3];
                    am.kt.text = Client.menuActions[var3];
                }

            }
        }
    }
}
