import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("lf")
public abstract class Producer {

    @ObfuscatedName("i")
    public int[] pixels;

    @ObfuscatedName("a")
    public int width;

    @ObfuscatedName("l")
    public int height;

    @ObfuscatedName("q")
    public abstract void q(int var1, int var2);

    @ObfuscatedName("i")
    public abstract void i(int var1, int var2, int var3, int var4);

    @ObfuscatedName("aj")
    public final void aj() {
        li.cf(this.pixels, this.width, this.height);
    }
}
