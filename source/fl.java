import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fl")
public class fl implements Runnable {

    @ObfuscatedName("t")
    Thread t;

    @ObfuscatedName("q")
    InputStream q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    byte[] a;

    @ObfuscatedName("l")
    int l = 0;

    @ObfuscatedName("b")
    int b = 0;

    @ObfuscatedName("e")
    IOException e;

    fl(InputStream var1, int var2) {
        this.q = var1;
        this.i = var2 + 1;
        this.a = new byte[this.i];
        this.t = new Thread(this);
        this.t.setDaemon(true);
        this.t.start();
    }

    @ObfuscatedName("t")
    boolean t(int var1)
            throws IOException {
        if (var1 == 0) {
            return true;
        } else if (var1 > 0 && var1 < this.i) {
            synchronized (this) {
                int var3;
                if (this.l <= this.b) {
                    var3 = this.b - this.l;
                } else {
                    var3 = this.i - this.l + this.b;
                }

                if (var3 < var1) {
                    if (this.e != null) {
                        throw new IOException(this.e.toString());
                    } else {
                        this.notifyAll();
                        return false;
                    }
                } else {
                    return true;
                }
            }
        } else {
            throw new IOException();
        }
    }

    @ObfuscatedName("q")
    int q()
            throws IOException {
        synchronized (this) {
            int var2;
            if (this.l <= this.b) {
                var2 = this.b - this.l;
            } else {
                var2 = this.i - this.l + this.b;
            }

            if (var2 <= 0 && this.e != null) {
                throw new IOException(this.e.toString());
            } else {
                this.notifyAll();
                return var2;
            }
        }
    }

    @ObfuscatedName("i")
    int i()
            throws IOException {
        synchronized (this) {
            if (this.b == this.l) {
                if (this.e != null) {
                    throw new IOException(this.e.toString());
                } else {
                    return -1;
                }
            } else {
                int var2 = this.a[this.l] & 255;
                this.l = (this.l + 1) % this.i;
                this.notifyAll();
                return var2;
            }
        }
    }

    @ObfuscatedName("a")
    int a(byte[] var1, int var2, int var3)
            throws IOException {
        if (var3 >= 0 && var2 >= 0 && var3 + var2 <= var1.length) {
            synchronized (this) {
                int var5;
                if (this.l <= this.b) {
                    var5 = this.b - this.l;
                } else {
                    var5 = this.i - this.l + this.b;
                }

                if (var3 > var5) {
                    var3 = var5;
                }

                if (var3 == 0 && this.e != null) {
                    throw new IOException(this.e.toString());
                } else {
                    if (var3 + this.l <= this.i) {
                        System.arraycopy(this.a, this.l, var1, var2, var3);
                    } else {
                        int var6 = this.i - this.l;
                        System.arraycopy(this.a, this.l, var1, var2, var6);
                        System.arraycopy(this.a, 0, var1, var6 + var2, var3 - var6);
                    }

                    this.l = (var3 + this.l) % this.i;
                    this.notifyAll();
                    return var3;
                }
            }
        } else {
            throw new IOException();
        }
    }

    @ObfuscatedName("l")
    void l() {
        synchronized (this) {
            if (this.e == null) {
                this.e = new IOException("");
            }

            this.notifyAll();
        }

        try {
            this.t.join();
        } catch (InterruptedException var3) {
            ;
        }

    }

    public void run() {
        while (true) {
            int var1;
            synchronized (this) {
                while (true) {
                    if (this.e != null) {
                        return;
                    }

                    if (this.l == 0) {
                        var1 = this.i - this.b - 1;
                    } else if (this.l <= this.b) {
                        var1 = this.i - this.b;
                    } else {
                        var1 = this.l - this.b - 1;
                    }

                    if (var1 > 0) {
                        break;
                    }

                    try {
                        this.wait();
                    } catch (InterruptedException var10) {
                        ;
                    }
                }
            }

            int var7;
            try {
                var7 = this.q.read(this.a, this.b, var1);
                if (var7 == -1) {
                    throw new EOFException();
                }
            } catch (IOException var11) {
                IOException var3 = var11;
                synchronized (this) {
                    this.e = var3;
                    return;
                }
            }

            synchronized (this) {
                this.b = (var7 + this.b) % this.i;
            }
        }
    }

    @ObfuscatedName("t")
    public static final void t(Model var0, int var1, int var2, int var3) {
        if (bv.a(var0, var1, var2, var3)) {
            iz.q(var0, var1, var2, var3, -65281);
        } else if (x.a == o.q) {
            iz.q(var0, var1, var2, var3, -16776961);
        }

    }

    @ObfuscatedName("q")
    public static boolean q(CharSequence var0) {
        boolean var2 = false;
        boolean var3 = false;
        int var4 = 0;
        int var5 = var0.length();
        int var6 = 0;

        boolean var1;
        while (true) {
            if (var6 >= var5) {
                var1 = var3;
                break;
            }

            label84: {
                char var7 = var0.charAt(var6);
                if (var6 == 0) {
                    if (var7 == '-') {
                        var2 = true;
                        break label84;
                    }

                    if (var7 == '+') {
                        break label84;
                    }
                }

                int var9;
                if (var7 >= '0' && var7 <= '9') {
                    var9 = var7 - 48;
                } else if (var7 >= 'A' && var7 <= 'Z') {
                    var9 = var7 - 55;
                } else {
                    if (var7 < 'a' || var7 > 'z') {
                        var1 = false;
                        break;
                    }

                    var9 = var7 - 87;
                }

                if (var9 >= 10) {
                    var1 = false;
                    break;
                }

                if (var2) {
                    var9 = -var9;
                }

                int var8 = var4 * 10 + var9;
                if (var4 != var8 / 10) {
                    var1 = false;
                    break;
                }

                var4 = var8;
                var3 = true;
            }

            ++var6;
        }

        return var1;
    }

    @ObfuscatedName("a")
    static boolean a(Packet var0, int var1) {
        int var2 = var0.jt(2);
        int var3;
        int var4;
        int var7;
        int var8;
        int var9;
        int var10;
        if (var2 == 0) {
            if (var0.jt(1) != 0) {
                a(var0, var1);
            }

            var3 = var0.jt(13);
            var4 = var0.jt(13);
            boolean var12 = var0.jt(1) == 1;
            if (var12) {
                cx.v[++cx.c - 1] = var1;
            }

            if (Client.loadedPlayers[var1] != null) {
                throw new RuntimeException();
            } else {
                Player var6 = Client.loadedPlayers[var1] = new Player();
                var6.y = var1;
                if (cx.l[var1] != null) {
                    var6.t(cx.l[var1]);
                }

                var6.ct = cx.n[var1];
                var6.interactingEntityIndex = cx.o[var1];
                var7 = cx.g[var1];
                var8 = var7 >> 28;
                var9 = var7 >> 14 & 255;
                var10 = var7 & 255;
                var6.cd[0] = cx.a[var1];
                var6.r = (byte) var8;
                var6.c((var9 << 13) + var3 - an.regionBaseX, (var10 << 13) + var4 - PlayerComposite.ep);
                var6.ay = false;
                return true;
            }
        } else if (var2 == 1) {
            var3 = var0.jt(2);
            var4 = cx.g[var1];
            cx.g[var1] = (var4 & 268435455) + (((var4 >> 28) + var3 & 3) << 28);
            return false;
        } else {
            int var5;
            int var11;
            if (var2 == 2) {
                var3 = var0.jt(5);
                var4 = var3 >> 3;
                var5 = var3 & 7;
                var11 = cx.g[var1];
                var7 = (var11 >> 28) + var4 & 3;
                var8 = var11 >> 14 & 255;
                var9 = var11 & 255;
                if (var5 == 0) {
                    --var8;
                    --var9;
                }

                if (var5 == 1) {
                    --var9;
                }

                if (var5 == 2) {
                    ++var8;
                    --var9;
                }

                if (var5 == 3) {
                    --var8;
                }

                if (var5 == 4) {
                    ++var8;
                }

                if (var5 == 5) {
                    --var8;
                    ++var9;
                }

                if (var5 == 6) {
                    ++var9;
                }

                if (var5 == 7) {
                    ++var8;
                    ++var9;
                }

                cx.g[var1] = (var8 << 14) + var9 + (var7 << 28);
                return false;
            } else {
                var3 = var0.jt(18);
                var4 = var3 >> 16;
                var5 = var3 >> 8 & 255;
                var11 = var3 & 255;
                var7 = cx.g[var1];
                var8 = (var7 >> 28) + var4 & 3;
                var9 = var5 + (var7 >> 14) & 255;
                var10 = var11 + var7 & 255;
                cx.g[var1] = (var9 << 14) + var10 + (var8 << 28);
                return false;
            }
        }
    }

    @ObfuscatedName("l")
    static void l(int var0) {
        ItemStorage var1 = (ItemStorage) ItemStorage.t.t((long) var0);
        if (var1 != null) {
            for (int var2 = 0; var2 < var1.ids.length; ++var2) {
                var1.ids[var2] = -1;
                var1.stackSizes[var2] = 0;
            }

        }
    }
}
