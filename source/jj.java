import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("jj")
public class jj extends hh {

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("q")
    public static jj[] q;

    @ObfuscatedName("i")
    public static int i;

    @ObfuscatedName("a")
    static Cache a = new Cache(256);

    @ObfuscatedName("l")
    public final int l;

    @ObfuscatedName("b")
    public int b = -1;

    @ObfuscatedName("e")
    int e = -1;

    @ObfuscatedName("x")
    public String x;

    @ObfuscatedName("p")
    public int p;

    @ObfuscatedName("g")
    public int g = 0;

    @ObfuscatedName("o")
    public String[] o = new String[5];

    @ObfuscatedName("c")
    public String c;

    @ObfuscatedName("v")
    int[] v;

    @ObfuscatedName("u")
    int u = Integer.MAX_VALUE;

    @ObfuscatedName("j")
    int j = Integer.MAX_VALUE;

    @ObfuscatedName("k")
    int k = Integer.MIN_VALUE;

    @ObfuscatedName("z")
    int z = Integer.MIN_VALUE;

    @ObfuscatedName("w")
    public jm w;

    @ObfuscatedName("s")
    public ji s;

    @ObfuscatedName("d")
    int[] d;

    @ObfuscatedName("f")
    byte[] f;

    @ObfuscatedName("r")
    public int r;

    public jj(int var1) {
        this.w = jm.q;
        this.s = ji.q;
        this.r = -1;
        this.l = var1;
    }

    @ObfuscatedName("t")
    public void t(ByteBuffer var1) {
        while (true) {
            int var2 = var1.av();
            if (var2 == 0) {
                return;
            }

            this.q(var1, var2);
        }
    }

    @ObfuscatedName("q")
    void q(ByteBuffer var1, int var2) {
        if (var2 == 1) {
            this.b = var1.aw();
        } else if (var2 == 2) {
            this.e = var1.aw();
        } else if (var2 == 3) {
            this.x = var1.ar();
        } else if (var2 == 4) {
            this.p = var1.az();
        } else if (var2 == 5) {
            var1.az();
        } else if (var2 == 6) {
            this.g = var1.av();
        } else {
            int var3;
            if (var2 == 7) {
                var3 = var1.av();
                if ((var3 & 1) == 0) {
                    ;
                }

                if ((var3 & 2) == 2) {
                    ;
                }
            } else if (var2 == 8) {
                var1.av();
            } else if (var2 >= 10 && var2 <= 14) {
                this.o[var2 - 10] = var1.ar();
            } else if (var2 == 15) {
                var3 = var1.av();
                this.v = new int[var3 * 2];

                int var4;
                for (var4 = 0; var4 < var3 * 2; ++var4) {
                    this.v[var4] = var1.am();
                }

                var1.ap();
                var4 = var1.av();
                this.d = new int[var4];

                int var5;
                for (var5 = 0; var5 < this.d.length; ++var5) {
                    this.d[var5] = var1.ap();
                }

                this.f = new byte[var3];

                for (var5 = 0; var5 < var3; ++var5) {
                    this.f[var5] = var1.aj();
                }
            } else if (var2 != 16) {
                if (var2 == 17) {
                    this.c = var1.ar();
                } else if (var2 == 18) {
                    var1.aw();
                } else if (var2 == 19) {
                    this.r = var1.ae();
                } else if (var2 == 21) {
                    var1.ap();
                } else if (var2 == 22) {
                    var1.ap();
                } else if (var2 == 23) {
                    var1.av();
                    var1.av();
                    var1.av();
                } else if (var2 == 24) {
                    var1.am();
                    var1.am();
                } else if (var2 == 25) {
                    var1.aw();
                } else if (var2 == 28) {
                    var1.av();
                } else if (var2 == 29) {
                    this.w = (jm) aj.t(iq.a(), var1.av());
                } else if (var2 == 30) {
                    this.s = (ji) aj.t(l.a(), var1.av());
                }
            }
        }

    }

    @ObfuscatedName("i")
    public void i() {
        if (this.v != null) {
            for (int var1 = 0; var1 < this.v.length; var1 += 2) {
                if (this.v[var1] < this.u) {
                    this.u = this.v[var1];
                } else if (this.v[var1] > this.k) {
                    this.k = this.v[var1];
                }

                if (this.v[var1 + 1] < this.j) {
                    this.j = this.v[var1 + 1];
                } else if (this.v[var1 + 1] > this.z) {
                    this.z = this.v[var1 + 1];
                }
            }
        }

    }

    @ObfuscatedName("a")
    public Sprite a(boolean var1) {
        int var2 = this.b;
        return this.l(var2);
    }

    @ObfuscatedName("l")
    Sprite l(int var1) {
        if (var1 < 0) {
            return null;
        } else {
            Sprite var2 = (Sprite) a.t((long) var1);
            if (var2 != null) {
                return var2;
            } else {
                var2 = q.t(t, var1, 0);
                if (var2 != null) {
                    a.i(var2, (long) var1);
                }

                return var2;
            }
        }
    }

    @ObfuscatedName("b")
    public int b() {
        return this.l;
    }

    @ObfuscatedName("s")
    static final void s(String var0) {
        ak.p(var0 + " is already on your ignore list");
    }
}
