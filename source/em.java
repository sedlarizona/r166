import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("em")
public class em extends ky {

    @ObfuscatedName("s")
    public static ByteBuffer s;

    @ObfuscatedName("t")
    final boolean t;

    public em(boolean var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    int t(kp var1, kp var2) {
        if (Client.currentWorld == var1.l && var2.l == Client.currentWorld) {
            return this.t ? var1.b - var2.b : var2.b - var1.b;
        } else {
            return this.p(var1, var2);
        }
    }

    public int compare(Object var1, Object var2) {
        return this.t((kp) var1, (kp) var2);
    }

    @ObfuscatedName("t")
    public static int t(int var0, int var1, int var2, fv var3, CollisionMap var4, boolean var5, int[] var6, int[] var7) {
        int var9;
        for (int var8 = 0; var8 < 128; ++var8) {
            for (var9 = 0; var9 < 128; ++var9) {
                fc.i[var8][var9] = 0;
                fc.a[var8][var9] = 99999999;
            }
        }

        boolean var27;
        if (var2 == 1) {
            var27 = eh.q(var0, var1, var3, var4);
        } else if (var2 == 2) {
            var27 = ItemNode.i(var0, var1, var3, var4);
        } else {
            var27 = bv.a(var0, var1, var2, var3, var4);
        }

        var9 = var0 - 64;
        int var10 = var1 - 64;
        int var11 = z.l;
        int var12 = j.b;
        int var13;
        int var14;
        int var16;
        if (!var27) {
            var13 = Integer.MAX_VALUE;
            var14 = Integer.MAX_VALUE;
            byte var15 = 10;
            var16 = var3.t;
            int var17 = var3.q;
            int var18 = var3.i;
            int var19 = var3.a;

            for (int var20 = var16 - var15; var20 <= var15 + var16; ++var20) {
                for (int var21 = var17 - var15; var21 <= var17 + var15; ++var21) {
                    int var22 = var20 - var9;
                    int var23 = var21 - var10;
                    if (var22 >= 0 && var23 >= 0 && var22 < 128 && var23 < 128 && fc.a[var22][var23] < 100) {
                        int var24 = 0;
                        if (var20 < var16) {
                            var24 = var16 - var20;
                        } else if (var20 > var18 + var16 - 1) {
                            var24 = var20 - (var16 + var18 - 1);
                        }

                        int var25 = 0;
                        if (var21 < var17) {
                            var25 = var17 - var21;
                        } else if (var21 > var19 + var17 - 1) {
                            var25 = var21 - (var17 + var19 - 1);
                        }

                        int var26 = var25 * var25 + var24 * var24;
                        if (var26 < var13 || var13 == var26 && fc.a[var22][var23] < var14) {
                            var13 = var26;
                            var14 = fc.a[var22][var23];
                            var11 = var20;
                            var12 = var21;
                        }
                    }
                }
            }

            if (var13 == Integer.MAX_VALUE) {
                return -1;
            }
        }

        if (var0 == var11 && var12 == var1) {
            return 0;
        } else {
            byte var28 = 0;
            fc.x[var28] = var11;
            var13 = var28 + 1;
            fc.p[var28] = var12;

            int var29;
            for (var14 = var29 = fc.i[var11 - var9][var12 - var10]; var0 != var11 || var12 != var1; var14 = fc.i[var11
                    - var9][var12 - var10]) {
                if (var29 != var14) {
                    var29 = var14;
                    fc.x[var13] = var11;
                    fc.p[var13++] = var12;
                }

                if ((var14 & 2) != 0) {
                    ++var11;
                } else if ((var14 & 8) != 0) {
                    --var11;
                }

                if ((var14 & 1) != 0) {
                    ++var12;
                } else if ((var14 & 4) != 0) {
                    --var12;
                }
            }

            var16 = 0;

            while (var13-- > 0) {
                var6[var16] = fc.x[var13];
                var7[var16++] = fc.p[var13];
                if (var16 >= var6.length) {
                    break;
                }
            }

            return var16;
        }
    }

    @ObfuscatedName("q")
    static String q(byte[] var0, int var1, int var2) {
        StringBuilder var3 = new StringBuilder();

        for (int var4 = var1; var4 < var2 + var1; var4 += 3) {
            int var5 = var0[var4] & 255;
            var3.append(lq.t[var5 >>> 2]);
            if (var4 < var2 - 1) {
                int var6 = var0[var4 + 1] & 255;
                var3.append(lq.t[(var5 & 3) << 4 | var6 >>> 4]);
                if (var4 < var2 - 2) {
                    int var7 = var0[var4 + 2] & 255;
                    var3.append(lq.t[(var6 & 15) << 2 | var7 >>> 6]).append(lq.t[var7 & 63]);
                } else {
                    var3.append(lq.t[(var6 & 15) << 2]).append("=");
                }
            } else {
                var3.append(lq.t[(var5 & 3) << 4]).append("==");
            }
        }

        return var3.toString();
    }
}
