import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gv")
public final class gv {

    @ObfuscatedName("ej")
    static Task ej;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int[] l = new int[256];

    @ObfuscatedName("b")
    int[] b = new int[256];

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    public gv(int[] var1) {
        for (int var2 = 0; var2 < var1.length; ++var2) {
            this.l[var2] = var1[var2];
        }

        this.a();
    }

    @ObfuscatedName("t")
    final int t() {
        if (0 == --this.a + 1) {
            this.i();
            this.a = 255;
        }

        return this.l[this.a];
    }

    @ObfuscatedName("q")
    final int q() {
        if (this.a == 0) {
            this.i();
            this.a = 256;
        }

        return this.l[this.a - 1];
    }

    @ObfuscatedName("i")
    final void i() {
        this.x += ++this.p;

        for (int var1 = 0; var1 < 256; ++var1) {
            int var2 = this.b[var1];
            if ((var1 & 2) == 0) {
                if ((var1 & 1) == 0) {
                    this.e ^= this.e << 13;
                } else {
                    this.e ^= this.e >>> 6;
                }
            } else if ((var1 & 1) == 0) {
                this.e ^= this.e << 2;
            } else {
                this.e ^= this.e >>> 16;
            }

            this.e += this.b[var1 + 128 & 255];
            int var3;
            this.b[var1] = var3 = this.b[(var2 & 1020) >> 2] + this.e + this.x;
            this.l[var1] = this.x = this.b[(var3 >> 8 & 1020) >> 2] + var2;
        }

    }

    @ObfuscatedName("a")
    final void a() {
        int var9 = -1640531527;
        int var8 = -1640531527;
        int var7 = -1640531527;
        int var6 = -1640531527;
        int var5 = -1640531527;
        int var4 = -1640531527;
        int var3 = -1640531527;
        int var2 = -1640531527;

        int var1;
        for (var1 = 0; var1 < 4; ++var1) {
            var2 ^= var3 << 11;
            var5 += var2;
            var3 += var4;
            var3 ^= var4 >>> 2;
            var6 += var3;
            var4 += var5;
            var4 ^= var5 << 8;
            var7 += var4;
            var5 += var6;
            var5 ^= var6 >>> 16;
            var8 += var5;
            var6 += var7;
            var6 ^= var7 << 10;
            var9 += var6;
            var7 += var8;
            var7 ^= var8 >>> 4;
            var2 += var7;
            var8 += var9;
            var8 ^= var9 << 8;
            var3 += var8;
            var9 += var2;
            var9 ^= var2 >>> 9;
            var4 += var9;
            var2 += var3;
        }

        for (var1 = 0; var1 < 256; var1 += 8) {
            var2 += this.l[var1];
            var3 += this.l[var1 + 1];
            var4 += this.l[var1 + 2];
            var5 += this.l[var1 + 3];
            var6 += this.l[var1 + 4];
            var7 += this.l[var1 + 5];
            var8 += this.l[var1 + 6];
            var9 += this.l[var1 + 7];
            var2 ^= var3 << 11;
            var5 += var2;
            var3 += var4;
            var3 ^= var4 >>> 2;
            var6 += var3;
            var4 += var5;
            var4 ^= var5 << 8;
            var7 += var4;
            var5 += var6;
            var5 ^= var6 >>> 16;
            var8 += var5;
            var6 += var7;
            var6 ^= var7 << 10;
            var9 += var6;
            var7 += var8;
            var7 ^= var8 >>> 4;
            var2 += var7;
            var8 += var9;
            var8 ^= var9 << 8;
            var3 += var8;
            var9 += var2;
            var9 ^= var2 >>> 9;
            var4 += var9;
            var2 += var3;
            this.b[var1] = var2;
            this.b[var1 + 1] = var3;
            this.b[var1 + 2] = var4;
            this.b[var1 + 3] = var5;
            this.b[var1 + 4] = var6;
            this.b[var1 + 5] = var7;
            this.b[var1 + 6] = var8;
            this.b[var1 + 7] = var9;
        }

        for (var1 = 0; var1 < 256; var1 += 8) {
            var2 += this.b[var1];
            var3 += this.b[var1 + 1];
            var4 += this.b[var1 + 2];
            var5 += this.b[var1 + 3];
            var6 += this.b[var1 + 4];
            var7 += this.b[var1 + 5];
            var8 += this.b[var1 + 6];
            var9 += this.b[var1 + 7];
            var2 ^= var3 << 11;
            var5 += var2;
            var3 += var4;
            var3 ^= var4 >>> 2;
            var6 += var3;
            var4 += var5;
            var4 ^= var5 << 8;
            var7 += var4;
            var5 += var6;
            var5 ^= var6 >>> 16;
            var8 += var5;
            var6 += var7;
            var6 ^= var7 << 10;
            var9 += var6;
            var7 += var8;
            var7 ^= var8 >>> 4;
            var2 += var7;
            var8 += var9;
            var8 ^= var9 << 8;
            var3 += var8;
            var9 += var2;
            var9 ^= var2 >>> 9;
            var4 += var9;
            var2 += var3;
            this.b[var1] = var2;
            this.b[var1 + 1] = var3;
            this.b[var1 + 2] = var4;
            this.b[var1 + 3] = var5;
            this.b[var1 + 4] = var6;
            this.b[var1 + 5] = var7;
            this.b[var1 + 6] = var8;
            this.b[var1 + 7] = var9;
        }

        this.i();
        this.a = 256;
    }

    @ObfuscatedName("q")
    public static double[] q(double var0, double var2, int var4) {
        int var5 = var4 * 2 + 1;
        double[] var6 = new double[var5];
        int var7 = -var4;

        for (int var8 = 0; var7 <= var4; ++var8) {
            var6[var8] = dt.t((double) var7, var0, var2);
            ++var7;
        }

        return var6;
    }

    @ObfuscatedName("a")
    public static void a(int var0) {
        if (hi.audioTrackId != 0) {
            hi.p = var0;
        } else {
            hi.audioTask.t(var0);
        }

    }

    @ObfuscatedName("o")
    static IdentityKitNode o(int var0) {
        IdentityKitNode var1 = (IdentityKitNode) kf.l.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            var1 = ah.t(kf.q, kf.i, var0, false);
            if (var1 != null) {
                kf.l.i(var1, (long) var0);
            }

            return var1;
        }
    }
}
