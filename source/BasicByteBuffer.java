import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ge")
public class BasicByteBuffer extends gr {

    @ObfuscatedName("t")
    java.nio.ByteBuffer buffer;

    @ObfuscatedName("t")
    byte[] t() {
        byte[] var1 = new byte[this.buffer.capacity()];
        this.buffer.position(0);
        this.buffer.get(var1);
        return var1;
    }

    @ObfuscatedName("q")
    void q(byte[] var1) {
        this.buffer = java.nio.ByteBuffer.allocateDirect(var1.length);
        this.buffer.position(0);
        this.buffer.put(var1);
    }
}
