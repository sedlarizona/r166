import java.awt.Component;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ac")
public final class MouseWheelTracker implements fs, MouseWheelListener {

    @ObfuscatedName("t")
    int rotation = 0;

    @ObfuscatedName("t")
    void t(Component var1) {
        var1.addMouseWheelListener(this);
    }

    @ObfuscatedName("q")
    void q(Component var1) {
        var1.removeMouseWheelListener(this);
    }

    @ObfuscatedName("i")
    public synchronized int i() {
        int var1 = this.rotation;
        this.rotation = 0;
        return var1;
    }

    public synchronized void mouseWheelMoved(MouseWheelEvent var1) {
        this.rotation += var1.getWheelRotation();
    }
}
