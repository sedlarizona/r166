import java.math.BigInteger;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gb")
public class ByteBuffer extends Node {

    @ObfuscatedName("i")
    static int[] i = new int[256];

    @ObfuscatedName("l")
    static long[] l;

    @ObfuscatedName("aq")
    protected static boolean aq;

    @ObfuscatedName("t")
    public byte[] buffer;

    @ObfuscatedName("q")
    public int index;

    static {
        int var2;
        for (int var1 = 0; var1 < 256; ++var1) {
            int var0 = var1;

            for (var2 = 0; var2 < 8; ++var2) {
                if ((var0 & 1) == 1) {
                    var0 = var0 >>> 1 ^ -306674912;
                } else {
                    var0 >>>= 1;
                }
            }

            i[var1] = var0;
        }

        l = new long[256];

        for (var2 = 0; var2 < 256; ++var2) {
            long var4 = (long) var2;

            for (int var3 = 0; var3 < 8; ++var3) {
                if (1L == (var4 & 1L)) {
                    var4 = var4 >>> 1 ^ -3932672073523589310L;
                } else {
                    var4 >>>= 1;
                }
            }

            l[var2] = var4;
        }

    }

    public ByteBuffer(int var1) {
        this.buffer = RSException.q(var1);
        this.index = 0;
    }

    public ByteBuffer(byte[] var1) {
        this.buffer = var1;
        this.index = 0;
    }

    @ObfuscatedName("i")
    public void i() {
        if (this.buffer != null) {
            cm.i(this.buffer);
        }

        this.buffer = null;
    }

    @ObfuscatedName("a")
    public void a(int var1) {
        this.buffer[++this.index - 1] = (byte) var1;
    }

    @ObfuscatedName("l")
    public void l(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) var1;
    }

    @ObfuscatedName("b")
    public void b(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 >> 16);
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) var1;
    }

    @ObfuscatedName("e")
    public void e(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 >> 24);
        this.buffer[++this.index - 1] = (byte) (var1 >> 16);
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) var1;
    }

    @ObfuscatedName("x")
    public void x(long var1) {
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 40));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 32));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 24));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 16));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 8));
        this.buffer[++this.index - 1] = (byte) ((int) var1);
    }

    @ObfuscatedName("p")
    public void p(long var1) {
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 56));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 48));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 40));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 32));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 24));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 16));
        this.buffer[++this.index - 1] = (byte) ((int) (var1 >> 8));
        this.buffer[++this.index - 1] = (byte) ((int) var1);
    }

    @ObfuscatedName("o")
    public void o(boolean var1) {
        this.a(var1 ? 1 : 0);
    }

    @ObfuscatedName("u")
    public void u(String var1) {
        int var2 = var1.indexOf(0);
        if (var2 >= 0) {
            throw new IllegalArgumentException("");
        } else {
            this.index += kp.i(var1, 0, var1.length(), this.buffer, this.index);
            this.buffer[++this.index - 1] = 0;
        }
    }

    @ObfuscatedName("z")
    public void z(String var1) {
        int var2 = var1.indexOf(0);
        if (var2 >= 0) {
            throw new IllegalArgumentException("");
        } else {
            this.buffer[++this.index - 1] = 0;
            this.index += kp.i(var1, 0, var1.length(), this.buffer, this.index);
            this.buffer[++this.index - 1] = 0;
        }
    }

    @ObfuscatedName("w")
    public void w(CharSequence var1) {
        int var3 = var1.length();
        int var4 = 0;

        int var5;
        for (var5 = 0; var5 < var3; ++var5) {
            char var6 = var1.charAt(var5);
            if (var6 <= 127) {
                ++var4;
            } else if (var6 <= 2047) {
                var4 += 2;
            } else {
                var4 += 3;
            }
        }

        this.buffer[++this.index - 1] = 0;
        this.h(var4);
        var4 = this.index;
        byte[] var12 = this.buffer;
        int var7 = this.index;
        int var8 = var1.length();
        int var9 = var7;

        for (int var10 = 0; var10 < var8; ++var10) {
            char var11 = var1.charAt(var10);
            if (var11 <= 127) {
                var12[var9++] = (byte) var11;
            } else if (var11 <= 2047) {
                var12[var9++] = (byte) (192 | var11 >> 6);
                var12[var9++] = (byte) (128 | var11 & 63);
            } else {
                var12[var9++] = (byte) (224 | var11 >> 12);
                var12[var9++] = (byte) (128 | var11 >> 6 & 63);
                var12[var9++] = (byte) (128 | var11 & 63);
            }
        }

        var5 = var9 - var7;
        this.index = var5 + var4;
    }

    @ObfuscatedName("s")
    public void s(byte[] var1, int var2, int var3) {
        for (int var4 = var2; var4 < var3 + var2; ++var4) {
            this.buffer[++this.index - 1] = var1[var4];
        }

    }

    @ObfuscatedName("d")
    public void d(int var1) {
        this.buffer[this.index - var1 - 4] = (byte) (var1 >> 24);
        this.buffer[this.index - var1 - 3] = (byte) (var1 >> 16);
        this.buffer[this.index - var1 - 2] = (byte) (var1 >> 8);
        this.buffer[this.index - var1 - 1] = (byte) var1;
    }

    @ObfuscatedName("f")
    public void f(int var1) {
        this.buffer[this.index - var1 - 2] = (byte) (var1 >> 8);
        this.buffer[this.index - var1 - 1] = (byte) var1;
    }

    @ObfuscatedName("r")
    public void r(int var1) {
        this.buffer[this.index - var1 - 1] = (byte) var1;
    }

    @ObfuscatedName("y")
    public void y(int var1) {
        if (var1 >= 0 && var1 < 128) {
            this.a(var1);
        } else if (var1 >= 0 && var1 < 32768) {
            this.l(var1 + '耀');
        } else {
            throw new IllegalArgumentException();
        }
    }

    @ObfuscatedName("h")
    public void h(int var1) {
        if ((var1 & -128) != 0) {
            if ((var1 & -16384) != 0) {
                if ((var1 & -2097152) != 0) {
                    if ((var1 & -268435456) != 0) {
                        this.a(var1 >>> 28 | 128);
                    }

                    this.a(var1 >>> 21 | 128);
                }

                this.a(var1 >>> 14 | 128);
            }

            this.a(var1 >>> 7 | 128);
        }

        this.a(var1 & 127);
    }

    @ObfuscatedName("av")
    public int av() {
        return this.buffer[++this.index - 1] & 255;
    }

    @ObfuscatedName("aj")
    public byte aj() {
        return this.buffer[++this.index - 1];
    }

    @ObfuscatedName("ae")
    public int ae() {
        this.index += 2;
        return (this.buffer[this.index - 1] & 255) + ((this.buffer[this.index - 2] & 255) << 8);
    }

    @ObfuscatedName("am")
    public int am() {
        this.index += 2;
        int var1 = (this.buffer[this.index - 1] & 255) + ((this.buffer[this.index - 2] & 255) << 8);
        if (var1 > 32767) {
            var1 -= 65536;
        }

        return var1;
    }

    @ObfuscatedName("az")
    public int az() {
        this.index += 3;
        return ((this.buffer[this.index - 3] & 255) << 16) + (this.buffer[this.index - 1] & 255)
                + ((this.buffer[this.index - 2] & 255) << 8);
    }

    @ObfuscatedName("ap")
    public int ap() {
        this.index += 4;
        return ((this.buffer[this.index - 3] & 255) << 16) + (this.buffer[this.index - 1] & 255)
                + ((this.buffer[this.index - 2] & 255) << 8) + ((this.buffer[this.index - 4] & 255) << 24);
    }

    @ObfuscatedName("ah")
    public long ah() {
        long var1 = (long) this.ap() & 4294967295L;
        long var3 = (long) this.ap() & 4294967295L;
        return (var1 << 32) + var3;
    }

    @ObfuscatedName("au")
    public boolean au() {
        return (this.av() & 1) == 1;
    }

    @ObfuscatedName("ax")
    public String ax() {
        if (this.buffer[this.index] == 0) {
            ++this.index;
            return null;
        } else {
            return this.ar();
        }
    }

    @ObfuscatedName("ar")
    public String ar() {
        int var1 = this.index;

        while (this.buffer[++this.index - 1] != 0) {
            ;
        }

        int var2 = this.index - var1 - 1;
        return var2 == 0 ? "" : ChatboxMessage.a(this.buffer, var1, var2);
    }

    @ObfuscatedName("an")
    public String an() {
        byte var1 = this.buffer[++this.index - 1];
        if (var1 != 0) {
            throw new IllegalStateException("");
        } else {
            int var2 = this.index;

            while (this.buffer[++this.index - 1] != 0) {
                ;
            }

            int var3 = this.index - var2 - 1;
            return var3 == 0 ? "" : ChatboxMessage.a(this.buffer, var2, var3);
        }
    }

    @ObfuscatedName("ai")
    public String ai() {
        byte var1 = this.buffer[++this.index - 1];
        if (var1 != 0) {
            throw new IllegalStateException("");
        } else {
            int var2 = this.aq();
            if (var2 + this.index > this.buffer.length) {
                throw new IllegalStateException("");
            } else {
                byte[] var4 = this.buffer;
                int var5 = this.index;
                char[] var6 = new char[var2];
                int var7 = 0;
                int var8 = var5;

                int var11;
                for (int var9 = var5 + var2; var8 < var9; var6[var7++] = (char) var11) {
                    int var10 = var4[var8++] & 255;
                    if (var10 < 128) {
                        if (var10 == 0) {
                            var11 = 65533;
                        } else {
                            var11 = var10;
                        }
                    } else if (var10 < 192) {
                        var11 = 65533;
                    } else if (var10 < 224) {
                        if (var8 < var9 && (var4[var8] & 192) == 128) {
                            var11 = (var10 & 31) << 6 | var4[var8++] & 63;
                            if (var11 < 128) {
                                var11 = 65533;
                            }
                        } else {
                            var11 = 65533;
                        }
                    } else if (var10 < 240) {
                        if (var8 + 1 < var9 && (var4[var8] & 192) == 128 && (var4[var8 + 1] & 192) == 128) {
                            var11 = (var10 & 15) << 12 | (var4[var8++] & 63) << 6 | var4[var8++] & 63;
                            if (var11 < 2048) {
                                var11 = 65533;
                            }
                        } else {
                            var11 = 65533;
                        }
                    } else if (var10 < 248) {
                        if (var8 + 2 < var9 && (var4[var8] & 192) == 128 && (var4[var8 + 1] & 192) == 128
                                && (var4[var8 + 2] & 192) == 128) {
                            var11 = (var10 & 7) << 18 | (var4[var8++] & 63) << 12 | (var4[var8++] & 63) << 6
                                    | var4[var8++] & 63;
                            if (var11 >= 65536 && var11 <= 1114111) {
                                var11 = 65533;
                            } else {
                                var11 = 65533;
                            }
                        } else {
                            var11 = 65533;
                        }
                    } else {
                        var11 = 65533;
                    }
                }

                String var3 = new String(var6, 0, var7);
                this.index += var2;
                return var3;
            }
        }
    }

    @ObfuscatedName("al")
    public void al(byte[] var1, int var2, int var3) {
        for (int var4 = var2; var4 < var3 + var2; ++var4) {
            var1[var4] = this.buffer[++this.index - 1];
        }

    }

    @ObfuscatedName("at")
    public int at() {
        int var1 = this.buffer[this.index] & 255;
        return var1 < 128 ? this.av() - 64 : this.ae() - '쀀';
    }

    @ObfuscatedName("ag")
    public int ag() {
        int var1 = this.buffer[this.index] & 255;
        return var1 < 128 ? this.av() : this.ae() - '耀';
    }

    @ObfuscatedName("as")
    public int as() {
        return this.buffer[this.index] < 0 ? this.ap() & Integer.MAX_VALUE : this.ae();
    }

    @ObfuscatedName("aw")
    public int aw() {
        if (this.buffer[this.index] < 0) {
            return this.ap() & Integer.MAX_VALUE;
        } else {
            int var1 = this.ae();
            return var1 == 32767 ? -1 : var1;
        }
    }

    @ObfuscatedName("aq")
    public int aq() {
        byte var1 = this.buffer[++this.index - 1];

        int var2;
        for (var2 = 0; var1 < 0; var1 = this.buffer[++this.index - 1]) {
            var2 = (var2 | var1 & 127) << 7;
        }

        return var2 | var1;
    }

    @ObfuscatedName("aa")
    public void aa(int[] var1) {
        int var2 = this.index / 8;
        this.index = 0;

        for (int var3 = 0; var3 < var2; ++var3) {
            int var4 = this.ap();
            int var5 = this.ap();
            int var6 = 0;
            int var7 = -1640531527;

            for (int var8 = 32; var8-- > 0; var5 += var4 + (var4 << 4 ^ var4 >>> 5) ^ var1[var6 >>> 11 & 3] + var6) {
                var4 += var5 + (var5 << 4 ^ var5 >>> 5) ^ var6 + var1[var6 & 3];
                var6 += var7;
            }

            this.index -= 8;
            this.e(var4);
            this.e(var5);
        }

    }

    @ObfuscatedName("af")
    public void af(int[] var1) {
        int var2 = this.index / 8;
        this.index = 0;

        for (int var3 = 0; var3 < var2; ++var3) {
            int var4 = this.ap();
            int var5 = this.ap();
            int var6 = -957401312;
            int var7 = -1640531527;

            for (int var8 = 32; var8-- > 0; var4 -= var5 + (var5 << 4 ^ var5 >>> 5) ^ var6 + var1[var6 & 3]) {
                var5 -= var4 + (var4 << 4 ^ var4 >>> 5) ^ var1[var6 >>> 11 & 3] + var6;
                var6 -= var7;
            }

            this.index -= 8;
            this.e(var4);
            this.e(var5);
        }

    }

    @ObfuscatedName("ak")
    public void ak(int[] var1, int var2, int var3) {
        int var4 = this.index;
        this.index = var2;
        int var5 = (var3 - var2) / 8;

        for (int var6 = 0; var6 < var5; ++var6) {
            int var7 = this.ap();
            int var8 = this.ap();
            int var9 = 0;
            int var10 = -1640531527;

            for (int var11 = 32; var11-- > 0; var8 += var7 + (var7 << 4 ^ var7 >>> 5) ^ var1[var9 >>> 11 & 3] + var9) {
                var7 += var8 + (var8 << 4 ^ var8 >>> 5) ^ var9 + var1[var9 & 3];
                var9 += var10;
            }

            this.index -= 8;
            this.e(var7);
            this.e(var8);
        }

        this.index = var4;
    }

    @ObfuscatedName("ab")
    public void ab(int[] var1, int var2, int var3) {
        int var4 = this.index;
        this.index = var2;
        int var5 = (var3 - var2) / 8;

        for (int var6 = 0; var6 < var5; ++var6) {
            int var7 = this.ap();
            int var8 = this.ap();
            int var9 = -957401312;
            int var10 = -1640531527;

            for (int var11 = 32; var11-- > 0; var7 -= var8 + (var8 << 4 ^ var8 >>> 5) ^ var9 + var1[var9 & 3]) {
                var8 -= var7 + (var7 << 4 ^ var7 >>> 5) ^ var1[var9 >>> 11 & 3] + var9;
                var9 -= var10;
            }

            this.index -= 8;
            this.e(var7);
            this.e(var8);
        }

        this.index = var4;
    }

    @ObfuscatedName("ac")
    public void ac(BigInteger var1, BigInteger var2) {
        int var3 = this.index;
        this.index = 0;
        byte[] var4 = new byte[var3];
        this.al(var4, 0, var3);
        BigInteger var5 = new BigInteger(var4);
        BigInteger var6 = var5.modPow(var1, var2);
        byte[] var7 = var6.toByteArray();
        this.index = 0;
        this.l(var7.length);
        this.s(var7, 0, var7.length);
    }

    @ObfuscatedName("ad")
    public int ad(int var1) {
        int var2 = fv.t(this.buffer, var1, this.index);
        this.e(var2);
        return var2;
    }

    @ObfuscatedName("bg")
    public boolean bg() {
        this.index -= 4;
        int var1 = fv.t(this.buffer, 0, this.index);
        int var2 = this.ap();
        return var2 == var1;
    }

    @ObfuscatedName("br")
    public void br(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 + 128);
    }

    @ObfuscatedName("ba")
    public void ba(int var1) {
        this.buffer[++this.index - 1] = (byte) (0 - var1);
    }

    @ObfuscatedName("bk")
    public void bk(int var1) {
        this.buffer[++this.index - 1] = (byte) (128 - var1);
    }

    @ObfuscatedName("be")
    public int be() {
        return this.buffer[++this.index - 1] - 128 & 255;
    }

    @ObfuscatedName("bc")
    public int bc() {
        return 0 - this.buffer[++this.index - 1] & 255;
    }

    @ObfuscatedName("bm")
    public int bm() {
        return 128 - this.buffer[++this.index - 1] & 255;
    }

    @ObfuscatedName("bh")
    public byte bh() {
        return (byte) (this.buffer[++this.index - 1] - 128);
    }

    @ObfuscatedName("bs")
    public byte bs() {
        return (byte) (0 - this.buffer[++this.index - 1]);
    }

    @ObfuscatedName("bj")
    public byte bj() {
        return (byte) (128 - this.buffer[++this.index - 1]);
    }

    @ObfuscatedName("bt")
    public void bt(int var1) {
        this.buffer[++this.index - 1] = (byte) var1;
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
    }

    @ObfuscatedName("by")
    public void by(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) (var1 + 128);
    }

    @ObfuscatedName("bn")
    public void bn(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 + 128);
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
    }

    @ObfuscatedName("bb")
    public int bb() {
        this.index += 2;
        return ((this.buffer[this.index - 1] & 255) << 8) + (this.buffer[this.index - 2] & 255);
    }

    @ObfuscatedName("bq")
    public int bq() {
        this.index += 2;
        return (this.buffer[this.index - 1] - 128 & 255) + ((this.buffer[this.index - 2] & 255) << 8);
    }

    @ObfuscatedName("bz")
    public int bz() {
        this.index += 2;
        return ((this.buffer[this.index - 1] & 255) << 8) + (this.buffer[this.index - 2] - 128 & 255);
    }

    @ObfuscatedName("bx")
    public int bx() {
        this.index += 2;
        int var1 = ((this.buffer[this.index - 1] & 255) << 8) + (this.buffer[this.index - 2] & 255);
        if (var1 > 32767) {
            var1 -= 65536;
        }

        return var1;
    }

    @ObfuscatedName("bf")
    public int bf() {
        this.index += 2;
        int var1 = (this.buffer[this.index - 1] - 128 & 255) + ((this.buffer[this.index - 2] & 255) << 8);
        if (var1 > 32767) {
            var1 -= 65536;
        }

        return var1;
    }

    @ObfuscatedName("bo")
    public int bo() {
        this.index += 2;
        int var1 = ((this.buffer[this.index - 1] & 255) << 8) + (this.buffer[this.index - 2] - 128 & 255);
        if (var1 > 32767) {
            var1 -= 65536;
        }

        return var1;
    }

    @ObfuscatedName("bv")
    public void bv(int var1) {
        this.buffer[++this.index - 1] = (byte) var1;
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) (var1 >> 16);
    }

    @ObfuscatedName("bi")
    public int bi() {
        this.index += 3;
        return (this.buffer[this.index - 1] & 255) + ((this.buffer[this.index - 3] & 255) << 8)
                + ((this.buffer[this.index - 2] & 255) << 16);
    }

    @ObfuscatedName("bu")
    public void bu(int var1) {
        this.buffer[++this.index - 1] = (byte) var1;
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) (var1 >> 16);
        this.buffer[++this.index - 1] = (byte) (var1 >> 24);
    }

    @ObfuscatedName("bl")
    public void bl(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
        this.buffer[++this.index - 1] = (byte) var1;
        this.buffer[++this.index - 1] = (byte) (var1 >> 24);
        this.buffer[++this.index - 1] = (byte) (var1 >> 16);
    }

    @ObfuscatedName("bw")
    public void bw(int var1) {
        this.buffer[++this.index - 1] = (byte) (var1 >> 16);
        this.buffer[++this.index - 1] = (byte) (var1 >> 24);
        this.buffer[++this.index - 1] = (byte) var1;
        this.buffer[++this.index - 1] = (byte) (var1 >> 8);
    }

    @ObfuscatedName("bp")
    public int bp() {
        this.index += 4;
        return (this.buffer[this.index - 4] & 255) + ((this.buffer[this.index - 3] & 255) << 8)
                + ((this.buffer[this.index - 2] & 255) << 16) + ((this.buffer[this.index - 1] & 255) << 24);
    }

    @ObfuscatedName("bd")
    public int bd() {
        this.index += 4;
        return ((this.buffer[this.index - 2] & 255) << 24) + ((this.buffer[this.index - 4] & 255) << 8)
                + (this.buffer[this.index - 3] & 255) + ((this.buffer[this.index - 1] & 255) << 16);
    }

    @ObfuscatedName("cb")
    public int cb() {
        this.index += 4;
        return ((this.buffer[this.index - 1] & 255) << 8) + ((this.buffer[this.index - 4] & 255) << 16)
                + (this.buffer[this.index - 2] & 255) + ((this.buffer[this.index - 3] & 255) << 24);
    }

    @ObfuscatedName("cm")
    public void cm(byte[] var1, int var2, int var3) {
        for (int var4 = var3 + var2 - 1; var4 >= var2; --var4) {
            var1[var4] = (byte) (this.buffer[++this.index - 1] - 128);
        }

    }

    @ObfuscatedName("ij")
    static final String ij(int var0) {
        return var0 < 999999999 ? Integer.toString(var0) : "*";
    }
}
