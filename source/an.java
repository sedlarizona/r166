import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("an")
public class an {

    @ObfuscatedName("ec")
    static int regionBaseX;

    @ObfuscatedName("fz")
    static Region loadedRegion;

    @ObfuscatedName("fv")
    static lk[] fv;

    @ObfuscatedName("t")
    public static jf regionChanged(int var0) {
        jf var1 = (jf) jf.a.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = io.t.i(3, var0);
            var1 = new jf();
            if (var2 != null) {
                var1.q(new ByteBuffer(var2));
            }

            jf.a.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("q")
    static void q(Sprite var0, int var1, int var2, int var3) {
        he var4 = au.e;
        long var6 = (long) (var3 << 16 | var1 << 8 | var2);
        var4.l(var0, var6, var0.pixels.length * 4);
    }

    @ObfuscatedName("jc")
    static void jc() {
        for (SubWindow var0 = (SubWindow) Client.subWindowTable.a(); var0 != null; var0 = (SubWindow) Client.subWindowTable
                .l()) {
            int var1 = var0.targetWindowId;
            if (RuneScript.i(var1)) {
                boolean var2 = true;
                RTComponent[] var3 = RTComponent.b[var1];

                int var4;
                for (var4 = 0; var4 < var3.length; ++var4) {
                    if (var3[var4] != null) {
                        var2 = var3[var4].modern;
                        break;
                    }
                }

                if (!var2) {
                    var4 = (int) var0.uid;
                    RTComponent var5 = Inflater.t(var4);
                    if (var5 != null) {
                        GameEngine.jk(var5);
                    }
                }
            }
        }

    }
}
