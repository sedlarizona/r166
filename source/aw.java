import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("aw")
public class aw {

    @ObfuscatedName("f")
    public static String f;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int[][] i;

    @ObfuscatedName("a")
    int[][] a;

    @ObfuscatedName("l")
    int[][] l;

    @ObfuscatedName("b")
    int[][] b;

    aw(int var1, int var2) {
        this.t = var1;
        this.q = var2;
        this.i = new int[var1][var2];
        this.a = new int[var1][var2];
        this.l = new int[var1][var2];
        this.b = new int[var1][var2];
    }

    @ObfuscatedName("t")
    void t(int var1, int var2, int var3, js var4) {
        if (var4 != null) {
            if (var3 + var1 >= 0 && var3 + var2 >= 0) {
                if (var1 - var3 <= this.t && var2 - var3 <= this.q) {
                    int var5 = Math.max(0, var1 - var3);
                    int var6 = Math.min(this.t, var3 + var1);
                    int var7 = Math.max(0, var2 - var3);
                    int var8 = Math.min(this.q, var3 + var2);

                    for (int var9 = var5; var9 < var6; ++var9) {
                        for (int var10 = var7; var10 < var8; ++var10) {
                            this.i[var9][var10] += var4.a * 256 / var4.e;
                            this.a[var9][var10] += var4.l;
                            this.l[var9][var10] += var4.b;
                            ++this.b[var9][var10];
                        }
                    }

                }
            }
        }
    }

    @ObfuscatedName("q")
    int q(int var1, int var2) {
        if (var1 >= 0 && var2 >= 0 && var1 < this.t && var2 < this.q) {
            if (this.l[var1][var2] == 0) {
                return 0;
            } else {
                int var3 = this.i[var1][var2] / this.b[var1][var2];
                int var4 = this.a[var1][var2] / this.b[var1][var2];
                int var5 = this.l[var1][var2] / this.b[var1][var2];
                return o.t((double) var3 / 256.0D, (double) var4 / 256.0D, (double) var5 / 256.0D);
            }
        } else {
            return 0;
        }
    }

    @ObfuscatedName("hh")
    static final void hh() {
        int var0 = dg.jv;
        int var1 = ci.menuY;
        int var2 = y.menuWidth;
        int var3 = ak.jy;
        int var4 = 6116423;
        li.dl(var0, var1, var2, var3, var4);
        li.dl(var0 + 1, var1 + 1, var2 - 2, 16, 0);
        li.dw(var0 + 1, var1 + 18, var2 - 2, var3 - 19, 0);
        b.ev.s("Choose Option", var0 + 3, var1 + 14, var4, -1);
        int var5 = bs.n;
        int var6 = bs.x;

        for (int var7 = 0; var7 < Client.menuSize; ++var7) {
            int var8 = (Client.menuSize - 1 - var7) * 15 + var1 + 31;
            int var9 = 16777215;
            if (var5 > var0 && var5 < var2 + var0 && var6 > var8 - 13 && var6 < var8 + 3) {
                var9 = 16776960;
            }

            b.ev.s(hb(var7), var0 + 3, var8, var9, 0);
        }

        ItemStorage.hn(dg.jv, ci.menuY, y.menuWidth, ak.jy);
    }

    @ObfuscatedName("hb")
    static String hb(int var0) {
        if (var0 < 0) {
            return "";
        } else {
            return Client.menuTargets[var0].length() > 0 ? Client.menuActions[var0] + " " + Client.menuTargets[var0]
                    : Client.menuActions[var0];
        }
    }

    @ObfuscatedName("it")
    static void it(RTComponent var0, int var1, int var2, boolean var3) {
        int var4 = var0.width;
        int var5 = var0.height;
        if (var0.h == 0) {
            var0.width = var0.av;
        } else if (var0.h == 1) {
            var0.width = var1 - var0.av;
        } else if (var0.h == 2) {
            var0.width = var0.av * var1 >> 14;
        }

        if (var0.m == 0) {
            var0.height = var0.aj;
        } else if (var0.m == 1) {
            var0.height = var2 - var0.aj;
        } else if (var0.m == 2) {
            var0.height = var2 * var0.aj >> 14;
        }

        if (var0.h == 4) {
            var0.width = var0.height * var0.ah / var0.au;
        }

        if (var0.m == 4) {
            var0.height = var0.width * var0.au / var0.ah;
        }

        if (var0.contentType == 1337) {
            Client.lz = var0;
        }

        if (var3 && var0.dm != null && (var4 != var0.width || var5 != var0.height)) {
            ScriptEvent var6 = new ScriptEvent();
            var6.i = var0;
            var6.args = var0.dm;
            Client.me.q(var6);
        }

    }
}
