import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("iy")
public class iy {

    @ObfuscatedName("k")
    public static int k(String var0) {
        return var0.length() + 2;
    }
}
