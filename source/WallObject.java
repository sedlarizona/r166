import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("el")
public final class WallObject {

    @ObfuscatedName("t")
    int height;

    @ObfuscatedName("q")
    int regionX;

    @ObfuscatedName("i")
    int regionY;

    @ObfuscatedName("a")
    int orientationA;

    @ObfuscatedName("l")
    int orientationB;

    @ObfuscatedName("b")
    int insetX;

    @ObfuscatedName("e")
    int insetY;

    @ObfuscatedName("x")
    public Renderable modelA;

    @ObfuscatedName("p")
    public Renderable modelB;

    @ObfuscatedName("g")
    public int hash = 0;

    @ObfuscatedName("n")
    int placement = 0;
}
