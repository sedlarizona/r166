import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("q")
public class q {

    @ObfuscatedName("t")
    static final q t = new q(3);

    @ObfuscatedName("q")
    static final q q = new q(0);

    @ObfuscatedName("i")
    static final q i = new q(6);

    @ObfuscatedName("a")
    static final q a = new q(4);

    @ObfuscatedName("l")
    static final q l = new q(1);

    @ObfuscatedName("b")
    static final q b = new q(5);

    @ObfuscatedName("e")
    static final q e = new q(2);

    @ObfuscatedName("ba")
    static DevelopmentStage ba;

    @ObfuscatedName("x")
    final int x;

    q(int var1) {
        this.x = var1;
    }

    @ObfuscatedName("t")
    public static Sprite t(FileSystem var0, int var1, int var2) {
        byte[] var4 = var0.i(var1, var2);
        boolean var3;
        if (var4 == null) {
            var3 = false;
        } else {
            RTComponent.k(var4);
            var3 = true;
        }

        return !var3 ? null : ItemNode.c();
    }

    @ObfuscatedName("t")
    static RuneScript t(byte[] var0) {
        RuneScript var1 = new RuneScript();
        ByteBuffer var2 = new ByteBuffer(var0);
        var2.index = var2.buffer.length - 2;
        int var3 = var2.ae();
        int var4 = var2.buffer.length - 2 - var3 - 12;
        var2.index = var4;
        int var5 = var2.ap();
        var1.stringArgCount = var2.ae();
        var1.stringStackCount = var2.ae();
        var1.intArgCount = var2.ae();
        var1.intStackCount = var2.ae();
        int var6 = var2.av();
        int var7;
        int var8;
        if (var6 > 0) {
            var1.p = var1.q(var6);

            for (var7 = 0; var7 < var6; ++var7) {
                var8 = var2.ae();
                int var9;
                int var11;
                if (var8 > 0) {
                    var11 = var8 - 1;
                    var11 |= var11 >>> 1;
                    var11 |= var11 >>> 2;
                    var11 |= var11 >>> 4;
                    var11 |= var11 >>> 8;
                    var11 |= var11 >>> 16;
                    int var10 = var11 + 1;
                    var9 = var10;
                } else {
                    var9 = 1;
                }

                FixedSizeDeque var13 = new FixedSizeDeque(var9);
                var1.p[var7] = var13;

                while (var8-- > 0) {
                    var11 = var2.ap();
                    int var12 = var2.ap();
                    var13.q(new hc(var12), (long) var11);
                }
            }
        }

        var2.index = 0;
        var2.ax();
        var1.q = new int[var5];
        var1.i = new int[var5];
        var1.a = new String[var5];

        for (var7 = 0; var2.index < var4; var1.q[var7++] = var8) {
            var8 = var2.ae();
            if (var8 == 3) {
                var1.a[var7] = var2.ar();
            } else if (var8 < 100 && var8 != 21 && var8 != 38 && var8 != 39) {
                var1.i[var7] = var2.ap();
            } else {
                var1.i[var7] = var2.av();
            }
        }

        return var1;
    }

    @ObfuscatedName("gx")
    static final void gx(int var0) {
        int[] var1 = GrandExchangeOffer.on.pixels;
        int var2 = var1.length;

        int var3;
        for (var3 = 0; var3 < var2; ++var3) {
            var1[var3] = 0;
        }

        int var4;
        int var5;
        for (var3 = 1; var3 < 103; ++var3) {
            var4 = (103 - var3) * 2048 + 24628;

            for (var5 = 1; var5 < 103; ++var5) {
                if ((bt.landscapeData[var0][var5][var3] & 24) == 0) {
                    an.loadedRegion.at(var1, var4, 512, var0, var5, var3);
                }

                if (var0 < 3 && (bt.landscapeData[var0 + 1][var5][var3] & 8) != 0) {
                    an.loadedRegion.at(var1, var4, 512, var0 + 1, var5, var3);
                }

                var4 += 4;
            }
        }

        var3 = (238 + (int) (Math.random() * 20.0D) - 10 << 16) + (238 + (int) (Math.random() * 20.0D) - 10 << 8)
                + (238 + (int) (Math.random() * 20.0D) - 10);
        var4 = 238 + (int) (Math.random() * 20.0D) - 10 << 16;
        GrandExchangeOffer.on.i();

        int var6;
        for (var5 = 1; var5 < 103; ++var5) {
            for (var6 = 1; var6 < 103; ++var6) {
                if ((bt.landscapeData[var0][var6][var5] & 24) == 0) {
                    n.gj(var0, var6, var5, var3, var4);
                }

                if (var0 < 3 && (bt.landscapeData[var0 + 1][var6][var5] & 8) != 0) {
                    n.gj(var0 + 1, var6, var5, var3, var4);
                }
            }
        }

        Client.oc = 0;

        for (var5 = 0; var5 < 104; ++var5) {
            for (var6 = 0; var6 < 104; ++var6) {
                int var7 = an.loadedRegion.ax(kt.ii, var5, var6);
                if (var7 != 0) {
                    var7 = var7 >> 14 & 32767;
                    int var8 = jw.q(var7).mapFunction;
                    if (var8 >= 0) {
                        Client.ou[Client.oc] = jj.q[var8].a(false);
                        Client.ov[Client.oc] = var5;
                        Client.od[Client.oc] = var6;
                        ++Client.oc;
                    }
                }
            }
        }

        ad.interfaceProducer.aj();
    }
}
