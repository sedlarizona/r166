import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ez")
public class ez {

    @ObfuscatedName("g")
    static lk g;

    @ObfuscatedName("fh")
    static int fh;

    @ObfuscatedName("gv")
    static int gv;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;
}
