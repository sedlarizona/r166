import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ey")
public class ey {

    @ObfuscatedName("e")
    public static int e(CharSequence var0) {
        int var1 = var0.length();
        int var2 = 0;

        for (int var3 = 0; var3 < var1; ++var3) {
            var2 = (var2 << 5) - var2 + ko.t(var0.charAt(var3));
        }

        return var2;
    }
}
