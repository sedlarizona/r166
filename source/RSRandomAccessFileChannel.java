import java.io.EOFException;
import java.io.IOException;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dk")
public class RSRandomAccessFileChannel {

    @ObfuscatedName("j")
    public static ByteBuffer j;

    @ObfuscatedName("t")
    RSRandomAccessFile file;

    @ObfuscatedName("q")
    byte[] q;

    @ObfuscatedName("i")
    long i = -1L;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    byte[] l;

    @ObfuscatedName("b")
    long b = -1L;

    @ObfuscatedName("e")
    int e = 0;

    @ObfuscatedName("x")
    long x;

    @ObfuscatedName("p")
    long p;

    @ObfuscatedName("g")
    long g;

    @ObfuscatedName("n")
    long n;

    public RSRandomAccessFileChannel(RSRandomAccessFile var1, int var2, int var3) throws IOException {
        this.file = var1;
        this.g = this.p = var1.l();
        this.q = new byte[var2];
        this.l = new byte[var3];
        this.x = 0L;
    }

    @ObfuscatedName("t")
    public void t()
            throws IOException {
        this.x();
        this.file.i();
    }

    @ObfuscatedName("q")
    public void q(long var1)
            throws IOException {
        if (var1 < 0L) {
            throw new IOException("");
        } else {
            this.x = var1;
        }
    }

    @ObfuscatedName("i")
    public long i() {
        return this.g;
    }

    @ObfuscatedName("a")
    public void a(byte[] var1)
            throws IOException {
        this.l(var1, 0, var1.length);
    }

    @ObfuscatedName("l")
    public void l(byte[] var1, int var2, int var3)
            throws IOException {
        try {
            if (var3 + var2 > var1.length) {
                throw new ArrayIndexOutOfBoundsException(var3 + var2 - var1.length);
            }

            if (-1L != this.b && this.x >= this.b && (long) var3 + this.x <= (long) this.e + this.b) {
                System.arraycopy(this.l, (int) (this.x - this.b), var1, var2, var3);
                this.x += (long) var3;
                return;
            }

            long var4 = this.x;
            int var7 = var3;
            int var8;
            if (this.x >= this.i && this.x < this.i + (long) this.a) {
                var8 = (int) ((long) this.a - (this.x - this.i));
                if (var8 > var3) {
                    var8 = var3;
                }

                System.arraycopy(this.q, (int) (this.x - this.i), var1, var2, var8);
                this.x += (long) var8;
                var2 += var8;
                var3 -= var8;
            }

            if (var3 > this.q.length) {
                this.file.t(this.x);

                for (this.n = this.x; var3 > 0; var3 -= var8) {
                    var8 = this.file.b(var1, var2, var3);
                    if (var8 == -1) {
                        break;
                    }

                    this.n += (long) var8;
                    this.x += (long) var8;
                    var2 += var8;
                }
            } else if (var3 > 0) {
                this.b();
                var8 = var3;
                if (var3 > this.a) {
                    var8 = this.a;
                }

                System.arraycopy(this.q, 0, var1, var2, var8);
                var2 += var8;
                var3 -= var8;
                this.x += (long) var8;
            }

            if (this.b != -1L) {
                if (this.b > this.x && var3 > 0) {
                    var8 = var2 + (int) (this.b - this.x);
                    if (var8 > var3 + var2) {
                        var8 = var3 + var2;
                    }

                    while (var2 < var8) {
                        var1[var2++] = 0;
                        --var3;
                        ++this.x;
                    }
                }

                long var13 = -1L;
                long var10 = -1L;
                if (this.b >= var4 && this.b < (long) var7 + var4) {
                    var13 = this.b;
                } else if (var4 >= this.b && var4 < (long) this.e + this.b) {
                    var13 = var4;
                }

                if (this.b + (long) this.e > var4 && (long) this.e + this.b <= var4 + (long) var7) {
                    var10 = this.b + (long) this.e;
                } else if (var4 + (long) var7 > this.b && var4 + (long) var7 <= (long) this.e + this.b) {
                    var10 = (long) var7 + var4;
                }

                if (var13 > -1L && var10 > var13) {
                    int var12 = (int) (var10 - var13);
                    System.arraycopy(this.l, (int) (var13 - this.b), var1, (int) (var13 - var4) + var2, var12);
                    if (var10 > this.x) {
                        var3 = (int) ((long) var3 - (var10 - this.x));
                        this.x = var10;
                    }
                }
            }
        } catch (IOException var16) {
            this.n = -1L;
            throw var16;
        }

        if (var3 > 0) {
            throw new EOFException();
        }
    }

    @ObfuscatedName("b")
    void b()
            throws IOException {
        this.a = 0;
        if (this.n != this.x) {
            this.file.t(this.x);
            this.n = this.x;
        }

        int var1;
        for (this.i = this.x; this.a < this.q.length; this.a += var1) {
            var1 = this.file.b(this.q, this.a, this.q.length - this.a);
            if (var1 == -1) {
                break;
            }

            this.n += (long) var1;
        }

    }

    @ObfuscatedName("e")
    public void e(byte[] var1, int var2, int var3)
            throws IOException {
        try {
            if (this.x + (long) var3 > this.g) {
                this.g = (long) var3 + this.x;
            }

            if (this.b != -1L && (this.x < this.b || this.x > (long) this.e + this.b)) {
                this.x();
            }

            if (this.b != -1L && (long) var3 + this.x > this.b + (long) this.l.length) {
                int var4 = (int) ((long) this.l.length - (this.x - this.b));
                System.arraycopy(var1, var2, this.l, (int) (this.x - this.b), var4);
                this.x += (long) var4;
                var2 += var4;
                var3 -= var4;
                this.e = this.l.length;
                this.x();
            }

            if (var3 <= this.l.length) {
                if (var3 > 0) {
                    if (-1L == this.b) {
                        this.b = this.x;
                    }

                    System.arraycopy(var1, var2, this.l, (int) (this.x - this.b), var3);
                    this.x += (long) var3;
                    if (this.x - this.b > (long) this.e) {
                        this.e = (int) (this.x - this.b);
                    }

                }
            } else {
                if (this.x != this.n) {
                    this.file.t(this.x);
                    this.n = this.x;
                }

                this.file.q(var1, var2, var3);
                this.n += (long) var3;
                if (this.n > this.p) {
                    this.p = this.n;
                }

                long var9 = -1L;
                long var6 = -1L;
                if (this.x >= this.i && this.x < (long) this.a + this.i) {
                    var9 = this.x;
                } else if (this.i >= this.x && this.i < this.x + (long) var3) {
                    var9 = this.i;
                }

                if (this.x + (long) var3 > this.i && this.x + (long) var3 <= (long) this.a + this.i) {
                    var6 = this.x + (long) var3;
                } else if (this.i + (long) this.a > this.x && this.i + (long) this.a <= this.x + (long) var3) {
                    var6 = (long) this.a + this.i;
                }

                if (var9 > -1L && var6 > var9) {
                    int var8 = (int) (var6 - var9);
                    System.arraycopy(var1, (int) ((long) var2 + var9 - this.x), this.q, (int) (var9 - this.i), var8);
                }

                this.x += (long) var3;
            }
        } catch (IOException var12) {
            this.n = -1L;
            throw var12;
        }
    }

    @ObfuscatedName("x")
    void x()
            throws IOException {
        if (this.b != -1L) {
            if (this.b != this.n) {
                this.file.t(this.b);
                this.n = this.b;
            }

            this.file.q(this.l, 0, this.e);
            this.n += (long) (this.e * -633452679) * 1408660169L;
            if (this.n > this.p) {
                this.p = this.n;
            }

            long var1 = -1L;
            long var3 = -1L;
            if (this.b >= this.i && this.b < (long) this.a + this.i) {
                var1 = this.b;
            } else if (this.i >= this.b && this.i < (long) this.e + this.b) {
                var1 = this.i;
            }

            if (this.b + (long) this.e > this.i && this.b + (long) this.e <= this.i + (long) this.a) {
                var3 = (long) this.e + this.b;
            } else if (this.i + (long) this.a > this.b && this.i + (long) this.a <= this.b + (long) this.e) {
                var3 = this.i + (long) this.a;
            }

            if (var1 > -1L && var3 > var1) {
                int var5 = (int) (var3 - var1);
                System.arraycopy(this.l, (int) (var1 - this.b), this.q, (int) (var1 - this.i), var5);
            }

            this.b = -1L;
            this.e = 0;
        }

    }
}
