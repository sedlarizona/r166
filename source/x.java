import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("x")
public class x {

    @ObfuscatedName("t")
    public static boolean t = false;

    @ObfuscatedName("q")
    public static boolean q = false;

    @ObfuscatedName("i")
    public static boolean i = false;

    @ObfuscatedName("a")
    public static o a;

    @ObfuscatedName("l")
    public static SinglyLinkedList l;

    @ObfuscatedName("ab")
    static int ab;

    static {
        a = o.t;
        l = new SinglyLinkedList();
    }
}
