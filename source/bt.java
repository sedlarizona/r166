import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bt")
public final class bt {

    @ObfuscatedName("t")
    static int[][][] tileHeights = new int[4][105][105];

    @ObfuscatedName("q")
    static byte[][][] landscapeData = new byte[4][104][104];

    @ObfuscatedName("i")
    static int i = 99;

    @ObfuscatedName("a")
    static byte[][][] a;

    @ObfuscatedName("l")
    static byte[][][] l;

    @ObfuscatedName("b")
    static byte[][][] b;

    @ObfuscatedName("x")
    static byte[][][] x;

    @ObfuscatedName("p")
    static int[][] p;

    @ObfuscatedName("u")
    static int[][][] u;

    @ObfuscatedName("w")
    static final int[] w = new int[] { 1, 2, 4, 8 };

    @ObfuscatedName("s")
    static final int[] s = new int[] { 16, 32, 64, 128 };

    @ObfuscatedName("d")
    static final int[] d = new int[] { 1, 0, -1, 0 };

    @ObfuscatedName("f")
    static final int[] f = new int[] { 0, -1, 0, 1 };

    @ObfuscatedName("r")
    static final int[] r = new int[] { 1, -1, -1, 1 };

    @ObfuscatedName("y")
    static final int[] y = new int[] { -1, -1, 1, 1 };

    @ObfuscatedName("h")
    static int h = (int) (Math.random() * 17.0D) - 8;

    @ObfuscatedName("m")
    static int m = (int) (Math.random() * 33.0D) - 16;

    @ObfuscatedName("ck")
    static ju ck;

    @ObfuscatedName("fj")
    static Sprite[] fj;

    @ObfuscatedName("gh")
    static int cameraYaw;

    @ObfuscatedName("gh")
    static void gh(Player var0, boolean var1) {
        if (var0 != null && var0.k() && !var0.f) {
            int var2 = var0.y << 14;
            var0.intransformable = false;
            if ((Client.bh && cx.b > 50 || cx.b > 200) && var1 && var0.bs == var0.au) {
                var0.intransformable = true;
            }

            int var3 = var0.regionX >> 7;
            int var4 = var0.regionY >> 7;
            if (var3 >= 0 && var3 < 104 && var4 >= 0 && var4 < 104) {
                if (var0.transformObjectModel != null && Client.bz >= var0.transformObjectStartCycle
                        && Client.bz < var0.transformObjectEndCycle) {
                    var0.intransformable = false;
                    var0.p = ef.gn(var0.regionX, var0.regionY, kt.ii);
                    var0.ah = Client.bz;
                    an.loadedRegion.k(kt.ii, var0.regionX, var0.regionY, var0.p, 60, var0, var0.orientation, var2,
                            var0.j, var0.k, var0.z, var0.w);
                } else {
                    if ((var0.regionX & 127) == 64 && (var0.regionY & 127) == 64) {
                        if (Client.hl[var3][var4] == Client.hu) {
                            return;
                        }

                        Client.hl[var3][var4] = Client.hu;
                    }

                    var0.p = ef.gn(var0.regionX, var0.regionY, kt.ii);
                    var0.ah = Client.bz;
                    an.loadedRegion.u(kt.ii, var0.regionX, var0.regionY, var0.p, 60, var0, var0.orientation, var2,
                            var0.animatingHeldItems);
                }
            }
        }

    }
}
