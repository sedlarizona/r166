import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.DataLine.Info;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("be")
public class AudioTaskData extends TaskData {

    @ObfuscatedName("t")
    AudioFormat format;

    @ObfuscatedName("q")
    SourceDataLine dataLine;

    @ObfuscatedName("i")
    int length;

    @ObfuscatedName("a")
    byte[] bytes;

    @ObfuscatedName("t")
    protected void t() {
        this.format = new AudioFormat((float) TaskData.l, 16, TaskData.b ? 2 : 1, true, false);
        this.bytes = new byte[256 << (TaskData.b ? 2 : 1)];
    }

    @ObfuscatedName("q")
    protected void q(int var1)
            throws LineUnavailableException {
        try {
            Info var2 = new Info(SourceDataLine.class, this.format, var1 << (TaskData.b ? 2 : 1));
            this.dataLine = (SourceDataLine) AudioSystem.getLine(var2);
            this.dataLine.open();
            this.dataLine.start();
            this.length = var1;
        } catch (LineUnavailableException var5) {
            if (ec.i(var1) != 1) {
                int var4 = var1 - 1;
                var4 |= var4 >>> 1;
                var4 |= var4 >>> 2;
                var4 |= var4 >>> 4;
                var4 |= var4 >>> 8;
                var4 |= var4 >>> 16;
                int var3 = var4 + 1;
                this.q(var3);
            } else {
                this.dataLine = null;
                throw var5;
            }
        }
    }

    @ObfuscatedName("i")
    protected int i() {
        return this.length - (this.dataLine.available() >> (TaskData.b ? 2 : 1));
    }

    @ObfuscatedName("a")
    protected void a() {
        int var1 = 256;
        if (TaskData.b) {
            var1 <<= 1;
        }

        for (int var2 = 0; var2 < var1; ++var2) {
            int var3 = super.v[var2];
            if ((var3 + 8388608 & -16777216) != 0) {
                var3 = 8388607 ^ var3 >> 31;
            }

            this.bytes[var2 * 2] = (byte) (var3 >> 8);
            this.bytes[var2 * 2 + 1] = (byte) (var3 >> 16);
        }

        this.dataLine.write(this.bytes, 0, var1 << 1);
    }

    @ObfuscatedName("l")
    protected void l() {
        if (this.dataLine != null) {
            this.dataLine.close();
            this.dataLine = null;
        }

    }

    @ObfuscatedName("b")
    protected void b() {
        this.dataLine.flush();
    }
}
