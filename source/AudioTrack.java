import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ck")
public class AudioTrack {

    @ObfuscatedName("t")
    int loopStart;

    @ObfuscatedName("i")
    AudioInstrument[] instruments = new AudioInstrument[10];

    @ObfuscatedName("a")
    int loopEnd;

    AudioTrack(ByteBuffer var1) {
        for (int var2 = 0; var2 < 10; ++var2) {
            int var3 = var1.av();
            if (var3 != 0) {
                --var1.index;
                this.instruments[var2] = new AudioInstrument();
                this.instruments[var2].i(var1);
            }
        }

        this.loopStart = var1.ae();
        this.loopEnd = var1.ae();
    }

    @ObfuscatedName("q")
    public DataRequestNode q() {
        byte[] var1 = this.a();
        return new DataRequestNode(22050, var1, this.loopStart * 22050 / 1000, this.loopEnd * 22050 / 1000);
    }

    @ObfuscatedName("i")
    public final int i() {
        int var1 = 9999999;

        int var2;
        for (var2 = 0; var2 < 10; ++var2) {
            if (this.instruments[var2] != null && this.instruments[var2].startTime / 20 < var1) {
                var1 = this.instruments[var2].startTime / 20;
            }
        }

        if (this.loopStart < this.loopEnd && this.loopStart / 20 < var1) {
            var1 = this.loopStart / 20;
        }

        if (var1 != 9999999 && var1 != 0) {
            for (var2 = 0; var2 < 10; ++var2) {
                if (this.instruments[var2] != null) {
                    this.instruments[var2].startTime -= var1 * 20;
                }
            }

            if (this.loopStart < this.loopEnd) {
                this.loopStart -= var1 * 20;
                this.loopEnd -= var1 * 20;
            }

            return var1;
        } else {
            return 0;
        }
    }

    @ObfuscatedName("a")
    final byte[] a() {
        int var1 = 0;

        int var2;
        for (var2 = 0; var2 < 10; ++var2) {
            if (this.instruments[var2] != null
                    && this.instruments[var2].duration + this.instruments[var2].startTime > var1) {
                var1 = this.instruments[var2].duration + this.instruments[var2].startTime;
            }
        }

        if (var1 == 0) {
            return new byte[0];
        } else {
            var2 = var1 * 22050 / 1000;
            byte[] var3 = new byte[var2];

            for (int var4 = 0; var4 < 10; ++var4) {
                if (this.instruments[var4] != null) {
                    int var5 = this.instruments[var4].duration * 22050 / 1000;
                    int var6 = this.instruments[var4].startTime * 22050 / 1000;
                    int[] var7 = this.instruments[var4].t(var5, this.instruments[var4].duration);

                    for (int var8 = 0; var8 < var5; ++var8) {
                        int var9 = (var7[var8] >> 8) + var3[var8 + var6];
                        if ((var9 + 128 & -256) != 0) {
                            var9 = var9 >> 31 ^ 127;
                        }

                        var3[var8 + var6] = (byte) var9;
                    }
                }
            }

            return var3;
        }
    }

    @ObfuscatedName("t")
    public static AudioTrack t(FileSystem var0, int var1, int var2) {
        byte[] var3 = var0.i(var1, var2);
        return var3 == null ? null : new AudioTrack(new ByteBuffer(var3));
    }
}
