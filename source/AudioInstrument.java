import java.util.Random;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dd")
public class AudioInstrument {

    @ObfuscatedName("z")
    static int[] z;

    @ObfuscatedName("w")
    static int[] w = new int['耀'];

    @ObfuscatedName("s")
    static int[] s;

    @ObfuscatedName("f")
    static int[] f;

    @ObfuscatedName("r")
    static int[] r;

    @ObfuscatedName("y")
    static int[] y;

    @ObfuscatedName("h")
    static int[] h;

    @ObfuscatedName("m")
    static int[] m;

    @ObfuscatedName("t")
    AudioEnvelope pitch;

    @ObfuscatedName("q")
    AudioEnvelope volume;

    @ObfuscatedName("i")
    AudioEnvelope pitchModifier;

    @ObfuscatedName("a")
    AudioEnvelope pitchAmplifier;

    @ObfuscatedName("l")
    AudioEnvelope volumeMode;

    @ObfuscatedName("b")
    AudioEnvelope volumeModifier;

    @ObfuscatedName("e")
    AudioEnvelope gatingRelease;

    @ObfuscatedName("x")
    AudioEnvelope gatingAttack;

    @ObfuscatedName("p")
    int[] oscillatingVolumes = new int[] { 0, 0, 0, 0, 0 };

    @ObfuscatedName("g")
    int[] oscillatingPitches = new int[] { 0, 0, 0, 0, 0 };

    @ObfuscatedName("n")
    int[] oscillatingDelays = new int[] { 0, 0, 0, 0, 0 };

    @ObfuscatedName("o")
    int delayTime = 0;

    @ObfuscatedName("c")
    int delayFeedback = 100;

    @ObfuscatedName("v")
    dn soundFilter;

    @ObfuscatedName("u")
    AudioEnvelope filter;

    @ObfuscatedName("j")
    int duration = 500;

    @ObfuscatedName("k")
    int startTime = 0;

    static {
        Random var0 = new Random(0L);

        int var1;
        for (var1 = 0; var1 < 32768; ++var1) {
            w[var1] = (var0.nextInt() & 2) - 1;
        }

        s = new int['耀'];

        for (var1 = 0; var1 < 32768; ++var1) {
            s[var1] = (int) (Math.sin((double) var1 / 5215.1903D) * 16384.0D);
        }

        z = new int[220500];
        f = new int[5];
        r = new int[5];
        y = new int[5];
        h = new int[5];
        m = new int[5];
    }

    @ObfuscatedName("t")
    final int[] t(int var1, int var2) {
        gj.o(z, 0, var1);
        if (var2 < 10) {
            return z;
        } else {
            double var3 = (double) var1 / ((double) var2 + 0.0D);
            this.pitch.i();
            this.volume.i();
            int var5 = 0;
            int var6 = 0;
            int var7 = 0;
            if (this.pitchModifier != null) {
                this.pitchModifier.i();
                this.pitchAmplifier.i();
                var5 = (int) ((double) (this.pitchModifier.end - this.pitchModifier.start) * 32.768D / var3);
                var6 = (int) ((double) this.pitchModifier.start * 32.768D / var3);
            }

            int var8 = 0;
            int var9 = 0;
            int var10 = 0;
            if (this.volumeMode != null) {
                this.volumeMode.i();
                this.volumeModifier.i();
                var8 = (int) ((double) (this.volumeMode.end - this.volumeMode.start) * 32.768D / var3);
                var9 = (int) ((double) this.volumeMode.start * 32.768D / var3);
            }

            int var11;
            for (var11 = 0; var11 < 5; ++var11) {
                if (this.oscillatingVolumes[var11] != 0) {
                    f[var11] = 0;
                    r[var11] = (int) ((double) this.oscillatingDelays[var11] * var3);
                    y[var11] = (this.oscillatingVolumes[var11] << 14) / 100;
                    h[var11] = (int) ((double) (this.pitch.end - this.pitch.start) * 32.768D
                            * Math.pow(1.0057929410678534D, (double) this.oscillatingPitches[var11]) / var3);
                    m[var11] = (int) ((double) this.pitch.start * 32.768D / var3);
                }
            }

            int var12;
            int var13;
            int var14;
            int var15;
            for (var11 = 0; var11 < var1; ++var11) {
                var12 = this.pitch.a(var1);
                var13 = this.volume.a(var1);
                if (this.pitchModifier != null) {
                    var14 = this.pitchModifier.a(var1);
                    var15 = this.pitchAmplifier.a(var1);
                    var12 += this.q(var7, var15, this.pitchModifier.form) >> 1;
                    var7 = var7 + var6 + (var14 * var5 >> 16);
                }

                if (this.volumeMode != null) {
                    var14 = this.volumeMode.a(var1);
                    var15 = this.volumeModifier.a(var1);
                    var13 = var13 * ((this.q(var10, var15, this.volumeMode.form) >> 1) + '耀') >> 15;
                    var10 = var10 + var9 + (var14 * var8 >> 16);
                }

                for (var14 = 0; var14 < 5; ++var14) {
                    if (this.oscillatingVolumes[var14] != 0) {
                        var15 = r[var14] + var11;
                        if (var15 < var1) {
                            z[var15] += this.q(f[var14], var13 * y[var14] >> 15, this.pitch.form);
                            f[var14] += (var12 * h[var14] >> 16) + m[var14];
                        }
                    }
                }
            }

            int var16;
            if (this.gatingRelease != null) {
                this.gatingRelease.i();
                this.gatingAttack.i();
                var11 = 0;
                boolean var19 = false;
                boolean var20 = true;

                for (var14 = 0; var14 < var1; ++var14) {
                    var15 = this.gatingRelease.a(var1);
                    var16 = this.gatingAttack.a(var1);
                    if (var20) {
                        var12 = (var15 * (this.gatingRelease.end - this.gatingRelease.start) >> 8)
                                + this.gatingRelease.start;
                    } else {
                        var12 = (var16 * (this.gatingRelease.end - this.gatingRelease.start) >> 8)
                                + this.gatingRelease.start;
                    }

                    var11 += 256;
                    if (var11 >= var12) {
                        var11 = 0;
                        var20 = !var20;
                    }

                    if (var20) {
                        z[var14] = 0;
                    }
                }
            }

            if (this.delayTime > 0 && this.delayFeedback > 0) {
                var11 = (int) ((double) this.delayTime * var3);

                for (var12 = var11; var12 < var1; ++var12) {
                    z[var12] += z[var12 - var11] * this.delayFeedback / 100;
                }
            }

            if (this.soundFilter.t[0] > 0 || this.soundFilter.t[1] > 0) {
                this.filter.i();
                var11 = this.filter.a(var1 + 1);
                var12 = this.soundFilter.a(0, (float) var11 / 65536.0F);
                var13 = this.soundFilter.a(1, (float) var11 / 65536.0F);
                if (var1 >= var12 + var13) {
                    var14 = 0;
                    var15 = var13;
                    if (var13 > var1 - var12) {
                        var15 = var1 - var12;
                    }

                    int var17;
                    while (var14 < var15) {
                        var16 = (int) ((long) z[var14 + var12] * (long) dn.p >> 16);

                        for (var17 = 0; var17 < var12; ++var17) {
                            var16 += (int) ((long) z[var14 + var12 - 1 - var17] * (long) dn.e[0][var17] >> 16);
                        }

                        for (var17 = 0; var17 < var14; ++var17) {
                            var16 -= (int) ((long) z[var14 - 1 - var17] * (long) dn.e[1][var17] >> 16);
                        }

                        z[var14] = var16;
                        var11 = this.filter.a(var1 + 1);
                        ++var14;
                    }

                    var15 = 128;

                    while (true) {
                        if (var15 > var1 - var12) {
                            var15 = var1 - var12;
                        }

                        int var18;
                        while (var14 < var15) {
                            var17 = (int) ((long) z[var14 + var12] * (long) dn.p >> 16);

                            for (var18 = 0; var18 < var12; ++var18) {
                                var17 += (int) ((long) z[var14 + var12 - 1 - var18] * (long) dn.e[0][var18] >> 16);
                            }

                            for (var18 = 0; var18 < var13; ++var18) {
                                var17 -= (int) ((long) z[var14 - 1 - var18] * (long) dn.e[1][var18] >> 16);
                            }

                            z[var14] = var17;
                            var11 = this.filter.a(var1 + 1);
                            ++var14;
                        }

                        if (var14 >= var1 - var12) {
                            while (var14 < var1) {
                                var17 = 0;

                                for (var18 = var14 + var12 - var1; var18 < var12; ++var18) {
                                    var17 += (int) ((long) z[var14 + var12 - 1 - var18] * (long) dn.e[0][var18] >> 16);
                                }

                                for (var18 = 0; var18 < var13; ++var18) {
                                    var17 -= (int) ((long) z[var14 - 1 - var18] * (long) dn.e[1][var18] >> 16);
                                }

                                z[var14] = var17;
                                this.filter.a(var1 + 1);
                                ++var14;
                            }
                            break;
                        }

                        var12 = this.soundFilter.a(0, (float) var11 / 65536.0F);
                        var13 = this.soundFilter.a(1, (float) var11 / 65536.0F);
                        var15 += 128;
                    }
                }
            }

            for (var11 = 0; var11 < var1; ++var11) {
                if (z[var11] < -32768) {
                    z[var11] = -32768;
                }

                if (z[var11] > 32767) {
                    z[var11] = 32767;
                }
            }

            return z;
        }
    }

    @ObfuscatedName("q")
    final int q(int var1, int var2, int var3) {
        if (var3 == 1) {
            return (var1 & 32767) < 16384 ? var2 : -var2;
        } else if (var3 == 2) {
            return s[var1 & 32767] * var2 >> 14;
        } else if (var3 == 3) {
            return (var2 * (var1 & 32767) >> 14) - var2;
        } else {
            return var3 == 4 ? var2 * w[var1 / 2607 & 32767] : 0;
        }
    }

    @ObfuscatedName("i")
    final void i(ByteBuffer var1) {
        this.pitch = new AudioEnvelope();
        this.pitch.t(var1);
        this.volume = new AudioEnvelope();
        this.volume.t(var1);
        int var2 = var1.av();
        if (var2 != 0) {
            --var1.index;
            this.pitchModifier = new AudioEnvelope();
            this.pitchModifier.t(var1);
            this.pitchAmplifier = new AudioEnvelope();
            this.pitchAmplifier.t(var1);
        }

        var2 = var1.av();
        if (var2 != 0) {
            --var1.index;
            this.volumeMode = new AudioEnvelope();
            this.volumeMode.t(var1);
            this.volumeModifier = new AudioEnvelope();
            this.volumeModifier.t(var1);
        }

        var2 = var1.av();
        if (var2 != 0) {
            --var1.index;
            this.gatingRelease = new AudioEnvelope();
            this.gatingRelease.t(var1);
            this.gatingAttack = new AudioEnvelope();
            this.gatingAttack.t(var1);
        }

        for (int var3 = 0; var3 < 10; ++var3) {
            int var4 = var1.ag();
            if (var4 == 0) {
                break;
            }

            this.oscillatingVolumes[var3] = var4;
            this.oscillatingPitches[var3] = var1.at();
            this.oscillatingDelays[var3] = var1.ag();
        }

        this.delayTime = var1.ag();
        this.delayFeedback = var1.ag();
        this.duration = var1.ae();
        this.startTime = var1.ae();
        this.soundFilter = new dn();
        this.filter = new AudioEnvelope();
        this.soundFilter.l(var1, this.filter);
    }
}
