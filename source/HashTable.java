import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("hp")
public final class HashTable {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    Node[] buckets;

    @ObfuscatedName("i")
    Node tail;

    @ObfuscatedName("a")
    Node head;

    @ObfuscatedName("l")
    int l = 0;

    public HashTable(int var1) {
        this.t = var1;
        this.buckets = new Node[var1];

        for (int var2 = 0; var2 < var1; ++var2) {
            Node var3 = this.buckets[var2] = new Node();
            var3.next = var3;
            var3.previous = var3;
        }

    }

    @ObfuscatedName("t")
    public Node t(long var1) {
        Node var3 = this.buckets[(int) (var1 & (long) (this.t - 1))];

        for (this.tail = var3.next; var3 != this.tail; this.tail = this.tail.next) {
            if (this.tail.uid == var1) {
                Node var4 = this.tail;
                this.tail = this.tail.next;
                return var4;
            }
        }

        this.tail = null;
        return null;
    }

    @ObfuscatedName("q")
    public void q(Node var1, long var2) {
        if (var1.previous != null) {
            var1.kc();
        }

        Node var4 = this.buckets[(int) (var2 & (long) (this.t - 1))];
        var1.previous = var4.previous;
        var1.next = var4;
        var1.previous.next = var1;
        var1.next.previous = var1;
        var1.uid = var2;
    }

    @ObfuscatedName("i")
    void i() {
        for (int var1 = 0; var1 < this.t; ++var1) {
            Node var2 = this.buckets[var1];

            while (true) {
                Node var3 = var2.next;
                if (var3 == var2) {
                    break;
                }

                var3.kc();
            }
        }

        this.tail = null;
        this.head = null;
    }

    @ObfuscatedName("a")
    public Node a() {
        this.l = 0;
        return this.l();
    }

    @ObfuscatedName("l")
    public Node l() {
        Node var1;
        if (this.l > 0 && this.buckets[this.l - 1] != this.head) {
            var1 = this.head;
            this.head = var1.next;
            return var1;
        } else {
            do {
                if (this.l >= this.t) {
                    return null;
                }

                var1 = this.buckets[this.l++].next;
            } while (var1 == this.buckets[this.l - 1]);

            this.head = var1.next;
            return var1;
        }
    }
}
