import java.awt.FontMetrics;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gn")
public class gn {

    @ObfuscatedName("c")
    static int[] c;

    @ObfuscatedName("ae")
    static FontMetrics ae;

    @ObfuscatedName("t")
    public static void t(FileSystem var0) {
        InventoryDefinition.t = var0;
    }
}
