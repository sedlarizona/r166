import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("iw")
public class iw implements it {

    @ObfuscatedName("t")
    public static final iw t = new iw("", 10);

    @ObfuscatedName("q")
    public static final iw q = new iw("", 11);

    @ObfuscatedName("i")
    public static final iw i = new iw("", 12);

    @ObfuscatedName("a")
    public static final iw a = new iw("", 13);

    @ObfuscatedName("l")
    public static final iw l = new iw("", 14);

    @ObfuscatedName("b")
    public static final iw b;

    @ObfuscatedName("e")
    public static final iw e;

    @ObfuscatedName("x")
    public static final iw x;

    @ObfuscatedName("p")
    static final iw p;

    @ObfuscatedName("g")
    public static final iw g;

    @ObfuscatedName("n")
    public final int n;

    static {
        b = new iw("", 15, new e[] { e.t, e.t }, (e[]) null);
        e = new iw("", 16, new e[] { e.t, e.t }, (e[]) null);
        x = new iw("", 17, new e[] { e.t, e.t }, (e[]) null);
        p = new iw("", 73, true, true);
        g = new iw("", 76, true, false);
    }

    iw(String var1, int var2) {
        this(var1, var2, false, (e[]) null, false, (e[]) null);
    }

    iw(String var1, int var2, boolean var3, e[] var4, boolean var5, e[] var6) {
        this.n = var2;
    }

    iw(String var1, int var2, e[] var3, e[] var4) {
        this(var1, var2, var3 != null, var3, var4 != null, var4);
    }

    iw(String var1, int var2, boolean var3, boolean var4) {
        this(var1, var2, var3, (e[]) null, var4, (e[]) null);
    }

    @ObfuscatedName("t")
    public int t() {
        return this.n;
    }

    @ObfuscatedName("ie")
    static final String ie(int var0, int var1) {
        int var2 = var1 - var0;
        if (var2 < -9) {
            return ar.q(16711680);
        } else if (var2 < -6) {
            return ar.q(16723968);
        } else if (var2 < -3) {
            return ar.q(16740352);
        } else if (var2 < 0) {
            return ar.q(16756736);
        } else if (var2 > 9) {
            return ar.q(65280);
        } else if (var2 > 6) {
            return ar.q(4259584);
        } else if (var2 > 3) {
            return ar.q(8453888);
        } else {
            return var2 > 0 ? ar.q(12648192) : ar.q(16776960);
        }
    }
}
