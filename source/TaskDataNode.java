import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("do")
public abstract class TaskDataNode extends Node {

    @ObfuscatedName("j")
    TaskDataNode node;

    @ObfuscatedName("k")
    int taskUid;

    @ObfuscatedName("z")
    IntegerNode integerNode;

    @ObfuscatedName("w")
    volatile boolean hasArray = true;

    @ObfuscatedName("b")
    protected abstract TaskDataNode b();

    @ObfuscatedName("e")
    protected abstract TaskDataNode e();

    @ObfuscatedName("x")
    protected abstract int x();

    @ObfuscatedName("p")
    protected abstract void p(int[] var1, int var2, int var3);

    @ObfuscatedName("c")
    protected abstract void c(int var1);

    @ObfuscatedName("aw")
    int aw() {
        return 255;
    }

    @ObfuscatedName("er")
    final void er(int[] var1, int var2, int var3) {
        if (this.hasArray) {
            this.p(var1, var2, var3);
        } else {
            this.c(var3);
        }

    }
}
