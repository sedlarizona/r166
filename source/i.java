import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("i")
final class i implements t {

    @ObfuscatedName("pm")
    static int pm;

    @ObfuscatedName("t")
    public static FileSystem t;

    @ObfuscatedName("g")
    public static int g;

    @ObfuscatedName("mq")
    static RTComponent[] mq;

    @ObfuscatedName("t")
    static final FixedSizeDeque t(ByteBuffer var0, FixedSizeDeque var1) {
        int var2 = var0.av();
        int var3;
        if (var1 == null) {
            int var4 = var2 - 1;
            var4 |= var4 >>> 1;
            var4 |= var4 >>> 2;
            var4 |= var4 >>> 4;
            var4 |= var4 >>> 8;
            var4 |= var4 >>> 16;
            var3 = var4 + 1;
            var1 = new FixedSizeDeque(var3);
        }

        for (var3 = 0; var3 < var2; ++var3) {
            boolean var7 = var0.av() == 1;
            int var5 = var0.az();
            Object var6;
            if (var7) {
                var6 = new gy(var0.ar());
            } else {
                var6 = new hc(var0.ap());
            }

            var1.q((Node) var6, (long) var5);
        }

        return var1;
    }

    @ObfuscatedName("c")
    static final void c(String var0) {
        ak.p("Please remove " + var0 + " from your ignore list first");
    }

    @ObfuscatedName("jz")
    static final void jz() {
        for (int var0 = 0; var0 < cx.b; ++var0) {
            Player var1 = Client.loadedPlayers[cx.e[var0]];
            var1.b();
        }

    }
}
