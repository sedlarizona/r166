import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ie")
public class ie extends TaskDataNode {

    @ObfuscatedName("fn")
    static Sprite[] fn;

    @ObfuscatedName("t")
    AudioTask t;

    @ObfuscatedName("q")
    Deque q = new Deque();

    @ObfuscatedName("i")
    ca i = new ca();

    ie(AudioTask var1) {
        this.t = var1;
    }

    @ObfuscatedName("t")
    void t(hb var1, int[] var2, int var3, int var4, int var5) {
        if ((this.t.s[var1.t] & 4) != 0 && var1.u < 0) {
            int var6 = this.t.h[var1.t] / TaskData.l;

            while (true) {
                int var7 = (var6 + 1048575 - var1.f) / var6;
                if (var7 > var4) {
                    var1.f += var4 * var6;
                    break;
                }

                var1.w.p(var2, var3, var7);
                var3 += var7;
                var4 -= var7;
                var1.f += var6 * var7 - 1048576;
                int var8 = TaskData.l / 100;
                int var9 = 262144 / var6;
                if (var9 < var8) {
                    var8 = var9;
                }

                dy var10 = var1.w;
                if (this.t.r[var1.t] == 0) {
                    var1.w = dy.a(var1.i, var10.ai(), var10.w(), var10.s());
                } else {
                    var1.w = dy.a(var1.i, var10.ai(), 0, var10.s());
                    this.t.f(var1, var1.q.i[var1.b] < 0);
                    var1.w.y(var8, var10.w());
                }

                if (var1.q.i[var1.b] < 0) {
                    var1.w.o(-1);
                }

                var10.av(var8);
                var10.p(var2, var3, var5 - var3);
                if (var10.as()) {
                    this.i.t(var10);
                }
            }
        }

        var1.w.p(var2, var3, var4);
    }

    @ObfuscatedName("q")
    void q(hb var1, int var2) {
        if ((this.t.s[var1.t] & 4) != 0 && var1.u < 0) {
            int var3 = this.t.h[var1.t] / TaskData.l;
            int var4 = (var3 + 1048575 - var1.f) / var3;
            var1.f = var3 * var2 + var1.f & 1048575;
            if (var4 <= var2) {
                if (this.t.r[var1.t] == 0) {
                    var1.w = dy.a(var1.i, var1.w.ai(), var1.w.w(), var1.w.s());
                } else {
                    var1.w = dy.a(var1.i, var1.w.ai(), 0, var1.w.s());
                    this.t.f(var1, var1.q.i[var1.b] < 0);
                }

                if (var1.q.i[var1.b] < 0) {
                    var1.w.o(-1);
                }

                var2 = var1.f / var3;
            }
        }

        var1.w.c(var2);
    }

    @ObfuscatedName("b")
    protected TaskDataNode b() {
        hb var1 = (hb) this.q.e();
        if (var1 == null) {
            return null;
        } else {
            return (TaskDataNode) (var1.w != null ? var1.w : this.e());
        }
    }

    @ObfuscatedName("e")
    protected TaskDataNode e() {
        hb var1;
        do {
            var1 = (hb) this.q.p();
            if (var1 == null) {
                return null;
            }
        } while (var1.w == null);

        return var1.w;
    }

    @ObfuscatedName("x")
    protected int x() {
        return 0;
    }

    @ObfuscatedName("p")
    protected void p(int[] var1, int var2, int var3) {
        this.i.p(var1, var2, var3);

        for (hb var6 = (hb) this.q.e(); var6 != null; var6 = (hb) this.q.p()) {
            if (!this.t.br(var6)) {
                int var4 = var2;
                int var5 = var3;

                do {
                    if (var5 <= var6.s) {
                        this.t(var6, var1, var4, var5, var5 + var4);
                        var6.s -= var5;
                        break;
                    }

                    this.t(var6, var1, var4, var6.s, var4 + var5);
                    var4 += var6.s;
                    var5 -= var6.s;
                } while (!this.t.ba(var6, var1, var4, var5));
            }
        }

    }

    @ObfuscatedName("c")
    protected void c(int var1) {
        this.i.c(var1);

        for (hb var3 = (hb) this.q.e(); var3 != null; var3 = (hb) this.q.p()) {
            if (!this.t.br(var3)) {
                int var2 = var1;

                do {
                    if (var2 <= var3.s) {
                        this.q(var3, var2);
                        var3.s -= var2;
                        break;
                    }

                    this.q(var3, var3.s);
                    var2 -= var3.s;
                } while (!this.t.ba(var3, (int[]) null, 0, var2));
            }
        }

    }
}
