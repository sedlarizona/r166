import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gc")
public class gc {

    @ObfuscatedName("pk")
    static TaskData pk;

    @ObfuscatedName("t")
    static int t = 0;

    @ObfuscatedName("q")
    static int q = 0;

    @ObfuscatedName("i")
    static int i = 0;

    @ObfuscatedName("a")
    static byte[][] a = new byte[1000][];

    @ObfuscatedName("l")
    static byte[][] l = new byte[250][];

    @ObfuscatedName("b")
    static byte[][] b = new byte[50][];

    @ObfuscatedName("x")
    public static int[] x;

    @ObfuscatedName("p")
    public static byte[][][] p;

    @ObfuscatedName("t")
    static synchronized byte[] t(int var0, boolean var1) {
        byte[] var2;
        if (var0 != 100) {
            if (var0 < 100) {
                ;
            }
        } else if (t > 0) {
            var2 = a[--t];
            a[t] = null;
            return var2;
        }

        if (var0 != 5000) {
            if (var0 < 5000) {
                ;
            }
        } else if (q > 0) {
            var2 = l[--q];
            l[q] = null;
            return var2;
        }

        if (var0 != 30000) {
            if (var0 < 30000) {
                ;
            }
        } else if (i > 0) {
            var2 = b[--i];
            b[i] = null;
            return var2;
        }

        if (p != null) {
            for (int var4 = 0; var4 < CombatBarData.e.length; ++var4) {
                if (CombatBarData.e[var4] != var0) {
                    if (var0 < CombatBarData.e[var4]) {
                        ;
                    }
                } else if (x[var4] > 0) {
                    byte[] var3 = p[var4][--x[var4]];
                    p[var4][x[var4]] = null;
                    return var3;
                }
            }
        }

        return new byte[var0];
    }

    @ObfuscatedName("l")
    static void l(GameEngine var0) {
        int var14;
        if (ci.isWorldSelectorOpen) {
            if (bs.j == 1 || !er.ch && bs.j == 4) {
                int var1 = ci.worldCount + 280;
                if (bs.cameraZ >= var1 && bs.cameraZ <= var1 + 14 && bs.z >= 4 && bs.z <= 18) {
                    bl.q(0, 0);
                } else if (bs.cameraZ >= var1 + 15 && bs.cameraZ <= var1 + 80 && bs.z >= 4 && bs.z <= 18) {
                    bl.q(0, 1);
                } else {
                    int var2 = ci.worldCount + 390;
                    if (bs.cameraZ >= var2 && bs.cameraZ <= var2 + 14 && bs.z >= 4 && bs.z <= 18) {
                        bl.q(1, 0);
                    } else if (bs.cameraZ >= var2 + 15 && bs.cameraZ <= var2 + 80 && bs.z >= 4 && bs.z <= 18) {
                        bl.q(1, 1);
                    } else {
                        int var16 = ci.worldCount + 500;
                        if (bs.cameraZ >= var16 && bs.cameraZ <= var16 + 14 && bs.z >= 4 && bs.z <= 18) {
                            bl.q(2, 0);
                        } else if (bs.cameraZ >= var16 + 15 && bs.cameraZ <= var16 + 80 && bs.z >= 4 && bs.z <= 18) {
                            bl.q(2, 1);
                        } else {
                            var14 = ci.worldCount + 610;
                            if (bs.cameraZ >= var14 && bs.cameraZ <= var14 + 14 && bs.z >= 4 && bs.z <= 18) {
                                bl.q(3, 0);
                            } else if (bs.cameraZ >= var14 + 15 && bs.cameraZ <= var14 + 80 && bs.z >= 4 && bs.z <= 18) {
                                bl.q(3, 1);
                            } else if (bs.cameraZ >= ci.worldCount + 708 && bs.z >= 4
                                    && bs.cameraZ <= ci.worldCount + 708 + 50 && bs.z <= 20) {
                                ci.isWorldSelectorOpen = false;
                                ItemNode.b.o(ci.worldCount, 0);
                                iv.e.o(ci.worldCount + 382, 0);
                                ii.x.i(ci.worldCount + 382 - ii.x.i / 2, 18);
                            } else if (ci.bq != -1) {
                                Server var15 = Server.l[ci.bq];
                                aq.k(var15);
                                ci.isWorldSelectorOpen = false;
                                ItemNode.b.o(ci.worldCount, 0);
                                iv.e.o(ci.worldCount + 382, 0);
                                ii.x.i(ci.worldCount + 382 - ii.x.i / 2, 18);
                            }
                        }
                    }
                }
            }

        } else {
            if ((bs.j == 1 || !er.ch && bs.j == 4) && bs.cameraZ >= ci.worldCount + 765 - 50 && bs.z >= 453) {
                al.qp.loadingAudioDisabled = !al.qp.loadingAudioDisabled;
                ar.i();
                if (!al.qp.loadingAudioDisabled) {
                    ga.q(Varpbit.ca, "scape main", "", 255, false);
                } else {
                    RuneScriptStackItem.l();
                }
            }

            if (Client.packet != 5) {
                if (-1L == ci.bz) {
                    ci.bz = au.t() + 1000L;
                }

                long var12 = au.t();
                boolean var3;
                if (Client.rk != null && Client.rm < Client.rk.size()) {
                    while (true) {
                        if (Client.rm >= Client.rk.size()) {
                            var3 = true;
                            break;
                        }

                        bn var4 = (bn) Client.rk.get(Client.rm);
                        if (!var4.t()) {
                            var3 = false;
                            break;
                        }

                        ++Client.rm;
                    }
                } else {
                    var3 = true;
                }

                if (var3 && ci.bx == -1L) {
                    ci.bx = var12;
                    if (ci.bx > ci.bz) {
                        ci.bz = ci.bx;
                    }
                }

                ++ci.ap;
                if (Client.packet == 10 || Client.packet == 11) {
                    if (Client.bs == 0) {
                        if (bs.j == 1 || !er.ch && bs.j == 4) {
                            var14 = ci.worldCount + 5;
                            short var5 = 463;
                            byte var6 = 100;
                            byte var7 = 35;
                            if (bs.cameraZ >= var14 && bs.cameraZ <= var6 + var14 && bs.z >= var5
                                    && bs.z <= var7 + var5) {
                                if (ex.t()) {
                                    ci.isWorldSelectorOpen = true;
                                }

                                return;
                            }
                        }

                        if (Server.g != null && ex.t()) {
                            ci.isWorldSelectorOpen = true;
                        }
                    }

                    var14 = bs.j;
                    int var17 = bs.cameraZ;
                    int var18 = bs.z;
                    if (var14 == 0) {
                        var17 = bs.n;
                        var18 = bs.x;
                    }

                    if (!er.ch && var14 == 4) {
                        var14 = 1;
                    }

                    int var8;
                    short var9;
                    if (ci.loginState == 0) {
                        boolean var19 = false;

                        while (ko.l()) {
                            if (cr.cd == 84) {
                                var19 = true;
                            }
                        }

                        var8 = ik.w - 80;
                        var9 = 291;
                        if (var14 == 1 && var17 >= var8 - 75 && var17 <= var8 + 75 && var18 >= var9 - 20
                                && var18 <= var9 + 20) {
                            bs.q(ki.jv("secure", true) + "m=account-creation/g=oldscape/create_account_funnel.ws",
                                    true, false);
                        }

                        var8 = ik.w + 80;
                        if (var14 == 1 && var17 >= var8 - 75 && var17 <= var8 + 75 && var18 >= var9 - 20
                                && var18 <= var9 + 20 || var19) {
                            if ((Client.br & 33554432) != 0) {
                                ci.an = "";
                                ci.loginLine1 = "This is a <col=00ffff>Beta<col=ffffff> world.";
                                ci.loginLine2 = "Your normal account will not be affected.";
                                ci.loginLine3 = "";
                                ci.loginState = 1;
                                if (ci.aw && ci.username != null && ci.username.length() > 0) {
                                    ci.be = 1;
                                } else {
                                    ci.be = 0;
                                }
                            } else if ((Client.br & 4) != 0) {
                                if ((Client.br & 1024) != 0) {
                                    ci.loginLine1 = "This is a <col=ffff00>High Risk <col=ff0000>PvP<col=ffffff> world.";
                                    ci.loginLine2 = "Players can attack each other almost everywhere";
                                    ci.loginLine3 = "and the Protect Item prayer won't work.";
                                } else {
                                    ci.loginLine1 = "This is a <col=ff0000>PvP<col=ffffff> world.";
                                    ci.loginLine2 = "Players can attack each other";
                                    ci.loginLine3 = "almost everywhere.";
                                }

                                ci.an = "Warning!";
                                ci.loginState = 1;
                                if (ci.aw && ci.username != null && ci.username.length() > 0) {
                                    ci.be = 1;
                                } else {
                                    ci.be = 0;
                                }
                            } else if ((Client.br & 1024) != 0) {
                                ci.loginLine1 = "This is a <col=ffff00>High Risk<col=ffffff> world.";
                                ci.loginLine2 = "The Protect Item prayer will";
                                ci.loginLine3 = "not work on this world.";
                                ci.an = "Warning!";
                                ci.loginState = 1;
                                if (ci.aw && ci.username != null && ci.username.length() > 0) {
                                    ci.be = 1;
                                } else {
                                    ci.be = 0;
                                }
                            } else {
                                af.b(false);
                            }
                        }
                    } else {
                        int var20;
                        short var22;
                        if (ci.loginState != 1) {
                            short var21;
                            if (ci.loginState == 2) {
                                var21 = 201;
                                var20 = var21 + 52;
                                if (var14 == 1 && var18 >= var20 - 12 && var18 < var20 + 2) {
                                    ci.be = 0;
                                }

                                var20 += 15;
                                if (var14 == 1 && var18 >= var20 - 12 && var18 < var20 + 2) {
                                    ci.be = 1;
                                }

                                var20 += 15;
                                var21 = 361;
                                if (var14 == 1 && var18 >= var21 - 15 && var18 < var21) {
                                    cx.p("Please enter your username.", "If you created your account after November",
                                            "2010, this will be the creation email address.");
                                    ci.loginState = 5;
                                    return;
                                }

                                var8 = ik.w - 80;
                                var9 = 321;
                                if (var14 == 1 && var17 >= var8 - 75 && var17 <= var8 + 75 && var18 >= var9 - 20
                                        && var18 <= var9 + 20) {
                                    ci.username = ci.username.trim();
                                    if (ci.username.length() == 0) {
                                        cx.p("", "Please enter your username/email address.", "");
                                        return;
                                    }

                                    if (ci.password.length() == 0) {
                                        cx.p("", "Please enter your password.", "");
                                        return;
                                    }

                                    cx.p("", "Connecting to server...", "");
                                    Player.fl(false);
                                    d.ea(20);
                                    return;
                                }

                                var8 = ci.z + 180 + 80;
                                if (var14 == 1 && var17 >= var8 - 75 && var17 <= var8 + 75 && var18 >= var9 - 20
                                        && var18 <= var9 + 20) {
                                    ci.loginState = 0;
                                    ci.username = "";
                                    ci.password = "";
                                    AreaSoundEmitter.br = 0;
                                    ik.ba = "";
                                    ci.bk = true;
                                }

                                var8 = ik.w + -117;
                                var9 = 277;
                                ci.ad = var17 >= var8 && var17 < var8 + x.ab && var18 >= var9 && var18 < var9 + jl.ac;
                                if (var14 == 1 && ci.ad) {
                                    ci.aw = !ci.aw;
                                    if (!ci.aw && al.qp.b != null) {
                                        al.qp.b = null;
                                        ar.i();
                                    }
                                }

                                var8 = ik.w + 24;
                                var9 = 277;
                                ci.bg = var17 >= var8 && var17 < var8 + x.ab && var18 >= var9 && var18 < var9 + jl.ac;
                                if (var14 == 1 && ci.bg) {
                                    al.qp.e = !al.qp.e;
                                    if (!al.qp.e) {
                                        ci.username = "";
                                        al.qp.b = null;
                                        if (ci.aw && ci.username != null && ci.username.length() > 0) {
                                            ci.be = 1;
                                        } else {
                                            ci.be = 0;
                                        }
                                    }

                                    ar.i();
                                }

                                while (true) {
                                    while (ko.l()) {
                                        boolean var10 = false;

                                        for (int var11 = 0; var11 < "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| "
                                                .length(); ++var11) {
                                            if (h.cv == "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| "
                                                    .charAt(var11)) {
                                                var10 = true;
                                                break;
                                            }
                                        }

                                        if (cr.cd == 13) {
                                            ci.loginState = 0;
                                            ci.username = "";
                                            ci.password = "";
                                            AreaSoundEmitter.br = 0;
                                            ik.ba = "";
                                            ci.bk = true;
                                        } else if (ci.be == 0) {
                                            if (cr.cd == 85 && ci.username.length() > 0) {
                                                ci.username = ci.username.substring(0, ci.username.length() - 1);
                                            }

                                            if (cr.cd == 84 || cr.cd == 80) {
                                                ci.be = 1;
                                            }

                                            if (var10 && ci.username.length() < 320) {
                                                ci.username = ci.username + h.cv;
                                            }
                                        } else if (ci.be == 1) {
                                            if (cr.cd == 85 && ci.password.length() > 0) {
                                                ci.password = ci.password.substring(0, ci.password.length() - 1);
                                            }

                                            if (cr.cd == 84 || cr.cd == 80) {
                                                ci.be = 0;
                                            }

                                            if (cr.cd == 84) {
                                                ci.username = ci.username.trim();
                                                if (ci.username.length() == 0) {
                                                    cx.p("", "Please enter your username/email address.", "");
                                                    return;
                                                }

                                                if (ci.password.length() == 0) {
                                                    cx.p("", "Please enter your password.", "");
                                                    return;
                                                }

                                                cx.p("", "Connecting to server...", "");
                                                Player.fl(false);
                                                d.ea(20);
                                                return;
                                            }

                                            if (var10 && ci.password.length() < 20) {
                                                ci.password = ci.password + h.cv;
                                            }
                                        }
                                    }

                                    return;
                                }
                            } else if (ci.loginState == 3) {
                                var20 = ci.z + 180;
                                var22 = 276;
                                if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                        && var18 <= var22 + 20) {
                                    af.b(false);
                                }

                                var20 = ci.z + 180;
                                var22 = 326;
                                if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                        && var18 <= var22 + 20) {
                                    cx.p("Please enter your username.", "If you created your account after November",
                                            "2010, this will be the creation email address.");
                                    ci.loginState = 5;
                                    return;
                                }
                            } else {
                                boolean var23;
                                int var24;
                                if (ci.loginState == 4) {
                                    var20 = ci.z + 180 - 80;
                                    var22 = 321;
                                    if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                            && var18 <= var22 + 20) {
                                        ik.ba.trim();
                                        if (ik.ba.length() != 6) {
                                            cx.p("", "Please enter a 6-digit PIN.", "");
                                            return;
                                        }

                                        AreaSoundEmitter.br = Integer.parseInt(ik.ba);
                                        ik.ba = "";
                                        Player.fl(true);
                                        cx.p("", "Connecting to server...", "");
                                        d.ea(20);
                                        return;
                                    }

                                    if (var14 == 1 && var17 >= ci.z + 180 - 9 && var17 <= ci.z + 180 + 130
                                            && var18 >= 263 && var18 <= 296) {
                                        ci.bk = !ci.bk;
                                    }

                                    if (var14 == 1 && var17 >= ci.z + 180 - 34 && var17 <= ci.z + 34 + 180
                                            && var18 >= 351 && var18 <= 363) {
                                        bs.q(ki.jv("secure", true) + "m=totp-authenticator/disableTOTPRequest", true,
                                                false);
                                    }

                                    var20 = ci.z + 180 + 80;
                                    if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                            && var18 <= var22 + 20) {
                                        ci.loginState = 0;
                                        ci.username = "";
                                        ci.password = "";
                                        AreaSoundEmitter.br = 0;
                                        ik.ba = "";
                                    }

                                    while (ko.l()) {
                                        var23 = false;

                                        for (var24 = 0; var24 < "1234567890".length(); ++var24) {
                                            if (h.cv == "1234567890".charAt(var24)) {
                                                var23 = true;
                                                break;
                                            }
                                        }

                                        if (cr.cd == 13) {
                                            ci.loginState = 0;
                                            ci.username = "";
                                            ci.password = "";
                                            AreaSoundEmitter.br = 0;
                                            ik.ba = "";
                                        } else {
                                            if (cr.cd == 85 && ik.ba.length() > 0) {
                                                ik.ba = ik.ba.substring(0, ik.ba.length() - 1);
                                            }

                                            if (cr.cd == 84) {
                                                ik.ba.trim();
                                                if (ik.ba.length() != 6) {
                                                    cx.p("", "Please enter a 6-digit PIN.", "");
                                                    return;
                                                }

                                                AreaSoundEmitter.br = Integer.parseInt(ik.ba);
                                                ik.ba = "";
                                                Player.fl(true);
                                                cx.p("", "Connecting to server...", "");
                                                d.ea(20);
                                                return;
                                            }

                                            if (var23 && ik.ba.length() < 6) {
                                                ik.ba = ik.ba + h.cv;
                                            }
                                        }
                                    }
                                } else if (ci.loginState == 5) {
                                    var20 = ci.z + 180 - 80;
                                    var22 = 321;
                                    if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                            && var18 <= var22 + 20) {
                                        FloorObject.e();
                                        return;
                                    }

                                    var20 = ci.z + 180 + 80;
                                    if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                            && var18 <= var22 + 20) {
                                        af.b(true);
                                    }

                                    while (ko.l()) {
                                        var23 = false;

                                        for (var24 = 0; var24 < "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| "
                                                .length(); ++var24) {
                                            if (h.cv == "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| "
                                                    .charAt(var24)) {
                                                var23 = true;
                                                break;
                                            }
                                        }

                                        if (cr.cd == 13) {
                                            af.b(true);
                                        } else {
                                            if (cr.cd == 85 && ci.username.length() > 0) {
                                                ci.username = ci.username.substring(0, ci.username.length() - 1);
                                            }

                                            if (cr.cd == 84) {
                                                FloorObject.e();
                                                return;
                                            }

                                            if (var23 && ci.username.length() < 320) {
                                                ci.username = ci.username + h.cv;
                                            }
                                        }
                                    }
                                } else if (ci.loginState == 6) {
                                    while (true) {
                                        do {
                                            if (!ko.l()) {
                                                var21 = 321;
                                                if (var14 == 1 && var18 >= var21 - 20 && var18 <= var21 + 20) {
                                                    af.b(true);
                                                }

                                                return;
                                            }
                                        } while (cr.cd != 84 && cr.cd != 13);

                                        af.b(true);
                                    }
                                }
                            }
                        } else {
                            while (ko.l()) {
                                if (cr.cd == 84) {
                                    af.b(false);
                                } else if (cr.cd == 13) {
                                    ci.loginState = 0;
                                }
                            }

                            var20 = ik.w - 80;
                            var22 = 321;
                            if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                    && var18 <= var22 + 20) {
                                af.b(false);
                            }

                            var20 = ik.w + 80;
                            if (var14 == 1 && var17 >= var20 - 75 && var17 <= var20 + 75 && var18 >= var22 - 20
                                    && var18 <= var22 + 20) {
                                ci.loginState = 0;
                            }
                        }
                    }

                }
            }
        }
    }

    @ObfuscatedName("jh")
    static void jh(String var0, boolean var1) {
        var0 = var0.toLowerCase();
        short[] var2 = new short[16];
        int var3 = 0;

        for (int var4 = 0; var4 < GrandExchangeOffer.p; ++var4) {
            ItemDefinition var5 = cs.loadItemDefinition(var4);
            if ((!var1 || var5.be) && var5.ak == -1 && var5.name.toLowerCase().indexOf(var0) != -1) {
                if (var3 >= 250) {
                    dh.ra = -1;
                    ab.rj = null;
                    return;
                }

                if (var3 >= var2.length) {
                    short[] var6 = new short[var2.length * 2];

                    for (int var7 = 0; var7 < var3; ++var7) {
                        var6[var7] = var2[var7];
                    }

                    var2 = var6;
                }

                var2[var3++] = (short) var4;
            }
        }

        ab.rj = var2;
        n.rd = 0;
        dh.ra = var3;
        String[] var8 = new String[dh.ra];

        for (int var9 = 0; var9 < dh.ra; ++var9) {
            var8[var9] = cs.loadItemDefinition(var2[var9]).name;
        }

        short[] var10 = ab.rj;
        Npc.t(var8, var10, 0, var8.length - 1);
    }
}
