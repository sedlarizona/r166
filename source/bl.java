import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("bl")
public final class bl extends Node {

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    int e;

    @ObfuscatedName("x")
    int x;

    @ObfuscatedName("p")
    int p;

    @ObfuscatedName("g")
    int g;

    @ObfuscatedName("n")
    int n = 0;

    @ObfuscatedName("o")
    int o = -1;

    @ObfuscatedName("t")
    public static jh t(int var0) {
        jh var1 = (jh) jh.a.t((long) var0);
        if (var1 != null) {
            return var1;
        } else {
            byte[] var2 = jh.t.i(32, var0);
            var1 = new jh();
            if (var2 != null) {
                var1.q(new ByteBuffer(var2));
            }

            jh.a.i(var1, (long) var0);
            return var1;
        }
    }

    @ObfuscatedName("t")
    public static void t(FileSystem var0) {
        jv.t = var0;
    }

    @ObfuscatedName("q")
    static void q(int var0, int var1) {
        int[] var2 = new int[4];
        int[] var3 = new int[4];
        var2[0] = var0;
        var3[0] = var1;
        int var4 = 1;

        for (int var5 = 0; var5 < 4; ++var5) {
            if (Server.p[var5] != var0) {
                var2[var4] = Server.p[var5];
                var3[var4] = Server.x[var5];
                ++var4;
            }
        }

        Server.p = var2;
        Server.x = var3;
        ItemPile.i(Server.l, 0, Server.l.length - 1, Server.p, Server.x);
    }
}
