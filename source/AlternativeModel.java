import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("di")
public class AlternativeModel extends Renderable {

    @ObfuscatedName("ag")
    static int[] ag = new int[10000];

    @ObfuscatedName("as")
    static int[] as = new int[10000];

    @ObfuscatedName("aw")
    static int aw = 0;

    @ObfuscatedName("aq")
    static int[] aq;

    @ObfuscatedName("aa")
    static int[] aa;

    @ObfuscatedName("t")
    int t = 0;

    @ObfuscatedName("q")
    int[] xVertices;

    @ObfuscatedName("i")
    int[] yVertices;

    @ObfuscatedName("a")
    int[] zVertices;

    @ObfuscatedName("l")
    int l = 0;

    @ObfuscatedName("b")
    int[] b;

    @ObfuscatedName("e")
    int[] e;

    @ObfuscatedName("x")
    int[] x;

    @ObfuscatedName("p")
    byte[] p;

    @ObfuscatedName("g")
    byte[] g;

    @ObfuscatedName("n")
    byte[] n;

    @ObfuscatedName("o")
    byte[] o;

    @ObfuscatedName("c")
    short[] c;

    @ObfuscatedName("v")
    short[] v;

    @ObfuscatedName("u")
    byte u = 0;

    @ObfuscatedName("j")
    int j;

    @ObfuscatedName("k")
    byte[] k;

    @ObfuscatedName("z")
    short[] z;

    @ObfuscatedName("w")
    short[] w;

    @ObfuscatedName("s")
    short[] s;

    @ObfuscatedName("d")
    short[] d;

    @ObfuscatedName("f")
    short[] f;

    @ObfuscatedName("r")
    short[] r;

    @ObfuscatedName("y")
    short[] y;

    @ObfuscatedName("h")
    short[] h;

    @ObfuscatedName("m")
    short[] m;

    @ObfuscatedName("ay")
    byte[] ay;

    @ObfuscatedName("ao")
    int[] ao;

    @ObfuscatedName("av")
    int[] av;

    @ObfuscatedName("aj")
    int[][] aj;

    @ObfuscatedName("ae")
    int[][] ae;

    @ObfuscatedName("am")
    ez[] am;

    @ObfuscatedName("az")
    ee[] az;

    @ObfuscatedName("ap")
    ee[] ap;

    @ObfuscatedName("ah")
    public short ah;

    @ObfuscatedName("au")
    public short au;

    @ObfuscatedName("ax")
    boolean ax = false;

    @ObfuscatedName("ar")
    int ar;

    @ObfuscatedName("an")
    int an;

    @ObfuscatedName("ai")
    int ai;

    @ObfuscatedName("al")
    int al;

    @ObfuscatedName("at")
    int at;

    static {
        aq = eu.m;
        aa = eu.ay;
    }

    AlternativeModel() {
    }

    public AlternativeModel(AlternativeModel[] var1, int var2) {
        boolean var3 = false;
        boolean var4 = false;
        boolean var5 = false;
        boolean var6 = false;
        boolean var7 = false;
        boolean var8 = false;
        this.t = 0;
        this.l = 0;
        this.j = 0;
        this.u = -1;

        int var9;
        AlternativeModel var10;
        for (var9 = 0; var9 < var2; ++var9) {
            var10 = var1[var9];
            if (var10 != null) {
                this.t += var10.t;
                this.l += var10.l;
                this.j += var10.j;
                if (var10.g != null) {
                    var4 = true;
                } else {
                    if (this.u == -1) {
                        this.u = var10.u;
                    }

                    if (this.u != var10.u) {
                        var4 = true;
                    }
                }

                var3 |= var10.p != null;
                var5 |= var10.n != null;
                var6 |= var10.av != null;
                var7 |= var10.v != null;
                var8 |= var10.o != null;
            }
        }

        this.xVertices = new int[this.t];
        this.yVertices = new int[this.t];
        this.zVertices = new int[this.t];
        this.ao = new int[this.t];
        this.b = new int[this.l];
        this.e = new int[this.l];
        this.x = new int[this.l];
        if (var3) {
            this.p = new byte[this.l];
        }

        if (var4) {
            this.g = new byte[this.l];
        }

        if (var5) {
            this.n = new byte[this.l];
        }

        if (var6) {
            this.av = new int[this.l];
        }

        if (var7) {
            this.v = new short[this.l];
        }

        if (var8) {
            this.o = new byte[this.l];
        }

        this.c = new short[this.l];
        if (this.j > 0) {
            this.k = new byte[this.j];
            this.z = new short[this.j];
            this.w = new short[this.j];
            this.s = new short[this.j];
            this.d = new short[this.j];
            this.f = new short[this.j];
            this.r = new short[this.j];
            this.y = new short[this.j];
            this.ay = new byte[this.j];
            this.h = new short[this.j];
            this.m = new short[this.j];
        }

        this.t = 0;
        this.l = 0;
        this.j = 0;

        for (var9 = 0; var9 < var2; ++var9) {
            var10 = var1[var9];
            if (var10 != null) {
                int var11;
                for (var11 = 0; var11 < var10.l; ++var11) {
                    if (var3 && var10.p != null) {
                        this.p[this.l] = var10.p[var11];
                    }

                    if (var4) {
                        if (var10.g != null) {
                            this.g[this.l] = var10.g[var11];
                        } else {
                            this.g[this.l] = var10.u;
                        }
                    }

                    if (var5 && var10.n != null) {
                        this.n[this.l] = var10.n[var11];
                    }

                    if (var6 && var10.av != null) {
                        this.av[this.l] = var10.av[var11];
                    }

                    if (var7) {
                        if (var10.v != null) {
                            this.v[this.l] = var10.v[var11];
                        } else {
                            this.v[this.l] = -1;
                        }
                    }

                    if (var8) {
                        if (var10.o != null && var10.o[var11] != -1) {
                            this.o[this.l] = (byte) (this.j + var10.o[var11]);
                        } else {
                            this.o[this.l] = -1;
                        }
                    }

                    this.c[this.l] = var10.c[var11];
                    this.b[this.l] = this.a(var10, var10.b[var11]);
                    this.e[this.l] = this.a(var10, var10.e[var11]);
                    this.x[this.l] = this.a(var10, var10.x[var11]);
                    ++this.l;
                }

                for (var11 = 0; var11 < var10.j; ++var11) {
                    byte var12 = this.k[this.j] = var10.k[var11];
                    if (var12 == 0) {
                        this.z[this.j] = (short) this.a(var10, var10.z[var11]);
                        this.w[this.j] = (short) this.a(var10, var10.w[var11]);
                        this.s[this.j] = (short) this.a(var10, var10.s[var11]);
                    }

                    if (var12 >= 1 && var12 <= 3) {
                        this.z[this.j] = var10.z[var11];
                        this.w[this.j] = var10.w[var11];
                        this.s[this.j] = var10.s[var11];
                        this.d[this.j] = var10.d[var11];
                        this.f[this.j] = var10.f[var11];
                        this.r[this.j] = var10.r[var11];
                        this.y[this.j] = var10.y[var11];
                        this.ay[this.j] = var10.ay[var11];
                        this.h[this.j] = var10.h[var11];
                    }

                    if (var12 == 2) {
                        this.m[this.j] = var10.m[var11];
                    }

                    ++this.j;
                }
            }
        }

    }

    AlternativeModel(byte[] var1) {
        if (var1[var1.length - 1] == -1 && var1[var1.length - 2] == -1) {
            this.q(var1);
        } else {
            this.i(var1);
        }

    }

    public AlternativeModel(AlternativeModel var1, boolean var2, boolean var3, boolean var4, boolean var5) {
        this.t = var1.t;
        this.l = var1.l;
        this.j = var1.j;
        int var6;
        if (var2) {
            this.xVertices = var1.xVertices;
            this.yVertices = var1.yVertices;
            this.zVertices = var1.zVertices;
        } else {
            this.xVertices = new int[this.t];
            this.yVertices = new int[this.t];
            this.zVertices = new int[this.t];

            for (var6 = 0; var6 < this.t; ++var6) {
                this.xVertices[var6] = var1.xVertices[var6];
                this.yVertices[var6] = var1.yVertices[var6];
                this.zVertices[var6] = var1.zVertices[var6];
            }
        }

        if (var3) {
            this.c = var1.c;
        } else {
            this.c = new short[this.l];

            for (var6 = 0; var6 < this.l; ++var6) {
                this.c[var6] = var1.c[var6];
            }
        }

        if (!var4 && var1.v != null) {
            this.v = new short[this.l];

            for (var6 = 0; var6 < this.l; ++var6) {
                this.v[var6] = var1.v[var6];
            }
        } else {
            this.v = var1.v;
        }

        this.n = var1.n;
        this.b = var1.b;
        this.e = var1.e;
        this.x = var1.x;
        this.p = var1.p;
        this.g = var1.g;
        this.o = var1.o;
        this.u = var1.u;
        this.k = var1.k;
        this.z = var1.z;
        this.w = var1.w;
        this.s = var1.s;
        this.d = var1.d;
        this.f = var1.f;
        this.r = var1.r;
        this.y = var1.y;
        this.ay = var1.ay;
        this.h = var1.h;
        this.m = var1.m;
        this.ao = var1.ao;
        this.av = var1.av;
        this.aj = var1.aj;
        this.ae = var1.ae;
        this.az = var1.az;
        this.am = var1.am;
        this.ap = var1.ap;
        this.ah = var1.ah;
        this.au = var1.au;
    }

    @ObfuscatedName("q")
    void q(byte[] var1) {
        ByteBuffer var2 = new ByteBuffer(var1);
        ByteBuffer var3 = new ByteBuffer(var1);
        ByteBuffer var4 = new ByteBuffer(var1);
        ByteBuffer var5 = new ByteBuffer(var1);
        ByteBuffer var6 = new ByteBuffer(var1);
        ByteBuffer var7 = new ByteBuffer(var1);
        ByteBuffer var8 = new ByteBuffer(var1);
        var2.index = var1.length - 23;
        int var9 = var2.ae();
        int var10 = var2.ae();
        int var11 = var2.av();
        int var12 = var2.av();
        int var13 = var2.av();
        int var14 = var2.av();
        int var15 = var2.av();
        int var16 = var2.av();
        int var17 = var2.av();
        int var18 = var2.ae();
        int var19 = var2.ae();
        int var20 = var2.ae();
        int var21 = var2.ae();
        int var22 = var2.ae();
        int var23 = 0;
        int var24 = 0;
        int var25 = 0;
        int var26;
        if (var11 > 0) {
            this.k = new byte[var11];
            var2.index = 0;

            for (var26 = 0; var26 < var11; ++var26) {
                byte var27 = this.k[var26] = var2.aj();
                if (var27 == 0) {
                    ++var23;
                }

                if (var27 >= 1 && var27 <= 3) {
                    ++var24;
                }

                if (var27 == 2) {
                    ++var25;
                }
            }
        }

        var26 = var11 + var9;
        int var28 = var26;
        if (var12 == 1) {
            var26 += var10;
        }

        int var29 = var26;
        var26 += var10;
        int var30 = var26;
        if (var13 == 255) {
            var26 += var10;
        }

        int var31 = var26;
        if (var15 == 1) {
            var26 += var10;
        }

        int var32 = var26;
        if (var17 == 1) {
            var26 += var9;
        }

        int var33 = var26;
        if (var14 == 1) {
            var26 += var10;
        }

        int var34 = var26;
        var26 += var21;
        int var35 = var26;
        if (var16 == 1) {
            var26 += var10 * 2;
        }

        int var36 = var26;
        var26 += var22;
        int var37 = var26;
        var26 += var10 * 2;
        int var38 = var26;
        var26 += var18;
        int var39 = var26;
        var26 += var19;
        int var40 = var26;
        var26 += var20;
        int var41 = var26;
        var26 += var23 * 6;
        int var42 = var26;
        var26 += var24 * 6;
        int var43 = var26;
        var26 += var24 * 6;
        int var44 = var26;
        var26 += var24 * 2;
        int var45 = var26;
        var26 += var24;
        int var46 = var26;
        var26 += var24 * 2 + var25 * 2;
        this.t = var9;
        this.l = var10;
        this.j = var11;
        this.xVertices = new int[var9];
        this.yVertices = new int[var9];
        this.zVertices = new int[var9];
        this.b = new int[var10];
        this.e = new int[var10];
        this.x = new int[var10];
        if (var17 == 1) {
            this.ao = new int[var9];
        }

        if (var12 == 1) {
            this.p = new byte[var10];
        }

        if (var13 == 255) {
            this.g = new byte[var10];
        } else {
            this.u = (byte) var13;
        }

        if (var14 == 1) {
            this.n = new byte[var10];
        }

        if (var15 == 1) {
            this.av = new int[var10];
        }

        if (var16 == 1) {
            this.v = new short[var10];
        }

        if (var16 == 1 && var11 > 0) {
            this.o = new byte[var10];
        }

        this.c = new short[var10];
        if (var11 > 0) {
            this.z = new short[var11];
            this.w = new short[var11];
            this.s = new short[var11];
            if (var24 > 0) {
                this.d = new short[var24];
                this.f = new short[var24];
                this.r = new short[var24];
                this.y = new short[var24];
                this.ay = new byte[var24];
                this.h = new short[var24];
            }

            if (var25 > 0) {
                this.m = new short[var25];
            }
        }

        var2.index = var11;
        var3.index = var38;
        var4.index = var39;
        var5.index = var40;
        var6.index = var32;
        int var48 = 0;
        int var49 = 0;
        int var50 = 0;

        int var51;
        int var52;
        int var53;
        int var54;
        int var55;
        for (var51 = 0; var51 < var9; ++var51) {
            var52 = var2.av();
            var53 = 0;
            if ((var52 & 1) != 0) {
                var53 = var3.at();
            }

            var54 = 0;
            if ((var52 & 2) != 0) {
                var54 = var4.at();
            }

            var55 = 0;
            if ((var52 & 4) != 0) {
                var55 = var5.at();
            }

            this.xVertices[var51] = var48 + var53;
            this.yVertices[var51] = var49 + var54;
            this.zVertices[var51] = var50 + var55;
            var48 = this.xVertices[var51];
            var49 = this.yVertices[var51];
            var50 = this.zVertices[var51];
            if (var17 == 1) {
                this.ao[var51] = var6.av();
            }
        }

        var2.index = var37;
        var3.index = var28;
        var4.index = var30;
        var5.index = var33;
        var6.index = var31;
        var7.index = var35;
        var8.index = var36;

        for (var51 = 0; var51 < var10; ++var51) {
            this.c[var51] = (short) var2.ae();
            if (var12 == 1) {
                this.p[var51] = var3.aj();
            }

            if (var13 == 255) {
                this.g[var51] = var4.aj();
            }

            if (var14 == 1) {
                this.n[var51] = var5.aj();
            }

            if (var15 == 1) {
                this.av[var51] = var6.av();
            }

            if (var16 == 1) {
                this.v[var51] = (short) (var7.ae() - 1);
            }

            if (this.o != null && this.v[var51] != -1) {
                this.o[var51] = (byte) (var8.av() - 1);
            }
        }

        var2.index = var34;
        var3.index = var29;
        var51 = 0;
        var52 = 0;
        var53 = 0;
        var54 = 0;

        int var56;
        for (var55 = 0; var55 < var10; ++var55) {
            var56 = var3.av();
            if (var56 == 1) {
                var51 = var2.at() + var54;
                var52 = var2.at() + var51;
                var53 = var2.at() + var52;
                var54 = var53;
                this.b[var55] = var51;
                this.e[var55] = var52;
                this.x[var55] = var53;
            }

            if (var56 == 2) {
                var52 = var53;
                var53 = var2.at() + var54;
                var54 = var53;
                this.b[var55] = var51;
                this.e[var55] = var52;
                this.x[var55] = var53;
            }

            if (var56 == 3) {
                var51 = var53;
                var53 = var2.at() + var54;
                var54 = var53;
                this.b[var55] = var51;
                this.e[var55] = var52;
                this.x[var55] = var53;
            }

            if (var56 == 4) {
                int var57 = var51;
                var51 = var52;
                var52 = var57;
                var53 = var2.at() + var54;
                var54 = var53;
                this.b[var55] = var51;
                this.e[var55] = var57;
                this.x[var55] = var53;
            }
        }

        var2.index = var41;
        var3.index = var42;
        var4.index = var43;
        var5.index = var44;
        var6.index = var45;
        var7.index = var46;

        for (var55 = 0; var55 < var11; ++var55) {
            var56 = this.k[var55] & 255;
            if (var56 == 0) {
                this.z[var55] = (short) var2.ae();
                this.w[var55] = (short) var2.ae();
                this.s[var55] = (short) var2.ae();
            }

            if (var56 == 1) {
                this.z[var55] = (short) var3.ae();
                this.w[var55] = (short) var3.ae();
                this.s[var55] = (short) var3.ae();
                this.d[var55] = (short) var4.ae();
                this.f[var55] = (short) var4.ae();
                this.r[var55] = (short) var4.ae();
                this.y[var55] = (short) var5.ae();
                this.ay[var55] = var6.aj();
                this.h[var55] = (short) var7.ae();
            }

            if (var56 == 2) {
                this.z[var55] = (short) var3.ae();
                this.w[var55] = (short) var3.ae();
                this.s[var55] = (short) var3.ae();
                this.d[var55] = (short) var4.ae();
                this.f[var55] = (short) var4.ae();
                this.r[var55] = (short) var4.ae();
                this.y[var55] = (short) var5.ae();
                this.ay[var55] = var6.aj();
                this.h[var55] = (short) var7.ae();
                this.m[var55] = (short) var7.ae();
            }

            if (var56 == 3) {
                this.z[var55] = (short) var3.ae();
                this.w[var55] = (short) var3.ae();
                this.s[var55] = (short) var3.ae();
                this.d[var55] = (short) var4.ae();
                this.f[var55] = (short) var4.ae();
                this.r[var55] = (short) var4.ae();
                this.y[var55] = (short) var5.ae();
                this.ay[var55] = var6.aj();
                this.h[var55] = (short) var7.ae();
            }
        }

        var2.index = var26;
        var55 = var2.av();
        if (var55 != 0) {
            new ey();
            var2.ae();
            var2.ae();
            var2.ae();
            var2.ap();
        }

    }

    @ObfuscatedName("i")
    void i(byte[] var1) {
        boolean var2 = false;
        boolean var3 = false;
        ByteBuffer var4 = new ByteBuffer(var1);
        ByteBuffer var5 = new ByteBuffer(var1);
        ByteBuffer var6 = new ByteBuffer(var1);
        ByteBuffer var7 = new ByteBuffer(var1);
        ByteBuffer var8 = new ByteBuffer(var1);
        var4.index = var1.length - 18;
        int var9 = var4.ae();
        int var10 = var4.ae();
        int var11 = var4.av();
        int var12 = var4.av();
        int var13 = var4.av();
        int var14 = var4.av();
        int var15 = var4.av();
        int var16 = var4.av();
        int var17 = var4.ae();
        int var18 = var4.ae();
        int var19 = var4.ae();
        int var20 = var4.ae();
        byte var21 = 0;
        int var45 = var21 + var9;
        int var23 = var45;
        var45 += var10;
        int var24 = var45;
        if (var13 == 255) {
            var45 += var10;
        }

        int var25 = var45;
        if (var15 == 1) {
            var45 += var10;
        }

        int var26 = var45;
        if (var12 == 1) {
            var45 += var10;
        }

        int var27 = var45;
        if (var16 == 1) {
            var45 += var9;
        }

        int var28 = var45;
        if (var14 == 1) {
            var45 += var10;
        }

        int var29 = var45;
        var45 += var20;
        int var30 = var45;
        var45 += var10 * 2;
        int var31 = var45;
        var45 += var11 * 6;
        int var32 = var45;
        var45 += var17;
        int var33 = var45;
        var45 += var18;
        int var10000 = var45 + var19;
        this.t = var9;
        this.l = var10;
        this.j = var11;
        this.xVertices = new int[var9];
        this.yVertices = new int[var9];
        this.zVertices = new int[var9];
        this.b = new int[var10];
        this.e = new int[var10];
        this.x = new int[var10];
        if (var11 > 0) {
            this.k = new byte[var11];
            this.z = new short[var11];
            this.w = new short[var11];
            this.s = new short[var11];
        }

        if (var16 == 1) {
            this.ao = new int[var9];
        }

        if (var12 == 1) {
            this.p = new byte[var10];
            this.o = new byte[var10];
            this.v = new short[var10];
        }

        if (var13 == 255) {
            this.g = new byte[var10];
        } else {
            this.u = (byte) var13;
        }

        if (var14 == 1) {
            this.n = new byte[var10];
        }

        if (var15 == 1) {
            this.av = new int[var10];
        }

        this.c = new short[var10];
        var4.index = var21;
        var5.index = var32;
        var6.index = var33;
        var7.index = var45;
        var8.index = var27;
        int var35 = 0;
        int var36 = 0;
        int var37 = 0;

        int var38;
        int var39;
        int var40;
        int var41;
        int var42;
        for (var38 = 0; var38 < var9; ++var38) {
            var39 = var4.av();
            var40 = 0;
            if ((var39 & 1) != 0) {
                var40 = var5.at();
            }

            var41 = 0;
            if ((var39 & 2) != 0) {
                var41 = var6.at();
            }

            var42 = 0;
            if ((var39 & 4) != 0) {
                var42 = var7.at();
            }

            this.xVertices[var38] = var35 + var40;
            this.yVertices[var38] = var36 + var41;
            this.zVertices[var38] = var37 + var42;
            var35 = this.xVertices[var38];
            var36 = this.yVertices[var38];
            var37 = this.zVertices[var38];
            if (var16 == 1) {
                this.ao[var38] = var8.av();
            }
        }

        var4.index = var30;
        var5.index = var26;
        var6.index = var24;
        var7.index = var28;
        var8.index = var25;

        for (var38 = 0; var38 < var10; ++var38) {
            this.c[var38] = (short) var4.ae();
            if (var12 == 1) {
                var39 = var5.av();
                if ((var39 & 1) == 1) {
                    this.p[var38] = 1;
                    var2 = true;
                } else {
                    this.p[var38] = 0;
                }

                if ((var39 & 2) == 2) {
                    this.o[var38] = (byte) (var39 >> 2);
                    this.v[var38] = this.c[var38];
                    this.c[var38] = 127;
                    if (this.v[var38] != -1) {
                        var3 = true;
                    }
                } else {
                    this.o[var38] = -1;
                    this.v[var38] = -1;
                }
            }

            if (var13 == 255) {
                this.g[var38] = var6.aj();
            }

            if (var14 == 1) {
                this.n[var38] = var7.aj();
            }

            if (var15 == 1) {
                this.av[var38] = var8.av();
            }
        }

        var4.index = var29;
        var5.index = var23;
        var38 = 0;
        var39 = 0;
        var40 = 0;
        var41 = 0;

        int var43;
        int var44;
        for (var42 = 0; var42 < var10; ++var42) {
            var43 = var5.av();
            if (var43 == 1) {
                var38 = var4.at() + var41;
                var39 = var4.at() + var38;
                var40 = var4.at() + var39;
                var41 = var40;
                this.b[var42] = var38;
                this.e[var42] = var39;
                this.x[var42] = var40;
            }

            if (var43 == 2) {
                var39 = var40;
                var40 = var4.at() + var41;
                var41 = var40;
                this.b[var42] = var38;
                this.e[var42] = var39;
                this.x[var42] = var40;
            }

            if (var43 == 3) {
                var38 = var40;
                var40 = var4.at() + var41;
                var41 = var40;
                this.b[var42] = var38;
                this.e[var42] = var39;
                this.x[var42] = var40;
            }

            if (var43 == 4) {
                var44 = var38;
                var38 = var39;
                var39 = var44;
                var40 = var4.at() + var41;
                var41 = var40;
                this.b[var42] = var38;
                this.e[var42] = var44;
                this.x[var42] = var40;
            }
        }

        var4.index = var31;

        for (var42 = 0; var42 < var11; ++var42) {
            this.k[var42] = 0;
            this.z[var42] = (short) var4.ae();
            this.w[var42] = (short) var4.ae();
            this.s[var42] = (short) var4.ae();
        }

        if (this.o != null) {
            boolean var46 = false;

            for (var43 = 0; var43 < var10; ++var43) {
                var44 = this.o[var43] & 255;
                if (var44 != 255) {
                    if (this.b[var43] == (this.z[var44] & '\uffff') && this.e[var43] == (this.w[var44] & '\uffff')
                            && this.x[var43] == (this.s[var44] & '\uffff')) {
                        this.o[var43] = -1;
                    } else {
                        var46 = true;
                    }
                }
            }

            if (!var46) {
                this.o = null;
            }
        }

        if (!var3) {
            this.v = null;
        }

        if (!var2) {
            this.p = null;
        }

    }

    @ObfuscatedName("a")
    final int a(AlternativeModel var1, int var2) {
        int var3 = -1;
        int var4 = var1.xVertices[var2];
        int var5 = var1.yVertices[var2];
        int var6 = var1.zVertices[var2];

        for (int var7 = 0; var7 < this.t; ++var7) {
            if (var4 == this.xVertices[var7] && var5 == this.yVertices[var7] && var6 == this.zVertices[var7]) {
                var3 = var7;
                break;
            }
        }

        if (var3 == -1) {
            this.xVertices[this.t] = var4;
            this.yVertices[this.t] = var5;
            this.zVertices[this.t] = var6;
            if (var1.ao != null) {
                this.ao[this.t] = var1.ao[var2];
            }

            var3 = this.t++;
        }

        return var3;
    }

    @ObfuscatedName("l")
    public AlternativeModel l() {
        AlternativeModel var1 = new AlternativeModel();
        if (this.p != null) {
            var1.p = new byte[this.l];

            for (int var2 = 0; var2 < this.l; ++var2) {
                var1.p[var2] = this.p[var2];
            }
        }

        var1.t = this.t;
        var1.l = this.l;
        var1.j = this.j;
        var1.xVertices = this.xVertices;
        var1.yVertices = this.yVertices;
        var1.zVertices = this.zVertices;
        var1.b = this.b;
        var1.e = this.e;
        var1.x = this.x;
        var1.g = this.g;
        var1.n = this.n;
        var1.o = this.o;
        var1.c = this.c;
        var1.v = this.v;
        var1.u = this.u;
        var1.k = this.k;
        var1.z = this.z;
        var1.w = this.w;
        var1.s = this.s;
        var1.d = this.d;
        var1.f = this.f;
        var1.r = this.r;
        var1.y = this.y;
        var1.ay = this.ay;
        var1.h = this.h;
        var1.m = this.m;
        var1.ao = this.ao;
        var1.av = this.av;
        var1.aj = this.aj;
        var1.ae = this.ae;
        var1.az = this.az;
        var1.am = this.am;
        var1.ah = this.ah;
        var1.au = this.au;
        return var1;
    }

    @ObfuscatedName("b")
    public AlternativeModel b(int[][] var1, int var2, int var3, int var4, boolean var5, int var6) {
        this.y();
        int var7 = var2 + this.an;
        int var8 = var2 + this.ai;
        int var9 = var4 + this.at;
        int var10 = var4 + this.al;
        if (var7 >= 0 && var8 + 128 >> 7 < var1.length && var9 >= 0 && var10 + 128 >> 7 < var1[0].length) {
            var7 >>= 7;
            var8 = var8 + 127 >> 7;
            var9 >>= 7;
            var10 = var10 + 127 >> 7;
            if (var3 == var1[var7][var9] && var3 == var1[var8][var9] && var3 == var1[var7][var10]
                    && var3 == var1[var8][var10]) {
                return this;
            } else {
                AlternativeModel var11 = new AlternativeModel();
                var11.t = this.t;
                var11.l = this.l;
                var11.j = this.j;
                var11.xVertices = this.xVertices;
                var11.zVertices = this.zVertices;
                var11.b = this.b;
                var11.e = this.e;
                var11.x = this.x;
                var11.p = this.p;
                var11.g = this.g;
                var11.n = this.n;
                var11.o = this.o;
                var11.c = this.c;
                var11.v = this.v;
                var11.u = this.u;
                var11.k = this.k;
                var11.z = this.z;
                var11.w = this.w;
                var11.s = this.s;
                var11.d = this.d;
                var11.f = this.f;
                var11.r = this.r;
                var11.y = this.y;
                var11.ay = this.ay;
                var11.h = this.h;
                var11.m = this.m;
                var11.ao = this.ao;
                var11.av = this.av;
                var11.aj = this.aj;
                var11.ae = this.ae;
                var11.ah = this.ah;
                var11.au = this.au;
                var11.yVertices = new int[var11.t];
                int var12;
                int var13;
                int var14;
                int var15;
                int var16;
                int var17;
                int var18;
                int var19;
                int var20;
                int var21;
                if (var6 == 0) {
                    for (var12 = 0; var12 < var11.t; ++var12) {
                        var13 = var2 + this.xVertices[var12];
                        var14 = var4 + this.zVertices[var12];
                        var15 = var13 & 127;
                        var16 = var14 & 127;
                        var17 = var13 >> 7;
                        var18 = var14 >> 7;
                        var19 = var1[var17][var18] * (128 - var15) + var1[var17 + 1][var18] * var15 >> 7;
                        var20 = var1[var17][var18 + 1] * (128 - var15) + var15 * var1[var17 + 1][var18 + 1] >> 7;
                        var21 = var19 * (128 - var16) + var20 * var16 >> 7;
                        var11.yVertices[var12] = var21 + this.yVertices[var12] - var3;
                    }
                } else {
                    for (var12 = 0; var12 < var11.t; ++var12) {
                        var13 = (-this.yVertices[var12] << 16) / super.modelHeight;
                        if (var13 < var6) {
                            var14 = var2 + this.xVertices[var12];
                            var15 = var4 + this.zVertices[var12];
                            var16 = var14 & 127;
                            var17 = var15 & 127;
                            var18 = var14 >> 7;
                            var19 = var15 >> 7;
                            var20 = var1[var18][var19] * (128 - var16) + var1[var18 + 1][var19] * var16 >> 7;
                            var21 = var1[var18][var19 + 1] * (128 - var16) + var16 * var1[var18 + 1][var19 + 1] >> 7;
                            int var22 = var20 * (128 - var17) + var21 * var17 >> 7;
                            var11.yVertices[var12] = (var6 - var13) * (var22 - var3) / var6 + this.yVertices[var12];
                        }
                    }
                }

                var11.r();
                return var11;
            }
        } else {
            return this;
        }
    }

    @ObfuscatedName("e")
    void e() {
        int[] var1;
        int var2;
        int var3;
        int var4;
        if (this.ao != null) {
            var1 = new int[256];
            var2 = 0;

            for (var3 = 0; var3 < this.t; ++var3) {
                var4 = this.ao[var3];
                ++var1[var4];
                if (var4 > var2) {
                    var2 = var4;
                }
            }

            this.aj = new int[var2 + 1][];

            for (var3 = 0; var3 <= var2; ++var3) {
                this.aj[var3] = new int[var1[var3]];
                var1[var3] = 0;
            }

            for (var3 = 0; var3 < this.t; this.aj[var4][var1[var4]++] = var3++) {
                var4 = this.ao[var3];
            }

            this.ao = null;
        }

        if (this.av != null) {
            var1 = new int[256];
            var2 = 0;

            for (var3 = 0; var3 < this.l; ++var3) {
                var4 = this.av[var3];
                ++var1[var4];
                if (var4 > var2) {
                    var2 = var4;
                }
            }

            this.ae = new int[var2 + 1][];

            for (var3 = 0; var3 <= var2; ++var3) {
                this.ae[var3] = new int[var1[var3]];
                var1[var3] = 0;
            }

            for (var3 = 0; var3 < this.l; this.ae[var4][var1[var4]++] = var3++) {
                var4 = this.av[var3];
            }

            this.av = null;
        }

    }

    @ObfuscatedName("x")
    public void x() {
        for (int var1 = 0; var1 < this.t; ++var1) {
            int var2 = this.xVertices[var1];
            this.xVertices[var1] = this.zVertices[var1];
            this.zVertices[var1] = -var2;
        }

        this.r();
    }

    @ObfuscatedName("o")
    public void o() {
        for (int var1 = 0; var1 < this.t; ++var1) {
            this.xVertices[var1] = -this.xVertices[var1];
            this.zVertices[var1] = -this.zVertices[var1];
        }

        this.r();
    }

    @ObfuscatedName("c")
    public void c() {
        for (int var1 = 0; var1 < this.t; ++var1) {
            int var2 = this.zVertices[var1];
            this.zVertices[var1] = this.xVertices[var1];
            this.xVertices[var1] = -var2;
        }

        this.r();
    }

    @ObfuscatedName("u")
    public void u(int var1) {
        int var2 = aq[var1];
        int var3 = aa[var1];

        for (int var4 = 0; var4 < this.t; ++var4) {
            int var5 = var2 * this.zVertices[var4] + var3 * this.xVertices[var4] >> 16;
            this.zVertices[var4] = var3 * this.zVertices[var4] - var2 * this.xVertices[var4] >> 16;
            this.xVertices[var4] = var5;
        }

        this.r();
    }

    @ObfuscatedName("k")
    public void k(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < this.t; ++var4) {
            this.xVertices[var4] += var1;
            this.yVertices[var4] += var2;
            this.zVertices[var4] += var3;
        }

        this.r();
    }

    @ObfuscatedName("z")
    public void z(short var1, short var2) {
        for (int var3 = 0; var3 < this.l; ++var3) {
            if (this.c[var3] == var1) {
                this.c[var3] = var2;
            }
        }

    }

    @ObfuscatedName("w")
    public void w(short var1, short var2) {
        if (this.v != null) {
            for (int var3 = 0; var3 < this.l; ++var3) {
                if (this.v[var3] == var1) {
                    this.v[var3] = var2;
                }
            }

        }
    }

    @ObfuscatedName("s")
    public void s() {
        int var1;
        for (var1 = 0; var1 < this.t; ++var1) {
            this.zVertices[var1] = -this.zVertices[var1];
        }

        for (var1 = 0; var1 < this.l; ++var1) {
            int var2 = this.b[var1];
            this.b[var1] = this.x[var1];
            this.x[var1] = var2;
        }

        this.r();
    }

    @ObfuscatedName("d")
    public void d(int var1, int var2, int var3) {
        for (int var4 = 0; var4 < this.t; ++var4) {
            this.xVertices[var4] = this.xVertices[var4] * var1 / 128;
            this.yVertices[var4] = var2 * this.yVertices[var4] / 128;
            this.zVertices[var4] = var3 * this.zVertices[var4] / 128;
        }

        this.r();
    }

    @ObfuscatedName("f")
    public void f() {
        if (this.az == null) {
            this.az = new ee[this.t];

            int var1;
            for (var1 = 0; var1 < this.t; ++var1) {
                this.az[var1] = new ee();
            }

            for (var1 = 0; var1 < this.l; ++var1) {
                int var2 = this.b[var1];
                int var3 = this.e[var1];
                int var4 = this.x[var1];
                int var5 = this.xVertices[var3] - this.xVertices[var2];
                int var6 = this.yVertices[var3] - this.yVertices[var2];
                int var7 = this.zVertices[var3] - this.zVertices[var2];
                int var8 = this.xVertices[var4] - this.xVertices[var2];
                int var9 = this.yVertices[var4] - this.yVertices[var2];
                int var10 = this.zVertices[var4] - this.zVertices[var2];
                int var11 = var6 * var10 - var9 * var7;
                int var12 = var7 * var8 - var10 * var5;

                int var13;
                for (var13 = var5 * var9 - var8 * var6; var11 > 8192 || var12 > 8192 || var13 > 8192 || var11 < -8192
                        || var12 < -8192 || var13 < -8192; var13 >>= 1) {
                    var11 >>= 1;
                    var12 >>= 1;
                }

                int var14 = (int) Math.sqrt((double) (var11 * var11 + var12 * var12 + var13 * var13));
                if (var14 <= 0) {
                    var14 = 1;
                }

                var11 = var11 * 256 / var14;
                var12 = var12 * 256 / var14;
                var13 = var13 * 256 / var14;
                byte var15;
                if (this.p == null) {
                    var15 = 0;
                } else {
                    var15 = this.p[var1];
                }

                if (var15 == 0) {
                    ee var16 = this.az[var2];
                    var16.t += var11;
                    var16.q += var12;
                    var16.i += var13;
                    ++var16.a;
                    var16 = this.az[var3];
                    var16.t += var11;
                    var16.q += var12;
                    var16.i += var13;
                    ++var16.a;
                    var16 = this.az[var4];
                    var16.t += var11;
                    var16.q += var12;
                    var16.i += var13;
                    ++var16.a;
                } else if (var15 == 1) {
                    if (this.am == null) {
                        this.am = new ez[this.l];
                    }

                    ez var17 = this.am[var1] = new ez();
                    var17.t = var11;
                    var17.q = var12;
                    var17.i = var13;
                }
            }

        }
    }

    @ObfuscatedName("r")
    void r() {
        this.az = null;
        this.ap = null;
        this.am = null;
        this.ax = false;
    }

    @ObfuscatedName("y")
    void y() {
        if (!this.ax) {
            super.modelHeight = 0;
            this.ar = 0;
            this.an = 999999;
            this.ai = -999999;
            this.al = -99999;
            this.at = 99999;

            for (int var1 = 0; var1 < this.t; ++var1) {
                int var2 = this.xVertices[var1];
                int var3 = this.yVertices[var1];
                int var4 = this.zVertices[var1];
                if (var2 < this.an) {
                    this.an = var2;
                }

                if (var2 > this.ai) {
                    this.ai = var2;
                }

                if (var4 < this.at) {
                    this.at = var4;
                }

                if (var4 > this.al) {
                    this.al = var4;
                }

                if (-var3 > super.modelHeight) {
                    super.modelHeight = -var3;
                }

                if (var3 > this.ar) {
                    this.ar = var3;
                }
            }

            this.ax = true;
        }
    }

    @ObfuscatedName("av")
    public final Model av(int var1, int var2, int var3, int var4, int var5) {
        this.f();
        int var6 = (int) Math.sqrt((double) (var5 * var5 + var3 * var3 + var4 * var4));
        int var7 = var6 * var2 >> 8;
        Model var8 = new Model();
        var8.c = new int[this.l];
        var8.v = new int[this.l];
        var8.u = new int[this.l];
        if (this.j > 0 && this.o != null) {
            int[] var9 = new int[this.j];

            int var10;
            for (var10 = 0; var10 < this.l; ++var10) {
                if (this.o[var10] != -1) {
                    ++var9[this.o[var10] & 255];
                }
            }

            var8.vertexCount = 0;

            for (var10 = 0; var10 < this.j; ++var10) {
                if (var9[var10] > 0 && this.k[var10] == 0) {
                    ++var8.vertexCount;
                }
            }

            var8.f = new int[var8.vertexCount];
            var8.r = new int[var8.vertexCount];
            var8.y = new int[var8.vertexCount];
            var10 = 0;

            int var11;
            for (var11 = 0; var11 < this.j; ++var11) {
                if (var9[var11] > 0 && this.k[var11] == 0) {
                    var8.f[var10] = this.z[var11] & '\uffff';
                    var8.r[var10] = this.w[var11] & '\uffff';
                    var8.y[var10] = this.s[var11] & '\uffff';
                    var9[var11] = var10++;
                } else {
                    var9[var11] = -1;
                }
            }

            var8.z = new byte[this.l];

            for (var11 = 0; var11 < this.l; ++var11) {
                if (this.o[var11] != -1) {
                    var8.z[var11] = (byte) var9[this.o[var11] & 255];
                } else {
                    var8.z[var11] = -1;
                }
            }
        }

        for (int var16 = 0; var16 < this.l; ++var16) {
            byte var17;
            if (this.p == null) {
                var17 = 0;
            } else {
                var17 = this.p[var16];
            }

            byte var18;
            if (this.n == null) {
                var18 = 0;
            } else {
                var18 = this.n[var16];
            }

            short var12;
            if (this.v == null) {
                var12 = -1;
            } else {
                var12 = this.v[var16];
            }

            if (var18 == -2) {
                var17 = 3;
            }

            if (var18 == -1) {
                var17 = 2;
            }

            ee var13;
            int var14;
            ez var19;
            if (var12 == -1) {
                if (var17 != 0) {
                    if (var17 == 1) {
                        var19 = this.am[var16];
                        var14 = (var4 * var19.q + var5 * var19.i + var3 * var19.t) / (var7 / 2 + var7) + var1;
                        var8.c[var16] = aj(this.c[var16] & '\uffff', var14);
                        var8.u[var16] = -1;
                    } else if (var17 == 3) {
                        var8.c[var16] = 128;
                        var8.u[var16] = -1;
                    } else {
                        var8.u[var16] = -2;
                    }
                } else {
                    int var15 = this.c[var16] & '\uffff';
                    if (this.ap != null && this.ap[this.b[var16]] != null) {
                        var13 = this.ap[this.b[var16]];
                    } else {
                        var13 = this.az[this.b[var16]];
                    }

                    var14 = (var4 * var13.q + var5 * var13.i + var3 * var13.t) / (var7 * var13.a) + var1;
                    var8.c[var16] = aj(var15, var14);
                    if (this.ap != null && this.ap[this.e[var16]] != null) {
                        var13 = this.ap[this.e[var16]];
                    } else {
                        var13 = this.az[this.e[var16]];
                    }

                    var14 = (var4 * var13.q + var5 * var13.i + var3 * var13.t) / (var7 * var13.a) + var1;
                    var8.v[var16] = aj(var15, var14);
                    if (this.ap != null && this.ap[this.x[var16]] != null) {
                        var13 = this.ap[this.x[var16]];
                    } else {
                        var13 = this.az[this.x[var16]];
                    }

                    var14 = (var4 * var13.q + var5 * var13.i + var3 * var13.t) / (var7 * var13.a) + var1;
                    var8.u[var16] = aj(var15, var14);
                }
            } else if (var17 != 0) {
                if (var17 == 1) {
                    var19 = this.am[var16];
                    var14 = (var4 * var19.q + var5 * var19.i + var3 * var19.t) / (var7 / 2 + var7) + var1;
                    var8.c[var16] = ae(var14);
                    var8.u[var16] = -1;
                } else {
                    var8.u[var16] = -2;
                }
            } else {
                if (this.ap != null && this.ap[this.b[var16]] != null) {
                    var13 = this.ap[this.b[var16]];
                } else {
                    var13 = this.az[this.b[var16]];
                }

                var14 = (var4 * var13.q + var5 * var13.i + var3 * var13.t) / (var7 * var13.a) + var1;
                var8.c[var16] = ae(var14);
                if (this.ap != null && this.ap[this.e[var16]] != null) {
                    var13 = this.ap[this.e[var16]];
                } else {
                    var13 = this.az[this.e[var16]];
                }

                var14 = (var4 * var13.q + var5 * var13.i + var3 * var13.t) / (var7 * var13.a) + var1;
                var8.v[var16] = ae(var14);
                if (this.ap != null && this.ap[this.x[var16]] != null) {
                    var13 = this.ap[this.x[var16]];
                } else {
                    var13 = this.az[this.x[var16]];
                }

                var14 = (var4 * var13.q + var5 * var13.i + var3 * var13.t) / (var7 * var13.a) + var1;
                var8.u[var16] = ae(var14);
            }
        }

        this.e();
        var8.indexCount = this.t;
        var8.xVertices = this.xVertices;
        var8.yVertices = this.yVertices;
        var8.zVertices = this.zVertices;
        var8.texturedVertexCount = this.l;
        var8.aIndices = this.b;
        var8.bIndices = this.e;
        var8.cIndices = this.x;
        var8.j = this.g;
        var8.k = this.n;
        var8.s = this.u;
        var8.h = this.aj;
        var8.m = this.ae;
        var8.w = this.v;
        return var8;
    }

    @ObfuscatedName("t")
    public static AlternativeModel t(FileSystem var0, int var1, int var2) {
        byte[] var3 = var0.i(var1, var2);
        return var3 == null ? null : new AlternativeModel(var3);
    }

    @ObfuscatedName("h")
    static void h(AlternativeModel var0, AlternativeModel var1, int var2, int var3, int var4, boolean var5) {
        var0.y();
        var0.f();
        var1.y();
        var1.f();
        ++aw;
        int var6 = 0;
        int[] var7 = var1.xVertices;
        int var8 = var1.t;

        int var9;
        for (var9 = 0; var9 < var0.t; ++var9) {
            ee var10 = var0.az[var9];
            if (var10.a != 0) {
                int var11 = var0.yVertices[var9] - var3;
                if (var11 <= var1.ar) {
                    int var12 = var0.xVertices[var9] - var2;
                    if (var12 >= var1.an && var12 <= var1.ai) {
                        int var13 = var0.zVertices[var9] - var4;
                        if (var13 >= var1.at && var13 <= var1.al) {
                            for (int var14 = 0; var14 < var8; ++var14) {
                                ee var15 = var1.az[var14];
                                if (var12 == var7[var14] && var13 == var1.zVertices[var14]
                                        && var11 == var1.yVertices[var14] && var15.a != 0) {
                                    if (var0.ap == null) {
                                        var0.ap = new ee[var0.t];
                                    }

                                    if (var1.ap == null) {
                                        var1.ap = new ee[var8];
                                    }

                                    ee var16 = var0.ap[var9];
                                    if (var16 == null) {
                                        var16 = var0.ap[var9] = new ee(var10);
                                    }

                                    ee var17 = var1.ap[var14];
                                    if (var17 == null) {
                                        var17 = var1.ap[var14] = new ee(var15);
                                    }

                                    var16.t += var15.t;
                                    var16.q += var15.q;
                                    var16.i += var15.i;
                                    var16.a += var15.a;
                                    var17.t += var10.t;
                                    var17.q += var10.q;
                                    var17.i += var10.i;
                                    var17.a += var10.a;
                                    ++var6;
                                    ag[var9] = aw;
                                    as[var14] = aw;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (var6 >= 3 && var5) {
            for (var9 = 0; var9 < var0.l; ++var9) {
                if (ag[var0.b[var9]] == aw && ag[var0.e[var9]] == aw && ag[var0.x[var9]] == aw) {
                    if (var0.p == null) {
                        var0.p = new byte[var0.l];
                    }

                    var0.p[var9] = 2;
                }
            }

            for (var9 = 0; var9 < var1.l; ++var9) {
                if (aw == as[var1.b[var9]] && aw == as[var1.e[var9]] && aw == as[var1.x[var9]]) {
                    if (var1.p == null) {
                        var1.p = new byte[var1.l];
                    }

                    var1.p[var9] = 2;
                }
            }

        }
    }

    @ObfuscatedName("aj")
    static final int aj(int var0, int var1) {
        var1 = (var0 & 127) * var1 >> 7;
        if (var1 < 2) {
            var1 = 2;
        } else if (var1 > 126) {
            var1 = 126;
        }

        return (var0 & 'ﾀ') + var1;
    }

    @ObfuscatedName("ae")
    static final int ae(int var0) {
        if (var0 < 2) {
            var0 = 2;
        } else if (var0 > 126) {
            var0 = 126;
        }

        return var0;
    }
}
