import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("gr")
public abstract class gr {

    @ObfuscatedName("q")
    static boolean q = false;

    @ObfuscatedName("t")
    abstract byte[] t();

    @ObfuscatedName("q")
    abstract void q(byte[] var1);

    @ObfuscatedName("t")
    public static int t(int var0, int var1, int var2) {
        var2 &= 3;
        if (var2 == 0) {
            return var1;
        } else if (var2 == 1) {
            return 7 - var0;
        } else {
            return var2 == 2 ? 7 - var1 : var0;
        }
    }

    @ObfuscatedName("l")
    public static int l(int var0, int var1) {
        int var2 = var0 >>> 31;
        return (var0 + var2) / var1 - var2;
    }
}
