import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("dt")
public class dt {

    @ObfuscatedName("c")
    public static km c;

    @ObfuscatedName("kr")
    static int kr;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int[][] b;

    public dt(int var1, int var2) {
        if (var2 != var1) {
            int var3 = fy.q(var1, var2);
            var1 /= var3;
            var2 /= var3;
            this.a = var1;
            this.l = var2;
            this.b = new int[var1][14];

            for (int var4 = 0; var4 < var1; ++var4) {
                int[] var5 = this.b[var4];
                double var6 = (double) var4 / (double) var1 + 6.0D;
                int var8 = (int) Math.floor(1.0D + (var6 - 7.0D));
                if (var8 < 0) {
                    var8 = 0;
                }

                int var9 = (int) Math.ceil(var6 + 7.0D);
                if (var9 > 14) {
                    var9 = 14;
                }

                for (double var10 = (double) var2 / (double) var1; var8 < var9; ++var8) {
                    double var12 = 3.141592653589793D * ((double) var8 - var6);
                    double var14 = var10;
                    if (var12 < -1.0E-4D || var12 > 1.0E-4D) {
                        var14 = var10 * (Math.sin(var12) / var12);
                    }

                    var14 *= 0.54D + 0.46D * Math.cos(((double) var8 - var6) * 0.2243994752564138D);
                    var5[var8] = (int) Math.floor(65536.0D * var14 + 0.5D);
                }
            }

        }
    }

    @ObfuscatedName("t")
    byte[] t(byte[] var1) {
        if (this.b != null) {
            int var2 = (int) ((long) var1.length * (long) this.l / (long) this.a) + 14;
            int[] var3 = new int[var2];
            int var4 = 0;
            int var5 = 0;

            int var6;
            for (var6 = 0; var6 < var1.length; ++var6) {
                byte var7 = var1[var6];
                int[] var8 = this.b[var5];

                int var9;
                for (var9 = 0; var9 < 14; ++var9) {
                    var3[var4 + var9] += var7 * var8[var9];
                }

                var5 += this.l;
                var9 = var5 / this.a;
                var4 += var9;
                var5 -= var9 * this.a;
            }

            var1 = new byte[var2];

            for (var6 = 0; var6 < var2; ++var6) {
                int var10 = var3[var6] + '耀' >> 16;
                if (var10 < -128) {
                    var1[var6] = -128;
                } else if (var10 > 127) {
                    var1[var6] = 127;
                } else {
                    var1[var6] = (byte) var10;
                }
            }
        }

        return var1;
    }

    @ObfuscatedName("q")
    int q(int var1) {
        if (this.b != null) {
            var1 = (int) ((long) this.l * (long) var1 / (long) this.a);
        }

        return var1;
    }

    @ObfuscatedName("i")
    int i(int var1) {
        if (this.b != null) {
            var1 = (int) ((long) this.l * (long) var1 / (long) this.a) + 6;
        }

        return var1;
    }

    @ObfuscatedName("t")
    static double t(double var0, double var2, double var4) {
        double var8 = (var0 - var2) / var4;
        double var6 = Math.exp(var8 * -var8 / 2.0D) / Math.sqrt(6.283185307179586D);
        return var6 / var4;
    }
}
