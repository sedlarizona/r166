import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("fb")
public class Task {

    @ObfuscatedName("t")
    Task task;

    @ObfuscatedName("l")
    public volatile int l = 0;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    public int e;

    @ObfuscatedName("x")
    Object x;

    @ObfuscatedName("p")
    public volatile Object p;
}
