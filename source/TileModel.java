import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("ei")
public final class TileModel {

    @ObfuscatedName("av")
    static int[] av;

    @ObfuscatedName("ek")
    static long ek;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int texture;

    @ObfuscatedName("b")
    boolean flat = true;

    @ObfuscatedName("e")
    int color;

    TileModel(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7) {
        this.t = var1;
        this.q = var2;
        this.i = var3;
        this.a = var4;
        this.texture = var5;
        this.color = var6;
        this.flat = var7;
    }

    @ObfuscatedName("t")
    static void t(int var0, fn var1, ju var2) {
        byte[] var3 = null;
        Deque var4 = jt.q;
        synchronized (jt.q) {
            for (ia var5 = (ia) jt.q.e(); var5 != null; var5 = (ia) jt.q.p()) {
                if (var5.uid == (long) var0 && var1 == var5.i && var5.t == 0) {
                    var3 = var5.q;
                    break;
                }
            }
        }

        if (var3 != null) {
            var2.dq(var1, var0, var3, true);
        } else {
            byte[] var8 = var1.t(var0);
            var2.dq(var1, var0, var8, true);
        }
    }

    @ObfuscatedName("gy")
    static final void gy(ga var0) {
        Packet var1 = Client.ee.b;
        int var2;
        int var3;
        int var4;
        int var5;
        int var6;
        int var7;
        int var8;
        if (ga.i == var0) {
            var2 = var1.bm();
            var3 = var2 >> 2;
            var4 = var2 & 3;
            var5 = Client.fb[var3];
            var6 = var1.bc();
            var7 = (var6 >> 4 & 7) + bn.fd;
            var8 = (var6 & 7) + ez.fh;
            if (var7 >= 0 && var8 >= 0 && var7 < 104 && var8 < 104) {
                ar.ht(kt.ii, var7, var8, var5, -1, var3, var4, 0, -1);
            }

        } else {
            int var9;
            if (ga.p == var0) {
                var2 = var1.av();
                var3 = (var2 >> 4 & 7) + bn.fd;
                var4 = (var2 & 7) + ez.fh;
                var5 = var1.bb();
                var6 = var1.bc();
                var7 = var6 >> 2;
                var8 = var6 & 3;
                var9 = Client.fb[var7];
                if (var3 >= 0 && var4 >= 0 && var3 < 104 && var4 < 104) {
                    ar.ht(kt.ii, var3, var4, var9, var5, var7, var8, 0, -1);
                }

            } else if (ga.q == var0) {
                var2 = var1.bc();
                var3 = (var2 >> 4 & 7) + bn.fd;
                var4 = (var2 & 7) + ez.fh;
                var5 = var1.bz();
                var6 = var1.bz();
                var7 = var1.bc();
                if (var3 >= 0 && var4 >= 0 && var3 < 104 && var4 < 104) {
                    var3 = var3 * 128 + 64;
                    var4 = var4 * 128 + 64;
                    AnimableObject var42 = new AnimableObject(var6, kt.ii, var3, var4, ef.gn(var3, var4, kt.ii) - var7,
                            var5, Client.bz);
                    Client.jf.q(var42);
                }

            } else {
                int var10;
                if (ga.g == var0) {
                    var2 = var1.bm();
                    var3 = (var2 >> 4 & 7) + bn.fd;
                    var4 = (var2 & 7) + ez.fh;
                    var5 = var1.bm();
                    var6 = var5 >> 4 & 15;
                    var7 = var5 & 7;
                    var8 = var1.bm();
                    var9 = var1.bb();
                    if (var3 >= 0 && var4 >= 0 && var3 < 104 && var4 < 104) {
                        var10 = var6 + 1;
                        if (az.il.co[0] >= var3 - var10 && az.il.co[0] <= var3 + var10 && az.il.cv[0] >= var4 - var10
                                && az.il.cv[0] <= var10 + var4 && Client.pa != 0 && var7 > 0
                                && Client.audioEffectCount < 50) {
                            Client.po[Client.audioEffectCount] = var9;
                            Client.pe[Client.audioEffectCount] = var7;
                            Client.pt[Client.audioEffectCount] = var8;
                            Client.audioTracks[Client.audioEffectCount] = null;
                            Client.pl[Client.audioEffectCount] = var6 + (var4 << 8) + (var3 << 16);
                            ++Client.audioEffectCount;
                        }
                    }
                }

                ItemNode var32;
                if (ga.a == var0) {
                    var2 = var1.be();
                    var3 = (var2 >> 4 & 7) + bn.fd;
                    var4 = (var2 & 7) + ez.fh;
                    var5 = var1.bz();
                    var6 = var1.bz();
                    if (var3 >= 0 && var4 >= 0 && var3 < 104 && var4 < 104) {
                        var32 = new ItemNode();
                        var32.id = var6;
                        var32.stackSize = var5;
                        if (Client.loadedGroundItems[kt.ii][var3][var4] == null) {
                            Client.loadedGroundItems[kt.ii][var3][var4] = new Deque();
                        }

                        Client.loadedGroundItems[kt.ii][var3][var4].q(var32);
                        Canvas.hj(var3, var4);
                    }

                } else {
                    int var11;
                    int var12;
                    int var13;
                    int var14;
                    if (ga.t == var0) {
                        var2 = var1.bz();
                        byte var38 = var1.aj();
                        var4 = var1.bz();
                        byte var39 = var1.aj();
                        var6 = var1.be();
                        var7 = (var6 >> 4 & 7) + bn.fd;
                        var8 = (var6 & 7) + ez.fh;
                        byte var40 = var1.bj();
                        var10 = var1.bb();
                        var11 = var1.bc();
                        var12 = var11 >> 2;
                        var13 = var11 & 3;
                        var14 = Client.fb[var12];
                        int var15 = var1.ae();
                        byte var16 = var1.bj();
                        Player var17;
                        if (var15 == Client.localPlayerIndex) {
                            var17 = az.il;
                        } else {
                            var17 = Client.loadedPlayers[var15];
                        }

                        if (var17 != null) {
                            ObjectDefinition var18 = jw.q(var10);
                            int var19;
                            int var20;
                            if (var13 != 1 && var13 != 3) {
                                var19 = var18.width;
                                var20 = var18.height;
                            } else {
                                var19 = var18.height;
                                var20 = var18.width;
                            }

                            int var21 = var7 + (var19 >> 1);
                            int var22 = var7 + (var19 + 1 >> 1);
                            int var23 = var8 + (var20 >> 1);
                            int var24 = var8 + (var20 + 1 >> 1);
                            int[][] var25 = bt.tileHeights[kt.ii];
                            int var26 = var25[var22][var24] + var25[var21][var24] + var25[var22][var23]
                                    + var25[var21][var23] >> 2;
                            int var27 = (var7 << 7) + (var19 << 6);
                            int var28 = (var8 << 7) + (var20 << 6);
                            Model var29 = var18.p(var12, var13, var25, var27, var26, var28);
                            if (var29 != null) {
                                ar.ht(kt.ii, var7, var8, var14, -1, 0, 0, var4 + 1, var2 + 1);
                                var17.transformObjectStartCycle = var4 + Client.bz;
                                var17.transformObjectEndCycle = var2 + Client.bz;
                                var17.transformObjectModel = var29;
                                var17.transformObjectX = var7 * 128 + var19 * 64;
                                var17.transformObjectY = var8 * 128 + var20 * 64;
                                var17.transformObjectZ = var26;
                                byte var30;
                                if (var38 > var39) {
                                    var30 = var38;
                                    var38 = var39;
                                    var39 = var30;
                                }

                                if (var40 > var16) {
                                    var30 = var40;
                                    var40 = var16;
                                    var16 = var30;
                                }

                                var17.j = var7 + var38;
                                var17.z = var7 + var39;
                                var17.k = var8 + var40;
                                var17.w = var16 + var8;
                            }
                        }
                    }

                    if (ga.b == var0) {
                        var2 = var1.ae();
                        var3 = var1.be();
                        var4 = (var3 >> 4 & 7) + bn.fd;
                        var5 = (var3 & 7) + ez.fh;
                        var6 = var1.av();
                        var7 = var6 >> 2;
                        var8 = var6 & 3;
                        var9 = Client.fb[var7];
                        if (var4 >= 0 && var5 >= 0 && var4 < 103 && var5 < 103) {
                            if (var9 == 0) {
                                BoundaryObject var33 = an.loadedRegion.aj(kt.ii, var4, var5);
                                if (var33 != null) {
                                    var11 = var33.hash >> 14 & 32767;
                                    if (var7 == 2) {
                                        var33.modelA = new AnimableNode(var11, 2, var8 + 4, kt.ii, var4, var5, var2,
                                                false, var33.modelA);
                                        var33.modelB = new AnimableNode(var11, 2, var8 + 1 & 3, kt.ii, var4, var5,
                                                var2, false, var33.modelB);
                                    } else {
                                        var33.modelA = new AnimableNode(var11, var7, var8, kt.ii, var4, var5, var2,
                                                false, var33.modelA);
                                    }
                                }
                            }

                            if (var9 == 1) {
                                WallObject var43 = an.loadedRegion.ae(kt.ii, var4, var5);
                                if (var43 != null) {
                                    var11 = var43.hash >> 14 & 32767;
                                    if (var7 != 4 && var7 != 5) {
                                        if (var7 == 6) {
                                            var43.modelA = new AnimableNode(var11, 4, var8 + 4, kt.ii, var4, var5,
                                                    var2, false, var43.modelA);
                                        } else if (var7 == 7) {
                                            var43.modelA = new AnimableNode(var11, 4, (var8 + 2 & 3) + 4, kt.ii, var4,
                                                    var5, var2, false, var43.modelA);
                                        } else if (var7 == 8) {
                                            var43.modelA = new AnimableNode(var11, 4, var8 + 4, kt.ii, var4, var5,
                                                    var2, false, var43.modelA);
                                            var43.modelB = new AnimableNode(var11, 4, (var8 + 2 & 3) + 4, kt.ii, var4,
                                                    var5, var2, false, var43.modelB);
                                        }
                                    } else {
                                        var43.modelA = new AnimableNode(var11, 4, var8, kt.ii, var4, var5, var2, false,
                                                var43.modelA);
                                    }
                                }
                            }

                            if (var9 == 2) {
                                EventObject var44 = an.loadedRegion.am(kt.ii, var4, var5);
                                if (var7 == 11) {
                                    var7 = 10;
                                }

                                if (var44 != null) {
                                    var44.model = new AnimableNode(var44.hash >> 14 & 32767, var7, var8, kt.ii, var4,
                                            var5, var2, false, var44.model);
                                }
                            }

                            if (var9 == 3) {
                                FloorObject var45 = an.loadedRegion.az(kt.ii, var4, var5);
                                if (var45 != null) {
                                    var45.model = new AnimableNode(var45.hash >> 14 & 32767, 22, var8, kt.ii, var4,
                                            var5, var2, false, var45.model);
                                }
                            }
                        }

                    } else if (ga.x == var0) {
                        var2 = var1.bc();
                        var3 = (var2 >> 4 & 7) + bn.fd;
                        var4 = (var2 & 7) + ez.fh;
                        var5 = var1.bb();
                        var6 = var1.bz();
                        var7 = var1.ae();
                        if (var3 >= 0 && var4 >= 0 && var3 < 104 && var4 < 104) {
                            Deque var31 = Client.loadedGroundItems[kt.ii][var3][var4];
                            if (var31 != null) {
                                for (ItemNode var34 = (ItemNode) var31.e(); var34 != null; var34 = (ItemNode) var31.p()) {
                                    if ((var6 & 32767) == var34.id && var5 == var34.stackSize) {
                                        var34.stackSize = var7;
                                        break;
                                    }
                                }

                                Canvas.hj(var3, var4);
                            }
                        }

                    } else if (ga.l != var0) {
                        if (ga.e == var0) {
                            byte var37 = var1.bj();
                            var3 = var1.bc();
                            var4 = var1.bb();
                            var5 = var1.bx();
                            var6 = var1.bc();
                            var7 = (var6 >> 4 & 7) + bn.fd;
                            var8 = (var6 & 7) + ez.fh;
                            var9 = var1.bb();
                            var10 = var1.be() * 4;
                            byte var41 = var1.bh();
                            var12 = var1.bz();
                            var13 = var1.bc();
                            var14 = var1.bc() * 4;
                            var11 = var41 + var7;
                            var2 = var37 + var8;
                            if (var7 >= 0 && var8 >= 0 && var7 < 104 && var8 < 104 && var11 >= 0 && var2 >= 0
                                    && var11 < 104 && var2 < 104 && var12 != 65535) {
                                var7 = var7 * 128 + 64;
                                var8 = var8 * 128 + 64;
                                var11 = var11 * 128 + 64;
                                var2 = var2 * 128 + 64;
                                Projectile var36 = new Projectile(var12, kt.ii, var7, var8, ef.gn(var7, var8, kt.ii)
                                        - var10, var4 + Client.bz, var9 + Client.bz, var3, var13, var5, var14);
                                var36.t(var11, var2, ef.gn(var11, var2, kt.ii) - var14, var4 + Client.bz);
                                Client.loadedProjectiles.q(var36);
                            }

                        }
                    } else {
                        var2 = var1.bc();
                        var3 = (var2 >> 4 & 7) + bn.fd;
                        var4 = (var2 & 7) + ez.fh;
                        var5 = var1.bb();
                        if (var3 >= 0 && var4 >= 0 && var3 < 104 && var4 < 104) {
                            Deque var35 = Client.loadedGroundItems[kt.ii][var3][var4];
                            if (var35 != null) {
                                for (var32 = (ItemNode) var35.e(); var32 != null; var32 = (ItemNode) var35.p()) {
                                    if ((var5 & 32767) == var32.id) {
                                        var32.kc();
                                        break;
                                    }
                                }

                                if (var35.e() == null) {
                                    Client.loadedGroundItems[kt.ii][var3][var4] = null;
                                }

                                Canvas.hj(var3, var4);
                            }
                        }

                    }
                }
            }
        }
    }
}
