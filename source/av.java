import java.util.LinkedList;
import net.runelite.mapping.ObfuscatedName;

@ObfuscatedName("av")
public abstract class av {

    @ObfuscatedName("by")
    static lk[] by;

    @ObfuscatedName("kp")
    static RTComponent kp;

    @ObfuscatedName("t")
    int t;

    @ObfuscatedName("q")
    int q;

    @ObfuscatedName("i")
    int i;

    @ObfuscatedName("a")
    int a;

    @ObfuscatedName("l")
    int l;

    @ObfuscatedName("b")
    int b;

    @ObfuscatedName("e")
    short[][][] e;

    @ObfuscatedName("x")
    short[][][] x;

    @ObfuscatedName("p")
    byte[][][] p;

    @ObfuscatedName("g")
    byte[][][] g;

    @ObfuscatedName("n")
    am[][][][] n;

    av() {
        new LinkedList();
    }

    @ObfuscatedName("b")
    void b(int var1, int var2, ByteBuffer var3) {
        int var4 = var3.av();
        if (var4 != 0) {
            if ((var4 & 1) != 0) {
                this.e(var1, var2, var3, var4);
            } else {
                this.x(var1, var2, var3, var4);
            }

        }
    }

    @ObfuscatedName("e")
    void e(int var1, int var2, ByteBuffer var3, int var4) {
        boolean var5 = (var4 & 2) != 0;
        if (var5) {
            this.x[0][var1][var2] = (short) var3.av();
        }

        this.e[0][var1][var2] = (short) var3.av();
    }

    @ObfuscatedName("x")
    void x(int var1, int var2, ByteBuffer var3, int var4) {
        int var5 = ((var4 & 24) >> 3) + 1;
        boolean var6 = (var4 & 2) != 0;
        boolean var7 = (var4 & 4) != 0;
        this.e[0][var1][var2] = (short) var3.av();
        int var8;
        int var9;
        int var11;
        if (var6) {
            var8 = var3.av();

            for (var9 = 0; var9 < var8; ++var9) {
                int var10 = var3.av();
                if (var10 != 0) {
                    this.x[var9][var1][var2] = (short) var10;
                    var11 = var3.av();
                    this.p[var9][var1][var2] = (byte) (var11 >> 2);
                    this.g[var9][var1][var2] = (byte) (var11 & 3);
                }
            }
        }

        if (var7) {
            for (var8 = 0; var8 < var5; ++var8) {
                var9 = var3.av();
                if (var9 != 0) {
                    am[] var14 = this.n[var8][var1][var2] = new am[var9];

                    for (var11 = 0; var11 < var9; ++var11) {
                        int var12 = var3.aw();
                        int var13 = var3.av();
                        var14[var11] = new am(var12, var13 >> 2, var13 & 3);
                    }
                }
            }
        }

    }

    @ObfuscatedName("p")
    int p(int var1, int var2) {
        if (var1 >= 0 && var2 >= 0) {
            return var1 < 64 && var2 < 64 ? this.e[0][var1][var2] - 1 : -1;
        } else {
            return -1;
        }
    }

    @ObfuscatedName("o")
    int o() {
        return this.i;
    }

    @ObfuscatedName("c")
    int c() {
        return this.a;
    }

    @ObfuscatedName("fz")
    static final void fz() {
        if (Client.ei > 0) {
            ej.fu();
        } else {
            Client.ey.q();
            d.ea(40);
            ah.eu = Client.ee.e();
            Client.ee.b();
        }
    }

    @ObfuscatedName("gt")
    static final void gt(Character var0, int var1) {
        m.gr(var0.regionX, var0.regionY, var1);
    }
}
